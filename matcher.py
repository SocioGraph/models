import os, sys, re
import decimal
import helpers as hp
import pgpersist as db
from pgpersist import pgpdict, pgparray, numberlist, stringlist, datetime, dtime, NoneType, date, get_meta_classes, geolocation, timedelta
from copy import deepcopy as copyof, copy
import numpy as np
from persist import PersistKeyError
from operator import itemgetter
import time
from image_operations import ImageMapper
    

MAX_BINS = 20
MIN_RES = 0.5

DEBUG_LEVEL = hp.logging.INFO
logger = hp.get_logger(__name__, DEBUG_LEVEL)

NULL = u'__NULL__'
NAN = np.nan

NGRAM = 5

class BinnerError(hp.BaseError):
    pass

class MatcherError(hp.BaseError):
    pass
class MatcherValueError(hp.BaseError):
    pass
class MatcherKeyError(hp.BaseError):
    pass

class MatcherDict(pgpdict):
    _allowed_value_types = (float, )

class IDFDict(pgpdict):
    _allowed_value_types = (int, )

    def add_word(self, word, decode = True):
        if decode:
            word = unicode(word, errors = 'ignore')
        if word:
            w = self.iadd(word, 1, int)
            m = self.imax(NULL, w, int)
            return float(w)/m
        return 0

    def get_word(self, word, decode = True):
        if decode:
            word = unicode(word, errors = 'ignore')
        if word:
            w = self.get(word, 0.)
            m = self.get(NULL, 1e-32)
            return float(w)/m
        return 0

    def yield_terms(self, words, add = False):
        if isinstance(words, (str, unicode)):
            words = words.split()
        words = hp.make_list(words)
        tf = {}
        idf = {}
        words = map(lambda x: x if isinstance(x, unicode) else unicode(x, errors = 'ignore'), words)
        l = len(words)
        def do_add(word):
            tf[word]  = tf.get(word, -1) + 1
            if add:
                idf[word] = self.add_word(word, decode = False)
            else:
                idf[word] = self.get_word(word, decode = False)
        for word in words:
            do_add(word)
        for r in range(1, NGRAM + 1):
            # ngram is 3
            for i in range(l - r):
                do_add(' '.join(words[i:i+r]))

        num_terms = max(min(20, l/10), 3)
        for word in sorted(tf.iterkeys(), key = lambda x: tf[x]/max(idf[x], 1e-32), reverse = True)[:num_terms]:
            yield word

class IndexerDict(pgpdict):
    _allowed_value_types = (int, )

    def add_index(self, attribute, value):
        key = [attribute, value]
        s = """
            INSERT INTO {table_name} ({key_names}, int, created, updated) 
            VALUES (
                {key_place}, 
                (SELECT COUNT(*) FROM {table_name}), 
                now(), now()
            )
            ON CONFLICT ({key_names})
            DO NOTHING RETURNING int;
            """.format(
            table_name = self.name,
            key_names = ', '.join(self.key_names),
            key_place = ', '.join(['%s']*len(key)),
        )
        value_in_statment = tuple(key)
        cur = self._connect(cursor = True, new = True)
        logger.debug("Indexer statement: " + s, *value_in_statment)
        cur = self._execute(s, values = value_in_statment, cur = cur)
        if cur.rowcount:
            ret = cur.fetchone()[0]
        else:
            ret = None
        if not self._in_transaction:
            self._commit(cur)
            self._close(cur)
        return ret

    def get_index(self, attribute, value):
        key = [attribute, value]
        s = """
            SELECT int from {table_name} WHERE {key_place};
            """.format(
            table_name = self.name,
            key_place = ' AND '.join(['{}=(%s)'.format(k) for k in self.key_names]),
        )
        value_in_statment = tuple(key)
        cur = self._connect(cursor = True, new = True)
        cur = self._execute(s, values = value_in_statment, cur = cur)
        if cur.rowcount:            
            return cur.fetchone()[0]
        self._close(cur)
        return None

class CacheDict(pgpdict):
    _allowed_value_types = (str, float, stringlist)

class Binner(object):

    def __init__(self, name, **kwargs):
        self.name = hp.normalize_join(name)
        self._bins = MatcherDict(self.name, key_types = [unicode, float, unicode], key_names = ['attribute', 'bin_id', 'stat'], **kwargs)
        self._transaction = self._bins._transaction

    def clear(self):
        self._bins.clear()
        self.__init__(self.name)

    def get_edges(self, attribute, bin_id):
        bin_id = float(bin_id)
        min_bin = self._bins.get((attribute, bin_id, 'min_edge'), None)
        max_bin = self._bins.get((attribute, bin_id, 'max_edge'), None)
        return min_bin, max_bin

    def get_range(self, attribute, bin_id, value = None):
        if value is not None:
            minimum = self._bins.imin((attribute, bin_id, 'min'), value)
            maximum = self._bins.imax((attribute, bin_id, 'max'), value)
        else:
            minimum = self._bins.get((attribute, bin_id, 'min'))
            maximum = self._bins.get((attribute, bin_id, 'max'))
        return minimum, maximum

    def get_bin_id(self, attribute, value, number = 1., add = False):
        value = float(value)
        number = np.round(float(number))
        s = "SELECT bin_id FROM {} WHERE attribute = %s AND stat = 'min_edge' AND float <= %s ORDER BY float DESC LIMIT 1".format(self.name)
        cur = self._bins._execute(s, values = (attribute, value))
        rows = [row for row in cur.fetchall()]
        min_bin_id = rows[0][0] if len(rows) > 0 else None
        s = "SELECT bin_id FROM {} WHERE attribute = %s AND stat = 'max_edge' AND float > %s ORDER BY float LIMIT 1".format(self.name)
        cur = self._bins._execute(s, values = (attribute, value))
        rows = [row for row in cur.fetchall()]
        max_bin_id = rows[0][0] if len(rows) > 0 else None
        if min_bin_id is None and max_bin_id is None:
            bin_id = value
            self._bins[(attribute, bin_id, 'min_edge')] = value - MIN_RES*value if value !=0 else -1.
            self._bins[(attribute, bin_id, 'max_edge')] = value + MIN_RES*value if value !=0 else 1.
        elif min_bin_id is None:
            max_edge = self._bins[(attribute, max_bin_id, 'min_edge')]
            if value > 0:
                min_edge = value - MIN_RES*value 
            elif value < 0:
                min_edge = value + MIN_RES*value
            else:
                min_edge = -max_edge
            bin_id = min_edge + (max_edge - min_edge)/2.
            self._bins[(attribute, bin_id, 'min_edge')] = min_edge
            self._bins[(attribute, bin_id, 'max_edge')] = max_edge
        elif max_bin_id is None:
            min_edge = self._bins[(attribute, min_bin_id, 'max_edge')]
            if value > 0:
                max_edge = value + MIN_RES*value 
            elif value < 0:
                max_edge = value - MIN_RES*value
            else:
                max_edge = -min_edge
            bin_id = min_edge + (max_edge - min_edge)/2.
            self._bins[(attribute, bin_id, 'min_edge')] = min_edge
            self._bins[(attribute, bin_id, 'max_edge')] = max_edge
        elif max_bin_id != min_bin_id:
            raise BinnerError("Seems like something wrong, min_bin and max_bin give different ids for attribute {}, value {}: {} {}".format(
                    attribute, value,
                    min_bin_id, max_bin_id
                )
            )
        else:
            bin_id = max_bin_id
        if add:
            bin_total = self._bins.iadd((attribute, bin_id, 'bin_total'), number)
            total = self._bins.iadd((attribute, NAN, 'attribute_total'), number)
            minimum, maximum = self.get_range(attribute, bin_id, value)
            min_bin, max_bin = self.get_edges(attribute, bin_id)

            if bin_total > max(2, number) and total > max(10, number) and bin_total > 0.2*total and maximum - minimum > 0:
                split_boundary = minimum + (maximum - minimum)/2.0
                try:
                    if split_boundary < bin_id:
                        self._bins[(attribute, bin_id, 'min_edge')] = split_boundary
                        self._bins[(attribute, bin_id, 'min')] = split_boundary
                        new_bin_id = min_bin + (split_boundary - min_bin)/2.0
                        self._bins[(attribute, new_bin_id, 'min_edge')] = min_bin
                        self._bins[(attribute, new_bin_id, 'max_edge')] = split_boundary
                        self._bins[(attribute, new_bin_id, 'max')] = split_boundary
                        self._bins[(attribute, new_bin_id, 'min')] = minimum
                        self._bins[(attribute, bin_id, 'bin_total')] = np.ceil(bin_total/2.)
                        self._bins[(attribute, new_bin_id, 'bin_total')] = np.floor(bin_total/2.)
                        if value < split_boundary: bin_id = new_bin_id
                    else:
                        self._bins[(attribute, bin_id, 'max_edge')] = split_boundary
                        self._bins[(attribute, bin_id, 'max')] = split_boundary
                        new_bin_id = split_boundary + (max_bin - split_boundary)/2.0
                        self._bins[(attribute, new_bin_id, 'min_edge')] = split_boundary
                        self._bins[(attribute, new_bin_id, 'max_edge')] = max_bin
                        self._bins[(attribute, new_bin_id, 'max')] = maximum
                        self._bins[(attribute, new_bin_id, 'min')] = split_boundary
                        self._bins[(attribute, bin_id, 'bin_total')] = np.ceil(bin_total/2.)
                        self._bins[(attribute, new_bin_id, 'bin_total')] = np.floor(bin_total/2.)
                except TypeError:
                    print('Error causing bin id : {}'.format(bin_id))
                    print('Min_bin and max_bin are : {}, {}'.format(min_bin, max_bin))
                    print('Minimum and maximum are : {}, {}'.format(minimum, maximum))
                    pass
                if value > split_boundary: bin_id = new_bin_id
        return bin_id


class metamatcher(type):
    all_instances = {}
    def __call__(cls, name, *args, **kwargs):
        return get_meta_classes(cls, metamatcher.all_instances, name, *args, **kwargs)


class Matcher(object):
    _allowed_value_types = (unicode, str, float, int, long, decimal.Decimal)
    __metaclass__ = metamatcher

    def __init__(self, name, binner_name = None, key_names = None, key_types = None, attribute_name = None, indexer_name = None, document_mapper_name = None, image_mapper_name = None, **kwargs):
        """
        extra_keys tells how many extra keys we need to add. Note they have to be of type list of str or integer
        key_names 
        resolution is the initial resolution neededd to compute histogram bins for scalar type of items
        init_dict is the version of dictionary we need to use. It is of type pdict
        """
        self.name = hp.normalize_join(name)
        self.stats_name = self.name + '_stats'
        self.dict_name = self.name + '_dict'
        self._dict = CacheDict(self.dict_name, **kwargs)
        self._transaction = self._dict._transaction
        self._key_names = hp.make_list(key_names or self._dict.get('_key_names', []))
        if not self._key_names:
            raise MatcherError("Key names are not provided or stored.")
        self._key_types = hp.make_list(key_types or self._dict.name2types(self._dict.get('_key_types', ['unicode']), list))
        self._aname = attribute_name or self._dict.get('_aname', 'attribute')
        self.binner_name = binner_name or self._dict.get('_binner', self.name + '_binner')
        self.binner = Binner(self.binner_name, **kwargs)
        # Dict for collecting the stats
        key_names = self._key_names + [self._aname, '_stats']
        key_types = self._key_types + [unicode, unicode]
        self._stats = MatcherDict(self.stats_name, key_types, key_names, **kwargs)
        # Main dict for collecting attributes
        key_names = self._key_names + [self._aname, 'value']
        key_types = self._key_types + [unicode, unicode]
        self._items = MatcherDict(self.name, key_types, key_names, **kwargs)
        self.indexer_name = indexer_name or self._dict.get('_indexer', self.name + '_indexer')
        self._indexer = IndexerDict(self.indexer_name, [unicode, unicode], [self._aname, 'value'], **kwargs)
        self.document_mapper_name = document_mapper_name or self._dict.get('_document_mapper_name', self.name + '_document_mapper')
        self.document_mapper = IDFDict(self.document_mapper_name, [unicode], ['term'], **kwargs)
        self.image_mapper_name = image_mapper_name or self._dict.get('_image_mapper_name', self.name + '_image_mapper')
        self.image_mapper = ImageMapper(self.image_mapper_name, DB_NAME=kwargs.get('DB_NAME',"core"))
        self._num_keys = len(self._key_names)
        do_update = False
        for n_, k_, f_ in [
                ('_key_names', '_key_names', None),
                ('_key_types', '_key_types', lambda x: self._dict.type2names(x, iterob = list)),
                ('_aname', '_aname', None),
                ('binner_name', '_binner', None),
                ('indexer_name', '_indexer', None),
                ('document_mapper_name', '_document_mapper_name', None),
                ('image_mapper_name', '_image_mapper_name', None),
            ]:
            n = getattr(self, n_)
            if hasattr(f_, '__call__'): n = f_(n)
            if n != self._dict.get(k_):
                self._dict[k_] = n 
                do_update = True
        self.null_keys = [NULL if issubclass(k, (str, unicode)) else NAN for k in self._key_types]
        self._mem_cache_name = self.name + '_' + kwargs['DB_NAME'] if 'DB_NAME' in kwargs else self.name
        if do_update:
            self._dict._update_master()
   
    def clear(self, attached = False):
        self.drop(attached)

    def _get_last_updated(self, default = None):
        self._dict._get_last_updated(default)

    def drop(self, attached = False):
        self._items.clear()
        self._stats.clear()
        self._dict.clear()
        if attached:
            self.binner.clear()
            self.document_mapper.clear()
            self._indexer.clear()
        self.__metaclass__.all_instances.pop(self._mem_cache_name, None)

    def _converter(self, attribute, value, add = False):
        if isinstance(value, (int, long, float, decimal.Decimal)):
            return unicode(self.binner.get_bin_id(attribute, value, add = add))
        return unicode(hp.normalize_join(value), errors='ignore')

    def _get_keys(self, instance, pop = True):
        keys = []
        for key in self._key_names:
            try:
                if pop:
                    keys.append(instance.pop(key))
                else:
                    keys.append(instance.get(key))
            except KeyError:
                raise MatcherKeyError("Key {} is not present in instance: {}".format(key, instance), log = False)
        return keys
        
    
    def _iter_attrs(self, instance, instance_types = None, add = False):
        def yield_attrs(attribute, value, value_type, normalize = True):
            if isinstance(value_type, (str, unicode)):
                if value_type in [
                    'name', 'uid', 'id', 'email', 'url', 'phone_number', 'mobile_number', 'password', 'audio', 'video', 'file', 'ip_address', 
                    "ip_address", "ipv6", "mac_address", "device_id", "access_token", "push_token", "facebook_id", "reddit_username",
                    "pintrest_username", "vine_username", "linkedin_id", "twitter_handle", "instagram_username", "sequence", 'image_list', 'image_object', 
                    'filter_attribute_link', "media_map", "action_settings"
                    ]:
                    raise StopIteration
                elif value_type in [
                    "pincode", "zipcode", "areacode", "city", "state", "county", "country", "region", "zone", "category", "rating"
                    ]:
                    yield attribute, hp.normalize_join(value)
                    raise StopIteration
                elif value_type in ["true_false", "yes_no", "bool", "boolean"]:
                    if str(value).lower() in ['true', 'yes', 'y', '1', 'on']:
                        yield attribute, 'true'
                    else:
                        yield attribute, 'false'
                    raise StopIteration
                elif value_type in ['description']:
                    for v in re.split('.;|:,', value):
                        # for longer texts, put each word separately
                        v = hp.normalize(v)
                        if v.count(' ') <= NGRAM:
                            yield attribute, self._converter(attribute, v, add = add) if normalize else v
                        else:
                            for w in self.document_mapper.yield_terms(v, add = add):
                                yield attribute, self._converter(attribute, w, add = add) if normalize else w
                    raise StopIteration
                elif value_type in ['image']:
                    for z in self.image_mapper.yield_k(value, add = add):
                        yield attribute, z
                    raise StopIteration
                elif value_type in ['address']:
                    for v in re.split(',.;', value):
                        # for longer texts, put each word separately
                        v = hp.normalize(v)
                        yield attribute, self._converter(attribute, v, add = add) if normalize else v
                    raise StopIteration
                elif value_type in ['discount', 'price', 'tax', 'currency', 'discount_percentage', 'tax_percentage', 'percentage', 'age']:
                    yield attribute, hp.resolve_log(value)
                    raise StopIteration
                else:
                    value_type = type(value)
            if issubclass(value_type, (geolocation)):
                yield attribute + '__1km', value.decimalize(3)
                yield attribute + '__11km', value.decimalize(2)
                yield attribute + '__111km', value.decimalize(1)
            elif issubclass(value_type, (list, tuple)):
                for v in value:
                    for y in yield_attrs(attribute, v, type(v)):
                        yield y
            elif issubclass(value_type, dict):
                for k, v in value.iteritems():
                    for y in yield_attrs(
                            '{}__{}'.format(attribute, self._converter(attribute, k, add = add)), 
                            self._converter(attribute, v, add = add),
                            unicode, 
                            normalize = False
                        ):
                        yield y
            elif issubclass(value_type, NoneType):
                pass
            elif issubclass(value_type, bool):
                yield attribute, str(value).lower()
            elif not value:
                pass
            elif issubclass(value_type, datetime):
                yield attribute + '__hour', str(value.hour)
                yield attribute + '__hour_quart', str(6*value.hour/6)
                yield attribute + '__month', str(value.month)
                yield attribute + '__day', str(value.isoweekday())
                yield attribute + '__year', str(value.year)
                yield attribute + '__decade', str(10*(value.year/10))
            elif issubclass(value_type, date):
                yield attribute + '__month', str(value.month)
                yield attribute + '__day', str(value.isoweekday())
                yield attribute + '__year', str(value.year)
                yield attribute + '__decade', str(10*(value.year/10))
            elif issubclass(value_type, dtime):
                yield attribute + '__hour', str(value.hour)
                yield attribute + '__hour_quart', str(6*value.hour/6)
            elif issubclass(value_type, (str, unicode)):
                if value.startswith('http'):
                    # Ignore if URL
                    raise StopIteration
                # Split based on comma
                for v in value.split(','):
                    # for longer texts, put each word separately
                    v = hp.normalize(v)
                    if v.count(' ') <= NGRAM:
                        yield attribute, self._converter(attribute, v, add = add) if normalize else v
                    else:
                        for w in self.document_mapper.yield_terms(v, add = add):
                            yield attribute, self._converter(attribute, w, add = add) if normalize else w
            elif issubclass(value_type, self._allowed_value_types):
                yield attribute, self._converter(attribute, value, add = add) if normalize else value
            else:
                raise MatcherValueError("Don't know how to handle value {} of type {} (attribute = {}). Allowed types are {}".format(
                        value, type(value), attribute, self._allowed_value_types,
                    )
                )
        if instance_types is None: instance_types = {}
        iterd = instance.iteritems() if isinstance(instance, dict) else instance
        for attribute, value in iterd:
            for y in yield_attrs(attribute, value, instance_types.get(attribute, type(value))):
                logger.debug('Yielding attribute - value: %s', y)
                yield y

    def get_repeat_gap(self, keys, attr):
        keys = hp.make_list(keys)
        ret = self._stats.get(keys + [attr, 'repeat_gap_mean'])
        if ret is None:
            return None
        st = self._stats.get(keys + [attr, 'repeat_gap_sqr'])
        den = self._stats.get(keys + [attr, 'attr_num_items'])
        if den > 1:
            mn = float(ret)/den
            return mn - 0.8*mn
        return float(ret)


    def cap(self, id_or_instance, **kwargs):
        if isinstance(id_or_instance, dict):
            keys = self._get_keys(id_or_instance, pop = False)
        else:
            keys = hp.make_list(id_or_instance)
        st = hp.time()
        pop_dict = dict(self._iter_display(keys, capped = True))
        logger.debug("pop_dict: %s", pop_dict.keys())
        r = self.add(id_or_instance, pop_dict = pop_dict, **kwargs)
        self.delete(keys, attribute_list = pop_dict.keys())
        return r

    def _add(self, instance, number = 1., timestamp_attr = None, instance_types = None, pop_dict = None, **kwargs):
        keys = self._get_keys(instance)
        if any(k_ is None for k_ in keys):
            raise MatcherKeyError("Empty keys found while adding instance {}".format(instance))
        ret_number = 0
        timestamp = instance.pop(timestamp_attr, None) if isinstance(timestamp_attr, (str, unicode)) else None
        st = hp.time()
        for attr, value in self._iter_attrs(instance, add = True, instance_types = instance_types):
            pk = (attr, value)
            if pop_dict and pop_dict.get(pk) == number:
                pop_dict.pop(pk)
                continue
            logger.info("Time to get %s: %s (timestamp = %s): %s", attr, value, timestamp, hp.time() - st)
            st = hp.time()
            with self._items._transaction():
                if timestamp:
                    nitem, time_diff = self._items.iadd(keys + [attr, value], number, float, timestamp = timestamp)
                    if time_diff is not None:
                        # Adding the repeat gap between the different keys
                        self._stats.iadd(keys + [attr, 'repeat_gap_mean'], time_diff, float)
                        self._stats.iadd(keys + [attr, 'repeat_gap_sqr'], time_diff**2, float)
                else:
                    nitem = self._items.iadd(keys + [attr, value], number, float)
                # Frequency of Attribute specific accumulation over all values
                self._items.iadd(self.null_keys + [attr, value], 1., float)
                # iadd, imax etc. are defined in pgpersist
                self._stats.iadd(self.null_keys + [attr, 'attr_total_items'], number, float)  # total is stored for each attribute
                self._stats.iadd(self.null_keys + [attr, 'attr_num_items'], 1., float)  # total is stored for each attribute
                self._stats.imax(keys + [NULL, 'id_max_bin'], abs(nitem), float) # max bin is stored for each id
                self._stats.imax(self.null_keys + [attr, 'attr_max_bin'], abs(nitem), float) # max bin is stored for each attribute
                self._indexer.add_index(attr, value)
                ret_number += 1
                logger.info("Time to add %s: %s (timestamp = %s): %s", attr, value, timestamp, hp.time() - st)
                st = hp.time()
                if pop_dict:
                    pop_dict.pop(pk, None)
        self._stats.iadd(keys + [NULL, 'id_total_items'], number, float)  # total is stored for each id
        self._stats.iadd(keys + [NULL, 'id_num_items'], 1., float)  # Number of interactions
        return ret_number

    def _iter_statistic(self, keys):
        lk = len(keys)
        for c in range(lk) - 1:
            for combs in itertools.combinations(range(lk), c):
                nk = keys.copy()
                for i in combs:
                    nk[i] = NULL
                yield nk + [stat]

    def add(self, instance, number = 1, **kwargs):
        """
        Num adds the item 'num' times
        """
        return self._add(instance, number, **kwargs)

    def delete(self, id_or_instance, attribute_list = None):
        if isinstance(id_or_instance, dict):
            keys = self._get_keys(id_or_instance, pop = False)
        else:
            keys = hp.make_list(id_or_instance)
        st = hp.time()
        d = dict(self._iter_display(keys, capped = True))
        logger.debug("Time to display: %s", hp.time() - st)
        for key, score in d.iteritems():
            if isinstance(attribute_list, list) and key not in attribute_list:
                continue
            key = list(key)
            self._items.pop(keys + key, None)
            self._items.iadd(self.null_keys + key, -score, float)
            self._stats.iadd(self.null_keys + [key[0], 'attr_total_items'], -score, float)  # total is stored for each attribute
            self._stats.iadd(keys + [key[0], 'attr_num_items'], -1., float)  # total is stored for each attribute
            logger.debug("Time to delete %s: %s", key, hp.time() - st)

    def transfer(self, id_or_instance1, id_or_instance2):
        if isinstance(id_or_instance1, dict):
            keys1 = self._get_keys(id_or_instance1)
        else:
            keys1 = hp.make_list(id_or_instance1)
        if isinstance(id_or_instance2, dict):
            keys2 = self._get_keys(id_or_instance2)
        else:
            keys2 = hp.make_list(id_or_instance2)
        d = dict(self._iter_display(keys1, capped = True))
        def pop_add(dictt, plus_keys, func = 'iadd', add_value = None):
            a = dictt.pop(keys1 + plus_keys, 0)
            return getattr(dictt, func)(keys2 + plus_keys, add_value or a, float)
        for key, score in d.iteritems():
            key = list(key)
            attr = key[0]
            value = key[1]
            nitem = pop_add(self._items, key)
            pop_add(self._stats, [attr, 'attr_num_items'], 'iadd')
            pop_add(self._stats, [NULL, 'id_max_bin'], 'imax', abs(nitem))
            pop_add(self._stats, [attr, 'repeat_gap_mean'])
            pop_add(self._stats, [attr, 'repeat_gap_sqr'])
            self._stats.imax(self.null_keys + [attr, 'attr_max_bin'], abs(nitem), float) # max bin is stored for each attribute
        pop_add(self._stats, [NULL, 'id_total_items'])
        pop_add(self._stats, [NULL, 'id_num_items'])


            
    def remove(self, instance, num = 1, **kwargs):
        """
        removes the item 'num' times
        """
        return self.add(instance, -num, **kwargs)

    def replace(self, instance1, instance2, num1 = 1, num2 = 1, **kwargs):
        """
        Replaces item1 with item2
        """
        self.add(instance2, num2, **kwargs)
        return self.remove(instance1, num1, **kwargs)

    def _get_attribute(self, attribute):
        return attribute.split('__')[0]

    def _value_to_display(self, attribute, value):
        attribute = self._get_attribute(attribute)
        logger.debug("To display value: %s", value)
        try:
            mn, mx = self.binner.get_edges(attribute, value)
        except ValueError:
            return value.replace('_', ' ').title()
        if mx is not None and mn is not None:
            return value.replace('_', ' ').title()
        logger.debug("Float value: %s", value)
        logger.debug("Edges are: %s, %s", mx)
        return '{}-{}'.format(*self.binner.get_edges(attribute, value))

    def pretty_display(self, attribute, value, separator = ':'):
        return '{} {} {}'.format(attribute.replace('_', ' ').title(), separator, self._value_to_display(value))

    def is_scalar(self, value):
        if isinstance(value, (int, float, long)):
            return True
        s = value.split('__')
        if len(s)>1:
            try:
                float(s[1])
            except ValueError:
                return False
            return True
        try:
            float(value)
        except ValueError:
            return False
        return True

    def __iter__(
            self, filter_by = None, keys = None, not_keys = None, rating_threshold = None, operator = '>', 
            get_score = False, updated_after = None, aggregate = False, now = None
        ):
        """
        filter_by: provides filter clauses for the matcher attributes
        keys is either a list or a tuple of lists of ids that we want to match with
        rating_threshold and operator provide a threshold for how high or low you want the rating to be within the iterator
        if get_score is True, then ratio of (num_matches per key)/(max matches of all the keys) is returned along with the ids
        yields ids (list)
        yields ids (list), score (float)
        """
        values = []
        filter_clauses = []
        if filter_by:
            logger.debug('in filter_by for matcher __iter__ (%s): %s', self.name, filter_by)
            filter_clause = ' ('
            clauses = []
            iteratord = filter_by.iteritems() if isinstance(filter_by, dict) else filter_by
            for k, v in iteratord:
                eq = '='
                lnot = ''
                if k.endswith('~'):
                    lnot = ' NOT'
                    eg = '<>'
                    k = k[:-1]
                if isinstance(v, (list, tuple)):
                    # cl = "attribute = <key> and value IN <value>" 
                    cl = '({} = %s AND {}{} IN %s)'.format(self._aname, 'value', lnot)
                    v = tuple(v)
                elif v is None:
                    # cl = "attribute = <key>" value can be anything 
                    cl = '({} = %s)'.format(self._aname)
                else:
                    # cl = "attribute = <key> and value = <value>" 
                    cl = '({} = %s AND {} {} %s)'.format(self._aname, 'value', eq)
                if isinstance(rating_threshold, (int, long, float)):
                    cl = '({} {} %s AND '.format('float', operator) + cl + ')'
                    values.append(float(rating_threshold))
                clauses.append(cl)
                values.append(k)
                if v is not None:
                    values.append(self._converter(k, v))
            filter_clause += ' OR '.join(clauses)
            filter_clause += ')'
            if clauses:
                filter_clauses.append(filter_clause)
        if keys:
            logger.debug('in keys for matcher __iter__ (%s): %s', self.name, filter_by)
            filter_clause = ' ('
            clauses = []
            keys = hp.make_tuple(keys)
            if not keys[0]:
                raise StopIteration
            for i, k in enumerate(self._key_names):
                clauses.append('{}{} IN %s'.format(k, ' NOT' if not_keys else ''))
                if isinstance(keys[0], (list, tuple)):
                    values.append(hp.make_tuple(keys[i]))
                else:
                    values.append(keys)
            filter_clause += ' AND '.join(clauses)
            filter_clause += ')'
            if clauses:
                filter_clauses.append(filter_clause)
        if isinstance(updated_after, datetime):
            logger.debug('in updated_after for matcher __iter__ (%s): %s', self.name, filter_by)
            filter_clause = '(updated >= %s)'
            values.append(updated_after)
            filter_clauses.append(filter_clause)
        if isinstance(now, datetime):
            logger.debug('in now for matcher __iter__ (%s): %s', self.name, filter_by)
            filter_clause = '(updated <= %s)'
            values.append(now)
            filter_clauses.append(filter_clause)

        filter_clause = ''
        if filter_clauses:
            filter_clause += ' WHERE {}'.format(' AND '.join(filter_clauses))

        score = ''
        order_by = ''
        if get_score:
            score += ', count({k}) as sm'.format(k=', '.join(self._key_names)) 
            order_by = ' ORDER BY sm DESC'
        if aggregate:
            score += ', array_agg({}), array_agg(value), array_agg(float), sum(float) as sm'.format(self._aname)
            order_by = ' ORDER BY sm DESC'

        s = "SELECT {key_names}{score} FROM {table_name}{filter_clause} GROUP BY {group_by}{order_by}".format(
            key_names = ', '.join(self._key_names),
            table_name = self.name,
            filter_clause = filter_clause,
            score = score,
            group_by = ', '.join(self._key_names),
            order_by = order_by
        )
        # e.g. call: SELECT user_id from user_matcher where attribute = <attribute> AND value = <value> OR attritbute = <attribute> AND value = <value> 
        #             GROUP_BY user_id
        # e.g. call with get_score: 
        #       SELECT user_id, count(user_id), max(user_id) FROM user_matcher 
        #             WHERE  attribute = <attribute> AND value = <value> OR attritbute = <attribute> AND value = <value> 
        #             GROUP_BY user_id
        logger.debug("Select query for matcher __iter__: " + s, *values)
        cur = None
        try:
            cur = self._items._connect(cursor = True, new = True, named = True)
            cur = self._items._execute(s, values = values, cur = cur)
            for record in cur:
                if not any(k == NULL or (isinstance(k, (int, float)) and np.isnan(k)) for k in record[:self._num_keys]):
                    yield (hp.make_single(record[:self._num_keys], list),) + tuple(record[self._num_keys:]) if get_score or aggregate else hp.make_single(record)
        finally:
            if cur: self._items._close(cur)

    iterkeys = __iter__

    def filter_similar(self, *args, **kwargs):
        """
        wrapper for gen_filter_similar to returns a list instead of an iterator
        """
        return_list = []
        for k in self.gen_filter_similar(*args, **kwargs):
            return_list.append(k)
        return return_list

    def make_filter_by(self, filter_by, instance_types):
        if filter_by is None: filter_by = {}
        for k, v in self._iter_attrs(filter_by, instance_types = instance_types, add = False):
            if not isinstance(filter_by[k], list):
                filter_by[k] = hp.make_list(filter_by[k])
            filter_by[k].append(v)
        return filter_by

    def obj_to_instance(self, obj, instance_types = None, number = 1.):
        for k, v in self._iter_attrs(obj, instance_types = instance_types, add = False):
            yield (k, v), number

    def gen_filter_similar(
            self, id_or_instance, other_matcher = None, filter_keys = None, not_keys = False, get_score = False, max_score_adder = None, limit = None,
            filter_by = None, instance_types = None
        ):
        """
        id_or_instance : if id, then all the attributes and values for the id are taken
                         if instances, then it is considered as the attriubutes and values
        other_matcher: If another matcher is provided for doing the filtering
        filter_keys: If provided then only these keys are used for filtering further
        get_score: if true, then the response is a tuple of (ids, score)
        max_score_adder: is a function which will add the maximum possible score, that is of self using the function
        output: yield ids
                yield ids, score
        """
        filter_by = self.make_filter_by(filter_by, instance_types)
        if isinstance(id_or_instance, dict):
            try:
                keys = self._get_keys(id_or_instance)
            except MatcherKeyError:
                keys = None
            filter_by.update(id_or_instance.iterkeys())
        else:
            try:
                filter_by.update(k for k, v in self._iter_display(id_or_instance, internal = True))
                keys = id_or_instance
            except PersistKeyError:
                raise StopIteration
        pop_self = False 
        if other_matcher is None: 
            other_matcher = self
            if not isinstance(id_or_instance, dict):
                pop_self = True
        logger.debug("In gen_filter_similar, filter_by: %s", filter_by)
        logger.debug("In gen_filter_similar, filter_keys: %s", filter_keys)
        i = -1
        last_sc = None
        for k in other_matcher.iterkeys(filter_by = filter_by, keys = filter_keys, not_keys = not_keys, get_score = get_score):
            i += 1
            if get_score and hasattr(max_score_adder, '__call__'):
                max_score_adder(k[-1])
                if limit and i < limit:
                    last_sc = k[1]
                elif limit and i >= limit and k[1] < last_sc:
                    break
            if pop_self and id_or_instance == (k[0] if get_score else k):
                i -= 1
                continue
            yield k

    def _iter_display(self, keys, normalizer = None, internal = False, indexes = False, capped = False):
        # Displays the histogram/accumulated bins as a dict
        if isinstance(keys, dict): 
            keys = self._get_keys(keys)
        keys = hp.make_list(keys)
        filter_by = {'AND': [(k, '=', v) for k, v in zip(self._key_names, keys) if v in keys]}
        if hasattr(normalizer, '__call__'):
            normalizer = normalizer(self, keys)
        if not isinstance(normalizer, (int, float, long)):
            try:
                normalizer = self._stats[keys + [NULL, 'id_max_bin']]
            except PersistKeyError:
                raise StopIteration
        max_bin = max(normalizer, 1e-32)
        for k, v in self._items.iteritems(filter_by = filter_by, sort_by = ('abs(float)', True)):
            attr = k[-2]
            value = k[-1]
            score = (v/max_bin if internal else v)
            if indexes:
                key = self._indexer.get_index(attr, value)
            elif capped:
                key = (attr, value)
            elif not internal:
                key = '{}={}'.format(attr, self._value_to_display(attr, value))
            else:
                key = (attr, value)
            yield key, score

    def display(self, keys, limit = None, **kwargs):
        # Displays the histogram/accumulated bins as a dict
        return_dict = {}
        nitems = 0
        for k, v in self._iter_display(keys, **kwargs):
            return_dict[k] = v
            nitems += 1
            if limit and nitems > limit:
                break
        return return_dict

    def display_indexes(self, keys, normalizer = None, internal = False):
        return_dict = {}
        for k, v in self._iter_display(keys, normalizer, internal, indexes = True):
            return_dict[k] = v
        return return_dict
            
    def distance(self, keys_or_display, other_keys_or_display, dim_normalized = 0, attribute_weights = None, update_factor = None, other_keys = None):
        """
        other_keys is provided for update_factor if other_keys_or_display is a display
        """
        if attribute_weights is None: attribute_weights = {}
        if not isinstance(keys_or_display, dict):
            iterator = self._iter_display(keys_or_display, internal = True)
        else:
            iterator = keys_or_display.iteritems()
        if not isinstance(other_keys_or_display, dict):
            other_keys = other_keys_or_display
            other_display = self.display(other_keys_or_display, internal = True)
        else:
            other_display = copyof(other_keys_or_display)
        dist = 0.
        denom = 1e-32
        for k, v1 in iterator:
            if dim_normalized < 0 and k not in other_display:
                continue
            w = attribute_weights.get(self._get_attribute(k[0]), 0.2)
            if w is None:
                w = 0.2
                # w = 1. - self.prob(k[0], k[1])
            denom += w
            if dim_normalized > 0 and k not in other_display:
                dist += w*(max(1 + v1,  1 - v1))**2
                continue
            v2 = other_display.pop(k, 0.)
            d = w*((v2 - v1)**2) 
            dist += d
            if hasattr(update_factor, '__call__'):
                update_factor(self, other_keys, self._get_attribute(k[0]), k[1], d, w)
        if dim_normalized > 0:
            for k, v1 in other_display.iteritems():
                w = attribute_weights.get(self._get_attribute(k[0]))
                if w is None: w = 1. - self.prob(k[0], k[1])
                denom += w
                dist += w*((max(1+v1, 1-v1))**2)
        return np.sqrt(dist/denom)

    def iter_distances(
            self, keys_or_display, filter_keys = None, dim_normalized = False,
            attribute_weights = None, max_distance = None, update_factor = None,
            filter_by = None, instance_types = None
        ):
        """
        if none of the optional parameters are provided, distance is calculated w.r.t all the other keys in the object
        If other keys are provided, then distance is calculated w.r.t to the keys specified
        """
        filter_by = self.make_filter_by(filter_by, instance_types)
        other_keys = filter_keys or self.iterkeys()
        if not isinstance(keys_or_display, dict):
            actual_keys = hp.make_list(keys_or_display)
            keys_or_display = self.display(keys_or_display, internal = True)
        else:
            try:
                actual_keys = self._get_keys(keys_or_display)
            except MatcherKeyError as e:
                actual_keys = []
        if isinstance(max_distance, (int, long, float)):
            if not max_distance:
                logger.warning("Received max_distance is 0")
                max_distance = max(float(2*len(keys_or_display)), 1e-16)
                logger.debug("max_distance: %s", max_distance)
        for k, attrs, values, ratings, sm in self.iterkeys(keys = filter_keys, filter_by = filter_by, aggregate = True):
            if actual_keys == hp.make_list(k):
                continue
            denom = max(self._stats[hp.make_list(k) + [NULL, 'id_max_bin']], 1e-32)
            ratings = map(lambda r: float(r)/denom, ratings)
            other_dict = dict(zip(zip(attrs, values), ratings))
            d = self.distance(keys_or_display, other_dict, dim_normalized, attribute_weights, update_factor, other_keys = k)
            if isinstance(max_distance, (int, long, float)):
                if d > max_distance:
                    logger.warning("Received max_distance is {}. Distance is {}".format(max_distance, d))
                    max_distance = max(d, 1e-16)
                yield hp.make_single(k), ((max_distance - d)/max_distance) 
            else:
                yield hp.make_single(k), d

    def iter_compare(
            self, keys, other_matcher, filter_keys = None, dim_normalized = False, 
            attribute_weights = None, max_distance = None, update_factor = None, make_key_tuple = False,
            filter_by = None, instance_types = None
        ):
        """
        This is to compare key with all values (ot other keys) of the other other_matcher
        """
        if not other_matcher.binner.name == self.binner.name:
            raise MatcherError("Cannot compare matcher {} and {}, since binners are not the same {} vs {}".format(
                    self.name, other_matcher.name, self.binner.name, other_matcher.binner.name
                )
            )
        keys_or_display = self.display(keys, internal = True)
        for k, score in other_matcher.iter_distances(
                keys_or_display, filter_keys, dim_normalized, attribute_weights, max_distance, update_factor, 
                filter_by = filter_by, instance_types = instance_types,
            ):
            if make_key_tuple: k = hp.make_tuple(k)
            yield k, score

    def prob(self, attribute, value = None):
        attr_total_items = self._stats.get((self.null_keys + [attribute, 'attr_num_items']), 0.)
        if not attr_total_items:
            return 0. if value is not None else {}
        if value is not None:
            attr_value_items = self._items.get((self.null_keys + [attribute, self._converter(attribute, value)]), 0.)
            return attr_value_items/attr_total_items
        s = "SELECT value, COUNT(value) FROM {table_name} WHERE {aname} = %s GROUP BY value".format(
            table_name = self.name, aname = self._aname
        )
        cur = self._items._connect(cursor = True, new = True)
        cur = self._items._execute(s, values = [attribute], cur = cur)
        return_dict = {}
        for record in cur.fetchall():
            return_dict[self._value_to_display(attribute, record[0])] = record[1]/attr_total_items
        self._items._close(cur)
        return return_dict

    def sample_generator(self, number):
        hist = self.display(internal = True)
        i = 0
        for v in hp.histogram_sample_generator(hist.values(), number, hist.keys()):
            if self.__item_type != 'dict':
                yield np.random.uniform(v[0],v[1])
            else:
                yield v
            i+=1
            if i >= number:
                raise StopIteration

    def sample(self):
        return self.sample_generator(1).next()

    @property
    def max_distance(self):
        s = "SELECT COUNT(*) FROM {table_name} WHERE {key_names}".format(
            key_names = ', '.join(['{} = {}'.format(k, NULL if issubclass(t, (str, unicode)) else NAN) for k, t in zip(self._key_names, self._key_types)]),
            table_name = self.name,
        )
        cur = self._items._connect(cursor = True)
        cur = self._items._execute(s, values = values, cur = cur)
        return cur[0][0]



if __name__ == '__main__':

    import json
    from time import time
    from tqdm import tqdm

    m = Matcher('test_matcher', key_names = 'id', DB_NAME='cat')
    n = 10
    tagses = ['tag1', 'tag2', 'tag3']
    start_time = time()
    for i in tqdm(range(n)):
        try:
            m.add({'id': 'a','city': 'Hyderabad', 'price': np.random.randint(20), 'tags': tagses[np.random.randint(3)], 'weighted_tags': {'tag1': np.random.randint(30), 'tag2': np.random.randint(30)}, 'description': ' '.join(['word' + str(np.random.randint(100)) for i in range(np.random.randint(50))])})
        except BinnerError:
            print('Error occured with i = {}'.format(i))
    m.add({'id': 'b','city': 'Hyderabad', 'price': 20, 'tags': ['tag1'], 'weighted_tags': {'tag1': 2.5, 'tag3': 3}})
    m.add({'id': 'c','city': 'Mumbai', 'price': 30, 'tags': ['tag3', 'tag4'], 'weighted_tags': {'tag3': 1.5, 'tag2': 7}})
    print "Show add", m.display('c')
    m.cap({'id': 'c', 'city': 'Pune', 'price': 35, 'tags': ['tag3', 'tag5'], 'weighted_tags': {'tag4': 1.6, 'tag3': 3}})
    print "Show cap", m.display('c')
    m.cap({'id': 'd', 'city': 'Pune', 'price': 35, 'tags': ['tag3', 'tag5'], 'weighted_tags': {'tag4': 1.6, 'tag3': 3}})
    print "Show before transfer", m.display('b')
    m.transfer('d', 'b')
    print "Show transfer", m.display('b')
    print "Show after transfer", m.display('d')
    
    print "Distances of 'a' with others", json.dumps(dict(m.iter_distances('a')), indent = 4)

    print "Showing a", json.dumps(m.display('a'), indent = 4)
    print "Showing a as indexes", json.dumps(m.display_indexes('a'), indent = 4)

    m1 = Matcher('another_matcher', key_names = 'id', binner_name = m.binner.name, document_mapper_name = m.document_mapper.name, DB_NAME = 'cat')
    m1.add({'id': 'd','city': 'Pune', 'price': 50, 'tags': ['tag2', 'tag4'], 'weighted_tags': {'tag1': 3, 'tag2': 2}})
    print "Show first key", json.dumps(m1.display('d'), indent = 4)
    m1.add({'id': 'e','city': 'Hyderabad', 'price': 20, 'tags': ['tag1'], 'weighted_tags': {'tag1': 2.5, 'tag3': 3}})
    print "Show second key", json.dumps(m1.display('e'), indent = 4)
    m1.add({'id': 'f','city': 'Mumbai', 'price': 30, 'tags': ['tag3', 'tag4'], 'weighted_tags': {'tag3': 1.5, 'tag2': 7}})
    print "Show third key", json.dumps(m1.display('f'), indent = 4)


    print "Distances of 'a' of matcher test_matcher with all of another_matcher", dict(m.iter_compare('a', m1))
    m2 = Matcher('test_matcher', DB_NAME = 'cat')
    m.drop()
    m1.drop(attached = True)
