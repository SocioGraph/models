from re import L
from werkzeug import secure_filename, wrappers
import tempfile
import os, sys
import libraries.json_tricks as json
import communication as com
import random
from glob import glob
from flask import Blueprint,Response, jsonify,request,session,flash,redirect,url_for, Flask, send_file, after_this_request, render_template, current_app as app, make_response
from functools import wraps
import helpers as hp
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile
from contextlib import contextmanager
from werkzeug.exceptions import BadRequest
from action import do_actions, ALGORITHM_CLASS, add_to_algo, delete_from_algo, do_action, to_action
from model import Model, CORE_MODELS, CORE_OBJECTS, AttributeTypeError
import model as base_model
import authenticate as auth
from nlpt import NlpModel, JsonBDict
from cacher import Cacher
import validators as val
import time
import uuid
import base64
from datetime import datetime,timedelta,date
import interactions
import algorithms
import traceback
import perspectives as pers
import conversation as convers
import conversation_merger as convm
from communication import Communication,View
import conversation_utils
import csv
import requests
from captcha.image import ImageCaptcha

i2ce_routes = Blueprint('i2ce_route', __name__, template_folder='templates')


logger = hp.get_logger(__name__)

class BadRequestError(hp.BaseErrorWrapper, BadRequest):
    pass

READ_METHODS = ['GET']
WRITE_METHODS = ['POST','PUT','UPDATE','PATCH']
DELETE_METHODS = ['DELETE']

I2CE_ADMIN=os.environ.get("I2CE_ADMIN","sriram@iamdave.ai")
I2CE_PASSWORD=os.environ.get("I2CE_PASSWORD","D@ve321")
I2CE_API_KEY=os.environ.get("I2CE_API_KEY", None)

WEBHOOKS = {}

CAPTCHA_FONTS = lambda: [
    joinpath(dirname(abspath(__file__)), 'captcha', 'alexbrush.ttf'),
    joinpath(dirname(abspath(__file__)), 'captcha', 'coneria.ttf'),
    joinpath(dirname(abspath(__file__)), 'captcha', 'titillium.ttf'),
    joinpath(dirname(abspath(__file__)), 'captcha', 'monterey.ttf'),
    joinpath(dirname(abspath(__file__)), 'captcha', 'vertigo.ttf')
]

def set_root_path(root_path):
    hp.mkdir_p(root_path)
    models.ROOT_PATH = root_path
    auth.ROOT_PATH = root_path


class SignUpPage(hp.I2CEError):
    pass

class DaveRequest(hp.I2CEError):
    pass

class ConversationNotFound(hp.I2CEError):
    pass

class CorsError(hp.I2CEError):
    pass

def validate_application_type(allow_multipart = False):
    if not request.headers.get('Content-Type'):
        return True
    if 'application/json' in request.headers.get('Content-Type'):
        return True
    if 'json' in request.headers.get('Content-Type'):
        return True
    if 'text/html' in request.headers.get('Content-Type'):
        return True
    if 'application/x-www-form-urlencoded' in request.headers.get('Content-Type'):
        return True
    if allow_multipart and 'multipart/form-data' in request.headers.get('Content-Type'):
        return True
    return False


def get_origin(enterprise = None, enterprise_id = None, agent_info = None):
    http_origin = request.environ.get('HTTP_ORIGIN','') or request.environ.get('Origin')
    s = app.config.get('SERVER_NAME')
    agent_info = agent_info or auth.agent_info()
    if not http_origin:
        if 'firefox' in (agent_info.get('browser') or '').lower():
            return "*"
        return "{}://{}".format(os.environ.get('HTTP', 'http'), s) if s else "*"
    enterprise = enterprise or Model('enterprise', 'core').get(enterprise_id)
    if not enterprise:
        logger.error("Enterprise Id not supplied to get_origin function")
        if 'firefox' in (agent_info.get('browser') or '').lower():
            return "*"
        return "{}://{}".format(os.environ.get('HTTP', 'http'), s) if s else "*"
    origins = hp.make_list(enterprise.get('origins',[]))
    if not s or 'localhost' in s:
        if 'firefox' in (agent_info.get('browser') or '').lower():
            return "*"
        return http_origin
    if s in http_origin:
        if 'firefox' in (agent_info.get('browser') or '').lower():
            return "*"
        return http_origin
    if http_origin in origins or "iamdave.ai" in http_origin:
        if 'firefox' in (agent_info.get('browser') or '').lower():
            return "*"
        return http_origin
    if "*" in origins:
        return "*"
    raise CorsError("Cannot serve request. Unknown Origin for request: {}".format(http_origin))

def trust_origin(enterprise_id, enterprise = None, agent_info = None, origins = None):
    http_origin = request.environ.get('HTTP_ORIGIN','')
    if not http_origin:
        return True
    enterprise = enterprise or Model('enterprise', 'core').get(enterprise_id)
    if not enterprise:
        return False
    origins = origins or hp.make_list(enterprise.get('origins',[]))
    agent_info = agent_info or auth.agent_info()
    s = app.config.get('SERVER_NAME')
    if not s or 'localhost' in s:
        return True
    if s in http_origin:
        return True
    if http_origin in origins or "iamdave.ai" in http_origin:
        return True
    if "*" in origins:
        return True
    return False

def make_dave_response(res, enterprise = None, enterprise_id = None, agent_info = None, origins = None, cookies = None, delete_cookies = None, headers = None):
    try:
        origins = origins or get_origin(enterprise, enterprise_id, agent_info)
    except CorsError as e:
        return jsonify({"error": "Cannot serve request. {}".format(e)}), 412, {'Access-Control-Allow-Origin': "*"}
    h = {}
    if isinstance(res, wrappers.Response):
        h = res.headers
    elif isinstance(res, tuple) and len(res) >= 3:
        h = res[2]
    h["Access-Control-Allow-Origin"] = origins
    h["Vary"] = "Origin"
    if isinstance(headers, dict):
        for k, v in headers.items():
            h[k] = v
    if isinstance(res, wrappers.Response):
        return res
    if isinstance(res, tuple):
        return make_response(res[0], 200 if len(res) < 2 else res[1], h)
    mr = make_response(res, 200, h)
    if isinstance(cookies, dict):
        for k, v in cookies.items():
            mr.set_cookie(k, v)
    if isinstance(delete_cookies, dict):
        for k, v in delete_cookies.items():
            mr.set_cookie(k, v, max_age = 0)
    return mr



def validate_headers(allow_html = True):
    def validate_headers_inner(f):
        @wraps(f)
        def validated(*args, **kwargs):
            if not allow_html and not validate_application_type():
                return auth.request_error('json', "Invalid application type. Only JSON and HTML are accepted", 400)
            return f(*args, **kwargs)
        return validated
    return validate_headers_inner

def authenticate(allow_html = True, allow_multipart = False):
    def authenticate_inner(f):
        @wraps(f)
        def authenticated(*args, **kwargs):
            st = time.time()
            request_type, model_name, route, ids, request_method, params = auth.parse_url(*args, **kwargs)
            agent_info = auth.agent_info()
            user = {}
            user_id = None
            origins = None
            if not allow_html and not validate_application_type(allow_multipart):
                return auth.request_error('json', "Invalid application type. Only JSON and HTML {}are accepted".format(' or Multipart' if allow_multipart else ''), 400)
            try:
                if route in ['model'] and model_name in ['enterprise','admin'] and request_method == 'read':
                    raise SignUpPage
                user_id, enterprise_id, role, api_key, push_token = auth.get_user_details(request, session, params)
                # Use this version if you want unauthenticated dave requests
                # if model_name in ['dave']:
                #    raise DaveRequest
                if not enterprise_id:
                    return auth.request_error(request_type, "No enterprise_id in AUTH", 400, 
                         redirect_to = url_for(app.config.get("LOGIN_URL",'i2ce_routes.login'), redirect_url = request.path)
                    )
                if route in ['model'] and request_method in ['delete', 'write'] and model_name in CORE_MODELS:
                    return auth.request_error(request_type, "Unauthorized access", 401)
                user = auth.authenticate_user(user_id, enterprise_id, api_key, push_token = push_token)
                permitted = auth.user_permitted(user, model_name, route, request_method, ids, params)
                origins = get_origin(enterprise_id = enterprise_id, agent_info = agent_info)
                if model_name in CORE_OBJECTS:
                    if route in auth.OBJECT_ROUTES:
                        enterprise_id = 'core'
            except (auth.UnknownUser, auth.Unauthenticated) as e:
                return auth.request_error(request_type, e, 401, redirect_to = url_for(app.config.get("LOGIN_URL",'i2ce_routes.login'), redirect_url = request.full_path))
            except auth.NotPermitted as e:
                return auth.request_error(request_type, e, 403)
            except BadRequest as e:
                hp.print_error(e)
                return auth.request_error(request_type, "Bad request, please contact admin", 400)
            except auth.ZeroBalanceError as e:
                return auth.request_error(request_type, e, 402)
            except SignUpPage:
                enterprise_id = 'core'
                user['role']='admin'
            except CorsError as e:
                return auth.request_error(request_type, e, 412)
            except DaveRequest:
                pass
            except Exception as e:
                return auth.request_error(request_type, "Unknown error while authenticating", 400)
            logger.info("Finished authenticating: %s", time.time() - st) 
            try:
                res = f(*args,
                    enterprise_id = enterprise_id,
                    role = user.get('role'),
                    request_type = request_type,
                    request_method = request_method,
                    params = params,
                    route = route,
                    ids = ids,
                    user = user,
                    user_id = user_id,
                    agent_info = agent_info,
                    **kwargs
                )
            except base_model.ModelNotFound as e:
                res = auth.request_error('json', e, 404)
            except base_model.NotPermitted as e:
                res = auth.request_error('json', e, 403)
            except Exception as e:
                hp.print_error(e)
                res = auth.request_error(request_type, e, 400, 
                    enterprise_id = enterprise_id, route = route, params = params , ids = ids, user = user
                )
            return make_dave_response(res, enterprise_id = enterprise_id, origins = origins)
        return authenticated
    return authenticate_inner

def json_download(obj,file_name):
    with tempfile.NamedTemporaryFile(delete = False) as fp:
        fileName = fp.name
        fp.write(json.dumps(obj,indent=2))
    fp.close()

    @after_this_request
    def remove_file(response):
        try:
            os.remove(fileName)
        except Exception as error:
            print "Error removing or closing downloaded file handle", error
        return response
    
    return send_file(
        fileName,
        mimetype='text/json',
        attachment_filename= file_name + ".json",
        as_attachment=True,
        cache_timeout=-1
    )


def csv_download(csv_list, keys, file_name):
    with tempfile.NamedTemporaryFile(delete = False) as fp:
        fileName = fp.name
        dict_writer = csv.DictWriter(fp, fieldnames = keys)
        dict_writer.writeheader()
        for row in csv_list:
            dict_writer.writerow(dict((k, v.encode('utf-8') if type(v) is unicode else v) for k, v in row.iteritems()))
    fp.close()

    @after_this_request
    def remove_file(response):
        try:
            os.remove(fileName)
        except Exception as error:
            hp.print_error(error)
            logger.error("Error removing or closing downloaded file handle: %s", error)
        return response
    
    return send_file(
        fileName,
        mimetype='text/csv',
        attachment_filename= file_name + ".csv",
        as_attachment=True,
        cache_timeout=-1
    )



@i2ce_routes.route('/settings/perspective/<perspective_name>', methods = ['GET', 'POST', 'DELETE'])
@authenticate(allow_html = False)
def perspective_settings(perspective_name, *args, **kwargs):
    pref = pers.Preference(kwargs['enterprise_id'], kwargs['user_id'], kwargs['role'])
    perspective = pref.perspective.get(perspective_name)
    if kwargs['role'] != 'admin':
        enterprise_set = False
        role_set = None
        kwargs['params'].pop('enterprise_set', None)
        kwargs['params'].pop('role_set', None)
    else:
        enterprise_set = kwargs['params'].pop('enterprise_set', True)
        role_set = kwargs['params'].pop('role_set', None)
    if kwargs['request_method'] == 'write':
       action = {
                   '_action': 'communicate',
                   '_name': 'update preferences',
                   "receiver_id": {},
                   "receiver_model": "login",
                   "channels": ["push_notification"],
                   '_data': {
                       "notification_data": {
                            "_action": "sync",
                            "_page": "perspective",
                            "_id": perspective_name,
                        }
                    },
                }
       e = Model('enterprise', 'core', 'admin').get(kwargs['enterprise_id'])
       actions = {'actions': [action], 'error_action': {'receiver': {'emails':  e.get('email')}}}
    if kwargs['request_method'] == 'read':
        return Response(
            json.dumps(
                pref.get_dict(
                    perspective_name,
                    {
                        preference_name: kwargs.get('params',{}).get(preference_name, preference_value.get('default')) for preference_name, preference_value in perspective.get('custom_attributes', {}).iteritems()
                    }
                ), 
                primitives = True
            ),
            mimetype = 'application/json'
        )
    elif isinstance(role_set, (str, unicode)):
        if kwargs['request_method'] == 'write':
            r = pref.role_set_dict(
                perspective_name, {
                    preference_name: preference_value for preference_name, preference_value in kwargs.get('params', {}).iteritems()
                }, 
                role_set
            )
            actions['actions'][0]["receiver_model"] = role_set
            do_actions.delay(actions, kwargs['enterprise_id'], kwargs['role'], kwargs['user_id'], return_act = False)
            return jsonify(r)
        else:
            return jsonify(
                pref.role_delete_dict(
                    perspective_name, {
                        preference_name: preference_value for preference_name, preference_value in kwargs.get('params', {}).iteritems()
                    }, 
                    role_set
                )
            )
    elif enterprise_set:
        if kwargs['request_method'] == 'write':
            r = pref.enterprise_set_dict(
                perspective_name, {
                    preference_name: preference_value for preference_name, preference_value in kwargs.get('params', {}).iteritems()
                } 
            )
            do_actions.delay(actions, kwargs['enterprise_id'], kwargs['role'], kwargs['user_id'], return_act = False)
            actions['actions'][0]["receiver_model"] = "admin"
            actions['actions'][0]["receiver_ids"] = {"enterprise_id": kwargs['enterprise_id']}
            do_actions.delay(actions, kwargs['enterprise_id'], kwargs['role'], kwargs['user_id'], return_act = False)
            return jsonify(r)
        else:
            return jsonify(
                pref.enterprise_delete_dict(
                    perspective_name, {
                        preference_name: preference_value for preference_name, preference_value in kwargs.get('params', {}).iteritems()
                    } 
                )
            )
    else:
        r = pref.set_dict(
            perspective_name, {
                preference_name: preference_value for preference_name, preference_value in kwargs.get('params', {}).iteritems()
            } 
        )
        actions['actions'][0]["receiver_model"] = kwargs["role"]
        actions['actions'][0]["receiver_id"] = [kwargs["user_id"]]
        do_actions.delay(actions, kwargs['enterprise_id'], kwargs['role'], kwargs['user_id'], return_act = False)
        return jsonify(r)


@i2ce_routes.route('/perspective/dave/attributes/<id>', methods = ['GET'])
@authenticate(allow_html = False)
def perspective_attributes(id,*args, **kwargs):
    params = kwargs.get('params')
    request_type = kwargs.pop('request_type')
    ca = pers.get_perspective_attributes(kwargs['ids'][1], kwargs['enterprise_id'], kwargs['user_id'], kwargs['role'])
    others = {}
    others['enterprise_set'] = (kwargs['role'] == 'admin')
    if others['enterprise_set']:
        roles=base_model.models(kwargs['enterprise_id'],attribute_name = 'name', has_login=True)
        others['roles']=roles
    others["perspective_id"] = id
    if params.get("_as_list"):
        others['attributes'] = ca.values()
        return Response(json.dumps(others, primitives = True), mimetype = 'application/json')
    ca.update(others)
    return Response(json.dumps(ca, primitives = True), mimetype = 'application/json')
    


@i2ce_routes.route('/perspective/dave/<id>', methods = ['GET', 'POST'])
@i2ce_routes.route('/perspective/dave', methods = ['POST'])
@authenticate(allow_html = True)
def get_perspective(id=None, *args, **kwargs):
    params = kwargs.get('params')
    request_type = kwargs.pop('request_type')
    id = id or kwargs.get("id")
    if id:
        p_model = Model('perspective', kwargs['enterprise_id'], kwargs['role'], user_id = kwargs['user']['user_id'])
        perspective = p_model.get(id)
        if not perspective:
            raise pers.PerspectiveNotFound("no perspective present at given id")
        if request.method=="POST":
            action_group_name=params.pop('action_group_name', None)
            action_group = perspective.get('post_action_group')
            if action_group_name:
                action_group[action_group_name] = action_group.get(action_group_name) or {}
                action_group[action_group_name]['data'] = action_group.get(action_group_name).get('data') or {}
                action_group[action_group_name]['data'].update(params)
            perspective['post_action_group']= action_group
            result = execute_actions(perspective['post_action_group'][action_group_name], kwargs['enterprise_id'], kwargs['role'])
            return jsonify({'info': "action execution started"})
    else:
        # Dynamic perspective
        perspective = params
    action_group = perspective.get('pre_action_group', {})
    action_group['data'] = action_group.get('data') or {}
    pref = pers.Preference(kwargs['enterprise_id'], kwargs['user_id'], kwargs['role'])
    for p, v in perspective.get('custom_attributes', {}).iteritems():
        if p not in params:
            params[p] = pref.get(perspective['name'], p, v.get('default'))
        if v.get('required') and params.get(p) is None:
            if not params.get('_no_errors', params.get('_no_error')):
                raise pers.PerspectiveParamError("Required parameter for perspective :{}: with title %{}% - !{}! not found".format(perspective['name'],perspective['title'], p))
    action_group['data'].update(params)
    perspective['pre_action_group'] = action_group
    result = do_actions(perspective.get('pre_action_group', {}), kwargs['enterprise_id'], kwargs['role'], raise_error = True, return_act = True)
    result.data["perspective_id"] = id
    if request_type == 'json':
        return jsonify(result.data)
    if perspective.get('template_name'):
        perspective_functions=pers.PersBaseFunc(kwargs['enterprise_id'],kwargs['user_id'],kwargs['role'])
        instance={
                "product_id_attribute" : perspective_functions._dict['interaction_model'].get_parent_ids(
                    perspective_functions._dict['product_model'].name
                )[0],
                "customer_id_attribute":perspective_functions._dict['interaction_model'].get_parent_ids(perspective_functions._dict['customer_model'].name)[0],
                "interaction_stage_attribute":perspective_functions._dict['interaction_model'].get_parent_ids(perspective_functions.interaction_stage_model.name)[0]
                }
        result.data['perspective_functions']=perspective_functions
        result.data.update(instance)
        return render_template(joinpath('perspectives',perspective.get('template_name')),**result.data )
    elif perspective.get('message'):
        return perspective.get('message').format(**result.flattened_data)
    return jsonify(result.data)
 
@i2ce_routes.route('/model/<model_name>', methods = ['GET', 'POST', 'DELETE'])
@authenticate(allow_html = False)
def model(model_name, *args, **kwargs):
    update_all = False
    if request.method == 'POST':
        model_json = kwargs["params"]
        update_all = model_json.pop("update_all", False)
    else:   
        model_json = None

    ret = jsonify(
        getattr(
            base_model, request.method.lower()
        )(model_name, enterprise_id = kwargs.get('enterprise_id'), role = kwargs.get('role'), model = model_json, user_id = kwargs.get('user', {}).get('user_id')
        )
    )
    if update_all:
        action = {
                    '_action': 'update_many',
                    '_name': '_update_all_{}@{}'.format(model_name, datetime.now()),
                    '_model': model_name,
                    '_data': {"_instance": {}},
                 }
        e = Model('enterprise', 'core', 'admin').get(kwargs['enterprise_id'])
        actions = {'actions': [action], 'error_action': {'receiver': {'emails':  e.get('email')}}}
        do_actions(actions, kwargs['enterprise_id'], kwargs['role'], kwargs['user_id'], return_act = False)
    return ret


@i2ce_routes.route('/conversation-manager-ui/<conversation_id>', methods = ['GET','POST'])
@authenticate(allow_html = True)
def manage_conversation_ui(conversation_id,  *args, **kwargs):
    return render_template(
            'conversation_ui.html', 
            conversation_id = conversation_id
        )

def validate_i2ce_admin(**kwargs):
    if kwargs["role"] == "admin":
        return True
    if session.get("developer_session"):
        if session.get("developer_session") + timedelta(minutes = 600 if os.environ.get('environment') != 'production' else 10) < datetime.now():
            session.pop("developer_session",None)
            flash("Session expired","error")
        else:
            session["developer_session"] = datetime.now()
            return True
    return False

def get_con_templates(lang=None):
    resp = {}
    def _temp(pt):
        tmp = glob("{}/*".format(pt))
        r = {}
        for j in tmp:
            if os.path.isdir(j):
                l = j.replace(pt+"/", "")
                with open("{}/conversation.json".format(j), "r") as fp:
                    r[l] = json.load(fp) 
        return r

    if lang:
        pt =  "models/core_conversations/{}".format(lang)
        return _temp(pt)


    fl = glob("models/core_conversations/*")
    for i in fl:
        ln = i.replace("models/core_conversations/", "")
        resp[ln] = _temp(i)

    return resp

@i2ce_routes.route('/conversation-templates/data', methods = ['GET'])
@i2ce_routes.route('/conversation-template/<lang>/<template>', methods = ['GET'])
@authenticate(allow_html = True)
def load_settings_conversation(lang=None, template=None, *args, **kwargs):
    valid = validate_i2ce_admin(**kwargs) 
    if not valid:
        return jsonify({"error": "Invalid request"}), 400, {'Access-Control-Allow-Origin': "*"}
    
    user_id = kwargs.get('user_id')
    enterprise_id = kwargs.get('enterprise_id')
    resp = {}
    m = Model("avatar", enterprise_id)
    resp["avatars"] = m.list()["data"]
    resp["templates"] = get_con_templates()

    return jsonify(resp)

@i2ce_routes.route('/merge-conversation/<language>', methods = ['POST', "GET"])
@authenticate(allow_html = False)
def merge_conversation(language="english", *args, **kwargs):
    include_templates = hp.make_list(kwargs["params"].get("include_templates", []))
    so = kwargs["params"].get("conversation", {})
    tmps = get_con_templates(language)
    if include_templates:
        lst = []
        try:
            for i in include_templates:
                if i in tmps:
                    lst.append(tmps[i])
            so = convm.merge_conversations(so, lst)

            return jsonify(so)
        except Exception as e:
            res = auth.request_error("json", e, 400)
            return res
    else:
        return jsonify(so)


@i2ce_routes.route('/conversation-manager/<conversation_id>', methods = ['GET','POST'])
@i2ce_routes.route('/conversation-manager/<conversation_id>/<mode>', methods = ['GET','POST'])
@authenticate(allow_html = True)
def manage_conversation(conversation_id, mode="console",  *args, **kwargs):
    valid = False
    if kwargs["request_method"] == "write":
        dev_user = kwargs["params"].get("dev_user")
        dev_pwd = kwargs["params"].get("dev_password")
        try:
            if dev_pwd == I2CE_PASSWORD and dev_user.lower() == I2CE_ADMIN.lower():
                session["developer_session"] = datetime.now()
                valid = True
            elif kwargs.get('role') == 'admin' and auth.authenticate_user(
                    kwargs.get('user_id'), 
                    kwargs.get('enterprise_id'), 
                    password = dev_pwd
                ):
                session["developer_session"] = datetime.now()
                valid = True
            else:
                flash("User name or password not valid", "error")
        except (auth.UnknownUser, auth.NotValidated, auth.Unauthenticated) as e:
            flash(str(e), "error")
    else:
        valid = validate_i2ce_admin(**kwargs) 
    if not valid:
        return render_template(
                'conversation_auth.html', 
                conversation_id = conversation_id
            )
    conv = convers.Conversation(kwargs['enterprise_id'], conversation_id, kwargs['role'], kwargs['user_id'])
    htmls = glob(joinpath(conv._uploads_path,"*.html"))
    js = glob(joinpath(conv._uploads_path,"*.js"))
    css = glob(joinpath(conv._uploads_path,"*.css"))
    if kwargs['request_type'] == 'json':
        tdir = joinpath(val.STATIC_DIR, kwargs['enterprise_id'], "conversations", conversation_id)
        udir = joinpath(val.CDN_DOMAIN, kwargs['enterprise_id'], "conversations", conversation_id)
        path = joinpath(tdir, "synthesize.zip")
        if not ispath(path):
            path = convers.create_zip(tdir, path)
        else:
            convers.create_zip.apply_async(args = [tdir, path], queue = 'priority')
        nlpm = NlpModel(conversation_id, db_name = kwargs['enterprise_id'])
        scache = JsonBDict("__{}_conv_cacher".format(conversation_id), [unicode], ['text'], DB_NAME = kwargs['enterprise_id'])
        return jsonify({
            "conversation_id": conversation_id, 
            "conversation_htmls": map(lambda x: x.replace(conv._uploads_path, conv._downloads_path), htmls),
            "conversation_js": map(lambda x: x.replace(conv._uploads_path, conv._downloads_path), js),
            "conversation_css": map(lambda x: x.replace(conv._uploads_path, conv._downloads_path), css),
            "conversation_function": joinpath(conv._downloads_path, "conversation_functions.txt"),
            "conversation_zip": joinpath(udir, "synthesize.zip"),
            "conversation_cache": json.dumps(dict(nlpm._response_cache.iteritems())),
            "synthesis_cache": json.dumps(dict(scache.iteritems()))
        })
    conversation_htmls = [ os.path.basename(f).rsplit(".",1)[0] for f in htmls]
    conversation_js = [ os.path.basename(f).rsplit(".",1)[0] for f in js]
    conversation_css = [ os.path.basename(f).rsplit(".",1)[0] for f in css]
    if mode == "ui":
        return render_template(
                'conversation_ui.html', 
                conversation_id = conversation_id, 
                conversation_htmls = conversation_htmls,
                conversation_js = conversation_js,
                conversation_css = conversation_css,
                conversation_functions_exist = os.path.exists(joinpath(conv._uploads_path,"conversation_functions.txt"))
            )

    return render_template(
            'conversation_manager.html', 
            conversation_id = conversation_id, 
            conversation_htmls = conversation_htmls,
            conversation_js = conversation_js,
            conversation_css = conversation_css,
            conversation_functions_exist = os.path.exists(joinpath(conv._uploads_path,"conversation_functions.txt"))
        )


@i2ce_routes.route('/conversation-manager/<conversation_id>/<file_type>/<action_type>', methods = ['GET','POST'])
@authenticate(allow_html = True)
def manage_conversation_files(conversation_id, file_type = None, action_type = None,  *args, **kwargs):
    valid = validate_i2ce_admin(**kwargs) 
    conv = convers.Conversation(kwargs['enterprise_id'], conversation_id, kwargs['role'], kwargs['user_id'])
    if valid and action_type in ["upload","download","upload_entities"] and file_type in ["json","html","py", "css", "js", "csv"]:
        if file_type == "json":
            if action_type == "download" and kwargs['request_method'] == 'read':
                return json_download(conv.conversation, conversation_id)
            elif action_type == "upload" and kwargs['request_method'] == 'write':
                try:
                    f = request.files["conversation_file"]
                    obj = json.load(f)
                    convers.Conversation.post(kwargs["enterprise_id"], obj, kwargs["role"], kwargs["user_id"], **kwargs.get('params', {}))
                    flash("Conversation updated successfully")
                except Exception as e:
                    hp.print_error(e)
                    flash(e.message,"error")
                return redirect(url_for('i2ce_route.manage_conversation',conversation_id = conversation_id))
        elif file_type == "csv":
            if action_type == "download" and kwargs['request_method'] == 'read':
                csv_list = conversation_utils.convert_json_to_csv(conv.conversation)
                return csv_download(csv_list, csv_list[0].keys(), conversation_id)
            elif action_type == "upload" and kwargs['request_method'] == 'write':
                try:
                    f = request.files["conversation_file"]                    
                    merged_json = conversation_utils.merge_csv_to_json(f,conv.conversation)
                    

                    entity_check_passed,errors = conversation_utils.check_entities(merged_json,kwargs['enterprise_id'])
                    if not entity_check_passed:
                        raise Exception(errors)

                    follow_ups_checked,message = conversation_utils.check_follow_ups_and_state_options(merged_json)
                    if not follow_ups_checked:
                        raise Exception(message)
                    convers.Conversation.post(kwargs["enterprise_id"], merged_json, kwargs["role"], kwargs["user_id"], **kwargs.get('params', {}))
                    flash("Conversation CSV updated successfully")
                except Exception as e:
                    hp.print_error(e)
                    flash(e.message,"error")
                    logger.exception(e)
                return redirect(url_for('i2ce_route.manage_conversation',conversation_id = conversation_id))
            elif action_type == "upload_entities" and kwargs['request_method'] == 'write':
                try:
                    f = request.files["conversation_file"]
                    merged_json = conversation_utils.merge_entities(f,conv.conversation)
                    merged_json = json.dumps(merged_json)
                    convers.Conversation.post(kwargs["enterprise_id"], merged_json, kwargs["role"], kwargs["user_id"], **kwargs.get('params', {}))
                except Exception as e:
                    hp.print_error(e)
                    flash(e.message,"error")
                    logger.exception(e)
                return redirect(url_for('i2ce_route.manage_conversation',conversation_id = conversation_id))
        else:
            if file_type == "py":
                file_type = "txt"
            if action_type == "download" and kwargs['request_method'] == 'read':
                fname = kwargs["params"]["file_name"]
                try:
                    f = joinpath(conv._uploads_path,"{}.{}".format(fname,file_type))
                    return send_file(
                        f,
                        mimetype='text/*',
                        attachment_filename= "{}.{}".format(fname,file_type),
                        as_attachment=True,
                        cache_timeout=-1
                    )
                except Exception as e:
                    hp.print_error(e)
                    flash(e.message)
            elif action_type == "upload" and kwargs['request_method'] == 'write':
                try:
                    f = request.files["conversation_file"]
                    filename = secure_filename(f.filename)
                    if (file_type in ["html", "css", "js"] and filename.rsplit(".",1)[-1] in ["html" , "css", "js"]) or (file_type == "txt" and filename in ["conversation_functions.txt", "conversation_functions.py"]) :
                        if not os.path.exists(conv._uploads_path):
                            os.makedirs(conv._uploads_path)
                        if (file_type == "txt" and filename in ["conversation_functions.txt", "conversation_functions.py"]):
                            filename = "conversation_functions.txt"
                            try:
                                conv.evaluate_func_file(f, conv._uploads_path)
                                r = conv.conversation_model.update(conversation_id, {})
                                logger.info("Conversation %s is updated with update time: %s", conversation_id, r['updated_time'])
                            except Exception as e:
                                hp.print_error(e)
                                flash("Error in evaluating function file: {}".format(e.message),"error")
                            else:
                                f.seek(0)
                                f.save(os.path.join(conv._uploads_path, filename))
                                flash("Conversation functions uploaded successfully")
                        else:
                            f.save(os.path.join(conv._uploads_path, filename))
                            flash("Conversation html uploaded successfully")
                    else:
                        flash("Conversation html/funtions file not accepted","error")
                except Exception as e:
                    hp.print_error(e)
                    flash(e.message,"error")
    
    return redirect(url_for('i2ce_route.manage_conversation',conversation_id = conversation_id))

@i2ce_routes.route('/make-conversation/dave', methods = ['POST', 'PATCH', 'DELETE'])
@authenticate(allow_html = False)
def create_conversation(*args, **kwargs):
    if kwargs["role"] != "admin":
        raise auth.NotPermitted("Only admin can create/update the conversaion")

    params = kwargs.get('params', {})
    retrain = params.pop("retrain", False)
    debug_mode = params.pop("debug_mode", False)
    refresh_cache = params.pop("refresh_cache", False)
    synthesis_cache = params.pop("synthesis_cache", False)
    return jsonify(convers.Conversation.post(kwargs["enterprise_id"], params, kwargs["role"], kwargs["user_id"], retrain = retrain, debug_mode = debug_mode, refresh_cache = refresh_cache, synthesis_cache = synthesis_cache))


@i2ce_routes.route('/conversation/dave/<string:conversation_id>', methods = ['GET'])
@i2ce_routes.route('/conversation/<string:conversation_id>', methods = ['GET'])
@authenticate(allow_html = False)
def conversation(conversation_id, *args, **kwargs):
    con = convers.Conversation(kwargs['enterprise_id'], conversation_id, role = "admin")
    kwargs['conversation_id'] = conversation_id
    kwargs['conversation_name'] = con.conversation["conversation_name"]
    return con.render_template(**kwargs)

@i2ce_routes.route('/conversation-keywords/<string:conversation_id>', methods = ['GET'])
@authenticate(allow_html = False)
def conversation_keywords(conversation_id, *args, **kwargs):
    con = convers.Conversation(kwargs['enterprise_id'], conversation_id, role = "admin")
    return jsonify(keywords = list(con.get_keywords(**kwargs.get('params', {}))))

@i2ce_routes.route('/conversation-refresh/dave/<string:conversation_id>', methods = ['POST'])
@authenticate(allow_html = False)
def conversation_refresh(conversation_id, *args, **kwargs):
    """
    e.g.  POST /conversation-refresh/dave/<conversation_id>
    {
       "sentence": [
            "sentence 1",
            "sentence 2"
       ]
    }
    """
    con = convers.Conversation(kwargs['enterprise_id'], conversation_id, role = "admin")
    n = convers.clear_cache( conversation_id, kwargs['enterprise_id'], con.ner_enabled, filter_by = kwargs.get('params', {}) )
    return jsonify(info = 'Clearing cache for {} utterances'.format(n))


@i2ce_routes.route('/conversation-phrases/<string:conversation_id>/<string:customer_id>/<string:engagement_id>', methods = ['GET'])
@i2ce_routes.route('/conversation-phrases/<string:conversation_id>/<string:customer_id>', methods = ['GET'])
@authenticate(allow_html = False)
def conversation_phrases(conversation_id, customer_id, engagement_id = None, **kwargs):
    engmt = convers.Engagement(
        kwargs['enterprise_id'], 
        conversation_id = conversation_id, 
        customer_id = customer_id, role = kwargs['role'], 
        user_id = kwargs['user_id'], engagement_id = engagement_id
    )
    return jsonify(keywords = convers.get_phrases(engmt, kwargs['params'].get('language', 'english')))

@i2ce_routes.route('/conversation-history/<string:conversation_id>/<string:customer_id>/<string:engagement_id>', methods = ['GET'])
@i2ce_routes.route('/conversation-history/<string:conversation_id>/<string:customer_id>', methods = ['GET'])
@authenticate(allow_html = False)
def conversation_history(conversation_id, customer_id, engagement_id = None, **kwargs):
    engmt = convers.Engagement(
        kwargs['enterprise_id'], 
        conversation_id = conversation_id, 
        customer_id = customer_id, role = kwargs['role'], 
        user_id = kwargs['user_id'], engagement_id = engagement_id
    )
    return jsonify(history = list(engmt.get_history(**kwargs.get('params', {}))))


@i2ce_routes.route('/conversation/dave/<string:conversation_id>/<string:customer_id>', methods = ['POST'])
@i2ce_routes.route('/conversation/<string:conversation_id>/<string:customer_id>', methods = ['POST'])
@authenticate(allow_html = False, allow_multipart = True)
def engagement(conversation_id, customer_id, *args, **kwargs):
    delayed = False
    params = kwargs.get('params', {})
    if params.get("channel") and params.get("channel_id"):
        delayed = True
    if any(getattr(request.files[f], 'filename', None) for f in request.files):
        logger.info("Received files: %s", request.files)
        for i in request.files:
            f = request.files[i]
            if f and f.filename.endswith('.wav'):
                logger.info("Recognizing file: %s", f.filename)
                tfile = hp.tempfile.NamedTemporaryFile(suffix = '.wav')
                tfile.close()
                f.save(tfile.name)
                params['audio_file'] = tfile.name
            break
    if delayed:
        res = convers.next.delay(enterprise_id = kwargs["enterprise_id"], conversation_id = conversation_id, customer_id = customer_id, role = kwargs['role'], user_id = kwargs['user_id'], **params)
        return jsonify(status = "Received Message")
    
    res = convers.next(enterprise_id = kwargs["enterprise_id"], conversation_id = conversation_id, customer_id = customer_id, role = kwargs['role'], user_id = kwargs['user_id'], **params)
    
    return Response(
        json.dumps( 
            res,
            primitives = True
        ), 
        mimetype = 'application/json'
    )

@i2ce_routes.route('/shorten-link/dave', methods = ['POST'])
@authenticate(allow_html = False)
def create_link_redirector(*args, **kwargs):
    vm = base_model.Model('view', 'core')
    params = kwargs.get('params', {})
    if not params.get('url'):
        return auth.request_error(request_type, "Invalid id for redirection", 404)
    u = params.pop('url')
    if not u.startswith('http'):
        u = '{}://{}/'.format(os.environ.get("HTTP", 'http'), app.config.get('SERVER_NAME')) + u.strip('/')
    params['url'] = u
    params['enterprise_id'] = kwargs['enterprise_id']
    v = vm.post(params)
    ourl = '{}://{}/r/{}'.format(os.environ.get("HTTP", 'http'), app.config.get('SERVER_NAME'), v['view_id'])
    return jsonify({
        'view_id': v['view_id'], 
        'url': ourl,
        'qr_code': val.make_qr_code_func(ourl, directory = joinpath(val.STATIC_DIR, kwargs['enterprise_id'], 'qr_code_links'))
    })


@i2ce_routes.route('/r/<string:view_id>', methods = ['GET'])
def link_redirector(view_id, **kwargs):
    vm = base_model.Model('view', 'core')
    request_type = 'json' if 'json' in request.headers.get('Content-Type', 'html') else 'html'
    v = vm.get(view_id)
    if not v:
        return auth.request_error(request_type, "Invalid id for redirection", 404)
    if not v.get('url'):
        return auth.request_error(request_type, "Invalid id for redirection", 404)
    u = v.pop('url')
    enterprise_id = v.pop('enterprise_id', None)
    v.pop('created', None)
    pre_actions = v.pop('pre_actions', None)
    err_act = error_reciever_medium(enterprise_id = enterprise_id)
    v.pop('view_id', None)
    if isinstance(pre_actions, list):
        for a in pre_actions:
            a['_instance'] = a.get('_instance', {})
            a['_instance'].update(auth.agent_info())
            a['_instance'].update(v)
        logger.info("Got pre actions: %s", pre_actions)
        do_actions({'actions': pre_actions, 'error_action': err_act}, enterprise_id, 'admin')
    if not u.startswith('http'):
        u = '{}://{}/'.format(os.environ.get("HTTP", 'http'), app.config.get('SERVER_NAME')) + u.strip('/')
    if v:    
        u += '?' + hp.urllib.urlencode(v)
    return redirect(u, 302)

@i2ce_routes.route('/data/dave/<string:enterprise_id>/<string:web_action>', methods = ['POST'])
@validate_headers(allow_html = False)
def send_data(enterprise_id, web_action):
    def return_hook(message, response_code = 200, log_level = 'info', args = None, kwargs = None):
        args = hp.make_list(args or [])
        kwargs = kwargs or {}
        logger.log(
            getattr(hp.logging, log_level.upper(), 'INFO'), 
            "Response from data %s for enterprise ID %s: {}".format(
                json.dumps(message, indent = 4)
            ), 
            web_action, 
            enterprise_id, 
            *args, **kwargs
        )
        if isinstance(message, tuple) and isinstance(message[0], Response):
            return message[0], message[1], {"Access-Control-Allow-Origin": "*"}

        if not isinstance(message, dict):
            return message, response_code, {"Access-Control-Allow-Origin": "*"}
        return jsonify(message), response_code, {"Access-Control-Allow-Origin": "*"}
    params = request.get_json() or hp.parse_forms_dict(request.values.to_dict(flat = False))
    if enterprise_id not in WEBHOOKS:
        return return_hook({'error': "No data-hook registered for enterprise {}".format(enterprise_id)}, 400, 'error')
    if web_action not in WEBHOOKS[enterprise_id]:
        return return_hook({'error': "No data-hook {} defined for enterprise {}".format(enterprise_id, web_action)}, 400, 'error')
    logger.info("Received web-hook %s for enterprise ID %s: %s", web_action, enterprise_id, json.dumps(params, indent = 4))
    args = []
    for k in [arg1, arg2, arg3, arg4, arg5]:
        if k is not None:
            args.append(k)
        else:
            break
    sync, func, queue, validate, authenticate = WEBHOOKS[enterprise_id][web_action]
    if validate:
        signup_id = request.headers.get('X-I2CE-SIGNUP-API-KEY')
        if not signup_id:
            return auth.request_error('json', "Authorization Required", 403)
        enterprise = Model("enterprise", "core").get(enterprise_id)
        if not enterprise or enterprise.get("signup_api_key") != signup_id:
            return auth.request_error('json', "Unauthorized access", 401)
        if not trust_origin(enterprise_id, enterprise):
            return auth.request_error('json', "Origin of request is not trusted", 412)
    if sync:
        r = func(*args, **params)
    else:
        run_webhook_action.apply_async(
            args = [func] + args,
            kwargs = params,
            queue = queue,
        )
        r = {'info': 'Received and processed data-hook {} for enterprise ID: {}'.format(web_action, enterprise_id)}
    return return_hook(r) 

@i2ce_routes.route('/webhook/dave/<string:enterprise_id>/<string:web_action>', methods = ['GET', 'POST'])
@i2ce_routes.route('/webhook/dave/<string:enterprise_id>/<string:web_action>/<string:arg1>', methods = ['GET', 'POST'])
@i2ce_routes.route('/webhook/dave/<string:enterprise_id>/<string:web_action>/<string:arg1>/<string:arg2>', methods = ['GET', 'POST'])
@i2ce_routes.route('/webhook/dave/<string:enterprise_id>/<string:web_action>/<string:arg1>/<string:arg2>/<string:arg3>', methods = ['GET', 'POST'])
@i2ce_routes.route('/webhook/dave/<string:enterprise_id>/<string:web_action>/<string:arg1>/<string:arg2>/<string:arg3>/<string:arg4>', methods = ['GET', 'POST'])
@i2ce_routes.route('/webhook/dave/<string:enterprise_id>/<string:web_action>/<string:arg1>/<string:arg2>/<string:arg3>/<string:arg4>/<string:arg5>', methods = ['GET', 'POST'])
@validate_headers(allow_html = False)
def webhook(enterprise_id, web_action, arg1 = None, arg2 = None, arg3 = None, arg4 = None, arg5 = None, **kwargs):
    def return_hook(message, response_code = 200, log_level = 'info', args = None, kwargs = None):
        args = hp.make_list(args or [])
        kwargs = kwargs or {}
        logger.log(
            getattr(hp.logging, log_level.upper(), 'INFO'), 
            "Response from web-hook %s for enterprise ID %s: {}".format(
                json.dumps(message, indent = 4)
            ), 
            web_action, 
            enterprise_id, 
            *args, **kwargs
        )
        if isinstance(message, tuple) and isinstance(message[0], Response):
            return message[0], hp.get_from_list(message, 1, response_code), hp.get_from_list(message, 2, {"Access-Control-Allow-Origin": "*"})
        if not isinstance(message, dict):
            return message, response_code, {"Access-Control-Allow-Origin": "*"}
        return jsonify(message), response_code, {"Access-Control-Allow-Origin": "*"}
    webhook_params = request.get_json() or hp.parse_forms_dict(request.values.to_dict(flat = False))
    webhook_params['webhook_recieved_time'] = hp.time();
    webhook_params['webhook_action'] = web_action
    webhook_params['enterprise_id'] = enterprise_id
    if web_action not in WEBHOOKS.get(enterprise_id, {}) and web_action not in WEBHOOKS.get('core', {}):
        return return_hook({'error': "No webhook {} defined for enterprise {}".format(enterprise_id, web_action)}, 400, 'error')
    logger.info("Received web-hook %s for enterprise ID %s: %s", web_action, enterprise_id, json.dumps(webhook_params, indent = 4))
    args = []
    for k in [arg1, arg2, arg3, arg4, arg5]:
        if k is not None:
            args.append(k)
        else:
            break
    sync, func, queue, validate, authenticate = WEBHOOKS.get(enterprise_id, {}).get(web_action) or WEBHOOKS.get('core', {}).get(web_action)
    if validate:
        signup_id = request.headers.get('X-I2CE-SIGNUP-API-KEY')
        if not signup_id:
            return auth.request_error('json', "Authorization Required", 403)
        enterprise = Model("enterprise", "core").get(enterprise_id)
        if not enterprise or enterprise.get("signup_api_key") != signup_id:
            return auth.request_error('json', "Unauthorized access", 401)
        if not trust_origin(enterprise_id, enterprise):
            return auth.request_error('json', "Origin of request is not trusted", 412)
    if authenticate:
        try:
            user_id, enterprise_id, role, api_key, push_token = auth.get_user_details(request, session, webhook_params)
            user = auth.authenticate_user(user_id, enterprise_id, api_key, push_token = push_token)
        except (auth.UnknownUser, auth.Unauthenticated) as e:
            return auth.request_error('json', e, 401)
        if not (validate or trust_origin(enterprise_id)):
            return auth.request_error('json', "Origin of request is not trusted", 412)
    if sync:
        try:
            r = func(*args, **webhook_params)
        except Exception as e:
            hp.print_error(e)
            return return_hook({'error': str(e)}, 400, 'error')
    else:
        run_webhook_action.apply_async(
            args = [func] + args,
            kwargs = webhook_params,
            queue = queue,
        )
        r = {'info': 'Received and processed webhook {} for enterprise ID: {}'.format(web_action, enterprise_id)}
    return return_hook(r) 

@convers.celery.task(name = 'webhook_action')
def run_webhook_action(func, *args, **kwargs):
    return func(*args, **kwargs)

def add_webhook_action(enterprise_id, function_name, sync = False, queue = 'communication', validate = False, authenticate = False):
    function_name = function_name or func.__name__
    def decorator(func):
        func.__name__ = function_name
        if not enterprise_id in WEBHOOKS:
            WEBHOOKS[enterprise_id] = {}
        WEBHOOKS[enterprise_id][function_name] = (sync, func, queue, validate, authenticate)
        # Note we are not binding func, but wrapper which accepts self but does exactly the same as func
        return func # returning func means func can still be used normally
    return decorator

@i2ce_routes.route('/sprinklr/dave/<string:enterprise_id>/<string:conversation_id>/<string:customer_model>/<string:id_attr>', methods = ['POST'])
@i2ce_routes.route('/sprinklr/dave/<string:enterprise_id>/<string:conversation_id>/<string:customer_model>/<string:id_attr>/<string:source_attr>/<string:mobile_number_attr>', methods = ['POST'])
def sprinkr_hook(enterprise_id, conversation_id, customer_model, id_attr, source_attr = 'source', mobile_number_attr = 'mobile_number', *args, **kwargs):
    sprinklr_params = request.get_json() or hp.parse_forms_dict(request.values.to_dict(flat = False))
    logger.info("Sprinklr data received in web-hook: %s", json.dumps(sprinklr_params, indent = 4))
    
    asyn = False
    if sprinklr_params.get('type') == 'message.created':
        sprinklr_params['message_received_time'] = hp.time();
        previous_message_id = sprinklr_params.get('payload', {}).get('messageId')
        try:
            message_type = previous_message_id.split('_')[4]
            if not all(a.isdigit() for a in message_type):
                message_type = previous_message_id.split('_')[5]
        except IndexError:
            message_type = '38'
        if not sprinklr_params.get('payload', {}).get('brandPost') and message_type in ['38', '320', '5', '316']:
            cch = Cacher(enterprise_id, 3600)
            sprinklr_id = sprinklr_params.get('payload', {}).get('senderProfile', {}).get('channelId')
            if sprinklr_id and not cch.get('SprinklrBotID{}'.format(sprinklr_id)):
                asyn = True
            cch.set('SprinklrBotID{}'.format(sprinklr_id), {'hi': 'there'})

    if sprinklr_params.get('type') in ['case.create', 'case.update']:
        sprinklr_params['case_update_time'] = hp.time();

    def return_hook(message, response_code = 200, log_level = 'info', args = None, kwargs = None):
        args = hp.make_list(args or [])
        kwargs = kwargs or {}
        logger.log(getattr(hp.logging, log_level.upper(), 'INFO'), message, *args, **kwargs)
        return message, response_code, {"Access-Control-Allow-Origin": "*"}
    
    err_act = error_reciever_medium(enterprise_id = enterprise_id)
    if asyn:
        asyn_task = [{
            "_name": "post_sprinklr_default_reply",
            "_action": "_function",
            "_function": "post_sprinklr_default_reply",
            "args": [
                sprinklr_params,
                enterprise_id,
                conversation_id,
                customer_model,
                id_attr,
            ],
            "kwargs": {
                "source_attr": source_attr,
                "mobile_number_attr": mobile_number_attr,
            }
        }]
        do_actions({'actions': asyn_task, 'error_action': err_act}, enterprise_id, 'admin', return_act = True)
        sprinklr_params['default_response'] = True
        #return return_hook("Sending default response")
    
    tasks = [{
        "_name": "process sprinklr",
        "_action": "_function",
        "_function": "process_sprinklr_message",
        "args": [
            sprinklr_params,
            enterprise_id,
            conversation_id,
            customer_model,
            id_attr,
        ],
        "kwargs": {
            "source_attr": source_attr,
            "mobile_number_attr": mobile_number_attr
        }
    }]

    do_actions.apply_async(
        args = [{'actions': tasks, 'error_action': err_act}, enterprise_id],
        kwargs = {'return_act': False},
        queue = 'communication',
    )
    #do_actions({'actions': tasks}, enterprise_id, 'admin', return_act = False, raise_error = True)
    return return_hook("Sent to queue for processing")


@i2ce_routes.route('/gmb/dave/<string:enterprise_id>/<string:conversation_id>/<string:customer_model>/<string:display_name>', methods = ['POST'])
@i2ce_routes.route('/google-mybusiness/dave/<string:enterprise_id>/<string:conversation_id>/<string:customer_model>/<string:display_name>', methods = ['POST'])
def gmb_hook(enterprise_id, conversation_id, customer_model, display_name, *args, **kwargs):
    unknown_message = "Hi, I don't know who you are. Sorry about that"
    gmb_params = request.get_json() or hp.parse_forms_dict(request.values.to_dict(flat = False))
    logger.info("Google My Business data received in web-hook: %s", json.dumps(gmb_params, indent = 4))
    if 'clientToken' in gmb_params:
        return gmb_params['secret'], 200, {"Access-Control-Allow-Origin": "*"}
    if 'message' not in gmb_params:
        return unknown_message, 200, {"Access-Control-Allow-Origin": "*"}
    em = Model("enterprise", "core")
    e = em.get(enterprise_id)
    if not e:
        return unknown_message, 200, {"Access-Control-Allow-Origin": "*"}
    cm = Model(customer_model, enterprise_id)
    
    username = gmb_params[u'context'][u'userInfo'][u'displayName']
    
    u = cm.get(gmb_params[u'conversationId'])
    try:
        dd = convers.Conversation(enterprise_id, conversation_id).conversation.get('data', {}).get('default_customer_data', {})
    except convers.UnknownConversation as e:
        return unknown_message, 200, {"Access-Control-Allow-Origin": "*"}
    
    if not u:
        d = {
            cm.ids[0]: gmb_params[u'conversationId'],
            display_name: username,
            "person_type": "visitor"
        }
        d.update(dd)
        try:
            u = do_action({'enterprise_id': enterprise_id}, {
                '_action': 'post',
                '_name': '_via_api_{}@{}'.format(customer_model, datetime.now()),
                '_model': customer_model,
                '_data': d
            }, raise_error = True)
        except Exception as e:
            hp.print_error(e)
            return unknown_message, 200, {"Access-Control-Allow-Origin": "*"}
    scache = JsonBDict("__{}_gmb_cacher".format(conversation_id), [unicode], ['text'], DB_NAME = enterprise_id)
    customer_id = hp.make_single(cm.dict_to_ids(u), force = True)
    if not customer_id:
        logger.error("Did not find customer id in GMB message: %s", gmb_params)
        return unknown_message, 200, {"Access-Control-Allow-Origin": "*"}
    cc = scache.get(customer_id)
    if not cc:
        scache[customer_id] = {
            'channel': "GoogleMyBusinessSender",
            'channel_id': customer_id,
            'channel_credentials': {
                "avatar_name": dd.conversation.get('data', {}).get('gmb_avatar_name') or e.get('social_media_credentials', {}).get(
                    'gmb_avatar_name', "DaveAI"
                ),
                "avatar_image": dd.conversation.get('data', {}).get('gmb_avatar_image') or e.get('social_media_credentials', {}).get(
                    'gmb_avatar_image', "https://www.iamdave.ai/wp-content/uploads/2020/02/cropped-Mask-Group-62.png"
                ),
            }
        }
    txt_msg = gmb_params[u'message'].get(u'text') or ''
    if not txt_msg:
        return unknown_message, 200, {"Access-Control-Allow-Origin": "*"}
    customer_state = None
    try:
        cr = json.loads(txt_msg)
        if 'customer_state' in cr and 'customer_response' in cr:
            customer_state = cr['customer_state']
            txt_msg = cr['customer_response']
    except ValueError as e:
        pass
    try:
        res = do_action({'enterprise_id': enterprise_id, 'role': customer_model, 'user_id': customer_id}, {
                '_action': 'converse',
                '_name': '_via_api_{}@{}'.format(conversation_id, datetime.now()),
                '_conversation_id': conversation_id,
                '_customer_id': customer_id,
                'customer_model': customer_model,
                'customer_response': txt_msg,
                'customer_state': customer_state,
                '_cacher_name': "__{}_gmb_cacher".format(conversation_id)
        }, raise_error = True)
    except convers.UnknownConversation as e:
        return unknown_message, 200, {"Access-Control-Allow-Origin": "*"}
    return res['placeholder'], 200, {"Access-Control-Allow-Origin": "*"}

@i2ce_routes.route('/whatsapp/dave/<string:enterprise_id>/<string:conversation_id>/<string:customer_model>/<string:phone_attr>', methods = ['POST'])
def whatsapp_hook(enterprise_id, conversation_id, customer_model, phone_attr, *args, **kwargs):
    unknown_message = "I don't know you, I'm really sorry"
    whatsapp_params = request.get_json() or hp.parse_forms_dict(request.values.to_dict(flat = False))
    if whatsapp_params.get('payload', {}).get('type') not in  ['text', 'audio', 'opted-in']:
        return "ok"
    em = Model("enterprise", "core")
    e = em.get(enterprise_id)
    if not e:
        return unknown_message, 200, {"Access-Control-Allow-Origin": "*"}
    cm = Model(customer_model, enterprise_id)
    source = whatsapp_params.get('payload', {}).get('source') or whatsapp_params.get('payload', {}).get('phone') 
    if source:
        source = source[2:] if source.startswith("91") else "+{}".format(source)
    if not source:
        return unknown_message, 200, {"Access-Control-Allow-Origin": "*"}
    u = hp.make_single(cm.list(**{phone_attr: source, '_page_size': 1, '_as_option': True}))
    try:
        dd = convers.Conversation(enterprise_id, conversation_id).conversation.get('data', {}).get('default_customer_data', {})
    except convers.UnknownConversation as e:
        return unknown_message, 200, {"Access-Control-Allow-Origin": "*"}
    if not u:
        d = {
            phone_attr: source,
            hp.make_single(cm.names, default = 'name'): whatsapp_params.get('payload', {}).get('sender', {}).get('name'),
        }
        d.update(dd)
        u = do_action({'enterprise_id': enterprise_id}, {
            '_action': 'post',
            '_name': '_via_api_{}@{}'.format(customer_model, datetime.now()),
            '_model': customer_model,
            '_data': d
        })
    scache = JsonBDict("__{}_whatsapp_message_cacher".format(conversation_id), [unicode], ['text'], DB_NAME = enterprise_id)
    customer_id = hp.make_single(cm.dict_to_ids(u), force = True)
    cc = scache.get(customer_id, {})
    try:
        res = convers.next(
            enterprise_id, conversation_id, customer_id, customer_model, customer_id, 
            customer_response = whatsapp_params.get('payload', {}).get('payload', {}).get('text'), 
            **cc
        )
        scache[customer_id] = {
            'system_response': res['name'], 'engagement_id': res['engagement_id'],
            'channel': "GupshupWhatsAppSender",
            'channel_id': source,
            'channel_credentials':  {
                "gupshup_app_name": app_name,
            }
        }
        for k in res.get('data', {}).get('_follow_ups', []):
            logger.info("Sending nudge: %s", k)
            nudge.apply_async(
                args = [
                    enterprise_id, 
                    conversation_id,
                    customer_id,
                    res['engagement_id'],
                    customer_model,
                    k,
                    hp.now(as_datetime = False),
                    source,
                    whatsapp_params.get('app')
                ], countdown = res.get('wait', 20000)/1000.0
            )
    except convers.UnknownConversation as e:
        return unknown_message, 200, {"Access-Control-Allow-Origin": "*"}
    return res['placeholder'], 200, {"Access-Control-Allow-Origin": "*"}

@convers.celery.task(name = 'nudge')
def nudge(enterprise_id, conversation_id, customer_id, engagement_id, customer_model, nudge_state, nudged_time, source, app_name):
    engmt = convers.Engagement(
        enterprise_id, 
        conversation_id = conversation_id, 
        customer_id = customer_id,
        role = customer_model, 
        user_id = customer_id, 
        engagement_id = engagement_id
    )
    last = hp.make_single(list(engmt.get_history(_page_size = 1)), force = True)
    if last:
        last_timestamp = hp.to_datetime(last.get('timestamp'))
        if last_timestamp > hp.to_datetime(nudged_time):
            return None
    scache = JsonBDict("__{}_whatsapp_cacher".format(conversation_id), [unicode], ['text'], DB_NAME = enterprise_id)
    cc = scache.get(customer_id, {})
    try:
        res = convers.next(
            enterprise_id, conversation_id, customer_id, customer_model, customer_id, 
            customer_state = nudge_state,
            **cc
        )
        scache[customer_id] = {'system_response': res['name'], 'engagement_id': res['engagement_id']}
    except convers.UnknownConversation as e:
        hp.print_error(e)
        return None
    return res['placeholder']

@i2ce_routes.route('/pass_through_api/dave', methods = ['POST'])
@authenticate(allow_html = False)
def call_api(*args, **kwargs):
    params = kwargs.get('params')
    enterprise_id = kwargs.get('enterprise_id')
    enterprise = Model("enterprise","core").get(enterprise_id)
    if not enterprise:
        return jsonify({'error': "Unknown Enterprise {}".format(enterprise_id)}), 400, {"Access-Control-Allow-Origin": "*"}
    if '_application' in params:
        params.update(enterprise.get('social_media_credentials', {}).get(params.pop('_application'), {}))
    url = params.pop("_url", None)
    if not url or not isinstance(url, (str, unicode)) or not url.startswith("http"):
        return jsonify({'error': "Unknown URL {}".format(url)}), 400, {"Access-Control-Allow-Origin": "*"}
    method = params.pop("_method", "get") or "get"
    method = str(method).lower()
    headers = params.pop("_headers", {})
    action_list = params.pop("_actions", [])
    delay = params.pop("_delay", True)
    files = params.pop('_files', None)
    verify = params.pop('_verify', True)
    default_response_param = params.pop('_response_key', 'response') or 'response'
    timeout = params.pop('_timeout', 5)
    try:
        if method in ["get", "options", "head"]:
            resp = getattr(requests, method)(url, headers = headers, params = params, verify = verify, timeout = timeout)
        elif method in ["post", "patch", "update", "delete"]:
            if files:
                headers.pop('Content-Type')
            resp = getattr(requests, method)(url, headers = headers, json = params, files = files, verify = verify, timeout = timeout)
        else:
            return jsonify({'error': "Unknown method {}".format(method)}), 400, {"Access-Control-Allow-Origin": "*"}
    except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout) as exc:
        return jsonify({'error': str(exc)}), 504, {"Access-Control-Allow-Origin": "*"}
    if resp.status_code >= 400:
        return jsonify({'error': resp.content}), resp.status_code, {"Access-Control-Allow-Origin": "*"}
    try:
        resp = resp.json()
    except ValueError:
        logger.warning("Response from API: %s %s is not JSON", method.upper(), url)
        resp = {default_response_param: resp.content}
    if action_list:
        actions = {'actions': action_list, 'data': resp}
        if delay:
            do_actions.delay(actions, enterprise_id, 'admin', return_act = False)
        else:
            try:
                r = do_actions(actions, enterprise_id, 'admin', raise_error = True, return_act = False)
            except Exception as exc:
                hp.print_error(e)
                return jsonify({'error': exc}), 400, {"Access-Control-Allow-Origin": "*"}
            else:
                resp.update(r)
    return jsonify(resp)

@i2ce_routes.route('/conversation-feedback/dave/<string:engagement_id>/<string:response_id>', methods = ['POST', 'PATCH'])
@i2ce_routes.route('/conversation-feedback/dave/<string:engagement_id>', methods = ['POST', 'PATCH'])
@authenticate(allow_html = False)
def feedback(engagement_id, response_id = None, *args, **kwargs):
    delayed = False
    params = kwargs.get('params', {})
    actions = []
    if response_id:
        r = {
            'response_rating': params.get('response_rating'),
            'recognition_rating': params.get('recognition_rating')
        }
        r = {k: v for k, v in r.iteritems() if v is not None}
        action = {
           '_action': 'update',
           '_name': '_update_audio_logs@{}'.format(datetime.now()),
           '_model': "audio_logs",
           '_instance': r,
           '_ids': response_id
        }
        actions.append(action)
    if engagement_id:
        r = {
           'usefulness_rating': params.get('usefulness_rating'),
           'accuracy_rating': params.get('accuracy_rating'),
           "feedback": params.get("feedback")
        }
        r = {k: v for k, v in r.iteritems() if v is not None}
        action = {
           '_action': 'update',
           '_name': '_update_engagement@{}'.format(datetime.now()),
           '_model': "engagement",
           '_instance': r,
           '_ids': engagement_id
        }
        actions.append(action)
    #e = Model('enterprise', 'core', 'admin').get(kwargs['enterprise_id'])
    err_act = error_reciever_medium(enterprise_id = kwargs['enterprise_id'])
    actions = {'actions': actions, 'error_action': err_act}
    do_actions.delay(actions, kwargs['enterprise_id'], return_act = False)
    return jsonify(info="Feedback info has been saved")

@i2ce_routes.route('/validators', methods = ['GET'])
@validate_headers(allow_html = False)
def get_validators():
    return jsonify(val.functions_with_help)

@i2ce_routes.route('/customer-signup', methods=["POST"])
@i2ce_routes.route('/customer-signup/<model_name>', methods=["POST", "PATCH", "UPDATE"])
@validate_headers(allow_html = False)
def customer_signup(model_name = None, **kwargs):
    enterprise_id = request.headers.get('X-I2CE-ENTERPRISE-ID')
    signup_id = request.headers.get('X-I2CE-SIGNUP-API-KEY')
    if not enterprise_id or not signup_id:
        return auth.request_error('json', "Authorization Required", 403)
    enterprise = Model("enterprise","core").get(enterprise_id)
    if not enterprise or enterprise.get("signup_api_key") != signup_id:
        return auth.request_error('json', "Unauthorized access", 401)
    agent_info = auth.agent_info()
    actions = {'actions': [], 'data': {'agent_info': agent_info}}
    login_model = base_model.Model('login', enterprise_id)
    admin_model = base_model.Model('admin', 'core')
    try:
        origins = get_origin(enterprise_id = enterprise_id, agent_info = agent_info)
        if model_name:
            customer_model = base_model.Model(model_name, enterprise_id)
        else:
            customer_model = base_model.models(enterprise_id, model_type = 'customer', _as_model = True)[0]
        object_json = request.get_json() or hp.parse_forms_dict(request.values.to_dict(flat = False))
        ids = customer_model.dict_to_ids(object_json)
        require_unique = object_json.pop('_require_unique', False)
        if all(ids):
            r = admin_model.get(ids)
            if r:
                return auth.request_error('json', 'Login with ids {} already exists'.format(','.join(ids)), 400)
            if require_unique:
                r = login_model.get(ids)
                if r:
                    if r['role'] == customer_model.name:
                        return auth.request_error('json', '{} with ids {} already exists'.format(customer_model.title, ','.join(ids)), 400)
                    else:
                        return auth.request_error('json', 'Login with ids {} already exists'.format(','.join(ids)), 400)
        if request.method.lower() == "post":
            actions['actions'].append({
                '_action': 'post',
                '_name': '_via_api_{}@{}'.format(customer_model.name, datetime.now()),
                '_model': customer_model.name,
                '_data': object_json,
            })
        else:
            actions['actions'].append({
                '_action': 'update',
                '_name': '_via_api_{}@{}'.format(customer_model.name, datetime.now()),
                '_model': customer_model.name,
                '_data': object_json,
                '_ids': ids
            })
        rdata = do_actions(actions, enterprise_id, 'admin', raise_error = True, return_act = False)
        rp = rdata.get(actions['actions'][0]['_name'], {})
        rp.pop('password', None)
        session['i2ce_user'] = rp
        if enterprise.get('social_media_credentials', {}).get('_encode_auth', False):
            rp = {'_token': hp.jwt_encode(rp)}
        mr = make_dave_response(jsonify(rp), origins = origins, headers = {'Access-Control-Allow-Credentials': 'true', 'Access-Control-Expose-Headers': 'Set-Cookie'})
        return mr
    except CorsError as ex:
        return auth.request_error('json', ex, 412)
    except Exception as ex:
        return auth.request_error('json', ex, 400)


@i2ce_routes.route('/iupdate/<model_name>/<id1>/<id2>/<id3>/<id4>', methods = ['PATCH'])
@i2ce_routes.route('/iupdate/<model_name>/<id1>/<id2>/<id3>', methods = ['PATCH'])
@i2ce_routes.route('/iupdate/<model_name>/<id1>/<id2>', methods = ['PATCH'])
@i2ce_routes.route('/iupdate/<model_name>/<id1>', methods = ['PATCH', 'POST'])
@i2ce_routes.route('/object/<model_name>/<id1>/<id2>/<id3>/<id4>', methods = ['GET', 'PUT', 'UPDATE', 'DELETE','PATCH'])
@i2ce_routes.route('/object/<model_name>/<id1>/<id2>/<id3>', methods = ['GET', 'PUT', 'UPDATE', 'DELETE','PATCH'])
@i2ce_routes.route('/object/<model_name>/<id1>/<id2>', methods = ['GET', 'PUT', 'UPDATE', 'DELETE','PATCH'])
@i2ce_routes.route('/object/<model_name>/<id1>', methods = ['GET', 'PUT', 'UPDATE', 'DELETE','PATCH'])
@i2ce_routes.route('/get/<model_name>/<id1>', methods = ['GET'])
@i2ce_routes.route('/patch/<model_name>/<id1>', methods = ['POST'])
@i2ce_routes.route('/update/<model_name>/<id1>', methods = ['POST'])
@i2ce_routes.route('/delete/<model_name>/<id1>', methods = ['POST'])
@i2ce_routes.route('/object/<model_name>', methods = ['POST'])
@i2ce_routes.route('/post/<model_name>', methods = ['POST'])
@i2ce_routes.route('/objects/<model_name>', methods = ['GET', 'POST', 'DELETE'])
@i2ce_routes.route('/list/<model_name>', methods = ['GET'])
@i2ce_routes.route('/filter/<model_name>', methods = ['GET'])
@i2ce_routes.route('/update_many/<model_name>', methods = ['POST'])
@i2ce_routes.route('/delete_many/<model_name>', methods = ['POST'])
@i2ce_routes.route('/attributes/<model_name>/<attribute_name>', methods = ['GET'])
@i2ce_routes.route('/attributes/<model_name>', methods = ['GET'])
@i2ce_routes.route('/pivot/<model_name>/<a1>/<a2>/<a3>/<a4>', methods = ['GET'])
@i2ce_routes.route('/pivot/<model_name>/<a1>/<a2>/<a3>', methods = ['GET'])
@i2ce_routes.route('/pivot/<model_name>/<a1>/<a2>', methods = ['GET'])
@i2ce_routes.route('/pivot/<model_name>/<a1>', methods = ['GET'])
@i2ce_routes.route('/pivot/<model_name>', methods = ['GET'])
@i2ce_routes.route('/unique/<model_name>/<a1>', methods = ['GET'])
@i2ce_routes.route('/options/<model_name>/<a1>', methods = ['GET'])
@i2ce_routes.route('/warehouse/<model_name>', methods = ['POST'])
@i2ce_routes.route('/restore/<model_name>', methods = ['POST'])
@i2ce_routes.route('/accumulate/<model_name>', methods = ['POST'])
@i2ce_routes.route('/preference/<model_name>/<a1>/<a2>/<a3>/<a4>', methods = ['POST', 'GET'])
@i2ce_routes.route('/preference/<model_name>/<a1>/<a2>/<a3>', methods = ['POST', 'GET'])
@i2ce_routes.route('/preference/<model_name>/<a1>/<a2>', methods = ['POST', 'GET'])
@i2ce_routes.route('/preference/<model_name>/<a1>', methods = ['POST', 'GET'])
@i2ce_routes.route('/transfer_session/<model_name>/<a1>/<a2>', methods = ['POST'])
@i2ce_routes.route('/interactions/<model_name>/<a1>/<a2>/<a3>/<a4>', methods = ['GET'])
@i2ce_routes.route('/interactions/<model_name>/<a1>/<a2>/<a3>', methods = ['GET'])
@i2ce_routes.route('/interactions/<model_name>/<a1>/<a2>', methods = ['GET'])
@i2ce_routes.route('/interactions/<model_name>/<a1>', methods = ['GET'])
@i2ce_routes.route('/interactions/<model_name>', methods = ['GET'])
@i2ce_routes.route('/next_interaction/<model_name>/<a1>/<a2>', methods = ['GET'])
@i2ce_routes.route('/link/<model_name>/<a1>/<a2>', methods = ['POST', 'DELETE'])
@i2ce_routes.route('/link/<model_name>/<a1>', methods = ['GET'])
@i2ce_routes.route('/link/<model_name>/<a1>/<a2>', methods = ['GET'])
@i2ce_routes.route('/similar_users/dave/<id1>', methods = ['GET'])
@i2ce_routes.route('/similar_customers/dave/<id1>', methods = ['GET'])
@i2ce_routes.route('/similar_products/dave/<id1>', methods = ['GET'])
@i2ce_routes.route('/similar_products/dave', methods = ['POST'])
@i2ce_routes.route('/recommendations/dave/<id1>', methods = ['GET'])
@i2ce_routes.route('/recommendations/dave/<id1>/<id2>', methods = ['GET'])
@i2ce_routes.route('/recommendations/dave/<id1>', methods = ['POST'])
@i2ce_routes.route('/influencers/dave/<id1>', methods = ['GET'])
@i2ce_routes.route('/influencers/dave/<id1>/<id2>', methods = ['GET'])
@i2ce_routes.route('/recommendees/dave/<id1>', methods = ['GET'])
@i2ce_routes.route('/recommendees/dave/<id1>/<id2>', methods = ['GET'])
@i2ce_routes.route('/quantity_predictions/dave/<id1>/<id2>', methods = ['GET'])
@i2ce_routes.route('/people_who_also/dave/<id>/<interaction_stage>', methods = ['GET'])
@i2ce_routes.route('/customer_priority/dave', methods = ['GET'])
@i2ce_routes.route('/customer_snapshot/dave/<id1>/<id2>', methods = ['GET'])
@i2ce_routes.route('/customer_snapshot/dave/<id1>', methods = ['GET'])
@i2ce_routes.route('/translate/dave', methods = ['POST'])
@i2ce_routes.route('/synthesize/dave', methods = ['POST'])
@i2ce_routes.route('/voices/dave', methods = ['GET'])
@i2ce_routes.route('/converse/dave', methods = ['POST'])
@authenticate(allow_html = False)
def get_object(model_name = None, *args, **kwargs):
    object_json = kwargs["params"]
    async = object_json.pop('_async', False)
    action = {
                '_action': to_action(request.method.lower(), kwargs.pop('route', None)),
                '_name': '_via_api_{}@{}'.format(model_name, datetime.now()),
                '_model': model_name,
                '_data': object_json,
             }
    if kwargs.get('ids', None):
       action.update({ '_ids': kwargs.get('ids', None) or None})
    e = Model('enterprise', 'core', 'admin').get(kwargs['enterprise_id'])
    err_act = error_reciever_medium(enterprise_id = kwargs['enterprise_id'])
    
    actions = {'actions': [action], 'error_action': err_act, 'data': {'agent_info': kwargs.get('agent_info', {})}}
    if async:
        do_actions.delay(actions, kwargs['enterprise_id'], kwargs['role'], kwargs['user_id'], return_act = False)
        return jsonify({'info': 'Delayed action {} on model {} executed'.format(action['_action'], action['_model'])})
    rdata = do_actions(actions, kwargs['enterprise_id'], kwargs['role'], user_id = kwargs['user']['user_id'], raise_error = True, return_act = False)
    return Response(json.dumps(rdata.get(action['_name']), primitives = True), mimetype = 'application/json')


def execute_actions(params, enterprise_id, role, user_id = None):
    if not 'actions' in params:
        return auth.request_error('json', 'No actions found', 400)
    if len(params['actions']) > 1002:
        return auth.request_error('json', 'Maximum number of chained actions is 1000', 400)
    if params.get('error_action') and not ('url' in params['error_action'] or 'receiver' in params['error_action']):
        return auth.request_error('json', 'Either url or receiver should be in error_action', 400)
    do_actions.delay(params, enterprise_id, role, user_id, return_act = False)

@i2ce_routes.route('/async/dave', methods = ['POST'])
@authenticate(allow_html = False)
def actions(*args, **kwargs):
    params = kwargs["params"]
    execute_actions(params, kwargs['enterprise_id'], kwargs['role'], kwargs['user']['user_id'])
    return jsonify({"info": "Created actions", "action_group": params})


class SearchFunctions(object):

    def __init__(self, enterprise_id, role):
        self.enterprise_id = enterprise_id
        self.role = role

    def get_models(self):
        return dict((v['name'], v['plural']) for v in base_model.models(self.enterprise_id))


    def get_model_attibutes(self, model):
        mo = Model(model, enterprise_id = self.enterprise_id, role = self.role).get_attributes()
        l = {}
        for a in mo:
            l[a.get("name","")] = a.get("title",a.get("name","").lower()).lower()
        return l

    def get_model_values(self, model, attribute):
        user_id, enterprise_id, role, api_key, push_token = auth.get_user_details(request,session)
        model = Model(model, enterprise_id = self.enterprise_id, role = self.role)
        l = []
        for v in model.get_unique(attribute, params={}).get("data",{}):
            if not v:
                continue
            v = unicode(v, errors = 'ignore')
            l.append(v.lower())
        return l


#get all models
@i2ce_routes.route('/models/core',methods=['GET'])
@i2ce_routes.route('/models/core/<string:attributes>',methods=['GET'])
@authenticate(allow_html = False)
def get_all_models(**kwargs):
    enterprise=kwargs['enterprise_id']
    params = kwargs['params']
    attribute_name=kwargs.get('attributes',None)
    return jsonify(base_model.models(enterprise,attribute_name = attribute_name, role = kwargs['role'], user_id = kwargs['user']['user_id'], **params))

@i2ce_routes.route('/search/dave',methods = ["GET","POST"])
@authenticate
def dave_search(*args,**kwargs):
    search_functions = SearchFunctions(kwargs['enterprise_id'], kwargs['role'])
    def find_match(s,l):
        ms = ""
        k = "__start__"
        ob = {}
        ob["__index__"] = []
        for i in range(len(l)):
            if s.startswith(l[i]['value']):
                ms += l[i]['value']+" "
                k = l[i]['keyword']
                s = s.replace(l[i]["value"],"",1).strip()
                ob["__index__"].append(l[i])
            else:
                break
        return ms,k,ob
    api = {}
    with open(joinpath(dirname(__file__),'data', '{}.json'.format("searchapi")), 'rb') as data_file:
        api = json.load(data_file,encoding="UTF-8")
    responseObj = kwargs.get("params",{})
    statement = responseObj.get("statement",request.args.get("statement",""))
    curr = "__start__"
    disable_previous = responseObj.get("disable_previous",False)
    ms,curr,responseObj = find_match(statement,responseObj.get("__index__",[]))
    
    ts = statement.replace(ms,"",1).strip()
    nav = "__next__"
    ms = ms.strip()+" "
    format = api.get("formats",{})
    def makeurl(obj):
        cmd = key_from_keyword("command")
        model = key_from_keyword("model")
        attribute = key_from_keyword("attribute") or key_from_keyword("custom_attribute")
        clause = key_from_keyword("clause",last= True)
        if not(cmd and model and attribute and clause):
            return ""
        if cmd == "unique":
            return "/"+cmd+"/"+model+"/"+ attribute
        if cmd == "feature":
            start = date.today().replace(day = 1)
            end = (date.today() + timedelta(days=31)).replace(day = 1) -  timedelta(days = 1)
            return "/objects/customer?contract_end_date="+str(start)+","+str(end)+"&interaction_stage=purchased"
        cmd ="/"+cmd+"/"+model+"?"
        params = ""
        for k in obj:
            if k["keyword"] == "attribute":
                param = k["key"]+"="
            elif k["keyword"] == "logical":
                params += "condition_type="+k["value"] +"&"
            elif k["keyword"] == "value":
                param += k["value"]
                params += param +"&"
        if params:
            return cmd+params
        else:
            return ""

    def get_vlist(o):
        if isinstance(o,dict):
            for k,v in o.iteritems():
                for (i,j) in get_vlist(v):
                    yield (k,j)
        elif isinstance(o,list):
            for v in o:
                if isinstance(v,list):
                    if key_from_keyword(v[0]) == v[1]:
                        for (i,j) in get_vlist(get_keys(v[2])):
                            yield (i,j)
                else:
                    yield (v,v)
        else:
            yield (o,o)

    def options(statement):
        # cur,stmt = parse(ts,ms,nav,curr)
        nwl = get_vlist(format.get(curr,{}).get("__next__",[]))
        l = []
        nml = []
        for (k,v) in nwl:
            for (i,j) in get_vlist(get_keys(k)):
                if(disable_previous):
                    l.append(j)
                else:
                    l.append((ms+j).strip())
                nml.append({'keyword':k,'key': i,'value': j})
        return l,nml


    def key_from_keyword(k, first = False, last = False):
        ret_list = []
        for v in responseObj["__index__"]:
            if v["keyword"] == k:
                ret_list.append(v['key'])
        if len(ret_list) == 0:
            if k == "command":
                return "feature"
            return None
        elif len(ret_list) == 1:
            return ret_list[0]
        if first:
            return ret_list[0]
        if last:
            return ret_list[-1]
        return ret_list

    def get_keys(k):
        if isinstance(k,list):
            return k
        elif isinstance(k,dict):
            keywords = k
        else:
            keywords = api["keywords"].get(k)
        if not keywords:
            return []
        if not '__function__' in keywords:
            return keywords
        f = getattr(search_functions, keywords['__function__'])
        if not hasattr(f, '__call__'):
            raise Exception("No local function named: {}".format(keywords['__function__']))
        return f(
            **dict((k, key_from_keyword(k,last = True)) for k in keywords.get('__args__',[]))
        )
    def match_keywords(ts,ms,keywords):
        for (i,j) in keywords:
            if ts.startswith(j):
                return ts.replace(j,"",1).strip(),ms+j+" ",i,j
        return ts,ms,None,""

    def parse(ts,ms="",nav="__next__",curr="__start__",pre="__start__"):
        if not curr or not ts:
            return curr or pre,ms
        for (k,v) in get_vlist(format.get(curr).get(nav,[])):
            ts,ms,z,val = match_keywords(ts,ms,get_vlist(get_keys(k)))
            if z:
                if "__index__" not in responseObj:
                    responseObj["__index__"] = []
                responseObj["__index__"].append({'keyword':k,'key': z,'value': val})
                return parse(ts,ms,"__next__",k,curr)
        return parse(ts,ms,nav,False,curr)

    l,nml = options(statement)
    return jsonify(values = l, __next__ = nml, query = responseObj, __url__ = makeurl(responseObj["__index__"]))

@i2ce_routes.route("/v/<string:channel>/<string:view_id>/<string:communication_id>", methods = ["GET", "POST"])
@i2ce_routes.route("/v/<string:channel>/<string:view_id>", methods = ["GET", "POST"])
@i2ce_routes.route("/v/<string:view_id>", methods = ["GET", "POST"])
@validate_headers()
def get_view(channel = 'w', view_id = None, communication_id = None):
    try:
        params = request.get_json() or {}
        params.update(hp.parse_forms_dict(request.values.to_dict(flat = False)))
        if 'action' in params:
            act = com.View.get_action(view_id, **params)
            execute_actions(*act)
            if 'redirect' in params:
                return redirect(params['redirect'])
            flash("Action {} completed".format(params['action']))
        res = com.View.render_view(view_id, channel = channel, communication_id = communication_id, agent_info = auth.agent_info())
        return res, 200, {
            "X-Frame-Options": "ALLOW-FROM *", 
            'Content-Type': ('application/json' if channel == 'j' else 'text/html; charset=utf-8'),
        }
    except (com.UnknownTemplateError, com.UnknownViewId, com.ViewTemplateError,com.TemplateNotFound, com.UnknownActionError,com.ViewParamError) as e:
        hp.print_error(e)
        return auth.request_error(None, str(e), 404)



@i2ce_routes.route('/available/dave', methods=['GET'])
@validate_headers(allow_html = False)
def get_available():
    """
    To check if User ID is available:

    GET /available/dave?enterprise_id=<enterprise_id>&user_id=<the key to check for availability>&role=<role for which we are checking>&attr=<attribute which we are checking for>
    HEADERS: 
        X-I2CE-SIGNUP-API-KEY: <signup API key>
        X-I2CE-ENTERPRISE-ID: <enterprise ID>
    response:
    {
        "available": true,
    }
    response:
    {
        "available": false,
        "taken_number": 1
    }

    """
    try:
        object_json = request.get_json(silent = True) or json.loads(request.data) if request.data else {}
    except ValueError:
        params = {}
    object_json.update(hp.parse_forms_dict(request.values.to_dict(flat = False)))
    enterprise_id = object_json.pop('enterprise_id', None) or request.cookies.get("enterprise_id") or request.headers.get('X-I2CE-ENTERPRISE-ID')
    signup_id = request.headers.get('X-I2CE-SIGNUP-API-KEY')
    if not enterprise_id or not signup_id:
        return auth.request_error('json', "Authorization Required", 403)
    e = Model('enterprise', 'core', 'admin').get(enterprise_id)
    if not e:
        return auth.request_error('json', "No Enterprise with id {}".format(enterprise_id), 400)
    if not e or e.get("signup_api_key") != signup_id:
        return auth.request_error('json', "Unauthorized access", 401)
    if not trust_origin(enterprise_id, e):
        return auth.request_error('json', "Origin of request is not trusted", 412)
    go = get_origin(e, enterprise_id)
    ui = object_json.pop('user_id', None) or request.cookies.get("user_id")
    if not ui:
        return auth.request_error('json', "No User ID/User Contact Info provided", 400)
    role = object_json.pop('role', None) or request.cookies.get('role')
    if not role:
        return auth.request_error('json', "No User Role provided", 400)
    attr = object_json.pop('attr', None) or request.cookies.get('attr')
    if not attr:
        return auth.request_error('json', "No User Role Attribute provided", 400)
    lm = base_model.Model(role, enterprise_id)
    user_id_attr = lm.ids[0]
    kw = {attr: ui}
    kw.update(object_json)
    u = lm.list(_as_option = True, _page_size = 2, **kw)
    if u:
        return jsonify(available = False, taken_number = len(u)), 200, {"Access-Control-Allow-Origin": "*"}
    return jsonify(available = True), 200, {"Access-Control-Allow-Origin": go}


@i2ce_routes.route('/otp/dave', methods=['GET', 'POST'])
@validate_headers(allow_html = False)
def two_factor_authentication():
    """
    To Send OTP:

    GET /otp/dave?enterprise_id=<enterprise_id>&user_id=<mobile/email/user_id>&otp_length=4&channel=<sms/email/push_notification/captcha>&template=Your OTP for {enterprise_name} is {otp}. - DaveAI&credentials=<credentials_attribute>&provider=<sms provider>
    HEADERS: 
        X-I2CE-SIGNUP-API-KEY: <signup API key>
    response:
    {
        "otp_validate": <validation code>
    }

    To Validate OTP:
    POST /otp/dave
    {
        "otp_validate": <validation code>
        "otp": <otp typed>
        "enterprise_id": "<enterprise id>",
        "user_id": <entered mobile number>,
        "role": <role for which we want to check creds (optional)>,
        "attr": <attribute for which we are validating (optional)>,
        "user_data": <data required for signup after validating>, role and attr need to also be present for signup
    }
    Response:
    if role and attr are provided then we get the authenticated user's credentials:
    if user_data is provided along with role and attr when we can auto-signup user provided data too:
    """
    try:
        object_json = request.get_json(silent = True) or json.loads(request.data) if request.data else {}
    except ValueErrori as e:
        hp.print_error(e)
        object_json = {}
    object_json.update(hp.parse_forms_dict(request.values.to_dict(flat = False)))
    enterprise_id = object_json.pop('enterprise_id', None) or request.cookies.get("enterprise_id") or request.headers.get('X-I2CE-ENTERPRISE-ID')
    signup_id = request.headers.get('X-I2CE-SIGNUP-API-KEY')
    if not enterprise_id or not signup_id:
        return auth.request_error('json', "Authorization Required", 403)
    e = Model('enterprise', 'core', 'admin').get(enterprise_id)
    if not e:
        return auth.request_error('json', "No Enterprise with id {}".format(enterprise_id), 400)
    if not e or e.get("signup_api_key") != signup_id:
        return auth.request_error('json', "Unauthorized access", 401)
    ui = object_json.pop('user_id', None) or request.cookies.get("user_id")
    if not ui:
        return auth.request_error('json', "No User ID/User Contact Info provided", 400)
    c = Cacher(enterprise_id, 600)
    if not trust_origin(enterprise_id, e):
        return auth.request_error('json', "Origin of request is not trusted", 412)
    go = get_origin(e, enterprise_id)
    if request.method == "GET":
        credentials = e.get('social_media_credentials', {}).get(object_json.pop('credentials', None), {})
        channel = object_json.pop('channel', None) or request.cookies.get("channel") or 'sms'
        template = credentials.pop('template', None) or object_json.pop('template', None) or 'Your OTP for {enterprise_name} is {otp}. - DaveAI'
        otp_length = int( hp.to_float( credentials.pop('otp_length', None) or object_json.pop('otp_length', 4) ) )
        if otp_length < 4:
            return auth.request_error('json', "OTP length too short".format(channel), 400)
        provider = object_json.get('provider')
        if not channel in ['sms', 'email', 'push_notification', 'captcha']:
            return auth.request_error('json', "Unknown channel {}".format(channel), 400)
        mmap = {
                'sms': 'phone_numbers',
                'email': 'emails',
                'push_notification': 'push_tokens'
        }
        otp = hp.id_generator(size = otp_length, chars = '0123456789')
        if any(t in app.config.get('SERVER_NAME') for t in ('test.iamdave.ai', 'localhost', '127.0.0.1', '0.0.0.0')):
            otp = hp.id_generator(size = otp_length, chars = '1')
        if any(t in app.config.get('SERVER_NAME') for t in ('staging.iamdave.ai',)) or os.environ.get('ENVIRONMENT','prod') == 'staging':
            otp = hp.id_generator(size = otp_length, chars = '2')
        otp_validate = hp.make_uuid3(otp, enterprise_id, ui, time.time())
        otp_check = hp.make_uuid3(enterprise_id, ui)
        bf = c.get(otp_check)
        if bf and bf['request_time'] > time.time() - 30:
            return auth.request_error('json', "Request too soon after previous one", 400)
        c.set(otp_validate, {'otp': otp, 'user_id': ui, 'enterprise_id': enterprise_id, 'request_time': time.time(), 'attempts': 0})
        c.set(otp_check, {'user_id': ui, 'enterprise_id': enterprise_id, 'request_time': time.time()})
        enterprise_name = object_json.pop('enterprise_name', None) or e['name']
        if channel == 'captcha':
            image = ImageCaptcha(fonts=CAPTCHA_FONTS())
            fname =  hp.make_uuid3(ui, time.time())
            filepath = joinpath(val.STATIC_DIR, enterprise_id, 'captcha', fname) + '.jpg'
            hp.mkdir_p(dirname(filepath))
            url = joinpath(val.CDN_DOMAIN, enterprise_id, 'captcha', fname) + '.jpg'
            image.write(otp, filepath)
            return jsonify({'otp_validate': otp_validate, 'url': url}), 200, {"Access-Control-Allow-Origin": go}
        receiver_ids = {}
        receiver_ids[mmap[channel]] = [ui]
        try:
            message = template.format(enterprise_name = enterprise_name, otp = otp)
        except (KeyError, ValueError) as e:
            return auth.request_error('json', "Incorrect template provided, should have {{enterprise_name}} and {{otp}}", 400)
        action = {
            '_action': 'communicate',
            '_name': 'send_otp',
            "receiver": receiver_ids,
            "channels": [channel],
            "message": message,
            "credentials": credentials,
            "provider": provider
        }
        action.update(object_json)
        try:
            r = do_action({'enterprise_id': enterprise_id, 'role': 'admin'}, action, 'send_otp', False)
        except Exception as e:
            hp.print_error(e)
            return jsonify({'error': str(e)}), 400, {"Access-Control-Allow-Origin": "*"}
        return jsonify({'otp_validate': otp_validate}), 200, {"Access-Control-Allow-Origin": go}
    if request.method == "POST":
        otp_validate = object_json.pop('otp_validate', None)
        if not otp_validate:
            return auth.request_error('json', "OTP Validate is not provided", 400)
        otp = object_json.pop('otp', None)
        if not otp:
            return auth.request_error('json', "OTP is not provided", 400)
        otp_check = hp.make_uuid3(enterprise_id, ui)
        j = c.get(otp_validate)
        if not j:
            return auth.request_error('json', "OTP validity has exprired/OR Unknown Validator {}".format(otp_validate), 404)
        if j.get('attempts', 0) >= 5:
            c.pop(otp_validate)
            c.pop(otp_check)
            return auth.request_error('json', "OTP validity has expired due to too many attempts {}".format(otp_validate), 401)
        if j['user_id'] != ui or j['otp'] != otp or j['enterprise_id'] != enterprise_id:
            j['attempts'] = j.get('attempts', 0) + 1
            c.set(otp_validate, j)
            return auth.request_error('json', "User ID and/or OTP provided are incorrect", 401)
        role = object_json.get('role')
        attr = object_json.get('attr')
        user_data = object_json.get('user_data')
        user_id_attr = 'user_id'
        on_validate = object_json.get('on_validate')
        on_authenticate = object_json.get('on_authenticate')
        on_user_not_found = object_json.get('on_user_not_found')
        on_multiple_users_found = object_json.get('on_multiple_users_found')
        if on_validate:
            do_action.delay({'enterprise_id': enterprise_id, 'role': 'admin'}, on_validate, 'on_validate')
        c.pop(otp_validate)
        c.pop(otp_check)
        if role and attr:
            lm = base_model.Model(role, enterprise_id)
            user_id_attr = lm.ids[0]
            u = lm.list(_as_option = True, _page_size = 2, **{attr: ui})
            if len(u) == 1:
                r = lm.get(lm.dict_to_ids(u[0]))
                r.pop('password', None)
                if on_authenticate:
                    do_action.delay({'enterprise_id': enterprise_id, 'role': 'admin'}, on_authenticate, 'on_authenticate')
                return jsonify(r), 200, {"Access-Control-Allow-Origin": go}
            elif len(u) > 1:
                if on_multiple_users_found:
                    do_action.delay({'enterprise_id': enterprise_id, 'role': 'admin'}, on_multiple_users_found, 'on_multiple_users_found')
                return jsonify({'users': map(lambda x: x[attr], u)}), 200, {"Access-Control-Allow-Origin": go}
            if on_user_not_found:
                do_action.delay({'enterprise_id': enterprise_id, 'role': 'admin'}, on_user_not_found, 'on_user_not_found')
            if user_data:
                user_data['validated'] = True
                actions = {'actions': [{'_name': 'customer-signup', '_data': user_data, '_action': 'post', '_model': role}]}
                try:
                    rdata = do_actions(actions, enterprise_id, 'admin', raise_error = True, return_act = False)
                    rp = rdata.get('customer-signup', {})
                    rp.pop('password', None)
                    return jsonify(rp), 200, {"Access-Control-Allow-Origin": go}
                except Exception as e:
                    hp.print_error(e)
                    return jsonify({'info': 'OTP is validated', 'error': 'Could not sign up: {}'.format(e)}), 400, {"Access-Control-Allow-Origin": "*"}
            return jsonify({user_id_attr: None}), 200, {"Access-Control-Allow-Origin": go}
        return jsonify({'info': 'OTP is validated'}), 200, {"Access-Control-Allow-Origin": go}
    return jsonify({'error': 'Unknown method'}), 405, {"Access-Control-Allow-Origin": "*"}


@i2ce_routes.route('/save_profile/dave', methods=['POST'])
@authenticate(allow_html = False)
def save_profile(**kwargs):
    """
    To save or copy the profile to another i2ce backend:
    POST /save_profile/dave
    {
        "token": <token to provide when retrieving profile"
    }
    HEADERS: (Required if we don't have user session)
        X-I2CE-API-KEY: <API key>
        X-I2CE-ENTERPRISE-ID: <enterprise id>
        X-I2CE-USER-ID: <user id>
    Response:
    {
        "key": <generated key to verify profile>,
        "url": <url to receive the profile>
    }
    """
    try:
        object_json = request.get_json(silent = True) or json.loads(request.data) if request.data else {}
    except ValueError:
        params = {}
    object_json.update(hp.parse_forms_dict(request.values.to_dict(flat = False)))
    enterprise_id = kwargs['enterprise_id']
    user_id = kwargs['user_id']
    user = kwargs['user']
    e = Model('enterprise', 'core', 'admin').get(enterprise_id)
    if not trust_origin(enterprise_id, e):
        return auth.request_error('json', "Origin of request is not trusted", 412)
    params = kwargs['params']
    if ( not params.get('token') ) or (not isinstance(params['token'], (str, unicode))) or len(params['token']) < 18:
        return auth.request_error('json', "No valid token provided", 400)
    c = Cacher(enterprise_id, 600)
    key = hp.make_uuid3(token, enterprise_id, user_id, time.time())
    key_check = hp.make_uuid3(enterprise_id, user_id)
    bf = c.get(key_check)
    if bf and bf.get('request_time', 0) > time.time() - 20:
        return auth.request_error('json', "Request too soon after previous one", 400)
    c.set(key, {'token': params['token'], 'user_id': user_id, 'enterprise_id': enterprise_id, 'request_time': time.time(), 'attempts': 0, 'profile': user})
    c.set(key_check, {'user_id': user_id, 'enterprise_id': enterprise_id, 'request_time': time.time()})
    return jsonify(token = params['token'], enterprise_id = enterprise_id, key = key, url = u'{}://{}/set_profile/dave'.format(os.environ.get('HTTP', 'http'), app.config.get('SERVER_NAME')))

@i2ce_routes.route('/set_profile/dave', methods=['POST'])
@validate_headers(allow_html = False)
def set_profile(**kwargs):
    """
    To save or copy the profile to another i2ce backend:
    POST /set_profile/dave
    {
        "token": <token to provide when retrieving profile",
        "key": <key to get the profile>,
        "url": <url>,
        "enterprise_id": <enterprise_id>
    }
    HEADERS: (Required if we don't have user session)
        X-I2CE-API-KEY: <API key>
        X-I2CE-ENTERPRISE-ID: <enterprise id>
        X-I2CE-USER-ID: <user id>
    Response:
    {
        user_profile data....
    }
    """
    try:
        object_json = request.get_json(silent = True) or json.loads(request.data) if request.data else {}
    except ValueError:
        params = {}
    object_json.update(hp.parse_forms_dict(request.values.to_dict(flat = False)))
    enterprise_id = object_json.pop('enterprise_id', None) or request.cookies.get("enterprise_id") or request.headers.get('X-I2CE-ENTERPRISE-ID')
    signup_id = request.headers.get('X-I2CE-SIGNUP-API-KEY')
    params = kwargs['params']
    if ( not params.get('token') ) or (not isinstance(params['token'], (str, unicode))) or len(params['token']) < 18:
        return auth.request_error('json', "No valid token provided", 400)
    if ( not params.get('key') ) or ( not isinstance(params['key'], (str, unicode)) ):
        return auth.request_error('json', "No valid key provided", 400)
    if not params.get('url'):
        # This is server to server call.. requires SECRET key to be sent.
        if os.environ.get('TRANSFER_SECRET_KEY') and params.get('SECRET_KEY') != os.environ.get('TRANSFER_SECRET_KEY'):
            return auth.request_error('json', "Not authorized to make this request", 401)
        c = Cacher(enterprise_id, 600)
        j = c.get(key)
        if not j:
            return auth.request_error('json', "Invalid/Expired key", 401)
        if j['enterprise_id'] != params.get('enterprise_id'):
            return auth.request_error('json', "Incorrect Enterprise ID", 401)
        if j['token'] != params.get('token'):
            if j.get('attempts', 0) >= 5:
                c.pop(key)
                return auth.request_error('json', "Invalid token, too many attempts", 401)
            j['attempts'] = j.get('attempts', 0) + 1
            c.set(key, j)
            return auth.request_error('json', "Invalid token", 401)
        return jsonify(**j['profile'])
    if not enterprise_id or not signup_id:
        return auth.request_error('json', "Authorization Required", 403)
    e = Model('enterprise', 'core', 'admin').get(enterprise_id)
    if not e:
        return auth.request_error('json', "No Enterprise with id {}".format(enterprise_id), 400)
    if not e or e.get("signup_api_key") != signup_id:
        return auth.request_error('json', "Unauthorized access", 401)
    allowed_apps = hp.make_list(e.get('social_media_credentials', {}).get('profile_transfer_application', [])) 
    if allowed_apps and not trust_origin(enterprise_id, e, origins = allowed_apps):
        return auth.request_error('json', "Origin of request is not trusted", 412)
    try:
        r = requests.post(params['url'], {'token': params['token'], 'key': params['key'], 'SECRET_KEY': os.environ.get('TRANSFER_SECRET_KEY'), 'enterprise_id': params['enterprise_id']})
        if r.status_code != 200:
            return auth.request_error('json', r.text, r.status_code)
        try:
            j = r.json()
        except ValueError as e:
            return auth.request_error('json', 'Remote server URL did not return with valid data', 400)
        if j.get('role'):
            p = Model(j['role'], enterprise_id)
            actions = {'actions': [], 'data': {'agent_info': auth.agent_info()}}
            actions['actions'].append({
                '_action': 'post',
                '_name': '_via_api_{}@{}'.format(j['role'], datetime.now()),
                '_model': j['role'],
                '_data': j,
            })
            rdata = do_actions(actions, enterprise_id, 'admin', raise_error = True, return_act = False)
            rp = rdata.get(actions['actions'][0]['_name'], {})
            rp.pop('password', None)
            session['i2ce_user'] = rp
            return make_dave_response(jsonify(rp), origins = origins)
        return make_dave_response(jsonify(j), origins = origins)
    except Exception as e:
        return auth.request_error('json', str(e), 500)
    return jsonify(key = key, url = u'{}://{}/set_profile/dave'.format(os.environ.get('HTTP', 'http'), app.config.get('SERVER_NAME')))


@i2ce_routes.route('/dave/oauth', methods=['POST'])
@validate_headers(allow_html = False)
def oauth():
    object_json = request.get_json(silent = True) or hp.parse_forms_dict(request.values.to_dict(flat= False))
    object_json['enterprise_id'] = enterprise_id = object_json.get('enterprise_id') or request.cookies.get("enterprise_id")
    object_json['user_id'] = ui = object_json.get('user_id') or request.cookies.get("user_id")
    roles = hp.make_list(object_json.get('roles') or request.cookies.get("role") or ['admin', 'login'])
    if not (object_json.get('user_id') and object_json.get('enterprise_id')):
        return auth.request_error('json', "No User Name and/or Enterprise Id provided", 400)
    enterprise_obj = base_model.Model("enterprise", "core").get(enterprise_id)
    cash = Cacher(enterprise_id, enterprise_obj.get('social_media_credentials', {}).get('_login_wait', 600))
    cash_id = "oauth:{}-{}".format(object_json.get('user_id'), object_json.get('enterprise_id'))
    pc = cash.get(cash_id)
    if pc:
        if pc.get('_attempts') > enterprise_obj.get('social_media_credentials', {}).get('_login_attempts', 2):
            return auth.request_error('json', 'Too many attempts at login. Wait for {} seconds to try again'.format(enterprise_obj.get('social_media_credentials', {}).get('_login_wait', 600)), 401)
        else:
            object_json['_attempts'] = pc['_attempts']
    found_user_id = False
    for r in roles:
        try:
            lm = base_model.Model(r, enterprise_id)
        except base_model.ModelNotFound as err:
            hp.print_error(err)
            continue
        if object_json.get('attrs'):
            attrs = hp.make_list(object_json.get('attrs'))
        elif val.phone_number_validator(ui):
            attrs = lm.mobile_numbers
        elif val.validate_email(ui):
            attrs = lm.emails
        else:
            attrs = lm.ids
        for a in attrs:
            gid = lm.list(_as_option = True, **{a: ui})
            if gid:
                found_user_id = hp.make_single(lm.dict_to_ids(hp.make_single(gid, force = True, ignore_dict = True)), force = True)
                break
        if found_user_id:
            break
    if not found_user_id:
        return auth.request_error('json', "Incorrect username or password for roles {}".format(roles), 401)
    object_json['user_id'] = found_user_id
    try:
        user=auth.authenticate_user(
            object_json.get('user_id'),
            object_json.get('enterprise_id'),
            password=object_json.get('password'), 
            update_login = True, 
            push_token = object_json.get('push_token')
        )
        user.pop('password', None)
    except (auth.Unauthenticated, auth.UnknownUser) as e:
        object_json['_attempts'] = object_json.get('_attempts', 0) + 1
        cash.set(cash_id, object_json)
        return auth.request_error('json', str(e), 401)
    except (auth.NotValidated) as e:
        return auth.request_error('json','Please activate your account using link sent to your mail/phone number', 401)
    cash.pop(cash_id)
    session['i2ce_user'] = user
    if enterprise_obj.get('social_media_credentials', {}).get('_encode_auth', False):
        user = {'_token': hp.jwt_encode(user)}
    mr = make_response(jsonify(user), 200, {"Access-Control-Allow-Origin": "*"})
    return mr

@i2ce_routes.route('/dave/logout', methods=['POST'])
@authenticate(allow_html = False)
def logout(*args, **kwargs):
    try:
        user=auth.authenticate_user(
            kwargs['user_id'],
            kwargs['enterprise_id'],
            api_key=kwargs['user']['api_key'], 
            update_login = True, 
            push_token = kwargs['user'].get('push_token'),
            logout = True
        )
    except (auth.Unauthenticated, auth.UnknownUser) as e:
        return auth.request_error('json', str(e), 401)
    except (auth.NotValidated) as e:
        this_url=url_for("i2ce.send_act_mail",email=object_json.get('user_id'),enterprise_id = object_json.get('enterprise_id'), _external=True)
        return auth.request_error('json','Please activate your account using link sent to your mail/phone number', 401)
    mr = make_response(jsonify({'info': 'logged out successfully'}), 200, {"Access-Control-Allow-Origin": "*"})
    mr.set_cookie('session', '', max_age=0)
    return mr

@i2ce_routes.route('/dave/login', methods=['POST'])
@i2ce_routes.route('/dave/login/<enterprise_id>', methods=['POST'])
@validate_headers(allow_html = False)
def login(enterprise_id = None):
    object_json = request.get_json(silent = True) or hp.parse_forms_dict(request.values.to_dict(flat= False))
    if not enterprise_id: enterprise_id = request.cookies.get("enterprise_id")
    object_json['enterprise_id'] = object_json.get('enterprise_id') or enterprise_id
    enterprise_id = object_json['enterprise_id']
    object_json['user_id'] = object_json.get('user_id') or request.cookies.get("user_id")
    if not (object_json.get('user_id') and object_json.get('enterprise_id')):
        return auth.request_error('json', "No UserId or Enterprise Id given", 400)
    if not ( isinstance(object_json.get('user_id'), (str, unicode)) and  isinstance(object_json.get('enterprise_id'), (str, unicode))):
        return auth.request_error('json', "Either UserId or EnterpriseId is not in str format!", 400)
    enterprise_obj = base_model.Model("enterprise", "core").get(enterprise_id)
    object_json['user_id'] = object_json['user_id'].lower()
    cash = Cacher(enterprise_id, enterprise_obj.get('social_media_credentials', {}).get('_login_wait', 600))
    cash_id = "oauth:{}-{}".format(object_json.get('user_id'), object_json.get('enterprise_id'))
    pc = cash.get(cash_id)
    if pc:
        if pc.get('_attempts') > enterprise_obj.get('social_media_credentials', {}).get('_login_attempts', 2):
            return auth.request_error('json', 'Too many attempts at login. Wait a bit before trying again', 401)
        else:
            object_json['_attempts'] = pc['_attempts']
    try:
        user=auth.authenticate_user(
            object_json.get('user_id'),
            object_json.get('enterprise_id'),
            password=object_json.get('password'), 
            update_login = True, 
            push_token = object_json.get('push_token')
        )
        user.pop('password', None)
    except (auth.Unauthenticated, auth.UnknownUser) as e:
        object_json['_attempts'] = object_json.get('_attempts', 0) + 1
        cash.set(cash_id, object_json)
        return auth.request_error('json', str(e), 401)
    except (auth.NotValidated) as e:
        this_url=url_for("i2ce.send_act_mail",email=object_json.get('user_id'),enterprise_id = object_json.get('enterprise_id'), _external=True)
        return auth.request_error('json','Please activate your account using link sent to your mail/phone number', 401)
    session['i2ce_user'] = user
    cash.pop(cash_id)
    if enterprise_obj.get('social_media_credentials', {}).get('_encode_auth', False):
        user = {'_token': hp.jwt_encode(user)}
    mr = make_response(jsonify(user), 200, {"Access-Control-Allow-Origin": "*"})
    return mr

def act_mail_sender(to_email,subject,sender_email,html2):
    rud = com.mail_sender.MailgunSender()
    singh = rud._toMailgun(sender_email, to_email, subject, html=html2)
    return

@i2ce_routes.route('/send_act_mail/<string:email>/<string:enterprise_id>')
@validate_headers()
def send_act_mail(email,enterprise_id):
    ts=time.time()
    t= datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S.%f')
    unique_id=base64.urlsafe_b64encode('__'.join([email, str(t), enterprise_id]))
    enterprise_obj=base_model.Model("enterprise","core").get(enterprise_id)
    html2=render_template('confirm_mail.html',unique_id=unique_id,enterprise_id=enterprise_id,user_id=email,enterprise_obj=enterprise_obj)    
    send_mail=act_mail_sender(email,"Confiration mail","info@i2ce.in",html2)
    flash('activation mail has been sent to your email id')
    res = make_response(redirect(url_for('login')))
    res.set_cookie("user_id", email)
    res.set_cookie("enterprise_id", enterprise_id)
    return res

@i2ce_routes.route('/error')
def error(message = 'Oops! Something went wrong!', code = 500, **kwargs):
    kwargs.update(hp.parse_forms_dict(request.values.to_dict(flat = False)))
    return render_template("error.html", message = kwargs.pop('message', message), code = kwargs.pop('code', code), **kwargs)

@contextmanager
def read_file(*path):
    with open(joinpath(dirname(__file__), *path), 'rb') as the_file:
        json_data = json.load(the_file, encoding = 'UTF-8')
        yield json_data

@contextmanager
def update_file(*path):
    path = joinpath(dirname(__file__), *path)
    mode = 'r+b' if ispath(path) else 'w+b'
    with open(path, mode) as the_file:
        try:
            json_data = json.load(the_file, encoding = 'UTF-8') if mode == 'r+b' else {}
            yield json_data
        finally:
            the_file.seek(0)
            json.dump(json_data, the_file, indent = 4, encoding = 'UTF-8')
            the_file.truncate()

def error_reciever_medium(enterprise_id = None):
    er = {'receiver': {'emails': ['ananth@i2ce.in']}}
    if not enterprise_id:
        return er
    e = Model('enterprise', 'core', 'admin').get(enterprise_id)
    if not e:
        return er
    err_act = {'receiver': {'emails':  e.get('email')}}
    err_log_dest = e.get('error_log_dest',{})
    if err_log_dest:
       if 'sthree' in err_log_dest:
           err_act = {'sthree': err_log_dest['sthree']}
       elif 'log_file' in err_log_dest:
           err_act=  {'log_file': err_log_dest['log_file']}
       elif 'emails' in err_log_dest:
           err_act = {'receiver': {'emails':  hp.make_list(err_log_dest['emails'])}}
    
    return err_act
