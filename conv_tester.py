#!/usr/bin/python
import os, sys
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile
d = dirname(dirname(abspath(__file__)))
if d not in sys.path:
    sys.path.append(d)
d = joinpath(dirname(dirname(abspath(__file__))), 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
import argparse
import requests
import yaml
import sys
import stuf
import helpers as hp
from helpers import json
import itertools
import unittest
import xmlrunner
import time

logger = hp.get_logger(__name__)

class ClientError(hp.I2CEError):
    pass


class ServerError(hp.I2CEError):
    pass

class UnknownEntity(hp.I2CEError):
    pass

class TestConfigError(hp.I2CEError):
    pass

class Req(object):

    def __init__(self, base_url, headers, verify = True):
        self.url = base_url
        self.headers = headers
        self.enterprise_id = self.headers.get('X-I2CE-ENTERPRISE-ID')
        self.user_id = self.headers.get('X-I2CE-USER-ID')
        self.verify = verify

    def m(self, method, *args, **kwargs):
        while True:
            try:
                r = None
                a = '/'.join([self.url] + list(args))
                logger.info("%s: %s", method, a)
                files = kwargs.pop('_files', None)
                if method == 'get':
                    logger.debug('Url = %s', a)
                    logger.debug('params = %s', kwargs)
                    logger.debug('headers = %s', self.headers)
                    r = getattr(requests, method)(a, headers = self.headers, params = kwargs, verify = self.verify)
                else:
                    r = getattr(requests, method)(a, headers = self.headers, json = kwargs, files = files, verify = self.verify)
                    logger.debug('json = %s', kwargs)
                    logger.debug('files= %s', files)
                if r.status_code in [503, 504]:
                    kwargs['_repeats'] = kwargs.get('_repeats', 0) + 1
                    if kwargs.get('_repeats') > 10:
                        raise ServerError("error in {} {} with status {} - {}".format(method, a, r.status_code, r.content))
                    logger.error("Got %s. Trying again after 10 secs..!!", r.status_code)
                    time.sleep(10)
                    return self.m(method, *args, **kwargs)
                if r.status_code >= 400:
                    logger.error(r.content)
                    raise ClientError("error in {} with status {} - {}".format(method, r.status_code, r.content))
                ret = r.json()
                logger.debug("Response is: %s", json.dumps(ret, indent = 4))
                return ret 
            except ValueError:
                if r:
                    logger.error("Response error: %s", r.content)
                raise
            except requests.exceptions.ConnectionError:
                logger.info("ConnectionError, trying after 10 secs...")
                time.sleep(10)
                kwargs['_repeats'] = kwargs.get('_repeats', 0) + 1
                if kwargs.get('_repeats') > 10:
                    raise ServerError("Server is not Up after 10 attempts")
                return self.m(method, *args, **kwargs)

    def get(self, *args, **kwargs):
        return self.m('get', *args, **kwargs)

    def post(self, *args, **kwargs):
        return self.m('post', *args, **kwargs)

    def patch(self, *args, **kwargs):
        return self.m('patch', *args, **kwargs)

    def delete(self, *args, **kwargs):
        return self.m('delete', *args, **kwargs)

    def put(self, *args, **kwargs):
        return self.m('put', *args, **kwargs)

    def download_and_upload(self, path):
        f = hp.download_file(path)
        r = self.post('upload_file', _files = {'upload_file': f}, large_file = True)
        return r['path']


def create_testsuite(base_url, headers, conversation_id, user = None, user_model = "admin", user_id_attr = "user_id", entities = None, params = None, coverage = None, test_creator = None):
    def create_testcase(names, utterances, system_responses = None, placeholders = None, customer_states = None, check_entities = None):
        class UtteranceTester(unittest.TestCase):

            def is_entity(self, w):
                if w.startswith('__') and w.endswith('__'):
                    return True
                return False

            def replace_current(self, sentence, entities):
                return sentence

            def do_recursive_entity(self, u):
                if isinstance(u, dict):
                    return
                for w in u.split():
                    if self.is_entity(w):
                        if w not in self.entities:
                            raise UnknownEntity("Utterance '{}' has entitity {} without a description".format(u, w))
                        self.run_entities[w] = self.entities[w]
                        for e in self.run_entities[w]:
                            self.do_recursive_entity(e)

            def get_variations(self, utt_split, current_entities = None):
                if not current_entities:
                    current_entities = {}
                if isinstance(utt_split, dict):
                    yield utt_split, current_entities
                    raise StopIteration
                if isinstance(utt_split, (unicode, str)):
                    utt_split = utt_split.split()
                utt_split = ' '.join(utt_split).split()
                any_entity = False
                logger.debug("Split utterance is :%s", utt_split)
                for i, e in enumerate(utt_split):
                    if self.is_entity(e):
                        any_entity = True
                        for w in self.run_entities[e]:
                            current_entities[e] = w
                            us = utt_split[:i] + [w] + utt_split[i+1:]
                            for v, e1 in self.get_variations(us, current_entities):
                                ind = 0
                                if (1-(1.0/self.coverage))**ind < (1.0/self.coverage):
                                    logger.debug("Finished coverage at ind: %s (%s < %s)", ind, (1-(1.0/self.coverage))**ind, 1.0/self.coverage)
                                    break
                                rs = hp.np.random.random_sample()
                                if rs < (1-(1.0/self.coverage))**ind:
                                    yield v, e1
                                ind += 1
                        break
                if not any_entity:
                    yield ' '.join(utt_split), current_entities
                raise StopIteration

            def tearDown(self):
                if self.user_model and not self.test_creator:
                    try:
                        self.base_req.delete("object", self.user_model, self.user_id)
                    except ClientError as e:
                        if not self.params.get('_allow_user_to_remain'):
                            raise

            def setUp(self):
                try:
                    self.base_url = base_url
                    self.headers = headers
                    self.conversation_id = conversation_id
                    self.user_model = user_model
                    self.user_id_attr = user_id_attr
                    self.user = user
                    self.entities = entities or {}
                    self.params = params or {}
                    self.params.update({
                        "csv_response": True,
                        "synthesize_now": True
                    })
                    self.test_creator = test_creator
                    self.coverage = coverage or 200
                    self.utterances = hp.make_list(utterances)
                    self.system_responses = hp.make_list(system_responses)
                    self.placeholders = hp.make_list(placeholders)
                    self.customer_states = hp.make_list(customer_states)
                    self.check_entities = hp.make_list(check_entities)
                    self.names = names
                    if isinstance(self.names, (str, unicode, int, float, long)):
                        self.names = [self.names]*len(self.utterances)
                    for s in ('system_responses', 'placeholders', 'customer_states', 'check_entities'):
                        if len(getattr(self, s)) == 1:
                            setattr(self, s, getattr(self, s)*len(self.utterances))
                        if len(getattr(self, s)) != len(self.utterances):
                            raise ClientError(
                                "The length of {} does not match with length of utterances {}!={}".format(
                                    s, len(getattr(self, s)), len(self.utterances)
                                )
                            )
                    self.base_req = Req(self.base_url, self.headers)
                    for k, v in self.entities.iteritems():
                        if isinstance(v, (str, unicode)):
                            v = v.strip('/')
                            r = self.base_req.get(*v.split('/'))
                            if isinstance(r.get('data'), dict):
                                self.entities[k] = r.get('data', {}).keys()
                            elif isinstance(r, list):
                                self.entities[k] = r
                    if isinstance(self.user, dict) and not self.test_creator:
                        r = self.base_req.post("object", self.user_model, **self.user)
                        self.user_id = r.get(self.user_id_attr)
                        self.user = r;
                    else:
                        self.user_id = self.headers['X-I2CE-USER-ID']
                        self.user = {'api_key': self.headers['X-I2CE-API-KEY']}
                    self.req = Req(self.base_url, {
                            "X-I2CE-ENTERPRISE-ID": self.headers['X-I2CE-ENTERPRISE-ID'],
                            "X-I2CE-USER-ID": self.user_id,
                            "X-I2CE-API-KEY": self.user.get('api_key'),
                            "Content-Type": "application/json"
                        }
                    )
                    if not self.test_creator:
                        r = self.req.post("conversation", self.conversation_id, self.user_id)
                    else:
                        r = {'engagement_id': '123456', 'name': 'sr_init'}
                    self.engagement_id = r.get('engagement_id')
                    self.prev_system_response = r.get("name")
                    self.run_entities = {}
                    for u in self.utterances:
                        if not isinstance(u, (str, unicode, dict)):
                            raise ClientError("Utterance %s should be a string, not a %s. Perhaps you missed quotes?", u, type(u))
                        self.do_recursive_entity(u)
                    logger.debug(u"Run entities are: %s", self.run_entities)
                except:
                    hp.print_error()
                    raise

            def assertTrueIf(self, eq, u):
                if not eq and self.params.get('_continue_on_fail'):
                    self.errors.append(u)
                    return
                return self.assertTrue(eq, u)


            def check_in_unsure(self, req, plc):
                so = req.get('state_options')
                o = req.get('data', {}).get('options')
                def get_itr(i):
                    if isinstance(i, dict):
                        return i.iteritems()
                    if isinstance(i, list):
                        return i
                if not (o or so):
                    logger.error("No state options or options in the response: %s", req)
                    return False
                for k, v in get_itr(so):
                    if plc in v:
                        return True
                for k, v in get_itr(o):
                    if plc in v:
                        return True
                return False
                

            def runTest(self):
                self.errors = []
                for n, u, s, p, c, ce  in itertools.izip(
                        self.names, self.utterances, self.system_responses, self.placeholders, self.customer_states, self.check_entities
                    ):
                    if not isinstance(u, (str, unicode, dict)):
                        raise TestConfigError("Utterances in testing config should be unicode or dict, not type: %s", type(u))
                    if isinstance(u,(str, unicode)):
                        us = u.split()
                    elif isinstance(u, dict):
                        us = u
                    i = 0
                    r = {}
                    if ce and ce != "__null__":
                        ce = hp.make_list(ce)
                    else:
                        ce = []
                    for v, e in self.get_variations(us):
                        if not v:
                            continue
                        logger.info(u"Executing test case %s for variation: %s", n, v)
                        if self.test_creator:
                            print >> self.test_creator, "{},{},{},\"{}\",{},{}".format(n, v, c, json.dumps({k_.strip('_'):v_ for k_, v_ in e.iteritems() if k_ in (ce or [])}), s, p)
                            continue
                        prms = self.params.copy()
                        if isinstance(v, dict):
                            prms['customer_state'] = v.pop("customer_state", None)
                            v = json.dumps(v)
                        try:
                            r = self.req.post('conversation', self.conversation_id, self.user_id, 
                                customer_response = v, system_response = self.prev_system_response,
                                engagement_id = self.engagement_id, 
                                _registered_entities = {k_: e.get(k_) for k_ in (ce or []) if e.get(k_)},
                                **prms
                            )
                        except Exception as err:
                            hp.print_error(err)
                            raise
                        if c and c != "__NULL__":
                            c = hp.make_list(c)
                            logger.debug("Now response: %s", r)
                            self.assertTrueIf(r.get('customer_state') in c, 
                                u"Utterance \"{}\" in test case {} responded with {}, not {}".format(
                                    v, n, r.get('customer_state'), c
                                )
                            )
                        if s and s != "__NULL__":
                            s = hp.make_list(s)
                            self.assertTrueIf(r['name'] in s, 
                                u"Utterance \"{}\" in test case {} responded with {}, not {}".format(
                                    v, n, r['name'], s
                                )
                            )
                        if p and p != "__NULL__":
                            p = hp.make_list(p.split('|') if isinstance(p, (str, unicode)) else p)
                            pr = map(lambda x: self.replace_current(x, e), p)
                            if self.params.get('_allow_unsure'):
                                self.assertTrueIf(any(pr1 in r['placeholder'] or self.check_in_unsure(r, pr1) for pr1 in pr),
                                    u"Utterance \"{}\" in test case {} has placeholder \"{}\", not one of [[ \n\t{} ]] and also not found in state options or options".format(
                                        v, n, r['placeholder'], ',\n\t'.join(pr)
                                    )
                                )
                            else:
                                self.assertTrueIf(any(pr1 in r['placeholder'] for pr1 in pr),
                                    u"Utterance \"{}\" in test case {} has placeholder \"{}\", not one of [[ \n\t{} ]]".format(
                                        v, n, r['placeholder'], ',\n\t'.join(pr)
                                    )
                                )
                        if ce and ce != "__NULL__":
                            ce = hp.make_list(ce)
                            er = r.get('registered_entities', {})
                            for k_ in ce:
                                if not e.get(k_):
                                    raise UnknownEntity("Unknown entity {} in check_entities list: {} among: {}".format(k_, ce, e))
                                self.assertTrueIf(k_ in er,
                                    u"Entity {} not in registered entities {} in test case {}".format(
                                        k_, json.dumps(r['registered_entities'], indent = 2), n
                                    )
                                )
                                self.assertTrueIf(e[k_] == er[k_],
                                    u"Entity value {} does not match with response value {} in registered entity {} within {} for test case {}".format(
                                        e[k_], er[k_], k_, json.dumps(r['registered_entities'], indent = 2), n
                                    )
                                )
                        i += 1
                        if i > self.coverage:
                            logger.info("Breaking because we crossed coverage threshold")
                            break
                        self.prev_system_response = r.get("name") or hp.make_single(s)
                self.assertTrue(not self.errors, ",\n".join(self.errors))
                time.sleep(5)
        return UtteranceTester()
    return create_testcase
   

def run_test(test_data, args = None):
    args = args or stuf.stuf({
        "ENTERPRISE_ID": "maruti_suzuki",
        "USER_ID": "ananth+maruti@i2ce.in",
        "API_KEY": "08e73ae1-f90b-313d-89c3-e18f080a4136",
        "COVERAGE": None,
        "RUNNER": None
    })
    if args.TEST_CREATOR:
        tcfile = open(args.TEST_CREATOR, 'w')
        print >> tcfile, "Name,Utterance,Customer State,Entities,System Response,Placeholder"
    else:
        tcfile = None
    test_case_creator = create_testsuite(
        args.BASE_URL or test_data.get("base_url"), 
        test_data.get("headers") or {
            "X-I2CE-ENTERPRISE-ID": args.ENTERPRISE_ID,
            "X-I2CE-USER-ID": args.USER_ID,
            "X-I2CE-API-KEY": args.API_KEY,
            "Content-Type": "application/json"
        }, 
        args.CONVERSATION_ID or test_data.get("conversation_id"), 
        user = test_data.get("user"), 
        user_model = test_data.get("user_model"), 
        user_id_attr = test_data.get("user_id_attr", "user_id"), 
        entities = test_data.get("entities"), 
        params = test_data.get("params"),
        coverage = args.COVERAGE or test_data.get("coverage") or 200,
        test_creator = tcfile
    )
    done_states = []
    backlog = {}
    def do_add(s, c):
        if any(c1 not in done_states for c1 in c.get("required_states", [])):
            backlog[s] = c
            return
        done_states.append(s)
        tc = test_case_creator(s, c.get("utterance"), c.get("system_response"), c.get("placeholder"), c.get('customer_state'), [c.get("check_entities")])
        if hasattr(args, 'RUNNER') and args.RUNNER == "xml":
            tr = xmlrunner.XMLTestRunner(output=args.REPORT_DIR)
        else:
            tr = unittest.TextTestRunner()
        tr.run(tc)
    for s, c in test_data.get("utterances", {}).iteritems():
        do_add(s, c)
    while backlog:
        backlog1 = hp.copyof(backlog)
        backlog = {}
        for s, c in backlog1.iteritems():
            do_add(s, c)
    for s, c in test_data.get("sequences", {}).iteritems():
        tc = test_case_creator(s, c.get('utterances'), c.get('system_responses'), c.get('placeholders'), c.get('customer_states'), c.get("check_entities"))
        if args.RUNNER == "xml":
            tr = xmlrunner.XMLTestRunner(output='test-reports')
        else:
            tr = unittest.TextTestRunner()
        tr.run(tc)
    if tcfile:
        tcfile.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--BASE_URL', help = "BASE URL to conduct tests", default = "https://marutisuzuki.iamdave.ai")
    parser.add_argument('--USER_ID', help = "USER_ID to conduct tests", default = "ananth+maruti@i2ce.in")
    parser.add_argument('--API_KEY', help = "ADMIN API KEY to conduct tests", default = "08e73ae1-f90b-313d-89c3-e18f080a4136")
    parser.add_argument('--ENTERPRISE_ID', help = "ENTERPRISE ID to conduct tests", default = "maruti_suzuki")
    parser.add_argument('--CONVERSATION_ID', help = "Conversation ID to conduct tests", default = None)
    parser.add_argument('--TEST_DATA', help = "Path to test files")
    parser.add_argument('--COVERAGE', help = "How many variations you want to cover", type=float, default = None)
    parser.add_argument('--RUNNER', help = "How to present the results options are xml and text", default = None)
    parser.add_argument('--REPORT_DIR', help = "Directory to store results", default = 'test-reports')
    parser.add_argument('--TEST_CREATOR', help = "If True, then only the test cases are printed and actual tests are not conducted", default = None)
    args = parser.parse_args()
    for f in args.TEST_DATA.split(","):
        if not f:
            continue
        with open(f, 'r') as fp:
            test_data = yaml.load(fp)
            run_test(test_data, args)
