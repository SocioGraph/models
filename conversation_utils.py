import json
import csv
import helpers as hp
import model
logger = hp.get_logger(__name__)
import re


csv_to_json_dict = { 
            "Type":"",  
            "Customer Query/Response":"customer_state|title",
            "Customer State(Intent)":"customer_state|name",
            "Show In History":"customer_state|show_in_history",
            "Show In State Option":"customer_state|show_in_state_option",
            "Previous Customer State":"customer_state|required_state",
            "Customer Attribute":"eng_var",
            "System Response/Query":"system_response|placeholder",
            "System Response State":"system_response|name",
            "White Board":"system_response|whiteboard",
            "Follow up Suggestions for User":"system_response|state_options",
            "Idle Follow up":"system_response|_follow_ups",
            "Options":"system_response|options",
            "Query Variants":"customer_state|keywords",
            "Positivity Score":"customer_state|positivity_score",
            "Conversation Function Name":"customer_state|to_response_function",
            "Data":"system_response|data",
            "Wait Time":"system_response|wait"
}
def check_sys_repeat(sys_responses, sys_resp_name):
    repeat = False
    existing_names = [d["name"] for d in sys_responses ] 
    if sys_resp_name in existing_names:
        repeat = True
        return repeat
    return False
def check_merge_dict(system_responses,new_dict):
    for old_dict in system_responses:
        if old_dict['name'] == new_dict['name']:            
            system_responses.remove(old_dict)
            for key in new_dict.keys():
                old_dict[key] = new_dict[key]
            return system_responses, old_dict
    return system_responses, new_dict

def merge_entities(csv_file, json_dict):
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    rows=[]
    rows = [row for row in csv_reader]
    print(rows)
    new_ents = {}
    for row in rows:
        new_ents[row[:1][0]] = {"name":row[:1][0], "values":[i for i in row[1:] if i]}
    print(new_ents)        
    #new_ents = {x[0]:{"name":x[0], "values":x[1:]} for x in [[row[i] for row in rows] for i,_ in enumerate(rows[0])]}

    old_ents = {ent["name"]:ent for ent in json_dict.get("entities",[])}


    for k,new_ent in new_ents.items():
        if k in old_ents.keys():
            old_ents[k] = new_ent


    entities = [ents for ents in old_ents.values()]

    print("\n\n\n\n{}\n\n\n\n\n".format(json.dumps(entities)))
    json_dict["entities"] = entities
    
    return json_dict


def merge_csv_to_json(csv_file, json_dict):
    customer_responses = json_dict.get("customer_states",[])
    system_responses = json_dict.get("system_responses",[])
    for line in csv.DictReader(csv_file):
        cust_data = {}
        sys_resp = {}
        if not line["Customer State(Intent)"]:
            continue
        for k in line.keys():
            if k in csv_to_json_dict.keys():
                json_keys = csv_to_json_dict[k].split('|')
                if len(json_keys) > 1:
                    key_type = json_keys[0]
                    sub_key = json_keys[1]
                    if key_type == 'customer_state':
                        if sub_key == 'keywords':
                            if line[k]:
                                cust_data[sub_key] = line[k].split(',')
                            else:
                                cust_data[sub_key] = []
                        elif sub_key == 'positivity_score': 
                            if line[k]:
                                cust_data[sub_key] = line[k]
                            else:
                                cust_data[sub_key] = 0.0
                        elif sub_key == 'to_response_function': 
                            if line[k]:
                                cust_data[sub_key] = {"function":line[k]}
                        else:
                            cust_data[sub_key] = line[k]
                    if key_type == 'system_response':
                        if sub_key == "name":
                            cust_data['follow_up'] = {line[k]:1}
                        if sub_key == 'whiteboard':
                            if line[k]:
                                sys_resp["whiteboard_template"] = "file"
                                sys_resp[sub_key] = line[k]
                        if sub_key == 'data':
                            if line[k].replace(" ","") and len(line[k].replace("\xc2\xa0","")) and len(line[k].replace(" ","")):
                                sys_resp[sub_key] = json.loads(line[k])
                        elif sub_key == 'state_options':
                            if line[k]:
                                line[k] = line[k].replace(" ", "")
                                sys_resp[sub_key] = line[k].split(',')
                            else:
                                sys_resp[sub_key] = []
                        elif sub_key == 'options':
                            if line[k]:
                                sys_resp[sub_key] = line[k].split(',')
                        elif sub_key == 'wait':
                            if line[k]:
                                sys_resp[sub_key] = line[k]
                            else:
                                sys_resp[sub_key] = 10000
                        elif sub_key == '_follow_ups':
                            if line[k]:
                                data = sys_resp.get("data",{})
                                data[sub_key] = line[k].split(',')
                                sys_resp["data"] = data
                        else:
                            sys_resp[sub_key] = line[k]
                    elif key_type == 'system_response':
                        if sub_key == "name":
                            cust_data['follow_up'] = {line[k]:1}

        if cust_data and len(cust_data):
            customer_responses, cust_data = check_merge_dict(customer_responses,cust_data)
            customer_responses.append(cust_data)
        if sys_resp and len(sys_resp):
            system_responses, sys_resp = check_merge_dict(system_responses,sys_resp)
            system_responses.append(sys_resp)
    json_dict ['customer_states'] = customer_responses
    json_dict['system_responses'] = system_responses 
    return json_dict


def check_entities(json_dict, enterprise_id):
    customer_states = json_dict.get("customer_states",[])
    errors = []
    entities_data = json_dict.get("entities",[])
    entities = [x["name"] for x in json_dict.get("entities",[])]
    for ent in entities_data:
        if "model" in ent.keys():  
            try:
                m = model.Model(ent.get("model"),enterprise_id)
                if not ent.get("attribute") in m.attributes:
                    return False,"attribute {} was not found in {} model for {} entity".format(ent.get("attribute"), ent.get("model"),ent.get("name"))
            except Exception as e:
                return False,"model {} is not found for {} entity".format(ent.get("model"),ent.get("name"))
    for customer_state in customer_states:
        keywords = customer_state.get("keywords")
        required_entities = []
        for keyword in keywords:
            result = re.findall(r'\b__\S+__\b',keyword)
            #result = re.findall(re.escape("__")+"(.*)"+re.escape("__"),keyword)
            result = [x[2:-2] for x in result]
            required_entities.extend(result)
        required_entities = list(dict.fromkeys(required_entities))
        missing_entities = filter(lambda ent: ent not in entities, required_entities)
        if len(missing_entities) > 0:
            errors.append({"customer_state" : customer_state["name"],"missing_entities" : missing_entities})

    if len(errors) > 0:
        error_msg = ""
        for err in errors:
            if not error_msg:
                miss_ents = ','.join(err.get("missing_entities"))
                error_msg = "{} entity used in {} not found.".format(miss_ents,err.get("customer_state"))
            else:
                miss_ents = ','.join(err.get("missing_entities"))
                er_now = "{} entity used in {} not found.".format(miss_ents,err.get("customer_state"))
                error_msg = "{},\n {}".format(error_msg,er_now)
        return False, error_msg
    else:
        return True, "Success"

def check_follow_ups_and_state_options(json_dict):
    customer_states = json_dict.get("customer_states",[])
    customer_state_names = [customer_state["name"] for customer_state in customer_states]
    follow_ups = [follow_up_keys for customer_state in customer_states for follow_up_keys in customer_state["follow_up"].keys()]
    system_responses = json_dict.get("system_responses",[])
    system_response_names = [system_response["name"] for system_response in system_responses]
    state_options = [state_option for system_response in system_responses for state_option in system_response.get("state_options")]
    failed_follow_ups = list(set(follow_ups) - set(system_response_names))
    if failed_follow_ups:
        return False, "{} listed as follow_ups were not found in system_responses".format(','.join(failed_follow_ups))
    failed_state_options = list(set(state_options) - set(customer_state_names))
    if failed_state_options:
        return False, "{} listed as state_options were not found in customer_states".format(','.join(failed_state_options))
    return True, "Success"

def get_sys_resp(sys_resps, resp_name):        
    for sys_resp in sys_resps:
        if sys_resp['name'] ==  resp_name:
            return sys_resp

def convert_json_to_csv(json_dict):
    csv_list = []
    for customer_state in json_dict['customer_states']:
        sys_resps = json_dict['system_responses']
        for k in  customer_state['follow_up'].keys():
            csv_dict = {}
            csv_dict["Customer Query/Response"] = customer_state["title"]
            csv_dict["Show In History"] = customer_state.get("show_in_history",False)
            csv_dict["Show In State Option"] = customer_state.get("show_in_state_option",False)
            csv_dict["Customer State(Intent)"] = customer_state["name"]
            csv_dict["Previous Customer State"] = customer_state.get("required_state","")
            csv_dict["Options"] = customer_state.get("options","")
            csv_dict["Query Variants"] = json.dumps(customer_state["keywords"], ensure_ascii=False).replace("[","").replace("]","").replace("\"","")
            sys_resp = get_sys_resp(sys_resps, k)
            if isinstance(sys_resp['placeholder'], str):
                csv_dict['System Response/Query'] = sys_resp['placeholder']
            else:
                csv_dict['System Response/Query'] = list(sys_resp.get('placeholder').keys())[0]
            csv_dict['System Response State'] = sys_resp['name']
            if sys_resp.get('wait',""):
                csv_dict['Wait Time'] = sys_resp['wait']
            else:
                csv_dict['Wait Time'] = 10000
            if sys_resp.get('data',""):
                csv_dict['Data'] = json.dumps(sys_resp['data'], ensure_ascii=False)
            else:
                csv_dict['Data'] = ""
            csv_dict['White Board'] = sys_resp.get('whiteboard',"")
            csv_dict['Follow up Suggestions for User'] = json.dumps(sys_resp['state_options'], ensure_ascii=False).replace("[","").replace("]","").replace("\"","")
            csv_list.append(csv_dict)
    return csv_list