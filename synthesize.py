import os, sys
from functools import wraps
import helpers as hp
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, getmtime as modified_time
from collections import OrderedDict
from stuf import stuf
import validators as val
from pgp_db import JsonBDict
from pgpersist import pgpdict, pgparray, numberlist, stringlist, datetime, dtime, NoneType, date, get_meta_classes, geolocation, timedelta
from celery import Celery
from scipy.signal import lfilter, hamming
import numpy as np
import math
import cv2
from scipy.io import wavfile
import requests
# import did_talk

import model

from google.cloud import texttospeech
from flask import request

CELERY_CONFIG = {  
    'CELERY_BROKER_URL': 'redis://{}/{}'.format(os.environ.get('REDIS_URL', 'localhost:6379'), os.environ.get('REDIS_DB_NUMBER',0)),
    'CELERY_TASK_SERIALIZER': 'pickle',
    'BROKER_USE_SSL': os.environ.get('BROKER_USE_SSL', 'False').lower() == 'true',
}
celery = Celery(__name__, broker=CELERY_CONFIG['CELERY_BROKER_URL'])
celery.conf.update(CELERY_CONFIG)

DEBUG_LEVEL = hp.logging.INFO
logger = hp.get_logger(__name__, DEBUG_LEVEL)

# Instantiates a client
if os.environ.get('GOOGLE_APPLICATION_CREDENTIALS'):
    GCLIENT = texttospeech.TextToSpeechClient()
else:
    GCLIENT = None

SYNTHESIS_PIPELINE_URL = os.environ.get('SYNTHESIS_PIPELINE_URL')
if SYNTHESIS_PIPELINE_URL:
    SYNTHESIZER_API = SYNTHESIS_PIPELINE_URL + '/synthesizer'

    SYNTHESIS_API_KEY = os.environ.get("SYNTHESIS_API_KEY")
    if not SYNTHESIS_API_KEY:
        SYNTHESIS_API_KEY = "08e73ae1-f90b-313d-89c3-e18f080a4136"

    SYNTHESIS_USER_ID = os.environ.get("SYNTHESIS_USER_ID")
    if not SYNTHESIS_USER_ID:
        SYNTHESIS_USER_ID = "ananth+expo@i2ce.in"

    SYNTHESIS_ENTERPRISE_ID = os.environ.get("SYNTHESIS_ENTERPRISE_ID", "dave_expo")
    if not SYNTHESIS_ENTERPRISE_ID:
        SYNTHESIS_ENTERPRISE_ID = "dave_expo"
else:
    SYNTHESIZER_API = ""


LANGUAGE_DICT = {
    "hindi-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='hi-IN',
            name = "hi-IN-Wavenet-B",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 0.85,
            volume_gain_db = 0,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "hindi-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='hi-IN',
            name = "hi-IN-Wavenet-A",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 0,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-IN',
            name = "en-IN-Wavenet-C",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 0,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-slow-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-IN',
            name = "en-IN-Wavenet-C",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 0.85,
            volume_gain_db = 6,
            effects_profile_id = ["large-home-entertainment-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-in-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-IN',
            name = "en-IN-Wavenet-B",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 0.95,
            volume_gain_db = 6,
            effects_profile_id = ["large-home-entertainment-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-slow-in-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-IN',
            name = "en-IN-Wavenet-B",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 0.85,
            volume_gain_db = 6,
            effects_profile_id = ["large-home-entertainment-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-low-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-IN',
            name = "en-IN-Standard-C",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 0.9,
            volume_gain_db = 6,
            effects_profile_id = ["large-home-entertainment-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-low-in-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-IN',
            name = "en-IN-Standard-B",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 0.9,
            volume_gain_db = 6,
            effects_profile_id = ["large-home-entertainment-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-IN',
            name = "en-IN-Wavenet-A",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 0,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-low-in-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-IN',
            name = "en-IN-Standard-A",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 6,
            effects_profile_id = ["large-home-entertainment-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-low-in-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-IN',
            name = "en-IN-Standard-D",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 6,
            effects_profile_id = ["large-home-entertainment-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-in-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-IN',
            name = "en-IN-Wavenet-D",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 6,
            effects_profile_id = ["large-home-entertainment-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-us-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-US',
            name = "en-US-Wavenet-D",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 0,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-low-us-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-US',
            name = "en-US-Standard-J",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 0.95,
            pitch = -4,
            effects_profile_id = ["large-home-entertainment-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-low-us-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-US',
            name = "en-US-Standard-E",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 0.95,
            pitch = -2,
            effects_profile_id = ["large-home-entertainment-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-high-us-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-US',
            name = "en-US-Neural2-F",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            pitch = 0,
            effects_profile_id = ["large-home-entertainment-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-us-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-US',
            name = "en-US-Wavenet-C",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 0,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-uk-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-GB',
            name = "en-GB-Wavenet-B",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 0,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "english-uk-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='en-GB',
            name = "en-GB-Wavenet-F",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 0,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "kannada-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='kn-IN',
            name = "kn-IN-Standard-B",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 12,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "kannada-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='kn-IN',
            name = "kn-IN-Wavenet-A",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 0.9,
            volume_gain_db = 0,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "arabic-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='ar-XA',
            name = "ar-XA-Wavenet-C",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 12,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "arabic-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='ar-XA',
            name = "ar-XA-Wavenet-D",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 0,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "bahasa-ind-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='id-ID',
            name = "id-ID-Wavenet-C",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 12,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "bahasa-ind-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='id-ID',
            name = "id-ID-Wavenet-D",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 0,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "bahasa-mal-male": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='ms-MY',
            name = "ms-MY-Wavenet-D",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 12,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
    "bahasa-mal-female": {
        "voice": texttospeech.types.VoiceSelectionParams(
            language_code='ms-MY',
            name = "ms-MY-Wavenet-C",
            ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE
        ),
        "audio_config": texttospeech.types.AudioConfig(
            speaking_rate = 1,
            volume_gain_db = 0,
            effects_profile_id = ["medium-bluetooth-speaker-class-device"],
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16
        )
    },
}

LANGUAGE_DICT['english'] = LANGUAGE_DICT['english-male']
LANGUAGE_DICT['hindi'] = LANGUAGE_DICT['hindi-male']
LANGUAGE_DICT['kannada'] = LANGUAGE_DICT['kannada-male']

DEFAULT_CHANNELS = ["utterance", "voice", "shapes", "frames"]
ALL_CHANNELS = ['utterance', 'voice', 'mouth', 'phonemes', 'visemes', 'shapes', 'frames']

DEFAULT_KWARGS = {
    'language': "english",
    'ssml': False,
    'channel_sample_rate': 25,
    'icon_image_closed': 'static/img/animation/closed.jpg',
    'icon_image_semi': 'static/img/animation/semi.jpg',
    'icon_image_open':'static/img/animation/open.jpg',
}

class GoogleClientError(hp.I2CEError):
    pass

class SynthesisError(hp.I2CEError):
    pass

####  Faster LPC code #####
from scipy.fftpack import fft, ifft
try:
    from scikits.talkbox.linpred._lpc import levinson as c_levinson
except:
    logger.error("Error importing talkbox, reverting to spectrum module")
    from spectrum.levinson import LEVINSON as c_levinson

def nextpow2(x):
    return int(np.ceil(np.log2(abs(x))))

def lpc(signal, order, axis=-1):
    """Compute the Linear Prediction Coefficients.
    Return the order + 1 LPC coefficients for the signal. c = lpc(x, k) will
    find the k+1 coefficients of a k order linear filter:
      xp[n] = -c[1] * x[n-2] - ... - c[k-1] * x[n-k-1]
    Such as the sum of the squared-error e[i] = xp[i] - x[i] is minimized.
    Parameters
    ----------
    signal: array_like
        input signal
    order : int
        LPC order (the output will have order + 1 items)
    Returns
    -------
    a : array-like
        the solution of the inversion.
    e : array-like
        the prediction error.
    k : array-like
        reflection coefficients.
    Notes
    -----
    This uses Levinson-Durbin recursion for the autocorrelation matrix
    inversion, and fft for the autocorrelation computation.
    For small order, particularly if order << signal size, direct computation
    of the autocorrelation is faster: use levinson and correlate in this case."""
    n = signal.shape[axis]
    if order > n:
        raise ValueError("Input signal must have length >= order")

    r = acorr_lpc(signal, axis)
    return levinson(r, order, axis)

def _acorr_last_axis(x, nfft, maxlag):
    a = np.real(ifft(np.abs(fft(x, n=nfft) ** 2)))
    return a[..., :maxlag+1] / x.shape[-1]



def acorr_lpc(x, axis=-1):
    """Compute autocorrelation of x along the given axis.
    This compute the biased autocorrelation estimator (divided by the size of
    input signal)
    Notes
    -----
        The reason why we do not use acorr directly is for speed issue."""
    if not np.isrealobj(x):
        raise ValueError("Complex input not supported yet")

    maxlag = x.shape[axis]
    nfft = 2 ** nextpow2(2 * maxlag - 1)

    if axis != -1:
        x = np.swapaxes(x, -1, axis)
    a = _acorr_last_axis(x, nfft, maxlag)
    if axis != -1:
        a = np.swapaxes(a, -1, axis)
    return a

def levinson(r, order, axis = -1):
    """Levinson-Durbin recursion, to efficiently solve symmetric linear systems
    with toeplitz structure.
    Parameters
    ----------
    r : array-like
        input array to invert (since the matrix is symmetric Toeplitz, the
        corresponding pxp matrix is defined by p items only). Generally the
        autocorrelation of the signal for linear prediction coefficients
        estimation. The first item must be a non zero real, and corresponds
        to the autocorelation at lag 0 for linear prediction.
    order : int
        order of the recursion. For order p, you will get p+1 coefficients.
    axis : int, optional
        axis over which the algorithm is applied. -1 by default.
    Returns
    -------
    a : array-like
        the solution of the inversion (see notes).
    e : array-like
        the prediction error.
    k : array-like
        reflection coefficients.
    Notes
    -----
    Levinson is a well-known algorithm to solve the Hermitian toeplitz
    equation: ::
                       _          _
        -R[1] = R[0]   R[1]   ... R[p-1]    a[1]
         :      :      :          :         :
         :      :      :          _      *  :
        -R[p] = R[p-1] R[p-2] ... R[0]      a[p]
    with respect to a. Using the special symmetry in the matrix, the inversion
    can be done in O(p^2) instead of O(p^3).
    Only double argument are supported: float and long double are internally
    converted to double, and complex input are not supported at all.
    """
    if axis != -1:
        r = np.swapaxes(r, axis, -1)
    a, e, k = c_levinson(r, order)
    if axis != -1:
        a = np.swapaxes(a, axis, -1)
        e = np.swapaxes(e, axis, -1)
        k = np.swapaxes(k, axis, -1)
    return a, e, k



class Synthesis(object):
    def __init__(self, enterprise_id, conversation_id, text, channels = None, gestures = None, **kwargs):
        """
        Input (required): enterprise_id, conversation_id, text, output_dir
        """
        if not GCLIENT:
            raise GoogleClientError("Google Client not initialized, check if credentials are provided")
        self.enterprise_id = enterprise_id
        self.conversation_id = conversation_id
        self._db = JsonBDict(
            "__{}_conv_cacher".format(conversation_id),
            [unicode],
            ['text'],
            DB_NAME = self.enterprise_id
        )
        d = DEFAULT_KWARGS.copy()
        d.update(kwargs)
        for k, v in d.iteritems():
            setattr(self, k, v)
        if self.language not in LANGUAGE_DICT:
            raise SynthesisError("Unsupported voice: {}".format(self.language))
        self.text = text
        if '|' in self.text:
            self.text = u"<speak>{}</speak>".format(' <break time="1s"/> '.join(self.text.split('|')))
            logger.info(u"SSML text is: %s", self.text)
        if self.text.startswith('<speak>'):
            self.ssml = True
        
        self.channels = DEFAULT_CHANNELS if not isinstance(channels, (list, tuple)) else channels
        self.id = hp.make_uuid3(self.text, self.language, self.channels)
        self.output_dir = joinpath(val.STATIC_DIR, enterprise_id, "conversations", conversation_id, self.id)
        self.output_url = hp.joinurl(val.CDN_DOMAIN, enterprise_id, "conversations", conversation_id, self.id)
        hp.mkdir_p(self.output_dir)
        self.output = self._db.get(self.id, {})
        
        for channel in self.channels:
            setattr(self, "{}_url".format(channel), self.output.get(channel))
            setattr(self, "{}_path".format(channel), self._to_path(self.output.get(channel)))
        self.fourcc = cv2.VideoWriter_fourcc(*'MJPG')
        if 'icon' in self.channels:
            self.images = {
                "open": cv2.imread(self.icon_image_open),
                "semi": cv2.imread(self.icon_image_semi),
                "closed": cv2.imread(self.icon_image_closed),
            }
            if any(i is None for i in self.images.values()):
                logger.error("Some of the icon images are missing, are you on the right path?")
        self.gestures = gestures or []

    def set_channel(self, url, channel = 'voice', suffix = '.wav', _save = True, **kwargs):
        output_path = joinpath(self.output_dir, "{}{}".format(channel, suffix))
        output_url = hp.joinurl(self.output_url, "{}{}".format(channel, suffix))
        hp.download_or_copy(url, output_path, url_path_map = val.URL_MAP, max_chunks = 1048576, force = not kwargs.get('_cached', True))
        self.output[channel] = output_url
        if _save:
            self._db[self.id] = self.output

    def _to_path(self, url):
        if not url: return None
        return url.replace(self.output_url, self.output_dir)

    def _to_stream(self, path):
        if not path: return None
        if path.startswith('http'):
            path = self._to_path(path)
        with open(path, 'rb') as f:
            return f.read()

    def as_csv(self, path):
        if not path: return None
        if path.startswith('http'):
            path = self._to_path(path)
        if ispath(path):
            with open(path, 'rb') as f:
                return f.read()
        return path

    def is_csv(self, path, channel):
        if not channel in ["frames", "shapes"]:
            return False
        if path.startswith('timestamp'):
            return True
        return False

    def mock(self):
        return self.run(_mock = True)

    def run(self, _cached = True, **kwargs):
        """
        kwargs: 
            _mock = False  ... if True, then does not generate the output but only the path
            _cached = True  ... if True and value exists in cache, then it will pick from there
        """
        new = False
        for channel in self.channels:
            if getattr(self, channel, None):
                if _cached and self.output.get(channel) and (ispath(self._to_path(self.output.get(channel))) or self.is_csv(self.output.get(channel), channel)):
                    if ispath(self._to_path(self.output.get(channel))):
                        logger.info("Already found file at %s", self._to_path(self.output.get(channel)))
                    else:
                        logger.info("Already found cached response for %s", channel)
                    setattr(self, "{}_path".format(channel), self._to_path(self.output.get(channel)))
                    setattr(self, "{}_url".format(channel), self.output.get(channel))
                    continue
                new = True
                getattr(self, channel)(_save = False, _cached = _cached, **kwargs)
            else:
                logger.warn("No channel called %s exists", channel)
        if new:
            self._db[self.id] = self.output
        return self.output

    def _exec(self, channel, func, req = None, _save = True, suffix = '.dat', _mock = False, _cached = True, **kwargs):
        if _cached and self.output.get(channel) and (ispath(self._to_path(self.output.get(channel))) or self.is_csv(self.output.get(channel), channel)):
            if ispath(self._to_path(self.output.get(channel))):
                logger.info("Already found file at %s", self._to_path(self.output.get(channel)))
            else:
                logger.info("Already found cached response for %s", channel)
            setattr(self, "{}_path".format(channel), self._to_path(self.output.get(channel)))
            setattr(self, "{}_url".format(channel), self.output.get(channel))
            return self.output.get(channel)
        for r in req or []:
            if not (getattr(self, "{}_path".format(r), None) and ispath(getattr(self, "{}_path".format(r)))):
                getattr(self, r)(_mock = _mock, _cached = _cached, _save = _save, **kwargs)
        output_path = joinpath(self.output_dir, "{}{}".format(channel, suffix))
        output_url = hp.joinurl(self.output_url, "{}{}".format(channel, suffix))
        if channel in self.channels or channel == 'utterance':
            self.output[channel] = output_url
        if not _mock:
            setattr(self, "{}_path".format(channel), output_path)
            setattr(self, "{}_url".format(channel), output_url)
            with open(output_path, 'wb') as out:
                # Write the response to the output file.
                try:
                    for response in func():
                        out.write(response)
                except StopIteration:
                    pass
                except Exception as e:
                    hp.print_error(str(e))
                    raise
                logger.info('%s params/content written to file %s', channel, output_path)
        if _save:
            self._db[self.id] = self.output
        return output_url

    def did_video(self, **kwargs):
        if not self.did_auth or not self.output.get("voice") or not self.did_source_url:
            return None
        
        print("------------------------------ Its reaching", self.did_auth, self.did_source_url, self.output["voice"])
        id = did_talk.generateDIDTalk(self.did_auth, self.did_source_url, self.output["voice"])
        return did_talk.getDIDStatus(id)
        # return None

    def voice(self, **kwargs):
        if kwargs.get('_external_audio'):
            return kwargs['_external_audio']
        def f():
            logger.info("Creating the Audio File")
            if self.ssml:
                synthesis_input = texttospeech.types.SynthesisInput(ssml = self.text)
            else:
                synthesis_input = texttospeech.types.SynthesisInput(text = self.text)
            response = GCLIENT.synthesize_speech(synthesis_input, LANGUAGE_DICT.get(self.language)["voice"], LANGUAGE_DICT.get(self.language)["audio_config"])
            yield response.audio_content
        return self._exec('voice', f, suffix = '.wav', **kwargs)

    def utterance(self, **kwargs):
        def f():
            logger.info("Creating the text File")
            try:
                yield self.text.encode('utf-8')
            except UnicodeDecodeError:
                yield self.text.decode('utf-8').encode('utf-8')
            except UnicodeEncodeError:
                yield self.text
        return self._exec('utterance', f, suffix = '.txt', **kwargs)

    def mouth(self, **kwargs):
        def f():
            logger.info("Creating the Mouth Parameters")
            sr, stream = wavfile.read(self.voice_path)
            if len(stream.shape) > 1:
                stream = stream[:,0]
            window_length = int(0.02*sr)
            w = hamming(window_length)
            hop_length = int(sr*0.01)
            refresh_rate = float(sr)/(self.channel_sample_rate)
            logger.info("Window length: %s, Refresh Rate: %s", window_length, refresh_rate)
            order = int(2 + (sr / 1000.0))
            i = 0
            c = 0
            r = [0]*order
            self.audio_length = len(stream)/float(sr)
            while i*hop_length < len(stream):
                st = hp.time()
                y1 = stream[i*hop_length:(i*hop_length)+window_length]
                y = y1*w[:len(y1)]
                try:
                    l = lfilter([1], [1., 0.63], y)
                    l_ = lpc(l, order)[0]
                    rts = np.roots(l_)
                except (FloatingPointError, np.linalg.LinAlgError, ValueError) as e:
                    r = map(lambda x: max(x[0], x[1]), zip(r, [0]*order))
                else:
                    # Remove imaginary roots.
                    rts = [_ for _ in rts if np.imag(_) >= 0]
                    # Get angles.
                    angz = np.arctan2(np.imag(rts), np.real(rts))
                    # Get frequencies.
                    frqs = filter(lambda x: x>100 and x <= 2000, sorted(angz * (sr / (2 * math.pi))))
                    frqs += [0]*(order - len(frqs))
                    r = map(lambda x: max(x[0], x[1]), zip(r, frqs))
                c += hop_length
                if c >= refresh_rate:
                    logger.debug("At frame: %s", i*hop_length/float(sr))
                    m1 = min(max(((r[0] - 100.0)/1200.0), 0.0), 1.0)
                    if r[1] < 500:
                        m2 = 0
                        m3 = 0
                    elif r[1] < 1500:
                        m2 = min(max(((1500-r[1])/1000.0), 0.0), 1.0)
                        m3 = 0
                    elif r[1] < 3200:
                        m3 = min(max(((r[1]-1500)/2000.0), 0.0), 1.0)
                        m2 = 0
                    else:
                        m2 = 0
                        m3 = 0
                    yield "{:0.4f},{:0.2f},{:0.2f},{:0.2f}\n".format(((i*hop_length)+(window_length/2.0))/float(sr), m1, m2, m3)
                    r = [0]*order
                    c = 0
                else:
                    logger.debug("At frame: %s", i*hop_length/float(sr))
                i += 1
        return self._exec('mouth', f, req = ['voice'], suffix = '.csv', **kwargs)


    def icon(self, **kwargs):
        tfile = hp.tempfile.NamedTemporaryFile(suffix = '.avi')
        tfile.close()
        def f():
            logger.info("Creating the Icon Video")
            out = cv2.VideoWriter( tfile.name, self.fourcc, self.channel_sample_rate, (self.images['closed'].shape[1], self.images['closed'].shape[0]) )
            with open(self.mouth_path, 'rb') as f:
                for line in f:
                    line = line.split(',')[1].strip()
                    if float(line) < 0.1:
                        m = 'closed'
                    elif float(line) < 0.6:
                        m = 'semi'
                    else:
                        m = 'open'
                    out.write(self.images.get(m))
                    yield self.images.get(m)
            out.release()
            cmd = "ffmpeg -i {} -i {} -c:v copy -c:a aac -strict experimental -vcodec libx264 -crf 20 -y {}".format(tfile.name, self.voice_path, self.icon_path)
            os.system(cmd)
            os.remove(tfile.name)
        return self._exec('icon', f, req = ['mouth'], suffix = '.mp4', **kwargs)

    def shapes(self, **kwargs):
        def f():
            weights = get_shapes_params(self.language, self.enterprise_id, self.conversation_id, _force_old=True)
            pairs = {'head_right': 'head_left', 'head_left': 'head_right', 'head_tilt_left': 'head_tilt_right', 'head_tilt_right': 'head_tilt_left'}
            allowed = weights.keys()
            previous = {}
            current = {}
            def func_next(p, duration = 15, scale = 1.0):
                scale = duration / (2.0 * scale)
                return (p/scale) if p <= duration/2 else ((duration - p)/scale)
            with open(self.mouth_path, 'rb') as f:
                yield "timestamp,open,u,smile,{}\n".format(','.join(weights.keys()))
                for line in f:
                    line = map(lambda x: float(x.strip()), line.split(','))
                    line += ([0.0]*len(weights))
                    if allowed:
                        i = hp.histogram_sample([weights[_][0] for _ in allowed], input_array = allowed)
                        ind = weights.keys().index(i)
                        if hp.histogram_sample([0.8, 0.2]):
                            allowed.remove(i)
                            previous[i] = 1
                            current[i] = ind
                            if i in pairs:
                                allowed.remove(pairs[i])
                    for k, v in previous.items():
                        line[current[k]+4] = func_next(v, weights[k][1], weights[k][2])
                        v += 1
                        if v >= weights[k][1]:
                            allowed.append(k)
                            if k in pairs:
                                allowed.append(pairs[k])
                            previous.pop(k)
                            current.pop(k)
                            continue
                        previous[k] = v
                    yield ",".join(map(lambda x: '{:0.4f}'.format(x) if isinstance(x, float) else x, line)) + "\n"
        r = self._exec('shapes', f, req = ['mouth'], suffix = '.csv', **kwargs)
        if kwargs.get('_as_csv') and not kwargs.get('_mock'):
            self.output['shapes'] = self.as_csv(r)
        return r

    def clear_missing_links(self):
        for k, v in self._db.iteritems():
            for c in self.channels:
                v1 = v.get(c)
                if isinstance(v1, (str, unicode)) and v1.startswith('http'):
                    r = requests.head(v1)
                    if r.status_code != 200:
                        logger.error("File for %s - %s is not found. Deleting id %s (%s)", c, v1, k, v.get('utterance',''))
                        self._db.pop(k)


    def frames(self, **kwargs):
        def get_ges():
            try:
                self.gestures.pop(0)
            except IndexError:
                return None
        def f():
            with open(self.mouth_path, 'rb') as f:
                yield "timestamp,animation\n"
                current = get_frames_params(self.language, self.enterprise_id, self.conversation_id, _force_old=True)
                now_counter = None
                animation_start = 0;
                for line in f:
                    line = float(line.split(',')[0])
                    ncurrent = filter(lambda x: not now_counter or now_counter not in x, current.keys())
                    if hp.histogram_sample([0.95, 0.05]) and ncurrent and animation_start <= 0:
                        i = hp.histogram_sample([current[_] for _ in ncurrent], input_array = ncurrent)
                        yield "{:0.4f},{}\n".format(line, i)
                        if 'left' in i:
                            now_counter = 'left'
                        elif 'right' in i:
                            now_counter = 'right'
                        else:
                            now_counter = None
                        current.pop(i)
                        animation_start = self.channel_sample_rate
                    if animation_start > 0:
                        animation_start -= 1
        r = self._exec('frames', f, req = ['mouth'], suffix = '.csv', **kwargs)
        if kwargs.get('_as_csv') and not kwargs.get('_mock'):
            self.output['frames'] = self.as_csv(r)
        return r

def synthesize(enterprise_id, conversation_id, text, _async = True, _cached = True, **kwargs):
    """
    kwargs: 
        sslm (True or False)
        language (e.g. 'english-male')
        channel_sample_rate (e.g. 30 )
        channels (e.g. ['voice', 'icon'])
        icon_image (e.g. path to image )
        3d_model (e.g. path to 3d model)
    """
    if _async:
        r = run(enterprise_id, conversation_id, text, _mock = True, **kwargs)
        kwargs['_cached'] = _cached
        kwargs['_mock'] = False
        run.apply_async(
            args = [enterprise_id, conversation_id, text],
            kwargs = kwargs,
            queue = "synthesize"
        )
        return r
    return run(enterprise_id, conversation_id, text, _mock = False, _cached = _cached, **kwargs)


@celery.task(name = 'synthesize')
def run(enterprise_id, conversation_id, text, _mock = False, _cached = True, **kwargs):
    voice = kwargs.get('language', 'english-male')
    channels = DEFAULT_CHANNELS if not isinstance(kwargs.get('channels'), (list, tuple)) else kwargs.get('channels')
    kwargs['channels'] = channels
    output = do_synthesis(enterprise_id, conversation_id, text, voice, _mock=_mock, _cached=_cached, **kwargs)
    return output


def do_synthesis(enterprise_id, conversation_id, text, voice, _mock = False, _cached = True, **kwargs):
    if SYNTHESIZER_API and kwargs.get("channels", DEFAULT_CHANNELS) and not kwargs.get('_force_old'):
        logger.info("synthesizing using url %s", SYNTHESIZER_API)
        body = kwargs.copy()
        headers = {
           'Content-Type': 'application/json',
           'X-I2CE-API-KEY': SYNTHESIS_API_KEY,
           'X-I2CE-USER-ID': SYNTHESIS_USER_ID,
           'X-I2CE-ENTERPRISE-ID': SYNTHESIS_ENTERPRISE_ID
        }
        body.update({
            "conversation_id": conversation_id,
            "utterance": hp.to_unicode(text),
            "voice_id": voice,
            "action_params": animation_params(voice, enterprise_id, conversation_id, type = 'action_params') or {},
            "gesture_params": animation_params(voice, enterprise_id, conversation_id, type = 'gesture_params') or {},
            "_cached": _cached
        })
        logger.info("Synthesis URL : %s", SYNTHESIZER_API)
        logger.info("Synthesis payload : %s", hp.json.dumps(body, indent= 4))
        try:
            r = requests.post(SYNTHESIZER_API, headers = headers, json = body)
            output = r.json()
            logger.info("received output: %s from synthesis API", output)
        except Exception as e:
            output = {
                "synthesis_error": e
            }
            logger.error('Error in calling external synthesis api :: %s', e)
    else:
        audio_url = kwargs.pop('_audio', None)
        syn = Synthesis(enterprise_id, conversation_id, text, **kwargs)
        if audio_url:
            syn.set_channel(audio_url, _cached = _cached)
        output = syn.run(_mock = _mock, _cached = _cached, _as_csv = kwargs.get('_as_csv', False), _external_audio = audio_url)
    return output

def get_shapes_params(language, enterprise_id, conversation_id, _force_old=True):
    '''
    returns a list of facial shape keys.
    for each shape key we have a tuple of ( probability of occurance, duration in frames, intensity of the shape key)
    '''
    weights = OrderedDict({
                "blink": (2.2, 7, 0.5),
                "browsup":  (3, 41, 1.0),
                "head_down": (0.3, 15, 0.5),
                "head_right": (2, 7, 0.1),
                "head_left": (2, 7, 0.1),
                "look_left": (0.8, 13, 0.1),
                "look_right": (0.8, 13, 0.1),
                "head_tilt_left": (2, 7, 0.1),
                "head_tilt_right": (2, 7, 0.1),
            })
    
    w = animation_params(language, enterprise_id, conversation_id, type = 'action_params') or weights
    if _force_old:
        weights = w.get("params", weights)
    else:
        weights = w
    return weights

def get_frames_params(language, enterprise_id, conversation_id, _force_old=True):
    current = OrderedDict({
                    "weight_shift": 20,
                    "animation1": 50,
                    "animation2": 50,
                    "talk_left_hand_1gesture1": 5,
                    "talk_left_hand_1gesture2": 5,
                    "talk_left_hand_1gesture3": 5,
                    "talk_left_hand_1gesture4": 5,
                    "talk_left_hand_1gesture5": 5,
                    "talk_left_hand_2gesture1": 1,
                    "talk_left_hand_2gesture2": 1,
                    "talk_left_hand_3gesture1": 0.5,
                    "talk_left_hand_3gesture2": 0.5,
                    "talk_right_hand_1gesture1": 5,
                    "talk_right_hand_1gesture2": 5,
                    "talk_right_hand_1gesture3": 5,
                    "talk_right_hand_1gesture4": 5,
                    "talk_right_hand_1gesture5": 5,
                    "talk_right_hand_2gesture1": 1,
                    "talk_right_hand_2gesture2": 1,
                    "talk_right_hand_3gesture1": 0.5,
                    "talk_right_hand_3gesture2": 0.5
                })
    c = animation_params(language, enterprise_id, conversation_id, type = 'gesture_params') or current
    if _force_old:
        if "params" not in c:
            current = c
        else:
            c = c.pop("params")
            current = {}
            for k, v in c.items():
                current[k] = v.get("weight", 1)            
    else:
        current = c
    return current

def animation_params(voice_id, enterprise_id, conversation_id, type = 'gesture_params',  model_name='animation_params'):
    
    try:
        am = model.Model(model_name, enterprise_id)
    except model.ModelNotFound as e:
        return {}
    
    params = am.list(
        voice_id = voice_id,
        conversation_id = conversation_id,
        _page_size = 1,
        _as_option = True,
        _fresh=True
    )
    if not params:
        return {}
    
    if type.lower() in ['gesture_params', 'frames']:
        type = 'gesture_params'
    elif type.lower() in ['action_params', 'shapes']:
        type = 'action_params'

    params = hp.make_single(params).get(type, {})
    if not params:
        return {}
    
    return params
    

if __name__ == '__main__':
    if len(sys.argv) > 1:
        if sys.argv[1] == '--voice':
            if len(sys.argv) < 4:
                raise Exception("Voice input needs text and audio file")
            output = synthesize("test", "test", ' '.join(sys.argv[3:]), _async = False, _cached = False, _audio = sys.argv[2])
        elif sys.argv[1] == '--voice-id':
            if len(sys.argv) < 4:
                raise Exception("Voice id needs text and voice id")
            output = synthesize("test", "test", ' '.join(sys.argv[3:]), _async = False, _cached = False, language = sys.argv[2])
        elif sys.argv[1] == '--text':
            output = synthesize("test", "test", ' '.join(sys.argv[2:]), _async = False, _cached = False)
        else:
            output = synthesize("test", "test", ' '.join(sys.argv[1:]), _async = False, _cached = False)
    else:
        output = synthesize("test", "test", "This is a test", _async = False, _cached = False)
    print(hp.json.dumps(output, indent = 4))
