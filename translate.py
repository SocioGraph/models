
# This Python file uses the following encoding: utf-8

import json
import argparse
import os, sys
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile
import helpers as hp
from collections import OrderedDict
import validators as val
import requests
import six
from celery import Celery
from pgpersist import pgpdict 
from google.cloud import translate_v2 as translate

class GoogleClientError(hp.I2CEError):
    pass

class GoogleServerError(hp.I2CEError):
    pass

if os.environ.get('GOOGLE_APPLICATION_CREDENTIALS'):
    GCLIENT = translate.Client()
else:
    GCLIENT = None

CELERY_CONFIG = {  
    'CELERY_BROKER_URL': 'redis://{}/{}'.format(os.environ.get('REDIS_URL', 'localhost:6379'), os.environ.get('REDIS_DB_NUMBER',0)),
    'CELERY_TASK_SERIALIZER': 'pickle',
    'BROKER_USE_SSL': os.environ.get('BROKER_USE_SSL', 'False').lower() == 'true',
}
celery = Celery(__name__, broker=CELERY_CONFIG['CELERY_BROKER_URL'])
celery.conf.update(CELERY_CONFIG)

class TranslateCacher(pgpdict):
    _allowed_value_types = (unicode,)

@celery.task(name = 'translate')
def translate(target=None,dest_lang= "en",*args,**kwargs):
    """ 
     This will translate the text from one language to other.

    Input Args:
      - target:
         1. it can be a string or list/tuple of string to be converted.
         2. It can be combination of one or more language
    Input Kwargs:
      - dest_lang(destination language):
        1. It can be one language code  or list/tuple of language code

    Example:
        print(translator(["how are you", "hello"], dest_lang= ["mr","hi"]))

    Output:
        [('तू कसा आहेस', 'आप कैसे हैं'), ('नमस्कार', 'नमस्ते')]

    """
    _db = TranslateCacher(
        "__translate_cacher",
        [unicode,unicode],
        ['text',"dest_lang"],
        DB_NAME = "core"
    )

    dest_lang = hp.make_list(dest_lang)
    target = hp.make_list(target)
    
    translate_client = GCLIENT
    if not translate_client:
        raise GoogleClientError("Google Client not initialized, check if credentials are provided")
    
    def get_translation(x,y):       
        o = None
        xe = hp.make_uuid(x)
        o =  _db.get((xe,y))   
        if o:
            return o
        try:
          o = translate_client.translate(x, target_language=y)["translatedText"]
          r = u"{}".format(o)
          _db[(xe,y)] = r 
          return r
        except Exception as e:
            hp.print_error(e)
            raise GoogleServerError(e)

    result  = []
    '''
    tr = []
    for i in target:
        if isinstance(i, six.binary_type):
            try:
                tr.append(i.decode("utf-8"))
            except:
                raise Exception("Can't decode")
    '''
    for i in target:
        t = []
        for j in dest_lang:
            try:
                t.append(get_translation(i,j))
            except Exception as e:
                hp.print_error(e)
        result.append(hp.make_single(t))
    return hp.make_single(result) if result else None

def translate_func(i, from_attr, dest_lang = 'en', default_response = None, **kwargs):
    target = i.get(from_attr)
    try:
        if not target or not isinstance(target, (str, unicode, list)):
            return default_response
    except Exception as e:
        hp.print_error(e)
        return default_response
    return translate(target = target, dest_lang= dest_lang, **kwargs) or default_response
 
val.make_function(
    translate_func,
    'make_translation',
    given_args = ['instance'],
    is_idempotent = False,
    help_string = """
    This will translate the text from one language to other. 
    Input is the from_attr , the attribute containing the source text.
    and dest_lang the destination language_code.
    Optional parameter is default_response
    Response is the translated text
    """
)




if __name__ == "__main__":

    parse = argparse.ArgumentParser()

    parse.add_argument("--fpath", help = "path to conversation file")
    parse.add_argument("--slang", default = "en", help = "source language")
    parse.add_argument("--dlang", default = "en", help = "destination language")

    args = parse.parse_args()

    with open(args.fpath, 'r') as f:
        obj = dict(json.load(f))

        for i in obj.get("entities"):
          if i.get("values"):
              i["values"]= translate(list(i.get("values")), dest_lang=args.dlang)
        for i in obj.get("customer_states"):
            if i.get("keywords"):
              i["keywords"]= translate(list(i.get("keywords")), dest_lang=args.dlang)


    # with open("~/virtual_store_hindi.json", "w", encoding="utf-8") as outfile:

    #     outfile.write(json.dumps(obj, indent=4)) 
    
    

            
      

 
