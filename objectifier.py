import os, sys
import helpers as hp
from base64 import b64encode, b64decode
import dill
from os.path import exists as ispath, dirname, join as joinpath, abspath
import types
TYPENAMES = dict((getattr(types, k).__name__, getattr(types, k)) for k in types.__all__ if isinstance(getattr(types, k), type) )


class ObjectifierError(hp.BaseError):
    pass

ALLOWED_TYPES = (str, unicode, int, long, float)
FPATH = joinpath(dirname(__file__), '.objf', 'func_list')
hp.mkdir_p(joinpath(dirname(__file__), ".objf"))
FUNCTION_LIST = {}
def init():
    if ispath(FPATH):
        with open(FPATH, 'rb') as fp:
            FUNCTION_LIST.update(dill.load(fp))

def objectify(func, name = None, _log = True, **kwargs):
    #if not FUNCTION_LIST: init()
    if not hasattr(func, '__call__'):
        raise ObjectifierError("Cannot encode object {} of type {}".format(func, type(func)), log = _log)
    name = name or func.__name__
    func.__name__ = name
    if isinstance(func, type):
        if kwargs:
            raise ObjectifierError("Cannot set local variables for Classes")
        now_str = func.__name__
    else:
        now_str = '__'.join([name] + ['{}={}'.format(k, v) for k, v in kwargs.iteritems()])
        func.__encoded__ = now_stre = b64encode(now_str)
        func.__decoded__ = now_str
    FUNCTION_LIST[now_stre] = {
        'func': func,
        'locals': kwargs,
        'name': name
    }
    with open(FPATH, 'wb') as fp:
        dill.dump(FUNCTION_LIST, fp)
    return func

def encode(func, _log = True):
    #if not FUNCTION_LIST: init()
    if func is None:
        return None
    if not hasattr(func, '__call__'):
        raise ObjectifierError("Cannot encode object {} of type {}".format(func, type(func)), log = _log)
    if isinstance(func, type):
        now_str = b64encode(func.__name__)
    elif hasattr(func, '__encoded__'):
        now_str = func.__encoded__
    else:
       func = objectify(func)
       now_str = func.__encoded__
    return now_str

    
def decode(now_str, _log = True):
    #if not FUNCTION_LIST: init()
    if not now_str in FUNCTION_LIST:
        try:
            now_strd = b64decode(now_str)
        except TypeError:
            raise ObjectifierError("String {} is not encoded correctly.".format(now_str), log = _log)
        else:
            if now_strd in TYPENAMES:
                return TYPENAMES[now_strd]
        raise ObjectifierError("Name {} (encoded {}) not found in current context".format(now_strd, now_str), log = _log)
    o = FUNCTION_LIST[now_str]
    name = o['name']
    func = o['func']
    if isinstance(func, type):
        return func
    local = o['locals']
    def myfunc(*args, **kwargs):
        for k, v in local.iteritems():
            func.func_globals[k] = v
        return func(*args, **kwargs)
    myfunc.__name__ = o['name']
    return myfunc

