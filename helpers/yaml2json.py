import yaml
import json
import os, sys
from os.path import isdir, join as joinpath
import glob

if len(sys.argv) <= 1:
    sys.exit(0)
yamld = sys.argv[1]
if isdir(yamld):
    files = glob.glob(joinpath(yamld,'*.yaml'))
else:
    files = sys.argv[1:]
    
outfiles = [f.replace('yaml', 'json') for f in files]

for yamlf, jsonf in zip(files, outfiles):
    with open(yamlf, 'rb') as fp:
        y = yaml.load(fp)
    with open(jsonf, 'wb') as fp:
        json.dump(y, fp, indent = 4, encoding = "UTF-8", sort_keys = True)

