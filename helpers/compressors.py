import os, sys
import errno
import string
import random
import numpy as np
from copy import deepcopy as copyof, copy
from datetime import datetime
from datetime import date
from datetime import timedelta
from pytz import timezone
UTC = timezone('UTC')
DTMAX = UTC.localize(datetime.max)
DTMIN = UTC.localize(datetime.min)
dtime = type(datetime.time(datetime.now()))
from collections import OrderedDict

# Remember to allow **kwargs for each of these compressors

trans = string.maketrans('/\\"\'?![]()&%,:*+{};`~$^=|<>','                           ')
def to_ascii_lower(inp, case = None, join = False, prefix = '', suffix = '', translate = None, **kwargs):
    if case is None: case = 'lower'
    if translate is None: 
        translate = trans
    else:
        translate = string.maketrans(*translate)
    if isinstance(inp, (list, tuple)):
        try:
            inp = u' '.join(inp)
        except:
            try:
                inp = unicode(inp)
            except:
                inp = str(inp)
    if isinstance(inp, str):
        inp = inp.decode('ascii', 'ignore').encode('ascii', 'ignore').translate(translate)
    elif isinstance(inp, unicode):
        inp = inp.encode('ascii', 'ignore').translate(translate)
    else:
        inp = str(inp).translate(translate)
    inp = inp.strip()
    if case != "retain":
        r = getattr(inp, case)()
    else:
        r = inp
    if isinstance(join, (str, unicode)):
        r = join.join(r.split())
    return prefix + r + suffix

to_lower_ascii = to_ascii_lower

def join_list(inp, *args, **kwargs):
    if not isinstance(inp, list):
        return to_ascii_lower(inp, *args, **kwargs)
    list_joiner = kwargs.get("join") or ", "
    return list_joiner.join(map(lambda x: to_ascii_lower(x, *args, **kwargs), inp))

def dummy(inp, *args, **kwargs):
    return inp

def shorten(inp, length = None, case = None, join = False, buffer_side = 'left', buffer = ' ', **kwargs):
    if length is None: length = 3
    r = to_ascii_lower(inp, case = case, join = join, **kwargs)
    if buffer_side == 'left':
        r = r[:length]
        if buffer:
            return r.ljust(length, str(buffer))
        return r
    r = r[-length:]
    if buffer:
        return r.rjust(length, str(buffer))
    return r

def abbreviate(inp, length = None, case = None, buffer = ' ', buffer_side = 'left', **kwargs):
    if length is None: length = 3
    s = inp.split()
    i = [_[0] for _ in s]
    r = to_ascii_lower(''.join(i[:length]) + s[-1][1:length - len(i) + 1], case = case, **kwargs)
    return r.ljust(length, str(buffer)) if buffer_side == 'left' else r.rjust(length, str(buffer))

def to_consonants(inp, length = None, case = None, join = '', buffer = ' ', buffer_side = 'left', **kwargs):
    if length is None: length = 3
    inp = to_ascii_lower(inp, case = case, join = join)
    i1 = inp[0]
    i2 = inp[1:]
    vowels = ['e', 'i', 'a', 'o', 'u']
    v = 0
    while len(i1) + len(i2) > length and v < len(vowels):
        i2 = i2.replace(vowels[v],'')
        v += 1
    r = i1 + i2[:max(0, length-1)]
    return r.ljust(length, str(buffer)) if buffer_side == 'left' else r.rjust(length, str(buffer))


