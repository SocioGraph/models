import os, sys
import errno
import string
import random
import numpy as np
from copy import deepcopy as copyof, copy
from datetime import datetime
from datetime import date
from datetime import timedelta
from pytz import timezone
UTC = timezone('UTC')
DTMAX = UTC.localize(datetime.max)
DTMIN = UTC.localize(datetime.min)
dtime = type(datetime.time(datetime.now()))
from collections import OrderedDict

def make_list_agg(inp):
    """
    Function to convert a single object into a list
    """
    if not isinstance(inp, (list, tuple)):
        inp = [inp]
    return list(inp)

def to_number(i, do_raise = False, default = 0):
    if isinstance(i, (int, float, long)):
        return i
    if isinstance(i, (str, unicode)):
        try:
            return float(i)
        except ValueError:
            if do_raise:
                raise TypeError("Unknown format to convert to float: {} ({})".format(i, type(i)))
            return default
    return default

def to_count(i):
    if isinstance(i, (int, float, long)):
        return 1
    if isinstance(i, (str, unicode)):
        try:
            float(i)
        except ValueError:
            return 0
        else:
            return 1
    return 0

def date_list(start_date, end_date, resolution_func, resolution):
    num_days = (end_date - start_date).days
    return_list = [resolution_func(start_date)]
    dt = start_date
    while True:
        bd = dt
        dt = dt + timedelta(seconds = resolution)
        if bd == dt:
            raise ValueError("Resolution {} seconds too small to pivot {} - {}".format(resolution, bd, dt))
        if dt >= end_date:
            break
        nd = resolution_func(dt)
        if nd not in return_list:
            return_list.append(nd)
    return return_list

def gen_total(
        name = 'get_total', 
        key_func = lambda x: x, 
        value_func = lambda x: x, 
        get_func = lambda x: x, 
        init_value = 0, 
        total_func  = lambda x, y: x + y
    ):
    
    total = OrderedDict()
    def init(key):
        key = key_func(key)
        if key not in total:
            total[key] = copyof(init_value) if not hasattr(init_value, '__call__') else init_value()
    def get_total(key, value, **params):
        ok = params.pop('_other_keys', [])
        for k in ok:
            k = key_func(k)
        key = key_func(key)
        value = value_func(value)
        if key not in total:
            total[key] = copyof(init_value) if not hasattr(init_value, '__call__') else init_value()
        total[key] = total_func(total[key], value)
    get_total.__name__ = name
    get_total.get = lambda: get_func(total)
    get_total.init = init
    return get_total

def gen_any():
    return gen_total(
        name = 'get_any',
        total_func = lambda x, y: x or y,
        init_value = False,
        value_func = lambda x: bool(x),
        get_func = lambda x: {k: bool(v) for k, v in x.iteritems()}
    )

def gen_all():
    return gen_total(
        name = 'get_any',
        total_func = lambda x, y: x and y,
        init_value = True,
        value_func = lambda x: bool(x),
        get_func = lambda x: {k: bool(v) for k, v in x.iteritems()}
    )



def gen_unique():
    return gen_total(
        name = 'get_unique',
        total_func = _add,
        init_value = set(),
        get_func = lambda x: {k: len(v) for k, v in x.iteritems()}
    )

def gen_count():
    return gen_total(
        name = 'get_count',
        value_func = lambda x: 1
    )

def gen_sum():
    return gen_total(
        name = 'get_sum',
        value_func = lambda x: to_number(x) 
    )

def gen_average():
    return gen_total(
        name = 'get_average',
        value_func = lambda x: {'sum': to_number(x), 'count': to_count(x)},
        total_func = lambda x, y: {'sum': x['sum'] + y['sum'], 'count': x['count'] + y['count']},
        get_func = lambda x: {k: (v.get('sum', 0.0)/(v.get('count') or 1e-32)) for k, v in x.iteritems()},
        init_value = {'sum': 0, 'count': 0}
    )

def gen_top(top=10):
    return gen_total(
        name = ('get_top' + str(top)) if top >= 0 else ('get_bottom' + str(-top)),
        value_func = lambda x: to_number(x, default = 1),
        get_func = lambda x: dict(sorted(x.iteritems(), key = lambda x: x[1], reverse = (top>=0))[:top])
    )

def iftype(y, atype):
    if issubclass(atype, (int, long, float)) and isinstance(y, (int, long, float)):
        return True
    if isinstance(y, atype):
        return True
    return False

def gen_min(atype):
    if issubclass(atype, datetime):
        init_value = DTMAX
        default_value = None
    elif issubclass(atype, date):
        init_value = date.max
        default_value = None
    elif issubclass(atype, dtime):
        init_value = dtime.max
        default_value = None
    elif issubclass(atype, str):
        init_value = "--------"
        default_value = ""
    elif issubclass(atype, unicode):
        init_value = u"{_}{_}{_}{_}{_}{_}{_}".format(_ = unichr(65518))
        default_value = u""
    else:
        init_value = 1e32
        default_value = None
    return gen_total(
        name = 'get_min',
        total_func = lambda x, y: min(x, y) if iftype(y, atype) else x,
        init_value = init_value,
        get_func = lambda x: {k: v if v < init_value else default_value for k, v in x.iteritems()},
    )
        
def gen_max(atype):
    if issubclass(atype, datetime):
        init_value = DTMIN
        default_value = None
    elif issubclass(atype, date):
        init_value = date.min
        default_value = None
    elif issubclass(atype, dtime):
        init_value = dtime.min
        default_value = None
    elif issubclass(atype, (str, unicode)):
        init_value = ""
        default_value = ""
    else:
        init_value = -1e32
        default_value = None
    return gen_total(
        name = 'get_max',
        total_func = lambda x, y: max(x, y) if iftype(y, atype) else x,
        init_value = init_value,
        get_func = lambda x: {k: v if v > init_value else default_value for k, v in x.iteritems()},
    )

def _extend(x, y):
    x.extend(y)
    return x

def _extset(x, y):
    x.extend(y)
    x = type(x)(set(x))
    return x

def _add(x, y):
    x.add(y)
    return x

def _append_dict(x, y):
    if isinstance(y, dict):
        x.append(y)
    return x

def _append_unique_dict(x, y):
    if isinstance(y, dict):
        if y not in x:
            x.append(y)
    return x


def gen_concat():
    return gen_total(
        name = 'get_concat',
        value_func = lambda x: make_list_agg(x),
        total_func = _extend,
        init_value = list
    )

def gen_set():
    return gen_total(
        name = 'get_set',
        value_func = lambda x: make_list_agg(x),
        total_func = _extset,
        init_value = list
    )

def gen_list():
    return gen_total(
        name = 'get_list',
        init_value = [], 
        total_func  = _append_dict
    )

def gen_unique_list():
    return gen_total(
        name = 'get_unique_list',
        init_value = [], 
        total_func  = _append_unique_dict
    )

if __name__ == '__main__':
    num_funcs = [
        ("count", gen_count()),
        ("min", gen_min(float)),
        ("max", gen_max(float)),
        ("average", gen_average()),
        ("sum", gen_sum())
    ]
    str_funcs = [
        ("unique", gen_unique()),
        ("uniquelist", gen_set()),
        ("concat", gen_concat())
    ]
    ins_funcs = [
        ("object_list", gen_list())
    ]
    top_funcs = [
        ("top3", gen_top(3)),
        ("bottom3", gen_top(-3)),
    ]
    for instance in [
            {
                "a": 10,
                "b": "A",
            },
            {
                "a": 20,
                "b": "B",
            },
            {
                "a": 12,
                "b": "C"
            },
            {
                "a": 245,
                "b": "A",
            },
            {
                "a": 23,
                "b": "D",
            },
            {
                "a": -234,
                "b": "E",
            },
            {
                "a": -20,
                "b": "B"
            }
        ]:
        for n, f in num_funcs:
            f.init("a")
            f("a", instance.get("a"))
        for n, f in str_funcs:
            f.init("b")
            f("b", instance.get("b"))
        for n, f in ins_funcs:
            f.init(None)
            f(None, instance)
        for n, f in top_funcs:
            f.init(instance.get("b"))
            f(instance.get('b'), instance.get("a"))
    for n, f in num_funcs:
        print(n, f.get()['a'])
    for n, f in str_funcs:
        print(n, f.get()['b'])
    for n, f in ins_funcs:
        print(n, f.get()[None])
    for n, f in top_funcs:
        print(n, f.get())
