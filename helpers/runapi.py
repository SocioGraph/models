import requests

from functools import wraps

from __init__ import I2CEError


class UserNotAdded(I2CEError):
    pass


class ProductNotAdded(I2CEError):
    pass


class RunApi(object):
    url = None

    @classmethod
    def via_request(cls, func):
        @wraps(func)
        def return_func(*args, **kwargs):
            if not cls.url:
                return func(*args, **kwargs)  
            if func.__name__ == '__init__':
                link = func.__name__
                enterprise_id = kwargs['enterprise_id'] if len(args) < 2 else args[1]
                _ = kwargs.pop('enterprise_id', None)
                args[0].enterprise_id = enterprise_id
                func_url = '{}/{}/{}'.format(cls.url, link, enterprise_id)
                if args[2:]:
                    func_url += ('/' + '/'.join(str(e) for e in args[1:]))
                r = getattr(requests, 'post')(
                    func_url,
                    json=kwargs,
                    headers={'Content-type': 'application/json'}
                )
                return None
            [method, link] = func.__name__.split('_', 1)
            enterprise_id = args[0].enterprise_id
            func_url = '{}/{}/{}'.format(cls.url, link, enterprise_id)
            if args[1:]:
                func_url += ('/' + '/'.join(str(e) for e in args[1:]))
            r = getattr(requests, method)(
                func_url,
                json=kwargs,
                headers={'Content-type': 'application/json'}
            )
            if r.status_code == 200:
                return r.json()
            elif r.status_code == 404:
                if 'product' in r.text.lower():
                    raise ProductNotAdded(r.text)
                elif 'user' in r.text.lower():
                    raise UserNotAdded(r.text)
                raise Exception(r.text)
            raise Exception(r.text)
        return return_func
