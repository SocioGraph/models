import jwt

GLOBAL_KEY = 'This is some really long key that I need to you )(**)*2137621^$^%#$@'

def jwt_encode(payload, key = None, algorithm = "HS256"):
    key = key or GLOBAL_KEY
    return jwt.encode(payload, key, algorithm="HS256")

def jwt_decode(token, key = None, algorithm = "HS256", **kwargs):
    key = key or GLOBAL_KEY
    return jwt.decode(token, key, algorithm="HS256", **kwargs)
