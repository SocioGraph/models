class ReviewTopicError(BaseError):
    pass

class ReviewFormatError(BaseError):
    pass

class ReviewGen(object):
    
    # _query_topic = ['prompt', 'support', 'quality', 'cost', 'duration', 'recommend', 'satisfaction', 'expertise']
        
    _query_dict = {
            'cost': {
                '1': [
                    'The service was cheaper than I expected.', 
                    "It was worth more than what I paid for.",
                    'The sevice was cheap.',
                    'The service was economical.', 
                ],
                '-1': [
                    'The cost of the service was highly priced.', 
                    'The service was costly.', 
                    'I paid a huge amount for the service.',
                    ' The service was such a rip off.',
                ],
                '0':[
                    'The cost of the service was apt for the work done.', 
                    'The cost of the service moderate.', 
                    'The price was nominal.',
                    
                ],
            },
            'duration': {
                '1': [
                    'The work was completed in a short span of time.', 
                    "The representative was quick enough to resolve the problems.",
                    'I was happy wit hthe duration of the service.',
                    'The service representative was fast to complete the complication .', 
                ],
                '-1': [
                    'The work was completed only after a long time', 
                    'The service representative took a very time to complete the work.', 
                    'It took forever to get the job done.',
                    'The representative took ages to resolve the problem',
                ],
                '0':[
                    'The representative took a resonable amount of time to get the work done.', 
                    'The duration of the service moderate.', 
                    'I am staisfied with the time taken for completing the work.',
                    
                ],
            },
            'support': {
                '1': [
                    'The customer represtative resolved all the problems to my complete satisfaction.',
                    'The service representatives responded in a friendly manner.',
                    ' The representative resolved all the problems.'
                ],
                '-1':[
                    'The customer representative was rude.', 
                    'The customer representative was impolite.',
                    'I felt uncomfortable during the service.'
                ],
                '0':[
                    'The service was satisfactory.', 
                    'The quality of service was mediocre.', 
                    
                    
                ],           
            },
            'prompt': {
                '1': [
                    'The customer rep came responded very quickly.', 
                    'The rep was fast to come by.', 
                    'I was happy the the promptness.'
                ],
                '-1': [ 
                    'The customer rep took a very long time to come to the place.', 
                    'Not happy with the promptness', 
                    'Came very late.'
                ],
                '0': [
                    'Took a moderate time to come by.', 
                    ' The response time was decent.', 
                    ' Took a considerate amount of time.'
                ]

            }
        }
        _query_questions = {
            'cost': [
                    'What do you think about the cost?',
                    'How expensive do you feel about the service/product?',
                    'Was the price apt?',
            ],
            'prompt': [
                    'How prompt was the serive?',
                    'Were they quick enough to attend your need?',
                    'Did they they keep up their timing?',
                    'Comment on the promptness.',
            ],
            'support': [
                    'How was the support?'
                    'Do you feel the support was appropriate?',
                    'Did you feel the support matched your requirements?',


            ],
            'duration': [
                    'How long did the service take to be completed?'
                    'Were the quick to complete the work?',
                    'How reasonable was the time taken to complete the work?',
            ]
        }
    
    def __init__(self, listoftopics):
        """
        list of topics should be a list among the self._query_dict
        """
        if not isinstance(listoftopics, list):
            raise ReviewFormatError("List of topics is a list from among {}".self._query_dict.keys())
        for element in listoftopics:
            if element not in self._query_dict:
                raise ReviewTopicError('{} not present in topics : {}'.format(keyword, self._query_dict))
        self.listoftopics = listoftopics
                
    def make_review(self):
        """
        Ouput is of format:
        [
            {'topic1': [{'rating1': 'utterance1'}, {'rating2': 'utterance2'} ... ]}
            {'topic2': [{'rating1': 'utterance1'}, {'rating2': 'utterance2'} ... ]}
            .
            .
        ]
        """
        return_output = []
        for topic in self.listoftopics:
            topic_dict = self._query_dict[topic]
            rating_list = []
            for key, value in topic_dict.iteritems():
                sentence = random.choice(value)
                rating_list.append({key: sentence})
            return_output.append({topic: rating_list})
        return return_output


    def make_question(self):
        """
        Ouput is of format:
        [
            {'topic1': [question1]}
            {'topic2': [question2]}
            .
            .
        ]
        """
        review_questions = {}
        for topic in self.listoftopics:
            ques_dict = self._query_questions[topic]
            question = random.choice(ques_dict)
            review_questions[topic] = question
        return review_questions

    
    def post_review(self, user_rating):
        """
        user_rating is of type (dict of dict):
        e.f. {
                'topic1': { <rating>: <utterance> }
                'topic2': { <rating>: <utterance>}
             }
        """
        if not isinstance(user_rating, dict):
            raise ReviewFormatError("user rating needs to be a dict of dict not {} ({})".format(type(user_rating), user_rating))
        review_utterance = []
        for topic, rating in user_rating.iteritems():
            if not isinstance(rating, dict):
                raise ReviewFormatError("Each user rating needs to be a dict of len 1 not {} ({})".format(type(rating), rating))
            if len(rating) != 1:
                raise ReviewFormatError("Each user rating needs to be a dict of len 1 not {} ({})".format(len(rating), rating))
            if topic not in self.listoftopics:
                logger.warn("Topic {} is not listed in current context".format(topic))
                continue
            r, u = rating.items()[0]
            r = str(r)
            if not r in self._query_dict[topic]:
                raise ReviewFormatError("Review rating {} for topic {} is not listed".format(r, t))
            if not u in self._query_dict[topic][r]:
                raise ReviewFormatError("Review utterance {} for rating {} in topic {} is not listed".format(u, r, t))
            u = u.strip('. ').capitalize()
            u = u + '.'
            review_utterance.append(u)
        random.shuffle(review_utterance)
        return ' '.join(review_utterance)
