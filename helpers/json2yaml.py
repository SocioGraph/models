import yaml
import json
import os, sys
from os.path import isdir, join as joinpath
import glob

if len(sys.argv) <= 1:
    sys.exit(0)
yamld = sys.argv[1]
if isdir(yamld):
    files = glob.glob(joinpath(yamld,'*.json'))
else:
    files = sys.argv[1:]
    
outfiles = [f.replace('json', 'yaml') for f in files]

for jsonf, yamlf in zip(files, outfiles):
    with open(jsonf, 'rb') as fp:
        y = json.load(fp, encoding = 'UTF-8')
    with open(yamlf, 'wb') as fp:
        yaml.safe_dump(y, fp, indent = 4, default_flow_style = False)

