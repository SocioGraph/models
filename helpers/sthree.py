import boto3
from StringIO import StringIO
import json
import string
import os, sys
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile
import requests
from celery import Celery
from __init__ import logger, id_generator, now, I2CEError, get_logger, mkdir_p
import logging
logger = logging.getLogger(__name__)


AWS_ACCESS_KEY_ID=os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY=os.environ.get('AWS_SECRET_ACCESS_KEY')



class AwsSessionNotInitializedError(I2CEError):
    pass


class S3Upload():
    def __init__(self, instance):
        self.instance = instance
        if not AWS_SECRET_ACCESS_KEY or not AWS_ACCESS_KEY_ID: raise ValueError("AWS_ACCESS_KEY and AWS_ACCESS_KEY_ID must be specified")

        try:
            self.session = boto3.Session(aws_access_key_id= AWS_ACCESS_KEY_ID, aws_secret_access_key= AWS_SECRET_ACCESS_KEY)
        except Exception as e:
            return AwsSessionNotInitializedError('Session is not initialized for aws_access_key_id = %s and aws_secret_access_key = %s :: %s', AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, e)

    def _create_file_object(self, data):
        try:
            if isinstance(data, dict):
                data = json.dumps(data, indent=4)
            f_obj = StringIO(data)
            logger.info('File object:: %s',f_obj.getvalue())
        except Exception as e:
            return  Exception('Failed to dump data:: %s',e)
        return f_obj

    def _client(self):
        
        try:
            client  = self.session.client(self.instance) 
        except Exception as e:
            return Exception('Failed load %s client:: %s', self.instance, e)

        return client
    
    def _get_object_name(self, object_name = None, type = None, path = None):
        if type is None and object_name:
            type = splitext(object_name)[1]
            object_name = splitext(object_name)[0]
        if type is None:
            type = ".log"
        if not type.startswith('.'):
            type = '.' + str(type) 
        if path is None:
            path = "sthree-py-logs"
        if object_name:
            return object_name + type
        return path + "/"+ str(id_generator(size=7)) + str(now()) + type
         

    def upload_file_to_aws(self,f_path, bucket_name, object_name = None, **kwargs):
        if not ispath(f_path):
            return ValueError('Given path %s is not a valid file path', f_path)
        object_name = self._get_object_name(object_name, type = kwargs.get('type','log'), path = kwargs.get("path"))
        try:
            self._client().upload_file(f_path, bucket_name, object_name, ExtraArgs= {'ACL': 'public-read'})
        except Exception as e:
            return Exception('Failed to upload object to %s:: %s', bucket_name,e)
        
        logger.info('File %s uploaded successfully', self._get_object_name(object_name, type = kwargs.get('type','log')))
        return "https://{}.s3.amazonaws.com/{}".format(bucket_name, object_name)


    def upload_file_object_to_aws(self, data, bucket_name, object_name = None, **kwargs):
        try:
            self._client().upload_fileobj(self._create_file_object(data), bucket_name, self._get_object_name(object_name, type = kwargs.get('type','log')), ExtraArgs= {'ACL': 'public-read'})
        except Exception as e:
            return Exception('Failed to upload object to %s:: %s', bucket_name,e)
        
        logger.info('File object %s uploaded successfully', self._get_object_name(object_name, type= kwargs.get('type','log')))


def create_log_file(logfile,  rotate=True, level=10, formatter=None):
	_logger = logger
	if rotate:
		hdlr = logging.handlers.TimedRotatingFileHandler(logfile, when = 'D', interval = 1, backupCount = 30, encoding = 'utf-8')
	else:
		hdlr = logging.FileHandler(logfile)
	formatter = logging.Formatter(formatter  or "%(asctime)s: name-%(name)s: func-%(funcName)s[%(lineno)s]: %(levelname)s:  %(message)s")
	hdlr.setFormatter(formatter)
	_logger.addHandler(hdlr)
	_logger.setLevel(level)
	return _logger



def get_log_file(data, file_path= None, file_name=None,type = '.log', level='error', **kwargs):
   
    if not file_path:
        file_path  = os.environ.get('LOG_FILE', '/var/log/application/')
    if not file_name:
        file_name = str(id_generator(chars = string.ascii_lowercase + string.digits, size=7)) + str(now().strftime('%d-%m-%y'))
    if not file_name.endswith('.log'):
        file_name += type

    file_path= joinpath(file_path, file_name)
    if not ispath(file_path):
        mkdir_p(dirname(file_path))
    
    formatter = ''.join('{}-{}: '.format(i, kwargs.get(i)) if kwargs.get(i) else '' for i in ['enterprise_id','role','user_id'])
    formatter += "%(asctime)s: [%(lineno)s]: %(levelname)s:  %(message)s"
    _logger= create_log_file(file_path, rotate = kwargs.pop('rotate', True), formatter= kwargs.pop('formatter',formatter))
    _logger.error(json.dumps(data, indent = 4))
    return file_path




if __name__ =='__main__':
    #get_log_file({"kk":23,"fdf":44})    
    ins = S3Upload('s3')
    print ins.upload_file_to_aws(sys.argv[1],'general-iamdave-mumbai', sys.argv[2])
    #print ins.upload_file_to_aws(sys.argv[1],'general-iamdave-mumbai')
    # ins.upload_file_object_to_aws({'k':1,'s':2}, 'general-iamdave-mumbai')
    pass
