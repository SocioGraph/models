import sys
import time
from types import GeneratorType as gt
from functools import wraps

import numpy as np


class Timer(object):
    '''
    A class to help time function calls and get stats about them.
    Initialize an instance of Timer (say t) and put @t.timeit for all the
    functions you want to time. Then import t from the file and call t.stats()
    '''
    def __init__(self):
        self.functions = {}

    def timeiter(self, o):
        for i in o:
            yield i

    def timeit(self, func):
        @wraps(func)
        def func_wrapper(*args, **kwargs):
            name = func.__name__
            if hasattr(func, 'im_class'):
                name += '_of_class_' + func.im_class.__name__
            if not self.functions.has_key(name):
                self.functions[name] = [0, 0., 0.]
            start_time = time.time()
            o = func(*args, **kwargs)
            if isinstance(o, gt):
                o = dict(self.timeiter(o))
            stop_time = time.time() - start_time
            self.functions[name][0] += 1
            self.functions[name][1] += stop_time
            self.functions[name][2] += (stop_time*stop_time)
            return o
        return func_wrapper

    def stats(self, fp = sys.stderr, reset = False, function_names = []):
        for name, st in self.functions.iteritems():
            if not function_names or name in function_names:
                print >> fp, "Function: {}".format(name)
                print >> fp, "\tNumber of executions: {}".format(st[0])
                if st[0] > 0:
                    print >> fp, "\tTotal execution time: {}".format(st[1])
                    print >> fp, "\tAverage execution time: {}".format(st[1]/st[0])
                if st[0] > 10:
                    print >> fp, "\tStd. deviation in execution time: {}".format(np.sqrt((st[2]/(st[0]-1)) - ((st[1]/st[0])**2)))
        if reset:
            self.functions = {}
