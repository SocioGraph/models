import os, sys
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile, getmtime as modified_time
d = joinpath(dirname(dirname(dirname(abspath(__file__)))), 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
import json_tricks as json
import errno
import math
import tempfile
import string
import random
import numpy as np
from copy import deepcopy as copyof, copy
from contextlib import contextmanager
import re
from glob import glob
import logging
import logging.handlers
import os, sys, shutil
import urllib, StringIO
import hashlib
import base64
import uuid
import bcrypt
import unicodedata
import pprint as pp
from timeit import Timer
from aggregators import *
from compressors import *
from encrypt import *
import inflect as inflect_
import traceback
import requests
from functools import wraps
from itertools import product
from stuf import stuf, defaultstuf
from time import time, mktime
from pytz import timezone
from tzlocal import get_localzone
from dateutil import parser as dateparser
from datetime import datetime
from datetime import date
from datetime import timedelta
import calendar
from zipfile import ZipFile
from babel.numbers import format_currency
dtime = type(datetime.time(datetime.now()))
DTFMT = '%Y-%m-%d %I:%M:%S %p %z'
DFMT = '%Y-%m-%d'
TFMT = "%I:%M:%S %p %z"
UTC = timezone('UTC')
DAYFIRST = (os.environ.get('DAYFIRST', 'False') == 'True')
YEARFIRST = (os.environ.get('YEARFIRST', 'False') == 'True')
PASSWORD_NOT_SET = '__NOT_SET__'
START_DELIMITER = '{'
END_DELIMITER = '}'
MAX_LEVEL = 5


inflect = inflect_.engine()

pprint = lambda x: pp.pformat(x, indent = 4)

inf = np.inf

DEBUG_LEVEL = logging.getLevelName(os.environ.get('DEBUG_LEVEL', 'INFO'))

def mkdir_p(path):
    """ 'mkdir -p' in Python """
    try:
        os.makedirs(path)
        #print "path:%s",path
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise    
            
class dstuf(defaultstuf):
    def __init__(self, default = lambda: None, *args, **kwargs):
        super(dstuf, self).__init__(default, *args, **kwargs)

def get_logger(name, level = None, formatter = None, stream = None, log_file = None):
    logger = logging.getLogger(name)
    LOG_FILE = log_file or os.environ.get('LOG_FILE')
    if not logger.handlers:
        if LOG_FILE and not stream:
            if not ispath(LOG_FILE):
                mkdir_p(dirname(LOG_FILE))
            interval = int(os.environ.get('LOG_ROTATE_HOURS', 24))
            backup_days = int(os.environ.get('LOG_BACKUP_DAYS', 30))
            h = logging.handlers.TimedRotatingFileHandler(LOG_FILE, when = 'H', interval = interval, backupCount = int(24*backup_days/interval), encoding = 'utf-8') 
        else:
            h = logging.StreamHandler(stream or sys.stderr)
        h.setFormatter(logging.Formatter(formatter or "%(asctime)s: name-%(name)s: func-%(funcName)s[%(lineno)s]: %(levelname)s:  %(message)s"))
        logger.addHandler(h)
    logger.setLevel(level or DEBUG_LEVEL)
    logger.propagate = False
    return logger
                             
logger = get_logger(__name__)

def parse_forms_dict(inp, matcher = None, level = 0):
    logger.debug("Entered level %s, input: %s", level, inp)
    if not matcher:
        matcher = re.compile('([^\[\]]*?)\[(.*?)\]')
    out = {}
    any_new = []
    for k, v in inp.iteritems():
        if isinstance(v, list):
            if len(v) == 0:
                v = None
            elif len(v) == 1:
                v = v[0]
        if isinstance(v, (str, unicode)):
            if v == 'None' or v.lower() == 'null':
                v = None
            elif v.lower() == 'true':
                v = True
            elif v.lower() == 'false':
                v = False
            elif v == '[]':
                v = []
        if k.endswith('[]'):
            k = k[:-2]
        m = matcher.match(k)
        if m:
            g = m.groups()
            nk = g[0]
            nnk = g[1] + k[m.end():]
            if not out.has_key(nk):
                any_new.append(nk)
                logger.debug("In level %s, new key: %s", level, nk)
                out[nk] = {}
            out[nk].update({nnk: v})
        else:
            out[k] = v
    for k in any_new:
        out[k] = parse_forms_dict(out[k], matcher, level + 1)
    if not out:
        return out
    if all(isinstance(k,int) or (isinstance(k, (str, unicode)) and k.isdigit()) for k in out.keys()):
        logger.debug("Before converting to list out is:  %s", out)
        out = map(lambda x: x[1], sorted(out.iteritems()))
    return out


def make_list(inp, mapper = None):
    """
    Function to convert a single object into a list
    """
    if not isinstance(inp, (list, tuple)):
        inp = [inp]
    if hasattr(mapper, '__call__'):
        return map(mapper, inp)
    return list(inp)


def make_list_from_csv(inp):
    """
    Function to convert a csv into a list
    """
    if not inp:
        return []
    if isinstance(inp, (str, unicode)):
        return map(lambda x: x.strip(), inp.split(","))
    return make_list(inp)

def make_tuple(inp):
    """
    Function to convert a single object into a tuple
    """
    if not isinstance(inp, (list, tuple)):
       inp = (inp,)
    return tuple(inp)

def make_single(inp, iterator = None, force = False, default = None, ignore_dict = False):
    """
    Function returns the first element if length is one, None if length is 0 and the list if len is more than 1
    iterator can be either list or tuple or some other iterator type
    """
    if not isinstance(inp, (list, tuple, dict)):
        return inp
    elif not ignore_dict and isinstance(inp, dict):
        inp = inp.values()
    if len(inp) == 0:
        return default
    elif len(inp) == 1:
        return inp[0]
    elif force:
        return inp[0]
    if iterator:
        return iterator(inp)
    return inp

def isgoodipv4(s):
    s = s.split(":")[0]
    pieces = s.split('.')
    if len(pieces) != 4: 
        return False
    try: 
        return all(0<=int(p)<256 for p in pieces)
    except ValueError: 
        return False

def id_generator(size=6, chars=string.ascii_lowercase, init = '', final = ''):
    return init + ''.join(random.choice(chars) for _ in range(size)) + final

def list_adder(ini, more):
    if isinstance(more, (list, tuple)):
        return ini + list(more)
    return ini + [more]

def dict_adder(ini, more):
    if not isinstance(more, dict):
        return ini
    oni = copyof(ini)
    oni.update(more)
    return oni

def to_unicode(inp):
    if isinstance(inp, unicode):
        return inp
    try:
        inp = unicode(inp)
        return inp
    except (UnicodeEncodeError, UnicodeDecodeError) as e:
        try:
            inp = unicode(inp, errors = 'replace')
            return inp
        except (UnicodeEncodeError, UnicodeEncodeError) as e:
            try:
                inp = unicode(inp, errors = 'ignore')
            except (TypeError) as e:
                return u'{}'.format(inp)
    return inp

trans = string.maketrans('_/\\"\'?!.()&%','            ')
def normalize(inp, joiner = ' ', translate = None, lowercase = True, **kwargs):
    if isinstance(translate, tuple):
        translate = string.maketrans(*translate)
    else:
        translate = None
    if not isinstance(inp, (str, unicode)):
        inp = str(inp)
    if isinstance(inp, str):
        try:
            inp = inp.decode('utf-8')
        except (UnicodeEncodeError, UnicodeDecodeError):
            try:
                inp = unicode(inp, errors = 'replace')
            except (UnicodeEncodeError, UnicodeDecodeError):
                inp = unicode(inp, errors = 'ignore')
    inp = urllib.unquote_plus(inp)
    if not kwargs.get('retain_unicode'):
        inp = unicodedata.normalize('NFKD', inp).encode('utf-8','replace')
    else:
        joiner = unicode(joiner)
    inp = inp.strip()
    if lowercase:
        inp = inp.lower()
    try:
        return joiner.join(inp.translate(translate or trans).split())
    except TypeError:
        return joiner.join(inp.split())


def normalize_join(*inp, **kwargs):
    sep = kwargs.pop('separator', '_')
    if kwargs.get('retain_unicode'):
        sep = unicode(sep)
    return sep.join(sep.join(normalize(i, **kwargs).split()) for i in inp)

def make_title(inp, **kwargs):
    if not isinstance(inp, (str, unicode)):
        return inp
    return normalize(inp, **kwargs).title()


def cumsum(weights):
    sw = max(float(sum(weights)), 1e-35)
    total = 0.
    for w in weights:
        yield (total + w)/sw
        total += w

def histogram_sample(weights, cumsummed = False, input_array = None):
    if not cumsummed: weights = cumsum(weights)
    r = np.random.random_sample()
    for i, w in enumerate(weights):
        if w > r:
            return input_array[i] if isinstance(input_array, (list, tuple)) and input_array else i
    return input_array[0] if isinstance(input_array, (list, tuple)) and input_array else 0

def histogram_sample_generator(weights, num = 1, input_array = None):
    weights = list(cumsum(list(weights)))
    if not input_array: input_array = range(len(weights))
    for n in range(num):
        i = histogram_sample(weights, True)
        yield input_array[i]


def validate_password(inp, min_len = 8, check_digit = False, check_punct = False, check_case = False):
    if inp == PASSWORD_NOT_SET:
        return True
    class PasswordValidationError(Exception):
        def __init__(self):
            log = ["Password should be at least {} characters long".format(min_len)]
            if check_digit:
                log.append("one digit")
            if check_punct:
                log.append("one special character")
            if check_case:
                log.append("have at least one uppercase, one lowercase character")
            super(PasswordValidationError, self).__init__(', '.join(log) + '.')
    if len(inp) < min_len:
        raise PasswordValidationError()
    if check_digit:
        if not any(d in inp for d in string.digits):
            raise PasswordValidationError()
    if check_punct:
        if not any(d in inp for d in string.punctuation):
            raise PasswordValidationError()
    if check_case:
        if not any(d in inp for d in string.uppercase):
            raise PasswordValidationError()
        if not any(d in inp for d in string.lowercase):
            raise PasswordValidationError()
    return True

def hash_filename(name):
    return hashlib.sha224(name).hexdigest()

def make_uuid(*ids, **kwargs):
    n = kwargs.get('normalize') or (lambda x: normalize(x, translate = ('',''), lowercase = False))
    return base64.urlsafe_b64encode(''.join(map(n, ids))).replace('=','_')

def unmake_uuid(uuid):
    r = str(uuid.replace('_','='))
    return base64.urlsafe_b64decode(r)

def make_uuid3(*ids):
    return str(uuid.uuid3(uuid.NAMESPACE_DNS, ''.join(map(lambda x: normalize(x, translate = ('',''), lowercase = False), ids))))

def name_ext(name):
    return ( make_uuid(name.rsplit('.',1)[0]+"_"+str(time())), name.rsplit('.',1)[1])

def make_salt():
    return bcrypt.gensalt()

def hash_password(password, *args, **kwargs):
    if password == PASSWORD_NOT_SET:
        return password
    password = password.encode('utf-8', 'xmlcharrefreplace')
    try:
        verify_password(password, password)
    except ValueError as e:
        if not 'Invalid salt' in e.message:
            logger.warning('Trying to re-hash password')
            return password
    else:
        logger.warning('Trying to re-hash password')
        return password
    return bcrypt.hashpw(password, bcrypt.gensalt())

def verify_password(password, matched):
    if password == PASSWORD_NOT_SET or matched == PASSWORD_NOT_SET:
        return False
    return bcrypt.hashpw(password.encode('utf-8', 'xmlcharrefreplace'), matched.encode('utf-8', 'xmlcharrefreplace')) == matched


def from_list(nowlist, full_num = 6, additional = u'...', regular = u'', separator = u', ', last_symbol = 'and', more_symbol = 'more'):
    if len(nowlist) == 0:
        return ''
    elif len(nowlist) == 1:
        return nowlist[0]
    elif len(nowlist) <= full_num:
        return separator.join(nowlist[:-1]) + u' %s %s %s' % (last_symbol, nowlist[-1], regular)
    else:
        return separator.join(nowlist[:2]) + u' {} {} {} {}'.format(last_symbol, len(nowlist) - 1, more_symbole, additional)

def format_inr(value, do_raise = False):
    if value is None:
        return None
    if not is_number(value):
        if do_raise:
            raise TypeError("Value {} is not a number".format(value))
        return None
    return format_currency(value, 'INR', locale='en_IN')

def inr_words(value, do_raise = False, default = None):                                                                                             
    value = to_float(value, do_raise = do_raise)
    if value is None:
        return default
    dic = [
        (14,'Crore Crore'),
        (12,'Lakh Crore'),
        (10,'Thousand Crore'),
        (7,'Crore'),
        (5,'Lakh'),
        (3,'Thousand'),
    ]
    for e, n in dic:
        v = value/10.0**e
        if v > 1:
            return u'\u20b9 {:,.2f} {}'.format(v, n)
    return u'\u20b9 {:,.2f}'.format(value)
def error_raiser(error):
    raise error

def is_json_request(request):
	return request.headers.get('Content-Type') == "application/json"

@contextmanager
def read_file(*path):
    with open(joinpath(dirname(dirname(__file__)), *path), 'rb') as the_file:
        json_data = json.load(the_file, encoding = 'UTF-8')
        yield json_data

@contextmanager
def update_file(*path):
    path = joinpath(dirname(dirname(__file__)), *path)
    mode = 'r+b' if ispath(path) else 'w+b'
    with open(path, mode) as the_file:
        try:
            json_data = json.load(the_file, encoding = 'UTF-8') if mode == 'r+b' else {}
            yield json_data
        finally:
            the_file.seek(0)
            json.dump(json_data, the_file, indent = 4, encoding = 'UTF-8')
            the_file.truncate()

class FormatDictFormatter(string.Formatter):
    def __init__(self, data):
        super(FormatDictFormatter, self).__init__()
        self.data = data

    def get_value(self, key, args, kwds):
        if isinstance(key, (str, unicode)):
            try:
                return kwds[key]
            except KeyError:
                try:
                    r = self.data[key]
                    return "<{}>".format(key) if r is None else r
                except (KeyError, AttributeError, NameError) as e:
                    return "<{}>".format(key)
        else:
            return super(FormatDictFormatter, self).get_value(key, args, kwds)

def format_string(s, data, raise_error = True):
    if not isinstance(s, (str, unicode)):
        return s
    fmt = FormatDictFormatter(data)
    try:
        return fmt.format(s, **data)
    except (KeyError, AttributeError, NameError) as e:
        if not raise_error:
            return s
        raise
    return s

def make_format_dict(data, key, value, available_keys = None, quoted = True, update = False, level = 0):
    """
    makes a nested object into a formattable string type
    outputs set of available keys for formatting
    """
    if available_keys is None: available_keys = []
    key = str(key) if not isinstance(key, unicode) else key.encode('ascii', 'ignore')
    if isinstance(value, dict):
        if not (update and isinstance(data.get(key), stuf)):
            data[key] = dstuf()
        if level < MAX_LEVEL:
            for k, va in value.iteritems():
                nav = make_format_dict(data[key], k, va, quoted = False, level = level + 1)
                if quoted:
                    available_keys.extend(['{{{}.{}}}'.format(key, n) for n in nav])
                else:
                    available_keys.extend(['{}.{}'.format(key, n) for n in nav])
    elif isinstance(value, list):
        if not (update and isinstance(data.get(key), stuf)):
            data[key] = dstuf()
        if level < MAX_LEVEL:
            for ind, v in enumerate(value):
                nav = make_format_dict(data[key], ind, v, quoted = False, level = level + 1)
                if quoted:
                    available_keys.extend(['{{{}.{}}}'.format(key, n) for n in nav])
                else:
                    available_keys.extend(['{}.{}'.format(key, n) for n in nav])
    else:
        data[key] = value
        if quoted:
            available_keys.append('{{{}}}'.format(key))
        else:
            available_keys.append(key)
    return available_keys

def make_all_hierarchies(data, key, value, update = False, seperator = '.', list_unique = False, level = 0):
    key = str(key) if not isinstance(key, unicode) else key.encode('ascii', 'ignore')
    if isinstance(value, dict):
        if update and isinstance(data.get(key), dict):
            data[key].update(value)
        else:
            data[key] = value
        if level < MAX_LEVEL:
            for k, va in value.iteritems():
                make_all_hierarchies(data, key + seperator + normalize_join(k), va, seperator = seperator, level = level + 1)
    elif isinstance(value, list):
        if update and isinstance(data.get(key), list):
            data[key].extend(value)
            if list_unique:
                data[key] = list(set(data[key]))
        elif update:
            if not (list_unique and value in data[key]):
                data[key].append(value)
        else:
            data[key] = value
        if level < MAX_LEVEL:
            for ind, v in enumerate(value):
                make_all_hierarchies(data, key + seperator + normalize_join(ind), v, seperator = seperator, level = level + 1)
    else:
        data[key] = value
    return data


def remove_unwanted_keys(data, key_match = None):
    if isinstance(data, dict):
        r = data.__class__()
        for k, v in data.items():
            if not (isinstance(k, (str, unicode)) and k.startswith('_')):
                if not key_match or (isinstance(key_match, str) and re.match(key_match, k)):
                    r[k] = remove_unwanted_keys(v)
        return r
    if isinstance(data, list):
        r = data.__class__()
        for v in data:
            r.append(remove_unwanted_keys(v))
        return r
    return data

def get_greeting(tz = None):
    hr = datetime.now(tz).hour
    if hr < 11:
        return "Good Morning"
    elif hr < 14:
        return "Good Day"
    elif hr < 17:
        return "Good Afternoon"
    else:
        return "Good Evening"

def now(tz = None, as_datetime = True, units = None):
    d = datetime.now(tz = timezone(tz) if isinstance(tz, (str, unicode)) else UTC)
    if as_datetime:
        return to_datetime(d, tz = tz, units = units)
    return d.strftime(units or DTFMT)

def today(tz = None, units = None, as_date = False):
    """
    return date are string, set as_date = True to get in date format
    You can set tz and units of printing too
    """
    d = datetime.now(tz = timezone(tz) if isinstance(tz, (str, unicode)) else UTC).date()
    if not as_date:
        return d.strftime(units or DFMT)
    return d


def epoch():
    return time()

def to_epoch(value):
    if isinstance(value, float):
        return value
    if not value:
        return value
    return mktime(to_datetime(value, tz = get_localzone()).timetuple())

def to_datetime(value, units = None, dayfirst = None, yearfirst = None, tz = None, fuzzy = False):
    try:
        tz = tz or self.model.timezone
    except (NameError, AttributeError) as e:
        pass
    if value is None:
        return None
    if tz is None: tz = 'UTC'
    if isinstance(tz, (str, unicode)) and tz != '__NULL__':
        tz = timezone(tz)
    if isinstance(value, datetime):
        if tz:
            if value.tzinfo:
                return value.astimezone(tz)
            return tz.localize(value)
        return value
    elif isinstance(value, (date, dtime)):
        value = value.strftime(DTFMT)
    elif isinstance(value, dict) and value.get('timestamp'):
        value = value.get('timestamp')
    if dayfirst is None: dayfirst = DAYFIRST
    if yearfirst is None: yearfirst = YEARFIRST
    try:
        value = float(value)
        if value > 2e10:
            value /= 1000.0
    except (ValueError, TypeError) as e:
        pass
    if isinstance(value, (str, unicode)):
        if value in ['null', 'None', '__NULL__']:
            return None
        if units:
            units = make_list_from_csv(units)
            d = None
            for u in units:
                try:
                    d = datetime.strptime(value, u)
                except ValueError:
                    continue
                else:
                    break
            if not d:
                raise ValueError("Could not convert to datetime with units %s".format(units))
        else:
            try:
                if value.lower().endswith('z'):
                    # Avoid issue with unparsable style
                    d = datetime.strptime(value, '%Y-%m-%dt%H:%M:%S.%fz')
                    tz = timezone('UTC')
                else:
                    try:
                        d = datetime.strptime(value, '%d-%m-%Y')
                    except ValueError:
                        try:
                            d = datetime.strptime(value, '%d/%m/%Y')
                        except ValueError:
                            d = make_single(
                                dateparser.parse(value, dayfirst = dayfirst, yearfirst = yearfirst, fuzzy_with_tokens = fuzzy),
                                force = True
                            )
            except ValueError:
                try:
                    v = float(value)
                    if v > 2e10:
                        v /= 1000.0
                    d = datetime.fromtimestamp(v)
                except ValueError as e:
                    raise TypeError("Cannot parse value {} to datetime: {}".format(value, e.message))
    elif isinstance(value, (float)):
        d = datetime.fromtimestamp(value)
    else:
        raise TypeError("Cannot parse value {} to datetime. Accepted types are time, date, datetime, float, str, unicode. Not {}".format(value, type(value)))
    if tz and tz != '__NULL__':
        if d.tzinfo:
            return d.astimezone(tz)
        return tz.localize(d)
    return d

def time_diff(a, b, *args, **kwargs):
    a = to_datetime(a, *args, **kwargs)
    b = to_datetime(b, *args, **kwargs)
    return (b - a).total_seconds()


def gen_get_top(number, compare_func = lambda x, y: x > y, key_func = lambda x: x, value_func = lambda x: x):
    num_list =[]
    def return_func(key, value):
        value = value_func(value)
        key = key_func(key)
        if len(num_list) < number:
            num_list.append((key, value))
            num_list.sort(key = lambda x: x[1], reverse = True)
            return num_list
        for i, l in enumerate(num_list):
            if compare_func(value, l[1]):
                num_list.insert(i, (key, value))
                num_list.pop()
                break
        return num_list
    return_func.__name__ = 'get_top_{}'.format(number)
    return_func.top_list = lambda: num_list
    return return_func

def gen_get_total():
    total = [0]
    def return_func(key, value):
        total[0] += value
        return total[0]
    return_func.__name__ = 'get_total'
    return_func.total = lambda: total[0]
    return return_func

def add_month(sourcedate, months = 1):
    month = sourcedate.month - 1 + months
    year = int(sourcedate.year + month / 12 )
    month = (month % 12) + 1
    day = min(sourcedate.day, calendar.monthrange(year,month)[1])
    return sourcedate.__class__(year, month, day)


def make_recovery_id(*object_id, **kwargs):
    ts = time()
    add_days = kwargs.get('add_days')
    if add_days:
        ts += float(add_days)*3600*24
    object_id = list(object_id) + [str(ts)]
    unique_id = base64.urlsafe_b64encode('__'.join(object_id))
    return unique_id

def is_number(i, replace_list = None):
    if isinstance(i, (int, float, long)):
        return True
    if not isinstance(i, (str, unicode)):
        return False
    i = i.strip()
    if isinstance(replace_list, dict):
        for r1, r2 in replace_list.iteritems():
            i = i.replace(r1, r2)
    try:
        float(i)
    except ValueError:
        return False
    return True

def to_string_search(inp):
    if isinstance(inp, dict):
        return map(to_string_search, inp.keys())
    if isinstance(inp, (list, tuple)):
        return map(to_string_search, inp)
    if inp is None:
        return inp
    if isinstance(inp, (int, float, long)):
        return inp
    if isinstance(inp, str):
        try:
            inp = inp.decode('utf-8')
        except (UnicodeEncodeError, UnicodeDecodeError):
            return unicode(inp, errors = 'ignore').lower()
    if not isinstance(inp, unicode):
        inp = unicode(inp)
    return inp.lower()


def to_float(i, do_raise = True, replace_list = None, default = None):
    if i is None:
        return default
    if isinstance(i, (int, float, long)):
        return float(i)
    if not isinstance(i, (str, unicode)):
        if do_raise:
            raise TypeError("Unknown format to convert to float: {} ({})".format(i, type(i)))
        else:
            return default
    i = i.strip()
    replace_list = replace_list or {',': ""}
    if isinstance(replace_list, dict):
        for r1, r2 in replace_list.iteritems():
            i = i.replace(r1, r2)
    if i in ('null', '__NULL__'):
        return default
    try:
        return float(i)
    except ValueError:
        if do_raise:
            raise 
        else:
            return default


def try_number(i, replace_list = None):
    if not isinstance(i, (str, unicode)):
        return i
    i = i.strip()
    if isinstance(replace_list, dict):
        for r1, r2 in replace_list.iteritems():
            i = i.replace(r1, r2)
    if i in ('null', '__NULL__'):
        return None
    try:
        return float(i)
    except ValueError:
        return i
    return i

def split_recovery_id(unique_id):
    u_id = base64.urlsafe_b64decode(unique_id.encode("ascii"))
    r = u_id.rsplit('__')
    return tuple(r)

def print_error(*args):
    tr = traceback.format_exc()
    logger.error(tr)
    if args: 
        logger.error(*args)
    tr  = tr.strip().splitlines() + list(args)
    return tr

	
class BaseError(Exception):
    def __init__(self, *args, **kwargs):
        if kwargs.pop('log', True):
            print_error(*args)
        super(BaseError, self).__init__(*args, **kwargs)

class BaseErrorWrapper(Exception):

    classname = 'BaseError'
    def __init__(self, *args, **kwargs):
        map(lambda x: logger.error(x), traceback.format_exc().strip().splitlines())
        map(lambda x: logger.error("Cause for the raising of {}: {}".format(self.classname, x)), args)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        args = tuple([args[0] + (exc_value or '')] + list(args[1:]))
        super(BaseErrorWrapper, self).__init__(*(args + (exc_value,)), **kwargs)

class I2CEError(BaseError):
    pass

class I2CEErrorWrapper(BaseErrorWrapper):
    classname = 'I2CEError'


def get_curr_func_name():
    return sys._getframe(1).f_code.co_name

class ConditionalList(object):
    def __init__(self):
        self._dict = {None: []}
        self._conditions = {None: {}}

    def replace(self, nlist, conditions = None):
        nlist = make_list(nlist)
        key = str(sorted(conditions))
        if conditions:
            self._dict[key] = nlist
            self._conditions[key] = conditions
        else:
            self._dict[None] = nlist

    def update(self, nlist, conditions = None):
        nlist = make_list(nlist)
        key = str(sorted(conditions))
        if conditions:
            self._dict[key] = list(set(self._dict[key] + nlist))
            self._conditions[key] = conditions
        else:
            self._dict[None] = list(set(self._dict[key] + nlist))

    def get(self, instance = None):
        if not instance:
            return self._dict[None]
        for c, i in self._conditions.iteritems():
            if all(instance.get(_i) == _v for _i, _v in i.iteritems()):
                return self._dict[c]
        return self._dict[None]

def read_text_from_url(url, params=None, content_type='json'):
    r = requests.get(url, params=params or {})
    
    try:
        if r.status_code < 400:
            return r.json() if content_type=='json' else r.text
        else:
            return {"error": r.text} if content_type == 'json' else r.text
    except Exception as e:
        return e.message


def joinurl(*args, **kwargs):
    r = '/'.join(map(lambda x: x.strip('/'), args))
    if kwargs:
        r += '?'
        for k, v in kwargs.iteritems():
            r += "{}={}".format(k, v)
    return r

def download_or_copy(url, path = None, force_copy = True, url_path_map = None, *args, **kwargs):
    # If the URL is a file on the system, based on url_path_map, then simple copy instead of downloading
    def do_path(nf):
        if not force_copy:
            return nf
        elif isinstance(path, (str, unicode)):
            if not ispath(path):
                mkdir_p(dirname(path))
            logger.info("Found download path %s, locally @ %s, copying to %s", url, nf, path)
            shutil.copy(nf, path)
            return path
        else:
            with open(nf, 'rb'):
                return nf.read()
    if ispath(url):
        return do_path(url)
    for k, v in (url_path_map or {}).iteritems():
        if url.startswith(k) and ispath(v):
            nf = url.replace(k, v)
            if ispath(nf):
                return do_path(nf)
    return download_file(url, path, *args, **kwargs)

class FileSizeExceeded(I2CEError):
    pass

class FileNotFound(I2CEError):
    pass

def download_file(url, path = None, max_chunks = 1024, chunk_size = 1024, raise_error_downloading = False, force = False):
    # NOTE max_chunks is 1MB
    # NOTE the stream=True parameter
    try:
        if isinstance(path, (str, unicode)):
            if not ispath(path):
                mkdir_p(dirname(path))
            elif isfile(path) and not force:
                ft = to_datetime(modified_time(path))
                r1 = requests.head(url)
                ut = r1.headers.get('Last-Modified') or r1.headers.get('last-modified')
                if ut:
                    ut = to_datetime(ut)
                    if ut < ft:
                        logger.info("File at path: %s is newer than at the URL: %s", path, url) 
                        return path
            string_file = open(path, 'wb')
        else:
            string_file = StringIO.StringIO()
        r = requests.get(url, stream=True)
        num_chunks = 0
        try:
            if r.status_code >= 400:
                logger.error("Error downloading from url: %s", url)
                logger.error(r.content)
                raise FileNotFound(r.content)
            for chunk in r.iter_content(chunk_size = chunk_size): 
                if chunk: # filter out keep-alive new chunks
                    string_file.write(chunk)
                    #f.flush() commented by recommendation from J.F.Sebastian
                    num_chunks += 1
                if num_chunks >= max_chunks:
                    raise FileSizeExceeded("File size exceeded {} Kb for URL: {}".format(max_chunks, url))
        except FileNotFound:
            if raise_error_downloading:
                raise
            if not isinstance(string_file, StringIO.StringIO):
                string_file.close()
                return path
            else:
                return string_file.getvalue()
        except:
            raise
        else:
            if not isinstance(string_file, StringIO.StringIO):
                string_file.close()
                return path
            else:
                return string_file.getvalue()
        finally:
            if not isinstance(string_file, StringIO.StringIO):
                string_file.close()
    except requests.exceptions.BaseHTTPError as e:
        if raise_error_downloading:
            raise
        return None


def get_from_inst(ins, att = None, default = None, allow_none = False, **kwargs):
    if not isinstance(ins, dict):
        return default if default != None else att
    if not isinstance(att, (str, unicode)):
        return default if default != None else att
    r = ins.get(att, default)
    if r != None or allow_none:
        return r
    return default if default !=None else  att

def get_from_list(l, index = 0, default = None):
    if not isinstance(l, (list, tuple)):
        return l
    try:
        return l[index]
    except IndexError:
        return default

def distance(latlong1, latlong2):
    if latlong1 is None: return None
    if latlong2 is None: return None
    try:
        latlong1 = map(float, make_list(latlong1))
        latlong2 = map(float, make_list(latlong2))
    except ValueError as e:
        raise Exception("Error in converting latlong to float {}".format(e)) 
    return np.sqrt(
        reduce(
            lambda x, y: x + (y[0] - y[1])**2.0 if isinstance(y[0], (int, float, long)) and isinstance(y[1], (int, float, long)) else 0, 
            zip(latlong1, latlong2),
            0
        )
    )

def number_to_range(value, default = None, *args, **kwargs):
    value = to_float(value, False)
    lower_bound = kwargs.get('lower_bound', 'inclusive')
    upper_bound = kwargs.get('upper_bound', 'inclusive')
    if value is None:
        return default
    for k in args:
        if len(k) != 3:
            continue
        (mn, mx, val) = k
        if mn is None:
            mn = -1e32
        if mx is None:
            mx = 1e32
        if ( (lower_bound == 'inclusive' and value >= mn ) or ( lower_bound == 'exclusive' and value > mn ) ) and ( ( upper_bound == 'inclusive' and value <= mx) or ( upper_bound == 'exclusive' and value < mx ) ):
            return val
    return default


def number_categories(value, sep = '-', suffix = '', prefix = '', min_exponent = 1, max_exponent = 6, default = 0, **kwargs):
    if not isinstance(value, (int, float, long)):
        return default or 0
    exps = ['1', '10', '100', '1K', '10K', '100K', '1M', '10M', '100M', '1B', '10B', '100B', '1T', '10T', '100T', '1Q', '10Q', '100Q']
    if min_exponent < 0:
        min_exponent = 0
    if max_exponent >= len(exps):
        max_exponent = len(exps) - 1
    if value < 10**min_exponent:
        return "{}<{}{}".format(prefix, exps[min_exponent], suffix)
    for e in range(min_exponent+1, max_exponent+1):
        if value < 10**e:
            return "{}{}{}{}{}".format(prefix, exps[e-1], sep, exps[e], suffix)
    return '{}>{}{}'.format(prefix, exps[max_exponent], suffix)

def resolve_log(value, exponent = 10, max_exponent = 10, min_exponent = 0, sep = '-', suffix='', prefix = '', insist_zero = False, shorten = False):
    if not isinstance(value, (int, float, long)):
        try:
            value = float(value)
        except ValueError:
            return value
    gs = '>'
    ns = ''
    afs = ''
    if value < 0:
        value = -value
        gs = '<'
        ns = '(-'
        afs = ')'
    if insist_zero and value == 0:
        return '{}0{}'.format(prefix, suffix)
    if value <= 10**min_exponent:
        return '{}0{}{}{}{}{}'.format(prefix, sep, ns, 10**min_exponent, afs, suffix)
    for e in range(1,max_exponent):
        if value <= exponent**e:
            v = (exponent**(e-1))*(float(value)//(exponent**(e-1)))
            v1 = v + (exponent**(e-1))
            return '{}{}{:0.0f}{}{}{}{:0.0f}{}{}'.format(prefix, ns, v, afs, sep, ns, v1, afs, suffix)
    return '{}{}{}{:0.0f}{}{}'.format(prefix, gs, ns, exponent**max_exponent, afs, suffix)

def all_combinations(lists, key_map = None):
    """
    Function to create all combinations of filters for keys with list of values
    e.g. lists = {
    "showroom_type": ["Nexa", "Arena"],
    "city": ["Delhi", "Mumbai", "Bengaluru"],
    "variant": ["Low-end", "High-end"]
    }
    key_map = {"showroom_type": "Channel Type"}
    """
    key_map = key_map or {}
    keys = []
    values = []
    titles = []
    for k, v in lists.iteritems():
        keys.append(k)
        values.append(make_list(v))
        titles.append(key_map.get(k, k.replace('_', ' ').title()))
    for value in product(*values):
        filters = {k: v for k, v in zip(keys, value)}
        filter_titles = {k: v for k, v in zip(titles, value)}
        yield filters, filter_titles
        

def evaluate_args(params, data, error_string = None, force_none = False):
    def check(v):
        if isinstance(v, dict):
            return evaluate_args(v, data, error_string)
        elif isinstance(v, (list, tuple)):
            return type(v)(check(v1) for v1 in v)
        elif isinstance(v, (unicode, str)):
            if v.startswith(START_DELIMITER) and v.endswith(END_DELIMITER):
                vi = v.strip(START_DELIMITER).strip(END_DELIMITER)
                if vi == '__data':
                    return copyof(data)
                if vi in data:
                    return data[vi]
            if '{' in v and '}' in v and '{{' not in v and '}}' not in v:
                try:
                    return eval(v.format(**data))
                except (NameError, KeyError, AttributeError, SyntaxError, IndexError) as e:
                    if error_string:
                        logger.warning(error_string)
                    try:
                        return v.format(**data)
                    except (NameError, KeyError, AttributeError, SyntaxError, IndexError) as e:
                        if error_string:
                            logger.warning(error_string)
                        if force_none:
                            return None
                        return v
        return v
    if not isinstance(params, dict):
        return check(params)
    for k, v in params.iteritems():
        params[k] = check(v)
    if force_none:
        params = {k: v for k, v in params.iteritems() if v is not None}
    return params

def map_instance(ins, key_maps = None, value_maps = None, default = None, do_by_key_map = False, splitter = '.'):
    """
    ins: the instance we want to get the mapping for
    key_maps: the mapping between the keys of first instance and second instance. If we want nesting in the second instance we can use . in the key
    sA
    value_maps is the mapping between values on the initial instance to the other instance
    default: default value
    do_by_key_map: if true, it will iterate over the keys from the key_maps else it will take from the instance
    splitter: for nested objects how do we want to split the attributes in the key_map by, default "."
    """
    value_maps = value_maps or {}
    key_maps = key_maps or ({k:k for k in ins} if do_by_key_map else {})
    d = {}
    if do_by_key_map:
        it = key_maps.iteritems()
    else:
        it = ins.iteritems()
    for k, v in it:
        prev_d = d;
        if do_by_key_map:
            new_k = v
            new_v = ins.get(k, None)
        else:
            new_k = key_maps.get(k, k)
            new_v = v
        if isinstance(new_v, (str, unicode, int, float, tuple, long)):
            new_v = value_maps.get(new_v, value_maps.get(k, {new_v: new_v}).get(new_v, new_v))
        if isinstance(new_k, (str, unicode)):
            new_ks = new_k.split(splitter)
        new_ks = make_list(new_ks)
        for v1 in new_ks[:-1]:
            if v1 not in prev_d:
                prev_d[v1] = {}
            prev_d = prev_d[v1]
        vf = new_ks[-1]
        prev_d[vf] = default if new_v is None else new_v
    return d

def make_entropy(instance):
    t_ = float(sum(instance.values()))
    if t_ <= 1:
        return 0.
    d_ = len(instance)
    if t_ == d_ or d_ <= 1.:
        return 0.
    d_ = np.log10(d_)
    e_ = sum(-(i*np.log10(i/t_))/(t_* d_) for i in instance.itervalues())
    if np.isnan(e_) or np.isinf(e_):
        return 0.
    return e_

def this_quarter(d):
    m = 1 + (((d.month - 1)/3)*3)
    s = '%Y-{0:02d}-01 00:00:00.000000%z'.format(m)
    return to_datetime(d.strftime(s), tz = d.tzinfo)

def url_validator(a):
    if not a:
        return False
    return True if re.match(r'^(http[s]?://){1}[\w+.\-]?\w+[.\w+\-]{2,}[/[\w+.]*]*', a) else False

def url_converter(a):
    if not a:
        return None
    if not isinstance(a, (str, unicode)):
        logger.error("input url should be a string not {}".format(type(a)))
        return None
    if a.startswith('www'):
        return 'http://' + a
    return a

def change_spelling(word, num_perturb = 1, chars = None):
    word = unicode(word).lower()
    chars = chars or string.ascii_lowercase
    def do(word, func):
        pword = word
        while pword == word:
            i = int(np.floor(np.random.random_sample()*len(word)))
            lword = list(word)
            word = u''.join(func(lword, i))
        return word
    def substitute(lword, i):
        lword[i] = random.choice(chars)
        return lword
    def insert(lword, i):
        lword.insert(i, random.choice(chars))
        return lword
    def delete(lword, i):
        lword.pop(i)
        return lword
    for n in range(num_perturb):
        word = do(word, random.choice([substitute, insert, delete]))
    return word

        

def invert_dict(init_dict, **kwargs):
    if not isinstance(init_dict, dict):
        return init_dict
    return {v:k for k,v in init_dict.items() if isinstance(v,(str,unicode,float,int,long))}

def is_ip_address(ip):
    if not isinstance(ip, (str, unicode)):
        return False
    if '.' not in ip and ':' not in ip:
        return False
    if not (len(ip.split('.')) >= 4 or len(ip.split(':')) >= 3):
        return False
    return True

class cd(object):
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


def split_name_to_3(name, index = None, allowed_titles = None, **kwargs):
    allowed_titles = make_list(allowed_titles or [])
    if not name:
        r = ("", "", "")
    else:
        for k in allowed_titles:
            if name.lower().startswith(k.lower()):
                name = name[len(k):]
                name = name.strip()
        n = name.split()
        r = (
          n[0],
          " ".join(n[1:-1]),
          n[-1]
        )
    if not isinstance(index, (int, long)):
        return r
    if index > 2:
        return r
    return r[index]

def zip_dir(tdir, path = None):
    path = path or joinpath(tdir, "synthesize.zip")
    logger.info("Generating zip file")
    try:
        with cd(tdir):
            with ZipFile(path, 'w') as z:
                for f in glob("*/*"):
                    if not f.endswith('zip'):
                        logger.info("Zipping file %s", f)
                        z.write(f)
        logger.info("Finished Generating zip file")
    except Exception as e:
        print_error(e)
        logger.error("Error in generating zip {}".format(e))
        return None
    return path


time_divisors = {
    'seconds': 1.0,
    'minutes': 60.0,
    'hours': 3600.0,
    'days': 24*3600.0,
    'weeks': 24*7*3600.0,
    'months': 365*24*3600.0/12.0,
    'years': 365*24*3600.0
}

def default_dict(tz = None): 
    return {
         'now': str(now(tz = tz)),
         'today': today(tz = tz),
         'now_ctime': str(now(tz = tz).time()),
         'start_of_day': now(tz = tz).strftime('%Y-%m-%d 00:00:00.%z'),
         'one_day_ago': str(now(tz = tz) - timedelta(hours = 24)),
         'start_of_week': str(to_datetime(datetime.strptime(now(tz = tz).strftime('%W %Y 1'), '%W %Y %w'), tz = tz)),
         'start_of_week_date': str(to_datetime(datetime.strptime(now(tz = tz).strftime('%W %Y 1'), '%W %Y %w'), tz = tz).date()),
         'one_week_ago': str(now(tz = tz) - timedelta(hours = 24*7)),
         'start_of_month': now(tz = tz).strftime('%Y-%m-01 00:00:00%z'),
         'start_of_month_date': now(tz = tz).strftime('%Y-%m-01'),
         'start_of_quarter': str(this_quarter(now(tz = tz))),
         'start_of_quarter_date': str( this_quarter(now(tz = tz)).date()),
         'start_of_year': now(tz = tz).strftime('%Y-01-01 00:00:00%z'),
         'start_of_year_date': now(tz = tz).strftime('%Y-01-01'),
         'one_year_ago': str(now(tz = tz) - timedelta(days = 365)),
         'start_of_financial_year': now(tz = tz).strftime('%Y-04-01 00:00:00%z'),
         'start_of_financial_year_date': now(tz = tz).strftime('%Y-04-01'),
         'base_url': "{}://{}".format(os.environ.get('HTTP', 'http'), os.environ.get('SERVER_NAME', 'localhost:5000'))
    }
