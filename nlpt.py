import helpers as hp
import model as mod
import algorithms
import json, types, math, string
import validators as val
from datetime import datetime,timedelta,date
from operator import itemgetter, attrgetter
import re, os, requests
from nltk.tokenize.treebank import TreebankWordTokenizer
from tempfile import NamedTemporaryFile
from pgnlp import NlpDict, JsonBDict
import helpers as hp
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, getmtime as modified_time
from itertools import chain
from strmatch import LevenshteinStringMatch
import time

NER_SERVER_URL= '{}/tags'.format(os.environ.get('NER_SERVER_URL', 'https://ner.iamdave.ai'))

logger = hp.get_logger(__name__)

ROOT_PATH = abspath(os.curdir)
DATA_PATH = joinpath(abspath(os.curdir), 'data')

class EntityNotDefined(hp.BaseError):
    pass

class FilterDict(dict):

    def __init__(self, input_dict, score_func=None, match_score_value= 1, penalty_value = 1):
        self._default = {}
        self.match_score_value= match_score_value
        self.penalty_value = penalty_value
        self.ms = score_func or (lambda x: 1)
        for key, value in input_dict.iteritems():
            self.add(key, value)
    
    def add(self, key, value):
        def copy(val):
            if isinstance(val, dict):
                return dict((k, copy(v)) for k,v in value.iteritems())
            elif isinstance(val, list):
                return list(copy(i) for i in val)
            else:
                return val

        self[key] = copy(value)
        #self._default[key] = copy(value)

    def filter(self, criteria, update=True):
        if not update:
            fDict = FilterDict(self)
        else:
            fDict = self
        
        for key, value in self.items():
            if (criteria(value)):
                fDict.pop(key)
        return fDict
   
    def update(self, dt):
        for key, value in input_dict.iteritems():
            self.add(key, value)

    @property
    def length(self):
        return len(self.keys())

    def reset(self):
        for key,val in self._default.iteritems():
            self.add(key, val)

    def filter_keys(self, keys):
        for k in keys:
            yield k, self[k]

    def filter_by_regx(self, wd):
        exp = "^"+wd
        for k in self:
            mc = re.match(exp, k)
            if not mc:
                continue
            ms = k[mc.span()[0]: mc.span()[1]]
            if ms:
                klen = len(k)*1.
                yield k, self[k] or 0.2, min(len(wd)/klen, klen/len(wd))

    def find_match(self, words):
        if not words or len(words) == 0:
            raise StopIteration 

        stk = {}
        c = len(words)
        while self.length and c > 0:
            wd = " ".join(words[:c])
            l = sorted(self.filter_by_regx(wd), key=lambda x: x[1], reverse=True)
            for w, v, ms in l:
                if ms == 1.:
                    yield wd, w, (c * self.ms(w, match_score_value= self.match_score_value, penalty_value = self.penalty_value))+v, c
                    self.pop(w)
                    stk.pop(w, None)
                    raise StopIteration
                elif ms > 0.7:
                    stk[w] = ( wd, (c * self.ms(w, match_score_value= self.match_score_value, penalty_value = self.penalty_value))+(v*ms ), c)
            c-=1
            if len(l) == 0 or len(stk) == 0:
                continue
            for k in self.keys():
                if k not in stk:
                    self.pop(k)

        for i,mp in sorted(stk.items(), key=lambda x:x[1][1], reverse=True):
            yield mp[0], i, mp[1], mp[2]

        self.reset()
 

class NLSen(object):
    def __init__(self, sentence, **kwargs):
        pass

"""
class NlpTagger(object):

    def __init__(self, name, corpus='en_core_web_lg'):
        self._model = self._load_model(corpus) #spacy.load(corpus or 'en_core_web_sm')

    def _load_model(self, name):
        model_path = "{}/{}/{}".format(ROOT_PATH,"spacy_models", name)
        default_path = "{}/{}/{}/{}".format( ROOT_PATH, "models", "spacy_models", name)

        try:
            if ispath(model_path):
                return spacy.load(model_path)
            elif ispath(default_path):
                return spacy.load(default_path)
            else:
                return spacy.load(name)
        except ValueError as e:
            pass
        return None

    def get_tags(self, query):
        if self._model is None:
            raise StopIteration
        document = self._model(query)
         
        for element in document.ents:
            yield element.label_, element.text
            print ('Type: %s, Value: %s' % (element.label_, element))
"""

class NerParser(object):
    def __init__(self, host= None):
        self.host = host or NER_SERVER_URL

        #functions for convert to canonical value
        def to_aadhar(x):
            x= x.replace(" ", "").replace("-", "").replace(".", "")
            return " ".join(x[4*i: (4*i) +4] for i in range(3))
        
        def to_phone(x):
            x= x.replace(" ", "").replace("-", "").replace("(", "").replace(")", "").replace(".", "")
            if x.startswith("00"):
                return "+{}".format(x[2:])
            return x
        def format_mobile(x):
            _x=x
            c = {}
            ls = re.findall(r"(double\s?\d\s?)", x)
            for se in ls:
                x = x.replace(se, re.findall(r"\d", se)[0]*2)
                c[re.findall(r"\d", se)[0]*2] = "".join(se.split())
            ls = re.findall(r"(triple\s?\d\s?)", x)
            for se in ls:
                x = x.replace(se, re.findall(r"\d", se)[0]*3)
                c[re.findall(r"\d", se)[0]*3] = "".join(se.split())
            ls = re.findall(r"\d\s\d", x)
            for se in ls:
                x = x.replace(se, "".join(re.findall(r"\d", se)))
            return x,_x,c

        def to_email(x):
            for p,r in ({" at the rate ": "@"," at ": "@", " dot ": "."}).items():
                x = x.replace(p, r)
            x = x.replace(" ", ".")
            return x
        
        def datetime_canonical(x):
            try:
                x = x.replace(" . ", ":")
                return hp.to_datetime(x, fuzzy=True).strftime('%Y-%m-%d %H:%M')
            except:
                return ""


        self.patterns = {
            "uid": r'[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}',
            "alpha_numeric": r"(([a-zA-Z]+[0-9]+[a-zA-Z]*)|([0-9]+[a-zA-Z]+[0-9]*))+",
            "number": {
                "pattern": r"[0-9]+(\s[0-9]+)*",
                "canonical": lambda x: x.replace(" ", "")
            },
            "aadhar": {
                "pattern": r"[0-9]{12}$|((([0-9]{4}(\s)){2})|(([0-9]{4}(-)){2})|(([0-9]{4}(\.)){2}))[0-9]{4}",
                "canonical": to_aadhar 
            },
            "pan_number": {
                "pattern": r"([A-Za-z]\s?){5}([0-9]\s?){4}[A-Za-z]",
                "canonical": lambda x: x.replace(" ", "")
            },
            "vehicle_number": {
                "pattern": r"[A-Za-z]{2}[\s\-\.\_]?[0-9]{1,2}[\s\-\.\_]?[A-Za-z]{1,3}[\s\-\.\_]?[0-9]{2,4}",
                "canonical": lambda x: (x.replace(z, "") for z in ["-",".", "_"," "])
            },
            "pincode": {
                "pattern": r"[0-9]{6}",
                "canonical": lambda x: x.replace(" ", "")
            },
            "email": {
                "pattern": r"[a-zA-Z]{1}([\w-]+(\.|\-))*[\w]+(@|\sat\s|\sat\sthe\srate\s){1}([\w-]+(\.|\sdot\s|\s))+[\w-]{2,4}",
                "canonical": to_email,
                "priority": True
            },
            "phone_number": {
                "pattern": r"([0]|((\+|00)91[\s\-\.]?)|\((\+|00)?91\)\s?)?(\d{10}|\d{5}[\s\-\.]?\d{5,6}|\d{3}[\s\-\.]?\d{3}[\s\-\.]?\d{4,5}|\(\d{4}\)[\s\-\.]?\d{3}[\s\-\.]?\d{3,4}|\(\d{3}\)[\s\-\.]?\d{3}[\s\-\.]?\d{4,5}|\d{2}[\s\-\.]?\d{3}[\s\-\.]?\d{6})",
                "canonical": to_phone,
                "formater": format_mobile,
                "priority": True
            },
            "datetime": {
                "pattern": r"(([0-9]{1,2}\/{1}([0-9]{1,2}|[a-z]{3,})\/(20|19)?[0-9]{2})|([0-9]{1,2}\-([0-9]{1,2}|[a-z]{3,})\-(20|19)?[0-9]{2}))?\s?([0-9]{1,2}\s?[\:\.]\s?[0-9]{1,2}\s?((a|A|p|P)[\.\s]?(m|M)[\.\s]?)?)?",
                "priority": True,
                "canonical": datetime_canonical
            }
        }


        """
        "date": {
            "pattern": r"(([0-9]{1,2}\/{1}([0-9]{1,2}|[a-z]{3,})\/[0-9]{2,4})|([0-9]{1,2}\-([0-9]{1,2}|[a-z]{3,})\-[0-9]{2,4}))?",
            "priority": True,
            "canonical": lambda x: hp.to_datetime(x, fuzzy=True)
        },
        "datetime": {
            "pattern": r"([0-9]{1,2}[\:\s\.][0-9]{1,2}\s?((a|A)[\.\s]?(m|M)[\.\s]?)?)?",
            "priority": True,
            "canonical": lambda x: hp.to_datetime(x, fuzzy=True)
        },

        "time":{
            "pattern": r"(1[0-2]|0?[1-9])[\:\.\s\-]?([0-5]?[0-9])?(\s?[A|a|p|P][\.\s]?[m|M][\.\s]?)?(\s?morning|\s?evening|\s?afternoon)?",
            "priority": True
        },
        "date":{
            "pattern": r"(0?[1-9]|[12][0-9]|3[01])[- \.\/](0?[1-9]|1[012])[- \.\/](19|20)?\d\d",
            "priority": True
        }
        """


    def regex_formatter(self, otext, pre=None):
        pre = pre or []
        ind = len(pre)

        def formatter_word_match(s1, s2, replables):
            for i,j in replables.items():
                s2 = s2.replace(i, j)

            s1 = s1.split() 
            s2 = s2.split() 
            if len(s1) == len(s2): 
                return {} 
            s3 = [] 
            c_map = {}  
            i2 = 0 
            for i in range(len(s1)):  
                if s1[i] == s2[i2]: 
                    i2+=1 
                    continue 
                else:  
                    s3.append(s1[i]) 
                if s2[i2] == "".join(s3):
                    ky = s2[i2]
                    for j,k in replables.items():
                        ky = ky.replace(k,j)
                    c_map[ky] = " ".join(s3)
                    i2+=1
            return c_map 
        for i, pt in self.patterns.items():
            patt = pt
            canonical_fun = None
            formater_fun = None
            text_ = otext
            priority_ = False
            can_map = {}
            if isinstance(pt, dict):
                patt = pt["pattern"]
                canonical_fun = pt.get("canonical")
                formater_fun = pt.get("formater")
                priority_ = pt.get("priority", False)
    
            text = otext
            if formater_fun:
                text, text_,rpl = formater_fun(otext)
                logger.info(u"result from fomater_function %s, %s, %s", text_, text, rpl)
                can_map = formatter_word_match(text_,text, rpl)
                logger.info(u"canonical map: %s", can_map)
            patt = r"(^|\s){}(\s|$)".format(patt)
            ls = re.finditer(patt, otext)
            for t in ls:
                word = text[t.span()[0]:t.span()[1]]
                si, ei = t.span()[0], t.span()[1]
                word = word[1:] if word[0] == " " else word
                canonical = word
                if canonical_fun:
                    canonical = canonical_fun(word)
                fl = False
                for z in pre:
                    if (z["word"] in canonical or z["word"] in word) or ( word in z["word"] or canonical in z["word"]):
                        fl= True
                        if z.get("priority", False):
                            continue
                        if priority_:
                            if len(z["word"]) < len(word):
                                z["word"] = can_map.get(canonical, word)
                            z["probs"] = {i: 1.0}
                            z["priority"] = True
                        else:
                            z["probs"][i] = 1.0
                        z["class"] = max(z["probs"], key=z["probs"].get)
                        z["canonical"] = canonical
                        
                        break

                if not fl:
                    ob = {
                        "index": ind,
                        "word": can_map.get(canonical, word),
                        "class": i,
                        "probs": {},
                        "canonical": canonical,
                        "priority": priority_,
                        "start_index": si,
                        "end_index": ei
                    }
                    ind = ind + 1

                    ob["probs"][i] = 1.0
                    
                    pre.append(ob)
                """
                if canonical:
                    ind = ind + 1
                    ob = {
                        "index": ind,
                        "word": canonical,
                        "class": i,
                        "probs": {},
                        "actual_word": word
                    }
                    ob["probs"]["{}_canonical".format(i)] = 1.0
                    pre.append(ob)
                """

        return pre

    def get_tags(self, sent):
        resp = requests.get(u"{}?q={}".format(self.host, sent))
        if resp.status_code < 400:
            resp = resp.json()
            rr = self.regex_formatter(sent, resp)
            resp = resp+rr
            return resp, True
        else:
            logger.error("Got response %s with status code %s from %s", resp.status_code, resp.content, self.host)
            return [], False


class NlpModel(object):
    def __init__(self, name, db_name, match_score=1, penalty=1, no_deletes=3, max_threshold=0.7, state_boost_factor=1.2, ner_enabled=False, w2v_model="default_model", lexicon="en", **kwargs):
        self.name = name
        self.db_name = db_name
        self.match_score = match_score
        self.penalty = penalty
        self.no_deletes = no_deletes
        self.max_threshold= max_threshold
        self.state_boost_factor= state_boost_factor
        self.w2v_model = w2v_model
        self.lexicon = lexicon
        self.ner_enabled = ner_enabled

        self._model = self._load_vector(self.db_name, self.w2v_model)
        
        self._entity = NlpDict("{}_entity".format(self.name), key_types = [unicode], key_names = ['entity'], DB_NAME= db_name, **kwargs)
        self._entity_desc = NlpDict('{}_entity_desc'.format(self.name), key_types = [unicode], key_names = ['entity'], DB_NAME = db_name, **kwargs)
        """
        Caching table
        """
        self._response_cache = JsonBDict('{}_response_cache'.format(self.name), key_types = [unicode], key_names = ['query'], DB_NAME = db_name, **kwargs)

        """
        Document frequency table
        """
        self._doc_frequency = JsonBDict('{}_doc_frequency'.format(self.name), key_types = [unicode], key_names = ['doc'], DB_NAME = db_name, **kwargs)
        self._spell_corrected = JsonBDict('{}_spell_corrected'.format(self.name), key_types = [unicode], key_names = ['doc'], DB_NAME = db_name, **kwargs)
        #self._dict = JsonBDict('{}_dict'.format(self.name), key_types = [unicode], key_names = ['doc'], DB_NAME = db_name, **kwargs)
        
        self.custom_weighted_words = {}
        """
        try:
            #self._ner_model = NlpTagger(name, "nermodel")
            self._ner_model = NlpTagger(name, "en_core_web_sm")
        except Exception as e:
            logger.error("Error in loading NER model, possible version mis-match?")
            hp.print_error(e)
            self._ner_model = None
        """
        self._ner_model = NerParser()
        self._spell_checker = LevenshteinStringMatch(self._doc_frequency, language=lexicon)
        

    def add_default_weighted_words(self, weight=0.5):
        dfw = ["<start>", "<end>"]
        self.custom_weighted_words.update(dict( map(lambda x: (x, weight) , dfw) ))

    def normalize(self, sen):
        if isinstance(sen, list):
            sen = " ".join(sen)
        sen = (sen or u"" ).lower()
        sen = u' '.join(sen.replace(',',' ').replace('.', ' ').replace('!',' ').replace('?',' ').replace('"','').replace(u"\u2019","'").replace("'s ","' s ").replace("' "," ").replace("'","").replace('&', ' and ').replace('|',' or ').replace('(',' ').replace(')',' ').replace('{',' ').replace('}',' ').replace('*', ' ').replace(':',' . ').replace('\s',' ').replace('[',' ').replace(']',' ').replace('^', '').replace('$', ' dollars').replace('#', ' number ').replace('@', ' at the rate ').replace('%', ' percent ').replace("\\"," ").replace(';', ' ').replace(r'/', ' ').replace('<', ' ').replace('>',' ').replace('~', ' ').replace('`','').replace('+', ' plus ').split())
        return sen.strip()

    def splice(self, sen, st = None, en = None):
        # sp = re.findall("[\w\-]+", sen)
        sp = sen.split()
        # sp = re.findall(u"[\w\-]+", sen)
        return sp[st:en]
        #return ' '.join(sp[st:en])

    def list_entities(self, sent):
        wrd = re.findall("__\w+__", sent)
        return filter(lambda x: x in self._entity_desc, wrd), filter(lambda x: x not in self._entity_desc, wrd)

    def find_alternates(self, word, threshold= 0.4):
        #print "-------------------"
        #print word
        #print "-------------------"


        yield word, 1.
        done_entities = {}
        for wr, sc in sorted(self._entity.get(word, {}).get("alternate_words",{}).items(), key=lambda x: x[1], reverse=True):
            if sc < threshold:
                raise StopIteration
            yield wr, sc
        else:
            for x,y in self.similar_words(word):
                if y < threshold:
                    raise StopIteration
                print x, y
                yield x, y
        raise StopIteration

    def correct_spelling(self, word):
        if word in self._spell_corrected:
            for i, j in self._spell_corrected[word].iteritems():
                #print word, "corrected to ::", i, "-----------------"
                yield i, j
        else:
            dt = self._spell_checker.similar_words(word)
            self._spell_corrected[word] = dt
            for i, j in dt.iteritems():
                #print word, "corrected to ::", i, "-----------------"
                yield i, j

    def correct_spelling_sentence(self, sen):
        if isinstance(sen, (unicode, str)):
            sen = self.splice(sen)
        for ind,i  in enumerate(sen):
            for w, sc in self.correct_spelling(i):
                if w != i:
                    sen[ind] = w
        return u" ".join(sen)


    def similar_words(self, word):
        #for i, j in self.correct_spelling(word):
        #    yield i, j
        if self._model:
            try:
                smlr = self._model.most_similar(positive=[word], topn=5)
                for i in smlr:
                    yield unicode(i[0]).lower(), i[1]
            except KeyError as ex:
                pass
        
        raise StopIteration

    #def get_prob(self, similarity):
    #    return similarity

    def normalized_entity_values(self, en, words, no_deletes = None):
        def prune(d):
            if not d:
                return 0
            _max_match = {}
            for _v, _w in d.iteritems():
                _max_match[_v] = sum(1 if __v in words or self.is_entity(__v) else -1 for __v in _v)
            mm = max(_max_match.values())
            for _v, _w in _max_match.iteritems():
                if _w < mm:
                    d.pop(_v, None)
            return mm
        vals = self._entity_desc[en].get("entity_values", {})
        jwords = ' '.join(words)
        norm_vals = {}
        mpe = {}
        for val, w in vals.iteritems():
            ents = re.findall("__\w+__", val)
            vs = val.split()
            g = set(vs) - set(ents)
            if ( g and sum(_ in words or _ in ents for _ in g) < len(g)*0.3) or (no_deletes and g and sum(_ not in words and _ not in ents for _ in g) > no_deletes):
                continue
            if not ents:
                norm_vals[val] = w
                continue
            nrm = { val: w }
            for _e in ents:
                tmp = {}
                _nrm, mcw = self.normalized_entity_values(_e, words, no_deletes)
                logger.info("Before pruning pruning: %s -> %s -> %s -> %s", en, val, _e,  _nrm)
                mm = prune(_nrm)
                logger.info("After pruning pruning less than %s: %s -> %s -> %s -> %s", mm, en, val, _e,  _nrm)
                logger.info("Before adding replacements: %s -> %s -> %s -> %s", en, val, _e,  nrm)
                for _en, we in nrm.iteritems():
                    if en == _e:
                        continue
                    for _v, _w in _nrm.iteritems():
                        if not any((True  if __z in _v else False) for __z in words):
                            continue
                        tmp[_en.replace(_e, _v)] = we+_w
                        _o = {_e: _v}
                        _o.update(mpe.get(_en, {}))
                        mpe[_en.replace(_e, _v)] = _o
                    else:
                        norm_vals[_en] = we
                nrm = tmp
                prune(nrm)
                logger.info("After adding replacements: %s -> %s -> %s -> %s", en, val, _e,  nrm)
            norm_vals.update( nrm )
            prune(norm_vals)
        return norm_vals, mpe
    
    def ms(self, word, is_penalty=False, penalty_value=None, match_score_value=None):
        if word in self.custom_weighted_words:
            return self.custom_weighted_words[word]
        return (penalty_value or self.penalty) if is_penalty else (match_score_value or self.match_score)

    def get_matches(self, en, words, _temp_entities=None, threshold=0.7, penalty_value=1, match_score_value=1):
        _temp_entities = _temp_entities or {}
        if len(re.findall("__\w+__", en)) > 0:
            # nre, mpw = self.normalized_entity_values(en, words)
            nre, mpw = _temp_entities.get(en, ({}, {}))
            en_v = FilterDict(nre, score_func = self.ms, match_score_value= match_score_value, penalty_value = penalty_value)
   
            for aw, w, v, ms in en_v.find_match(words):
                yield w, v, ms, mpw.get(w)
            raise StopIteration 
        elif len(re.findall("\*\*\w+\*\*", en)) > 0:
            yield en.replace("**", ""), self.ms(en, match_score_value= match_score_value, penalty_value = penalty_value) ,1, None
            raise StopIteration 
        elif len(re.findall("_\w+_$", en)) > 0:
            en_v = FilterDict(_temp_entities.get(en, ({}, {}))[0], score_func = self.ms, match_score_value= match_score_value, penalty_value = penalty_value)
            for aw, w, sc, c in en_v.find_match(words):
                yield w, sc * 0.5, c, None
            raise StopIteration 
        
        def find_match_score( wd, wd2):
            exp = u"^"+wd
            mc = re.match(exp, wd2)
            if not mc:
                wd, wd2 = wd2, wd
                exp = u"^"+wd
                mc = re.match(exp, wd2)

            if not mc:
                return 0.
            ms = wd2[mc.span()[0]: mc.span()[1]]
            if ms:
                klen = len(wd2)*1.
                return min(len(wd)/klen, klen/len(wd))


        for w, s in self.find_alternates(words[0], threshold):
            if w == en:
                yield w, s*self.ms(w, match_score_value= match_score_value, penalty_value = penalty_value), 1, None
                break
        else:
            msc = find_match_score(words[0], en)
            if  msc> 0.7:
                #self.add_words(words[0], en, weight=msc, mkey="alternate_words")
                yield en, msc*self.ms(en, match_score_value= match_score_value, penalty_value = penalty_value), 1, None

   
    def similarity(self, word1, words, _temp_entities, threshold= 0.4, penalty_value=1, match_score_value=1):
        # TODO: Need to implement
        for w, s, mc, _ in self.get_matches(word1, words, _temp_entities, threshold, penalty_value, match_score_value):
            if s < threshold:
                raise StopIteration
            yield w, s, mc, _
            #print ("Match found :: ", word1, w, "Match score ::", s)

        raise StopIteration
   
    def match_phrase(self, sen, phrase, sti, endi, score, _temp_entities, no_deletes=3, penalty=0, boosted_keys=None, matched_words=None, max_threshold=0.7, state_boost_factor=1.2, penalty_value=1, match_score_value=1):
        _temp_entities = _temp_entities or {}
        def max_match(l):
            if len(l) == 0:
                return None

            if len(l) == 1:
                return l[0]
            max_mat =sorted(l, key=lambda x: x[2])[-1]
            mats = filter(lambda x: x[2] == max_mat[2] and x[0] != max_mat[0], l)
            if len(mats) <= 1:
                return max_mat
            else:
                tt = list(map(lambda x: x[0], mats))
                tt.append(max_mat[0])
                max_mat[0] = tt
                return max_mat
        l = []
        if len(sen) == 0 or "word_{}".format(sti) not in phrase or sti > endi:
            if len(sen) > no_deletes or phrase["number_of_words"]-sti > no_deletes:
                return None
            
            if len(matched_words.keys()) > 0 and not any(matched_words.values()):
                return None

            if len(sen):
                logger.info("Didn't get any match, previous penalty: %s", penalty)
                words_missing = sen
                penalty = penalty + sum(list( self.ms(z, is_penalty=True, match_score_value= match_score_value, penalty_value = penalty_value) for z in words_missing ))
            elif phrase["number_of_words"]-(sti+1) > 0 :
                logger.info("Penalty before words_missing: %s", penalty)
                words_missing = list( phrase.get("word_{}".format(s), "")  for s in range(sti, phrase["number_of_words"]) )
                logger.info("Words missing: %s", words_missing)
                penalty = penalty + sum(list( self.ms(z, is_penalty=True, match_score_value= match_score_value, penalty_value = penalty_value) for z in words_missing ))
            logger.info("Penalty before missing words is %s, phrase %s", penalty, phrase.get("keys"))
            logger.info("Matched words: %s", matched_words)
            penalty += (sum(v is None for k, v in matched_words.iteritems())*penalty_value)
            logger.info("Score is: %s, penalty is :%s, phrase: %s", score, penalty, phrase.get("keys"))
            phs = ((match_score_value or self.match_score)+score)-penalty
            phs_ratio = (phs / (sti+1) )*100
            if "keys" in phrase and phs_ratio > 10:
                for k, sc in sorted( phrase["keys"].items(), key=lambda x: x[1], reverse=True):
                    bs = boosted_keys.get(k, 1)
                    if bs == None:
                        continue
                    l.append( [ k, phrase["entity"], bs*phs, matched_words, phs_ratio] )
                return max_match(l) 
                #yield phrase["entity"], k, (score*sc)-penalty, matched_words
            return None
   
        word = phrase.get("word_{}".format(sti))
        match = False
        for w, s, mc, mpw in self.similarity(word, sen,_temp_entities, threshold = max_threshold):
            # w is the simlar word
            # s is the score
            # mc ?
            # mpw is it is a matched entity
            match = True
            if word != w:
                word = word.replace("__", "")
                matched_words[word] = self.canonicals.get(word) or w
                
            if mpw:
                matched_words.update( dict( (i.replace("__", ""), mpw[i])  for i in mpw) )
                logger.debug("Matched words: %s", matched_words)
            logger.debug("Got similar match: %s->%s, phrase: %s", w, s, phrase.get("keys"))
            z =  self.match_phrase(sen[mc:], phrase, sti+1, endi, score+s, _temp_entities, no_deletes=no_deletes, penalty=penalty, boosted_keys=boosted_keys, matched_words=matched_words, max_threshold=max_threshold, state_boost_factor=state_boost_factor, penalty_value=penalty_value, match_score_value=match_score_value)
            if z:
                l.append(z)

        if not match:
            if no_deletes !=0:
                logger.debug("Got no match for word ahead %s", word)
                z =self.match_phrase(sen, phrase, sti+1, endi, score, _temp_entities, no_deletes=no_deletes-1, penalty=penalty+self.ms(word, is_penalty=True, match_score_value= match_score_value, penalty_value = penalty_value), boosted_keys=boosted_keys, matched_words=matched_words, max_threshold=max_threshold, state_boost_factor=state_boost_factor, penalty_value=penalty_value, match_score_value=match_score_value)
                if z:
                    l.append(z)
                else:
                    logger.debug("Got no match for word before %s", word)
                    z1 =  self.match_phrase(sen[1:], phrase, sti, endi, score,_temp_entities,  no_deletes=no_deletes-1, penalty=penalty+ self.ms(word, is_penalty=True, match_score_value= match_score_value, penalty_value = penalty_value), boosted_keys=boosted_keys, matched_words=matched_words, max_threshold=max_threshold, state_boost_factor=state_boost_factor, penalty_value=penalty_value, match_score_value=match_score_value)
                    if z1:
                        l.append(z1)
        return max_match(l)	

    def _clear_cache_by_state(self, state_list = None):
        state_list = hp.make_list(state_list or [])
        for k, v in self._response_cache.iteritems():
            logger.info("Clearing response: %s", v['response'])
            for m in v['response']:
                do_break = False
                for n in m:
                    if n and (n[0] in state_list or '__{}__'.format(n[0]) not in self._entity_desc):
                        logger.info("Entity %s is not there in entity_desc... deleting", n[0])
                        self._response_cache.pop(k)
                        do_break = True
                        break
                if do_break:
                    break

    def corrector(self, x):
        return self.correct_spelling_sentence(self.normalize(x))

    def clear_cache(self, parser_task = None, *args, **kwargs):
        l = len(self._response_cache)
        i = 1
        if 'filter_by' in kwargs and 'sentence' in kwargs['filter_by']:
            st = kwargs['filter_by']['sentence']
            if isinstance(st, (unicode, str)):
                if st.startswith('~'):
                    st = '~' + self.corrector(st)
                else:
                    st = self.corrector(st)
            if isinstance(st, (list, tuple)):
                new_list = map(self.corrector, st)
                st = list(set(st + new_list))
            kwargs['filter_by']['sentence'] = st
            hp.logger.info("Corrected and normalized sentence is: %s", st)
        for k, v in self._response_cache.iteritems(*args, **kwargs):
            s = v.pop('sentence', k)
            c = v.pop('count', 0)
            pr = v.pop('response', None)
            if not parser_task:
                r = self.parse(
                    s, boosted_keys = v.pop('boosted_keys', None), 
                    exclude = v.pop('exclude', None), 
                    _fresh = True, 
                    prev_count = c, 
                    **v
                )
                if r == pr:
                    logger.info("Response for utterance %s is: %s", s, r)
                else:
                    logger.info(
                        "Response for utterance %s changed from: %s to %s", s, self._state_from_response(pr), self._state_from_response(r)
                    )
                logger.info("Finished %s out of %s", i, l)
            else:
                v.update({
                    "boosted_keys": v.get('boosted_keys', None), 
                    "exclude": v.get('exclude', None), 
                    "_fresh": True, 
                    "prev_count": c, 
                })
                parser_task.apply_async(
                    args = [self.name, self.db_name, self.ner_enabled, l - i, s],
                    kwargs = v,
                    queue = 'priority'
                )
            i += 1
        return i - 1

    def _state_from_response(self, response):
        if not response:
            return response
        if not response[0]:
            if not (len(response)>1 and response[1]):
                return response[0]
            return response[1][0]
        return response[0][0]

    def _to_cache_id(self, sen, boosted_keys = None, exclude = None, **params):
        if boosted_keys is None: boosted_keys = {}
        if exclude is None: exclude = []
        id_ = sen.split() + ['{k}.{v}'.format(k = k, v = sorted(v.iteritems()) if isinstance(v, dict) else v) for k, v in sorted(boosted_keys.iteritems()) if v] + exclude + ['{k}.{v}'.format(k = k, v = sorted(v.iteritems()) if isinstance(v, dict) else v) for k, v in sorted(params.iteritems()) if v]
        id_ = hp.normalize_join(*id_, retain_unicode = True)
        i_ = hp.make_uuid3(id_)
        logger.info("Making id for %s: %s", id_, i_) 
        return i_

    def faq_setof_phrase_score(self, sen, partial_match, k, kdocs, idf, matched_words, boosted_keys=None, state_boost_factor=1.2, customer_state_count = -1):
        respk = []
        ktf_idf = 0
        customer_state_count = len(self._entity_desc) if customer_state_count == -1 else customer_state_count 
        logger.info("Customer state count for conversation %s in enterprise %s is : %s", self.name, self.db_name, customer_state_count)
        customer_state_count = max(customer_state_count, 1)
        for doc, we, docO in kdocs:
            dtf_idf = 0
            docl = doc.split()
            mc = 0
            mws = []
            me = []
            for mw in docO.get("entity_list", []):
                if mw.replace("__", "") in matched_words:
                    wd = matched_words.get(mw.replace("__", ""), "")
                    #mws.append(wd.split())
                    mws.append(partial_match.get(wd, "").split())
                    me.append(mw)

            mws = list(set(sum(mws, [])))
            sen_ents = me + list(filter(lambda x: x not in mws ,sen))
            logger.debug("Sentence to compare: %s with %s, matched_words: %s", doc, sen_ents, mws)
            for wd in sen_ents:
                tmc = docl.count(wd)
                if tmc:
                    docl.remove(wd)
                    dtf_idf += (float(tmc) * idf[wd] *  ( len(matched_words.get(wd.replace("__", ""), "").split()) or 1.0 ) )
                else:
                    dtf_idf -= (idf[wd] *  ( len(matched_words.get(wd.replace("__", ""), "").split()) or 1.0 ) )
                mc += tmc
            for ed in docl:
                dtf_idf -= math.log(customer_state_count/float(self._doc_frequency.get(ed,{}).get('frequency', 0.1)), customer_state_count)

            respk.append((k, doc, (dtf_idf * state_boost_factor if k in boosted_keys else dtf_idf), matched_words, mc/len(sen_ents)))
            """
            [ phrase key (customer state), input phrase, match score, matched_words {entity: word}, phrase match ratio]
            """
            logger.debug("Phrase faq match score ::  %s :: %s :: %s", k, doc, dtf_idf)
            ktf_idf += dtf_idf
        return sorted(respk, key= lambda x: x[2],  reverse=True), ktf_idf
    

    def _find_elimination(self, phrase, sen, matched_entities, last_best_score = 0, match_score_value = None, penalty_value = None):
        ms = sum(self.ms(word, match_score_value= match_score_value) for word in sen)
        ens =  set(list(re.findall(r"\b_\w+_\b", " ".join(phrase))) + list(re.findall(r"\b__\w+__\b", " ".join(phrase))))
        ophrase = list(set(hp.copyof(phrase)))
        ephrase = list(set(hp.copyof(phrase)))
        logger.debug("Existing Entites in phrase are :: %s, :: %s", ens, matched_entities.keys())
        for e in ens:
            if e in matched_entities:
                try:
                    ophrase.remove(e)
                    ephrase.remove(e)
                except ValueError as er:
                    logger.warning("Probable issue: not found {} in phrase {}".format(e, phrase))
                ephrase.extend(matched_entities[e])

        ms_kn = sum(self.ms(word, match_score_value= match_score_value) for word in list(set(sen).intersection(set(ephrase))))
        ps_kn = sum(self.ms(word, match_score_value= match_score_value, is_penalty=True, penalty_value=penalty_value) for word in list( (set(ophrase) - set(sen)) ))
        #logger.info("max score :: %s, known penalties :: %s, last_best_score :: %s", ms_kn, ps_kn, last_best_score)
        if (ms_kn - ps_kn) < last_best_score:
            return False
        return ms_kn - ps_kn

    def faq_match(self, sen, phrases, boosted_keys=None, _temp_entities=None,state_boost_factor=1.2, match_score_value= None, penalty_value = None, customer_state_count=-1, prev_matched = None, no_deletes = None):
        k_phmap = {}
        _temp_entities = _temp_entities or {}
        matched_entities = []
        prev_matched = prev_matched or {}
        last_best_score = -1e10
        last_best_match = None
        for i in phrases:
            s = self._find_elimination(i.split(), sen, prev_matched, last_best_score = last_best_score, match_score_value = match_score_value, penalty_value = penalty_value)
            if s != False:
                last_best_score = s
                last_best_match = i
                for k, w in phrases[i].get("keys", {}).iteritems():
                    if k not in k_phmap:
                        k_phmap[k] = []
                    k_phmap[k].append((i, w, phrases[i]))
                matched_entities = matched_entities+phrases[i].get("entity_list", [])
        logger.info("Best match from find elimination: %s (%s)", last_best_score, last_best_match)
        logger.info("No of key sets are in the faq_search %s %s", len(k_phmap), k_phmap.keys());
        customer_state_count = max(len(self._entity_desc) if customer_state_count < 0 else customer_state_count, 0.1)
        matched_entities = list(set(matched_entities))
        matched_words = {}
        partial_match = {}
        sttt = time.time()
        logger.info("Start to get matched entities: %s", sttt)
        for i in matched_entities:
            nre, mpw = self.normalized_entity_values(i, sen, no_deletes)
            _temp_entities[i] = (nre, mpw)
        logger.info("Time to get normalized entities: %s", time.time() - sttt)

        for ind in range(len(sen)):
            for i in _temp_entities:
                ner, mpw = _temp_entities[i]
                en_v = FilterDict(ner, score_func = self.ms, match_score_value= match_score_value, penalty_value = penalty_value)
                for aw, w, v, mc in en_v.find_match(sen[ind: ]):
                    matched_words[i] = w
                    partial_match[w] = aw
                    for k1, v1 in mpw.get(w, {}).iteritems():
                        if k1 != i:
                            matched_words[k1] = v1

        logger.info("Finied time to get temp entities: %s", time.time() - sttt)
        _temp_entities = { i: _temp_entities.get(i, matched_words[i]) for i in matched_words }
        matched_entities = list(matched_words.keys())
        sen_ents = sen + matched_entities
        logger.info("Finied time to get matched entities: %s", time.time() - sttt)
        df = [(i, float((self._doc_frequency.get(i) or {}).get("frequency", 0.))) for i in sen_ents]
        logger.info("document frequency ::  %s", df)

        # df = list((i, (self._doc_frequency.get() or {}).get("frequency", 0)) for i in doc_wc_sen)
        idf = dict((i[0], math.log(customer_state_count/(i[1] or 0.1), customer_state_count*0.5)) for i in df)
        # 0. to 1.
        logger.info("Inverse document frequency ::  %s", idf)

        
        resp = []
        for k, kdocs in k_phmap.iteritems():
            respk, ktf_idf = self.faq_setof_phrase_score(sen, partial_match, k, kdocs, idf, dict( (i.replace("__", ""), matched_words[i]) for i in matched_words), boosted_keys, state_boost_factor, customer_state_count)
            ktf_idf = (ktf_idf * state_boost_factor) if k in boosted_keys else ktf_idf
            resp.extend([list(r) + [ktf_idf] for r in respk])
            logger.info("Key set faq match score for customer_state '%s' == %s %s", k, ktf_idf, map(lambda x: x[0], kdocs))

        
        #for res in sorted(sum(list(map(lambda x: x[1], resp)), []), key= lambda x: x[2], reverse=True):
        for res in sorted(resp, key=lambda x: (x[2], x[5]), reverse=True):
            yield res[0], res[1], res, _temp_entities

            logger.info(u"Yield from faq search ::  %s :: %s :: %s", res[0], res[1], res[2])

        """
        index = 0
        while True: 
            for res in sorted(sum(list(map(lambda x: x[1][index:index+3], resp)), []), key= lambda x: x[2], reverse=True):
                yield res[0], res[1], res
                logger.info("Yield from faq search ::  %s :: %s :: %s", res[0], res[1], res[2])
                logger.info("-----------------------------------")
            else:
                break
            index+=3
        """
            
        
    def parse(self, _sen, no_deletes= 3, boosted_keys= None, exclude= None, match_score=1, penalty=1, max_threshold=0.7, state_boost_factor=1.2, search_limit = None, _fresh=False, prev_count = 0, _faq_states=None, customer_state_count=-1, match_limit=10, _disallow_spell_check=False, _allow_unsure_direct=False, **kwargs):
        """
        retriving from cacher
        """
        def _correct_spell(sen):
            for ind,i  in enumerate(sen):
                for w, sc in self.correct_spelling(i):
                    if w != i:
                        #matched_entities[w] = [i]
                        sen[ind] = w
                        #matched_entities[i] = matched_entities.get(i, [])+[w]
                        break
            # Return sen as a list and sen_corrected as a sentence
            sen = ' '.join(sen)
            return self.splice(sen), sen

        logger.info("Received sentence to parse: %s", _sen)
        sen = self.normalize(_sen)
        logger.info("Received sentence after normalization: %s", sen)
        sen = self.splice(sen)

        sen, sen_corrected = _correct_spell(sen)
        logger.info("Received sentence after correction: %s", sen_corrected)

        params = {
            "no_deletes": no_deletes,
            "match_score": match_score,
            "penalty": penalty,
            "max_threshold": max_threshold,
            "state_boost_factor": state_boost_factor,
            "_allow_unsure_direct": _allow_unsure_direct
        }
        if isinstance(boosted_keys, list):
            boosted_keys = dict( (i, self.state_boost_factor) for i in boosted_keys )
        cache_id1 = self._to_cache_id(sen_corrected, boosted_keys, exclude, **params)
        cache_id2 = self._to_cache_id(sen_corrected, None, None, **params)
        boost = boosted_keys or {}
        exc = exclude or {}
        res_ch = self._response_cache.get(cache_id2)
        if res_ch and not _fresh:
            # we take a combination of the faq states and recognized states
            reps = res_ch['response'][0] + res_ch['response'][1]
            # If none of the possible options are in the boosted states, then send the cache2 which is without boosted states or exclude
            if not reps or not reps[0] or (not (exc or boost)) or (reps[0][0] not in exc and not any((z[0] if isinstance(z[0], (str, unicode)) else z[0][0]) in boost for z in reps)):
                self._response_cache.iadd(cache_id2, "count", 1, 0)
                logger.info("Responded from cache without boost....................")
                return res_ch["response"]
        res_ch = self._response_cache.get(cache_id1)
        if res_ch and not _fresh:
            # we take a combination of the faq states and recognized states
            reps = res_ch['response'][0] + res_ch['response'][1]
            pboost = res_ch.get('boosted_keys') or {}
            # if the recognized state is in the boosted state or if the previous boost is exactly the same as the other
            if reps[0][0] in boost or not(set(pboost) - set(boost)):
                self._response_cache.iadd(cache_id1, "count", 1, 0)
                logger.info("Responded from cache with boost....................")
                return res_ch["response"]
        boosted_keys = boosted_keys or {}
        phrases = {}
        ner_data = {}
        ents = []
        matched_entities = {}
        self.canonicals = {}
        if self.ner_enabled:
            resp, status= self._ner_model.get_tags(sen_corrected)
            if status:
                for w in resp:
                    for cl, pr in w["probs"].items():
                        nerk = "_{}_".format(cl.lower())
                        dm = ner_data.get(nerk, [{}, {}])[0] 
                        dm.update({w["word"]: float(pr)})
                        ner_data[nerk] = [ dm, {}]
                        ents.append("_{}_".format(cl.lower()))
                        if w.get("canonical"):
                            self.canonicals["_{}_".format(cl.lower())] = w["canonical"]
                        matched_entities["_{}_".format(cl.lower())] = w["word"].split(" ")
            
        _temp_entities = ner_data

        def add_to_matched(sen, entity, phrase):
            ps = phrase.split()
            m = map(lambda hj: ' '.join(hp.make_list(matched_entities.get(hj, hj))), ps)
            ps = (' '.join(m)).split()
            f = filter(lambda hj: hj in sen, ps)
            if f and float(len(f))/len(ps) > 0.3:
                ents.append("__{}__".format(entity))
                matched_entities["__{}__".format(entity)] = list(set(matched_entities.get("__{}__".format(entity), []) + f))
                return True
            return False

        for p, o in self._entity.search(sen, limit = search_limit):
            for i in o.get("entities", {}):
                add_to_matched(sen, i, p)
            if "keys" in o:
                phrases[p] = o
        else:
            for p, o in self._entity.iteritems({"entity": u"~{}".format('|'.join(sen))}):
                for i in o.get("entities", {}):
                    add_to_matched(sen, i, p)
                if "keys" in o:
                    phrases[p] = o
        logger.info("Doing a word by word search, got %s phrases and %s entities", len(phrases), len(ents))

        logger.info("Doing a phrase search, got %s phrases and %s entities", len(phrases), len(ents))
        for se in sen:
            for p, o in self._entity.iteritems({"entity": u" {} ".format(se)}):
                for i in o.get("entities", {}):
                    add_to_matched(sen, i, p)
                if "keys" in o:
                    phrases[p] = o
        logger.info("Doing a word by word search, got %s phrases and %s entities", len(phrases), len(ents))

        def get_depended_phrases(en):
            phr = {}
            for k, ph in self._entity.iteritems({"entity": u" {} ".format(en)}):
                for i in ph.get("entities", []):
                    if u"__{}__".format(i) not in ents:
                        if add_to_matched(sen, i, k):
                            phr.update(get_depended_phrases(u"__{}__".format(i)))
                if "keys" in ph:
                    phr[k] = ph
            return phr

        logger.info("Adding, ner phrases")
        #for i in ner_data.keys():
        #    rs= get_depended_phrases(i)
        #    phrases.update(rs)
        #logger.info("Dependent phrase search after NER, got %s phrases and %s entities", len(phrases), len(ents))

        for e in set(ents):
            rs = get_depended_phrases( e )
            phrases.update( rs )
 
        logger.info("After dependent phrase search, got %s phrases and %s entities", len(phrases), len(ents))
        for i in hp.make_list(exclude):
            l = map(lambda x: x['entity'] if 'entity' in x else None, filter(lambda x: i in x.get("keys", {}), phrases.values()) )
            for z in l:
                if not z:
                    continue
                if len(phrases[z].get("keys", {}).keys()) > 1:
                    phrases[z]["keys"].pop(i, None)
                else:
                    phrases.pop(z, None)
            boosted_keys[i] = None
        resp = []
        resp_faq = []
        dindex = 0
        logger.info("Matched entities: %s", matched_entities)
        for k, i, res, te in self.faq_match(sen, phrases, boosted_keys=boosted_keys, _temp_entities=_temp_entities, state_boost_factor=state_boost_factor, match_score_value=match_score, penalty_value=penalty, customer_state_count = customer_state_count, prev_matched = matched_entities, no_deletes = no_deletes):
            dindex += 1
            phrases[i]["entity"] = i
            if dindex >= match_limit:
                break
            if "keys" not in phrases[i]:
                continue
            resp_faq.append(res)
            logger.info("examining phrase: %s", i)
            matched_words = dict( (ky.replace("__", ""), None) for ky in phrases[i].get("entity_list", []))
            z = self.match_phrase(sen, phrases[i], 0, phrases[i]["number_of_words"]+1, 0, te, no_deletes=no_deletes, penalty=0, boosted_keys=boosted_keys, matched_words=matched_words,  max_threshold=max_threshold, state_boost_factor=state_boost_factor, penalty_value = penalty, match_score_value=match_score)
            
            """
            z will be : [ phrase key (customer state), input phrase, match score, matched_words {entity: word}, phrase match ratio]
            """
            if z:
                if z[0] in list(map(lambda x: x[0], resp)):
                    for kym in resp:
                        if kym[0] == z[0] and kym[2] < z[2]:
                            kym[1:] = z[1:]
                else:
                    resp.append(z)
        if _faq_states:
            resp_faq = list(filter(lambda x: x[0] in _faq_states, resp_faq))
            mats_faq = sorted(resp_faq, key=lambda x: x[2], reverse=True)
            # removing duplicates in FAQ
            while True:
                prev = [];
                pop_n = -1
                for i, x in enumerate(mats_faq):
                    if x[0] in prev:
                        pop_n = i
                        break
                    prev.append(x[0])
                if pop_n >= 0:
                    mats_faq.pop(pop_n)
                else:
                    break
        else:
            resp_faq = []; mats_faq = [];
        mats = sorted(resp, key=lambda x: x[2], reverse=True)
        # This is the case where we get multi-match from the full parsing
        if mats and mats[0] and isinstance(mats[0][0], list):
            mats_faq = [([_] + mats[0][1:]) for _ in mats[0][0]]
            resp = None

        if not _allow_unsure_direct:
            resp =  (mats[:1], mats[1:] + mats_faq) if resp else (([], mats_faq) if (len(mats_faq) > 1) else (mats_faq[:1], mats_faq[1:]))
        elif len(mats) > 1:
            same_mats = list(filter(lambda x: x[2] == mats[0][2], mats))
            other_mats = list(filter(lambda x: x[2] != mats[0][2], mats))
            resp = (same_mats, other_mats+mats_faq)
        elif not mats:
            resp = (mats_faq[:1], mats_faq[1:])
        else:
            resp = (mats, mats_faq)
    
        #resp =  (mats[:1], mats[1:] + mats_faq) if resp else ([], mats_faq)


        params.update({
            "response": resp, 
            'timestamp': hp.now(),
            "_faq_states": _faq_states,
            "customer_state_count": customer_state_count,
            "sentence": sen_corrected
        })
        logger.info("For sentence: '%s'", sen_corrected)
        logger.info("Response is:\t %s", resp)
        # we take a combination of the faq states and recognized states
        reps = resp[0] + resp[1]
        # If none of the possible options are in the boosted states, then send the cache2 which is without boosted states or exclude
        if not reps or not reps[0] or (not (exc or boost)) or (reps[0][0] not in exc and not any((z[0] if isinstance(z[0], (str, unicode)) else z[0][0]) in boost for z in reps)):
            c = self._response_cache.get(cache_id2, {})
            params.update({
                "count": c.get("count", prev_count) + 1, 
            })
            self._response_cache[cache_id2] = params
        else:
            c = self._response_cache.get(cache_id1, {})
            params.update({
                "count": c.get("count", prev_count) + 1, 
                'boosted_keys': boosted_keys or None, 
                'exclude': exclude or None,
            })
            self._response_cache[cache_id1] = params
        return resp

    def states_from_query(self, sen):
        
        def _make_phrases(phrases, wd, matchs):
            l = {}
            for ph,s in phrases.iteritems():
                for k,sc in matchs.iteritems():
                    sent = ph.replace(wd, k)
                    l[sent] = sc*s
            return l
            
        def match_phrases(phrase, no_deletes):
            mp = {}
            phrase = phrases.split()

            for i in range(len(phrase) - no_deletes):
                l = phrases[0:i]+phrases[i+no_deletes: len(phrases)]
                mp[" ".join(l)] = 0 - (no_deletes*self.penalty)
            return mp
        
        phrases = {sen: 1.}
        for i in words:
            if i in self._entities:
                phrases = _make_matches(phrases, i, self._entity.get("entities") or self._entity["alternate_words"])

        keys = {}
        for i in range(self.no_deletes+1):
            for ph,sc in sorted(phrases.items(), key=lambda x: x[1], reverse=True):
                if ph not in self._phrases:
                    mp = match_phrase(ph, i)
                    for _ph, _sc in mp.iteritems():
                        if _ph in self._phrases:
                            yield _ph, _sc
                else:
                    yield self._phrases[ph], sc


    def _train_model(self, content, model_name="default_model"):
        model = None
        with NamedTemporaryFile() as _fp:
            cnt = "\n".join(content)
            _fp.write(content)
            model= fasttext.cbow(_fp, "{}/{}/{}".format(DATA_PATH, db_name, model_name))
        return "{}.vec".format(model_name)

    def _load_vector(self, db_name, model_name="default_model"):
        try:
            KeyedVectors
        except NameError:
            return None

        model_path = "{}/{}/{}.vec".format(DATA_PATH, db_name, model_name)
        default_path = "{}/{}.vec".format( joinpath(abspath(os.curdir), "models", "data"), model_name)
        if ispath(model_path):
            return KeyedVectors.load_word2vec_format(model_path)
        elif ispath(default_path):
            return KeyedVectors.load_word2vec_format(default_path)
        return None

    def add_entity(self, name, word_weights, weight=0.5, mkey="entities", **kwargs):
        if isinstance(word_weights, (str, unicode)):
            if hp.url_validator(word_weights):
                word_weights = hp.read_text_from_url(word_weights)
            word_weights = hp.make_list(word_weights)
 
        if isinstance(word_weights, list):
            word_weights = dict((self.normalize(i), weight) for i in word_weights)
        else:
            word_weights = dict((self.normalize(i), w) for i,w in word_weights.iteritems())
        for wr, we in word_weights.iteritems():
            entys, not_def = self.list_entities(wr) 
            if not_def:
                raise EntityNotDefined("Entity/ies are not defined :: {}".format(",".join(not_def)))

            mp = {}
            if wr in self._entity:
                mp = self._entity[wr]
 
            if entys:
                mp["entity_list"] = list(set(mp.get("entity_list", []) + entys))
           
            if mkey not in mp:
                mp[mkey] = {}

            mp[mkey][name] = we if we > mp[mkey].get(name, we) else mp[mkey].get(name, we)
            mp["number_of_words"] = max(mp.get("max_words", 0),len(wr.split()))
            mp.update(kwargs)
            self._entity[wr] = mp
        
        ents_d = self._entity_desc.get("__{}__".format(name), {}).get("entity_values", {})
        ents_d.update(word_weights)
        self._entity_desc[u"__{}__".format(name)] = { "entity_values" : ents_d }

    def remove_entity(self, name):
        _name = "__{}__".format(name)
        ent = self._entity_desc.pop(_name, [])
        
        for i in ent.get("entity_values"):
            self._entity[i]["entities"].pop(name)
            if not self._entity[i]["entities"]:
                self._entity.pop(i)
            
        for z in self._entity.iteritems({ "entity": "~".format(_name) }):
            self._entity.pop(z["entity"])

    def delete_all(self):
        self._entity.clear()
        self._entity_desc.clear()
        self._doc_frequency.clear()
        #self._dict.clear()
        self._spell_corrected.clear()

    def add_words(self, name, words_or_url, weight=0.8, mkey="alternate_words", **kwargs):
        if isinstance(words_or_url, (str, unicode)) and hp.url_validator(words_or_url):
            words_or_url = hp.read_text_from_url(words_or_url)
        arg = map(self.normalize, hp.make_list(words_or_url))
       
        #self._entity[name] = kwargs 
        self.add_entity(name, arg, weight, mkey)


    def add_rm_doc_freq(self, word, doc, remove=False, remove_alpha_numeric = True):
        words = self.normalize(word)
        for word in hp.make_list(words.split()):
            if remove_alpha_numeric:
                if any(c in string.digits for c in word):
                    continue
            dc = self._doc_frequency.get(word) or {}
            dc["docs"] = hp.make_list(dc.get("docs", []))
            df = 0
            if remove:
                for i in hp.make_list(doc):
                    if i in dc["docs"]:
                        dc["docs"].remove(i)
                self._spell_corrected._delete_many(filter_by = {word: (0.0, None)  })
            else:
                if doc in self._doc_frequency:
                    # This means that the doc is actually an entity and we have to check how frequently that entity comes in other documents
                    od = self._doc_frequency[doc]
                    df = od.get('frequency', 1)
                    dc["docs"] = list(set(dc.get("docs", []) + hp.make_list(od.get('docs', []))))
                else:
                    # This is a regular document (customer state)
                    dc["docs"] = list(set(dc.get("docs", []) + hp.make_list(doc)))
            dc["frequency"] = df + len(dc['docs'])
            if word.startswith("__"):
                dc["entity"] = True
                for i1, v1 in self._doc_frequency.iteritems(filter_by = {'docs': word}, filter_by_types = {'docs': list}):
                    v1['docs'] = list(set(v1.get("docs", []) + dc["docs"]))
                    self._doc_frequency[i1] = v1
            else:
                dc["entity"] = False
            dc["length"] = len(word)
            dc["lword"] = list(word)
            dc["sword"] = " ".join(list(word))
            self._doc_frequency[word] = dc
            if word in self._spell_corrected:
                self._spell_corrected.pop(word)


    def remove_words_phrases(self, words_or_url, entities=None, **kwargs):
        if isinstance(words_or_url, (str, unicode)) and hp.url_validator(words_or_url):
            words_or_url = hp.read_text_from_url(words_or_url)
        #words = map(self.normalize, hp.make_list(words_or_url))
        words = hp.make_list(words_or_url)
        entities = hp.make_list(entities or [])
        #self._entity[name] = kwargs
        def pop_entity_values(de, i, e):
            de["entity_values"].pop(i, None)
            if len(de["entity_values"]) == 0:
                self._entity_desc.pop(e, None)
            else:
                self._entity_desc[e] = de

        for e in entities:
            for i in words:
                ei = self._entity.get(i, {})
                de = self._entity_desc.get(e, {'entity_values': {}})
                if not ei:
                    if not de:
                        continue
                    pop_entity_values(de, i, e)
                    continue
                if "keys" in ei:
                    for w in i.split():
                        self.add_rm_doc_freq(w, ei["keys"], remove=True)
                    ei["keys"].pop(e, None)
                    if not ei["keys"]:
                        self._entity[i] = ei
                    else:
                        self._entity.pop(i, None)
                    pop_entity_values(de, i, e)    
                        
                if "entities" in ei:
                    for w in i.split():
                        self.add_rm_doc_freq(w, ei["entities"], remove=True)
                    ei["entities"].pop(e, None)
                    if not ei["entities"]:
                        self._entity.pop(i, None)
                    else:
                        self._entity[i] = ei
                    pop_entity_values(de, i, e)    


    def add_phrase(self, st, phs, wt=0.8):
        if not isinstance(phs, (dict, list)):
            phs = hp.make_list(phs)
        for ph in phs:
            logger.info(u"Adding phrase: %s to %s", ph, st)
            ph = self.normalize(ph)
            par = {}
            for i in enumerate(ph.split()):
                par[u"word_{}".format(i[0])] =  i[1]
                self.add_rm_doc_freq(i[1], st, remove_alpha_numeric = False)
            if isinstance(phs, dict):
                w = phs.get(ph, wt)
            else:
                w = wt
            self.add_entity(st, ph, w, "keys", **par)

    def gen(self, coverage = 20.0, total_number = None):
        num = 0
        for k in self._entity:
            for k1, _ in self.get_variations(k, coverage):
                yield k1
                num += 1
                if total_number and num > total_number:
                    raise StopIteration

    def gen_ner(self, entity, coverage = 20.0):
        return {
            "_person_": {
                "Ananth": None,
                "Dinesh": None
            },
            "_location_": {
                "Mumbai": None,
                "Pune": None,
                "Hyderabad": None
            },
            "_number_": {
                "1": None,
                "2": None
            }
        }.get(entity)

    def is_entity(self, w):
        if w.startswith('__') and w.endswith('__'):
            return 'entity'
        if w.startswith('_') and w.endswith('_'):
            return 'ner'
        return None

    def get_variations(self, utt_split, coverage = 20.0, current_entities = None, ind = 0):
        if isinstance(utt_split, (unicode, str)):
            utt_split = utt_split.split()
        utt = ' '.join(utt_split)
        utt_split = utt.split()
        current_entities = current_entities or {}
        if not any(self.is_entity(e) for e in utt_split):
            yield ' '.join(utt_split), current_entities
            raise StopIteration
        if 0.9**ind < (1.0/coverage):
            logger.debug("Breaking from utterance: %s (%s)", utt, ind)
            raise StopIteration
        logger.debug("Split utterance is :%s", utt_split)
        for i, e in enumerate(utt_split):
            et = self.is_entity(e)
            if et:
                if et == "entity" and e not in self._entity_desc:
                    raise EntityNotDefined("Utterance '{}' has entitity {} without a description".format(utt, e))
                elif et == "ner":
                    ed = self.gen_ner(e, coverage)
                    if not ed:
                        break
                else:
                    ed = self._entity_desc[e]['entity_values'].keys()
                    logger.debug("Examining entity: %s in %s", e, utt_split)
                if ed:
                    try:
                        hp.random.shuffle(ed)
                    except KeyError:
                        pass
                for w in ed:
                    ind += 1
                    current_entities[e] = w
                    us = utt_split[:i] + [w] + utt_split[i+1:]
                    for v, e1 in self.get_variations(us, coverage, current_entities, ind):
                        logger.debug("%s, %s", ind, 0.9**ind)
                        if 0.9**ind < (1.0/coverage):
                            logger.debug("Breaking from utterance: %s", us)
                            break
                        ind += 1
                        yield v, e1
                break


if __name__ == "__main__":
    x = NlpModel("test", db_name="nlptest", state_boost_factor=1.2, w2v_model="default_model", lexicon="en", ner_enabled=True)
    x.add_entity("product_name", ["product 1", "product 2", "product 3", "product 4"])
    x.add_entity("product_name", ["product 6", "product 7", "product 8", "product 9"])
    x.add_entity("product_title", ["product 1", "product 2", "product 3", "product 4"])
    x.add_entity("product_variant", ["__product_name__ variant 1", "__product_name__ variant 2", "__product_name__ variant 3", "__product_name__ variant 4"])
    x.add_entity("product_variant_two", ["__product_variant__ test"])

    x.add_entity("test", ["testing"], 1.)
    x.add_phrase("first", "i have product with __product_name__ along with the title is __product_title__")
    x.add_phrase("first", "i need details of __product_name__")
    x.add_phrase("name", "my name is _person_")
    x.add_phrase("name", "my email is _email_")
    x.add_phrase("location", "my location is _location_")
    x.add_phrase("location", "_location_")
    x.add_phrase("vehicle_number", "my bike number is _vehicle_number_")
    x.add_phrase("first", "i need details of __product_variant__")
    x.add_phrase("mobile", "my mobile number is _phone_number_")
    x.add_phrase("datetime", "_datetime_")
    
    """
    print "norm entities 1::",  x.normalized_entity_values("__product_name__", ["looking", "product", "4"])
    print "norm entities 2::",  x.normalized_entity_values("__product_variant__", ["looking", "test", "4"])
    print "norm entities 2::",  x.normalized_entity_values("__product_variant_two__", ["looking", "product", "4", "variant", "4"])

    #x.add_phrase("first", "i need details of product 1")
    print "Query search 1 (i need details of product 1) :: {}".format( list(x.parse("i need details of product 1", _fresh=True)))
    print "Query search 2 (i nedd details of product 1) :: {}".format( list(x.parse("i nedd details of product 1", _fresh=True)))
    print "Query search 3 (my name is dinesh) :: {}".format( list(x.parse("my name is dinesh", _fresh=True)))
    print "Query search 4 (my email id is dinesh at gmail dot com) :: {}".format( list(x.parse("my email id is dinesh at gmail dot com", _fresh=True)))
    print "Query search 5 (my email id is dinesh@iamdave.ai) :: {}".format( list(x.parse("my email id is dinesh@iamdave.ai", _fresh=True)))
    print "Query search 6 (my bike number is KA02QL1234) :: {}".format( list(x.parse("my bike number is KA02QL1234", _fresh=True)))
    print "Query search 6 (my mobile number is 9700938804) :: {}".format( list(x.parse("my mobile number is 9700938805", _fresh=True)))
    """


    #for k in x.gen(5):
    #    print k
    
    """
    t = NerParser()
    print t.regex_formatter("my email id is dinesh at gmail dot com")
    print t.regex_formatter("my mobile number is 9700938804")
    print t.regex_formatter("my mobile number is 9700 9388 04")
    print t.regex_formatter("my mobile number is 97 double 0 93 double 8 04")
    
    print t.regex_formatter("my email id is dinesh at gmail dot com")
    """
    
    t = NerParser()
    print t.regex_formatter("my mobile number is 9700938804")
    print t.regex_formatter("21-04-2021 09:12pm")
    print t.regex_formatter("09:12pm")
    #print x.parse("my city is bangalore", _fresh=True)
    #print x.parse("bangalore", _fresh=True)
    print x.parse("21-03-1234", _fresh=True)
    print x.parse("21-aug-1234", _fresh=True)
    print x.parse("21-03-1934 01:22PM", _fresh=True)
    print t.regex_formatter("21-03-1934 01 . 22pm")
    print x.parse("3:40 pm")
