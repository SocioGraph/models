import os, sys 
import helpers as hp
import operator
import tempfile,re
import requests
from functools import partial
try:
    from validate_email import validate_email
except:
    hp.print_error()
    validate_email = lambda *x, **y: True
import string
import objectifier as objf
import types
from datetime import datetime
from datetime import date
from datetime import timedelta
from os.path import exists as ispath, dirname, basename, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile
DASH_DIR = dirname(dirname(abspath(__file__)))
UPLOAD_FOLDER = joinpath('static', 'uploads')
CDN_DOMAIN = '/{}'.format(UPLOAD_FOLDER)
STATIC_DIR = joinpath(DASH_DIR, UPLOAD_FOLDER)
SERVER_NAME = "{}://{}".format(os.environ.get('HTTP', 'http'), os.environ.get('SERVER_NAME', 'localhost:5000'))
hp.mkdir_p(STATIC_DIR)
import time
import base64
dtime = type(datetime.time(datetime.now()))
from pytz import timezone
from dateutil import parser as dateparser
import pgpersist as db
import qrcode
import qrcode.image.svg
import barcode
import pdf417gen
from google.auth.exceptions import DefaultCredentialsError
from jinja2 import utils
DTFMT = '%Y-%m-%d %I:%M:%S %p %z'
DFMT = '%Y-%m-%d'
TFMT = "%I:%M:%S %p %z"
UTC = timezone('UTC')
URL_MAP = {
    'http://{}'.format(os.environ.get('SERVER_NAME', 'localhost:5000')): DASH_DIR,
    'https://{}'.format(os.environ.get('SERVER_NAME', 'localhost:5000')): DASH_DIR,
    'http://{}'.format(os.environ.get('CDN_DOMAIN', os.environ.get('SERVER_NAME', 'localhost:5000'))): DASH_DIR,
    'https://{}'.format(os.environ.get(os.environ.get('SERVER_NAME', 'localhost:5000'))): DASH_DIR
}

if os.environ.get('LARGE_FILE_BASE_DIR') and os.environ.get('LARGE_FILE_BASE_URL'):
    URL_MAP.update({
        'http://{}'.format(os.environ.get('LARGE_FILE_BASE_URL')): os.environ.get('LARGE_FILE_BASE_DIR'),
        'https://{}'.format(os.environ.get('LARGE_FILE_BASE_URL')): os.environ.get('LARGE_FILE_BASE_DIR'),
        'https://s3-us-west-2.amazonaws.com/general-iamdave': os.environ.get('LARGE_FILE_BASE_DIR'),
        'http://s3-us-west-2.amazonaws.com/general-iamdave': os.environ.get('LARGE_FILE_BASE_DIR'),
        'https://general-iamdave.s3-us-west-2.amazonaws.com': os.environ.get('LARGE_FILE_BASE_DIR'),
        'http://general-iamdave.s3-us-west-2.amazonaws.com': os.environ.get('LARGE_FILE_BASE_DIR'),
    })

current_module = __import__(__name__)
"""
Validators in the format required by database models 
"""

logger = hp.get_logger(__name__)

logger.debug("Current module is %s, %s", current_module, __name__)

if os.environ.get('GOOGLE_APPLICATION_CREDENTIALS'):
    from firebase_admin import project_management as fbpm
    import firebase_admin
    try:
        firebase_admin.initialize_app()
    except (ValueError, DefaultCredentialsError) as e:
        firebase_admin = None
else:
    firebase_admin = None
    fbpm = None


class ValidatorError(hp.I2CEErrorWrapper):
    pass

class AttributeRequiredError(hp.I2CEError):
    pass

bar_code_pattern = re.compile('([^\s\w0-9.\-$/]|_)+')
def bar_code_stripper(value):
    if not isinstance(value, (str, unicode)):
        return value
    return bar_code_pattern.sub('', value.replace('_','-'))

functions_with_help = {}
default_dict = hp.default_dict
# Default make_function functions
def make_function(func, name = None, given_args = None, is_idempotent = True, *args, **kwargs):
    """
    Create a serializer for the function
    If name is not provided, then a base664 encoded name is egenrated
    given_args can be among self, instance, value, attribute
    'self' refers to the instance of the attribute
    'self.model' will refer to the instance of the model
    is_idempontent is by defult True. If set to False, the function will not get executed during a get request
    you can provide a set of args or kwargs
    NOTE: args are called after given_args
    """
    if isinstance(func, dict):
        if 'function' in func:
            return func
        else:
            raise ValidatorError("Cannot make function from dict {}".format(func))
    elif not isinstance(func, (types.FunctionType, types.LambdaType, types.TypeType, types.MethodType, partial)):
        raise ValidatorError("Cannot make function from obj {} of type {}".format(func, type(func)))
    given_args = hp.make_list(given_args or [])
    help_string = kwargs.get('help_string')
    if name:
        if hasattr(current_module, name):
            ret_func = getattr(current_module, name)
            if hasattr(ret_func, '__as_dict__'):
                return ret_func.__as_dict__
            raise ValidatorError('Attempting to redefine/override existing function {} in make_function'.format(name))
        if help_string:
            if given_args:
                help_string += ':: given_args = {}'.format(given_args)
            functions_with_help[name] = {'function': name, 'args': [], 'kwargs': {}, 'help_string': help_string}
    else:
        name = base64.b64encode('__'.join([func.__name__] + given_args + hp.make_list(args) + ['{}={}'.format(k, v) for k, v in kwargs.iteritems()]))
    add_globals = False if isinstance(func, types.TypeType) else True
    def ret_func(self, instance, attribute = None, value = None, *func_args, **func_kwargs):
        sent_args = []
        if add_globals:
            if isinstance(func, types.MethodType):
                f = func.im_func.func_globals
            elif isinstance(func, partial):
                f = func.func.func_globals
            else:
                f = func.func_globals
        else:
            f = {}
        pf = {}
        for a_, v_ in [('attribute', attribute), ('instance', instance), ('self', self), ('value', value)]:
            pf[a_] = f.get(a_)
            f[a_] = v_
        for arg in given_args:
            sent_args.append(f[arg])
        for arg in func_args:
            sent_args.append(arg)
        try:
            r = func(*sent_args, **func_kwargs)
        finally:
            for a_ in ['attribute', 'instance', 'self', 'value']:
                f[a_] = pf[a_]
                pass
        if isinstance(r, str):
            r = unicode(r)
        return r
    ret_func.__name__ = name
    ret_func.__as_dict__ = {'function': name, 'args': args, 'kwargs': kwargs, 'is_idempotent': is_idempotent}
    ret_func.is_idempotent = is_idempotent
    if not hasattr(current_module, name):
        setattr(current_module, name, ret_func)
    return ret_func.__as_dict__

def is_validator(ndict, do_raise = False):
    if not (isinstance(ndict, dict) and 'function' in ndict):
        if do_raise:
            raise ValueError("Function descriptor {} is not a function".format(ndict))
        return False
    if not hasattr(current_module, ndict['function']):
        raise ValidatorError("Unknown function {} in validators".format(ndict['function']))
    return True

def is_idempotent(ndict):
    try:
        if is_validator(ndict):
            return getattr(current_module, ndict['function']).is_idempotent
    except ValidatorError:
        return False
    return False

def execute(ndict, self, instance, attribute, value, prev_value = None, **params):
    """
    ndict is the dictionary type function
    self is an instance of the attribute
    instance is the complete object of the model as a dict
    attribute is the attribute in question
    value is the current value of that attribute
    """
    is_validator(ndict, do_raise = True)
    # Evaluating from default dict
    args = hp.make_list(ndict.get('args', []))
    kwargs = ndict.get('kwargs', {})
    if hasattr(self, 'model'):
        d = default_dict(self.model.timezone)
    else:
        d = default_dict()
    try:
        d['_enterprise_id'] = self.model.enterprise_id
        if hasattr(self.model, 'enterprise'): hp.make_all_hierarchies(d, '_enterprise', self.model.enterprise)
        d['_role'] = self.model.role
        if hasattr(self.model, 'user'): 
            hp.make_all_hierarchies(d, '_user', self.model.user)
        d['_user_id'] = getattr(self.model, "user_id")
        d['_model_name'] = self.model.name
    except (AttributeError, KeyError) as e:
        logger.debug("Executing out of context of Attribute?: %s", e)
    d['_attribute'] = attribute
    d['_value'] = value
    d['_prev_value'] = prev_value
    d.update(params)
    d.update(instance)
    args = hp.evaluate_args(hp.copyof(args), d)
    kwargs = hp.evaluate_args(hp.copyof(kwargs), d)
    try:
        return getattr(current_module, ndict['function'])(self, instance, attribute, value, *args, **kwargs)
    except Exception as e:
        logger.warning(hp.print_error())
        logger.error("""
            Error in executing function 
            ndict: {}
            attribute : {} ({})
            instance: {}
            attribute_ : {}
            value: {}
            error: {}
            """.format(
                ndict,
                self.name if hasattr(self, 'name') else '',
                self.model.name if hasattr(self, 'model') and hasattr(self.model, 'name') else '',
                instance,
                attribute,
                value,
                e.message
            )
        )
        em = 'Error in executing function {} for attribute {} in model {}: '.format(ndict['function'], attribute, self.model.name)
        e.args = (em,) + (getattr(e,'args') or tuple())
        if getattr(e, 'message'):
            e.message = em + '\n' + str(e.message)
        raise

# Some examples of make functions

def phone_number_validator(ph_no):
    """
    Accepts phone number of the type
    9999999999
    09999999999
    +919999999999
    +91-9999999999
    +91 9999999999
    (+91) 9999999999
    0091999999999
    999-999-9999
    (999) 999-9999
    999.999.9999
    +91-999-999-9999
    0091-999-999-9999
    01-888-888888
    011-888-88888
    0111-888-8888
    01111-888888
    01888888888
    01188888888
    01118888888
    """
    if not isinstance(ph_no, (str, unicode)):
        return False
    ph_no = ph_no.replace(' ','').replace('-','').replace('.','').replace('(','').replace(')','')
    if ph_no.startswith('00'):
        if len(ph_no) < 12 or len(ph_no) > 15:
            return False
    elif ph_no.startswith('0'):
        if len(ph_no) < 11 or len(ph_no) > 11:
            return False
    elif ph_no.startswith('+'):
        if len(ph_no) < 12 or len(ph_no) > 14:
            return False
    elif len(ph_no)>12 or len(ph_no) < 8:
        return False
    ph_no = ph_no.replace('+','')
    if any(not p.isdigit() for p in ph_no):
        return False
    return True

def rgb_validator_function(value):
    if not isinstance(value, (list, tuple)):
        return False
    if len(value) != 3:
        return False
    for v1 in value:
        try:
            if int(v1) > 255 or int(v1) < 0:
                return False
        except ValueError:
            return False
    return True

def to_rgb_func(value):
    if value is None:
        return None
    if not isinstance(value, (str, unicode, list, tuple)):
        raise ValueError("Error in converting to RGB")
    if isinstance(value, (str, unicode)):
        if ',' in value:
            value = map(lambda x: x.strip(), value.split(','))
        elif value.startswith('#') and len(value) == 7:
            value = value[1:]
            value = ['0x{}'.format(value[0:2]), '0x{}'.format(value[2:4]), '0x{}'.format(value[4:6])]
        elif value.startswith('0x') and len(value) == 8:
            value = value[2:]
            value = ['0x{}'.format(value[0:2]), '0x{}'.format(value[2:4]), '0x{}'.format(value[4:6])]
        elif len(value) == 6:
            value = ['0x{}'.format(value[0:2]), '0x{}'.format(value[2:4]), '0x{}'.format(value[4:6])]
        try:
            value = map(lambda x: int(x, 0), value)
        except ValueError as e:
            hp.raise_error(e)
            raise ValueError("Error in converting to RGB")
    elif not len(value) == 3 and all(isinstance(v, int) for v in value):
        raise ValueError("Error in convertint to RGB.. incorrect format")
    return value

def from_rgb_func(value):
    if value is None:
        return None
    return ','.join(map(str, value))

# Raise error
make_function(
    lambda: hp.error_raiser(
        AttributeRequiredError(
            "Attribute '{a}({key})' is required for object '{obj}'".format(
                a = self.name, key = attribute, obj = self.model.name,
            )
        )
    ),
    'raise_error',
    help_string = 'Raises error if attribute is not provided in instance'
)

def raise_error_if_none_of_func(self, instance, *args):
    if all(instance.get(i) is None for i in args):
        raise AttributeRequiredError(
            "One of a attributes '{}' is required for object '{obj}'".format(
                args, obj = self.model.name,
            )
        )

def make_app_data_func(self, instance):
    enterprise_id = instance.get('enterprise_id') or self.model.enterprise_id
    enterprise_name = (instance.get('name') or enterprise_id).replace('_',' ').title()
    res = {}
    onsh = {
        "Authorization": "Basic NzYyNmI4NDMtYjgxOC00ZGEwLThhNTAtZTk0OWM0OWMxMDEx",
        "Content-Type": "application/json"
    }
    jdata = {
        "name": enterprise_id,
        "gcm_key": "AAAATeTsjLI:APA91bFHb_Vl8u6fugMPoFqywQgY7-XeAwODWk0n1rHAKfz_Q6OIvaAsxedOAyimOTN1Nrg5UW4vJVwTPjWgivx1fmVAkMozYc5_nVbliWd03AwZiTTrACNXYe8TTof3ptm7fj_aZdxa",
        "android_gcm_sender_id": "334553189554"
    }
    st = time.time()
    logger.info("Getting one signal data for %s", enterprise_id)
    try:
        r = requests.get("https://onesignal.com/api/v1/apps", headers = onsh)
        if r.status_code == 200:
            r = r.json()
            rf = hp.make_single(filter(lambda x: x.get('name') == enterprise_id, r), force = True)
            if rf:
                res['one_signal_app_id'] = rf.get('id') or '__NULL__'
                res['one_signal_basic_web_authorization'] = rf.get("basic_auth_key") or '__NULL__'
        else:
            logger.error("Problem getting apps from one signal: %s", r.text)
            return {}
        if not res.get('one_signal_app_id'):
            logger.info("One signal app not found, creating a new one")
            r = requests.post("https://onesignal.com/api/v1/apps", headers = onsh, json = jdata)
            if r.status_code == 200:
                r = r.json()
                res['one_signal_app_id'] = r.get('id') or '__NULL__'
                res['one_signal_basic_web_authorization'] = r.get("basic_auth_key") or '__NULL__'
        logger.info("Time spent on getting one signal app %s", time.time() - st)
    except Exception as e:
        hp.print_error(e)
        return {}
    if fbpm:
        st = time.time()
        logger.info("Getting firebase data for %s", enterprise_id)
        try:
            a = hp.make_single(filter(lambda x: x.get_metadata().package_name.split('.')[2] == enterprise_id, fbpm.list_android_apps()), force = True)
            if a:
                res['mobilesdk_app_id'] = a.app_id
        except Exception as e:
            hp.print_error(e)
            logger.error("Error in retrieving android apps from firebase: %s", e)
        if not res.get('mobilesdk_app_id'):
            logger.info("Firebase app not found, creating a new one")
            try:
                a = fbpm.create_android_app('com.daveai.{}'.format(enterprise_id), enterprise_name)
                res['mobilesdk_app_id'] = a.app_id
            except Exception as e:
                hp.print_error(e)
                logger.error("Error creating android app in firebase: %s", e)
        logger.info("Time spent on getting firebase app %s", time.time() - st)
    return res

make_function(make_app_data_func,
    "make_app_data",
    given_args = ['self', 'instance'],
    help_string = "Sets up the app data required for notifications",
    is_idempotent = False
)

make_function(
    raise_error_if_none_of_func,
    'raise_error_if_none_of',
    given_args = ["self", "instance"],
    help_string = 'Raises error if attribute is not provided in instance'
)

make_function(hp.make_list_from_csv,
    'make_list_from_csv',
    given_args = 'value',
    help_string = 'Making a list from comma separated values'
)

make_function(phone_number_validator, 
    'phone_validator',
    given_args = 'value',
    help_string = 'Phone number validator'
)

make_function(
    hp.url_validator,
    'url_validator',
    given_args = 'value',
    help_string = 'Match with regular expression the attribute value args = [<attribute_name>]}'
)

make_function(
    hp.url_converter,
    'url_converter',
    given_args = 'value',
    help_string = 'Add http if url starts with www'
)


make_function(rgb_validator_function,
    'rgb_validator',
    given_args = 'value',
    help_string = 'RGB validator'
)

make_function(to_rgb_func,
    'to_rgb',
    given_args = 'value',
    help_string = 'RGB converter'
)

make_function(from_rgb_func,
    'from_rgb',
    given_args = 'value',
    help_string = 'RGB converter'
)

make_function(lambda x: all(map(phone_number_validator, x)), 
    'phone_validator_list',
    given_args = 'value',
    help_string = 'Checks if each value in the list is a phone number'
)

make_function(lambda x: int(x)>99999 and int(x) < 1000000,
    'pincode_validator',
    given_args = 'value',
    help_string = 'Pincode validator'
)

def geolocation_validator_func(value):
    pattern = r'^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$'
    # Check if the string matches the pattern
    if re.match(pattern, value):
        return True
    else:
        return False

make_function(geolocation_validator_func,
    'geolocation_validator',
    given_args = 'value',
    help_string = 'GeoLocation validator'
)

make_function(lambda x: isinstance(x, (unicode, str)) and len(x.strip()) == 4 and all(x_.isdigit() for x_ in x.strip()),
    'aus_zip_code_validator',
    given_args = 'value',
    help_string = 'Zipcode validator for australia'
)

make_function(lambda y: map(lambda x: x>99999 and x < 1000000, y),
    'pincode_validator_list',
    given_args = 'value',
    help_string = 'Checks if each value in the list is a pincode'
)

make_function(lambda x: True,
    'dummy_validator',
    given_args = 'value',
    help_string = "Dummy validator"
)

make_function(lambda x: isinstance(x, (str, unicode)),
    'text_validator',
    given_args = 'value',
    help_string = "Validated if text type"
)

make_function(
    lambda x: validate_email(x) if x not in ('', None) else True,
    'email_validator',
    given_args = 'value',
    check_mx = False,
    help_string = 'Email validator'
)

make_function(
    lambda x: all(map(lambda y: validate_email if y not in ('', None) else True, x)),
    'email_validator_list',
    given_args = 'value',
    check_mx = False,
    help_string = 'Checks if each value in the list is an email'
)


make_function(
    hp.validate_password,
    'password_validator',
    given_args = 'value',
)
make_function(
    lambda instance: hp.make_uuid(instance['email'], instance['timestamp']),
    'uuid_email_timestamp',
    given_args = 'instance'
)
make_function(lambda: hp.make_uuid(instance['email']),
    'uuid_email',
)

make_function(lambda *args: hp.make_uuid(*(instance.get(x, x) for x in args)),
    'make_uuid',
    help_string = "Takes a list of attributes within the instance and creates a uuid. args = ['attr1', 'attr2'...]"
)

make_function(lambda *args: hp.make_uuid3(*(instance.get(x, x) for x in args)),
    'make_uuid3',
    help_string = "Takes a list of attributes within the instance and creates a uuid3. args = ['attr1', 'attr2'...]"
)

make_function(lambda *args: hp.make_uuid(time.time(), *(instance.get(x, x) for x in args)),
    'make_uuid_timestamp',
    help_string = "Takes a list of attributes within the instance and creates a uuid along with timestamp. args = ['attr1', 'attr2'...]"
)

make_function(lambda *args: hp.make_uuid3(date.today(), *(instance.get(x, x) for x in args)),
        'make_uuid_date',
        help_string = "Takes a list of attributes within the instance and creates a uuid along with date. args = ['attr1', 'attr2'...]"
)

make_function(lambda *args: hp.make_uuid3(date.today(),datetime.now().hour, *(instance.get(x, x) for x in args)),
        'make_uuid_date_hour',
        help_string = "Takes a list of attributes within the instance and creates a uuid along with date and current hour. args = ['attr1', 'attr2'...]"
)

def auto_increment_func(index_length = None, justify = 'right', padding = '0'):
    i = self.model.count()
    while True:
        if not self.model.exists(**{attribute: i}):
            if index_length:
                if justify == 'right':
                    return str(i).rjust(index_length, padding)
                else:
                    return str(i).ljust(index_length, padding)
            return i
        i += 1

# use at own risk
make_function(
    auto_increment_func,
    'auto_increment',
    help_string='Auto-increment in DB. Note, does not work concurrently. Use at own risk'
)

def update_model_func(self, value, model_name = None):
    m = self.model.__class__(model_name or self.model.name, self.model.enterprise_id)
    m.pop_cache()
    return value

make_function(
    update_model_func,
    "refresh_model",
    given_args = ["self", "value"],
    help_string = "Refreshes self model or another mentioned model, typically used when the user model needs to be refreshed",
    is_idempotent = False
)

def name_to_id_func(self, instance, attribute, *attr, **kwargs):
    disallow_empty = kwargs.get('disallow_null', True)
    separator = kwargs.get('separator', '')
    compressor = kwargs.get('compressor', 'to_ascii_lower')
    if isinstance(compressor, (str, unicode)) and hasattr(hp, compressor):
        compressor = getattr(hp, compressor)
    else:
        compressor = lambda x, **y: x
    l = []
    if kwargs.get('prefix'):
        l.append(kwargs['prefix'])
    for a in attr:
        c = kwargs.get(a+'_compressor')
        if c:
            c = getattr(hp, c)
        else:
            c = compressor
        if kwargs.get('skip_empty', False) and instance.get(a) in ([None] + hp.make_list(kwargs.get(a + '_null_value', kwargs.get('null_value', None)))):
            continue
        l1 = c(
            instance.get(a),
            length = kwargs.get(a+'_length') or kwargs.get('length'),
            case = kwargs.get(a+'_case') or kwargs.get('case', 'lower'),
            join = kwargs.get(a+'_join', kwargs.get('attribute_join', False)),
            buffer = kwargs.get(a+'_buffer', kwargs.get('buffer', ' ')),
            buffer_side = kwargs.get(a+'_buffer_side', kwargs.get('buffer_side', 'left')),
            prefix = kwargs.get(a+'_prefix', ''),
            suffix = kwargs.get(a+'_suffix', ''),
            translate = kwargs.get(a+'_translate') or kwargs.get('translate')
        )
        l.append(l1)
    if kwargs.get('suffix'):
        l.append(kwargs['suffix'])
    ori = separator.join(l)
    i = 0
    if disallow_empty and not ori:
        raise ValueError("Cannot create ids for empty attributes {}".format(attr))
    if kwargs.get("is_unique", False):
        return ori or None
    o = ori
    if kwargs.get('index_length'):
        si = '0'.rjust(kwargs.get('index_length', 1), '0')
        o = ori + '{}{}'.format(separator, si)
    while True:
        d = {'{}~'.format(k): instance.get(k) for k in self.model.ids if instance.get(k)}
        d.update({attribute: o})
        if not self.model.exists(**d):
            return o
        si = str(i)
        si = si.rjust(kwargs.get('index_length', len(si)), '0')
        o = ori + '{}{}'.format(separator, si)
        i += 1


make_function(
    name_to_id_func,
    'name_to_id',
    given_args = ['self', 'instance', 'attribute'],
    help_string = "Takes a list of attributes within the instance and creates a human readable id which is unique. Note this is not good for highly concurrent inserts. args = ['attr1', 'attr2', 'attr3']"
)



def make_view_id_func(slf, attr, length=4, chars = None, init = '', final = '', make_unique = True):
    if isinstance(chars, (str, unicode)) and chars in ['letters', 'lowercase', 'uppercase', 'digits']:
        chars = getattr(string, chars)
    else:
        chars = string.letters + string.digits + '-_'
    i = 0
    r = hp.id_generator(length, chars, str(init), str(final))
    while make_unique:
        if not slf.model.exists(**{attr: r}):
            return r
        hp.random.seed()
        i += 1
        if i > 10:
            i = 0
            length += 1
        r = hp.id_generator(length, chars, str(init), str(final))
    return r

make_function(make_view_id_func,
    'make_view_id',
    given_args = ["self", "attribute"]
)

make_function(make_view_id_func,
    'make_conversation_id',
    given_args = ["self", "attribute"]
)

def make_recommendation_id_func(l=4):
    i = 0
    while True:
        r = hp.id_generator(l, string.letters + string.digits + '_-.~')
        if not self.model.exists(recommendation_id = r, recommendation_number = 0):
            return r
        hp.random.seed()
        i += 1
        if i > 10:
            i = 0
            l += 1

make_function(make_recommendation_id_func,
    'make_recommendation_id',
)
make_function(lambda i: hp.make_uuid3(i['user_id'], time.time()),
    'update_api_key',
    given_args = ['instance']
)
make_function(lambda i: hp.make_uuid(i['enterprise_id'], time.time()),
    'update_signup_api_key',
    given_args = ['instance']
)
make_function(hp.now,
   'now',
   help_string = 'Get the current datetime'
)

make_function(hp.epoch,
   'now_ctime',
   help_string = 'Get the current epoch'
)

make_function(lambda *x, **y: hp.date.today(),
   'today',
   help_string = 'Get the current date'
)

make_function(lambda *args: hp.normalize_join(*(instance.get(x, x) for x in args)),
    'normalize_join',
    help_string = 'normalize the string and replace spaces with _'
)

make_function(hp.normalize_join,
    'normalize_join_converter',
    given_args = 'value',
    help_string = 'normalize the string and replace spaces and periods with _'
)

def copy_attribute_func(instance, value, *args, **kwargs):
    default = kwargs.pop('_default', None)
    for k, v in kwargs.iteritems():
        if instance.get(k) not in hp.make_list(v):
            return default if value is None or is_validator(value) else default
    return hp.make_single([instance.get(a) for a in args if instance.get(a) is not None])


make_function(
    copy_attribute_func,
    'copy_attribute',
    given_args = ['instance', 'value'],
    help_string = "copy another attribute from the instance. args = ['attr1', 'attr2'], make a list in case of multiple args or a singleton if only one arg"
)

def deep_nested_copy(instance, *args, **kwargs):
    now_dict = instance
    for a in args:
        now_dict = now_dict.get(a, {}) or {}
    return now_dict or kwargs.get('_default', kwargs.get('default', None))

make_function(
    deep_nested_copy,
    'nested_copy_attribute',
    given_args = ['instance'],
    help_string = "copy a value from a nested_object attribute from the instance. args = ['attr1', 'nested_attribute1', .....]"
)

def attribute_mapper_func(instance, attribute, attr = None, _default = None, **kwargs):
    return kwargs.get(hp.make_single(instance.get(attr or attribute), force = True), _default or kwargs.get('default'))
    
make_function(attribute_mapper_func,
    'attribute_name',
    given_args = ['instance', 'attribute'],
    help_string = "Use a mapping from the kwargs to get a certain value depending on another attribute in the instance, args: 'attr' the value of the attribute in an instance and 'default' value, kwargs are the mapping conditions (Alias of attibute_mapper)"
)

make_function(attribute_mapper_func,
    'attribute_mapper',
    given_args = ['instance', 'attribute'],
              help_string = "Use a mapping from the kwargs to get a certain value depending on another attribute in the instance, args: 'attr' the value of the attribute in an instance and 'default' value, kwargs are the mapping conditions e.g. args: ['attribute1'], kwargs: {'val_in_attribute1': 'new val in currentattribute'... }"
)

def ensure_in_list_func(inst, value, attr_name, default = None):
    na = inst.get(attr_name, default)
    if na is None:
        return value
    if value is None:
        value = []
    if is_validator(value):
        value = []
    if not isinstance(value, list):
        return value
    if na not in value:
        value.append(na)
    return value

make_function(ensure_in_list_func,
    'ensure_in_list',
    given_args = ['instance', 'value'],
    help_string = "Ensures that the value of attr mentioned in attr_name or default is present among the values in a list"
)

def set_if_condition_func(self, instance, attr, v, value, default = None,  **kwargs):
    if self.model.run_filter(instance, self.model.attributes, filter_by = self.model.check_filter_by(self.model.attributes, kwargs)):
        if is_validator(value):
            return execute(value, self, instance, attr, v, prev_value = kwargs.get('_prev_value'))
        return value
    if is_validator(default):
        return execute(default, self, instance, attr, v, prev_value = kwargs.get('_prev_value'))
    return default

make_function(set_if_condition_func,
    'set_if_condition',
    given_args = ['self', 'instance', 'attribute', 'value'],
    help_string = "If an instance satisfies a condition then set it to 'value' provided in 'args'. The default can also be provided as second argument, the conditions are provided as keyword arguments"
)

def multi_function(self, instance, attribute, value, *args, **kwargs):
    for i, a in enumerate(args):
        if is_validator(a):
            nvalue = execute(a, self, instance, attribute, value, prev_value = kwargs.get('_prev_value'), **kwargs)
            if nvalue == None:
                break
            value = nvalue
            kwargs['_value_{}'.format(i)] = value
    return value

make_function(
    multi_function,
    'chain_functions',
    given_args = ['self', 'instance', 'attribute', 'value'],
    help_string = "Chain multiple function, where each function is a list of args, kwargs are available to the entire list",
    is_idempotent = False
)

make_function(lambda s: s.model.enterprise_id,
    'enterprise_from_model',
    given_args = 'self'
)

make_function(lambda x: isinstance(x, (float, int)) and x >= 0. and x <= 1.,
    'between_0_1',
    given_args = 'value',
    help_string = 'Validate if between 0 and 1'
)

make_function(lambda i, x, y, **d: x > hp.get_from_inst(i, y, **d) if isinstance(x, (float, int)) else hp.get_from_inst(i, x, **d) > hp.get_from_inst(i, y, **d),
    'greater_than',
    given_args = ['instance', 'value'],
    help_string = 'Validate if greater than args'
)

make_function(lambda i, x, y, **d: x >= hp.get_from_inst(i, y, **d) if isinstance(x, (float, int)) else hp.get_from_inst(i, x, **d) >= hp.get_from_inst(i, y, **d),
    'greater_or_equal',
    given_args = ['instance', 'value'],
    help_string = 'Validate if greater or equal to than args = [<min>]'
)

make_function(lambda i, x, y, **d: x < hp.get_from_inst(i, y, **d) if isinstance(x, (float, int)) else hp.get_from_inst(i, x, **d) < hp.get_from_inst(i, y, **d),
    'less_than',
    given_args = ['instance', 'value'],
    help_string = 'Validate if less than arg value'
)

make_function(lambda i, x, y, **d: x <= hp.get_from_inst(i, y, **d) if isinstance(x, (float, int)) else hp.get_from_inst(i, x, **d) <= hp.get_from_inst(i, y, **d),
    'less_or_equal',
    given_args = ['instance', 'value'],
    help_string = 'Validate if less than or equal to args = [<max>]'
)

make_function(lambda i, x, y, z, **d: isinstance(x, (float, int)) and x >= hp.get_from_inst(i, y, **d) and x <= hp.get_from_inst(i, z, **d),
    'is_between',
    given_args = ['instance', 'value'],
    help_string = 'Validate if value in between args = [<min>, <max>], including the args'
)

make_function(lambda x: isinstance(x, (float, int)) and x >= -1. and x <= 1.,
    'between_minus_1_1',
    given_args = 'value',
    help_string = 'Validate if between -1 and 1'
)

def make_title_func(*x):
    try:
        l = list(unicode(instance[i].replace('_',' ')) for i in x)
    except KeyError as e:
        raise hp.error_raiser(
            AttributeRequiredError(
                "Attribute '{a}({key})' is required for object '{obj}'. Dependent attribute {dep} not found".format(
                    a = self.name, key = attribute, obj = self.model.name, dep = e.message
                )
            )
        )
    return (' '.join(l)).title()

make_function(make_title_func,
    'make_title',
    help_string = "Make attribute as title case for other attributes. args = ['attr1', 'attr2'....]"
)

make_function(lambda: self._to_ui_element(instance),
    'to_ui_element',
)

make_function(lambda: self._to_atype(instance),
    'to_atype',
)

make_function(lambda *x: self._refers_constraint(*x),
    'refers_constraint',
    given_args = ['value', 'instance']
)

make_function(lambda s, *x: s._refers_model(*x),
    'refers_model',
    given_args = ['self', 'instance']
)

make_function(
    lambda x: self._to_constraint(x),
    'to_constraint',
    given_args = 'value'
)

def choose_image_func(inst, list_attr, *filters, **kwargs):
    def_ = kwargs.pop('default', None)
    kwargs['return_empty'] = True
    select_list = choose_images_func(inst, list_attr, *filters, **kwargs)
    if not select_list:
        if def_:
            return inst.get(def_, def_)
        else:
            select_list = inst.get(list_attr)
    return hp.random.choice(select_list)['image']

make_function(
    lambda *x: hp.random.choice(x),
    'random_choice',
    help_string = 'Picks a random value from the samples provided in the args'
)

def choose_images_func(inst, list_attr, *filters, **kwargs):
    def do_if_empty():
        if re_:
            return []
        elif def_:
            return inst.get(def_, def_)
        else:
            return inst.get(list_attr)
    select_list = []
    def_ = kwargs.pop('default', None)
    re_ = kwargs.pop('return_empty', False)
    if inst.get(list_attr) is None:
        return do_if_empty()
    if not isinstance(inst.get(list_attr), (list, tuple)):
        raise ValidatorError("Attribute {} in instance is of incorrect type: {}".format(list_attr, inst))
    if not (filters and all(inst.get(f_) for f_ in filters)):
        return do_if_empty()
    for i in inst.get(list_attr):
        hit = True
        if hp.normalize(kwargs.get('_default_title', 'foierdfkdklnfds')) in hp.normalize(i.get('title')):
            logger.debug("Matched value for default_title {}: {}".format(kwargs.get('_default_title'), i.get('title', '').lower()))
            select_list.append(i)
            continue
        for f_ in filters:
            i_ = inst.get(f_)
            if (isinstance(i_, (str, unicode)) and hp.normalize(i_) in hp.normalize(i.get('title',''))):
                continue
            elif (isinstance(i_, (list, tuple)) and any(hp.normalize(_) in hp.normalize(i.get('title','')) for _ in i_)):
                continue
            else:
                hit = False
                logger.debug("Not getting value for additional {}: {}, looking in title: {}".format(f_, i_, i.get('title', '').lower()))
                break
        if hit:
            logger.info("Matched value for additional {}: {}, looking in title: {}".format(f_, i_, i.get('title', '').lower()))
            select_list.append(i)
    logger.debug("Selected list: %s", select_list)
    if not select_list:
        return do_if_empty()
    return select_list


make_function(
    choose_image_func,
    'choose_image',
    given_args = ['instance'],
    help_string = "Choose the correct image among the list to display as main image"
)

make_function(
    choose_images_func,
    'choose_images',
    given_args = ['instance'],
    help_string = "Choose the correct image among the list to display as main image"
)


def load_json(x, t = dict):
    if x is None:
        return None
    if isinstance(x, (list, tuple)):
        return x
    if isinstance(x, (dict,)):
        return x
    if isinstance(x, (str, unicode)):
        if not x:
            return None
        try:
            return hp.json.loads(x)
        except ValueError as e:
            if ',' in x and issubclass(t, list):
                try:
                    return map(string.strip, x.replace("\"","").split(','))
                except ValueError as e:
                    logger.error("Could not jsonify %s", x)
                    hp.print_error(e)
                    raise
            if ":" in x and issubclass(t, dict):
                try:
                    return {x1.split(":")[0].strip():x1.split(":")[1].strip() for x1 in x.replace("\"","").split(',')} 
                except Exception as e:
                    logger.error("Could not jsonify %s", x)
                    hp.print_error(e)
                    raise
            logger.error("Could not jsonify %s", x)
            hp.print_error(e)
            raise
    if issubclass(t, dict):
        return {}
    return t(x)

def to_unicode_safe_func(x):
    if isinstance(x, (str, unicode)):
        x = utils.escape(x)
    return to_unicode_func(x)

def to_unicode_func(x):
    if isinstance(x, str):
        return unicode(x, 'utf-8', errors = 'ignore')
    elif isinstance(x, unicode):
        return x
    try:
        return unicode(x, 'utf-8', errors = 'ignore')
    except (UnicodeEncodeError, UnicodeDecodeError, TypeError) as e:
        return unicode(str(x), 'utf-8', errors = 'ignore')

for datatype in (int, float, unicode, str, bool, db.geolocation, db.stringlist, db.numberlist, db.objectlist, dict, list):
    n = datatype.__name__
    f = datatype
    if datatype in (unicode, str):
        f = to_unicode_func
    if datatype in (int, float):
        f = hp.to_float
    if datatype in (bool,):
        f = lambda x: True if (isinstance(x, (str, unicode)) and x.lower() == 'true') or x is True else False
    if datatype in (dict, db.stringlist, db.numberlist, db.objectlist, list):
        f = partial(load_json, t = datatype)
    make_function(f, 'to_' + n, given_args = ['value'])
    if hasattr(datatype, 'to_json'):
        make_function(datatype.to_json, 'from_' + n, given_args = ['value'])
    else:
        make_function(lambda x: x, 'from_' + n, given_args = ['value'])

def to_datetime_func(value, units = None):
    return hp.to_datetime(value, units)

def from_datetime_func(value, units = DTFMT, tz = None):
    if isinstance(value, datetime):
        try:
            tz = tz or self.model.timezone
        except (NameError, AttributeError) as e:
            pass
        if isinstance(tz, (str, unicode)):
            tz = timezone(tz)
        if tz:
            if value.tzinfo:
                value = value.astimezone(tz)
            else:
                value = tz.localize(value)
        return value.strftime(units)
    return value

def to_date_func(value, units = None):
    if isinstance(value, date):
        return value
    r = hp.to_datetime(value, units)
    if isinstance(r, datetime):
        return r.date()
    return None

def from_date_func(value, units = DFMT):
    return value.strftime(units) if isinstance(value, date) else value

def to_time_func(value, units = None):
    if isinstance(value, dtime):
        return value
    return hp.to_datetime(value, units).time()

def from_time_func(value, units = TFMT):
    if value is None:
        return None
    return value.strftime(units)

def make_percent_func(value):
    if not value:
        return value
    value = float(value)
    if value < 1:
        return value
    return round(value/100., 4)

def make_inv_percent_func(value):
    if not value:
        return value
    value = float(value)
    if value > 1:
        return value
    return round(100.* value, 2)

def make_rounded_func(value, no_of_decimals = 2, round_type = "round"):
    if value is None:
        return value
    value = float(value)
    if round_type == 'ceil':
        value = hp.np.ceil(value)
    elif round_type == 'floor':
        value = hp.np.floor(value)
    value = round(value, no_of_decimals)
    if no_of_decimals == 0:
        return int(value)
    return value

for f in (to_datetime_func, to_date_func, to_time_func, from_datetime_func, from_date_func, from_time_func, make_rounded_func, make_percent_func, make_inv_percent_func, to_unicode_safe_func):
    make_function(f,
        f.__name__.replace('_func',''),
        given_args = ['value'],
        help_string = f.__name__.replace('_',' ').title()
    )

def to_login_id_func(x, allow_none = True):
    if x is None and allow_none:
        return x
    if not isinstance(x, (str, unicode, int, long)):
        raise AttributeError("Id should only be of types str, unicode and int")
    if isinstance(x, (str, unicode)):
        return hp.to_ascii_lower(x, case = 'lower', join = '_')
    return x

make_function(to_login_id_func,
    'to_login_id',
    given_args = 'value',
    help_string = 'Make type id'
)

make_function(lambda x: x.lower() if isinstance(x,  (str, unicode)) else x,
    "to_lowercase",
    given_args = 'value',
    help_string = 'To Lower Case'
)

make_function(lambda x: x.upper() if isinstance(x, (str, unicode)) else x,
    "to_uppercase",
    given_args = 'value',
    help_string = 'To Upper Case'
)

make_function(lambda x: x.title() if isinstance(x, (str, unicode)) else x,
    "to_titlecase",
    given_args = 'value',
    help_string = 'To Title Case'
)

make_function(hp.hash_password,
    'hash_password',
    given_args = 'value'
)

make_function(lambda x: True if (isinstance(x, (str, unicode)) and x.lower() == 'yes') or x is True else False,
    'to_bool_from_yes_no',
    given_args = 'value'
)

make_function(lambda x: True if (isinstance(x, (str, unicode)) and x.lower() == 'true') or x is True else False,
    'to_bool_from_true_false',
    given_args = 'value'
)

make_function(lambda x: 'yes' if x else 'no',
    'from_bool_to_yes_no',
    given_args = 'value'
)

make_function(lambda x: 'true' if x else 'false',
    'from_bool_to_true_false',
    given_args = 'value'
)

make_function(lambda x: datetime.now() + timedelta(seconds = float(x) * 60),
    'now_plus_n_minutes',
    help_string = 'Add n minutes to current time. args = [<num of minutes>]'
)

make_function(lambda x: datetime.now() + timedelta(seconds = float(x) * 3600),
    'now_plus_n_hours',
    help_string = 'Add n hours to current time. args = [<num of hours>]'
)

make_function(lambda x: datetime.now() + timedelta(days = float(x)),
    'now_plus_n_days',
    help_string = 'Add n days to current time. args = [<num of days>]'
)

make_function(lambda: datetime.now() + timedelta(days = 7),
    'now_plus_one_week',
    help_string = 'One week later'
)

make_function(lambda: datetime.now() + timedelta(days = 31),
    "now_plus_one_month",
    help_string = 'One month later'
)

make_function(lambda ins, attr, n: (hp.to_datetime(ins.get(attr, datetime.now())) + timedelta(days = n)),
    "plus_n_days",
    given_args = ['instance'],
    help_string = "Gets number of days since the given date attribute, args = 'date/datetime attribute'"
)

def add_time_func(ins, attr, timediff, units = 'seconds', subtract = False, tz = None):
    nt = hp.to_datetime(ins.get(attr, datetime.now(tz = ins.get(tz, tz))), tz = ins.get(tz, tz))
    timediff = float(ins.get(timediff, timediff))
    if subtract:
        timediff = -timediff
    timediff = timedelta(seconds = hp.time_divisors.get(ins.get(units, units), 1) * timediff)
    return nt + timediff

make_function(add_time_func,
    "add_time",
    given_args = ['instance'],
    help_string = "Gives a timestamp for n seconds after the given date attribute, args = 'date/datetime attribute', timediff attribute or quantity, units, whether to subtract (default is false) and the timezone"
)


make_function(lambda ins, attr, n: (hp.to_datetime(ins.get(attr, datetime.now())) - timedelta(seconds = ins.get(n))),
    "minus_n_seconds",
    given_args = ['instance'],
    help_string = "Gives a timestamp for n seconds before the given date attribute, args = 'date/datetime attribute'"
)

make_function(lambda ins, attr: (datetime.now() - ins.get(attr, datetime.now())).days,
    "days_since",
    given_args = 'instance',
    help_string = "Gets number of days since the given date attribute, args = 'date/datetime attribute'"
)

make_function(lambda x, y: x*y,
    'multiply',
    given_args = 'value'
)

make_function(lambda x, y: instance.get(x, x)*y,
    'multiply_attribute',
    help_string = 'multiply the argument with a value of attribute in an instance. args = [<attribute>, <value to multiple>]'
)

make_function(lambda *s,**kw: reduce(operator.mul, [gfiis(instance, x,  kw.get("default",0.)) for x in s], 1),
    'multiply_attributes',
    help_string = 'multiply the values of attributes in an instance. args = [<attribute_name>, <attribute_name1>, ...]'
)

make_function(lambda *s, **kw: reduce(operator.add, [gfiis(instance, x, kw.get("default",0.)) for x in s], 0),
    'sum_attributes',
    help_string = 'sum the argument with a value of attribute in an instance. args = [<attribute_name>, <attribute_name1>, ...] (alias of add_attributes)'
)

make_function(lambda *s, **kw: reduce(operator.add, [gfiis(instance, x, kw.get("default",0.)) for x in s], 0),
    'add_attributes',
    help_string = 'sum the argument with a value of attribute in an instance. args = [<attribute_name>, <attribute_name1>, ...] (alias of sum_attributes)'
)

def percentage_of_func(ins, attr, percentage, **kwargs):
    r = (gfiis(ins, attr, 0) * (1 + (gfiis(ins, percentage, 0)/100.0))) or kwargs.get('default') or 0
    if 'round_type' in kwargs or 'no_of_decimals' in kwargs:
        return make_rounded_func(r, **kwargs)
    return r

make_function(
    percentage_of_func,
    'percentage_of',
    given_args = ['instance'],
    help_string = 'calculated percentage of a value and adds to the value'
)

make_function(
    lambda a, **kw: True if re.match(kw.get('regex'), a) else False,
    'match_pattern',
    given_args = 'value',
    help_string = 'Match with regular expression the attribute value args = [<attribute_name>], kwargs = {regex: <pattern to match>}'
)

def gfiis(instance, x, default):
    """
    Get float from instance if string
    """
    if isinstance(x, (float, int, long)):
        return x
    if isinstance(x, (str, unicode)):
        return float(instance.get(x) or default)
    return default

def do_subtract(*s, **kw):
    default = kw.get('default', 0.)
    if len(s) <= 0:
        return default
    fs = gfiis(instance, s[0], default)
    logger.debug("First instance for attribute %s: %s", s[0], fs)
    if len(s) <= 1:
        return fs
    ss = reduce(operator.add, [gfiis(instance, _x,  default) for _x in s[1:]], 0)
    logger.debug("Second instances for attributes %s: %s", s[1:], ss)
    return (fs - ss)

make_function(do_subtract,
    'subtract_attributes',
    help_string = 'subtract the values of attribute in an instance. args = [<attribute_name>, <attribute_name1>]'
)

def do_divide(*s, **kw):
    default = kw.get('default', 0.)
    if len(s) <= 0:
        return default
    fs = gfiis(instance, s[0], default)
    if len(s) <= 1:
        return fs
    default = kw.get('default', 1.)
    ss = reduce(operator.mul, [gfiis(instance, _x,  default) for _x in s[1:]], 1)
    if ss == 0:
        ss = 1e-32
    return (fs/ss)

make_function(do_divide,
    'divide_attributes',
    help_string = 'divide the values of attribute in an instance. args = [<attribute_name>, <attribute_name1>]'
)

def quantity_adder_func(x, default = 1, stage_attr = 'stage', stage_value = 'cart', **kwargs):
    stage = instance.get(stage_attr)
    if stage != stage_value:
        return instance.get(attribute) or default
    if x is None:
        return instance.get(attribute, default) + 1
    return x

make_function(
    quantity_adder_func,
    'quantity_adder',
    given_args = 'value'
)


def make_adder(x, unique = False, allow_replace = None):
    default = self.atype[0]()
    att = attribute
    slf = self
    replace = instance.pop('_' + att + '_replace', instance.pop('_replace', False))
    subtract = instance.pop('_' + att + '_subtract', instance.pop('_subtract', False))
    i = self.model.get(self.model.dict_to_ids(instance))
    if i:
        p = i.get(att, default)
    elif not is_validator(slf.default):
        p = slf.default or default
    else:
        p = default
    if not issubclass(slf.atype[0], list):
        unique = False
    if issubclass(slf.atype[0], (list, tuple, db.stringlist, db.numberlist, db.objectlist)):
        if isinstance(x,list) and allow_replace is None:
            allow_replace = True
        x = slf.atype[0](hp.make_list(x))
        if replace or allow_replace:
            r = x
        elif subtract:
            r = slf.atype[0]( set(p) - set(x) )
        else:
            p = slf.atype[0](hp.make_list(p))
            r = p + x
        if unique:
            return slf.atype[0](set(r))
        return r
    elif issubclass(slf.atype[0], dict):
        if not isinstance(x, dict):
            raise TypeError("Cannot create type dict from value {}".format(x))
        if replace or allow_replace:
            p = x
        elif subtract:
            for k in x:
                p.pop(k, None)
        else:
            p.update(x)
        return p
    elif issubclass(slf.atype[0], (int, float, long)):
        x = float(x)
        return p + x
    return p + x

for a in ['list_adder', 'dict_adder', 'number_adder']:
    make_function(
        make_adder,
        a,
        given_args = 'value',
        help_string = 'add the current value to the existing {} incrementally'.format(a.replace('_adder', ''))
    )


make_function(lambda: instance.get(self.model.__class__(instance['role'], self.model.enterprise_id, self.model.role).ids[0]),
    'id_from_role'
)

def get_password_attribute(model):
    a = model.get_attributes('name', _as_option = True, type = 'password')
    if a:
        return a[0]
    return 'password'

def get_password():
    model = self.model.__class__(instance['role'], self.model.enterprise_id, self.model.role)
    attr = get_password_attribute(model)
    if instance.get(attr):
        return instance.get(attr)
    return hp.PASSWORD_NOT_SET

make_function(get_password,
    'password_from_role'
)

make_function(lambda: instance.get(self.model.__class__(instance['role'], self.model.enterprise_id, self.role).emails[0]),
    'email_from_role'
)

def is_enterprise_model_func(slf, ins):
    try:
        return ins['role'] in __import__(slf.__class__.__module__).model.models(slf.model.enterprise_id, 'name')
    except AttributeError:
        return ins['role'] in __import__(slf.model.__class__.__module__).models(slf.model.enterprise_id, 'name')

make_function(is_enterprise_model_func,
    'is_enterprise_model',
    given_args = ['self', 'instance']
)



make_function(lambda: not instance.get('validated', False),
    'true_if_not_validated'
)


make_function(lambda *x: self.model.get_from_parent(instance, *x),
    'get_from_parent',
)

def get_from_model_func(self, ins, x, y, z, **kwargs):
    smn = self.model.name
    s_id = hp.make_single([ins.get(_) for _ in self.model.ids], force = True)
    m_id = ins.get(y)
    dft = kwargs.pop('_default', None) or kwargs.pop('default', None)
    _attributes = kwargs.pop('_attributes', None)
    _internal = kwargs.pop('_internal', False)
    if m_id is None:
        return dft
    if smn == x and m_id == s_id:
        return dft
    logger.debug("Inside get from model: %s, enterprise_id: %s, role: %s, user_id: %s", x, self.model.enterprise_id, self.model.role, self.model.user_id)
    m = self.model.__class__(x, 
        self.model.enterprise_id, 
        kwargs.get('_role', self.model.role), 
        user_id = kwargs.get('_user_id', self.model.user_id)
    )
    logger.debug("Value for attribute %s in instance is %s", y, m_id)
    m_obj = m.get(m_id, {}, internal = _internal, attributes = _attributes, _additional_values = {k: (ins.get(k) or v) for k, v in kwargs.iteritems()})
    logger.debug("Foreign instance is: %s", m_obj)
    if z is None:
        logger.debug("Output: %s", m_obj)
        return m_obj
    op = m_obj.get(z, dft)
    logger.debug("Output: %s", op)
    return op


make_function(get_from_model_func,
    'get_from_model',
    given_args = ['self', 'instance'],
    help_string = 'Get a value from another model. args = [<model name>, <foreign key in current model>, <attribute in other model whose value you want>]. kwargs = \'_default\', \'_attributes\'',
    is_idempotent = False
)

def get_from_model_map_func(self, ins, model_name, attr, attr_list = None, default = None, as_list = False, make_unique = False, flatten = False, default_value = None, concat = False, **kwargs):
    smn = self.model.name
    s_id = hp.make_single([ins.get(_) for _ in self.model.ids], force = True)
    m_ids = ins.get(attr)
    dft = default or ([] if as_list else {})
    if m_ids is None:
        return dft
    m = self.model.__class__(
        model_name, self.model.enterprise_id, 
        kwargs.get('_role', self.model.role), 
        user_id = kwargs.get('_user_id', self.model.user_id)
    )
    m_ids = hp.make_list(m_ids)
    r = {}
    for m_id in m_ids:
        if smn == model_name and mid == s_id:
            continue
        kw = {_: __ for _, __ in kwargs.iteritems() if _.startswith('_')}
        m_obj = m.get(m_id, {}, _additional_values = {k: (ins.get(k) or v) for k, v in kwargs.iteritems()}, **kw)
        if attr_list is None:
            op = m_obj or None
        else:
            op = m_obj.get(attr_list, default_value)
        if op is None:
            continue
        if concat and m_id in r:
            op = hp.make_list(r[m_id]) + make_list(op)
            if make_unique:
                op = list(set(op))
        if as_list:
            op = hp.make_list(op)
            if make_unique:
                r[m_id] = list(set(op))
                continue
        r[m_id] = op
    if as_list:
        r = r.values()
        if flatten:
            r = reduce(lambda y, x: hp.make_list(x) + y, r, [])
        if not make_unique:
            return r
        return list(set(r))
    return r

make_function(get_from_model_map_func,
    "get_from_model_map",
    given_args = ['self', 'instance'],
    help_string = 'Get a dict of values from another model, based on a list of values for attr. args = [<model name>, <foreign key list in current model>, <attribute in other model whose value you want>]. kwargs = \'default\'',
    is_idempotent = False
)

make_function(
    lambda x, y: self.model.__class__(x, self.model.enterprise_id, self.model.role) if '.parents.' in y.get('refers','') else True,
    'is_model',
    given_args = ['value', 'instance']
)

make_function(lambda: self.model.parents['enterprise'].get(instance['enterprise_id'])['name'] + ' admin',
    'get_enterprise_name'
)

make_function(lambda *x: dict((i, instance.get(i)) for i in x),
    'add_to_history'
)

make_function(lambda x: None,
    'to_none',
    given_args = 'value'
)

def make_campaign_id_func():
    return hp.normalize_join(self.model.enterprise_id,instance.get('campaign_name'))


def get_rates_func(instance):
    with hp.read_file('data', 'billings', 'rates.json') as rates:
        return rates[instance.get('deployment_scenario', 'common')]

def get_jobs_func(instance):
    with hp.read_file('data', 'beats', '{}.json'.format(instance.get('deployment_scenario', 'common'))) as jobs:
        return jobs

def pivot_from_children_func(ins, child_model, child_attribute, action, default = None, **kwargs):
    sn = self.model.name
    m = self.model.__class__(
        child_model, self.model.enterprise_id, 
        kwargs.get('_role', self.model.role), 
        user_id = kwargs.get('_user_id', self.model.user_id)
    )
    default = kwargs.pop('_default', default)
    attribs = kwargs.pop('_attribs', None)
    kwargs.update(m.from_parent_id_dict(sn, ins))
    r = m.pivot(attributes = attribs, _over = child_attribute, _action = action, _internal = True, _as_option = True, **kwargs)
    if attribs:
        return r
    return r.get(child_attribute, default)

make_function(pivot_from_children_func,
	'pivot_from_child',
	given_args = 'instance',
        help_string = 'Get pivot function over a child model, args = [child_model, child_attribute, action], kwargs = {..any filter parameters..}',
        is_idempotent = False
)
   
def pivot_from_model_func(ins, pivot_model, over, action, default = None, **kwargs):
    sn = self.model.name
    m = self.model.__class__(
        pivot_model, self.model.enterprise_id, 
        kwargs.get('_role', self.model.role), 
        user_id = kwargs.get('_user_id', self.model.user_id)
    )
    attribs = kwargs.pop('_attribs', None)
    default = kwargs.pop('_default', default)
    kwargs = {k: hp.get_from_inst(ins, v.strip('{').strip('}')) if isinstance(v, (str, unicode)) and v.startswith('{') and v.endswith('}') else v  for k, v in kwargs.iteritems()}
    r = m.pivot(attributes = attribs, _over = over, _action = action, _internal = True, _as_option = True, **kwargs)
    if attribs:
        return r
    return r.get(over, default)

make_function(pivot_from_model_func,
    'pivot_from_model',
    given_args = 'instance',
    help_string = 'Get pivot function over a model, args = [pivot_model, over, action ], kwargs = Filter attributes is a dict of the {attribute in pivot_model: attribute in current model or static value}',
    is_idempotent = False
)

def search_among_model_func(ins, model_name, attribute_name = None, _default = None, **kwargs): 
    sn = self.model
    m = sn.__class__(model_name, sn.enterprise_id, sn.role, user_id = sn.user_id)
    r = hp.make_single(m.list(_page_size = 1, _as_option = True, **kwargs), force = True, default = {})
    if attribute_name:
        return r.get(attribute_name, _default)
    return r or _default
   
def search_among_children_func(ins, child_model, child_attribute = None, _default = None, **kwargs):
    sn = self.model.name
    m = self.model.__class__(
        child_model, self.model.enterprise_id, 
        kwargs.get('_role', self.model.role), 
        user_id = kwargs.get('_user_id', self.model.user_id)
    )
    kwargs.update(m.from_parent_id_dict(sn, ins))
    r = m.filter(**kwargs)
    if child_attribute:
        try:
            return r.next().get(child_attribute, _default)
        except StopIteration:
            return _default
    return list(r)

make_function(search_among_model_func,
    'search_in_model',
    given_args = 'instance',
    help_string = 'Search among objects of a model and return an instance or the value of the attribute if provided of the first result',
    is_idempotent = False
)

make_function(search_among_children_func,
    'search_among_children',
    given_args = 'instance',
    help_string = 'Search among the children and return a list or the value of the child_attribute of the first result if provided (alias of search_from_children)',
    is_idempotent = False
)
make_function(search_among_children_func,
    'search_in_children',
    given_args = 'instance',
    help_string = 'Search among the children and return a list or the value of the child_attribute of the first result if provided (alias of search_among_children) args = (child model, child_attribute) If attribute name is provided then only a single value is provided',
    is_idempotent = False
)

make_function(search_among_children_func,
    'search_from_children',
    given_args = 'instance',
    help_string = 'Search among the children and return a list or the value of the child_attribute of the first result if provided (alias of search_among_children)'
)
def make_store_pricing_func(self, ins, store_model_name, stock_model_name, price_attribute, default = None):
    store_model = self.model.__class__(store_model_name, self.model.enterprise_id, self.model.role, user_id = self.model.user_id)
    stock_model = self.model.__class__(stock_model_name, self.model.enterprise_id, self.model.role, user_id = self.model.user_id)
    if isinstance(value, (float, int, long)):
        default = value
    elif isinstance(default, (unicode, str)):
        default = ins.get(default)
    return_dict = {'_default': default}
    for s in store_model.filter(_filter_attributes = store_model.ids[0]):
        k = stock_model.from_parent_id_dict(self.model.name, ins)
        k = stock_model.from_parent_id_dict(store_model, s, k)
        t = stock_model.list(_page_size = 1, _as_option = True, **k)
        if t:
            return_dict['--'.join(map(str, store_model.dict_to_ids(s)))] = t.get(price_attribute, default)
    return return_dict


def re_key_map_func(ins, attr1, attr2, default = None, default_value = None, singlify = True, flatten = True, make_unique = True):
    r = {}
    i1 = ins.get(attr1)
    i2 = ins.get(attr2)
    if not isinstance(i1, (dict, list)):
        return default
    if isinstance(i1, list):
        ii = lambda: enumerate(i1)
    if isinstance(i1, dict):
        ii = i1.iteritems
    if not isinstance(i2, (dict, list)):
        return {v: default_value for k, v in ii()}
    def do(v, v1):
        if isinstance(v, list):
            for v2 in v:
                do(v2, v1)
        try:
            if v not in r:
                r[v] = []
        except TypeError:
            return
        if flatten:
            r[v].extend(hp.make_list(v1))
        else:
            r[v].append(v1)
    for k, v in ii():
        try:
            v1 = i2[k]
        except (IndexError, KeyError) as e:
            v1 = default_value
        do(v, v1)
    if flatten and make_unique:
        for k, v in r.items():
            r[k] = list(set(v))
    if singlify:
        for k, v in r.items():
            r[k] = hp.make_single(v)
    return r

make_function(
    re_key_map_func,
    "remap_dicts",
    given_args = ['instance'],
    help_string = "Takes two attributes which are dicts or lists and creates a dict which maps the values in attr1 to values in attr2, options are singlify, flatten and make_unique, default (if any of the attributes are empty) and default_value when a key is not present"
)
make_function(
    lambda i, a, **kw: hp.invert_dict(hp.get_from_inst(i, a, kw.get('default'), True), **kw),
    "invert_dict",
    given_args = ['instance'],
    help_string = "Takes an attribute which is nested object and then converts the keys to values and values to keys, inputs are the attribute which you need to invert"
)
    
def get_store_pricing_func(self, value, store_id_attribute, default = None):
    if not isinstance(value, dict):
        return value
    user = self.model.user
    store_id = '--'.join(map(str, (user.get(_) for _ in hp.make_list(store_id_attribute))))
    if not store_id:
        return value.get('_default', default)
    return value.get(store_id, value.get('_default', default))

make_function(
   make_store_pricing_func,
   'make_store_pricing',
   given_args = ['self', 'instance'],
   help_string = "Creates a dict of pricing for every store, args = store_model_name, stock_model_name, price_attribute is stock model, default can be provided"
)

make_function(
    get_store_pricing_func,
    'get_store_pricing',
    given_args = ['self', 'value'],
    help_string = "Picks the price according to the store based on the store id attribute present in the user dict"
)

make_function(
    get_rates_func,
    'get_rates',
    given_args = ['instance']
)

make_function(
    lambda: self.model.user_id,
    'get_user_id',
    help_string = 'Get the user_id from the model',
    is_idempotent = False
)

def get_from_user_role_func(attr, role, default = None):
    if self.model.role == role:
        return get_from_user_func(attr, default = default)
    return None

def get_from_user_func(attr, default = None):
    logger.info("In get from user: Model: %s, User: %s, User_id: %s, Role: %s", self.model.name, self.model.user, self.model.user_id, self.model.role)
    if not isinstance(self.model.user, dict):
        return None
    return self.model.user.get(attr, default)

def get_from_enterprise_func(slf, attr, default = None, *args, **kwargs):
    logger.info("In get from user: Model: %s, User: %s, User_id: %s, Role: %s", self.model.name, self.model.user, self.model.user_id, self.model.role)
    if not isinstance(slf.model.enterprise, dict):
        return None
    attr = hp.make_list(attr)
    r = slf.model.enterprise
    for a in attr:
        if isinstance(r, dict):
            r = r.get(attr, {})
        elif isinstance(r, list) and isinstance(a, (int, long)):
            r = r[a]
        else:
            return r
    return r or default

make_function(
    get_from_enterprise_func,
    'get_from_enterprise',
    given_args = ['self'],
    help_string = 'Get the enterprise attribute from the models\'s enterprise. attr can be a single attribute or a list to get ',
    is_idempotent = False
)

make_function(
    get_from_user_func,
    'get_from_user',
    help_string = 'Get the user attribute from the user\'s profile',
    is_idempotent = False
)

make_function(
    get_from_user_role_func,
    'get_from_user_role',
    help_string = 'Get the user attribute from the user\'s profile if role is what is specified in args = [<user attr>, <role>]',
    is_idempotent = False
)

make_function(
    get_jobs_func,
    'get_jobs',
    given_args = ['instance']
)

make_function(
    make_campaign_id_func,
    'make_campaign_id'
)

make_function(
    hp.inflect.plural,
    'make_plural',
    given_args = ['value'],
    help_string = 'Make word into plural'
)

def manage_directory_name(name = None, directory = None, self = None, suffix = '.svg'):
    if directory is None:
        try:
            directory = joinpath(STATIC_DIR, self.model.enterprise_id, self.name)
            cdn = joinpath(CDN_DOMAIN, self.model.enterprise_id, self.name)
        except Exception as e:
            logger.warning("Cannot create image in enterprise sepcific directory, probably out of validator context..")
            directory = STATIC_DIR
            cdn = CDN_DOMAIN
    else:
        cdn = directory.replace(STATIC_DIR, CDN_DOMAIN)
    hp.mkdir_p(directory)
    if not isinstance(name, (str, unicode)):
        tfile = tempfile.NamedTemporaryFile(dir = directory, suffix = suffix)
        tfile.close()
        name = basename(tfile.name)
    return name, directory, cdn

def make_qr_code_func(ins, self = None, attr = None, name = None, directory = None):
    '''
    INPUT: ins can be string or dict.
        if dict then attr should be given
    OUTPUT: gives URL of the bar_code generated
    '''
    v = ins.get(attr) if isinstance(ins, dict) and attr else ins
    if v is None: return
    if not isinstance(v, (str, unicode)):
        raise ValidatorError("No text value for attribute {} for instance {} in model {}: received {}".format(attr, ins, self.model.name, v))
    img = qrcode.make(v, image_factory = qrcode.image.svg.SvgPathImage)
    name, directory, cdn = manage_directory_name(name, directory, self)
    img.save(joinpath(directory, name))
    return joinpath(cdn, name)

make_function(
    make_qr_code_func,
    'make_qr_code',
    given_args = ['instance', 'self'],
    help_string = 'Saves the QR code svg image to a path in the uplaods folder',
    is_idempotent = False
)

def time_diff_func(ins, attr1, attr2, units = 'seconds', **kwargs):
    if units not in hp.time_divisors:
        logger.error("Unknown unit in time_diff: %s", units)
        return None
    try:
        a1 = hp.to_datetime(hp.get_from_inst(ins, attr1))
        a2 = hp.to_datetime(hp.get_from_inst(ins, attr2))
        return int(((a1 - a2).total_seconds()/hp.time_divisors.get(units, 1.0)))
    except Exception as e:
        hp.print_error(e)
    return None

make_function(
    time_diff_func,
    'time_diff',
    given_args = ['instance'],
    help_string = 'Finds the time different between two attributes'
)

def make_bar_code_func(ins, self = None, attr = None, ean = None, name = None, directory = None):
    '''
    INPUT: ins can be string or dict.
        if dict then attr should be given
    OUTPUT: gives URL of the bar_code generated
    '''
    v = ins.get(attr) if isinstance(ins, dict) and attr else ins
    if v is None: return
    barcoder = barcode.get_barcode_class(hp.get_from_inst(ins, ean, default = ean or 'code39'), add_checksum = False)
    if not isinstance(v, (str, unicode)):
        raise ValidatorError("No text value for attribute {} for instance {} in model {}: received {}".format(attr, ins, self.model.name, v))
    img = barcoder(bar_code_stripper(v))
    name, directory, cdn = manage_directory_name(name, directory, self)
    namewext, ext = splitext(name)
    img.save(joinpath(directory, namewext))
    return joinpath(cdn, name)

make_function(
    make_bar_code_func,
    'make_bar_code',
    given_args = ['instance', 'self'],
    help_string = 'Saves the Bar code svg image to a path in the uploads folder, ean type can be specified in kwargs',
    is_idempotent = False
)

def make_pdf417_func(ins, self = None, attr = None, name = None, directory = None):
    '''
    INPUT: ins can be string or dict.
        if dict then attr should be given
    OUTPUT: gives URL of the bar_code generated
    '''
    v = ins.get(attr) if isinstance(ins, dict) and attr else ins
    if not isinstance(v, (str, unicode)):
        raise ValidatorError("No text value for attribute {} for instance {} in model {}: received {}".format(attr, ins, self.model.name, v))
    codes = pdf417gen.encode(v)
    img = pdf417gen.render_svg(codes)
    name, directory, cdn = manage_directory_name(name, directory, self)
    img.write(joinpath(directory, name))
    return joinpath(cdn, name)

make_function(
    make_pdf417_func,
    'make_pdf417',
    given_args = ['instance', 'self'],
    help_string = 'Saves the pdf417 code svg image to a path in the uploads folder',
    is_idempotent = False
)

def is_timed_out(slf, ins, vl, attr, timeout_days = 0, timeout_secs = 600, tz = None):
    # If not logged in then return False
    # If logged in then check if timed out
    try:
        tz = tz or slf.model.timezone
    except (NameError, AttributeError) as e:
        tz = None
    timeout_period = hp.timedelta(days = timeout_days, seconds = timeout_secs)
    if not vl:
        return False
    if not ins:
        return False
    v = ins.get(attr)
    if not v:
        return vl
    try:
        if hp.to_datetime(v, tz = tz) + timeout_period < hp.now(tz = tz):
            return False
    except ValueError as e:
        return True
    return True

make_function(
    is_timed_out,
    'timed_out',
    given_args = ['self', 'instance', 'value'],
    help_string = 'Returns true if the time attribute is less than timeout_period from instance attribute'
)

make_function(
    lambda x, y: "{}/view-product/{}/{}".format(SERVER_NAME, x.model.enterprise_id, y.get(x.model.ids[0])),
    'make_product_url',
    given_args = ['self', 'instance'],
    help_string = "Returns the product url",
    is_idempotent = False
)

make_function(
    lambda i, v, *x, **y: hp.resolve_log(hp.get_from_inst(i, *x, default = y.get('default', v)), **y),
    'make_range',
    given_args = ['instance', 'value'],
    help_string = 'Converts any given number into a range, args = if given will be the attribute name in the instance, kwargs = exponent (def: 10), max_exponent (def: 10), sep (def: -), prefix and suffix, if insist_zero is true, then zero will not return a range'
)

make_function(
    lambda i, x, **y: hp.number_categories(hp.get_from_inst(i, x, default = y.get('default', 0)), **y),
    'categorize_number',
    given_args = ['instance'],
    help_string = "Converts any given number into a range as exponents of 10"
)

make_function(
    lambda i, x, d, *y, **z: hp.number_to_range(hp.get_from_inst(i, x, d), d, *y, **z),
    'number_to_category',
    given_args = ['instance'],
    help_string = 'Converts any given number into a category args = 1. name of attribute which is a number, default value, and list of min, max and category. If the number is within the said range, the category will be assigned, or the default value will be set. By default lower bound of range is inclusive and upper bound of range is exclusive'
)


make_function(
    lambda x, **kw: value.get(instance.get(x, x), kw.get('default', value)) if (isinstance(value, dict) and not is_validator(value)) else kw.get('default', value),
    'get_from_dict',
    help_string = "If you have a dictionary, then you can get one value by passing the key you need parameter, and default as kw"
)

def convert_filter_to_tags(ins, filter_attr, separator = '--', **kwargs):
    def do(k, v, prev):
        if v is None:
            prev += [u'{}{}null'.format(k, separator)]
            return prev
        if isinstance(v, bool):
            prev += [u'{}{}{}'.format(k, separator, 'true' if v else 'false')]
            return prev
        if isinstance(v, (str, unicode)):
            prev += [u'{}{}{}'.format(k, separator, v)]
            return prev
        if isinstance(v, (int, float, long)):
            prev += [u'{}{}{}'.format(k, separator, v), u'{}{}{}'.format(k, separator, hp.resolve_log(v)), u'{}{}{}'.format(k, separator, hp.number_categories(v))]
            return prev
        if isinstance(v, date):
            prev += [u'{}{}year{}{}'.format(k, separator, separator, v.year), u'{}{}month{}{}'.format(k, separator, separator, v.strftime('%b')), u'{}{}day_of_week{}{}'.format(k, separator, separator, v.strftime('%a')), u'{}{}date{}{}'.format(k, separator, separator, v.strftime('%d'))]
            return prev
        if isinstance(v, datetime):
            prev += [u'{}{}year{}{}'.format(k, separator, separator, v.year), u'{}{}month{}{}'.format(k, separator, separator, v.strftime('%b')), u'{}{}day_of_week{}{}'.format(k, separator, separator, v.strftime('%a')), u'{}{}date{}{}'.format(k, separator, separator, v.strftime('%d')), u'{}{}hour{}{}'.format(k, separator, v.strftime('%H'))]
            return prev
        if isinstance(v, list):
            for v1 in v:
                prev = do(k, v1, prev)
            return prev
        if isinstance(v, dict):
            for k1, v1 in v:
                prev = do(u'{}{}{}'.format(k, separator, k1), v1, prev)
            return prev
        prev += [u'{}{}{}'.format(k, separator, v)]
    j = {}
    if isinstance(filter_attr, (str, unicode)):
        j = ins.get(filter_attr, {})
    elif isinstance(filter_attr, list):
        filter_attr = hp.make_list(filter_attr)
        j = {k: ins.get(k) for k in filter_attr}
    else:
        j = ins
    prev = []
    if not isinstance(j, dict):
        return None
    for k, v in sorted(j.iteritems()):
        prev = do(k, v, prev)
    return prev

make_function(
        convert_filter_to_tags,
        "filters_to_tags",
        given_args = "instance",
        help_string = "Converts a dictionary of key valu pairs to a list of tags, like key1--value1, key2--value2, args = 'filter_attr' (can be a single attribute of the instance, or a list of attributes. If empty the entire instance is converted to tags"
)

def delete_url_or_path(url, url_map = None):
    if not isinstance(url, (str, unicode)):
        return url
    if ispath(url):
        os.remove(url)
        return url
    url_map = url_map or URL_MAP
    for k, v in url_map.iteritems():
        if url.startswith(k) and ispath(v):
            nf = url.replace(k, v)
            if ispath(nf):
                os.remove(nf)
                return nf
    return None

make_function(
    delete_url_or_path,
    "delete_url",
    given_args = ["value"],
    help_string = "If the given URL is a locally accessible URL, then delete it",
    is_idempotent = False
)

# IP STACK SAMPLE RESPONSE
# {
#  "ip": "134.201.250.155",
#  "hostname": "134.201.250.155",
#  "type": "ipv4",
#  "continent_code": "NA",
#  "continent_name": "North America",
#  "country_code": "US",
#  "country_name": "United States",
#  "region_code": "CA",
#  "region_name": "California",
#  "city": "Los Angeles",
#  "zip": "90013",
#  "latitude": 34.0453,
#  "longitude": -118.2413,
#  "location": {
#    "geoname_id": 5368361,
#    "capital": "Washington D.C.",
#    "languages": [
#        {
#          "code": "en",
#          "name": "English",
#          "native": "English"
#        }
#    ],
#    "country_flag": "https://assets.ipstack.com/images/assets/flags_svg/us.svg",
#    "country_flag_emoji_unicode": "U+1F1FA U+1F1F8",
#    "calling_code": "1",
#    "is_eu": false
#  },
#  "time_zone": {
#    "id": "America/Los_Angeles",
#    "current_time": "2018-03-29T07:35:08-07:00",
#    "gmt_offset": -25200,
#    "code": "PDT",
#    "is_daylight_saving": true
#  },
#  "currency": {
#    "code": "USD",
#    "name": "US Dollar",
#    "plural": "US dollars",
#    "symbol": "$",
#    "symbol_native": "$"
#  },
#  "connection": {
#    "asn": 25876,
#    "isp": "Los Angeles Department of Water & Power"
#  },
#  "security": {
#    "is_proxy": false,
#    "proxy_type": null,
#    "is_crawler": false,
#    "crawler_name": null,
#    "crawler_type": null,
#    "is_tor": false,
#    "threat_level": "low",
#    "threat_types": null
#  }
#}
def get_location_func(ip, inst, attribute = None, default = None, from_attribute = None):
    ip = ip or hp.get_from_inst(inst, from_attribute, None)
    if ip is None:
        return None
    if not hp.is_ip_address(ip) and from_attribute:
        ip = hp.get_from_inst(inst, from_attribute, None)
    if not hp.is_ip_address(ip):
        logger.warning("Ip address: %s is not an IP address", ip)
        return default if default is not None else ip
    try:
        r = requests.get("http://api.ipstack.com/{}?access_key={}".format(ip, os.environ.get("IP_STACK_KEY", "1cd645fd9e0e98c0ef517eaeb10dea8c")))
        logger.info("Get Response JSON: %s", r)
        if r.status_code != 200:
            logger.error("Error in IP look up: %s, error: %s", ip, r.content)
            return None
        ret = r.json()
        logger.debug("Got Response JSON: %s", ret)
        if ret.get('success') == False:
            logger.error("Issue with API: %s: %s", ret.get('error', {}).get('code', 'NA'), ret.get('error', {}).get('info', 'Unable to call IP location API'))
        if not attribute:
            return ret
        if attribute not in ret:
            logger.warning("Attribute %s not found in response from ipstack", attribute)
        return ret.get(attribute, default)
    except ValueError:
        logger.error("Could not understand ip lookup content: %s", r.content)
        return None
    except requests.exceptions.ConnectionError:
        logger.error("ConnectionError, in looking up ip: %s", ip)
        return None
    return None

make_function(
    get_location_func,
    "get_location",
    given_args = ["value", "instance"],
    help_string = "Get location city from ip address",
    is_idempotent = True
)

def replace_string_func(ins, value, attr = None, *args, **kwargs):
    s = hp.get_from_inst(ins, attr, value)
    if not isinstance(s, (str, unicode)) and not is_validator(s):
        return value
    flags = None
    if kwargs.get('ignore_case'):
        flags = re.IGNORECASE
    for r1, r2 in args:
        if flags:
            s = re.sub(r"{}".format(r1), r2, s, flags=flags)
        else:
            s = re.sub(r"{}".format(r1), r2, s)
    return s

make_function(
    replace_string_func,
    "replace_string",
    given_args = ["instance", "value"],
    help_string = "pass a list of strings to replace in the current string, e.g. [[string1, string2], [string3, string4]]",
    is_idempotent = False
)

make_function(
    lambda ins, x, *a, **kw: hp.split_name_to_3(hp.get_from_inst(ins, x, *a), 0, **kw),
    "get_first_name",
    given_args = ["instance"],
    help_string = "Get first name from another full name attribute"
)

make_function(
    lambda ins, x, *a, **kw: hp.split_name_to_3(hp.get_from_inst(ins, x, *a), 2, **kw),
    "get_last_name",
    given_args = ["instance"],
    help_string = "Get last name from another full name attribute"
)

make_function(
    lambda ins, x, *a, **kw: hp.split_name_to_3(hp.get_from_inst(ins, x, *a, **kw), 1, **kw),
    "get_middle_name",
    given_args = ["instance"],
    help_string = "Get middle name from another full name attribute"
)

def string_splice_function(val, start_index = 0, end_index = None, count = None):
    if not isinstance(val, (str, unicode)):
        return val
    if end_index is None and isinstance(count, (int, float)):
        end_index = start_index + count
    return val[start_index:end_index]

make_function(
    string_splice_function,
    "splice_string",
    given_args = ["value"],
    help_string = "pass the start index and end index as arguments to splice the string, e.g. kwargs: {'start_index': 1, 'count': 10}, will return the 2nd to the 11th index of the string"
)

def to_pronounciation_func(ins, *args, **kwargs):
    if not args:
        return None
    value = ins.get(args[0],'').lower()
    replace_dict = kwargs.pop('replace_dict', {})
    from_start_remove = hp.make_list(kwargs.pop('from_start_remove', []))
    from_word_replace = kwargs.pop("from_word_replace", {})

    if replace_dict:
        for k, v in replace_dict.items():
            value = value.replace(k.lower(), v)
    if from_start_remove:
        for x in from_start_remove:
            if x.lower() not in value:
                continue
            i = value.index(x.lower())
            value = value[:i]

    if from_word_replace:
        for word, v in from_word_replace.items():
            if word.lower() not in value:
                continue
            value = v
    
    value = value.strip().replace('  ', ' ')
    m = {
        "lower":lambda x : x.lower(),
        "upper":lambda x : x.upper(),
        "title":lambda x : x.title(),
        "capital": lambda x : x.capitalize()
    }
    output_case = kwargs.get('output_case', 'lower').lower()
    return m[output_case](value) if output_case in m else value

make_function(
    to_pronounciation_func, 
    "to_pronounciation",
    given_args = ["instance"],
    help_string = "Replace the value of attribute to its pronounciation"
)
    
def time_format_func(ins, format, dt = None, tz  = 'Asia/Kolkata', timestamp_attribute = None):
    dt = dt or hp.now(tz = tz)
    if timestamp_attribute:
        dt = ins.get(timestamp_attribute)
    if not isinstance(dt, datetime):
       if isinstance(dt, (str, int, float)):
           try:
             dt = hp.to_datetime(dt, tz = tz, fuzzy = True)
           except:
               return dt
       if not isinstance(dt, datetime):
           return dt
    
    dt = hp.to_datetime(dt, tz = tz, fuzzy=True)
    return dt.strftime(format)

make_function(
    time_format_func, 
    "time_format",
    given_args= ['instance'],
    help_string = "To get time format"
)
    
if __name__ == '__main__':
    import time

    now = time.time()
    class MyClass(object):
        def __init__(self, *args, **kwargs):
            pass
        

    obj = MyClass()
    obj.model = MyClass()
    obj.name = 'Attribute Name'
    obj.model.name = 'Object Name'
    obj.model.enterprise_id = 'cat'
    obj.model.role = 'admin'
    obj.model.user_id = None
    obj.model.timezone = 'Asia/Kolkata'
    obj.model.ids = ['a', 'b']
    obj.model.exists = lambda *x, **y: False
    print current_module.uuid_email(obj, {'email': 'my@email.com'}, 'uid')
    try:
        current_module.raise_error(obj, {}, 'my attribute')
    except AttributeRequiredError:
        pass
    print current_module.make_uuid(obj, {'a': 1, 'b': 2, 'c': 3}, 'uid', None, 'a', 'b')
    print current_module.to_datetime(obj, {}, None, 'Sat Jan 14 00:00:36 2017')
    print "Time taken is", time.time() - now

    #{'function': 'multiply', 'args': [0.25]}
    print current_module.multiply(obj, {'a': 10}, 'a', 10, 0.25)


    #{'function': 'multiply_attribute': 'args': ['a', 0.25]}
    print current_module.multiply_attribute(obj, {'a': 10}, 'b', None, 'a', 0.25)

    #{'function': 'multi_function': 'args': [...]}
    print "Chain functions", current_module.chain_functions(obj, {'a': 10, 'b': 6}, 'c', 0,
            {"function": "add_attributes", "args": [1000, 'a']},
            {"function": "subtract_attributes", "args": ['{_value_0}', 10]},
            {"function": "multiply_attributes", "args": ['b', '{_value_1}']}
    )

    print current_module.name_to_id(
        obj, {'mobile_number': '+91 9980 838 165', 'id': None}, 'id', None, 
        "mobile_number", 
        case = 'lower', attribute_join = '', compressor = 'shorten', 
        buffer_side = 'right', buffer = "", length = 10
    )
    print current_module.name_to_id(
        obj, {'mobile_number': '080-412-70-965', 'id': None}, 'id', None, 
        "mobile_number", 
        case = 'lower', attribute_join = '', compressor = 'shorten', 
        buffer_side = 'right', buffer = "", length = 10, 
    )
    print current_module.replace_string(
        obj, {'mobile_number': '080-412-70-965-Abcd', 'id': None}, 'mobile_number', None, 
        "mobile_number", ["-", "."], ["965", "999"], ["abcd", "ABD"], ignore_case = True
    )
    print current_module.add_time(
        obj, {'start_time': '2020-12-24 17:25:00', 'duration': 60, 'units': 'minutes', 'plus_time': None}, 'plus_time', None,
        "start_time",
        "duration",
        "units",
        True
    )
    print current_module.add_time(
        obj, {'start_time': '2020-12-24', 'duration': 60, 'units': 'minutes', 'plus_time': None}, 'plus_time', None,
        "start_time",
        "duration",
        "units",
        True
    )
    print current_module.make_range(
        obj, {'influencers': 200}, 'range', None,
        "influencers",
        exponent = 10,
        min_exponent = 3,
        max_exponent = 6
    )
    print current_module.categorize_number(
        obj, {'influencers': 800}, 'range', None,
        "influencers",
        min_exponent = 3,
        max_exponent = 7
    )
    print current_module.categorize_number(
        obj, {'influencers': 2000000}, 'range', None,
        "influencers",
        min_exponent = 3,
        max_exponent = 7
    )
    print current_module.remap_dicts(
        obj, {"a": {"a": "m", "b": ["n", "m"]}, "b": {"a": "x", "b": ["y", "z"]}},'c', None, 
        "a", "b"
    )
    print current_module.remap_dicts(
        obj, {"a": {"a": "m", "b": "n"}, "b": {"a": "x", "b": "y"}},'c', None, 
        "a", "b",
    )
    print current_module.remap_dicts(
        obj, {"a": ["m", "n"], "b": ["x", "y"]}, 'c', None, 
        "a", "b",
    )
    print current_module.remap_dicts(
        obj, {"a": {"a": "m", "b": ["n", "m"]}, "b": {"a": "x", "b": ["y", "z"]}},'c', None, 
        "a", "b", flatten = False
    )
    print current_module.remap_dicts(
        obj, {"a": {"a": "m", "b": "n", "c": "m"}, "b": {"a": "x", "b": "y", "c": "z"}},'c', None, 
        "a", "b",
    )
    print current_module.name_to_id(
        obj, {'tags': ['tag 1 is this', 'this is tag 2', ['a', 'b'], 2, None], 'id': None}, 'id', None, 
        "tags", 
        case = 'lower', attribute_join = ' ', 
    )
    print current_module.number_to_category(
        obj, {'my_number': 10}, 'my_category', None, 
        "my_number", None, 
        [None, 5, 'Low'], [5, 10, 'High'],
        upper_bound = 'inclusive'
    )
    print current_module.filters_to_tags(
        obj, {'my_attribute': {'a': 1, 'b': 'c', 'd': True, 'e': ['a', 'b', 1]}}, 'my_tags', None,
        'my_attribute'
    )

    
    

    
    
    
