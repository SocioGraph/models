#!/usr/bin/python
import os, sys
import inspect
import logging
import requests
from backends import get_queue_manager, set_queue_manager as backend_sqm, MODULES
from functools import wraps
from os.path import exists as ispath, basename, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile, getmtime as modified_time
d = joinpath(dirname(dirname(dirname(abspath(__file__)))), 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
d = joinpath(dirname(dirname(dirname(abspath(__file__)))), 'models')
if d not in sys.path:
    sys.path.insert(0, d)
import helpers as hp

LIST_OF_TASKS = {}
SERVICE = None
RESERVED_FUNCTION_NAMES = ["result", "error", "status"]
ENVIRONMENT = os.environ.get('ENVIRONMENT', 'local')
if ENVIRONMENT in ('prod', 'production'):
    ENVIRONMENT = ""
else:
    ENVIRONMENT = "-{ENVIRONMENT}".format(ENVIRONMENT = ENVIRONMENT)
# If config file is set up in the environment, then we use the same configuration    
CONFIG_FILE = os.environ.get('WORKER_CONFIG_PATH')
# If importing class sets up the base path, we can use the same to get the configuration file
BASE_PATH = ""

class TaskNotFound(hp.I2CEError):
    pass

class GrydBackendError(hp.I2CEError):
    pass

logger = hp.get_logger(__name__)

def set_queue_manager(base_path = None, config = None, module = None):
    module = module or get_module_name(1)
    base_path = base_path or BASE_PATH
    # If config file is set up in the environment, then we use the same configuration    
    if CONFIG_FILE:
        return backend_sqm(module, config_path = CONFIG_FILE)
    if base_path and ispath(hp.joinpath(base_path, 'worker-config.json')):
        return backend_sqm(module, config_path = hp.joinpath(base_path, 'worker-config.json'))
    if config:
        return backend_sqm(module, queue_manager = config)
    logger.warn("No config provided by {module} while setting queue manager, setting global default".format(module = module))
    qm = backend_sqm(module, config)

def get_module_name(stack_depth = 0):
    global SERVICE
    if SERVICE:
        return SERVICE
    stack_depth += 1
    frm = inspect.stack()[stack_depth]
    function = frm[0]
    module = inspect.getmodule(function)
    module_name = getattr(module, 'SERVICE', module.__name__)
    return module_name

def get_calling_function_name(stack_depth = 0):
    stack_depth += 1
    frm = inspect.stack()[stack_depth]
    function = frm[0]
    return function.__name__


def is_a_task(fn = None, srvice = None, q_manager = None, is_special_task = False, input_generator = None, result_verifier = None, sample_input = None):
    """
    API: /execute/<service_name>/<function_name>
    DOCS: /docs/<service_name>/<function_name>
    decorator function which allows any function to be called in an asynchronous manner
    NOTE: If the calling function and caller are not in the same module, then please use create_async_task
    otherwise any such task within the same (or imported module) can be called using the apply_async function
    """
    def is_a_task_inner(func):
        function_name = fn or func.__name__
        service = srvice or get_module_name()
        queue_manager = q_manager or get_queue_manager(module_name = service)
        if function_name in RESERVED_FUNCTION_NAMES and not is_special_task:
            raise GrydBackendError("Function {function_name} is reserved".format(function_name = function_name))
        global LIST_OF_TASKS
        if not service in LIST_OF_TASKS:
            LIST_OF_TASKS[service] = {}
        LIST_OF_TASKS[service][function_name] = func
        func.__name__ = function_name
        if is_special_task:
            func._is_special_task = True
        
        def create_job_task(*args, **kwargs):
            return {
                "enterprise_id": kwargs.pop('enterprise_id', 'core'), 
                "job_id": kwargs.pop('job_id', None) or hp.make_uuid3(service, hp.make_salt(), hp.time()),
                "eta": kwargs.pop('eta', None),
                "delay": kwargs.pop('delay', None),
                "status_callback": kwargs.pop('status_callback', None),
                "result_callback": kwargs.pop('result_callback', None),
                "error_callback": kwargs.pop('error_callback', None),
                "runtime_limit": kwargs.pop('runtime_limit', None),
                "result_expiry": kwargs.pop('result_expiry', None),
                "publisher": kwargs.pop('publisher', None) or get_module_name(2),
                "priority": kwargs.pop('priority', None),
                "input_queue": kwargs.pop("input_queue", None),
                "result_queue": kwargs.pop("result_queue", None),
                "result_queue_token": kwargs.pop("result_queue_token", None),
                "args": args,
                "kwargs": kwargs
            }
        def apply_async(*args, **kwargs):
            return create_async_task(
                function_name, 
                service,
                queue_manager = queue_manager,
                **create_job_task(*args, **kwargs)
            )
        def execute(*args, **kwargs):
            j = create_job_task(*args, **kwargs)
            j.update({
                "task": function_name, 
                "service": service,
                "task_id": hp.make_uuid3(j.get('job_id'), service, function_name),
            })
            return execute_task(j)
        
        func.apply_async = apply_async
        func.execute = execute
        
        @wraps(func)
        def decorator(*args, **kwargs):
            st = hp.time()
            r = func(*args, **kwargs)
            logger.info("Executing function {function_name} under {service} took {t} secs.".format(
                function_name = function_name, 
                service = service,
                t = hp.time() - st
            ))
            return r  # returning the results means func can still be used normally
        return decorator
    return is_a_task_inner


def create_async_task(
        function_name,
        service,
        args = None,
        kwargs = None,
        publisher = None,
        queue_manager = None,
        enterprise_id = "core", 
        job_id = None,
        eta = None,
        delay = None,
        published_timestamp = None,
        status_callback = None,
        result_callback = None,
        error_callback = None,
        runtime_limit = None,
        result_expiry = None,
        priority = None,
        input_queue = None,
        status_queue = None,
        error_queue = None,
        result_queue = None,
        result_queue_token = None,
    ):
    """
    publisher: the service which is calling this task, usually the calling module itself, otherwise the originator of the job
    enterprise_id: is required in most cases, otherwise audits, billing etc. will not work
    queue_manager: config is not required. If not provided, it will pick from global settings
    job_id: if this task is a part of the longer chain, then job id is required to be passed on from the publisher
    eta: expected time of execution in linux timestamp
    delay: time in seconds after now to execute the task
    result_callback: the URL to callback with the results
    status_callback: if status is available, we want to callback to a URL on status update, defaults to result callback
    error_callback: the URL to callback with the error, defaults to result_callback
    runtime_limit: max duration of the task after which the task will be killed, default 1 hour
    result_expiry: max duration of the result availability
    priority: usually None. But if specified, then we can configure different set of workers to take up the job for each priority
    result_queue_token: usually None, but if specified, then will be used as the partition to send the result to.
    args, kwargs: what is to be sent to the function
    """
    st = hp.time()
    publisher = publisher or get_module_name(1)
    queue_manager = queue_manager or get_queue_manager(module_name = publisher)
    job_id = job_id or hp.make_uuid3(service, hp.make_salt(), hp.time())
    input_queue = (input_queue or queue_manager._input_queue or "input-{service}".format(service = service))
    try:
        queue = queue_manager.send(
            "{input_queue}{ENVIRONMENT}".format(input_queue = input_queue, ENVIRONMENT = ENVIRONMENT),  {
            "job_id": job_id,
            "task_id": hp.make_uuid3(job_id, service, function_name, hp.make_salt(), hp.time()),
            "task": function_name,
            "eta": eta or (hp.time() + (delay or  0)),
            "args": args,
            "kwargs": kwargs,
            "service": service,
            "published_timestamp": published_timestamp or hp.time(),
            "publisher": publisher,
            "status_callback": status_callback,
            "result_callback": result_callback,
            "error_callback": error_callback,
            "runtime_limit": runtime_limit,
            "result_expiry": result_expiry,
            "result_queue": result_queue,
            "result_queue_token": result_queue_token
        }, partition = priority)
        logger.info("Task {function_name} created under service {service} with job_id {job_id} in {t} secs.".format(
            function_name = function_name, 
            service = service,
            job_id = job_id,
            t = hp.time() - st
        ))
        queue_manager.exit()
        return {'info': 'sent job to queue {input_queue}'.format(input_queue = input_queue)}
    except Exception as e:
        logger.error("Error while creating task {function_name} under service {service}".format(function_name = function_name, service = service), exc_info=True)
        raise


class JobSpecificationError(hp.I2CEError):
    pass


def verify_job_dict(job, raise_error = False):
    if not isinstance(job, dict):
        if raise_error:
            raise JobSpecificationError("Job is not of valid format")
        logger.error("Job {job} is not if valid format. Should be dict, not: {job}".format(job = job))
        return False
    if not all(_ in job for _ in ['job_id', 'task_id', 'task', 'service']):
        if raise_error:
            raise JobSpecificationError("Job is not of valid format")
        logger.error("Job is not of valid format, should contain job_id, task_id, task and service: {job}".format(job = job))
        return False
    task = job.get('task')
    service = job.get('service')
    if service not in LIST_OF_TASKS or task not in LIST_OF_TASKS[service]:
        if raise_error:
            raise TaskNotFound("Task {task} ({task_id}) is not a valid task in this module {service}".format(
                task = task,
                task_id = job.get('task_id'),
                service = service
            ))
        if task in RESERVED_FUNCTION_NAMES and LIST_OF_TASKS.get("{task}-scanner".format(task = task), {}).get(task):
            return True
        logger.error("Task {task} ({task_id}) is not a valid task in this module {service}".format(
            task = task, 
            task_id = job.get('task_id'), 
            service = service
        ))
        return False
    if service not in MODULES:
        if raise_error:
            raise TaskNotFound("Service {service} ({job_id}) is not a valid service in this module".format(service = service, job_id = job.get('job_id')))
        logger.error("Service {service} ({job_id}) is not a valid service in this module".format(service = service, job_id = job.get("job_id")))
        return False
    return True

def publish_result(job, result, key = 'result', start_timestamp = None, queue_manager = None, published_timestamp = None):
    """
    job needs job_id, task_id, task, service, which is the generator of the results
    """
    if not verify_job_dict(job):
        return
    task = job.get('task')
    service = job.get('service')
    logger.debug("Publishing {key}: {task} ({task_id}) in {service} (job {job_id}) published by {publisher}: {result}".format(
        key = key,
        task = task,
        task_id = job.get('task_id'),
        job_id = job.get("job_id"),
        publisher = job.get("publisher"),
        service = service,
        result = result
    ))
    do_exit = False
    if queue_manager is None:
        do_exit = True
    queue_manager = queue_manager or get_queue_manager(module_name = service)
    start_timestamp = start_timestamp or hp.time()
    result_dict = {
        "job_id": job.get('job_id'),
        "task_id": job.get('task_id'),
        "task": key,
        "{key}_to".format(key = key): task,
        "service": service,
        "publisher": job.get('publisher') or service,
        "published_timestamp": published_timestamp or start_timestamp,
        "start_timestamp": start_timestamp,
        "end_timestamp": hp.time(),
        "{key}_queue_token".format(key = key): job.get("{key}_queue_token".format(key = key)),
        key: result
    }
    def publish_result_():
        """
        Publishes the result to the result queue of the backedn set up for this service/module
        """
        qn = job.get('{key}_queue'.format(key = key)) or getattr(queue_manager, "_{key}_queue".format(key = key), None) or "{key}-{publisher}".format(
            key = key, 
            publisher = job.get('publisher') or service
        )
        qn = "{qn}{ENVIRONMENT}".format(qn = qn, ENVIRONMENT = ENVIRONMENT)
        try:
            queue_manager.send(
                qn, 
                result_dict, 
                partition = job.get('priority')
            )
            logger.debug("{key} for task {task} ({task_id}) under service {service} with job_id {job_id} was published in queue {qn}".format(
                key = key,
                task = task,
                task_id = job.get('task_id'),
                service = service,
                job_id = job.get('job_id'),
                qn = qn
            ))
        except Exception as e:
            logger.error("Error while publishing result for task {task} ({task_id}) under service {service} with job_id {job_id}".format(
                task = task,
                task_id = job.get('task_id'),
                service = service,
                job_id = job.get('job_id')
            ), exc_info=True)
            return False
    if isinstance(result, dict):
        try:
            hp.json.dumps(result)
        except ValueError as e:
            er = "The result of task {task} in service {service} could not be converted to valid json".format(task = task),
            publish_result(job, er, "error", start_timestamp = start_timestamp, queue_manager = queue_manager)
            logger.error(er, exc_info=True)
            return False
    publish_result_()
    url = job.get("{key}_callback".format(key = key))
    if isinstance(url, str) and url.startswith('http'):
        requests.post(url, json = result_dict)
    if do_exit:
        queue_manager.exit()
    return True



def execute_task(job):
    """
        "job_id": job_id,
        "task_id": make_uuid3(job_id, service, function_name),
        "task": function_name,
        "eta": eta or hp.time() + (delay or  0)
        "args": args,
        "kwargs": kwargs,
        "service": service,
        "publisher": module_name,
        "priority": <priority>,
        "status_callback": status_callback,
        "result_callback": result_callback,
        "error_callback": error_callback,
        "runtime_limit": runtime_limit,
        "result_expiry": result_expiry,
        "result_queue": result_queue,
        "status_queue": status_queue,
        "error_queue": error_queue,
        "result_queue_token": result_queue_token
    Executes the task and send the status and results.
    Also puts the results in the results queue
    """
    global LIST_OF_TASKS
    if not verify_job_dict(job):
        return
    task = job.get('task')
    if task in RESERVED_FUNCTION_NAMES:
        service = "{task}-scanner".format(task = task) 
        job['args'] = [hp.copyof(job)]
    else:    
        service = job.get('service')
    logger.info("Executing task: {task} ({task_id}) in {service} (job {job_id}) published by {publisher}".format(
        task = task,
        task_id = job.get('task_id'),
        job_id = job.get('job_id'),
        service = service,
        publisher = job.get('publisher')
    ))
    # Check if the listener is able to work on the task
    published_timestamp = job.get('published_timestamp')
    start_timestamp = hp.time()
    queue_manager = get_queue_manager(module_name = service)
    stc = job.get('status_callback')
    # Internal functions
    def send_result(result, status = "success"):
        if publish_result(job, result, key = "result", 
                start_timestamp = start_timestamp, 
                queue_manager = queue_manager, 
                published_timestamp = published_timestamp
            ):
            send_status(status)
    def send_status(result):
        publish_result(
            job, result, key = "status", 
            queue_manager = queue_manager, 
            start_timestamp = start_timestamp, 
            published_timestamp = published_timestamp
        )
    def send_error(result):
        publish_result(
            job, result, key = "error", 
            start_timestamp = start_timestamp, 
            queue_manager = queue_manager,
            published_timestamp = published_timestamp
        )
        send_status("error")
    def parse_results(r, status = "executing"):
        if isinstance(r, dict):
            if '_job' in r:
                jb = r.pop('_job')
                if 'task' in jb and 'service' in jb:
                    for k in [
                        'job_id', 'publisher', 'result_queue', 'status_queue', 'error_queue', 
                        'status_callback', 'result_callback', 'error_callback', 
                        'priority', 'result_queue_token'
                        ]:
                        if job.get(k):
                            jb[k] = job.get(k)
                        jb['queue_manager'] = queue_manager    
                    create_async_task(jb.pop('task'), jb.pop('service'), **jb)
            if '_result' in r:
                r = r.pop('_result')
        if r:
            send_result(r, status)
    # Needs work.. currently putting this task back in the queue until ETA. Will not work in the long run
    if start_timestamp < (job.get('eta') or 0) - 1:
        create_async_task(task, service, **job)
        send_status('queued')
        return
    if task in RESERVED_FUNCTION_NAMES:
        task_function = LIST_OF_TASKS["{task}-scanner".format(task = task)][task]
    else:    
        task_function = LIST_OF_TASKS[service][task]
    result = None
    try:
        send_status("started")
        st = hp.time()
        if inspect.isgeneratorfunction(task_function):
            for result in task_function(*(job.get('args') or []), **(job.get('kwargs') or {})):
                parse_results(result, "executing")
            result = None
        else:
            result = task_function(*(job.get('args') or []), **(job.get('kwargs') or {}))
    except Exception as ec:
        logger.error(str(ec), exc_info = True)
        send_error(str(ec))
    else:
        log = "Result for task {task} ({task_id}) under service {service} with job_id {job_id} was executed in {t} secs".format(
            task = task,
            task_id = job.get("task_id"),
            service = service,
            job_id = job.get("job_id"),
            t = hp.time() - st
        )
        if result is not None:
            logger.info(log + ":\n{}".format(hp.json.dumps(result, indent = 2)))
            parse_results(result)
        else:
            send_status("success")
    queue_manager.exit()
    return result


