#!/usr/bin/python
import argparse
import sys, time
import signal
import importlib
from os.path import exists as ispath, basename, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile, getmtime as modified_time
d = joinpath(dirname(dirname(dirname(abspath(__file__)))), 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
d = joinpath(dirname(dirname(dirname(abspath(__file__)))), 'models')
if d not in sys.path:
    sys.path.insert(0, d)
import gryd, helpers as hp
import multiprocessing
parser = argparse.ArgumentParser(
    prog='Worker to execute background tasks',
)

logger = hp.get_logger(__name__)
parser.add_argument('-m', '--module', help = "Path to the module you want to convert to a worker", required = True)
parser.add_argument('-q', '--queue_name', help = "Queue name the worker has to listen to", default = None)
parser.add_argument('-p', '--partition', help = "Partition to which the worker should be listening to", default = None)
parser.add_argument('-t', '--timeout', help = "How long to wait for an event before sleeping", default = None)
parser.add_argument('-s', '--sleep', help = "How long to sleep before we try to receive again", default = 1e-10, type=float)
parser.add_argument('-n', '--num_processes', help = "The number of processes to spawn", default = 1, type=int)

class ArgumentError(hp.I2CEError):
    pass

args = parser.parse_args()
if not isfile(args.module):
    raise ArgumentError("Path provided to worker should be a valid python file")
path_dir = dirname(abspath(args.module))
if path_dir not in sys.path:
    sys.path.append(path_dir)
module = splitext(basename(args.module))[0]
module = importlib.import_module(module)
queue_manager = module.gryd.get_queue_manager(module.gryd.get_module_name(module.__name__))


class GracefulKiller:
    kill_now = False
    def __init__(self, queue_manager, worker_pool):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)
        self.queue_manager = queue_manager
        self.worker_pool = worker_pool
    def exit_gracefully(self, signum, frame):
        self.kill_now = True
        self.queue_manager.exit()
        self.worker_pool.close()
        self.worker_pool.join()


if __name__ == '__main__':
    worker_pool = multiprocessing.Pool(processes = args.num_processes)
    killer = GracefulKiller(queue_manager, worker_pool)
    service = module.gryd.get_module_name(module.__name__)
    while not killer.kill_now:
        iq = args.queue_name or queue_manager._input_queue or 'input-{service}{env}'.format(service = service, env = gryd.ENVIRONMENT)
        logger.info("Now listening to queue: %s in service: %s", iq, service)
        try:
            for job in queue_manager.listen(iq, service = service, timeout = args.timeout, partition = args.partition):
                worker_pool.apply_async(gryd.execute_task, args = (job, ))
            time.sleep(args.sleep)
        except Exception as e:
            hp.print_error()
