import boto3
import sys
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile, getmtime as modified_time
BASE_DIR_PATH = dirname(dirname(dirname(abspath(__file__))))
if BASE_DIR_PATH not in sys.path:
    sys.path.append(BASE_DIR_PATH)
d = joinpath(dirname(dirname(dirname(dirname(abspath(__file__))))), 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
d = joinpath(dirname(dirname(dirname(dirname(abspath(__file__))))), 'models')
if d not in sys.path:
    sys.path.insert(0, d)
import helpers as hp
from .base import Backend
import boto3
from botocore import errorfactory
from botocore.exceptions import ClientError
logger = hp.get_logger(__name__)

hp.logging.getLogger('boto3').setLevel(hp.logging.CRITICAL)
hp.logging.getLogger('botocore').setLevel(hp.logging.CRITICAL)
hp.logging.getLogger('s3transfer').setLevel(hp.logging.CRITICAL)
hp.logging.getLogger('urllib3').setLevel(hp.logging.CRITICAL)

class SQSGrydBackend(Backend):

    customCallbacks = {}

    def __init__(self, **backend_config):
        self.sqs = boto3.resource('sqs')
        self._message_delay = backend_config.pop('message_delay', 0)
        self._message_visibility_timeout = backend_config.pop('message_visibility_timeout', 10)
        self._message_retention = backend_config.pop('message_retention', 1209600) 
        super(SQSGrydBackend, self).__init__(**backend_config)
        self.sio = None

    def get_config(self):
        return self._config

    def wsSetup(self, roomname, username = None):
        if not roomname.endswith('.fifo'):
            roomname = "{roomname}.fifo".format(roomname = roomname)
        if self.sio:
            logger.debug("Is already connected to queue")
            return
        try:
            self.sio = self.sqs.get_queue_by_name(QueueName=roomname)
        except errorfactory.ClientError as e:
            self.sio = self.sqs.create_queue(QueueName=roomname, Attributes={
                'DelaySeconds' : str(self._message_delay),
                'VisibilityTimeout': str(self._message_visibility_timeout), 'ReceiveMessageWaitTimeSeconds': str(self._timeout),
                'MessageRetentionPeriod': str(self._message_retention),
                'FifoQueue': 'true'
            })

    def send(self, queue_name, data, partition = None, publisher = None, trials = 2):
        partition = partition or "default"
        publisher = publisher or data.get('publisher') or data.get('service') or self.id_generator()
        self.wsSetup(queue_name, publisher)
        try:
            self.sio.send_message(MessageBody = hp.json.dumps(data).decode('utf-8'), MessageAttributes = {
                    'publisher': { 'StringValue': publisher, 'DataType': 'String' },
                    'partition': {'StringValue': partition, 'DataType': 'String' },
                },
                MessageGroupId = str(data.get('job_id') or hp.make_uuid3(hp.make_salt(), hp.time())),
                MessageDeduplicationId = str(data.get('task_id') or  hp.make_uuid3(hp.make_salt(), hp.time()))
            )
        except Exception as e:
            hp.print_error(e)
            if trials > 0:
                logger.warning("Trying to send message again with {trials} attempt(s) left.".format(trial = trials))
                self.send(queue_name, data, partition, publisher, trials - 1)
            else:
                raise
        self.sio = None    

    def listen(self, queue_name, service = None, timeout = None, partition = None, max_num_messages = None, delete_condition = None):
        self.wsSetup(queue_name)
        try:
            num_messages = 0
            for message in self.sio.receive_messages(MessageAttributeNames=['publisher', 'partition'], 
                WaitTimeSeconds = timeout or self._timeout,
                MaxNumberOfMessages = min(max_num_messages or 10, 10) ):
                num_messages += 1
                if message.message_attributes is None:
                    continue
                if partition and message.message_attributes.get('partition', {}).get('StringValue') != partition:
                    continue
                logger.info("Received message in queue %s with publisher: %s and partition: %s", queue_name, message.message_attributes.get('publisher', {}).get('StringValue'), message.message_attributes.get('partition', {}).get('StringValue'))
                evd = message.body
                if not isinstance(evd, str):
                    evd = str(evd, 'utf-8')
                try:
                    evd = hp.json.loads(evd)
                    yield evd
                except ValueError as exc:
                    yield evd
                if not callable(delete_condition) or delete_condition(evd):
                    message.delete()
        except ClientError as e:
            hp.print_error(e)
        if not num_messages:    
            logger.info("No more messages in queue %s for now", queue_name)
        self.sio = None    

    def exit(self):
        self.sio = None
