#!/usr/bin/python
import sys, inspect
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile, getmtime as modified_time
BASE_DIR_PATH = dirname(dirname(abspath(__file__)))
if BASE_DIR_PATH not in sys.path:
    sys.path.append(BASE_DIR_PATH)
d = joinpath(dirname(dirname(dirname(dirname(abspath(__file__))))), 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
d = joinpath(dirname(dirname(dirname(dirname(abspath(__file__))))), 'models')
if d not in sys.path:
    sys.path.insert(0, d)
import helpers as hp
from .sqs_backend import SQSGrydBackend
from .base import GrydBackendError, Backend

def validate_queue_manager(queue_manager, raise_error = True):
    """
    Validates if the queue_manager data is provided in the correct format. Raises appropriate errors
    """
    global VALID_BROKERS
    if not isinstance(queue_manager, dict):
        raise GrydBackendError("Backend {queue_manager} is not for proper format")
    if queue_manager.get("broker_type") not in VALID_BROKERS:
        raise GrydBackendError("Broker Type: {bt} is not valid".format(bt = queue_manager.get('broker_type')))



VALID_BACKENDS = {
    "sqs": SQSGrydBackend
}
MODULES = {}

def read_queue_manager(path = None):
    if path:
        with hp.read_file(path) as j:
            return j
    return {
        "broker_type": "sqs",
    }

def set_queue_manager(module, queue_manager = None, config_path = None):
    global MODULES
    queue_manager = queue_manager or read_queue_manager(path = config_path)
    MODULES[module] = queue_manager
    return queue_manager


def get_queue_manager(module_name = None, function = None, stack_depth = 1, raise_error = True):
    """
    get the queue_manager from module_name, or from function
    If module and function is not provided, then we can use the inspect stack depth to identify the queue_manager
    If the queue_manager is not found, we can raise an error if raise_error is True
    """
    global MODULES
    if module_name is None:
        frm = inspect.stack()[stack_depth]
        function = frm[0]
        module = inspect.getmodule(function)
        module_name = module.__name__
    queue_manager = MODULES.get(module_name)
    if raise_error and queue_manager is None:
        raise GrydBackendError("No Backend defined for module {module_name}, available MODULES: {MODULES}".format(module_name = module_name, MODULES = MODULES))
    if not isinstance(queue_manager, dict):
        raise GrydBackendError("Queue manager config is not a dict but {tqm}".format(tqm = type(queue_manager)))
    if not 'broker_type' in queue_manager and queue_manager['broker_type'] in VALID_BACKENDS:
        raise GrydBackendError("Queue manager config broker type is {} is not a valid broker".format(queue_manager.get('broker_type')))
    queue_manager_inst = VALID_BACKENDS[queue_manager['broker_type']]

    if not isinstance(queue_manager_inst, Backend):
        # if uninitialized, then we initialize it with the config
        queue_manager_inst = queue_manager_inst(**queue_manager)
        VALID_BACKENDS[queue_manager['broker_type']] = queue_manager_inst
    return queue_manager_inst
