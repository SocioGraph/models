#!/usr/bin/python
import sys
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile, getmtime as modified_time
BASE_DIR_PATH = dirname(dirname(dirname(abspath(__file__))))
if BASE_DIR_PATH not in sys.path:
    sys.path.append(BASE_DIR_PATH)
d = joinpath(dirname(dirname(dirname(dirname(abspath(__file__))))), 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
d = joinpath(dirname(dirname(dirname(dirname(abspath(__file__))))), 'models')
if d not in sys.path:
    sys.path.insert(0, d)
import helpers as hp
import string, random
class GrydBackendError(hp.I2CEError):
    pass

class Backend(object):
    def __init__(self, **backend_config):
        self._config = backend_config
        self.validate(backend_config)
        self._config = backend_config
        self._timeout = backend_config.get('timeout', 10)
        self._input_queue = backend_config.get('input_queue')
        self._result_queue = backend_config.get('result_queue')
        self._error_queue = backend_config.get('error_queue')
        self._status_queue = backend_config.get('status_queue')

    def validate(self, backend_config):
        return True

    def exit(self):
        pass

    def send(self, queue_name, service):
        pass

    def listen(self, queue_name, service, timeout = None, partition = None, **kwargs):
        pass

    def wait(self):
        pass
    
    def id_generator(self,size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))
