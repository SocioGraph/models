import gryd
import time
gryd.SERVICE = 'sample_service'

# In actual application, please just pass the base path of the application to this function
gryd.set_queue_manager(config = {
    "broker_type": "sqs"
})

@gryd.is_a_task()
def test_function(a = 1, b = 2):
    time.sleep(a)
    return a*b

@gryd.is_a_task("my_publisher")
def example_publisher(data):
    yield  {"my_results": "partial results 1"}
    yield  {"my_results": "partial results 2"}
    yield  {"my_results": "partial results 3"}
    yield  {"my_results": "partial results 4"}
    yield  {    
              "_job": {"task": "test_function", "service": "sample_service", "args": [], "kwargs": {'a': 7, 'b': 1.3}},
              "_result": { "my_results": "partial results 5"}
            }



if __name__ == "__main__":
    # create an async task with your own service
    example_publisher.execute({'my_data': 1})
    test_function.apply_async(a = 10, b = 20)
    #create a task for another service 
    gryd.create_async_task('test_function', 'sample_service', kwargs = {'a': 1, 'b': 2})
    gryd.create_async_task('test_function', 'sample_service', kwargs = {'a': 3, 'b': 3})
    gryd.create_async_task('test_function', 'sample_service', kwargs = {'a': 5, 'b': 4})
    #Send results of a asyncronously
    gryd.publish_result({
        "job_id": "myjob",
        "task_id": "mytask_id",
        "task": "test_function",
        "service": gryd.SERVICE
    }, {
        "my_result": "this is the result"
    })
