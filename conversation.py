#!/usr/bin/python
import os, sys, codecs
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile
d = dirname(dirname(abspath(__file__)))
if d not in sys.path:
    sys.path.append(d)
d = joinpath(dirname(dirname(abspath(__file__))), 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
import json, types
import algorithms
from jinja2 import exceptions as jexc
from functools import wraps
import helpers as hp
import model as base_model
from objects import run_filter
import validators as val
from datetime import datetime,timedelta,date
import time
import traceback
from stuf import stuf
from collections import OrderedDict
import matcher
import pgpersist as db
from nlpt import NlpModel
import pytz
import random
import action
import requests
try:
    from cStringIO import StringIO      # Python 2
except ImportError:
    from io import StringIO
from stuf import stuf
from celery import Celery
import re
from pgp_db import JsonBDict
from flask import Response, jsonify, request, session, flash, redirect, url_for, Flask, render_template, render_template_string, current_app as app
import synthesize
import recognize
import translate

logger = hp.get_logger(__name__)

CELERY_CONFIG = {  
    'CELERY_BROKER_URL': 'redis://{}/{}'.format(os.environ.get('REDIS_URL', 'localhost:6379'), os.environ.get('REDIS_DB_NUMBER',0)),
    'CELERY_TASK_SERIALIZER': 'pickle',
    'BROKER_USE_SSL': os.environ.get('BROKER_USE_SSL', 'False').lower() == 'true',
}
celery = Celery(__name__, broker=CELERY_CONFIG['CELERY_BROKER_URL'])
celery.conf.update(CELERY_CONFIG)
NoneType = type(None)
NUM_SIMILAR = 50

class CustomerStateError(hp.I2CEError):
    pass

class EntityFormatError(hp.I2CEError):
    pass

class UnknownSystemResponse(hp.I2CEError):
    pass

class UnknownVariable(hp.I2CEError):
    pass

class UnknownFunction(hp.I2CEError):
    pass

class ResponseStringError(hp.I2CEError):
    pass

class UnknownConversation(hp.I2CEError):
    pass

class FunctionError(hp.I2CEError):
    pass

class UnknownFormat(hp.I2CEError):
    pass

class UnknownState(hp.I2CEError):
    pass

class FileSizeExceeded(hp.I2CEError):
    pass

class ConversationFunctionError(hp.I2CEError):
    pass

class StateOptionError(hp.I2CEError):
    pass

class OneTimeStateError(hp.I2CEError):
    pass

class ResponseOptionError(hp.I2CEError):
    pass

class InvalidEngagementIdError(hp.I2CEError):
    pass

# These should be set during the entirety of the conversation, don't mess with these
SPECIAL_CONVERSATION_DATA_KEYS = [
    "_allow_unsure",
    "_boost_states",
    "_default_state_options",
    "_buttons",
    "_state_options_as_list",
    "_disallow_spell_check",
    "_best_of_unsure",
    "_force_old_pipeline",
    "_disable_conversation_cache",
    "_skip_all_synthesize",
    "_skip_parse_num_words"
]

# These are going to be purged at the end of the response, please don't set these variables
SPECIAL_ENGAGEMENT_DATA_KEYS = [
    "_unsure_state_options",
    "_custom_placeholder",
    "_custom_state_options",
    "_add_state_options",
    "_remove_state_options",
    "_registered_entities",
    "_parse_results",
    "_skip_synthesize"
]

default_system_response = [
    {
        "name": "unknown_customer_state",
        "placeholder": {
            "I did not understand what you meant. Can you rephrase that?": 1.0,
            "I don't have an answer to that. Can you rephrase what you said?": 1.0,
            "I'm still learning about the world. Can you ask me to do something simpler?": 1.0,
            "I'm not sure what you mean. Can you rephrase that?": 1.0,
            },
        "state_options": []
    },
    {
        "name": "unsure_customer_state",
        "placeholder": {"{_custom_placeholder}": 1.0},
        "state_options": []
    }
]

default_customer_state = [
        {
            "name": "unknown_customer_state",
            "title": "Unknown Customer State",
            "follow_up": "unknown_customer_state",
            "positivity_score": 0,
            "show_in_keywords": False
        },
        {
            "name": "unsure_customer_state",
            "title": "Unsure Customer State",
            "follow_up": "unsure_customer_state",
            "positivity_score": 0,
            "show_in_keywords": False
        }
]

RESERVED_KEYWORDS = {
    "__previous__": ["previous", "go back", "back", "last page", "previous page"],
    "__init__": ["start over", "restart", "start from first", "start again", "cancel", "start from beginning", "begin again", "start from the beginning"]
}
RESERVED_STATES = dict(reduce(lambda x, y: x+y,([(v1, k) for v1 in v]for k, v in RESERVED_KEYWORDS.iteritems()), []))


def mail_error(enterprise_id, conversation_id, message, title=None,act=None):
    act = act or action.Action(
                enterprise_id, "admin", 
                data = action.setup(None, enterprise_id, 'admin', None),
            )


    conversation = base_model.Model("conversation", enterprise_id, "admin").get(conversation_id)
    dev_email = conversation.get("developer_email")
    if dev_email:
        emails = [dev_email]
    else:
        admins = base_model.Model("admin", enterprise_id, "admin").filter(enterprise_id=enterprise_id)
        emails = list(map(lambda x: x["user_id"], admins))
        emails.append("dev@iamdave.ai")
    logger.info("Sending error reports mail to %s", emails)
    mail_data = {
        '_action': 'communicate',
        '_name': "Training failed alert", 
        "receiver": {
            "emails": emails
            },
        'message': title or "Conversation Training Failed",
        "html_string":u"""
            <h4>Training failed for conversation</h4>
            <br />
            <br />
            Server Name : {}
            <br />
            Enterprise id : {}
            <br />
            Conversation id : {}
            <br />
            Timestamp: {}
            <br />
            Error Message: {}
        """.format(os.environ.get('SERVER_NAME', 'localhost:5000'), enterprise_id, conversation_id,  datetime.now(), message),
        "title": title or "Conversation Training Failed",
        "channels": ["email"],
        'enterprise_id': enterprise_id,
        'enterprise_name' : act.enterprise['name']
        }
    logger.info("Message :: %s", json.dumps(mail_data, indent=2))
    action.do_action(act.to_init(get_data = False), mail_data)

def mail_success(enterprise_id, conversation_id, message="Trained successfully", title=None,act=None):
    act = act or action.Action(
                enterprise_id, "admin", 
                data = action.setup(None, enterprise_id, 'admin', None),
            )
    conversation = base_model.Model("conversation", enterprise_id, "admin").get(conversation_id)
    dev_email = conversation.get("developer_email")
    if dev_email:
        emails = [dev_email]
    else:
        admins = base_model.Model("admin", enterprise_id, "admin").filter(enterprise_id=enterprise_id)
        emails = list(map(lambda x: x["user_id"], admins))
        emails.append("dev@iamdave.ai")

    logger.info("Sending success mail to %s", emails)
    mail_data = {
        '_action': 'communicate',
        '_name': "Conversation training done successfully", 
        "receiver": {
            "emails": emails
            },
        'message': title or "Conversation training done successfully",
        "html_string":u"""
            <h4>Conversation training successfull</h4>
            <br />
            <br />
            Server Name : {}
            <br />
            Enterprise id : {}
            <br />
            Conversation id : {}
            <br />
            Timestamp: {}
            <br />
            Message: {}
        """.format(os.environ.get('SERVER_NAME', 'localhost:5000'), enterprise_id, conversation_id,  datetime.now(), message),
        "title": title or "Conversation training done successfully",
        "channels": ["email"],
        'enterprise_id': enterprise_id,
        'enterprise_name' : act.enterprise['name']
        }
    logger.info("Message :: %s", json.dumps(mail_data, indent=2))
    action.do_action(act.to_init(get_data = False), mail_data)




@action.add_action_method('delete_from_entities')
def delete_from_entities(act, name, entities, object_json=None, pre_object_json=None, **kwargs):
    x = NlpModel(name, db_name=act.enterprise_id)
    object_json = object_json if isinstance(object_json, (str, unicode)) else pre_object_json
    for i in entities:
        v = object_json.get(i["attribute"])
        x.remove_words_phrases(i["name"], v )

@action.add_action_method('add_to_entities')
def add_to_entities(act, name, entities, object_json, pre_object_json, default_weight=0.4, **kwargs):
    st = time.time()
    x = NlpModel(name, db_name=act.enterprise_id)
    previous_data = pre_object_json if isinstance(pre_object_json, dict) else {} 

    logger.info("Updating the entites %s", previous_data)
    for i in entities:
        v = object_json.get(i["attribute"])
        if i.get('filters') and not run_filter(object_json, i['attribute'], i.get('filters')):
            continue
        pv = previous_data.get(i["attribute"])
        if v and pv != v:
            if pv:
                logger.info("Removing previous phrase: %s = %s", i['name'], pv)
                x.remove_words_phrases(i["name"], pv)
            x.add_words(i["name"], v, i.get("weight", default_weight) , mkey="entities")
            for v1 in hp.make_list(v):
                x.add_rm_doc_freq(v1, "__{}__".format(i['name']))

    logger.info("Time taken to add to entities for model %s is %s", name, time.time() - st)


@celery.task(name= 'update_nlentities')
def update_nlentities(name, enterprise_id, entities, old_entities, phrases, old_phrases, retrain = False, refresh_cache = False, ner_enabled = False):
    logger.info("name: %s enterprise_id: %s", name, enterprise_id)
    x = NlpModel(name, db_name=enterprise_id, ner_enabled = ner_enabled)
    try:
        if retrain:
            logger.info("Retrain True")
            x.delete_all()
            x = NlpModel(name, db_name=enterprise_id, ner_enabled = ner_enabled)
        m_entity= {}
        for i in entities:
            logger.info("Training entity: %s, %s", i["name"], i.get("values", i.get('model')))
            if "model" in i:
                if i["model"] not in m_entity:
                    m_entity[i["model"]] = []
                if i['name'] not in map(lambda x: x['name'], m_entity[i["model"]]):
                    m_entity[i["model"]].append(i)
                    x.add_entity(i["name"], {}, i.get("weight", 0.5) )
            else:
                x.add_entity(i["name"], i["values"], i.get("weight", 0.2))
                for vi in i["values"]:
                    x.add_rm_doc_freq(vi, "__{}__".format(i["name"]))

        for k, v in old_entities.iteritems():
            logger.info("Removing entity: %s, %s", k, v)
            if not (k.startswith('__') and k.endswith('__')):
                k = u'__{}__'.format(k)
            if isinstance(v, dict):
                x.remove_words_phrases(v.keys(), k)
            else:
                x.remove_words_phrases(v, k)

        _name = "nltraing_{}".format(name)
        for i, en in m_entity.iteritems():
            m = base_model.Model(i, enterprise_id)

            with open(m.filepath, "r") as fp:
                js_obj = json.load(fp)

                for actype in ["write", "delete"]:
                    _act = "add_to_entities" if actype == "write" else "delete_from_entities"
                    acts = []
                    if actype in js_obj["actions"]:
                        acts = filter(lambda x: x.get("_name", "") != _name, js_obj["actions"][actype])

                    ac_obj = {
                            "_action": "_function",
                            "_function": _act,
                            "_name": _name,
                            "kwargs":{
                                "name" : name,
                                "entities": en,
                                "object_json": "{__data}",
                                "pre_object_json": "{_previous_instance}"
                                }
                            }
                    acts.append(ac_obj.copy())
                    js_obj["actions"][actype] = acts
            base_model.post(i, enterprise_id, model=js_obj)

        for k, ph in phrases.iteritems():
            logger.info("Adding phrase: %s, %s", k, ph)
            x.add_phrase(k, ph)

        for k, ph in old_phrases.iteritems():
            logger.info("Removing phrase: %s, %s", k, ph)
            if not (k.startswith('__') and k.endswith('__')):
                k = u'__{}__'.format(k)
            x.remove_words_phrases(ph.keys(), k)

        for i, en in m_entity.iteritems():
            m = base_model.Model(i, enterprise_id)
            for i1 in en:
                c = 0
                logger.info("Working on entity %s for model %s", i1['name'], i)
                for ob in m.yield_list(**i1.get("filter", {})):
                    if i1["attribute"] in ob and ob[i1["attribute"]] is not None:
                        v1 = hp.make_list( ob.get(i1["attribute"], []) )
                        if v1:
                            x.add_words(i1["name"], v1, i1.get("weight"), mkey="entities")
                            for v2 in v1:
                                x.add_rm_doc_freq(v2, "__{}__".format(i1['name']))
                    c += 1
                    if c % 100 == 0:
                        logger.info("%s words to %s in conv %s, ent_id: %s", c, i1['name'], name, enterprise_id)


    except Exception as ex:
        ms_ = hp.print_error(ex)
        mail_error(enterprise_id, name, ex.message + '\n\n\n' + '\n'.join(ms_))
        raise ex

    x._clear_cache_by_state()
    if refresh_cache:
        if isinstance(refresh_cache, (str, unicode)):
            x = NlpModel(name, db_name = enterprise_id, ner_enabled = ner_enabled)
            try:
                ops = json.loads(refresh_cache)
            except ValueError:
                logger.error("Json error while decoding refresh_cache")
                pass
            else:
                for k, v in ops.iteritems():
                    x._response_cache[k] = v
        else:
            clear_cache.delay(name, enterprise_id, ner_enabled)
    mail_success(enterprise_id, name)

@celery.task(name = 'clear_cache')
def clear_cache(name, enterprise_id, ner_enabled, **kwargs):
    x = NlpModel(name, db_name = enterprise_id, ner_enabled = ner_enabled)
    return x.clear_cache(parser_task = parse, **kwargs)

@celery.task(name = 'parse')
def parse(name, db_name, ner_enabled, remaining, *args, **kwargs):
    logger.info("Reamining to re-cache %s", remaining)
    x = NlpModel(name, db_name = db_name, ner_enabled = ner_enabled)
    x.parse(*args, **kwargs)

def to_meta_id(*args):
    return '__'.join(args)

def _add_function(cls, function_name, func):
    if isinstance(func, (str, unicode)):
        try:
            cls.functions[function_name] = eval(func)
        except Exception as e:
            raise FunctionError("Error while evaluating function {} in conversation {} under entperise {}".format(
                function_name, cls.conversation_id, cls.enterprise_id
                )
                )
    elif hasattr(func, '__call__'):
        cls.functions[function_name] = func


MAX_CACHE = 10
class MetaConversation(type):
    """
    Don't reload unless it has been updated
    """
    all_instances = {}
    all_data = {}
    def __call__(cls, enterprise_id, conversation_id, role = 'admin', user_id = None, conversation_obj= None, act=None):
        _conversation_id = to_meta_id(enterprise_id, conversation_id)

        if _conversation_id in MetaConversation.all_instances:
            _created_time = MetaConversation.all_instances[_conversation_id][1]
            logger.info("Conversation already loaded with created time: %s", _created_time)
            conversation_obj = base_model.Model("conversation", enterprise_id).get(conversation_id, internal = True)
            if conversation_obj and conversation_obj.get("updated_time") <= _created_time:
                logger.info("Conversation not updated, meanwhile updated time: %s", conversation_obj.get("updated_time"))
                r = MetaConversation.all_instances[_conversation_id][2]
                if (_conversation_id, role, user_id) in MetaConversation.all_data:
                    logger.info("Getting Conversation data from cache")
                    r.act = MetaConversation.all_data[(_conversation_id, role, user_id)][2]
                    r.user_id = user_id
                    r.role = role
                    r.act.data = hp.copy(r._clean_data)
                    r.act.format_data = hp.copy(r._clean_format_data)
                    r.act.flattened_data = hp.copy(r._clean_flattened_data)
                    r.act.format_keys = hp.copy(r._clean_format_keys)
                    r.data = r.act.data
                else:
                    logger.info("Creating conversation data again for {}-{}-{}".format(_conversation_id, role, user_id))
                    r.create_data(role, user_id)
                return r
        # Check for updated function file
        r = type.__call__(cls, enterprise_id, conversation_id, role, user_id, conversation_obj)
        now = hp.to_datetime(hp.now())
        logger.info("Created time for cache is : %s", now)
        MetaConversation.all_instances[r._meta_conversation_id] = (r.conversation.get('updated_time'), now, r)
        MetaConversation.all_data[(r._meta_conversation_id, role, user_id)] =  (r.conversation.get('updated_time'), now, r.act)

        l = len(MetaConversation.all_instances)
        if l > MAX_CACHE:
            for k, v in sorted(MetaConversation.all_instances.iteritems(), key = lambda k: k[1][1])[:l-MAX_CACHE]:
                MetaConversation.all_instances.pop(k, None)
        l = len(MetaConversation.all_data)
        if l > MAX_CACHE * 50:
            for k, v in sorted(MetaConversation.all_data.iteritems(), key = lambda k: k[1][1])[:l-MAX_CACHE]:
                MetaConversation.all_data.pop(k, None)
        return r

class Engagement(object):

    def __init__(self, enterprise_id, conversation_id, customer_id, engagement_id = None, role = 'admin', user_id = None, **kwargs):
        self.enterprise_id = enterprise_id
        self.enterprise_model = base_model.Model("enterprise", "core")
        self.customer_id = customer_id
        self.role = role
        self.user_id = user_id
        self.engagement_model = base_model.Model('engagement', self.enterprise_id)
        self.conversation_id = conversation_id
        self.engagement_id = engagement_id or hp.make_uuid3(customer_id, conversation_id)
        self.history_model = base_model.Model('conversation_history', self.enterprise_id)
        self._setup()

    def _setup(self):
        self.conversation = Conversation(self.enterprise_id, self.conversation_id, role = self.role, user_id = self.user_id)
        self.engagement = self.engagement_model.get(self.engagement_id)
        if not self.engagement:
            logger.info("No engagement with id: %s", self.engagement_id)
            self.engagement = {
                    'customer_id': self.customer_id,
                    'conversation_id': self.conversation_id,
                    'engagement_id' : self.engagement_id
                    }
            self.engagement = self.engagement_model.post(self.engagement)
            self.engagement_id = self.engagement['engagement_id']

        self.data = self.engagement.get("data") or {}
        self.previous_data = self.engagement.get("previous_data",{})
        self.excluded_states = self.engagement.get("excluded_states", []) 
        self.act = self.conversation.act
        self.act = action.Action(
            self.enterprise_id, self.role, 
            user_id = self.user_id,
            data = hp.copy(self.conversation.act.data), 
        )
        self.act.add_to_data(self.data, 'engagement_data')
        self.act.add_to_data(self.data)
        self.act.add_to_data(self.customer_id, 'customer_id')
        self.act.add_to_data(self.engagement_id, 'engagement_id')
        self.act.add_to_data(self.excluded_states, 'excluded_states') 
        self.customer = self.act.data["customer_model"].get(self.customer_id)
        self.required_variables = self.data.pop("required_variables",[])
        self.stream = StringIO()
        self.logger = hp.get_logger("conversation_functions_debug", 
            level = 'DEBUG' if self.conversation.conversation.get('debug_mode') else 'INFO', stream = self.stream
        )
        self.logger.addHandler(logger.handlers[0])
        self.logger.addHandler(hp.logging.StreamHandler(self.stream))
        self._set_response_defaults()

    def get_history(self, reverse = True, _page_size = 20, _page_number = 1, _sequence = None, **kwargs):
        _page_size = int(_page_size or kwargs.get('limit') or 20)
        _page_number = int(_page_number or  1)
        add_key = {}
        _sequence = _sequence or kwargs.get('sequence')
        if _sequence:
            add_key = {'sequence': ',{}'.format(_sequence)}
            _page_number = 1
        wlst = self.history_model.list(
            _as_option = True, engagement_id = self.engagement_id, _sort_by = 'sequence', _sort_reverse = reverse,
            _page_size = _page_size,
            _page_number = _page_number,
            **add_key
        )
        if reverse:
            wlst = reversed(wlst)
        for h in wlst:
            if isinstance(h.get('customer_state'), dict):
                customer_state = h.get('customer_state').keys()[0]
            else:
                customer_state = h.get('customer_state')
            if customer_state and self.conversation.customer_states.get(
                    customer_state, {}
                ).get('show_in_history', True):
                yield {
                        "direction": "user",
                        "customer_response": h.get('customer_response') or self.conversation.customer_states.get(customer_state or '__init__', {}).get('title', (customer_state or '__init__').replace('_', ' ').strip().title()),
                        "customer_state": customer_state,
                        "user_id": h.get('agent_id') or self.customer_id,
                        "user_model": h.get('agent_model') or self.customer_model,
                        "timestamp": h.get("start_timestamp") or h.get('timestamp'),
                        "response_id": h.get("response_id"),
                        "sequence": h.get('sequence')
                }
            if h.get('system_response') and self.conversation.system_responses.get(h['system_response'].get("name"), {}).get('show_in_history', True):
                yield {
                    "direction": "system", 
                    "name": h["system_response"].get("name"),
                    "customer_state": customer_state,
                    "placeholder": h['placeholder'],
                    "data": h["system_response"].get("data", {}),
                    "title": h["system_response"].get("title"),
                    "options": h["system_response"].get("options"),
                    "state_options": h["system_response"].get("state_options"),
                    "whiteboard": h["system_response"].get("whiteboard"),
                    "placeholder_aliases": h["system_response"].get("placeholder_aliases"),
                    "timestamp": h["timestamp"],
                    "response_id": h.get("response_id"),
                    "sequence": h.get('sequence')
                }
            elif h.get('agent_id') and h.get('agent_id') != self.customer_id:
                yield {
                        "direction": "other",
                        "user_id": h.get("agent_id"),
                        "user_name": h.get("user_name") or hp.make_title(h.get("agent_id")),
                        "user_icon": h.get("user_icon"),
                        "reply_to": h.get("reply_to"),
                        "data": h.get("data"),
                        "placeholder": h.get("placeholder"),
                        "timestamp": h.get("timestamp"),
                        "response_id": h.get("response_id"),
                        "sequence": h.get("sequence")
                }

    def get_previous_system_response(self, ind=-2):
        st = hp.time()
        c = self.history_model.count(engagement_id = self.engagement_id)
        logger.info("Time taken to count the history model %s", hp.time() - st)
        if ind < 0:
            ind = c + 1 + ind
        if ind <= 0:
            d, p = self._get_response_from_state(None,'__init__')
            return d
        r = hp.make_single(
            self.history_model.list(
                _as_option = True, 
                engagement_id = self.engagement_id, 
                sequence = ind,
                _page_size = 1
            ),
            default = {}
        ).get('system_response')
        if r:
            return r
        d, p = self._get_response_from_state(None,'__init__')
        return d


    def run_actions(self, customer_state):
        actions = self.conversation.customer_states[customer_state].get('actions')
        if actions:
            for a in actions:
                if a.get("_instance",{}).pop("_async", False):
                    action.do_action.delay(self.act.to_init(), a, raise_error = True)
                else:
                    r = self.act.act(a, raise_error = True)
                    if a.get('_name'):
                        self.set(r, a['_name'])

    def _required_response(self,customer_state=None):
        while self.required_variables:
            rv = self.required_variables.pop()
            customer_state = rv[0]
            if customer_state not in self.data:
                self.save(self.required_variables, "required_variables") 
                if rv[1] is None:
                    return self._make_system_response(
                            self.customer_id, self.agent_id, customer_state, 
                            )
                else:
                    return self._generate_system_response(rv[1])

            if customer_state in self.conversation.customer_states and not self.required_variables:
                self.run_actions(customer_state)

        if customer_state:
            self.run_actions(customer_state)
            rvs = self.conversation.customer_states[customer_state].get("required_variables", {})
            for v, sr in rvs.iteritems():
                if v in self.data:
                    rvs.pop(v)
            rvs = list((v, sr) for v,sr in rvs.iteritems())
            if rvs:
                self.required_variables += [(customer_state, None)] + rvs
                return self._required_response()

            return self._make_system_response(
                    self.customer_id, self.agent_id, customer_state, 
                    ) 
        return self._generate_system_response("unsure_customer_state")

    def _get_response_from_state(self, system_response, customer_state, customer_response = None):
        if not customer_state in self.conversation.customer_states:
            raise StateOptionError("No customer state {} in conversation {}".format(customer_state, self.conversation_id))
        if customer_response:
            self.save(customer_response, customer_state)
        self._set_response_defaults()

        return self._required_response(customer_state)

    def _set_response_defaults(self):
        self.save(self.customer, 'customer_profile')
        for n, en in self.conversation.entities.iteritems():
            if "initial_value" in en:
                self.save(en["initial_value"], n)

    def get(self, name, default = None):
        r = self.data.get(name) 
        if r is not None:
            return r
        r = self.act.flattened_data.get(name)
        if r is not None: 
            return r
        r = self.conversation.act.data.get(name)
        if r is not None: 
            return r
        r = self.conversation.act.flattened_data.get(name)
        if r is not None: 
            return r
        return default

    def pop(self, name, default = None, purge = False):
        """
        Pops temporarily
        """
        r1 = self.data.pop(name, None)
        if not purge:
            return r1 if r1 != None else default
        if self.act.data.get('engagement_data'):
            ed = self.act.data['engagement_data']
            r2 = ed.pop(name, None)
            if r1 != None:
                return r1
            if r2 != None:
                return r2
        return default

    def purge(self, name, default = None):
        """
        Pops Permanently
        """
        return self.pop(name, default, True)

    def add_to_data(self, data, name = None, **kwargs):
        if name is None and not isinstance(data, dict):
            raise action.ActionError("Data %s should be of type dict or name should be provided", data)
        self.set(data, name)
        if not isinstance(self.act.data.get('engagement_data'), dict):
            self.act.data['engagement_data'] = {}
        if name:
            pv = self.act.data.get('engagement_data', {}).pop(name, None)
            if pv is not None:
                self.set({name: pv}, 'engagement_previous_data', update = True)
            return self.set({name: data}, 'engagement_data', update = True)
        else:
            #self.set(copyof(self.act.get("engagement_data", {})), "engagement_previous_data", update = True)
            return self.set(data, 'engagement_data', update = True)

    def set_variable(self, name, data):
        # Allias for set with default syntax, name and then value
        return self.set(data, name)

    def save_variable(self, name, data):
        # Allias for save with default syntax, name and then value
        return self.save(data, name)

    def delete_variable(self, name, default = None):
        # Alias for default syntax of name
        return self.purge(name, default)

    def save(self,*args, **kwargs):
        # Saved across multiple conversations for the same engagement
        return self.add_to_data(*args, **kwargs)

    def set(self,*args, **kwargs):
        # Saved during the current response generation
        return self.act.add_to_data(*args, **kwargs)

    def validate_customer_response(self, attr, value):
        if attr == "__init__":
            return True, None
        try:
            a = self.conversation.conversation_data_model.attributes[attr]
            value = a.process({attr: value}, attr)
            return True, None
        except Exception as ex:
            hp.print_error()
            return False, {"error": ex.message}

    def init(self):
        return self._get_response_from_state(None,'__init__')


    def save_agent_response(self, agent_id, placeholder, reply_to = None, data = None, user_name = None, agent_model = None, timestamp = None, response_id = None, user_icon = None, **kwargs):
        _time = hp.to_datetime(timestamp or hp.now())
        data = data or {}
        ins = {
            "agent_id": agent_id,
            "placeholder": placeholder,
            "user_name": user_name or hp.make_title(agent_id),
            "agent_model": agent_model,
            "timestamp": _time,
            "reply_to": reply_to,
            "user_icon": user_icon,
            "data": data,
            "response_id": response_id or hp.make_uuid3(_time, self.enterprise_id, self.conversation_id, agent_id, agent_model, placeholder, self.engagement_id)
        }
        lst = self.engagement.get('history',[])
        if response_id and lst and lst[-1]["response_id"] == response_id:
            lst[-1] = ins
        else:
            lst.append(ins)
        action.do_action(self.act.to_init(get_data = False), {
            '_action': 'update',
            '_ids': self.engagement_id, 
            '_model': 'engagement',
            '_instance': {
                "history": lst
            }
        })
        return ins

    def save_session(self, system_response, placeholder, customer_state = None, customer_response = None):
        rc = system_response.pop('response_channels', None)
        action.do_action(self.act, {
            '_action': 'update',
            '_ids': self.engagement_id, 
            '_model': 'engagement',
            '_instance': {
                'history': [{
                    "agent_id": self.agent_id,
                    "agent_model": self.role,
                    "system_response": system_response,
                    "placeholder": placeholder,
                    "customer_response": customer_response,
                    "customer_state": customer_state or self.get('customer_state'),
                    "start_timestamp": hp.to_datetime(self.get('start_timestamp')),
                    "timestamp": hp.now(),
                    "response_id": self.get("response_id"),
                }],
                "data": self.act.data.get('engagement_data',{}),
                "previous_data" : self.act.data.get("engagement_previous_data",{}),
                "excluded_states": self.excluded_states
                }
            })
        action.do_action(self.act.to_init(False), {
            '_action': 'post',
            '_model': 'conversation_history',
            '_instance': {
                    "agent_id": self.agent_id,
                    "agent_model": self.role,
                    "system_response": system_response,
                    "placeholder": system_response.get('placeholder') or placeholder,
                    "customer_response": customer_response,
                    "customer_state": customer_state or self.get('customer_state'),
                    "start_timestamp": hp.to_datetime(self.get('start_timestamp')),
                    "timestamp": hp.time(),
                    "response_id": self.get("response_id"),
                    "sequence": self.history_model.count(engagement_id = self.engagement_id) + 1,
                    "customer_id": self.customer_id,
                    "conversation_id": self.conversation_id,
                    "engagement_id": self.engagement_id
                }
            })
        self.set(customer_state, 'customer_state')
        self.set(customer_response, 'customer_response')
        system_response['response_channels'] = rc or {}

    def _get_customer_state(self, system_response, *args, **kwargs):
        kwargs['system_response'] = system_response
        return self.execute(
            self.conversation.system_responses[system_response].get('to_state_function', {'function':'_function_find_customer_state' }),
            *args,
            **kwargs
        )

    def _generate_system_response(self, system_response, response_params = None, placeholder_key = 'placeholder'):
        return_response, chosen_placeholder = self.conversation._get_system_response(system_response, response_params=response_params, placeholder_key=placeholder_key)
        # replace the params in the placeholder and title strings
        try:
            return_response['data'] = self.act.evaluate_args(return_response.get("data") or {})
            for k, v in return_response["data"].iteritems():
                self.act.add_to_data(v, k)

            if return_response.get('{}_template'.format(placeholder_key), False):
                return_response["placeholder"] = self.conversation.parse_template(chosen_placeholder, return_response["{}_template".format(placeholder_key)], self.act.data)
                return_response["placeholder_aliases"]= dict((l, self.conversation.parse_template(p, return_response["{}_template".format(placeholder_key)], self.act.data)) for l,p in return_response["placeholder_aliases"].iteritems())
            else:
                return_response['placeholder'] = chosen_placeholder.format(
                        **self.act.format_data
                )
                return_response["placeholder_aliases"]= dict((l, p.format(**self.act.format_data)) for l,p in return_response["placeholder_aliases"].iteritems())
                if not ( self.get('_skip_all_synthesize') or self.get('_skip_synthesize') ):
                    st = time.time()
                    return_response['response_channels'] = synthesize.synthesize(
                            self.enterprise_id, self.conversation_id, 
                            self.do_translations(return_response['placeholder'], self.get('language')),
                            language = self.get('voice_id', self.get('language', 'english')),
                            avatar_id = self.get('avatar_id'),
                            _async = not (self.get('synthesize_now') or self.get('csv_response')),
                            _cached = not self.get('refresh_synth'), 
                            _audio = return_response['data'].get('voice'),
                            _as_csv = self.get('csv_response', True),
                            channels = self.conversation.conversation.get('response_channels'),
                            did_auth = self.conversation.enterprise.get("app_data", {}).get("did_auth"),
                            did_source_url = self.conversation.conversation["data"].get("did_source_url"),
                            _force_old = self.conversation.data.get('_force_old_pipeline', False)
                    )
                    logger.info("Time taken to synthesize the response: %s", time.time() - st)
                else:
                    return_response['response_channels'] = {}

            return_response['title'] = return_response.get('title', u'').format(
                **self.act.format_data
            )
            return_response['whiteboard'] = self.conversation.parse_template(
                self.execute(
                    return_response.get('whiteboard',"").format(**self.act.format_data),
                    self.act.evaluate_args,
                    **self.act.format_data
                ), 
                return_response.get("whiteboard_template","file").format(**self.act.format_data),
                self.act.data
            )
            return_response['whiteboard_title'] = self.execute(
                return_response.get('whiteboard_title',return_response.get('title', u'')),
                self.act.evaluate_args,
                **self.act.format_data
            )
            return_response['options'] = self.execute(
                return_response.get('options'),
                self.act.evaluate_args,
                **self.act.format_data
            )
            return_response['maintain_whiteboard'] = self.act.evaluate_args(return_response.get("maintain_whiteboard", True))
            return_response['overwrite_whiteboard'] = self.act.evaluate_args(return_response.get("overwrite_whiteboard", False))
            return_response['wait'] = self.act.evaluate_args(return_response.get("wait", 10000))
            return_response['show_in_history'] = self.act.evaluate_args(return_response.get("show_in_history", True))
            return_response['show_feedback'] = self.act.evaluate_args(return_response.get("show_feedback", True))
        except (AttributeError, NameError, KeyError, TypeError, jexc.TemplateError) as e:
            hp.print_error()
            raise ResponseStringError(
                u"{}: Error in formatting conversation response string for response {}, Placeholder {} ({}) in conversation {} for enterprise {}: {}".format(
                    type(e).__name__, 
                    return_response.get('name'), return_response['placeholder'], return_response['title'], 
                    self.conversation_id, self.enterprise_id, e.message
                )
            )
        # Function to create a state_options like dict
        def set_options(inp):
            inp = {inp: None} if isinstance(inp, (str, unicode)) else ({i: None for i in inp} if isinstance(inp, (list, tuple)) else inp)
            return {} if not isinstance(inp, dict) else {k: (v if v else self.conversation.customer_states[k]['title']) for k, v in inp.iteritems() }
        state_options = return_response['state_options'] or self.get('state_options')
        state_options = set_options(state_options)
        save_boosted_states = False
        # Replace all the state options
        if self.get('_custom_state_options'):
            save_boosted_states = True
            state_options = set_options(self.get('_custom_state_options'))
        # Add state state options
        if self.get('_add_state_options'):
            save_boosted_states = True
            state_options.update(set_options(self.get('_add_state_options')))
        # Remove some state options
        if self.get('_remove_state_options'):
            save_boosted_states = True
            for so in set_options(self.get('_remove_state_options')):
                state_options.pop(so, None)
        if save_boosted_states:
            self.save({return_response['name']: state_options.keys()}, '_boost_states')
        else:
            self.purge('_boost_states')
        # Strip out the hidden states
        return_response['state_options'] = {
                so: v for so, v in state_options.iteritems() if self.conversation.customer_states[so].get('show_in_state_option', True)
        }
        return return_response, chosen_placeholder


    def _make_system_response(self, customer_id, agent_id, customer_state):
        system_response = self.execute(
            self.conversation.customer_states[customer_state].get('to_response_function'), 
            self.act.evaluate_args,
            **self.act.format_data
        )

        system_response, response_params = self.conversation._pick_follow_up(customer_id, agent_id, customer_state, system_response)

        return self._generate_system_response(system_response, response_params)

    def set_onetime_states(self, cs, add=True, replace=False):
        exc = []
        if isinstance(cs, (str, unicode)):
            if cs in self.conversation.customer_state_groups:
                exc = self.conversation.customer_state_groups[cs]
            elif cs in self.conversation.customer_states:
                exc = [cs]
            else:
                raise OneTimeStateError("Onetime state name with the name {} is not in either customer states or groups".format(cs)) 

        elif isinstance(cs, list):
            exc = cs

        if replace:
            self.excluded_states = exc or []
        elif add:
            self.excluded_states = list(set(self.excluded_states+exc))
        elif not add:
            self.excluded_states = filter(lambda x: x not in exc, self.excluded_states)

    def unset_onetime(self, cs):
        self.set_onetime_states(cs, add=False)

    def reset_onetime(self):
        self.set_onetime_states([], replace=True)

    def execute(self, func, evaluator = None, *args,**kwargs):
        if not func:
            return
        if not isinstance(func, dict) or not ('function' in func or 'eval' in func or 'lambda' in func):
            if hasattr(evaluator, '__call__'):
                return evaluator(func)
            return func
        args = tuple(args)
        kwargs = kwargs.copy()
        args += tuple(self.act.evaluate_args(hp.copyof(func.get('args', []))))
        kwargs.update(self.act.evaluate_args(hp.copyof(func.get('kwargs', {}))))
        if 'eval' in func:
            return eval(func['eval'].format(*args, **kwargs))
        if 'function' in func:
            if hasattr(self.conversation, func['function']):
                return getattr(self.conversation, func['function'])(self, *args, **kwargs)
            elif func['function'] in self.conversation.functions:
                return self.conversation.functions[func['function']](self, *args, **kwargs)
            else:
                raise UnknownFunction("Unknown function {} in conversation {}".format(func['function'], self.conversation.name))
        f = eval(func['function'])
        return f(*args, **kwargs)

    def _process_unsure(self, _sr, prev_sr=None, cr=None):
        unsure_st = self.conversation.customer_states["unsure_customer_state"]
        matchs = _sr["state_options"]
        if len(matchs) <= 2:
            titles = " or ".join( list( self.conversation.customer_states[i[0]]["title"] for i in matchs if self.conversation.customer_states.get(i[0])))
            _custom_placeholder = "Did you mean {}".format(titles)
        else:
            _custom_placeholder = "Did you mean any of the below options?"

        unsr = self.conversation.system_responses['unsure_customer_state']

        if "data" in unsr:
            if isinstance(unsr['data'], dict):
                _sr['data'].update(unsr.get("data") or {})
            else:
                _sr['data'] = unsr["data"] or _sr['data'] 
        _sr['placeholder'] = unsr.get("placeholder") or _sr.get("placeholder") or _custom_placeholder
        _sr["placeholder_aliases"] = unsr.get("placeholder_aliases") or _sr.get("placeholder_aliases") or {}
        _sr["placeholder_template"] = unsr.get('placeholder_template', False)
        _sr["state_options"] = unsr.get("state_options") or list(map(lambda x: x[0] ,matchs))
        if isinstance(unsr.get("state_options"), (str, unicode)) and unsr.get("state_options") == "_previous_state_options":
            _sr["state_options"].extend(prev_sr['state_options'])
        self.set({m[0]:self.conversation.customer_states[m[0]]["title"] for m in matchs}, "_unsure_state_options")

        wt = _sr.pop("whiteboard", None)
        if "whiteboard" in unsr:
            _sr["whiteboard"] = unsr["whiteboard"]
            _sr["whiteboard_template"] = unsr.get("whiteboard_template", "file")

        _sr["state_options"] = list(set(_sr["state_options"]))
        logger.info("Processing unsure_customer_state %s", _sr)
        self.set(_custom_placeholder, "_custom_placeholder")
        if "to_response_function" in unsure_st:
            _sr = self.execute(
                unsure_st['to_response_function'], 
                self.act.evaluate_args,
                system_response = _sr,
                **self.act.format_data
            )
        r, p = self._generate_system_response(_sr or "unsure_customer_state")

        logger.info("Processed unsure_customer_state %s, %s", r["name"], r["state_options"])
        if not r.get("whiteboard"):
            r["whiteboard"] = wt
        return r, p

    def _process_unknown(self, _sr, prev_sr=None, cr=None):
        logger.info("Processing unknown_customer_state %s", _sr)
        unknown_st = self.conversation.customer_states["unknown_customer_state"]

        unsr = self.conversation.system_responses['unknown_customer_state']

        if "data" in unsr:
            if isinstance(unsr['data'], dict):
                _sr['data'].update(unsr.get("data") or {})
            else:
                _sr['data'] = unsr["data"] or _sr['data'] 
        _sr['placeholder'] = unsr["placeholder"] or _sr["placeholder"]
        _sr["placeholder_aliases"] = unsr.get("placeholder_aliases") or _sr.get("placeholder_aliases") or {}
        _sr["placeholder_template"] = False

        wt = _sr.pop("whiteboard", None)
        if "whiteboard" in unsr:
            _sr["whiteboard"] = unsr["whiteboard"]
            _sr["whiteboard_template"] = unsr.get("whiteboard_template", "file")

        logger.info("Processing unknown_customer_state %s", _sr)

        if "to_response_function" in unknown_st:
            system_response = self.execute(
                    unknown_st['to_response_function'], 
                    self.act.evaluate_args,
                    system_response = _sr,
                    **self.act.format_data
                    )
            r, p = self._generate_system_response(system_response or 'unknown_customer_state')
        else:
            r, p = self._generate_system_response(_sr)

        logger.info("Processed unknown_customer_state %s", r["name"])
        if not r.get("whiteboard"):
            r["whiteboard"] = wt

        action.do_action.delay(self.act.to_init(get_data = False),{
            '_action': 'communicate',
            '_name': "Unknown customer state alert", 
            "receiver": {
                "emails": ["dev@iamdave.ai"]
                },
            'message': u"""
                Reached unknown customer state.
            """.format(
                self.act.enterprise['name'],
                ),
            "html_string":u"""
                <h4>Reached unknown customer state</h4>
                <br />
                <br />
                Server Name : {}
                <br />
                Enterprise id : {}
                <br />
                Conversation id : {}
                <br />
                Engagement id : {}
                <br />
                Customer id: {}
                <br />
                Customer response: {}
                <br />
                Previous system response: {}
                <br />
                Timestamp: {}
                <br />
            """.format(os.environ.get('SERVER_NAME', 'localhost:5000'), self.enterprise_id, self.conversation_id, self.engagement_id, self.customer_id, cr, prev_sr, datetime.now()),
            "title": "Conversation reached unknown state",
            "channels": ["email"],
            'enterprise_id': self.enterprise_id,
            'enterprise_name' : self.act.enterprise['name']
            })
        return r, p

    def next(self, agent_id = None, system_response = None, customer_state = None, customer_response = None):
        start_time = time.time()
        self.agent_id = agent_id or self.user_id
        if system_response is None and customer_state is None:
            r,p = self.init()
            self.save_session(r,p)
            r['engagement_id'] = self.engagement_id
            r['customer_state'] = "__init__"
            return r
        if system_response and system_response not in self.conversation.system_responses:
            logger.error("Unknown system response: %s taking the init system response", system_response)
            system_response = self.conversation.customer_states.get('__init__').get('follow_up').keys()[0]
        logger.info("Time to load: %s", time.time() - start_time)
        if not customer_state:
            if not customer_response:
                raise CustomerStateError("Require either customer state or customer response")
            self.set(customer_response,'customer_response')
            customer_state = self._get_customer_state(
                    system_response, 
                    customer_response = customer_response,
                    )
            if not customer_state:
                customer_state = {}
        else:
            self.set(customer_state,'customer_state')
        resp = {'engagement_id': self.engagement_id}
        logger.info("Time to get execute function: %s", time.time() - start_time)
        logger.info("Returned customer state: %s", customer_state)
        if isinstance(customer_state, dict):
            _sr = self.get_previous_system_response(-1)
            logger.info("Time to get previous system response: %s", time.time() - start_time)
            if 'unsure_customer_state' in customer_state:
                ms = customer_state['unsure_customer_state']
                _sr['state_options'] = ms
                r,p = self._process_unsure(_sr,
                        prev_sr = system_response, 
                        cr = customer_response,
                        )

            elif "__init__" in customer_state:
                r, p = self._get_response_from_state(None,'__init__')
            elif "__previous__" in customer_state:
                r = self.get_previous_system_response()
                p = r["placeholder"]
            else:
                r,p = self._process_unknown(
                   _sr,
                   prev_sr = system_response, 
                   cr = customer_response,
                )

            resp.update(r)
            self.save_session(r,p, customer_response=customer_response, customer_state=customer_state)
            resp["customer_state"] = hp.make_single(customer_state.keys())
            resp["registered_entities"] = self.get('_registered_entities')
            if self.conversation.conversation.get('debug_mode') or self.get('_debug_mode'):
                resp["parse_results"] = self.get('_parse_results')
            for k in SPECIAL_ENGAGEMENT_DATA_KEYS:
                self.purge(k)
            return resp

        cs = self.conversation.customer_states[customer_state]
        if "one_time" in cs:
            if cs["one_time"] == True:
                self.set_onetime_states(customer_state)
            elif not isinstance(cs["one_time"], bool):
                self.set_onetime_states(cs["one_time"])

        self.set(customer_state,'customer_state')
        self.set(customer_response,'customer_response')

        self.conversation._add_to_probs(system_response, customer_state, self.customer_id, self.agent_id, self.engagement_id, self.engagement)
        logger.info("Time to add to probs: %s", time.time() - start_time)
        if customer_state not in self.conversation.customer_states:
            raise StateOptionError("Unknown customer state {} in response to {}, customer_response {}".format(
                customer_state, system_response, customer_response
                )
            )

        valid, message = self.validate_customer_response(customer_state, customer_response)
        if valid:
            r,p =  self._get_response_from_state(system_response, customer_state, customer_response)
        else:
            r,p = self._generate_system_response(system_response, placeholder_key = 'error_placeholder')

        resp.update(r)
        logger.info("Time before save session: %s", time.time() - start_time)
        ss = time.time()
        self.save_session(resp, p, customer_response=customer_response, customer_state=customer_state)
        logger.info("Time after save session: %s/%s", time.time() - ss, time.time() - start_time)
        resp["customer_state"] = customer_state
        resp["hide_in_customer_history"] = not self.conversation.customer_states.get(customer_state, {}).get('show_in_history', True)
        resp["registered_entities"] = self.get('_registered_entities')
        if self.conversation.conversation.get('debug_mode') or self.get('_debug_mode'):
            resp["parse_results"] = self.get('_parse_results')
        for k in SPECIAL_ENGAGEMENT_DATA_KEYS:
            self.purge(k)
        logger.info("Time to get response: %s", time.time() - start_time)
        return resp

    def do_translations(self, sentence, language = None):
        for k, v in self.conversation.conversation.get('translations', {}).iteritems():
            if isinstance(v, dict):
                v = v.get(language or self.get('language'), v.get('default'))
            if isinstance(v, (str, unicode)):
                sentence = re.sub(ur"\b{}\b".format(k), u"{}".format(v), sentence)
                sentence = re.sub(ur"\s{}\s".format(k), u" {} ".format(v), sentence)
                sentence = re.sub(ur"\s{}$".format(k), u" {}".format(v), sentence)
                sentence = re.sub(ur"^{}\s".format(k), u"{} ".format(v), sentence)
                sentence = re.sub(ur"^{}$".format(k), u"{}".format(v), sentence)
        sentence = re.sub(r'<.*?>', '', sentence)
        return sentence

class Conversation(object):

    @classmethod
    def retrain(cls, enterprise_id, conversation_id=None, role="admin", user_id=None):
        conm = base_model.Model("conversation", enterprise_id, role)
        cons = conm.pivot(["conversation_id"])["data"]
        for i in cons:
            if conversation_id is not None:
                if i != conversation_id:
                    continue
            cls.post(enterprise_id, conm.get(i), role, user_id, retrain=True)

    @classmethod
    def post(cls, enterprise_id, json_obj, role = 'admin', user_id = None, retrain=False, debug_mode=False, refresh_cache = False, synthesis_cache = False, no_train = False):
        conversation_id = json_obj["conversation_id"]
        _conversation_id = to_meta_id(enterprise_id, conversation_id)
        debug_mode = debug_mode or json_obj.pop('debug_mode', debug_mode)
        retrain = retrain or json_obj.pop('retrain', retrain)
        no_train = no_train or json_obj.pop('no_train', no_train)
        ner_enabled = json_obj.get('include_ner')
        def extract_entities(entities):
            if not entities:
                return []

            for i in entities:
                if not i.get("name") and ( all( i.get(z)  for z in ["model", "attribute"]) or "values" in i ):
                    raise EntityFormatError("Name is mandatory and either 'model' and 'attribute' or 'values' are mandatory for creating entity", conversation_id)
                if isinstance(i.get("values"), (str, unicode)):
                    e = i["values"].split(".")
                    if len(e) != 2:
                        raise  EntityFormatError("Entity values format {} for the conversation {} is not correct".format(i["values"], conversation_id))
                    i["model"] = e[0]
                    i["attribute"] = e[1]
                    i.pop("values")

                if all( i.get(z)  for z in ["model", "attribute"]):
                    m = base_model.Model(i["model"], enterprise_id)
                    if i["attribute"] not in m.attributes:
                        raise EntityFormatError("Attribute({}) not persent in the model({})".format(i["attribute"], i["model"]))
                    i["weight"] = i.get("weight", 0.5)
                i["filter"] = i.pop("filter", {})
            return entities

        entities = extract_entities(json_obj.get("entities"))
        json_obj["entities"] = hp.copyof(entities)


        phrases = {}
        for cs in json_obj["customer_states"]:
            k = []
            if "keywords" in cs and cs["keywords"]:
                #k = cs["keywords"] if isinstance(cs["keywords"], list) else cs["keywords"].keys()
                phrases[cs["name"]] = hp.copyof(cs["keywords"])
            else:
                phrases[cs["name"]] = []

        old_entities = {}
        old_phrases = {}
        if not retrain and not no_train:
            try:
                x = NlpModel(conversation_id, db_name=enterprise_id, ner_enabled = ner_enabled)
                old_entities = dict(map(lambda k: (k[0][2:-2], k[1]['entity_values']) , x._entity_desc.iteritems()))
                old_keywords = x._entity.keys()
                for k, v in phrases.items():
                    if isinstance(v, list):
                        for i, k1 in enumerate(v):
                            v[i] = x.normalize(k1)
                    elif isinstance(v, dict):
                        for k1, i in v.items():
                            v[x.normalize(k1)] = v.pop(k1)
                    ks = k.strip('_')
                    if k in old_entities or ks in old_entities:
                        keywords = old_entities.pop(k, {})
                        keywords.update(old_entities.pop(ks, {}))
                        to_pop = {}
                        for k1, v1 in keywords.iteritems():
                            k2 = x.normalize(k1)
                            if k2 in v:
                                if k2 not in old_keywords:
                                    logger.error("Red flag!!, keyword %s found in entity_desc but not in entity", k2)
                                    continue
                                if isinstance(v, list):
                                    while k2 in v:
                                        v.remove(k2)
                                    logger.info("keyword already found in db, not doing: %s from %s", k2, k)
                                elif isinstance(v, dict):
                                    if v.get(k2) == v1:
                                        v.pop(k2)
                                        logger.info("keyword already found in db, not doing: %s from %s", k2, k)
                            elif k1 != k2:
                                to_pop[k1] = v1
                            else:
                                to_pop[k1] = v1
                        if to_pop:
                            old_phrases[k] = to_pop
                phrases = {k: v for k, v in phrases.iteritems() if v}
                for k in entities:
                    if not k.get('name'):
                        raise KeyError("Entity: {} should have a name".format(k))
                    if not k.get('values') and not k.get('model'):
                        raise KeyError("Entity: {} should have values or a model".format(k))
                    if k["name"] in old_entities and "model" not in k:
                        values = old_entities.pop(k["name"])
                        to_pop = {}
                        k['values'] = map(x.normalize, k['values'])
                        for k1, v1 in values.iteritems():
                            if k1 in k["values"]:
                                if isinstance(k["values"], list):
                                    while k1 in k['values']:
                                        k["values"].remove(k1)
                                else:
                                    logger.warn("values is not a list!")
                            else:
                                to_pop[k1] = v1
                        if to_pop:
                            old_entities[k["name"]] = to_pop
                    elif "model" in k:
                        old_entities.pop(k["name"], None)
                entities = [k for k in entities if ((isinstance(k.get('values'), list) and k['values']) or k.get('model'))]
            except UnknownConversation as ex:
                logger.info("There is no conversation with same id")
        try:
            if not no_train:
                if retrain:
                    old_entities = {}
                    old_phrases = {}
                if debug_mode:
                    update_nlentities(conversation_id, enterprise_id, entities, old_entities, phrases, old_phrases, retrain=retrain, refresh_cache = refresh_cache, ner_enabled = ner_enabled)
                else:
                    update_nlentities.delay(conversation_id, enterprise_id, entities, old_entities, phrases, old_phrases, retrain=retrain, refresh_cache = refresh_cache, ner_enabled = ner_enabled)
        except Exception as ex:
            hp.print_error()
            logger.info("Failed to post conversation :: %s", ex.message)
            raise
        finally:
            cls.__metaclass__.all_instances.pop(_conversation_id, None)
            cls.__metaclass__.all_data.pop((_conversation_id, role, user_id), None)

        if not "variables" in json_obj:
            json_obj["variables"] = []

        con = Conversation(enterprise_id, conversation_id, role, user_id, json_obj)
        con._make_data_model()
        if isinstance(synthesis_cache, (str, unicode)):
            scache = JsonBDict("__{}_conv_cacher".format(conversation_id), [unicode], ['text'], DB_NAME = enterprise_id)
            try:
                ops = json.loads(synthesis_cache)
            except ValueError:
                pass
            else:
                for k, v in ops.iteritems():
                    scache[k] = v
        json_obj['retrain'] = False
        json_obj['debug_mode'] = False
        json_obj['refresh_cache'] = False
        return con.conversation_model.post(json_obj)

    _uploads_base_path = joinpath(dirname(dirname(abspath(__file__))), 'static', 'uploads')
    _downloads_base_path = "{}://{}/static/uploads".format(os.environ.get("HTTP", "http"), os.environ.get('SERVER_NAME', 'localhost:5000'))
    __metaclass__ = MetaConversation

    def evaluate_func_file(self, fp, ff, do_add = False):
        c = compile(fp.read(), ff, 'exec')
        lf = {}
        try:
            exec(c, {
                'hp': hp, 
                'act': self.act, 
                'base_model': base_model, 
                'val': val, 
                'alg': algorithms, 
                'do_action': action.do_action, 
                'do_actions': action.do_actions,
                'logger': hp.get_logger("conversation_functions_{}".format(self.conversation_id)),
                }, lf)
        except Exception as e:
            hp.print_error(e)
            raise ConversationFunctionError(
                    "Cannot load conversation functions for conversation {} in enterprise {}".format(self.conversation_id, self.enterprise_id)
                    )
        if do_add:
            for k, v in lf.iteritems():
                if isinstance(v, types.FunctionType):
                    setattr(self, k, types.MethodType(v, self, Conversation))
                    # if the function starts with _action_ then it should be executable from actions too.
                    if v.__name__.startswith('_action_'):
                        setattr(self.act, k, types.MethodType(v, self.act, action.Action))

    def __init__(self, enterprise_id, conversation_id, role = 'admin', user_id = None, conversation_obj = None, act = None):
        self.enterprise_id = enterprise_id
        self.enterprise_model = base_model.Model("enterprise","core")
        self.enterprise = self.enterprise_model.get(self.enterprise_id)
        if not self.enterprise:
            raise UnknownConversation("Enterprise with id: {}".format(enterprise_id))
        self.conversation_id = conversation_id
        self.conversation_data_model_name = "conversation_data_{}".format(self.conversation_id)
        self._meta_conversation_id = to_meta_id(enterprise_id,conversation_id)
        self.conversation_model = base_model.Model('conversation', self.enterprise_id, "admin")
        if not conversation_obj:
            self.conversation = self.conversation_model.get(conversation_id)
        else:
            self.conversation = conversation_obj
        if not self.conversation:
            raise UnknownConversation("No conversation with  id {} found".format(self.conversation_id))

        # Manage all the data
        self.action_data = action.setup(None, self.enterprise_id, 'admin', None)
        self.create_data(role, user_id, act = act)
        self.customer_model = self.data['customer_model']
        self.product_model = self.data['product_model']
        self._clean_data = hp.copyof(self.act.data)
        self._clean_format_data = hp.copyof(self.act.format_data)
        self._clean_flattened_data = hp.copyof(self.act.flattened_data)
        self._clean_format_keys = hp.copyof(self.act.format_keys)

        # Initialize the models
        self.ner_enabled = self.conversation.get("include_ner", False)
        self.nl_mod = NlpModel(conversation_id, db_name=enterprise_id, ner_enabled=self.ner_enabled)
        data = self.setup()
        self._uploads_path = joinpath(self._uploads_base_path, enterprise_id, conversation_id)
        self._downloads_path = joinpath(self._downloads_base_path, enterprise_id, conversation_id)
        self.load_functions()
        self._response_prob = matcher.Matcher(
                '{}_customer'.format(conversation_id), 
                key_names = self.customer_model.ids, 
                key_types = self.customer_model.id_atypes, 
                DB_NAME = self.enterprise_id
                )
        self.alg = algorithms.BaseAlgorithm(self.enterprise_id)

    def create_data(self, role, user_id, act = None):
        self.user_id = user_id
        self.role = role
        self.act = act or action.Action(
            self.enterprise_id, self.role, 
            user_id = self.user_id,
            data = (getattr(self, 'action_data') or action.setup(None, self.enterprise_id, 'admin', None)) 
        )
        # all enterprise specific attributes are now attached to self
        self.data = self.act.data
        self.act.add_to_data(self.conversation.get("data",{}))
        self._default_variables()


    def load_functions(self):
        self.functions = stuf()
        for k, v in self.conversation.get('functions', {}):
            _add_function(self,k, v)
        _add_function(self,'dict_string', lambda x: json.dumps(x))
        ff = joinpath(self._uploads_path, self.conversation.get('conversation_functions_file', 'conversation_functions.txt'))
        if ispath(ff):
            with codecs.open(ff, 'r', 'utf-8') as fp:
                self.evaluate_func_file(fp, ff, do_add = True)

    def _default_variables(self):
        tz = pytz.timezone(self.enterprise_model.timezone)
        self.act.add_to_data(self.conversation_id, 'conversation_id')
        self.act.add_to_data(self.enterprise, 'enterprise')
        self.act.add_to_data(self.enterprise_id, 'enterprise_id')
        self.act.add_to_data(self.user_id, 'user_id')
        self.act.add_to_data(self.role, 'role')
        self.act.add_to_data(hp.get_greeting(tz), "greeting")

    def get_keywords(self, **kwargs):
        for csn, cs in self.customer_states.iteritems():
            if cs.get('show_in_keywords', True):
                yield ' '.join(cs.get('keywords', []) + [cs.get('title', csn), csn.replace('_', ' ')]), csn, cs.get('title', csn), cs.get('ui_element'), self.act.evaluate_args(cs.get('options', [])), cs.get('keywords', []) 
        if kwargs.get('conversation_response_model'):
            csn = kwargs.get('default_customer_response', 'cs_faq')
            kw_attr = kwargs.get('keyword_attribute', 'keywords')
            title_attr = kwargs.get('title_attribute', 'title')
            options_attr = kwargs.get('options_attribute', 'options')
            for cs in base_model.Model(kwargs.get('conversation_response_model'), self.enterprise_id):
                yield ' '.join(cs.get(kw_attr, []) + [cs.get(title_attr, csn), csn.replace('_', ' ')]), csn, cs.get(title_ttr, csn), 'text', self.act.evaluate_args(cs.get(options_attr, [])), cs.get(kw_attr, [])


    def _make_data_model(self):
        logger.info("Creating conversation data model")
        _data_model = {
                "name" : self.conversation_data_model_name,
                "attributes":[{
                    "name" : "{}_id".format(self.conversation_data_model_name),
                    "id" : True
                    }],
                "delete_all": True,
                "allow_anonymous_view": True
                }

        for i,cs in self.customer_states.iteritems():
            if i in self.variables or i in ["__init__"]: continue
            _data_model["attributes"].append({
                "name" : cs["name"].lower(),
                "title" : cs.get("title"),
                "type" : cs.get("type","text"),
                "ui_element" : cs.get("ui_element"),
                "options" : cs.get("options",[])
                })
        for i,v in self.variables.iteritems():
            _data_model["attributes"].append({
                "name" : v["name"],
                "title" : v.get("title"),
                "type" : v.get("type","text"),
                "ui_element" : v.get("ui_element"),
                "options" : v.get("options",[]),
                "default" : v.get("default")
                })
        try:
            base_model.post(self.conversation_data_model_name, self.enterprise_id, model=_data_model)
            self.conversation_data_model = base_model.Model(self.conversation_data_model_name, self.enterprise_id, "admin")
        except:
            raise

    def setup(self):
        self.name = self.conversation.get('name', self.conversation_id)
        self.description = self.conversation.get('description',self.name)
        self.customer_states = {}
        self.system_responses = {}
        self.variables = {}
        self.entities = {}
        self.unadded_states = {}
        self.customer_state_groups = {}


        for i in self.conversation.get("entities", []):
            self.entities[i["name"]] = i

        for v in self.conversation.get("variables",[]):
            self.variables[v["name"]] = v

        for s in self.conversation.get('customer_states',[]):
            if not (s.get('name') or s.get('title')):
                raise UnknownFormat("Customer state should have name or title in conversation {}".format(self.name))
            s['name'] = s.get('name') or hp.normalize_join(s.get('title'))
            s['title'] = s.get('title') or hp.make_title(re.sub(r'^cs_','', s.get('name')))
            s['faq'] = s.get('faq', True)
            s['show_in_state_option'] = s.get('show_in_state_option', True)
            s['show_in_history'] = s.get('show_in_history', True)
            s['show_in_keywords'] = s.get('show_in_keywords', True)
            if s.get('keywords') is not None and not (isinstance(s['keywords'], (list, dict)) and all(isinstance(_k, (str, unicode)) for _k in s['keywords'])):
                raise UnknownFormat("Keywords for customer_state {} in conversation {} should be a list of strings".format(
                        s['name'],
                        self.name
                    )
                )
            if s.get('positivity_score') is None:
                s['positivity_score'] = 0.0
            if not (isinstance(s.get('positivity_score'), (float, int)) and s['positivity_score'] >= -1.0 and s['positivity_score'] <= 1.0):
                raise UnknownFormat("Positivity Score for customer_state {} in conversation {} should be beween -1 and 1".format(
                    s['name'],
                    self.name
                    )
                    )
            s['positivity_score'] = float(s['positivity_score'])
            if s.get('actions') is not None and not (isinstance(s.get('actions'), (list, NoneType))):
                raise UnknownFormat("Actions for customer_state {} in conversation {} should be list of actions".format(
                    s['name'],
                    self.name
                    )
                    )
            if s.get('to_response_function') is not None and not \
                    (isinstance(s.get('to_response_function'), dict) and any(('function' in s['to_response_function'], 'eval' in s["to_response_function"]))):
                        raise UnknownFormat("Response function for customer_state {} in conversation {} should be dict with key function".format(
                            s['name'],
                            self.name
                            )
                            )
            if isinstance(s.get('follow_up'), (str, unicode)):
                s['follow_up'] = OrderedDict({s['follow_up']: 1})
            elif isinstance(s.get('follow_up'), (list, tuple)):
                s['follow_up'] = OrderedDict({_s: 1./len(s['follow_up']) for _s in s['follow_up']})
            elif not isinstance(s.get('follow_up'), dict):
                raise UnknownFormat("Follow up for customer_state {} in conversation {} is not of type str, list or dict, but {}".format(
                    s['name'], self.name, type(s['follow_up'])
                    )
                    )
            else:
                s['follow_up'] = OrderedDict(s['follow_up'])
            self.customer_states[s['name']] = s

            if "customer_state_group" in s:
                if s["customer_state_group"] not in self.customer_state_groups:
                    self.customer_state_groups[s["customer_state_group"]] = []
                self.customer_state_groups[s["customer_state_group"]].append(s["name"])

        if '__init__' not in self.customer_states:
            raise StateOptionError("No customer state __init__ present among customer states in conversation {}".format(self.name))
        for s in self.conversation.get('system_responses', []):
            if not (s.get('name') or s.get('title')):
                raise UnknownFormat("system response should have name or title in conversation {}".format(self.name))
            s['name'] = s.get('name') or hp.normalize_join(s.get('title'))
            s['title'] = s.get('title') or hp.make_title(re.sub(r'^sr_','', s.get('name')))
            s['wait'] = s.get('wait', 10000)
            s['show_in_history'] = s.get('show_in_history', True)
            s['maintain_whiteboard'] = s.get('maintain_whiteboard', bool(s.get('whiteboard')))
            s['overwrite_whiteboard'] = s.get('overwrite_whiteboard', False)
            s['show_feedback'] = s.get('show_feedback', False)
            if isinstance(s.get('placeholder'), (str, unicode)):
                s['placeholder'] = OrderedDict({s['placeholder']: 1})
            elif isinstance(s.get('placeholder'), (list, tuple)):
                s['placeholder'] = OrderedDict({_s: 1./len(s['placeholder']) for _s in s['placeholder']})
            elif not isinstance(s.get('placeholder'), dict):
                raise UnknownFormat("placeholder for system response {} in conversation {} is not of type str, list or dict, but {}".format(
                    s['name'], self.name, type(s['placeholder'])
                    )
                    )
            else:
                s['placeholder'] = OrderedDict(s['placeholder'])
            if isinstance(s.get('state_options'), (str, unicode)):
                s['state_options'] =  hp.make_list(s.get('state_options'))
            if not isinstance(s.get('state_options'), (list, tuple)):
                raise UnknownFormat("state options for system response {} in conversation {} should be of type list, not {}".format(
                    s['name'], self.name, type(s['state_options'])
                    )
                    )
            s['state_options'] = self.conversation.get('data', {}).get('_default_state_options',[]) + s['state_options']
            #TODO create default to_state_function for case s.get('to_state_function') is None. check text for keywords and state options.
            if s.get('to_state_function') is None:
                s['to_state_function'] = OrderedDict({'function':'_function_find_customer_state' })
            if not (s.get('to_state_function') or \
                    (isinstance(s.get('to_state_function'), dict)  and 'function' in s['to_state_function'])):
                raise UnknownFormat("state function for system response {} in conversation {} should be of type dict, not {}".format(
                    s['name'], self.name, type(s['to_state_function'])
                    )   
                    )
            if len(s['state_options']) == 1:
                s['next_state'] = s['state_options'][0]
                self.customer_states[s['next_state']]['ui_element'] = s.get('ui_element')
                self.customer_states[s['next_state']]['options'] = s.get('options', [])
            for r in s['state_options']:
                if r not in self.customer_states:
                    raise StateOptionError("State {} in state options fro system_response {} is not present in conversation {}".format(
                        r, s['name'], self.name
                        )
                        )
            self.system_responses[s['name']] = s
        for s, cs in self.customer_states.iteritems():
            for r in cs['follow_up']:
                if not r in self.system_responses:
                    raise ResponseOptionError("Response {} in follow ups from customer state {} is not present in conversation {}".format(
                        r, s, self.name
                        )
                        )
            for v,sr in cs.get("required_variables",{}).iteritems():
                if v not in self.variables and v not in self.customer_states:
                    raise UnknownVariable("Variable named {} is not present in variables or customer states in conversation {} defined in customer state {}".format(v,self.name, s))

                if sr not in self.system_responses:
                    raise UnknownSystemResponse("System response {} for required variable {} specified in customer state {} in conversation {}".format(sr, v,s, self.name)
                            )

        for default_state in default_customer_state:
            if default_state["name"] not in self.customer_states:
                self.customer_states[default_state["name"]] = default_state

        for default_state in default_system_response:
            if default_state["name"] not in self.system_responses:
                self.system_responses[default_state['name']] = default_state
            elif not self.system_responses[default_state["name"]].get("placeholder"):
                self.system_responses[default_state['name']]["placeholder"] = default_state["placeholder"]
                self.system_responses[default_state['name']]["placeholder_aliases"] = default_state.get("placeholder_aliases", {})


        try:
            self.conversation_data_model = base_model.Model(self.conversation_data_model_name, self.enterprise_id, "admin")
            logger.info("Conversation data model loaded from disk")
        except Exception as ex:
            print(ex.message, ex)
            self._make_data_model()

        self._one_time_states = list( map(lambda x: x["name"] , filter(lambda x: "one_time" in x and x["one_time"] == True , self.customer_states.values()) ))
        self._default_exclude_states = list( map(lambda x: x["name"] , filter(lambda x: "exclude" in x and x["exclude"] == True , self.customer_states.values()) ))
        return self.conversation.get('data', {})

    def render_template(self, **data):
        template = self.conversation.get('conversation_template')
        data.update(self.act.data)
        data.update(self.act.enterprise)
        data.update(self.conversation["data"])

        server_name = os.environ.get('SERVER_NAME', 'localhost:5000')
        http = os.environ.get('HTTP', 'http')
        host_url = "{}://{}".format(http, server_name)
        data["host_url"] = data.get("host_url", host_url)

        with app.app_context():
            if not isinstance(template, (str, unicode)):
                return render_template('conversation_exe.html', functions = self.functions, hp = hp, **data)
            if any(template.startswith(_a) for _a in ['http://', 'https://', 'www']):
                return render_template_string(hp.download_file(template), functions = self.functions, hp = hp, **data)
            elif ispath(template):
                with open(template, 'rb') as f:
                    return render_template_string(f.read(), functions = self.functions, hp = hp, **data)
            elif ispath(joinpath(self._uploads_path, template)):
                with open(joinpath(self._uploads_path, template), 'rb') as f:
                    return render_template_string(f.read(), functions = self.functions, hp = hp, **data)
            elif ispath(joinpath(app.template_folder, template)):
                return render_template(template, functions = self.functions, hp = hp, **data)
            else:
                return render_template_string(template, functions = self.functions, hp = hp, **data)

    def _pick_follow_up(self, customer_id, agent_id, customer_state, system_response):
        followups = self.customer_states[customer_state]['follow_up']
        response_params = {}
        if isinstance(system_response, (list, dict)) or system_response not in self.system_responses: 
            if not followups and not isinstance(system_response, (list, dict)):
                raise ResponseOptionError("Unknown output {} of function for customer state {}, and empty followups".format(
                    system_response, customer_state
                    )
                    )
            if isinstance(system_response, (list, dict)):
                followups = system_response if isinstance(system_response, dict) else dict((k,1.0) for k in system_response)
            cf = OrderedDict({k: ((NUM_SIMILAR/10.)*(v or 0.001)) for k, v in followups.iteritems()})
            pf = OrderedDict()
            for f in followups:
                pf.update((k, ((NUM_SIMILAR/10.)*(v or 0.001))) for k, v in self.system_responses[f]['placeholder'].iteritems())
            if len(followups) > 1:
                for c, score in self.alg.get_similar_customers(customer_id, limit = NUM_SIMILAR, internal = True):
                    for k, sc in self._response_prob._iter_display(customer_id, capped = True):
                        attr, val = k
                        w = score*sc
                        if attr == 'system_response' and val in cf:
                            cf[val] = cf.get(val, 0) + w
                        if attr == 'placeholder' and val in pf:
                            pf[val] = pf.get(val, 0.) + w
            for k in cf:
                if cf[k] < 0: cf[k] = 0
            for k in pf:
                if pf[k] < 0: pf[k] = 0
            system_response =  hp.histogram_sample_generator(cf.values(), input_array = cf.keys()).next()
        else:
            pf = OrderedDict(self.system_responses[system_response]['placeholder'])
        return system_response, response_params

    def parse_template(self, template ,template_type="static", data = None):
        data = data or self.act.data
        if not template: 
            return None
        if template_type in ['file']:
            return render_template(template, functions = self.functions, hp = hp, **data)
        elif template_type in ['url']:
            return render_template_string(hp.download_file(template), functions = self.functions, hp = hp, **data)
        elif template_type in ['static', 'custom']:
            if not ispath(joinpath(self._uploads_path, template)):
                logger.error("Static template :%s does not exist in the conversation data path. Trying file template", template)
                return render_template(template, functions = self.functions, hp = hp, **data)
            with open(joinpath(self._uploads_path, template), 'r') as fp: 
                return render_template_string(fp.read(), functions = self.functions, hp = hp, **data)
        else:
            return render_template_string(template, functions = self.functions, hp = hp, **data)

    def _get_system_response(self, system_response, response_params = None, placeholder_key = 'placeholder'):
        if isinstance(system_response, dict):
            return_response = hp.copyof(system_response)
        else:
            return_response = hp.copyof(self.system_responses[system_response])
        response_params = response_params or {}
        return_response.update(response_params)

        if placeholder_key not in return_response:
            placeholder_key = 'placeholder'

        pf =  {return_response[placeholder_key]: 1.0} if isinstance(return_response[placeholder_key], (str, unicode)) else OrderedDict(return_response[placeholder_key])
        placeholders = OrderedDict({k:pf[k] for k in pf.iterkeys()})
        # Generate sample randomly based on the weights

        chosen_placeholder = hp.histogram_sample_generator(
                placeholders.values(), input_array = placeholders.keys()
                ).next()

        return_response["placeholder_aliases"] = return_response.get("{}_aliases".format(placeholder_key), {}).get(chosen_placeholder,{})

        return return_response, chosen_placeholder

    def _add_to_probs(self, system_response, customer_state, customer_id, agent_id, engagement_id, engagement):
        placeholder = None
        for h in reversed(engagement.get('history', [])):
            if h.get('system_response', {}).get('name') == system_response:
                placeholder = h['placeholder']
                break
        d = self.customer_model.ids_to_dict(hp.make_list(customer_id))
        d.update({'system_response': system_response, 'placeholder': hp.normalize_join(placeholder)})
        ps = self.customer_states[customer_state]['positivity_score']
        if ps:
            action.matcher_ops.delay(self._response_prob.name, self.enterprise_id, 'add', d, ps)
        #d = self.customer_model.ids_to_dict(hp.make_list(customer_id))
        #d.update({'customer_state': customer_state})
        #action.matcher_ops.delay("_user_matcher", self.enterprise_id, 'cap', d)
        #self.alg._user_matcher.cap(d)



    def _function_find_customer_state(self, engagement, *args, **kwargs):
        bks = []
        exclude = self._default_exclude_states[:]
        sr = {}
        if "system_response" in kwargs:
            bs = engagement.get('_boost_states') 
            if bs and kwargs["system_response"] in bs:
                bks = bs.get(kwargs["system_response"])
                logger.info("Getting boosted states from engagement, instead of system response for system response %s: %s", kwargs["system_response"], bks)
                engagement.purge('_boost_states')
            else:
                sr = hp.copyof(self.system_responses[kwargs["system_response"]])
                bks = sr.get("state_options", {})
        if isinstance(bks, dict):
            bks = bks.keys()
        kwargs["customer_response"] = kwargs["customer_response"].lower()
        if kwargs["customer_response"] in RESERVED_STATES:
            return { RESERVED_STATES.get(kwargs["customer_response"]): None }
        #exc = list( filter(lambda x: x in self.data , self._one_time_states) )
        exclude = exclude + engagement.excluded_states
        logger.info("excluded states %s", exclude)
        for i in bks:
            if i in exclude:
                exclude.remove(i)
        cs_count = len(self.customer_states.keys())
        faqs = {ob["name"]: None for ob in self.customer_states.values() if ob.get("faq", True)}

        logger.info("Parsing started for utterance %s, boosted_keys: %s, excluded: %s", kwargs['customer_response'], bks, exclude)       
       
        if engagement.get('_skip_parse_num_words') and isinstance(engagement.get('_skip_parse_num_words'), (int, float)):
            cs = kwargs.get('customer_response')
            if isinstance(cs, (str, unicode)):
                if len(cs.split()) >= engagement.get('_skip_parse_num_words'):
                    return {'unknown_customer_state': []}
        match, other_match = self.nl_mod.parse(kwargs['customer_response'], boosted_keys=bks, exclude= exclude, _fresh = engagement.get("refresh_cache") or engagement.get('_disable_conversation_cache'), customer_state_count=cs_count, match_limit=engagement.get("match_limit", 10), _faq_states = faqs, _disallow_spell_check = self.data.get("_disallow_spell_check"), _allow_unsure_direct=self.data.get("_allow_unsure_direct"))

        logger.info("Response detected from parser %s, %s", match, other_match)       

        #if not match and not other_match:
        #    faqs =[]
        #    for l, ob in self.customer_states.iteritems():
        #        if ob.get("faq", True):
        #            faqs.append(ob["name"])
        #    if len(faqs):
        #        exc = exclude + list(set(self.customer_states.keys()) - set(faqs))
        #        match, other_match = self.nl_mod.parse(kwargs['customer_response'], boosted_keys=bks, exclude= exc, no_deletes= 10, penalty=0.1, max_threshold=0.7, _fresh = engagement.get("refresh_cache"), customer_state_count=cs_count, _faq_search=True, match_limit=engagement.get("match_limit", 20))
        #        logger.info("Response detected from faq parser %s %s", match, other_match) 

        if not match and not other_match:
            return {'unknown_customer_state': []}

        registered_entities = engagement.get('_registered_entities', {})
        engagement.set(
            [
                {
                    "customer_state": _[0], 
                    "score": _[2], 
                    "entities": _[3], 
                    'title': _[1], 
                    'direct_match': True
                } for _ in match
            ] + [
                {
                    "customer_state": _[0], 
                    "score": _[2], 
                    "entities": _[3], 
                    'title': _[1], 
                    'direct_match': False
                } for _ in other_match
            ],
            "_parse_results"
        )
        # All Matches
        all_matches = match + other_match
        all_matches.reverse()    # We want to over-ride the entities with lower score by the ones with a higher score
        for match_now in all_matches:
            for pw, w in match_now[3].iteritems():
                en = self.entities.get(pw, {})
                vr = en.get("variable_name")
                val = w

                if en.get("return_entity"):
                    val = en["return_entity"] if en["return_entity"] != True else pw
                    # Badly written, probably needs to change

                if pw.startswith("_"):
                    engagement.set(val, pw)
                if pw in registered_entities:
                    registered_entities[pw] = val
                if not vr:
                    continue
                if en.get("replacement_function", "replace") == "replace":
                    engagement.set(val, vr)
                elif en.get('replacement_function') == "add":
                    lst = hp.make_list(engagement.data.get(vr, []))
                    lst.append(val)
                    lst = list(set(lst))
                    engagement.save(lst, vr)
                else:
                    engagement.set(val, vr)
        engagement.set(registered_entities, '_registered_entities')
        if len(all_matches) == 1:
            return all_matches[0][0]
        if len(match) > 1 and self.data.get("_allow_unsure_direct"):
            return {'unsure_customer_state' : match}
        if self.data.get("_best_of_unsure") and all_matches[-1][2] >= all_matches[-2][2] + self.data.get("_best_of_unsure"):
            return all_matches[-1][0]
        if not match:
            if other_match and len(other_match) == 1:
                return other_match[0][0]
            if other_match and self.data.get("_best_of_unsure") and other_match[0][2] >= other_match[1][2] + self.data.get("_best_of_unsure"):
                return other_match[0][0]
            if other_match and self.data.get("_allow_unsure"):
                return {'unsure_customer_state': other_match}
            return {'unknown_customer_state': []}
        # Return the last match (which is basically the one with the highest score)
        return all_matches[-1][0]

def get_phrases(engagement, language = None):
    sp = engagement.conversation.conversation.get('speech_phrases', {})
    r = hp.make_list(
            sp.get(
                language or engagement.get('language'),
                sp.get('default', [])
                )
            )
    return r + map(lambda x: x[0][:100], engagement.conversation.get_keywords())[:(500 - len(r))]

@celery.task(name = 'next')
def next(enterprise_id, conversation_id, customer_id, role, user_id, engagement_id = None, system_response = None, customer_state = None, customer_response = None, channel = None, channel_id = None, audio_file = None, stream = None, language = 'english', **kwargs):
    """
    kwargs:
        refresh_cache (False) ... True refreshes parsing
        refresh_synth (False) ... True refreshes the synthesis
        synthesize_now (False) ... True does the synthesis immediately instead of as a job
        voice_id (<language>) ... Setting voice ID for synthesis
        match_limit (20)  ... how many sentences to consider after FAQ
        csv_response (False) ... returns the shapes and frames as a part of the response instead of file name
        language
        stream
        start_timestamp
        recognized_speech
        query_type
    """
    start_timestamp = hp.now()
    _time = time.time()
    logger.info("Time at initiating request %s", _time) 
    engmt = Engagement(
            enterprise_id, 
            conversation_id = conversation_id, 
            customer_id = customer_id, role = role, 
            user_id = user_id, engagement_id = engagement_id
            )
    logger.info("Time after initializing engagement  %s", time.time() - _time)
    if not kwargs.get('query_type'):
        kwargs['query_type'] = 'speech' if audio_file else (kwargs.get('query_type', 'click')) if customer_state else 'type'
    kwargs['start_timestamp'] = kwargs.get('start_timestamp', _time)
    engmt.set(kwargs)
    engmt.set(language, 'language')
    engmt.set(engmt.get('voice_id') or language, 'voice_id')
    engmt.set(channel, "channel")
    engmt.set(channel, "channel_id")
    recognized_speech = kwargs.get('recognized_speech')
    logger.info("Recognized Speech: %s", recognized_speech)
    logger.info("Audio File: %s", audio_file)
    if audio_file and recognized_speech is None:
        with open(audio_file, 'rb') as fp:
            stream = fp.read()
    if stream:
        try:
            r = recognize.Recognizer(
                    phrases = get_phrases(engmt, language = language), 
                    **kwargs
                    )
            recognized_speech = r.recognize(stream)
            customer_response = recognized_speech
        except recognize.RecognizerError as e:
            customer_state = {
                    'unknown_customer_state': None 
                    }

    logger.info("Time before initiating next request %s", time.time() - _time) 
    audio_id = hp.make_uuid3(_time, enterprise_id, conversation_id, user_id, role, engmt.engagement_id, customer_state, customer_response)
    engmt.set(audio_id, 'response_id')
    try:
        r = engmt.next(user_id, system_response=system_response, customer_state = customer_state, customer_response = customer_response)
    except Exception as e:
        if role == 'admin' and engmt.get('_debug_mode'):
            engmt.logger.error(u'\n'.join(hp.print_error(e)))
        else:
            hp.print_error(e)
        engmt.logger.error(e)
        raise
    logger.info("Time after initiating next request %s with response: [%s/%s) -> %s]", time.time() - _time, engmt.get('customer_response'), engmt.get('customer_state'), r.get('name'))
    if recognized_speech:
        r['recognized_speech'] = recognized_speech
    r['show_feedback'] = r.get('show_feedback') or (kwargs.get('query_type') in [ 'speech', 'type'])
    r['console'] = engmt.stream.getvalue()
    r['response_id'] = audio_id
    if channel_id:
        action.do_action(
            engmt.act.to_init(get_data = False), 
            {
                '_action': 'communicate',
                '_name': 'conversation_response',
                'receiver': {
                    "push_tokens": [channel_id],
                    },
                'title': "Response for {}".format(
                    customer_state
                    ),
                "message" : r["placeholder"],
                'enterprise_id':enterprise_id,
                "notification_data": r,
                "push_provider": channel,
                "credentials": kwargs.get('channel_credentials')
            }
        )
    logger.info("Time before saving audio file  %s", time.time() - _time)
    i = engmt.customer
    t = time.time() - _time
    if isinstance(kwargs.get('audio_start_time'), (int, long, float)):
        tt = time.time() - kwargs.get('audio_start_time')
    else:
        tt = t
    for a__ in ["created", "updated", "response_rating", "recognition_rating"]:
        i.pop(a__, None)
    i.update({
        'enterprise_id': enterprise_id,
        'conversation_id': conversation_id,
        'customer_id': customer_id,
        'user_id': user_id,
        'role': role,
        'engagement_id': engmt.engagement_id,
        'previous_system_response': system_response,
        'system_response': r.get("name"),
        'customer_state': engmt.get('customer_state'),
        'customer_response': engmt.get('customer_response'),
        'raw_audio': audio_file,
        'tag_1': engmt.get('_tag_1'),
        'tag_2': engmt.get('_tag_2'),
        'tag_3': engmt.get('_tag_3'),
        'tag_4': engmt.get('_tag_4'),
        'tag_5': engmt.get('_tag_5'),
        'recognized_speech': recognized_speech,
        'placeholder': r.get('placeholder'),
        'registered_entities': r.get('registered_entities'),
        'conversation_response': r,
        'response_time': t,
        'total_response_time': tt,
        'language': engmt.get('language'),
        'voice_id': engmt.get('voice_id'),
        'origin': kwargs.get('origin'),
        'query_type': kwargs.get('query_type'),
        'audio_id': audio_id,
        'created': hp.time()
    })
    action.do_action.delay({
        'enterprise_id': enterprise_id,
        'role': 'admin',
        },{
            '_action': 'post',
            '_name': "Update audio logs", 
            '_model': 'audio_logs',
            '_instance': i
            })
    logger.info("Time after Served the request: %s, [%s/%s) -> %s]", time.time() - _time, engmt.get('customer_response'), engmt.get('customer_state'), r.get('name')) 
    r['start_timestamp'] = str(start_timestamp)
    r['timestamp'] = hp.time()
    return r

@action.add_action_method("converse")
def converse(self, conversation_id, customer_id = None, action_name = None, **kwargs):
    timeout = kwargs.pop('_timeout')
    fus = kwargs.pop('_current_followups', [])
    kwargs.update(kwargs.pop('_data', kwargs.pop('data', {})))
    cacher = kwargs.pop('_cacher_name', None)
    scache = None
    cc = {}
    if cacher:
        scache = JsonBDict(cacher, [unicode], ['text'], DB_NAME = self.enterprise_id)
        cc = scache.get(customer_id, {})
        kwargs.update(cc)
    kwargs = self.evaluate_args(kwargs)
    if timeout:
        engmt = Engagement(
            self.enterprise_id, 
            conversation_id = conversation_id, 
            customer_id = customer_id,
            role = self.role, 
            user_id = self.user_id, 
            engagement_id = kwargs.get('engagement_id')
        )
        if fus:
            kw = {'customer_state~': fus}
        else:
            kw = {}
        last = engmt.history_model.list(_as_option = True, _page_size = 1, timestamp = '{},'.format(timeout), **kw)
        if last:
            return None
    logger.info("Converse for conversation %s in Enterprise %s", conversation_id, self.enterprise_id)
    r = next(self.enterprise_id, conversation_id, customer_id, self.role, self.user_id, **kwargs)
    self.add_to_data(r, action_name)
    if cacher:
        cc.update({'system_response': r['name'], 'engagement_id': r['engagement_id']})
        cc.pop('customer_state', None)
        cc.pop('customer_reponse', None)
        if scache:
            scache[customer_id] = cc
    fus = r.get('data', {}).get('_follow_ups', [])
    for k in fus:
        logger.info("Sending nudge: %s for conversation %s in enterprise %s for cutomer %s", k, conversation_id, self.enterprise_id, customer_id)
        na = {
            "_action": "converse",
            "_conversation_id": conversation_id,
            "_customer_id": customer_id,
        }
        na['_timeout'] = hp.now(as_datetime = False)
        na['_current_followups'] = fus
        na.update(kwargs)
        action.do_action.apply_async(
            args = [
                {
                    "enterprise_id": self.enterprise_id,
                    "role": self.role,
                    "user_id": self.user_id
                },
                na,
                "Nudge_for_conversation_{}_{}_in_enterprise_{}".format(conversation_id, k, self.enterprise_id)
            ], countdown = r.get('wait', 10000)/1000.0
        )
    return r

@action.add_action_method('add_agent_response')
def add_agent_response(self, conversation_id, customer_id, role, agent_id, placeholder, engagement_id = None, **kwargs):
    engmt = Engagement(
        self.enterprise_id, 
        conversation_id = conversation_id, 
        customer_id = customer_id, 
        role = role, 
        user_id = customer_id, 
        engagement_id = engagement_id
    )
    return engmt.save_agent_response(agent_id, placeholder, **kwargs)
    

@action.add_action_method("synthesize")
def do_synthesize(self, conversation_id, utterance, action_name = None, **kwargs):
    """
    kwargs:
        utterance
        voice_id
        synthesize_now (Creates is the audio and params immediately, default: True)
        refresh_synth (Re-create the audio and params)
        voice (input can be an audio file directly)
        csv_response (Responds with the csv parameters within the 
        response_channels
    """
    kwargs.update(kwargs.pop('_data', kwargs.pop('data', {})))
    kwargs = self.evaluate_args(kwargs)
    r = synthesize.synthesize(
        self.enterprise_id, conversation_id, 
        utterance,
        language = kwargs.get('voice_id', kwargs.get('language', True)),
        avatar_id = kwargs.get('avatar_id'),
        _async = not kwargs.get('synthesize_now', kwargs.get('csv_response', True)),
        _cached = not kwargs.get('refresh_synth'), 
        _audio = kwargs.get('voice'),
        _as_csv = kwargs.get('csv_response', True),
        channels = kwargs.get('response_channels') or kwargs.get('channels')
    )
    self.add_to_data(r, action_name)
    return r

@celery.task(name = 'create_zip')
def create_zip(tdir, path = None):
    return hp.zip_dir(tdir, path)


@action.add_action_method("translate")
def do_translate(self, action_name = None, **kwargs):
    kwargs.update(kwargs.pop('_data', kwargs.pop('data', {})))
    kwargs = self.evaluate_args(kwargs)
    r = translate.translate(
                    target = kwargs.get('target',[]),
                    dest_lang = kwargs.get('dest_lang',"en"),
    )
    self.add_to_data(r, action_name)
    return r
