import os, sys
from copy import deepcopy as copyof, copy
import dill
import types
import helpers as hp
import persist
from pgpersist import get_meta_classes, geolocation, date, datetime, dtime, stringlist, numberlist, objectlist
from pyArango.connection import Connection
from pyArango import collection as COL
from pyArango import validation as VAL
from pyArango.theExceptions import ValidationError as ArangoValidationError, QueryError, CreationError, DocumentNotFoundError, DeletionError, AQLQueryError
    
DEFAULT_DB_NAME = 'i2ce'
DATABASE_URL = os.environ.get('ARANGO_URL', "http://localhost:8529")
if not DATABASE_URL.startswith('http'):
    DATABASE_URL = "http://{}".format(DATABASE_URL)
USERNAME = os.environ.get('ARANGO_USERNAME', 'root')
PASSWORD = os.environ.get('ARANGO_PASSWORD', '')
TZ = hp.timezone('UTC')

logger = hp.get_logger(__name__)

class LinkError(hp.I2CEError):
    pass

class ArangoError(hp.I2CEError):
    pass

class metabasearango(type):
    all_instances = {}
    def __call__(cls, name, *args, **kwargs):
        return get_meta_classes(cls, metabasearango.all_instances, name, *args, **kwargs)

class ardict(persist.pdict):

    _allowed_key_types = (unicode,)
    _allowed_value_types = (dict, )
    __metaclass__ = metabasearango

    def __init__(self, name, key_types = None, key_names = None, indexes = None, iterable = None, DB_NAME = None, **kwargs):
        self.dbname = DB_NAME or DEFAULT_DB_NAME
        indexes = indexes or []
        for i, ind in enumerate(indexes):
            if not isinstance(ind, tuple):
                indexes[i] = (ind, False, False, str)
        self._db = self._connect(self.dbname)
        if self._exists('master___table'):
            self._mt = self._db.collections.get('master___table')
        else:
            try:
                self._mt = self._db.createCollection(name = 'master___table', keyOptions = {'allowUserKeys': True})
            except CreationError as e:
                logger.warning("Probably creating master___table in parallel")
                self._mt = self._db.collections.get('master___table')
                if self._mt is None:
                    raise
        super(ardict, self).__init__(name, key_types, key_names, indexes = indexes, iterable = iterable, **kwargs)

    def _create(self, cached = False, name = None):
        self._ec = '{}__Edge'.format(self.name)
        if not self._exists(self.name):
            self._c = self._db.createCollection(name = self.name, keyOptions = {'allowUserKeys': True})
            self._update_master()
        else:
            self._c = self._db.collections.get(self.name)
        if not self._exists(self._ec):
            self._e = self._db.createCollection(name = self._ec, className = 'Edges')
        else:
            self._e = self._db.collections.get(self._ec)
        self.create_indexes()
        self._update_master()
        self._db = self._connect(self.dbname)
        self._c = self._db.collections.get(self.name)
        self._e = self._db.collections.get(self._ec)

    def create_indexes(self):
        indexes = self._c.getIndexes()
        now_indexes = {
            (x.infos['fields'][0], x.infos['type']): x for x in (
                indexes['geo'].values() + \
                indexes['skiplist'].values() + \
                indexes['hash'].values()
            )
        }
        for a, u, r, t in self.indexes:
            t = hp.make_list(t)
            if any(issubclass(
                    t_, (str, unicode, int, long, float, date, datetime, dtime)
                ) for t_ in t) and a not in reduce(
                    lambda x, y: x + y, [_.infos['fields'] for _ in indexes['skiplist'].values()], []
                ):
                self._c.ensureSkiplistIndex([a], unique = u, sparse = not r)
            else:
                now_indexes.pop((a, 'skiplist'), None)
            if any(issubclass(t_, (geolocation,)) for t_ in t) and a not in reduce(
                lambda x, y: x + y, [_.infos['fields'] for _ in indexes['geo'].values()], []
                ):
                self._c.ensureGeoIndex([a])
            else:
                now_indexes.pop((a, 'geo'), None)
            if any(
                issubclass(
                        t_, (stringlist, numberlist, dict, objectlist)
                    ) for t_ in t
                ) and '{}[*]'.format(a) not in reduce(
                    lambda x, y: x + y,
                    [_.infos['fields'] for _ in indexes['hash'].values()], []
                ):
                self._c.ensureSkipListIndex(["{}[*]".format(a)], unique = u, sparse = not r)
            else:
                now_indexes.pop((a, 'hash'), None)
        while now_indexes:
            # Deleting indexes that don't exist now
            i = now_indexes.pop(next(iter(now_indexes)))
            i.delete()
        return

    def _update_master(self):
        self._upsert(self.name, {
                'key_names': self.key_names, 
                'key_types': [n.__name__ for n in self._key_types], 
            }, _cname = 'master___table'
        )

    def _drop(self):
        try:
            self._c.delete()
        except DeletionError:
            pass
        try:
            self._e.delete()
        except DeletionError:
            pass
        try:
            s = self._mt[self.name]
        except DocumentNotFoundError as e:
            pass
        else:
            s.delete()
            s.save()
        self._db = self._connect()


    def _make_key(self, key):
        key = hp.make_list(key)
        key = '--'.join(key)
        return hp.make_uuid(key)

    def _unmake_key(self, key):
        if key is None:
            return None
        try:
            key = hp.unmake_uuid(key)
        except Exception as e:
            logger.error("Got an error in converting key: %s", key)
            raise
        return key.split('--')

    def _upsert(self, key, value, *args, **kwargs):
        if not isinstance(value, dict):
            value = {'_value': value}
        value['_key'] = self._make_key(key)
        for i, k in enumerate(self.key_names):
            value[k] = key[i]
        command = """
            UPSERT {{ _key: @_key }}
            INSERT {{ _key: @_key, __created: DATE_NOW(), __updated: DATE_NOW(), {values} }}
            UPDATE {{ __updated: DATE_NOW(), {values} }} in {cname}
        """.format(
                cname = kwargs.get('_cname', self.name),
                values = ", ".join(['{k}: @{k}'.format(k = k) for k, v in value.iteritems()])
        )
        try:
            result = self._db.AQLQuery(command, rawResults = False, batchSize = 1, bindVars = value)
        except AQLQueryError as e:
            logger.error("Error in executing query: %s, values: %s", command, value)
            raise ArangoError(e.errors['errorMessage'])
        except Exception as e:
            logger.error("Error in executing query: %s, values: %s", command, value)
            raise
        return result.cursor

    _update = _upsert
    _insert = _upsert

    def _select(self, key, *args, **kwargs):
        key = self._make_key(key)
        try:
            ret = self._c[key]
        except DocumentNotFoundError as e:
            raise persist.PersistKeyError("No rows found for key: {}".format(key), log = kwargs.get('log_error', True))
        else:
            _key, ret = self._jsonify(ret, **kwargs)
        return ret

    def _jsonify(self, ret, _timestamp = False, _id = False, *args, **kwargs):
        ret = ret.getStore()
        _id = _id or ret.pop('_id', None); ret.pop('_rev', None); _key =  ret.pop('_key', None); ret.pop('__created', None);
        if '_value' in ret:
            return self._unmake_key(_key), ret['_value']
        logger.debug("Return: %s", ret)
        if _timestamp and '__updated' in ret: 
            ret['__timestamp'] = TZ.localize(hp.datetime.utcfromtimestamp(ret.pop('__updated')/1000.))
        else:
            ret.pop('__updated', None)
        return self._unmake_key(_key), ret

    def _delete(self, key, *args, **kwargs):
        key = self._make_key(key)
        try:
            r = self._c[key]
        except DocumentNotFound as e:
            return False
        else:
            _key, ret = self._jsonify(r)
            r.delete()
            r.save()
            self._delete_many({"_from": key, "_to": key}, condition_type = 'OR', _cname = self._ec)
        return ret


    def _count(self):
        return self._c.count()

    def _commit(self):
        pass

    def _close(self):
        pass

    def _exists(self, name):
        if self._db.hasCollection(name):
            return True
        return False

    def _get_key_def(self):
        try:
            ret = self._mt[self._make_key(self.name)]
        except DocumentNotFoundError as e:
            return None, None
        return ret['key_names'], [self._name2type[i] for i in ret['key_types']]

    def _get_last_updated(self):
        return self._mt[self._make_key(self.name)].getStore()['__updated']/1000.

    def _connect(self, trial = 0, dbname = None, **kwargs):
        try:
            if dbname is None: dbname = self.dbname
            conn = Connection(username = USERNAME, password = PASSWORD, arangoURL = DATABASE_URL)
            db = conn.databases.get(dbname)
            if db is None:
                try:
                    db = conn.createDatabase(name = dbname)
                except CreationError as e:
                    logger.warning("Probably connecting in parallel: %s", e)
                    db = conn.databases.get(dbname)
                    if db is None:
                        raise
            return db
        except Exception as e:
            raise
        
    def _pre_filter(self, filter_by = None, sort_by = None, unique = None, condition_type=None, start = 0, limit = None, load_total = None, base_filter = None, filter_by_types = None, last_updated = None, updated_before = None, _cname = None, *args, **kwargs):
        if filter_by and ('AND' in filter_by or 'OR' in filter_by):
            raise persist.PersistFilterError("Filter by clause currently only supports dict type of filters")
        joiner = ' && '
        if condition_type: joiner = ' && ' if condition_type == 'AND' else ' || '
        if filter_by_types is None: filter_by_types = {}
        clauses, orderbys, unique, values, within, coordinates = self._handle_clauses(filter_by, sort_by, unique, filter_by_types = filter_by_types)
        clauses_string = ""; base_filter_string = ""; last_updated_string = ""; limits_string = "";
        clauses_string = "FILTER ({})".format(" {} ".format(joiner).join(clauses)) if clauses else ""
        if base_filter:
            clauses, _, _, v = self._handle_clauses(base_filter)
            values.update(v)
            base_filter_string = 'FILTER ({})'.format(" && ".join(clauses))
        if last_updated:
            try:
                last_updated = hp.to_datetime(last_updated)
            except Exception as e:
                logger.error("Could not parse last_updated value : %s", last_updated)
                raise 
            last_updated_string = 'FILTER (u.__updated >= @__updated)'
            values['__updated'] = last_updated
        elif updated_before:
            try:
                updated_before = hp.to_datetime(updated_before)
            except Exception as e:
                logger.error("Could not parse updated_before value : %s", updated_before)
                raise 
            last_updated_string = 'FILTER (u.__updated < @__updated)'
            values['__updated'] = updated_before

        if isinstance(load_total, dict):
            # For loading total
            sc = """
            LET sc = ( 
                FOR u IN {within}({table_name}{coordinates})
                    {clauses}
                    {base_filter}
                    {last_updated}
                    RETURN u 
            )
            RETURN LENGTH(sc)
            """.format(
                table_name = _cname or self.name,
                clauses = clauses_string,
                base_filter = base_filter_string,
                last_updated = last_updated_string,
                orderbys = orderbys,
                within = within,
                coordinates = coordinates
            )
            cur = self._db.AQLQuery(sc, rawResults = True, bindVars = values)
            try:
                load_total['total_number'] = cur.next() 
            except StopIteration:
                load_total['total_number'] = 0
        
        if isinstance(limit, (int, float)):
            limits_string = 'LIMIT @_start, @_limit'
            values["_start"] = start or 0
            values["_limit"] = limit

        return clauses_string, base_filter_string, last_updated_string, orderbys, limits_string, values, load_total, within, coordinates


    def _delete_many(self, filter_by, condition_type = 'AND', 
            filter_by_types = None, base_filter = None, last_updated = None, updated_before = None, _cname = None, *args, **kwargs
        ):
        load_total = {'total_number': 0}
        clauses_string, base_filter_string, last_updated_string, orderbys, limits_string, values, load_total, within, coordinates = self._pre_filter(
            filter_by, condition_type = condition_type,  
            base_filter = base_filter, 
            filter_by_types = filter_by_types, 
            last_updated = last_updated,
            updated_before = updated_before,
            load_total = load_total,
            _cname = _cname
        )
        s = """
        FOR u IN {table_name}
            {clauses}
            {base_filter}
            {last_updated}
            REMOVE u in {table_name}
        """.format(
            table_name = _cname or self.name,
            clauses = clauses_string,
            base_filter = base_filter_string,
            last_updated = last_updated_string
        )
        logger.info("\n Query is\n %s: Value: %s", s, values)
        try:
            cur = self._db.AQLQuery(s, bindVars = values)
        except Exception as e:
            logger.error("error in statement: %s, %s", s, values)
            logger.error("database: %s", self._db)
            raise
        return load_total['total_number']

    def _update_many(self, instance, filter_by, condition_type = 'AND', 
            filter_by_types = None, base_filter = None, last_updated = None, updated_before = None, _cname = None, *args, **kwargs
        ):
        if not (instance and isinstance(instance, dict)):
            return 0
        load_total = {'total_number': 0}
        clauses_string, base_filter_string, last_updated_string, orderbys, limits_string, values, load_total, within, coordinates = self._pre_filter(
            filter_by, condition_type = condition_type,  
            base_filter = base_filter, 
            filter_by_types = filter_by_types, 
            last_updated = last_updated,
            updated_before = updated_before,
            load_total = load_total,
            _cname = _cname
        )
        s = """
        FOR u IN {within}({table_name}{coordinates})
            {clauses}
            {base_filter}
            {last_updated}
            UPDATE {{_key: u._key, {instance_exp} }} in {table_name}
        """.format(
            table_name = _cname or self.name,
            clauses = clauses_string,
            base_filter = base_filter_string,
            last_updated = last_updated_string,
            instance_exp = ", ".join("{k}: @{k}_exp_".format(k = k) for k in instance),
            within = within,
            coordinates = coordinates
        )
        for k, v in instance.iteritems():
            values["{k}_exp_".format(k=k)] = v
        logger.info("\n Query is\n %s: Value: %s", s, values)
        try:
            cur = self._db.AQLQuery(s, bindVars = values)
        except Exception as e:
            logger.error("error in statement: %s, %s", s, values)
            logger.error("database: %s", self._db)
            raise
        return load_total['total_number']


    def _filter(self, filter_by = None, sort_by = None, unique = None, condition_type=None, start = 0, limit = None, load_total = None, base_filter = None, filter_by_types = None, last_updated = None, updated_before = None, _cname = None, *args, **kwargs):
        clauses_string, base_filter_string, last_updated_string, orderbys, limits_string, values, load_tota, within, coordinates = self._pre_filter(
            filter_by, sort_by, unique, condition_type, start, limit, 
            load_total, base_filter, filter_by_types, last_updated, updated_before, _cname
        )
        s = """
        FOR u IN {within}({table_name}{coordinates})
            {clauses}
            {base_filter}
            {last_updated}
            {orderbys}
            {limits} 
            RETURN u
        """.format(
            table_name = _cname or self.name,
            clauses = clauses_string,
            base_filter = base_filter_string,
            last_updated = last_updated_string,
            orderbys = orderbys,
            within = within,
            coordinates = coordinates,
            limits = limits_string
        )
        logger.info("\n Query is\n %s: Value: %s", s, values)

        try:
            cur = self._db.AQLQuery(s, bindVars = values, batchSize = limit)
        except Exception as e:
            logger.error("error in statement: %s, %s", s, values)
            logger.error("database: %s", self._db)
            raise
        for ret in cur:
            _key, ret = self._jsonify(ret, _timestamp = bool(last_updated or updated_before))
            logger.debug("Fetching record %s", ret)
            yield _key, ret


    def _handle_clauses(self, filter_by = None, sort_by = None, unique = None, as_dict = False, do_lower = True, filter_by_types = None):
        if filter_by_types is None: filter_by_types = {}

        def handle_dict(d, init = '', as_dict = False, do_lower = True, filter_by_types = None):
            if filter_by_types is None: filter_by_types = {}
            within = ''; coordinates = '';
            clauses = []; values = {}
            # Check if there is any indexed geolocation, we can have only one of them
            gsp = map( 
                lambda x: x[0],
                filter(
                    lambda x: any(issubclass(_, geolocation) for _ in hp.make_list(x[1])), 
                    filter_by_types.iteritems()
                )
            )
            if gsp and filter(lambda x: x[0] in gsp, self.indexes):
                # even if a geolocation, it needs to be indexed
                gsp = hp.make_single(filter(lambda x: x[0] in gsp, self.indexes), force = True)[0]
                co = d.get(gsp, None)
                if isinstance(co, tuple) and len(co) == 3:
                    co = d.pop(gsp)  # Take it out of the clauses
                    co = (co[0], co[1], co[2]*1000.)
                    within = 'WITHIN'
                    coordinates = ', {}, {}, {}'.format(*co)
            for k, v in d.iteritems():
                list_type = bool(filter_by_types.get(k) and issubclass(filter_by_types[k], (list, dict)))
                eq = 'IN' if list_type else '=='; _any = ' ANY' if list_type else ''
                not_symbol = ''; not_clause = ''
                any_ = ' ANY' if isinstance(v, list) else '' 
                if k.endswith('~'):
                    eq = 'NOT IN' if list_type else '!='; 
                    not_clause = 'NOT '; not_symbol = '!'
                    k = k[:-1]
                if not isinstance(v, tuple):
                    if isinstance(v, (str, unicode)) and v.startswith('~'):
                        v = v[1:]
                        if (v.startswith('"') and v.startswith('"')) or (v.startswith("'") and v.startswith("'")):
                            v = v[1:-1]
                            spl = v
                        else:
                            spl = '|'.join(v.split('|'))
                        clauses.append(
                            '{ns}REGEX_TEST(TO_STRING(u.{k}), \'{q}.*({spl}).*{q}\', true)'.format(
                                k = k,
                                ns = not_symbol,
                                q = '\"' if list_type else '',
                                spl = spl
                            )
                        )
                    elif isinstance(v, (str, unicode)) and v.startswith('^'):
                        v = v[1:]
                        clauses.append(
                            '{ns}REGEX_TEST(TO_STRING(u.{k}), \'{q}{v}.*\', true)'.format(
                                k = k,
                                q = '\"' if list_type else '',
                                ns = not_symbol,
                                v = v
                            )
                        )
                    elif isinstance(v, (str, unicode)) and v.endswith('^'):
                        v = v[:-1]
                        clauses.append(
                            '{ns}REGEX_TEST(TO_STRING(u.{k}), \'{q}.*{v}{q}\', true)'.format(
                                k = k,
                                q = '\"' if list_type else '',
                                ns = not_symbol,
                                v = v
                            )
                        )
                    elif isinstance(v, (str, unicode)):
                        clauses.append(
                            '{ns}REGEX_TEST(TO_STRING(u.{k}), \'{q}{v}{q}\', {do_lower})'.format(
                                k = k,
                                q = '\"' if list_type else '',
                                do_lower = 'true' if do_lower else 'false',
                                ns = not_symbol,
                                v = v
                            )
                        )
                    elif v is None:
                        clauses.append("NULL {eq} u.{k}".format(k = k, eq = eq))
                    else:
                        clauses.append('@{k}{any_} {eq} u.{k}'.format(
                                k = k,
                                any_ = any_,
                                eq = eq
                            )
                        )
                        values[k] = v
                elif isinstance(v,tuple):
                    if len(v)==2:
                        if v[0] is None and v[1] is None:
                            pass
                        elif v[1] is None:
                            clauses.append("u.{k}{_any} >= @{k}".format(k = k, _any = _any))
                            values[k] = v[0]
                        elif v[0] is None:
                            clauses.append("u.{k}{_any} <= @{k}".format(k = k, _any = _any))
                            values[k] = v[1]
                        else:
                            clauses.append("u.{k}{_any} >= @{k}_1_ && u.{k}{_any} <= @{k}_2_".format(k = k, _any = _any))
                            values[k+'_1_'] = v[0]
                            values[k+'_2_'] = v[1]
                    elif len(v) == 3:
                        clauses.append("u.{k}[*][0] >= @{k}_1_ - @{k}_3_ && u.{k}[*][0] <= @{k}_1_ + @{k}_3_ && u.{k}[*][1] >= @{k}_2_ - @{k}_3_ && u.{k}[*][1] <= @{k}_2_ + @{k}_3_ ".format(k = k, _any = _any))
                        values[k+'_1_'] = v[0]
                        values[k+'_2_'] = v[1]
                        values[k+'_3_'] = v[2]
                else:
                    pass
            return clauses, values, within, coordinates


        def handle_filter_by(filter_by, as_dict = False, do_lower = True, filter_by_types = None):
            if filter_by_types is None: filter_by_types = {}
            if filter_by is None:
                cl = ''; vl = {}; w = ''; co = '';
            elif isinstance(filter_by, dict):
                cl, vl, w, co = handle_dict(filter_by, as_dict = as_dict, do_lower = do_lower, filter_by_types = filter_by_types)
            else:
                raise persist.PersistFilterError("Unknown format of type {} or incorrect length. Should be len(3)".format(type(filter_by)))
            return cl, vl, w, co

        def check_if_key(k):
            if k not in self.key_names:
                raise persist.PersistFilterError("Unique and Order by constraint {} cannot be applied on non-key fields".format(k))
            return self.key_names.index(k)

        def handle_sort_by(sort_by):
            def handle_tuple(o, obs):
                if isinstance(o, (list, tuple)) and len(o) >= 2 and isinstance(o[0], (str, unicode)) and isinstance(o[1], bool):
                    if o[0] == '__timestamp':
                        obs.append('u.__updated {}'.format('DESC' if o[1] else 'ASC'))
                    else:
                        obs.append('u.{} {}'.format(
                                o[0], 'DESC' if o[1] else 'ASC'
                            )
                        )
                else:
                    raise persist.PersistFilterError(
                        ("Unknown format of sort_by {}. "
                         "Should be tuple of (<key attr (str)>, <reverse(bool)>, <atype>)").format(type(o))
                    )
            if sort_by is None:
                return ''
            obs = []
            handle_tuple(sort_by, obs)
            return ('\nSORT ' + ', '.join(obs))

        clauses, values, within, coordinates = handle_filter_by(filter_by, do_lower = do_lower, filter_by_types = filter_by_types)
        orderbys = handle_sort_by(sort_by)

        logger.debug("After handing clauses, %s , %s, %s, %s, %s, %s", clauses, orderbys, unique, values, within, coordinates)
        return clauses, orderbys, '', values, within, coordinates

    def link(self, id1, id2, _bi_directional = True, _distance = None, _distance_attr = None, _default_distance = 1., **instance):
        try:
            id1 = self._select(id1, _id = True)['_id']
        except persist.PersistKeyError:
            raise LinkError("Cannot link id: %s, does not exist", id1)
        try:
            id2 = self._select(id2, _id = True)['_id']
        except persist.PersistKeyError:
            raise LinkError("Cannot link id: %s, does not exist", id1)
        instance.update({
            "_from": id1,
            "_to": id2,
            "_weight_": _distance if isinstance(_distance, (int, float, long)) else instance.get(_distance_attr, _default_distance)
        })
        self._upsert(
            hp.make_list(id1) + hp.make_list(id2), 
            instance,
            _cname = self._ec
        )
        if _bi_directional:
            instance.update({
                "_from": id2,
                "_to": id1,
                "_weight_": _distance if isinstance(_distance, (int, float, long)) else instance.get(_distance_attr, _default_distance)
            })
            self._upsert(
                hp.make_list(id2) + hp.make_list(id1),
                instance,
                _cname = self._ec
            )
        return self._make_key(hp.make_list(id1) + hp.make_list(id2))

    def unlink(self, id1, id2, _bi_directional = True):
        try:
            id1 = self._select(id1, _id = True)['_id']
        except persist.PersistKeyError:
            raise LinkError("Cannot link id: %s, does not exist", id1)
        try:
            id2 = self._select(id2, _id = True)['_id']
        except persist.PersistKeyError:
            raise LinkError("Cannot link id: %s, does not exist", id2)
        r = self._e[self._make_key(hp.make_list(id1) + hp.make_list(id2))]
        r.delete()
        #r.save()
        _key, ret = self._jsonify(r)
        if _bi_directional:
            r = self._e[self._make_key(hp.make_list(id2) + hp.make_list(id1))]
            r.delete()
            #r.save()
        return ret

    def closest(self, id1, id2 = None, limit = None, filter_by = None, base_filter = None, last_updated = None, updated_before = None, *args, **kwargs):
        clauses_string, base_filter_string, last_updated_string, orderbys, limits_string, values, load_total, within, coordinates = self._pre_filter(
            filter_by, base_filter = base_filter, last_updated = last_updated, updated_before = updated_before
        )
        try:
            id1_ = self._select(id1, _id = True)['_id']
        except persist.PersistKeyError:
            raise LinkError("No %s id found in collection: %s", id1, self.name)
        if id2 is not None:
            try:
                id2_ = self._select(id2, _id = True)['_id']
            except persist.PersistKeyError:
                raise PersistKeyError("No %s id found in collection: %s", id2, self.name)
            s = """
            FOR v, e IN OUTBOUND SHORTEST_PATH
                \"{id1}\" TO \"{id2}\" {table_name}
                OPTIONS {{ weightAttribute: \"_weight_\" }}
                RETURN {{
                    {id_}, "distance": e._weight_
                }}
            """.format(
                table_name = self._ec,
                clauses = clauses_string,
                base_filter = base_filter_string,
                last_updated = last_updated_string,
                limits = limits_string,
                id1 = id1_,
                id2 = id2_,
                id_ = ', '.join('{k}: v.{k}'.format(k = k_) for k_ in self.key_names), 
            )
        else:
            s = """
            FOR v, e IN 1..1 OUTBOUND \"{id1}\" {table_name}
                OPTIONS {{ weightAttribute: \"_weight_\", bfs: true }}
                RETURN {{
                    {id_}, "distance": e._weight_
                }}
            """.format(
                table_name = self._ec,
                clauses = clauses_string,
                id_ = ', '.join('{k}: v.{k}'.format(k = k_) for k_ in self.key_names), 
                base_filter = base_filter_string,
                last_updated = last_updated_string,
                limits = limits_string,
                id1 = id1_
            )
        logger.info("\n Query is\n %s: Value: %s", s, values)
        try:
            cur = self._db.AQLQuery(s, bindVars = values, batchSize = limit, rawResults = True)
        except Exception as e:
            logger.error("error in statement: %s, %s", s, values)
            logger.error("database: %s", self._db)
            raise
        for ret in cur:
            logger.debug("Fetching record %s", ret)
            yield ret




if __name__ == '__main__':
    d = ardict('test', DB_NAME = 'cat', a = {'a': 'a', 'b': 'b'}, b = {'b': 'b'}, c = {'a': 3.0, 'b': 5}, d = {'a':  ['a','b','c']}, e = {'a': 1, 'b': 2}, f = {'a': {'a': 1, 'b': 2}, 'b': {'c': 3}})
    print "This is the document", d
    print "This is the value of key 'c'", d.get('c')
    d['c'] = {'a': 'saldka', 'c': 'skajfdf'}
    print "Length of collection", len(d)
    print "Popping c", d.pop('c')
    print "Popping unavailable g", d.pop('g', None)
    print "Length of collection", len(d)
    for k,v in d.iteritems({'a': ['a', 'b']}):
        print 'filter', k, v
    d.clear()
