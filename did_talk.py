import requests
import time, json
import i2ce_routes
import os
import helpers as hp
from flask import request, jsonify


#YW5hbnRoQGlhbWRhdmUuYWk:Mi4nGt7BtskQf-RtbTX2c
DID_AUTH = os.environ.get("D_ID_AUTH_KEY",'aW5mb0Bzb2Npb2dyYXBoc29sdXRpb25zLmlu:Fn4MaHOuFKtj1JDCio7aa')

reqUrl = "https://api.d-id.com/talks"
logger = hp.get_logger(__name__)

def generateDIDTalk(auth_key, soure_url, audio_url=None, text=None,  **config):
    headersList = {
        "accept": "application/json",
        "authorization": "Basic {}".format(auth_key),
        "Content-Type": "application/json"
    }
    scr = {
        "type": "text" if text else "audio"
    }
    if text:
        scr["input"] = text
    else:
        scr["audio_url"] = audio_url

    
    config["stitch"] = True

    payload = json.dumps({
        "source_url": soure_url,
        "script": scr,
        "config": config
    })
    response = requests.request("POST", reqUrl, data=payload,  headers=headersList)
    
    logger.info('response %s:%s', response.status_code, response.text)
    if response.status_code < 400:
        response  = response.json()
        return response["id"], True
    
    return response, False


def getDIDStatus(auth_key, id, tries = 5):
    if not tries:
        return None, None
    headersList = {
        "accept": "application/json",
        "authorization": "Basic {}".format(auth_key),
        "Content-Type": "application/json"
    }
    time.sleep(2)
    
    response = requests.request("GET", '{}/{}'.format(reqUrl, id),  headers=headersList)

    # print(response.text)
    if response.status_code < 400:
        response  = response.json()
        if response["status"] == "done":
            return response["result_url"], True
    else:
        return response.json(), False
    
    return getDIDStatus(auth_key, id, tries = tries-1)



#https://staging.iamdave.ai/webhook/dave/maruti_core/predict-formulation"
@i2ce_routes.add_webhook_action('d-id-synth', 'generate-video', True, authenticate=True)
@i2ce_routes.add_webhook_action('dave_expo', 'generate-video', True, authenticate=True)
def generateTalks(*args, **kwargs):
    """
    Webhook to generate a talk using d-id.

    Parameters (all optional):

    - `audio_url`: The URL of the audio file to use for synthesis.
    - `image_url`: The URL of the image to use in the talk.
    - `text`: The text to use for the talk.

    Returns a JSON object with the following keys:

    - `status`: One of "success" or "error".
    - `id`: The ID of the talk, if successful.

    If the `status` is "error", the response will have a 400 status code.
    """
    if not DID_AUTH:
        return jsonify({"status": "error", "message": "DID_AUTH not set"}), 400
    audio_url = kwargs.get("audio_url")
    image_url = kwargs.get("image_url")
    text = kwargs.get("text")
    
    en_id = kwargs.get('enterprise_id', 'dave_expo')
    rs, st = generateDIDTalk(DID_AUTH, image_url, audio_url = audio_url, text = text)

    if st:
        return jsonify({"status": "success", "id": rs, "status_url": "/webhook/dave/{}/status?id={}".format(en_id, rs)})
    else:
        logger.info(rs)
    return jsonify({"status": "error", "message": "Couldn't able to generate talks"}), 400


@i2ce_routes.add_webhook_action('d-id-synth', 'status', True)
@i2ce_routes.add_webhook_action('dave_expo', 'status', True)
def statusTalks(*args, **kwargs):
    """
    Webhook to get the status of a talk.

    Parameters:

    - `id`: The ID of the talk.

    Returns a JSON object with the following keys:

    - `status`: One of "success" or "error".
    - `url`: The URL of the talk, if successful.

    If the `status` is "error", the response will have a 400 status code.
    """
    if not DID_AUTH:
        return jsonify({"status": "error", "message": "DID_AUTH not set"}), 400
    id = kwargs.get("id")
    if not id:
        return jsonify({"status": "error", "message": "id is required"}), 400

    resp, sts = getDIDStatus(DID_AUTH, id, tries=2)

    if sts is None:
        return jsonify({"status": "Processing"})
    if sts == False:
        return jsonify({"status": "error", "message": resp}), 400

    if sts and resp:
        return jsonify({"status": "success", "id": id, "url": resp})
    
    key = 'd-id-resp'
    if kwargs.get('enterprise_id', '') in ['dave_expo']:
        key = 'resp'

    return jsonify({"status": "error", "message": "Couldn't able to generate talks", key: resp}), 400

    