import os, sys
import helpers as hp
from copy import deepcopy as copyof, copy
import types
from libraries import json_tricks as json
from contextlib import contextmanager
import time
import numpy as np
from functools import partial
from datetime import datetime
import operator
from objectifier import encode, decode
import decimal
    
    
DEBUG_LEVEL = hp.logging.INFO
logger = hp.get_logger(__name__, DEBUG_LEVEL)


class PersistError(hp.BaseError):
    pass

class ValueTypeError(hp.BaseError):
    pass

class KeyTypeError(hp.BaseError):
    pass

class PersistKeyError(hp.BaseError, KeyError):
    pass

class PrimaryKeyError(hp.BaseError):
    pass

class PersistFilterError(hp.BaseError):
    pass

class PersistCollectionError(hp.BaseError):
    pass

class PersistCollectionKeyError(hp.BaseError):
    pass

class PersistFilterError(hp.I2CEError):
    pass

class NotImplementedError(hp.BaseError):
    def __init__(self, method):
        super(NotImplementedError, self).__init__("{} method has not been implmented".format(method))




class pdict(object):

    _allowed_key_types = (unicode, int, float)
    _allowed_value_types = (type(None), unicode, bool, int, float, list, dict)

    @classmethod
    def type2names(cls, keys = None, iterob = tuple):
        if keys is None: 
            keys = cls._allowed_value_types
        return iterob(t.__name__ for t in keys)

    def name2types(self, keys = None, iterob = list):
        if keys is None: keys = self._allowed_value_types_names
        return iterob(k if isinstance(k, type) else self._name2type[k] for k in keys)

    def __init__(self, name, key_types = None, key_names = None, indexes = None, iterable = None, time_zone = None, cached = False, **kwargs):
        """
        key types is a list with the datatypes of the keys
            if it is not a list or tuple, then it is considered as a single key field, else a compound key field
        key names is the list of names of the keys. If not provided, then it is auto generated
        indexes is a list of tuples with (<column name (str)>, <unique or not (bool)>, where clause if it is a partial index)
            where clause if of type (<column name <str>, <equality>, <value>)
        """
        self._allowed_key_types_names = self.type2names(self._allowed_key_types)
        self._allowed_value_types_names = self.type2names(self._allowed_value_types)
        self._convertible_types = tuple(t for t in (self._allowed_value_types + self._allowed_key_types) if t != object)
        self._name2type = dict((n, t) for n, t in zip(self._allowed_value_types_names, self._allowed_value_types))
        kt = dict((n, t) for n, t in zip(self._allowed_key_types_names, self._allowed_key_types))
        self._name2type.update(kt)
        self.name = hp.normalize_join(name)
        self.string_encoding = None
        self._conn = None
        self._in_transaction = []
        self.time_zone = time_zone
        self._updated = None

        # Currently we don't support on-the-fly update of tables 
        if self._exists(self.name) :
            self.key_names, self._key_types = self._get_key_def()
            if not self._key_types and not key_types:
                raise PersistError("""
                        Dictionary of name {} exists, but no key_types specified.
                        Auto-migration is not supported currently.
                        """.format(self.name)
                )
            if not self.key_names and not key_names:
                raise PersistError("""
                        Dictionary of name {} exists, but no key_names specified.
                        Auto-migration is not supported currently.
                        """.format(self.name)
                )
            if (key_types or key_names or indexes):
                if not self._key_types:
                    self._key_types = hp.make_list(key_types or [unicode])
                elif key_types and hp.make_list(key_types) != self._key_types:
                    raise PersistError("""
                        Dictionary of name {} exists, and has different key_types from previous defined.
                        Previous: {}
                        Current: {}
                        Auto-migration is not supported currently.
                        """.format(self.name, self._key_types, key_types)
                    )
                if not self.key_names:
                    self.key_names = hp.make_list(key_names or ['_key_{}'.format(i) for i, k in enumerate(self._key_types)])
                elif key_names and hp.make_list(key_names) != self.key_names:
                    raise PersistError("""
                        Dictionary of name {} exists, and has different key_name from previous defined.
                        Previous: {}
                        Current: {}
                        Auto-migration is not supported currently.
                        """.format(self.name, self.key_names, key_names)
                    )
        else:
            self._key_types = hp.make_list(key_types or [unicode])
            self.key_names = hp.make_list(key_names or ['_key_{}'.format(i) for i, k in enumerate(self._key_types)])
        self.indexes = indexes or []

        self._key_types = self.name2types(self._key_types)
        if any(not issubclass(k, self._allowed_key_types) for k in self._key_types):
            raise PersistKeyError("pdict supports keys of types {} only (given: {})".format(self._allowed_key_types, self._key_types))
        if len(self.key_names) != len(self._key_types):
            raise PersistKeyError("Key name is not equal to number of key types")
        self._create(cached)
        self.update(iterable, **kwargs)
    
    def _normalize(self, key, check = True):
        if check:
            return self._check_key_type(key)
        if not isinstance(key, (list, tuple)):
            key = [key]
        return list(key)


    def _denormalize(self, key, as_str = False):
        def do_type(k, t):
            if isinstance(k, str) and issubclass(t, unicode):
                return k.decode('utf-8').encode('utf-8')
            if not isinstance(k, t):
                return t(k)
            return k

        if isinstance(key, (list, tuple)):
            r = tuple(do_type(k, t) for k, t in zip(key, self._key_types))
            if len(r) == 1:
                return r[0]
            elif as_str:
                return str(r)
            else:
                return r
        return self._key_types[0](key)

    def _check_value_type(self, value):
        t = self._type_2_name(value, as_string = False)
        if t is None:
            raise ValueTypeError("pdict supports values of types '{}', not {} in key {}".format(
                    self._allowed_value_types, type(value), value,
                )
            )
        return t
    
    def _type_2_name(self, value, as_string = True):
        for t in self._allowed_value_types:
            if isinstance(value, t):
                return t.__name__ if as_string else t
        return None
        
    def _check_key_type(self, key):
        def do_type(k, t):
            if isinstance(k, str) and issubclass(t, unicode):
                return k.decode('utf-8').encode('utf-8')
            if not isinstance(k, t):
                return t(k)
            return k
        if not isinstance(key, (list, tuple)):
            key = [key]
        tmap = map(type, key)
        ret_key = []
        for t, t1, k in zip(self._key_types, tmap, key):
            try:
                ret_key.append(do_type(k, t))
            except (ValueError, TypeError, UnicodeDecodeError, UnicodeEncodeError):
                raise KeyTypeError("Assigned key tuple is {}. Given key tuple is {} for key {}".format(
                        self._key_types, tmap, key
                    )
                )
        return ret_key

    def __setitem__(self, key, value):
        nkey = self._normalize(key)
        value_type = self._check_value_type(value)
        logger.debug('value_type {} type(value_type)={}'.format(value_type, type(value_type)))
        with self._transaction():
            try:
                self._upsert(nkey, value, value_type)
            except NotImplementedError:
                try:
                    self._insert(nkey, value, value_type)
                except PrimaryKeyError:
                    self._update(nkey, value, value_type)
        return nkey

    def __getitem__(self, key, value_type = None, log_error = False):
        nkey = self._normalize(key)
        try:
            return self._select(nkey, value_type = value_type, log_error = False)
        except (KeyError, PersistKeyError) as e:
            s = u"Key {} (normalized {}) not found in PDict {}".format(
                    key, u','.join(nkey), self.name
                )
            logger.error(s)
            raise PersistKeyError(s, log = log_error)

    def __contains__(self, key, value_type = None):
        nkey = self._normalize(key)
        try:
            self._select(nkey, value_type = value_type, log_error = False)
        except PersistKeyError:
            return False
        return True

    def __delitem__(self, key, return_value = False, return_key = False, value_type = None, log_error = True ):
        nkey = self._normalize(key)
        try:
            ret = self.__getitem__(nkey, value_type = value_type)
        except Exception as e:
            if log_error:
                logger.error("Select failed with exception: %s", e)
            raise PersistKeyError(
                u"Key {} (normalized {}) not found in PDict {}".format(
                    key, nkey, self.name
                ),
                log = log_error
            )
        else:
            with self._transaction():
                self._delete(nkey)
        if return_key and return_value:
            return key, ret
        if return_value:
            return ret
        if return_key:
            return key

    def __len__(self):
        return self._count()

    def pop(self, key = None, default = PersistKeyError):
        try:
            return self.__delitem__(key, return_value = True, log_error = False)
        except PersistKeyError:
            if isinstance(default, type) and issubclass(default, PersistKeyError):
                raise
            return default

    def popitem(self):
        k, v = self.__delitem__(k, return_key = True, return_value = True)
        return self._denormalize(k), v

    def update(self, iterable = None, **kwargs):
        iterable = iterable or []
        kwargs.update(iterable)
        if kwargs:
            with self._transaction():
                for k, v in kwargs.iteritems():
                    self[k] = v

    def clear(self):
        self._drop()

    def keys(self, filter_by = None, sort_by = None, unique = None, **kwargs):
        return list(self.iterkeys(filter_by, sort_by, unique, **kwargs))

    def values(self, filter_by = None, sort_by = None, unique = None, **kwargs):
        return list(self.itervalues(filter_by, sort_by, unique, **kwargs))

    def items(self, filter_by = None, sort_by = None, unique = None, **kwargs):
        return list(self.iteritems(filter_by, sort_by, unique, **kwargs))


    def itervalues(self, filter_by = None, sort_by = None, unique = None, **kwargs):
        for k, v in self._filter(filter_by, sort_by, unique, **kwargs):
            yield v

    def iteritems(self, filter_by = None, sort_by = None, unique = None, as_str = False, **kwargs):
        for k, v in self._filter(filter_by, sort_by, unique, **kwargs):
            yield self._denormalize(k, as_str = as_str), v

    def __iter__(self, filter_by = None, sort_by = None, unique = None, **kwargs):
        for k, v  in self._filter(filter_by, sort_by, unique, **kwargs):
            yield self._denormalize(k)

    def iterkeys(self, filter_by = None, sort_by = None, unique = None, **kwargs):
        return self.__iter__(filter_by, sort_by, unique, **kwargs)

    def get(self, key, default = None, value_type = None):
        try:
            nkey = self._normalize(key)
        except Exception as e:
            hp.print_error()
            logger.warning("Error while normalizing key, assuming not available...")
            return default
        try:
            return self._select(nkey, value_type = value_type, log_error = False)
        except PersistKeyError:
            return default

    def has_key(self, key, value_type = None):
        return self.__contains__(key, value_type = value_type)

    def to_dict(self, as_str = False):
        out = {}
        for k, v in self.iteritems(as_str = as_str):
            out[k] = str(v) if as_str and hasattr(v, '__call__') else v
        return out

    def copy(self):
        return self.to_dict()

    def __repr__(self):
        return "Object {} with name {} (key_names = {})".format(self.__class__.__name__, self.name, self.key_names)

    def __str__(self):
        return json.dumps(self.to_dict(as_str = True), indent = 4)

    @contextmanager
    def _transaction(self, cur = None):
        """
        Performs a commit if there is no error
        """
        try:
            self._in_transaction.append(True)
            if not cur:
                cur = self._connect(cursor = True)
            if len(self._in_transaction) <= 1:
                logger.debug("Started transaction at {}".format(time.time()))
            else:
                logger.debug("Already in transaction {}".format(self._in_transaction))
            yield cur
            if len(self._in_transaction) <= 1:
                logger.debug("Committing transaction at {}".format(time.time()))
                self._commit()
                self._close()
                logger.debug("Transaction successful at {}".format(time.time()))
            else:
                logger.debug("In transaction {}".format(self._in_transaction))
        except Exception as e:
            hp.print_error(e)
            if len(self._in_transaction) <= 1:
                logger.error("Transaction failed at {}".format(time.time()))
            self._close(cur)
            raise
        finally:
            self._in_transaction.pop()
            if not len(self._in_transaction):
                self._close(cur)
            logger.debug("Transaction completed level at {}".format(self._in_transaction))

    def iadd(self, key, value):
        """
        for adding a value to an existing key
        """
        self[key] = self.get(key, 0) + value
        return self[key]
    
    def imax(self, key, value):
        """
        For setting to value is value is greater than current value
        """
        self[key] = max(self[key], value)
        return self[key]

    def imin(self, key, value):
        """
        For setting to value is value is less than current value
        """
        self[key] = min(self[key], value)
        return self[key]


    ############# SubClass Defined #########

    def _get_key_def(self):
        """
        Returns the previously defined key names and key types
        """
        raise NotImplementedError('_get_key_def')

    def _get_last_updated(self, key):
        """
        Sets the type of primary key.
        It is called after we know the type of primary key
        """
        raise NotImplementedError("_get_last_updated")
    
    
    def _update_master(self):
        """
        updates master table about change in config
        """
        raise NotImplementedError("_update_master")


    def _exists(self, name):
        """
        checks for table with specified name
        """
        raise NotImplementedError("_exists")

    def _create(self, name):
        """
        returns connection object
        is idempotent. 
        """
        raise NotImplementedError("_create")

    def _drop(self):
        """
        drops table of specified name
        """
        raise NotImplementedError('_drop')

    def _insert(self, key, value, value_type = str):
        """
        inserts record in table with key and value
        returns tuple of key, value
        """
        raise NotImplementedError("_insert")

    def _update(self,  key, value, value_type = str):
        """
        updates record in table with key and value
        returns tuple of key, value
        """
        raise NotImplementedError("_update")

    def _upsert(self,  key, value, value_type = str):
        """
        updates record in table with key and value
        returns tuple of key, value
        """
        raise NotImplementedError("_upsert")

    def _delete(self,  key):
        """
        deletes record in table with key and value
        returns true or false
        """
        raise NotImplementedError("_delete")

    def _select(self, key, log_error = True):
        """
        returns record by key
        """
        raise NotImplementedError("_select")

    def _count(self):
        """
        returns the count of number of object
        """
        raise NotImplementedError("_count")


    def _filter(self, filter_by = None, sort_by = None, unique = None):
        """
        Returns an iterator for a list of records by filter
        filter_by is a list of list of tuples with the following format

        e.g. Simple rule
        ('value', '>', 3) # value > 3
        ('value', '<>', None) # value != None
        ('value', 'LIKE', 'xyz_') # value like xyz_
        ('value, 'IN', ('abc', 'def', 'geh')) # value IN

        e.g. OR / AND / NOT
        {
            'OR': [('key', '=', 'xyz'), ('value', '=', 'xyz')], # value == xyz
        }

        e.g. Nested
        {
            'AND': [
                ('key', '=', 'xyz'), 
                {'OR': [
                    ('value', '=', 'xyz'),
                    ('value', 'LIKE', 'xyz%'),
                ]
            ]
        }

        sort_by is either a string or a tuple
        e.g. 
            'value'
            'key'
            ('value', True)  for reverse
        """
        raise NotImplementedError("_filter")

    def _commit(self):
        """
        Commits to the database
        """
        raise NotImplementedError("_commit")

    def _connect(self, *args, **kwargs):
        """
        Connects to the database
        sets and returns self._conn
        """
        raise NotImplementedError("_connect")

    def _close(self):
        """
        Closes the connection to the database
        sets and returns None
        """
        raise NotImplementedError("_close")




class pcollection(object):

    _dict_class = pdict

    _saved_attributes = ()


    def __init__(self, name, key_names = None, key_types = None, 
                 get_hook = None, 
                 set_hook = None, 
                 uniques = None,
                 unique_types = None,
                 **kwargs
        ):
        """
        uniques specify which attribute names are unique. It is a list of attributes. It cannot be a part of the keys
        """
        self.name = name
        self._attribute_dict = self._dict_class(self.name + '_attribute_dict_', **kwargs)

        # Set optional parameters or get from previously stored
        self.key_names = hp.make_list(key_names) or self._attribute_dict.get('key_names', [])
        if not key_names:
            raise PersistCollectionError("At least one key name is required for model {}".format(self.name))
        if key_types is None:
            key_types = [unicode]*len(self.key_names)
        self.key_types = hp.make_list(key_types or self._attribute_dict.name2types(self._attribute_dict.get('key_types', []), list))
        if len(self.key_types) != len(self.key_names):
            raise PersistCollectionError("Key Names is len {}, Key Types is len {}".format(len(self.key_names), len(self.key_types)))
        self.get_hook = get_hook or self._attribute_dict.get('get_hook', None)
        self.set_hook = set_hook or self._attribute_dict.get('set_hook', None)


        # Store optional parameters for restarts
        uniques = list(self._make_unique_constraints(uniques or [], unique_types or []))
        for k, v in [
            ('key_names', self.key_names), 
            ('key_types', self._attribute_dict.type2names(self.key_types, iterob = list)), 
            ('get_hook', encode(self.get_hook) if self.get_hook else None ),
            ('set_hook', encode(self.set_hook) if self.set_hook else None )
            ]:
            kn = self._attribute_dict.get(k)
            if (kn != v):
                self._attribute_dict[k] = v

        logger.debug("key types: %s", self.key_types)
        self._dict = self._dict_class(self.name, self.key_types + [unicode], self.key_names + ['__attributes'], uniques, **kwargs)
        self._len = self._count()
        self._updated = self._dict._updated
        self.delete = partial(self.pop, _default = PersistKeyError)

    def __hasattr__(self, attr):
        if attr in self._attribute_dict:
            return True
        return super(pcollection, self).__hasattr__(attr)

    def __getattribute__(self, attr):
        if attr == '_saved_attributes' or attr not in self._saved_attributes:
            return super(pcollection, self).__getattribute__(attr)
        if attr == 'key_types':
            return self._attribute_dict.__getitem__(key_types)

    def __setattr__(self, attr, value):
        if attr == '_attribute_dict':
            super(pcollection, self).__setattr__(attr, value)
        if attr in self._saved_attributes:
            self._attribute_dict[attr] = value
            return
        return super(pcollection, self).__setattr__(attr, value)

    def clear(self):
        self._attribute_dict.clear()
        self._dict.clear()

    def _get_key_list(self, instance):
        for k in self.key_names:
            try:
                yield instance[k]
            except KeyError:
                raise PersistCollectionKeyError("Key {} not found in instance. Cannot set.".format(k))

    def _check_key_list(self, keys):
        keys = hp.make_list(keys)
        if not len(keys) == len(self.key_names):
            raise PersistCollectionKeyError("Key length {} does not match with settings: {}".format(len(keys), len(self.key_names)))
        return keys

    def _keys_to_list(self, keys):
        if isinstance(keys, dict):
            nkeys = list(self._get_key_list(keys))
        else:

            nkeys = self._check_key_list(keys)
        return nkeys

    def _recgen(self, keys, get_inst = True, instance = None, only_one = False, log_error = True):
        """
        Internal generator for iterating over the keys and values
        If get_inst is true, it yields all the key, value pairs
        if False, then it yields only the _dict keys and the values
        keys can be dictionary of {key_name: key} or 
            a list of keys in the same order as initialized or 
            just the key itself if there is only one key
        """
        nkeys = self._keys_to_list(keys)
        logger.debug("In _recgen, nkeys: %s", nkeys)
        filter_by = {'AND': []}
        for n, k in zip(self.key_names, nkeys):
            if isinstance(instance, dict):
                instance[n] = k
            if get_inst:
                yield n, k
            filter_by['AND'].append((n, '=', k))
        logger.debug("In recgen, filter_by: %s, instance: %s", filter_by, instance)
        rec = 0
        for k, v in self._dict._filter(
                filter_by = filter_by 
            ):
            k = self._dict._denormalize(k, as_str = False)
            rec += 1
            if only_one:
                break
            if isinstance(instance, dict):
                instance[k[-1]] = v
            if get_inst:
                yield k[-1], v
            else:
                yield k, v
        if rec == 0:
            raise PersistKeyError(u"Key {} not found for pgpcollection {}".format(keys, self.name), log = log_error)

    def _set(self, _get_hook = None, _set_hook = None, **instance):
        set_hook = _set_hook or self.set_hook
        if set_hook: instance = set_hook(instance)
        keys = list(self._get_key_list(instance))
        return self.__setitem__(keys, instance, _get_hook = _get_hook, check = False)

    def set(self, _get_hook = None, _set_hook = None, **instance):
        return self._set(_get_hook, _set_hook, **instance)

    def _update(self, _get_hook = None, _set_hook = None, **instance):
        """
        Used to update only part of the instance.
        i.e. only the fields present in instance will be set, the others fields will be untouched
        """
        set_hook = _set_hook or self.set_hook
        if set_hook: instance = set_hook(instance)
        keys = list(self._get_key_list(instance))
        return self.__setitem__(keys, instance, _get_hook = _get_hook, check = False, replace = False)

    def __setitem__(self, keys, value, replace = True, _get_hook = None, _set_hook = None, check = True, **kwargs):
        get_hook = _get_hook or self.get_hook
        set_hook = _set_hook or self.set_hook
        if check: 
            keys = self._check_key_list(keys)
            if set_hook: value = set_hook(value)
        ret_instance = dict((n, k) for n, k in zip(self.key_names, keys))
        logger.debug("Value: %s", value)
        with self._dict._transaction():
            old = self._get(keys, _default = {})
            for k, v in value.iteritems():
                logger.debug("Now setting: %s, %s", k, v)
                ret_instance[k] = v
                old.pop(k, None)
                if k in self.key_names:
                    continue
                nkey = keys + [str(k)]
                self._dict[nkey] = v
            if replace:
                for k, v in old.iteritems():
                    nkey = keys + [str(k)]
                    self._dict.pop(nkey)
        logger.debug("Ret Instance: %s", ret_instance)
        if get_hook: ret_instance = get_hook(ret_instance)
        return ret_instance

    def _get(self, _keys = None, _default = None, _get_hook = None, **kwargs):
        keys = _keys or kwargs
        get_hook = _get_hook or self.get_hook
        try:
            ret = dict(self._recgen(keys, get_inst = True, log_error = False))
            if get_hook:
                return get_hook(ret)
            return ret
        except PersistKeyError:
            return _default
    
    def get(self, _keys = None, _default = None, _get_hook = None, **kwargs):
        return self._get(_keys, _default, _get_hook, **kwargs)

    def __getitem__(self, keys, _get_hook = None, check = True):
        get_hook = _get_hook or self.get_hook
        ret = dict(self._recgen(keys, get_inst = True))
        if get_hook:
            return get_hook(ret)
        return ret

    def __delitem__(self, _keys, _instance = None, _get_hook = None, log_error = True, **kwargs):
        get_hook = _get_hook or self.get_hook
        if _instance is None: _instance = {}
        with self._dict._transaction():
            for k, v in self._recgen(_keys, get_inst = False, instance = _instance, log_error = log_error):
                self._dict.pop(k)
        try:
            if get_hook:
                return get_hook(_instance)
            else:
                return _instance
        except:
            return _instance

    def __len__(self):
        return self._count()

    def pop(self, _keys = None, _default = PersistKeyError, _get_hook = None, **kwargs):
        return self._pop(_keys, _default, _get_hook, **kwargs)

    def _pop(self, _keys = None, _default = PersistKeyError, _instance = None, _get_hook = None, **kwargs):
        _keys = _keys or kwargs
        try:
            return self.__delitem__(_keys, _instance,  _get_hook = _get_hook, log_error = False)
        except PersistKeyError as e:
            if isinstance(_default, Exception):
                raise _default
            elif isinstance(_default, type) and issubclass(_default, Exception):
                raise _default(e)
            return _default

    def _unique(self, attribute):
        unique = {}
        for v in self.itervalues():
            r =  v.get(attribute)
            if r is not None and r not in unique:
                yield r
            if r is not None:
                unique[r] = None

    def __contains__(self, keys):
        try:
            dict(self._recgen(keys, get_inst = False, only_one = True))
        except PersistKeyError:
            return False
        return True

    def has_key(self, key):
        return self.__contains__(key)
    
    
    def list(self, filter_by = None, sort_by = None, limit = 100, start = 0):
        return list(
                self.filter(filter_by = filter_by, sort_by = sort_by, _get_filter = lambda x: self.get(**x), limit = limit, start = start)
        )

    def all(self, filter_by = None, sort_by = None, limit = 100, start = 0):
        return {self.plural: self.list(filter_by = filter_by, sort_by = sort_by, limit = limit, start = start)}


    def __repr__(self):
        return "collection {} with name {} (key_names = {})".format(self.__class__.__name__, self.name, self.key_names)

    def __str__(self):
        return json.dumps(self.list(), indent = 4)
    
    def __iter__(self, filter_by = None):
        """
        Note: You need to write your own __iter__ for subclass this one is very inefficient
        """
        for keys, v in self._dict._filter(
                filter_by = filter_by, unique = self.key_names, 
            ):
            keys = self._dict._denormalize(keys)
            logger.info("In __iter__, keys %s: %s", self.key_names, keys[:-1])
            k = keys[:-1]
            if len(k) == 1:
                yield k[0]
            else:
                yield k

    def iterkeys(self, filter_by = None):
        return self.__iter__(filter_by = filter_by)

    def iteritems(self, filter_by = None):
        for keys in self.iterkeys(filter_by = filter_by):
            yield keys, self.__getitem__(keys)

    def itervalues(self, filter_by = None):
        for keys in self.iterkeys(filter_by):
            yield self.__getitem__(keys)
    
    def keys(self, filter_by = None):
        return list(self.iterkeys(filter_by = filter_by))

    def items(self, filter_by = None):
        return list(self.iteritems(filter_by = filter_by))

    def values(self, filter_by = None):
        return list(self.itervalues(filter_by = filter_by))
    
    def filter(self, filter_by = None, sort_by = None, _get_filter = None, limit = None, start = 0):
        """
        Returns an iterator for a list of records by filter
        filter_by is a list of list of tuples with the following format

        e.g. Simple rule
        (<attribute>, '>', 3) # value > 3
        (<attribute>, '<>', None) # value != None
        (<attribute>, 'LIKE', 'xyz%') # value like xyz%
        (<attribute>, 'IN', ('abc', 'def', 'geh')) # value IN

        e.g. OR / AND / NOT
        {
            'OR': [(<attribute>, '=', 'xyz'), (<attribute>, '=', 'xyz')], # value == xyz
        }

        e.g. Nested
        {
            'AND': [
                (<attribute>, '=', 'xyz'), 
                {'OR': [
                    (<attribute>, '=', 'xyz'),
                    (<attribute>, 'LIKE', 'xyz%'),
                ]
            ]
        }
        """
        if _get_filter is None: _get_filter = lambda x: x
        # Main function
        logger.debug("In filter, filter_by: %s", filter_by)
        i = 0
        sent = 0
        for instance in sorted(self.itervalues(filter_by), key = operator.itemgetter(sort_by) if sort_by else None):
            if i < start:
                i+=1
                continue
            f = _get_filter(instance)
            yield f
            sent += 1
            if limit is not None and sent >= limit:
                raise StopIteration

    def _make_unique_constraints(self, uniques, *args, **kwargs):
        for u in uniques:
            if u in self.key_names:
                raise PersistCollectionError("Cannot add further unique constraint on key(id) type {}".format(u))
            if isinstance(u, (str, unicode)):
                cols = u
                unique = True
                clauses = ()
                yield (cols, unique, clauses)
            if isinstance(u, (list, tuple)):
                cols = tuple(u)
                if any(u1 in self.key_names for u1 in u):
                    raise PersistCollectionError("Cannot add further unique constraint on key(id) type {}".format(u))
                unique = True
                clauses = ()
                yield (cols, unique, clauses)

    
    def _count(self):
        """
        return the number of objects in collections
        """
        raise NotImplementedError("_count")

class parray(object):
    #TODO: Redo iadd and imul for this should be more efficient
    _dict_class = pdict
  
    def wrap(self, attr, *args, **kwargs):
        with self._dict._transaction():
            # TODO: Have to Have to optimize this
            self._list = self._dict[self.name]
            logger.debug("List now is %s", self._list)
            r = getattr(self._list, attr)(*args, **kwargs)
            self._dict[self.name] = self._list
            logger.debug("Updated list now is %s", self._dict[self.name])
            return r

    def padecorator(self, func):
        def func_wrapper(self, *args, **kwargs):
            return self.wrap(func.__name__, *args, **kwargs)
        func_wrapper.__doc__ = func.__doc__
        func_wrapper.__name__ = func.__name__
        return func_wrapper

    def __init__(self, name, iterable = None):
        self.name = hp.normalize_join(name)
        self._list = []
        self._dict = self._dict_class('__all_lists')
        if iterable is not None:
            self._dict[self.name] = iterable
        if self.name not in self._dict:
            self._dict[self.name] = self._list
        self._list = self._dict[self.name]
        logger.debug("Array type is %s", type(self._list))
        for attr, func in list.__dict__.iteritems():
            logger.debug("Looking into attr %s", attr)
            if attr not in ['__init__', 'clear'] and callable(func):
                setattr(self, attr, types.MethodType(self.padecorator(func), self))
                logger.debug("Setting attr %s to %s", attr, getattr(self, attr))

    def __iter__(self):
        return self.wrap('__iter__')

    def __len__(self):
        return self.wrap('__len__')

    def __contains__(self, *args, **kwargs):
        return self.wrap('__contains__', *args, **kwargs)

    def __delitem__(self, *args, **kwargs):
        return self.wrap('__delitem__', *args, **kwargs)

    def __delslice__(self, *args, **kwargs):
        return self.wrap('__delslice__', *args, **kwargs)

    def __getitem__(self, *args, **kwargs):
         return self.wrap('__getitem__', *args, **kwargs)
     
    def __getslice__(self, *args, **kwargs):
         return self.wrap('__getslice__', *args, **kwargs)
    
    def __setitem__(self, *args, **kwargs):
         return self.wrap('__setitem__', *args, **kwargs)
     
    def __setslice__(self, *args, **kwargs):
         return self.wrap('__setslice__', *args, **kwargs)
     
    def __ge__(self, *args, **kwargs):
        return self.wrap('__ge__', *args, **kwargs)
   
    def __eq__(self, *args, **kwargs):
         return self.wrap('__eq__', *args, **kwargs)
     
    def __gt__(self, *args, **kwargs):
        return self.wrap('__gt__', *args, **kwargs)
    
    def __le__(self, *args, **kwargs):
        return self.wrap('__le__', *args, **kwargs)
    
    def __lt__(self, *args, **kwargs):
        return self.wrap('__lt__', *args, **kwargs)
    
    def __ne__(self, *args, **kwargs):
        return self.wrap('__ne__', *args, **kwargs)
    
    def __repr__(self, *args, **kwargs):
        return self.wrap('__repr__', *args, **kwargs)
    
    def __str__(self, *args, **kwargs):
        return self.wrap('__str__', *args, **kwargs)
    
    def __add__(self, *args, **kwargs):
        return self.wrap('__add__', *args, **kwargs)

    def __mul__(self, *args, **kwargs):
        return self.wrap('__mul__', *args, **kwargs)
    
    def __iadd__(self, *args, **kwargs):
        return self.wrap('__iadd__', *args, **kwargs)
    
    def __imul__(self, *args, **kwargs):
        return self.wrap('__imul__', *args, **kwargs)
    
    def __reversed__(self, *args, **kwargs):
        return self.wrap('__reversed__', *args, **kwargs)
    
    def __sizeof__(self, *args, **kwargs):
        return self.wrap('__sizeof__', *args, **kwargs)
   
    def copy(self):
        self._list = self._dict[self.name]
        return copy(self._list)

    def clear(self):
        self._dict.pop(self.name)

