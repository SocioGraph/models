import os, sys, re
import decimal
import helpers as hp
import pgpersist as db
from pgpersist import pgpdict, pgparray, numberlist, stringlist, datetime, dtime, NoneType, date, get_meta_classes, geolocation, timedelta
from copy import deepcopy as copyof, copy
import numpy as np
from persist import PersistKeyError
from operator import itemgetter
import time
from pgp_db import JsonBDict

DEFAULT_SHELF_LIFE = 24*3600

DEBUG_LEVEL = hp.logging.INFO
logger = hp.get_logger(__name__, DEBUG_LEVEL)

class Cacher(object):

    def __init__(self, enterprise_id, default_shelf_life = None):
        if isinstance(default_shelf_life, (int, float, long, str, unicode)):
            default_shelf_life = int(default_shelf_life)
        else:
            default_shelf_life = DEFAULT_SHELF_LIFE
        self.enterprise_id = enterprise_id
        self._db = JsonBDict(
                '___cacher',
                [unicode],
                ['key'],
                DB_NAME = self.enterprise_id
        )
        self.default_shelf_life = default_shelf_life

    def set(self, ids, instance, refresh = True, shelf_life = None, as_key = True, params = None, api = ''):
        if isinstance(shelf_life, bool) or not isinstance(shelf_life, (int, float, long)):
            shelf_life = self.default_shelf_life
        logger.debug("In set, shelf life is %s, default_shelf_life: %s", shelf_life, self.default_shelf_life)
        if not as_key:
            id_ = self.make_id(ids, params, api)
        else:
            id_ = ids
        prev = self._db.get(id_, {})
        if refresh or prev.get('__timeout__', 0) < time.time():
            instance['__timeout__'] = time.time() + shelf_life
            self._db[id_] = instance
            logger.info("Setting instance for id_ %s, timeout:%s, time: %s", id_, instance['__timeout__'], time.time())
        instance.pop('__timeout__', None)
        return instance

    def make_id(self, ids, params = None, api = ''):
        if params is None: params = {}
        id_ = hp.make_list(ids) + ['{k}.{v}'.format(k = k, v = sorted(v.iteritems()) if isinstance(v, dict) else v) for k, v in sorted(params.iteritems()) if v] + [api]
        id_ = hp.normalize_join(*id_)
        i_ = hp.make_uuid3(id_)
        logger.info("Making id for %s: %s", id_, i_) 
        return i_

    def get(self, ids, default = None, allow_stale = False, as_key = True, params = None, api = ''):
        if not as_key:
            id_ = self.make_id(ids, params, api)
        else:
            id_ = ids
        logger.info("Getting instance for id_ %s", id_)
        d = self._db.get(id_)
        if not isinstance(d, dict):
            if isinstance(default, Exception):
                raise default
            elif isinstance(default, type) and issubclass(default, Exception):
                raise default("No ids {} found in cache".format(ids))
            return default
        t = d.pop('__timeout__')
        nt = time.time()
        if allow_stale:
            return d
        elif t > nt:
            return d
        elif isinstance(default, Exception):
            raise default
        elif isinstance(default, type) and issubclass(default, Exception):
            raise default("No ids {} found in cache".format(ids))
        logger.info("Instance present in cacher, but stale.. timeout: %s, now: %s", t, nt)
        return default
    
    def store(self, ids, params = None, api = '', instance = None, shelf_life = None):
        id_ = self.make_id(ids, params, api)
        return self.set(id_, instance, shelf_life = shelf_life)

    def check(self, ids, params = None, api = ''):
        id_ = self.make_id(ids, params, api)
        return self.get(id_)

    def pop(self, ids, default = None, as_key = True, params = None, api = '', **kwargs):
        if not as_key:
            id_ = self.make_id(ids, params, api)
        else:
            id_ = ids
        return self._db.pop(id_, default)

    def cleanup(self, now_t = None):
        t = now_t or time.time()
        for id_, instance in self._db._filter(filter_by = {'__timeout__': (None, t)}):
            logger.info("Clearing cache for id: %s", id_)
            try:
                self._db.pop(id_)
            except:
                pass

