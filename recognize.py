import os, sys
from functools import wraps
import helpers as hp
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, getmtime as modified_time
from collections import OrderedDict
from stuf import stuf
import validators as val
from pgp_db import JsonBDict
from pgpersist import pgpdict, pgparray, numberlist, stringlist, datetime, dtime, NoneType, date, get_meta_classes, geolocation, timedelta
from celery import Celery
from scipy.signal import lfilter, hamming
import numpy as np
import math
import cv2
import socket

from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types as gtypes

logger = hp.get_logger(__file__)


# Globals
UDP_IP = "127.0.0.1"
UDP_PORT = int(os.environ.get('UDP_PORT', 5101))
WINDOW_TIME = 0.03

#sock = socket.socket(socket.AF_INET, # Internet
#                     socket.SOCK_DGRAM) # UDP
#sock.bind((UDP_IP, UDP_PORT))

google_client = speech.SpeechClient()

class RecognizerError(hp.I2CEError):
    pass

LANGUAGE_DICT = { 
    'english': 'en-IN',
    'hindi': 'hi-IN',
    'kannada': 'kn-IN',
    'english-us': 'en-US',
    'english-uk': 'en-GB',
    'arabic': 'ar-XA',
    'bahasa-ind': 'id-ID',
    'bahasa-mal': 'ms-MY'
}

class Recognizer(object):

    def __init__(self, sample_rate_hertz = 16000, language = 'english', **kwargs):
        self.sample_rate_hertz = int(sample_rate_hertz)
        if language not in LANGUAGE_DICT:
            raise RecognizerError("Language {} not supported".format(language))
        self.language_code = LANGUAGE_DICT.get(language) or 'en-IN'
        self.config = gtypes.RecognitionConfig(
            encoding = enums.RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz = self.sample_rate_hertz,
            language_code = self.language_code,
            speech_contexts=[
                speech.types.SpeechContext(
                    phrases = kwargs.get("phrases") or [],
                )
            ]
        )
        self.silence_total = 0
        self.silence_totals = 0
        self.silence_avg = 0
        self.silence_std = 0.1
        self.num_silence = 0
        self.any_speech = False
        self.window_time = kwargs.get('window_time') or WINDOW_TIME
        self.window_length = self.window_time*float(self.sample_rate_hertz)
        self.client = kwargs.get('client') or google_client
    
    def is_silence(self, window):
        e = self.energy(window)
        if e < (self.silence_avg + self.silence_std):
            self.silence_total += e
            self.silence_totals += e**2
            self.num_silence += 1
            self.silence_avg = self.silence_total/float(self.num_silence)
            if num_silence < 30:
                f = 1
            else:
                f = self.num_silence/(self.num_silence - 1)
            self.silence_std = np.sqrt(
                ((self.silence_totals/self.num_silence) - (self.silence_avg**2)) * f
            )
            return True
        self.num_silence = 0
        self.silence_total = 0
        self.silence_totals = 0
        return False

    def energy(self, window):
        return sum((w**2 for w in window))

    def is_ended(self, window):
        if self.is_silence(window) and self.num_silence*self.window_time >= self.silence_length:
            return True
        return False

    def recognize(self, audio):
        audio = gtypes.RecognitionAudio(content=audio)
        response = self.client.recognize(self.config, audio)
        try:
            r = response.results[0].alternatives[0].transcript
            logger.info("Recognized text is: %s", r)
            return r
        except (IndexError, AttributeError) as e:
            logger.error(e)
            raise RecognizerError("Could not perform reconition, due to lack of speech")


#while True:
#    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
#    sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
#    print "received message:", data

if __name__ == '__main__':
    r = Recognizer(sample_rate_hertz = 24000)
    with open(sys.argv[1], 'rb') as fp:
        print(r.recognize(fp.read()))


