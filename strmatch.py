import numpy as np
import model
import helpers as hp
import re, requests
from collections import OrderedDict
from pgnlp import JsonBDict

np.set_printoptions(suppress=True)

class LevenshteinStringMatch(dict):
    
    def __init__(self, input_dict, max_penalty=3, max_penalty_ratio=0.3, default_penalty=1, deletion_weights=None, substitution_weights=None, doubling_penalty=0.5, interchange_penalty=0.5, language="multi"):
        self.deletion_weights = deletion_weights or {'i': 0.8, 'e': 0.7, 'o':0.9, 'a': 0.9, 'u': 0.9}
        self.substitution_weights = substitution_weights or {('a', 's'): 0.8, ('i', 'e'): 0.8}
        
        self.input_dict = input_dict

        self.max_penalty = max_penalty
        self.max_penalty_ratio = max_penalty_ratio
        self.default_penalty = default_penalty
        self.doubling_penalty = doubling_penalty
        self.interchange_penalty = interchange_penalty

        self._general_dict = model.Model("dictionary", "core").objects._db
        self.language = language


    def find_matching_score(self, w1, w2, max_penalty=3):
        x = len(w1)
        y = len(w2)

        mtx = np.empty([x+1, y+1])
        mtx.fill(1000)
        mtx[:, 0] = range(x+1)
        mtx[0, :] = range(y+1)

        if abs(x-y) > max_penalty:
            return abs(x-y)

        tp = 0
        for i in range(x):
            mcd = False
            sc = 0

            sj = int(round(max(0, i-max_penalty)))
            ej = min(y, int(round(i+max_penalty)))
            for j in range(sj, ej):
                if w1[i] == w2[j]:
                    mcd = True
                    mtx[i+1, j+1] = mtx[i, j]
                    continue

                sc = 1
                insertion = mtx[i+1, j] 
                deletion = mtx[i, j+1] 
                replacement =  mtx[i, j]
    
                #min_op = min(mtx[i+1, j], mtx[i, j], mtx[i, j+1])
                min_op = min(insertion, replacement, deletion)

                if replacement == min_op:
                    for ls, pt in self.substitution_weights.items():
                        if (ls[0] == w1[i] and ls[1] == w2[j]) or (ls[1] == w1[i] and ls[0] == w2[j]):
                            sc = pt
                            break
                elif deletion == min_op:
                    sc = self.deletion_weights.get(w1[i], 1)
                    if i >= 1 and j < len(w2) - 1 and w1[i] == w2[j+1]:
                        sc = self.doubling_penalty

                elif insertion == min_op:
                    sc = self.default_penalty
                    if j >= 1 and i< len(w1) - 1 and w1[i+1] == w2[j]:
                        sc = self.doubling_penalty

                mtx[i+1, j+1] = min_op + sc 
                if i > 1 and j > 1 and w1[i] == w2[j-1] and w1[i-1] == w2[j] and w1[i-1] != w2[j-1] and w1[i] != w2[j]:
                    sc = self.interchange_penalty
                    mtx[i+1, j+1] = min(mtx[i+1, j+1], mtx[i-1, j-1] + sc)
                    mcd = True

                
                

            if min(mtx[i+1, : ]) > max_penalty:
                return min(mtx[i+1, : ])+1

            if not mcd:
                tp += 1
        return mtx[-1][-1]



    def levenshtein_distance(self, w1, w2, return_matchs=False, max_penalty=None):
        def _return(ratio=None, words=None, penalty=0, matched=False, **kwargs):
            if return_matchs:
                mw = words or [w1] if matched else []
                kwargs.update({
                    "matched_words": mw,
                    "ratio": ratio or round((len("".join(mw))*1.)/len(w1), 2),
                    "penalty": penalty,
                    "matched": matched
                })
                return kwargs
            return penalty
    
        max_penalty = max_penalty or self.max_penalty
        if w1 == w2:
            return _return(penalty=0, matched=True)
        md = np.ceil(len(w1) * self.max_penalty_ratio)
        md = min(md, max_penalty)

        sc = self.find_matching_score(w1, w2, max_penalty=md)
        if sc > md:
            return _return(penalty=max_penalty+1)

        return _return(matched=True, penalty=sc) if sc <= self.max_penalty else _return(penalty=sc) 

        """
        mbs = []

        tp = 0
        for x in range(len(mb)):
            mbs.append(w2[mb[x][1]:mb[x][1]+mb[x][2]])

            if len(mb) -1 == x:
                b1 = w1[mb[x][0]+mb[x][2]:]
                b2 = w2[mb[x][1]+mb[x][2]:]
            else:
                b1 = w1[mb[x][0]+mb[x][2]: mb[x+1][0]]
                b2 = w2[mb[x][1]+mb[x][2]: mb[x+1][1]]

            
            if b1 and b2:
                if b1 == b2[::-1]:
                    tp += self.interchange_penalty
                    continue
                p = 0
                for ls, pt in self.substitution_weights.items():
                    if ls[0] == b2 and ls[1] == b1:
                        p = self.deletion_weights[b2]
                        break
                tp += (p or self.default_penalty)

            elif b2 or b1:
                if len(b2 or b1) == 1:
                    if mbs and mbs[-1].endswith(b2 or b1):
                        tp += self.doubling_penalty
                        continue
                for l in (b2 or b1):
                    if l in self.deletion_weights:
                        tp += self.deletion_weights[l]
                    else:
                        tp += self.default_penalty
            if tp > max_penalty and not return_matchs:
                return _return(penalty = tp)

        return _return(words=mbs, matched=True, penalty=tp) if tp <= self.max_penalty else _return(penalty=tp) 
        """

    def similar_words(self, w, no_words=1, max_penalty=None):
        ld = OrderedDict()
        mp = max_penalty or self.max_penalty

        if w in self.input_dict or w in self._general_dict:
            ld[w] = 1.
            return ld

        n = len(w)

        def _filter(dct, no_words, mp=mp):
            if no_words == 0:
                return 0
            mn = 0 if n-mp < 0 else n-mp
            flw = dct.iteritems({"length" : (mn, n+mp), "entity": False, "lword": list(w)}, filter_by_types = {'length': int, 'lword': list}, sort_by=("frequency", True), limit=100000)
            for wd, i in flw:
                x = self.levenshtein_distance(w, wd, max_penalty=mp)
                if x <= mp:
                    ld[wd] =  (1 - x/(len(w)*1.)) #self.input_dict[wd]
                    no_words -= 1
                    if not no_words:
                        break
                #mp = x if x else mp
            return no_words

        no_words = _filter(self.input_dict, no_words)
        #no_words = _filter(self._general_dict, no_words)
        return ld

    def write_to_validation_queue(self, w, similar_words):
        similar_words = hp.make_list(similar_words)
        with open("dictionary_verification_queue.text", "a") as fp:
            fp.write("${}".format(w))
            fp.write("\n".join(similar_words))

    def add_rm_words(self, words, doc=None, remove=False, validate=False, language="multi"):
        words = hp.make_list(words)
        docs = hp.make_list(doc)
        
        if remove:
            for i in words:
                if i in self._general_dict:
                    self._general_dict.pop(i)
            return


        if validate:
            for w in words:
                ld = self.similar_words(w, no_words=2, max_penalty=2)
                if ld.keys():
                    self.write_to_validation_queue(w, list( "{}\t{}".format(i, ld[i]) for i in ld))
            return

        for word in words:
            dc = self._general_dict.get(word) or {}
            dc["docs"] = dc.get("docs") or []
            if remove:
                for i in docs:
                    if i in dc["docs"]:
                        dc["docs"].remove(i)
            else:
                dc["docs"] = list(set(dc.get("docs", []) + docs))
            dc["frequency"] = len(dc["docs"])
            dc["language"] = language
            dc["length"] = len(word)
            dc["lword"] = list(word)
            dc["sword"] = " ".join(list(word))

            self._general_dict[word] = dc

    def load_file(self, url):
        def _train(string):
            sents = string.split(".")
            for i in sents:
                words = re.findall("\w+", i)
                self.add_rm_words(words, " ".join(words)) 

        try:
            if url.startswith("http"):
                res = requests.get(url)
                if res.status_code < 400:
                    _train(res.text)
                else:
                    raise res.text
            else:
                with open(url, "r") as fp:
                    _train(fp)
        except:
            raise

if __name__ == "__main__":
    dt = JsonBDict('{}_test'.format("test"), key_types = [unicode], key_names = ['doc'], DB_NAME = "test")
    l = LevenshteinStringMatch(dt, default_penalty=1)

    print "near", "fnekjra"
    print l.find_matching_score("fnekjra", "near")

    print "dinesh", "dineshh", l.find_matching_score("dinesh", "dineshh")
    print "dinesh", "resh", l.find_matching_score("dinesh", "resh")
    print "dinesh", "dresh", l.find_matching_score("dinesh", "dresh")
    print "dinesh", "demesh", l.find_matching_score("dinesh", "demesh")
    
    print "dinesh", "demesh", l.levenshtein_distance("dinesh", "demesh", return_matchs=True)
    print "dinesh", "dinesh", l.levenshtein_distance("dinesh", "dinesh", return_matchs=True)
    print "dinesh", "denesh", l.levenshtein_distance("dinesh", "denesh", return_matchs=True)
    

    print "dinesh", "demesh", l.levenshtein_distance("dinesh", "demesh", return_matchs=True)
    print "dinesh", "denesh", l.levenshtein_distance("dinesh", "denesh", return_matchs=True)
    
    print "dinesh", "diinesh", l.levenshtein_distance("dinesh", "diinesh", return_matchs=True)

    print "dinesh", "dinseh", l.levenshtein_distance("dinesh", "dinseh", return_matchs=True)

    print "denesh", "dinesh", l.levenshtein_distance("denesh", "dinesh", return_matchs=True)
    print "deneshsdfdfsadafdin", "dineshsdffdin", l.levenshtein_distance("deneshsdfdfsadafdin", "dineshsdffdin", return_matchs=True)
    print "dineshsdffdin", "deneshsdfdfsadafdin", l.levenshtein_distance("dineshsdffdin", "deneshsdfdfsadafdin", return_matchs=True)
    print "dineshs", "deneshd", l.levenshtein_distance("dineshs", "deneshd", return_matchs=True)
    print "apple", "aple", l.levenshtein_distance("apple", "aple", return_matchs=True)
    print "aple", "apple", l.levenshtein_distance("aple", "apple", return_matchs=True)
    

    """
    ld = []
    lw = []
    import re
    with open("wikipedia.txt", "r") as fp:
        word = ""
        for i in fp:
            i = "".join(re.findall("[\$\w]+", i))
            if i.startswith("$"):
                word = i.replace("$", "")
                ld.append(word)
            else:
                lw.append(i)

    print "LD :: ", len(ld), "LW :: ", len(lw) 
    
    #for i in ld:
    #    dt[i] = {"frequency": 1, "docs": [], "length": len(i)}

    l = LevenshteinStringMatch(dt)
   
    import time

    c = 0
    ln = 0
    with open("wikipedia.txt", "r") as fp:
        start = time.time()
        word = ""
        for i in fp:
            i = "".join(re.findall("[\$\w]+", i))
            if i.startswith("$"):
                word = i.replace("$", "")
            else:
                ln+=1
                rs = l.similar_words(i, no_words=3)
                if word in rs:
                    c += 1
                if ln % 100 == 0:
                    print "Lapp ::", ln/100, "Mins :: ", int((time.time() - start)/60), "Secs :: ", int((time.time() - start)%60)
                    print "Accuricy ::",  round((c*1.)/ln, 2)


    print "Mins :: ",int((time.time() - start)/60), "Secs :: ", int((time.time() - start)%60)
    print "Accuricy :: ", c, (c*1.)/len(lw), c, len(ld), ln
   
    """

    


    """
    c = 0
    for i in ld:
        for j in lw:
            mb = l.levenshtein_distance(i,j, return_matchs=True)
            c += 1
            if c % 100:
                print "Lapp ::", c/100, "Mins :: ",int((time.time() - start)/60), "Secs :: ", int((time.time() - start)%60)

    print "Mins :: ",int((time.time() - start)/60), "Secs :: ", int((time.time() - start)%60)
    print len(ld), len(lw)
    """
    """
                if l == []:
                    word = i.replace("$", "")
                    continue

                print("Actual word :: ", word)
                for wd in l:
                    o =spell.candidates(wd) 
                    print("Likely :: ", o)
                    if word.lower() in o:
                        pmc += 1
                c += len(l)
                c2 += 1
                l = []
                print("Next word index :: ", c)
                word = i.replace("$", "")
                if c > 1000:
                    print("Total words ::", c, "Total corrections", c2, "Full Match :: ", fmc, "Partial Match ::", pmc)
                    break


    print "Mins :: ",int((time.time() - last_time)/60), "Secs :: ", int((time.time() - last_time)%60)

    """

