import os, sys
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile, getmtime as modified_time
d = joinpath(dirname(dirname(dirname(abspath(__file__)))), 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
import json_tricks as json
from functools import wraps
import helpers as hp


def identity(a):
    return a

def datetime_to_text(inp, *args, **kwargs):
    d = hp.to_datetime(inp, *args, **kwargs)
    for y in date_to_text(inp, *args, **kwargs):
        yield y
    yield hp.number_to_range(d.hour, None, [0, 3, '12am-4am'], [4, 7, '4am-8am'], [8, 11, '8am-12pm'], [12, 15, '12pm-4pm'], [16, 19, '4pm-8pm'], [20, 23, '8pm-12am'])
    yield hp.number_to_range(d.hour, None, [0, 2, '12am-3am'], [3, 5, '3am-6am'], [6, 8, '6am-9am'], [9, 11, '9am-12apm'], [12, 14, '11pm-3pm'], [15, 17, '3pm-6pm'], [18, 20, '6pm-9pm'], [21, 23, '9pm-12am'])
    yield d.strftime('%-I%p')

def date_to_text(inp, *args, **kwargs):
    d = hp.to_datetime(inp, *args, **kwargs)
    yield 'year-{}'.format(d.strftime('%Y'))
    yield d.strftime('%B')
    yield '{}'.format(d.strftime('%A'))
    yield 'week_of_year-{}'.format(d.strftime('%U'))
    yield d.strftime('%Y-%m-%d')


def to_yielder(func):
    @wraps(func)
    def yielder(*args, **kwargs):
        r = func(*args, **kwargs)
        yield r
    return yielder

def to_text(inp, att_type = unicode, *args, **kwargs):
    if inp is None:
        yield 'null'
        raise StopIteration
    if isinstance(inp, (float, int, long)):
        yield hp.number_categories(inp)
        raise StopIteration
    if isinstance(inp, hp.datetime):
        for k in datetime_to_text(inp, *args, **kwargs):
            yield k
        raise StopIteration
    if isinstance(inp, hp.date):
        for k in date_to_text(inp, *args, **kwargs):
            yield k
        raise StopIteration
    if isinstance(inp, list):
        for i in inp:
            yield to_text(i, unicode, *args, **kwargs)
        raise StopIteration
    if isinstance(inp, dict):
        for i, v in inp.iteritems():
            yield u'{}--{}'.format(i, to_text(v, unicode, *args, **kwargs))
        raise StopIteration
    if isinstance(inp, str):
        inp = unicode(inp)
    if issubclass(att_type, (float, int, long)):
        try:
            inp = float(inp)
            for i in to_text(inp, float, *args, **kwargs):
                yield i
            raise StopIteration
        except ValueError as e:
            yield unicode(inp)
            raise StopIteration
    elif issubclass(att_type, (hp.date, hp.datetime)):
        inp = hp.to_datetime(inp, *args, **kwargs)
        for i in to_text(inp, hp.datetime, *args, **kwargs):
            yield i
        raise StopIteration
    yield unicode(inp)

def to_value(inp, *args, **kwargs):
    if isinstance(inp, (float, int, long)):
        return float(inp)
    return 1

def to_initial(att_type):
    if issubclass(att_type, (float, int, long, unicode, str)):
        return 0
    if issubclass(att_type, list):
        return []
    if issubclass(att_type, dict):
        return {}
    return 0
    

def do(iterator, stats_dict, attributes, values, attribute_to_text = None, value_modifiers = None, types = None, initials = None, *args, **kwargs):
    values = values or ["_count"]
    attribute_to_text = attribute_to_text or {}
    value_modifiers = value_modifiers or {}
    types = types or {}
    initials = initials or {}
    for a in attributes:
        types[a] = types.get(a, unicode)
        attribute_to_text[a] = attribute_to_text.get(a, to_text)
    for v in values:
        types[v] = types.get(v, float)
        value_modifiers[v] = value_modifiers.get(v, to_value)
        initials[v] = initials.get(v, to_initial(types[v]))
    if '_count' not in values:
        values.append('_count')
    initials['_count'] = 0
    types['_count'] = int
    value_modifiers['_count'] = to_value
    
    def make_key(text_instance):
        return ','.join(u'{}--{}'.format(_, text_instance.get(_)) for _ in attributes)

    def make_instance(instance):
        i = {}
        for a in attributes:
            i[a] = []
            for k in attribute_to_text[a](instance.get(a), types.get(a, unicode), *args, **kwargs):
                i[a].append(k)
        for v in values:
            i[v] = instance.get(v, initials[v])
        i['_count'] = instance.get('_count', 1)
        return i

    local_stats = {}
    for row in iterator:
        row = make_instance(row)
        for trow, _ in hp.all_combinations(row):
            key_ = make_key(trow)
            ins = local_stats.get(key_) or stats_dict.get(key_) or {_: trow.get(_) for _ in attributes}
            for v in values:
                ins[v] = ins.get(v, initials[v]) + value_modifiers[v](trow.get(v, initials[v]), types[v])
            local_stats[key_] = ins

    for k, v in local_stats.iteritems():
        stats_dict[k] = v
    return local_stats

if __name__ == '__main__':
    #lists = {
    #    "a": [1, 2, 3, 4],
    #    "b": ["c", "d", "e", "f"]
    #}
    #for k, v in hp.all_combinations(lists):
    #    print k

    iterator = [
        {
            "system_response": "sr_init",
            "updated": "2021-10-01 08:04:33 AM +0000",
            "user_id": "6d252986-feb5-342b-80f0-3d4415a9855e",
            "created": "2021-10-01 08:04:33 AM +0000",
            "query_type": "type",
            "total_response_time": 4.738079071044922,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 4.738079071044922
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-01 08:13:19 AM +0000",
            "user_id": "6d252986-feb5-342b-80f0-3d4415a9855e",
            "created": "2021-10-01 08:13:19 AM +0000",
            "query_type": "type",
            "total_response_time": 6.724350214004517,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 6.724350214004517
        },
        {
            "system_response": "sr_popular",
            "updated": "2021-10-01 08:14:30 AM +0000",
            "user_id": "6d252986-feb5-342b-80f0-3d4415a9855e",
            "created": "2021-10-01 08:14:30 AM +0000",
            "query_type": "type",
            "customer_response": "popular",
            "customer_state": "cs_popular",
            "total_response_time": 0.9879529476165771,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "These are our popular items ",
            "previous_system_response": "sr_init",
            "response_time": 0.9879529476165771
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-04 07:41:49 AM +0000",
            "user_id": "c7d91212-e2e3-3b35-a315-17bd24583158",
            "created": "2021-10-04 07:41:49 AM +0000",
            "query_type": "type",
            "total_response_time": 0.64646315574646,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.64646315574646
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-04 07:51:03 AM +0000",
            "user_id": "c7d91212-e2e3-3b35-a315-17bd24583158",
            "created": "2021-10-04 07:51:03 AM +0000",
            "query_type": "type",
            "total_response_time": 0.3114910125732422,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.3114910125732422
        },
        {
            "system_response": "sr_non_veg_supreme",
            "updated": "2021-10-12 04:22:27 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-12 04:22:27 AM +0000",
            "query_type": "type",
            "customer_response": "Non Veg Supreme",
            "customer_state": "cs_non_veg_supreme",
            "total_response_time": 1.268172025680542,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Non Veg Supreme Pizza is added into your cart, what would you like to have.",
            "previous_system_response": "sr_pizza",
            "response_time": 1.268172025680542
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-12 04:26:35 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-12 04:26:35 AM +0000",
            "query_type": "type",
            "total_response_time": 0.40784406661987305,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.40784406661987305
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-04 07:52:26 AM +0000",
            "user_id": "c7d91212-e2e3-3b35-a315-17bd24583158",
            "created": "2021-10-04 07:52:26 AM +0000",
            "query_type": "type",
            "total_response_time": 0.5862381458282471,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.5862381458282471
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-04 07:55:49 AM +0000",
            "user_id": "c7d91212-e2e3-3b35-a315-17bd24583158",
            "created": "2021-10-04 07:55:49 AM +0000",
            "query_type": "type",
            "total_response_time": 0.37192201614379883,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.37192201614379883
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-04 07:56:55 AM +0000",
            "user_id": "c7d91212-e2e3-3b35-a315-17bd24583158",
            "created": "2021-10-04 07:56:55 AM +0000",
            "query_type": "type",
            "total_response_time": 0.3182659149169922,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.3182659149169922
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-04 07:58:02 AM +0000",
            "user_id": "c7d91212-e2e3-3b35-a315-17bd24583158",
            "created": "2021-10-04 07:58:02 AM +0000",
            "query_type": "type",
            "total_response_time": 1.488142967224121,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 1.488142967224121
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-04 08:24:38 AM +0000",
            "user_id": "eaa72bbf-d644-329e-8cfc-56ec1f0aad22",
            "created": "2021-10-04 08:24:38 AM +0000",
            "query_type": "type",
            "total_response_time": 0.3942131996154785,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.3942131996154785
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-04 08:53:26 AM +0000",
            "user_id": "c7d91212-e2e3-3b35-a315-17bd24583158",
            "created": "2021-10-04 08:53:26 AM +0000",
            "query_type": "type",
            "total_response_time": 0.38463401794433594,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.38463401794433594
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-11 04:07:13 PM +0000",
            "user_id": "d6d2c046-94c8-358a-80dc-295e00a73166",
            "created": "2021-10-11 04:07:13 PM +0000",
            "query_type": "type",
            "total_response_time": 0.9402709007263184,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.9402709007263184
        },
        {
            "system_response": "sr_popular",
            "updated": "2021-10-11 04:07:39 PM +0000",
            "user_id": "d6d2c046-94c8-358a-80dc-295e00a73166",
            "created": "2021-10-11 04:07:39 PM +0000",
            "query_type": "type",
            "customer_response": "popular",
            "customer_state": "cs_popular",
            "total_response_time": 0.3328120708465576,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "These are our popular items ",
            "previous_system_response": "sr_init",
            "response_time": 0.3328120708465576
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-11 06:21:21 PM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-11 06:21:21 PM +0000",
            "query_type": "type",
            "total_response_time": 5.325918912887573,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 5.325918912887573
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-11 06:21:42 PM +0000",
            "user_id": "0053d71d-844d-3775-8ad7-61df9dbd8180",
            "created": "2021-10-11 06:21:42 PM +0000",
            "query_type": "type",
            "total_response_time": 0.3397200107574463,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.3397200107574463
        },
        {
            "system_response": "sr_popular",
            "updated": "2021-10-11 06:21:52 PM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-11 06:21:52 PM +0000",
            "query_type": "type",
            "customer_response": "popular",
            "customer_state": "cs_popular",
            "total_response_time": 1.2012569904327393,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "These are our popular items ",
            "previous_system_response": "sr_init",
            "response_time": 1.2012569904327393
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-11 06:22:37 PM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-11 06:22:37 PM +0000",
            "query_type": "type",
            "total_response_time": 0.34769392013549805,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.34769392013549805
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-11 06:32:37 PM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-11 06:32:37 PM +0000",
            "query_type": "type",
            "total_response_time": 0.3286478519439697,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.3286478519439697
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-11 06:35:41 PM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-11 06:35:41 PM +0000",
            "query_type": "type",
            "total_response_time": 0.30697011947631836,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.30697011947631836
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-11 06:36:35 PM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-11 06:36:35 PM +0000",
            "query_type": "type",
            "total_response_time": 0.3295018672943115,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.3295018672943115
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-11 06:37:17 PM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-11 06:37:17 PM +0000",
            "query_type": "type",
            "total_response_time": 0.35009098052978516,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.35009098052978516
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-12 01:59:25 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-12 01:59:25 AM +0000",
            "query_type": "type",
            "total_response_time": 0.3856630325317383,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.3856630325317383
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-12 02:32:43 AM +0000",
            "user_id": "d6d2c046-94c8-358a-80dc-295e00a73166",
            "created": "2021-10-12 02:32:43 AM +0000",
            "query_type": "type",
            "total_response_time": 10.48195219039917,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 10.48195219039917
        },
        {
            "system_response": "sr_popular",
            "updated": "2021-10-12 02:32:58 AM +0000",
            "user_id": "d6d2c046-94c8-358a-80dc-295e00a73166",
            "created": "2021-10-12 02:32:58 AM +0000",
            "query_type": "type",
            "customer_response": "popular",
            "customer_state": "cs_popular",
            "total_response_time": 0.3545529842376709,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "These are our popular items ",
            "previous_system_response": "sr_init",
            "response_time": 0.3545529842376709
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-12 04:21:59 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-12 04:21:59 AM +0000",
            "query_type": "type",
            "total_response_time": 0.41050004959106445,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.41050004959106445
        },
        {
            "system_response": "sr_pizza",
            "updated": "2021-10-12 04:22:20 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-12 04:22:20 AM +0000",
            "query_type": "type",
            "customer_response": "pizza",
            "customer_state": "cs_pizza",
            "total_response_time": 1.8974030017852783,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Here is our Pizzas menu for you, what would you like to have.",
            "previous_system_response": "sr_init",
            "response_time": 1.8974030017852783
        },
        {
            "system_response": "sr_combo",
            "updated": "2021-10-12 04:27:08 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-12 04:27:08 AM +0000",
            "query_type": "type",
            "customer_response": "what pizza do you have",
            "customer_state": "cs_combo",
            "total_response_time": 2.167261838912964,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "our combo features are here",
            "previous_system_response": "sr_init",
            "response_time": 2.167261838912964
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-13 06:23:14 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-13 06:23:14 AM +0000",
            "query_type": "type",
            "total_response_time": 1.9197471141815186,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 1.9197471141815186
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-13 06:23:53 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-13 06:23:53 AM +0000",
            "query_type": "type",
            "total_response_time": 0.40939784049987793,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.40939784049987793
        },
        {
            "system_response": "sr_popular",
            "updated": "2021-10-13 06:24:58 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-13 06:24:58 AM +0000",
            "query_type": "type",
            "customer_response": "popular",
            "customer_state": "cs_popular",
            "total_response_time": 0.7986071109771729,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "These are our popular items ",
            "previous_system_response": "sr_init",
            "response_time": 0.7986071109771729
        },
        {
            "system_response": "sr_burger",
            "updated": "2021-10-13 06:25:03 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-13 06:25:03 AM +0000",
            "query_type": "type",
            "customer_response": "burger",
            "customer_state": "cs_burger",
            "total_response_time": 1.7020409107208252,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "which burger do you like?",
            "previous_system_response": "sr_popular",
            "response_time": 1.7020409107208252
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-13 06:48:02 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-13 06:48:02 AM +0000",
            "query_type": "type",
            "total_response_time": 0.6674940586090088,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.6674940586090088
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-18 06:29:48 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 06:29:48 AM +0000",
            "query_type": "type",
            "total_response_time": 0.6048061847686768,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.6048061847686768
        },
        {
            "system_response": "sr_large",
            "updated": "2021-10-18 06:30:23 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 06:30:23 AM +0000",
            "query_type": "type",
            "customer_response": "prefer drnk",
            "customer_state": "cs_large",
            "total_response_time": 2.1182451248168945,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Alright, would you like to add more?",
            "previous_system_response": "sr_init",
            "response_time": 2.1182451248168945
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-18 06:40:08 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-18 06:40:08 AM +0000",
            "query_type": "type",
            "total_response_time": 0.41635799407958984,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.41635799407958984
        },
        {
            "system_response": "sr_drinks",
            "updated": "2021-10-18 06:40:50 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-18 06:40:50 AM +0000",
            "query_type": "type",
            "customer_response": "prefer drink",
            "customer_state": "cs_drinks",
            "total_response_time": 1.1903231143951416,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "which drink would you like?",
            "previous_system_response": "sr_init",
            "response_time": 1.1903231143951416
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-18 07:16:08 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 07:16:08 AM +0000",
            "query_type": "type",
            "total_response_time": 0.3877561092376709,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.3877561092376709
        },
        {
            "system_response": "sr_large",
            "updated": "2021-10-18 07:16:27 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 07:16:27 AM +0000",
            "query_type": "type",
            "customer_response": "prefer drnk",
            "customer_state": "cs_large",
            "total_response_time": 0.5160720348358154,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Alright, would you like to add more?",
            "previous_system_response": "sr_init",
            "response_time": 0.5160720348358154
        },
        {
            "system_response": "sr_large",
            "updated": "2021-10-18 07:17:10 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 07:17:10 AM +0000",
            "query_type": "type",
            "customer_response": "prefer drnk",
            "customer_state": "cs_large",
            "total_response_time": 0.29591798782348633,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Alright, would you like to add more?",
            "previous_system_response": "sr_large",
            "response_time": 0.29591798782348633
        },
        {
            "system_response": "sr_drinks",
            "updated": "2021-10-18 07:17:55 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 07:17:55 AM +0000",
            "query_type": "type",
            "customer_response": "prefer drink",
            "customer_state": "cs_drinks",
            "total_response_time": 0.6439149379730225,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "which drink would you like?",
            "previous_system_response": "sr_large",
            "response_time": 0.6439149379730225
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-18 07:24:22 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 07:24:22 AM +0000",
            "query_type": "type",
            "total_response_time": 0.3810689449310303,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.3810689449310303
        },
        {
            "system_response": "sr_drinks",
            "updated": "2021-10-18 07:24:47 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 07:24:47 AM +0000",
            "query_type": "type",
            "customer_response": "prefer drink",
            "customer_state": "cs_drinks",
            "total_response_time": 0.3740088939666748,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "which drink would you like?",
            "previous_system_response": "sr_init",
            "response_time": 0.3740088939666748
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-18 07:46:12 AM +0000",
            "user_id": "8315f1ee-7d92-3937-b56d-156e0bb1b069",
            "created": "2021-10-18 07:46:12 AM +0000",
            "query_type": "type",
            "total_response_time": 0.39824700355529785,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.39824700355529785
        },
        {
            "system_response": "sr_drinks",
            "updated": "2021-10-18 07:49:29 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 07:49:29 AM +0000",
            "query_type": "type",
            "customer_response": "drnks",
            "customer_state": "cs_drinks",
            "total_response_time": 0.8385121822357178,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "which drink would you like?",
            "previous_system_response": "sr_drinks",
            "response_time": 0.8385121822357178
        },
        {
            "system_response": "sr_drinks",
            "updated": "2021-10-18 07:49:36 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 07:49:36 AM +0000",
            "query_type": "type",
            "customer_response": "drngs",
            "customer_state": "cs_drinks",
            "total_response_time": 0.9144251346588135,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "which drink would you like?",
            "previous_system_response": "sr_drinks",
            "response_time": 0.9144251346588135
        },
        {
            "system_response": "sr_combo",
            "updated": "2021-10-18 07:49:44 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 07:49:44 AM +0000",
            "query_type": "type",
            "customer_response": "combo",
            "customer_state": "cs_combo",
            "total_response_time": 1.7466349601745605,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "our combo features are here",
            "previous_system_response": "sr_drinks",
            "response_time": 1.7466349601745605
        },
        {
            "system_response": "sr_init",
            "updated": "2021-10-18 09:58:42 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 09:58:42 AM +0000",
            "query_type": "type",
            "total_response_time": 0.9150938987731934,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "Hi, I am dave , Have a look into new arrivals.",
            "response_time": 0.9150938987731934
        },
        {
            "system_response": "sr_combo",
            "updated": "2021-10-18 09:59:28 AM +0000",
            "user_id": "a1ebb2ab-3f35-3e51-a8bf-87cca18a85c7",
            "created": "2021-10-18 09:59:28 AM +0000",
            "query_type": "type",
            "customer_response": "combo",
            "customer_state": "cs_combo",
            "total_response_time": 0.3647458553314209,
            "conversation_id": "deployment_kiosk",
            "enterprise_id": "dave_restaurant",
            "placeholder": "our combo features are here",
            "previous_system_response": "sr_init",
            "response_time": 0.3647458553314209
        }
    ]
    stats_dict = {}
    print hp.json.dumps(do(iterator, stats_dict, ["system_response", "updated", "query_type", "customer_state"], ["response_time", "total_response_time"], types = {'updated': hp.datetime}, tz = 'Asia/Kolkata'), indent = 4)


