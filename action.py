#!/usr/bin/python
#
import sys, os
from os.path import exists as ispath, dirname, join as joinpath, abspath, sep as dirsep,isfile
from functools import wraps
BASE_DIR_PATH = dirname(dirname(dirname(abspath(__file__))))
if BASE_DIR_PATH not in sys.path:
    sys.path.append(BASE_DIR_PATH)
d = joinpath(BASE_DIR_PATH, 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
import model, helpers as hp, validators as val
from celery import Celery
from xml.etree import ElementTree
import requests
import pprint
import urllib
import ast
import communication
from communication import Communication, mail_sender, msgsender, ViewTemplateError, TemplateNotFound, View, notification_sender, request_hook
import algorithms
import matcher
from gryd import gryd
import authenticate as auth
import interactions
from flask import render_template, current_app as app, url_for
import time
from helpers import sthree

START_DELIMITER = '{'
END_DELIMITER = '}'
I2CE_PHONE_NUMBER = '9980838165'
I2CE_EMAIL_ID = 'ananth@i2ce.in'

CELERY_CONFIG = {  
    'CELERY_BROKER_URL': 'redis://{}/{}'.format(os.environ.get('REDIS_URL', 'localhost:6379'), os.environ.get('REDIS_DB_NUMBER',0)),
    'CELERY_TASK_SERIALIZER': 'pickle',
    'BROKER_USE_SSL': os.environ.get('BROKER_USE_SSL', 'False').lower() == 'true',
}


celery = Celery(__name__, broker=CELERY_CONFIG['CELERY_BROKER_URL'])
celery.conf.update(CELERY_CONFIG)
gryd.SERVICE = 'i2ce'
gryd.set_queue_manager()

logger = hp.get_logger(__name__)

def ALGORITHM_CLASS(enterprise_id, role = 'admin', user_id = None, *args, **kwargs):
    logger.info("Algorithm Class invoked")
    e = model.Model('enterprise', 'core', 'admin').get(enterprise_id)
    n = {'customer': 0, 'product': 1, "interaction": 2, "store": 3}
    model_list = [None]*4
    for m in model.models(enterprise_id, _as_model = True, role = role, user_id = user_id):
        if m.model_type in n:
            model_list[n[m.model_type]] = m
    logger.info("algorithm models are: %s", map(lambda x: x.name if x else None, model_list))
    return getattr(algorithms, os.environ.get("ALGORITHM","BaseAlgorithm"))(enterprise_id, *model_list, **kwargs)

@celery.task(name = 'add_to_algo')
def add_to_algo(model_name, enterprise_id, role, user_id, model_type, object_json):
    st = time.time()
    b = ALGORITHM_CLASS(enterprise_id, role, user_id)    
    getattr(b, 'post_' + model_type)(object_dict = object_json, add_dict = False, delete_dict = False)
    logger.info("Time taken to add to algo for model %s is %s", model_name, time.time() - st)

@celery.task(name = 'delete_from_algo')
def delete_from_algo(model_name, enterprise_id, role, user_id, model_type, object_json):
    b = ALGORITHM_CLASS(enterprise_id, role, user_id)    
    getattr(b, 'delete_' + model_type)(object_dict = object_json, delete_dict = False, add_dict = False)

@celery.task(name = 'set_customer')
def set_customer(enterprise_id, role, output, user_id = None):
    tasks = interactions.set_customer(enterprise_id, role, output, user_id = user_id)
    do_actions.apply_async(
        args = [{'actions': tasks, 'error_action': {'receiver': {'emails': ['ananth@i2ce.in']}}}, enterprise_id, role],
        kwargs = {'user_id': user_id, 'return_act': False},
        countdown = 120,
    )



def setup(self, enterprise_id, role, user_id):
    return interactions.setup(enterprise_id, role, user_id, self)


DB_ACTIONS = [
    'put', 'get', 'update', 'delete', 'post', 'patch', 'link', 'closest', 'unlink',
    'pivot', 'get_attributes', 'get_options',
    'list', 'exists', 'warehouse', 'accumulate', 'restore',
    'interactions', 'next_interaction',
    'post_transfer', 'post_preference', 'get_preference',
    'iupdate', 'iadd', 'update_many','delete_many'
]

DAVE_ACTIONS = [
    'recommendations', 'recommendees', 'influencers',
    'similar_customers', 'similar_users', 'similar_products', 'people_who_also',
    'quantity_predictions', 'customer_priority', 'customer_snapshot',
    'converse'
]

def to_action(request_method, route):
    if not route or route == 'object':
        return request_method.lower()
    if route == "link":
        if request_method.lower() == "get":
            return  "closest"
        elif request_method.lower() == "delete":
            return "unlink"
        elif request_method.lower() == "post":
            return "link"
    if route == 'objects':
        if request_method.lower() == 'get':
            return 'list'
        elif request_method.lower() == 'post':
            return 'update_many'
        elif request_method.lower() == 'delete':
            return 'delete_many'
    if route in ['transfer_session']:
        return 'post_transfer'
    if route in ['unique']:
        return 'pivot'
    if route in ['options']:
        return 'get_options'
    if route in ['attributes']:
        return 'get_attributes'
    if route in ['pivot', 'exists', 'interactions', 'next_interaction', 'warehouse', 'accumulate', 'restore'] + DAVE_ACTIONS:
        return route
    if route in ['preference']:
        return request_method.lower() + '_' + route
    return route



"""
Action Group is a list of chained actions. 
Initial data is provided in data. 
If the chain of actions is successful, then success_action is called. This is either a webhook (call) or a communicate
If there is any error in the chain then error_action is called. Similar to success_action
{
    "actions": [
        {
            _action
            _name
            ...
        },
        {
        ..
        },
        ..
    ],
    'data': {}
    'error_action': {
        'url' or 'recipient'
    },
    'success_action': {
        'url' or recipient'
    }
}

Action processes a dictionary of the following types 
{
    "_action": "action" (allowed are communicate, 
                        update, put, post, iupdate, get,
                        recommendations, recommendees, influencers, similar_customers, similar_products)
    "_name": "action_name" (used to add to the previous data under a specific key)
    "_wait_for": float ot timedelta (not required, but can be used if you want to delay the action)
    "_retries": number of times
    "_act_when": (dictionary for function which needs to evaluate to true to execute the action.
                     If evaluates to false, then the process waits for 'wait_for' time and retries)
    "_again_again_if": (this is used to repeat the action until a certain condition is satisfied, if not satisfied, then action is executed.
                The action is repeated for the number of retries)
    # _act_when gets precedence over _again_again_if
}
"""

class ActionTypeError(hp.I2CEError):
    pass

class ActionError(hp.I2CEError):
    pass

class UnknownFunctionError(hp.I2CEError):
    pass

def add_action_method(function_name = None, enterprise_id = None):
    function_name = function_name or func.__name__
    if enterprise_id: function_name = '{}_{}'.format(enterprise_id, function_name)
    def decorator(func):
        func.__name__ = function_name
        setattr(Action, function_name, func)
        # Note we are not binding func, but wrapper which accepts self but does exactly the same as func
        return func # returning func means func can still be used normally
    return decorator


class Action(object):

    def __init__(self, enterprise_id, role = None, data = None, user_id = None):
        """
        data should have all that is required to execute a certain action
        """
        self.enterprise_id = enterprise_id
        self.enterprise = model.Model('enterprise', 'core').get(self.enterprise_id)
        self.role = role
        self.user_id = user_id
        self.data = {}
        self.flattened_data = {}
        self.format_data = hp.dstuf()
        self.format_keys = []
        if data:
            self.add_to_data(data)
        self.exceptions = {}
        self.data['_exceptions'] = self.exceptions
        self.data['_exceptions_len'] = self.exceptions_len
        for k, v in self.data.iteritems():
            hp.make_all_hierarchies(self.flattened_data, k, v)
        self.add_to_data(val.default_dict(), update = True)

    def to_init(self, get_data = True):
        r = {
            'enterprise_id': self.enterprise_id,
            'role': self.role,
            'user_id': self.user_id
        }
        if get_data:
            r['data'] = self.data
        return r

    @property
    def exceptions_len(self):
        return len(self.exceptions)

    def format_for_params(self):
        d = {}
        for key in self.format_keys:
            try:
                d[key.replace('{','').replace('}','')] = key.format(**self.format_data)
            except IndexError:
                d[key.replace('{','').replace('}','')] = key.format(*self.format_data.values())
                #logger.warning("cannot write key: %s, %s", key, self.format_data)
        logger.debug("got the following keys: %s", hp.json.dumps(d, indent = 4))
        return d

    _db_actions = DB_ACTIONS
    _dave_actions = DAVE_ACTIONS
    _to_action = to_action

    def add_to_data(self, data, action_name = None, update = False):
        if isinstance(action_name, (str, unicode)):
            hp.make_all_hierarchies(self.flattened_data, action_name, data, update = update)
            hp.make_format_dict(self.format_data, action_name, data, self.format_keys, update = update)
            if update and isinstance(self.data.get(action_name), dict) and isinstance(data, dict):
                self.data[action_name].update(data)
            elif update and isinstance(self.data.get(action_name), list) and isinstance(data, list):
                self.data[action_name].extend(data)
            else:
                self.data[action_name] = data
        elif isinstance(data, dict):
            for k, v in data.iteritems():
                hp.make_all_hierarchies(self.flattened_data, k, v, update = update)
                hp.make_format_dict(self.format_data, k, v, self.format_keys, update = update)
                self.data[k] = v
        return data

    def clean_copy(self):
        d = hp.copyof(self.data)
        for a in ['_exceptions', '_exceptions_len'] + hp.default_dict().keys():
            d.pop(a, None)
        return d

    def evaluate_args(self, params):
        def check(v):
            if isinstance(v, dict):
                return self.evaluate_args(v)
            elif isinstance(v, (list, tuple)):
                return [check(v1) for v1 in v]
            elif isinstance(v, (unicode, str)):
                if v.startswith(START_DELIMITER) and v.endswith(END_DELIMITER):
                    vi = v.strip(START_DELIMITER).strip(END_DELIMITER)
                    if vi == '__data':
                        return self.clean_copy()
                    if vi in self.flattened_data:
                        return self.flattened_data[vi]
                if '{' in v and '}' in v and '{{' not in v and '}}' not in v:
                    try:
                        return eval(v.format(**self.format_data))
                    except (NameError, KeyError, AttributeError, SyntaxError, IndexError, ValueError) as e:
                        try:
                            return v.format(**self.format_data)
                        except (NameError, KeyError, AttributeError, SyntaxError, IndexError, ValueError) as e:
                            return v
            return v
        if not isinstance(params, dict):
            return check(params)
        for k, v in params.iteritems():
            params[k] = check(v)
        return params

    def process_alg_user(self, action, model_name, ids, model_types = None, **params):
        if model_types is None: model_types = ['customer', 'product', 'store']
        alg = ALGORITHM_CLASS(self.enterprise_id, self.role, self.user_id, weights = params.pop('weights', False))
        m = model.Model(model_name, enterprise_id = self.enterprise_id, role = self.role, user_id = self.user_id)
        if m.model_type not in model_types:
            raise Exception("Model {} is not a model in types {}, but {}".format(m.name, model_types, m.model_type))
        return getattr(alg, action)(*ids, model_type = m.model_type, **params)

    def recommend(self, action, action_name, *ids, **kwargs):
        kwargs.pop('model', None)
        kwargs.pop('_model', None)
        kwargs.update(kwargs.pop('_data', {}))
        alg = ALGORITHM_CLASS(self.enterprise_id, self.role, self.user_id)
        ids = hp.make_list(self.evaluate_args(ids))
        kwargs = self.evaluate_args(kwargs)
        r = dict(getattr(alg, 'get_' + action)(*ids, **kwargs))
        self.add_to_data(r, action_name)
        return r

    def communicate(self, action_name = None, **params):
        params['data'] = params.pop('_data', params.get('data', {}))
        params['data'].update(self.format_data)
        params = self.evaluate_args(params)
        c = Communication(self.enterprise_id, self.role, params.get('receiver_model'), user_id = self.user_id)
        logger.info("Executing communication with params: %s", params)
        r =  c.send(**params)
        self.add_to_data(params, action_name)
        return r

    def call(self, url, action_name = None, method = 'POST', application_type = 'json', headers = None, url_params = None, data = None, **kwargs):
        url_params = url_params or {}
        data = self.evaluate_args(data or {})
        url = url.strip()
        if method.upper() == 'GET':
            url_params.update(self.format_for_params())
            response = requests.get(url, headers = headers, params = url_params)
        else:
            data = self.evaluate_args(data)
            url_params = self.evaluate_args(url_params)
            logger.info("Sent data: %s", hp.json.dumps(data, indent = 4))
            logger.info("Sent params: %s", hp.json.dumps(url_params, indent = 4))
            logger.info("Sent URL: %s", url)
            logger.info("Sent Headers: %s", headers)
            if application_type == 'json':
                response = requests.request(method.upper(), url, headers = headers, json = data, params = url_params)
            else:
                response = requests.request(method.upper(), url, headers = headers, data = data, params = url_params)
        try:
            r = response.json()
            logger.info("Received response from URL: %s", r)
            self.add_to_data(r, action_name)
        except ValueError:
            r = response.text.encode('utf-8')
            logger.warn("Received non-json response from url {} ({}): {}".format(url, method, r))
        if response.status_code >= 400:
            raise ActionError("Response status {} for request:{} {}: response : {}".format(response.status_code, method, url, r))
        return r


    def db_action(self, action, action_name = None, model_name = None, **params):
        if not model:
            raise ActionError("parameter 'model' needs to be sepcified for action {} (name = {})".format(action, action_name or action))
        logger.info("[%s:%s:%s] Executing DB action '%s' named '%s' on model '%s'", 
            self.enterprise_id, self.role, self.user_id, action, action_name, model_name
        )
        m = model.Model(model_name, self.enterprise_id, role = self.role, user_id = self.user_id)
        _chain_together = params.pop('_chain_together', False)
        params = self.evaluate_args(params)
        params.update(params.pop('_data', {}))
        params.update(params.pop('_params', {}))
        self.add_to_data(params)
        params = self.evaluate_args(params)
        if action in ['list', 'get', 'exist', 'pivot']:
            r = self._do_model_actions(m, action, params, _chain_together = True, _return_output = True)
            if r:
                self.add_to_data(r, action_name)
                return r
        params_with_ids = params.copy()
        ids = params.pop('_model_id', None) or \
              params.pop('_model_ids', None) or \
              params.pop('model_id', None) or \
              params.pop('model_ids', None) or \
              params.pop('ids', None) or \
              params.pop('_ids', None) or \
              params.pop('id', None) or \
              params.pop('_id', None) or \
              params.pop('_args', None) or \
              params.pop('args', None)
        ids = hp.make_list(ids or [])
        if ids and action in ['put', 'update', 'get', 'delete', 'interactions', 'iupdate', 'patch'] and len(m.ids) == 1 and len(ids) > 1:
            ids = ['/'.join(ids)]
        if ids and action in ['link', 'closest', 'unlink']:
            ids =[ids[i:i+len(m.ids)] for i in range(0, len(ids), len(m.ids))]
        params_with_instance = params.copy()
        instance = params.pop('_instance', None) or \
                   params.pop('instance', None) or params
        old_instance = {}
        if action in ['patch', 'update', 'post'] and '_previous_instance' not in params:
            params['_previous_instance'] = old_instance
            params = self.evaluate_args(params)
        if action in ['put', 'update', 'post'] and m.model_type in ['interaction']:
            if not instance:
                raise ActionError("Instance is none for action post on model {}".format(m.name))
            params = interactions.set_interaction(self.enterprise_id, 'admin', instance)
            if action in ['post']:
                for a, i in interactions.before_interaction(self, instance):
                    do_action(self.to_init(), a, i)
        if action in ['update_many', 'delete_many']:
            naction = action.replace('_many','')
            output = []
            for c in m.filter(**params):
                ids_ = m.dict_to_ids(c)
                output.append(hp.make_single(ids_))
                do_action.delay(self.to_init(False), {
                    '_action': naction,
                    '_model': m.name,
                    '_name': '{} on object of model {} with id {}'.format(naction, m.name, ids_),
                    '_ids': ids_,
                    '_instance': instance,
                    '_previous_instance': c
                })
        elif action in ['pivot']:
            if not '_fresh' in params_with_instance:
                params_with_instance['_fresh'] = False
            if not '_freshen' in params_with_instance:
                params_with_instance['_freshen'] = 3600*6
            output = getattr(m, action)(ids, **params_with_instance)
        elif ids and action in ['link', 'closest', 'unlink']:
            output = getattr(m, action)(*ids, **params_with_instance)
        elif action in ['get_attributes', 'get_options']:
            output = getattr(m, action)(hp.make_single(ids), **params_with_instance)
        elif action in ['list', 'exists']:
            output = getattr(m, action)(**params_with_ids)
        elif action in ['warehouse', 'accumulate', 'restore']:
            output = getattr(m, action)(**params_with_ids)
        elif action in ['iupdate']:
            old_instance = params_with_instance
            output = getattr(m, action)(ids, params_with_instance)
        elif action in ['interactions']:
            output = interactions.get_interactions(
                model_name, 
                enterprise_id = self.enterprise_id, 
                role = self.role, 
                ids = ids, 
                user_id = self.user_id,
                **params_with_instance
            )
        elif action in ['next_interaction']:
            is_product = model_name in model.models(self.enterprise_id, role = self.role, model_type = 'product', _as_model = True, user_id = self.user_id)
            output = interactions.get_last_interaction_stage(
                enterprise_id = self.enterprise_id, 
                role = self.role, 
                product_id = ids[0] if is_product else ids[1],
                customer_id =  ids[1] if is_product else ids[0],
                user_id = self.user_id,
                **params_with_instance
            )
        elif any(action.endswith(a_) for a_ in ["transfer", "preference"]):
            output = self.process_alg_user(action, model_name, ids, **params)
        elif action in ['post']:
            if not instance:
                raise ActionError("Instance is none for action post on model {}".format(m.name))
            output = m.post(instance)
            ids = m.dict_to_ids(output)
        elif action in ['put', 'get' ,'update', 'delete', 'patch']:
            if ids:
                output = getattr(m, action)(ids, instance = instance, **params)
            else:
                ids = m.dict_to_ids(instance)
                output = getattr(m, action)(ids, instance = instance, **params)
        else:
            raise ActionError("Unknown action {}".format(action))
        self.add_to_data(output, action_name)
        if self.enterprise.get('do_algorithms', False) and action in ['put', 'update', 'post', 'patch'] and m.model_type in ['customer', 'product', 'interaction', 'store']:
            add_to_algo.apply_async(
                args = [m.name, m.enterprise_id, m.role, m.user_id, m.model_type, output],
                queue = 'algorithms'
            )
        if self.enterprise.get('do_algorithms', True) and action in ['delete'] and m.model_type in ['customer', 'product', 'interaction', 'store']:
            delete_from_algo.apply_async(
                args = [m.name, m.enterprise_id, m.role, m.user_id, m.model_type, output],
                queue = 'algorithms'
            )
        if self.enterprise.get('do_algorithms', True) and action in ['post', 'put', 'update'] and m.model_type in ['interaction']:
            set_customer.apply_async(
                args = [self.enterprise_id, 'admin', output], 
                kwargs = {"user_id": None},
                countdown = 120
            )
            for a, i in interactions.after_interaction(self, output):
                do_action.delay(self.to_init(False), a, i)
        if action in ['post', 'put', 'update', 'delete'] and m.name in ('interaction_stage',):
            model.pop_enterprise_cache(self.enterprise_id)
        # POST Action actions....
        if action in ['put', 'update', 'post', 'patch', 'delete', 'iupdate', 'link', 'unlink']:
            self._do_model_actions(m, action, output, old_instance, _chain_together = _chain_together)
        if action in ['post', 'put'] and m.has_login and (
                all(_i is None for _i in ids ) or \
                output.get('password', hp.PASSWORD_NOT_SET) == hp.PASSWORD_NOT_SET or \
                not output.get('validated', False)
            ):
            with app.app_context():
                do_action.delay(self.to_init(False), {
                    '_action': 'communicate',
                    '_name': 'send password notification',
                    'receiver': {'emails': [output.get(e) for e in m.emails], 'phone_numbers': [output.get(e) for e in m.phone_numbers]},
                    'email_template': 'set_password.html',
                    'message': """
                        A new login has been created for you for Enterprise {} at Dave.AI. Please visit {{personalized_url}} to login before 5 days.
                        """.format(
                            self.enterprise['name'],
                            '{personalized_url}'
                        ),
                    'enterprise_id': self.enterprise_id,
                    'enterprise_name': self.enterprise['name'],
                    'data' : {
                        'role' : m.name,
                        'object_id' : hp.make_single(m.dict_to_ids(output)),
                        'timestamp' : time.time() + 5*3600*24  # Now plus 5 days
                    }
                })
        return output

    def _do_model_actions(self, model_, action_, output_, old_instance_ = None, role = None, user_id = None, _chain_together = None, _return_output = True):
        a = model_.actions.get(action_)
        if not a:
            if action_ in ['post', 'update', 'patch', 'put']:
                a = model_.actions.get('write')
            if action_ in ['get', 'list', 'exist']:
                a = model_.actions.get('refresh')
        if not a:
            return None
        a1 = self.evaluate_args(hp.copyof(model_.actions))
        role = a1.pop('_role', self.role)
        user_id = a1.pop('_user_id', self.user_id)
        _chain_together = _chain_together or a1.get('_chain_together')
        logger.info("Performing 'post_action/pre_action' action {} for {} in model {}".format(a, action_, model_))
        actions_ = {
            'actions': hp.copyof(a), 
            'error_action': {
                "receiver": {'emails': self.enterprise['email']},
                "message": "Error in executing post-action action for {} in model {} by user_id {}".format(action_, model_.name, self.user_id)
            }
        }
        actions_['data'] = hp.copyof(output_)
        if old_instance_:
            actions_['data']['_previous_instance'] = old_instance_
        if _chain_together:
            logger.info("Executing chained actions")
            r = do_actions(actions_, self.enterprise_id, role = role, user_id = user_id, return_act = True)
            if _return_output:
                o = r.data.get(a[-1].get('_name'))
                if o:
                    return o
                return False
            return r
        do_actions.delay(actions_, self.enterprise_id, role = role, user_id = user_id, return_act = False)


    def act(self, action, action_index = 0, raise_error = False):
        _action = action.copy()
        logger.info("[%s:%s:%s] Executing action #%s: %s", self.enterprise_id, self.role, self.user_id, action_index, action)
        try:
            if action.get('_action') in self._db_actions:
                return self.db_action(_action.pop('_action'), _action.pop('_name', None), _action.pop('_model', _action.pop('model', None)), **_action)
            elif action.get('_action') == 'communicate':
                return self.communicate(action_name = _action.pop('_name', None), **_action)
            elif action.get('_action') == 'converse':
                _action.pop('_action')
                _action.update(_action.pop('_data',_action.pop('data', {})))
                return self.converse(_action.pop('_conversation_id', _action.pop('conversation_id', None)), _action.pop('_customer_id', _action.pop('customer_id', None)), _action.pop('_name', None), **_action)
            elif action.get('_action') == 'synthesize':
                _action.pop('_action')
                _action.update(_action.pop('_data', _action.pop('data', {})))
                return self.synthesize(_action.pop('_conversation_id', _action.pop('conversation_id', "synthesize")), _action.pop('_utterance', _action.pop('utterance', None)), _action.pop('_name', None), **_action)
            elif action.get('_action') == 'voices':
                _action.pop('_action')
                _action.update(_action.pop('_data', _action.pop('data', {})))
                return self.voices(_action.pop('_name', None), **_action)
            elif action.get('_action') == 'translate':
                _action.pop('_action')
                _action.update(_action.pop('_data',_action.pop('data', {})))
                return self.translate(action_name = _action.pop('_name', None), **_action)
            elif action.get('_action') in self._dave_actions:
                return self.recommend(_action.pop('_action'), _action.pop('_name', None), *_action.pop('_ids', _action.pop('ids',[])), **_action)
            elif action.get('_action') in ['call']:
                _action.pop('_action')
                if not 'url' in _action:
                    raise ActionError("No url specified in call action")
                return self.call(_action.pop('url'), _action.pop('_name', None), **_action)
            elif action.get('_action') in ['_function', '__function__', 'function']:
                f = _action.pop('_function', _action.pop('__function__', _action.pop('function', None)))
                if hasattr(self, '{}_{}'.format(self.enterprise_id, f)):
                    f = getattr(self, '{}_{}'.format(self.enterprise_id, f))
                elif hasattr(self, f):
                    f = getattr(self, f)
                else:
                    raise UnknownFunctionError("Unknown function {} in action {}".format(f, _action.get('_name', "Unnamed")))
                args = self.evaluate_args(_action.pop('args', []))
                kwargs = self.evaluate_args(_action.pop('kwargs', {}))
                r = f(*args, **kwargs)
                self.add_to_data(r, _action.pop('_name', None))
                return r
            elif action.get('_action') in ['_gryd_task', 'gryd_task']:
                _action.pop('_action')
                task = _action.pop('_gryd_task', _action.pop('gryd_task', _action.pop('task', None)))
                if not task:
                    logger.error("No task provided in gryd task")
                    return
                service = _action.pop('_gryd_service', _action.pop('gryd_service', _action.pop('service', 'i2ce')))
                args = self.evaluate_args(_action.pop('args', []))
                kwargs = self.evaluate_args(_action.pop('kwargs', {}))
                publisher = _action.pop('publisher', 'i2ce')
                kwargs.update({
                    "i2ce_headers": {
                        "X-I2CE-ENTERPRISE-ID": self.enterprise_id,
                        "X-I2CE-USER-ID": self.user_id,
                        "X-I2CE-API-KEY": model.Model(self.role, self.enterprise_id).get(self.user_id, {}, internal = True).get('api_key'),
                        "Content-Type": "application/json"
                    }
                })
                r = gryd.create_async_task(task, service, args = args, kwargs = kwargs, publisher = publisher, enterprise_id = self.enterprise_id, **_action) 
                return r
            elif action.get('_action') in ['eval', '__eval__', '_eval']:
                r = eval(_action.pop('_eval').format(**self.format_data))
                self.add_to_data(r, _action.pop('_name', None))
                return r
            else:
                raise ActionTypeError("Unknown action {}".format(action.get('_action')))
        except Exception as e:
            if raise_error:
                raise
            self.exceptions.update({ action.get('_name') or action_index : {'message': e.message, 'traceback': hp.print_error(), 'action': hp.json.dumps(action, indent = 4)}})
            if action.get('_exception_retries'):
                daction = action.copy()
                daction['_exception_retries'] -= 1
                do_action.apply_async(
                    args = [self.to_init(False), daction, action_index, raise_error], 
                    countdown = max(action.get('_wait_for', 60), 60)
                )
            return {}



@celery.task(name = 'do_action')
def do_action(act, action, action_index = 0, raise_error = False):
    if isinstance(act, dict):
        act = Action(**act)
    if not isinstance(act, Action):
        raise ActionError("Cannot perform action on type: %s", type(act))
    return act.act(action, action_index, raise_error)

Action.do_action = do_action

"""
e.g.  action_group
[

    {
        "_action": "get"
        "model": "interaction",
        "model_ids": "{interaction_id}"
    }, # This will call the get action add the response to the scope
    {
        "_action": "communicate",
        "_name": "send_communication",
        "_act_when": "{stage} != 'interested'",
        "_retries": 3,
        "_exception_retries": 3,
        "_wait_for": 3000,
    }, # stage is received from the scope of the previous action
       # if the condition is not met, then the action series stops
    {
        "_action": "update",
        "_act_when": "{stage} == 'interested',
        "model": "customer",
        "models_ids": "{customer_id}",
        "instance": {
            "engagement_stage": "qualified"
        }
    }
]
"""



@celery.task(name="do_actions")
def do_actions(action_group, enterprise_id=None, role = None, user_id = None, raise_error = False, act = None, return_act = True):
    """
    action is a dict of this sort
    {
        "actions" []
        "data": {},
        "error_action": {
            "recipient": {...}
            or
            "url": "..."
        },
        "success_action": {
            "recipient": {...}
            or
            "url": "..."
        }
    }
    """
    logger.info(
        "Received do_actions: %s from enterprise_id: %s, role: %s, user_id: %s, return_act: %s", 
        action_group, enterprise_id, role, user_id, return_act
    )
    try:
        act = act or Action(enterprise_id, role, data = action_group.get('data'), user_id = user_id)
        user_id=act.user_id
        role=act.role
        enterprise_id=act.enterprise_id
        error_action = action_group.get('error_action')
        success_action = action_group.get('success_action')
        for action_index, action in enumerate(action_group.get('actions', [])):
            def do_retry(action, action_index, condition, evaluation = False):
                if action.get(condition):
                    try:
                        act_when = action[condition].format(**act.format_data)
                        ev = eval(act_when)
                        if not evaluation: ev = not ev
                    except (KeyError, NameError, AttributeError) as e:
                        logger.warn("Could not evaluate {} - [{}] with available data: {} for {}: - {}".format(
                                condition, action[condition], e, action_index, enterprise_id
                            )
                        )
                        return not evaluation
                    if ev:
                        logger.info("%s condition %s %s met for enterprise %s: data: %s", 
                            condition, act_when,  '' if evaluation else 'not', enterprise_id, act.data
                        )
                        r = action.get('_retries', None)
                        if isinstance(r, (str, unicode)):
                            # Evaluation of retry criterion
                            # r1 is the number of retries next time
                            r1 = r.format(**act.format_data)
                            r2 = eval(r1)
                            r1 = '({}) - 1'.format(r)
                            r = r2
                        if isinstance(r, (int, bool, float)):
                            r = int(r)
                            r1 = r - 1
                        else:
                            r = None
                            r1 = None
                        if r > 0:
                            logger.info("%s condition %s %s met for enterprise %s: data: %s, retrying...", 
                                condition, act_when, '' if evaluation else 'not', enterprise_id, act.data, 
                            )
                            action['_max_retries'] = action.get('_max_retries', r)
                            action['_retries'] = r1
                            do_actions.apply_async(
                                args = [action_group, enterprise_id], 
                                kwargs = {'role': role, 'user_id': user_id, 'return_act': False},
                                countdown = max(action.get('_wait_for', 60), 60)
                            )
                            logger.info("New action group is: %s", hp.json.dumps(action_group, indent = 4))
                        else:
                            logger.info("%s condition %s %s met %s time for enterprise %s: data: %s, finishing...", 
                                condition, act_when, '' if evaluation else 'not', action.get('_max_retries', 0), enterprise_id, act.data
                            )
                            if not evaluation:
                                act.exceptions.update({
                                    action.get('_name') or action_index : {
                                        'message': """
            Action group was not completed after {} retries due to unsatisfied condition: {}
            Action Group: {}
                                        """.format(
                                            action.get('_max_retries', 0),
                                            act_when,
                                            hp.json.dumps(action_group, indent = 4)
                                        )
                                    }
                                })
                    return ev
                return evaluation
            if do_retry(action, action_index, '_act_when'):
                break
            if action.get('_act_if'):
                try:
                    if not eval(action['_act_if'].format(**act.format_data)):
                        logger.info("Could not evaluate {} with available data".format(action['_act_if'].format(**act.format_data)))
                        continue
                except (KeyError, NameError, AttributeError) as e:
                    logger.warn("Could not evaluate {} with available data, Error: {}".format(action['_act_if'], e))
                    continue
            if action.get('_break_on_error'):
                try:
                    act.act(action, action_index = action_index, raise_error = True)
                except Exception as e:
                    hp.print_error(e)
                    logger.error("Could not execute the action {} successfully".format(action_index))
                    do_retry(action, action_index, '_break_on_error', True)
                    break
            else:
                act.act(action, action_index = action_index, raise_error = raise_error)
            do_retry(action, action_index, '_act_again_if', True)

        logger.info("action exceptions: %s", act.exceptions)
        if act.exceptions:
            logger.error("error_action: %s", error_action)
            logger.error("exceptions: %s", act.exceptions)
            if error_action and error_action.get('sthree'):
                object_name = 'error_logs/{}/{}/{}/{}_{}_{}'.format(os.environ.get('SERVER_NAME'), act.enterprise_id, act.role or 'default', act.user_id or 'user', str(hp.now()), hp.id_generator(size=4) )
                sthree.S3Upload('s3').upload_file_object_to_aws(act.exceptions, error_action['sthree'], object_name = object_name)
            if error_action and error_action.get('log_file'):
                file_name = '{}_{}_{}'.format(act.enterprise_id,act.role, act.user_id) 
                sthree.get_log_file(act.exceptions, file_path = error_action['log_file'], file_name= file_name, enterprise_id = act.enterprise_id, role = act.role, user_id = act.user_id)
                
            if error_action and error_action.get('url'):
                response = requests.post(error_action.get('url'), headers = error_action.get('headers'), json = act.exceptions, params = error_action.get('url_params'))
                logger.info("Response after calling url in response to errors (%s): %s", response.status, response.text)
            if error_action and error_action.get('receiver'):
                logger.error("Sending mail to %s in response to exceptions.", error_action.get('receiver'))
                c = Communication(act.enterprise_id, act.role)
                c.send(
                    receiver = error_action.get('receiver'), title = error_action.get('title', "Errors in executing async/dave"),
                    html_template = 'async_error.html', data = {'exceptions': act.exceptions},
                    message = error_action.get("message", "Please contact info@iamdave.ai to understand the reason.")
                )
                c.send(receiver = {'emails': ['ananth@i2ce.in']}, html_template = 'async_error_traceback.html', data = {
                    'exceptions': act.exceptions,
                    'enterprise_id': act.enterprise_id,
                    'role': act.role,
                    'user_id': act.user_id,
                    'data': """
                        <table>
                    """ + '\n\t\t\t'.join(u'<tr><td>{}</td><td>{}</td></tr>'.format(k,v) for k, v in act.flattened_data.iteritems()) +
                    """
                        </table>
                    """,
                    'server_url': os.environ.get('SERVER_NAME')
                })

        else:
            logger.info("success_action: %s", success_action)
            if success_action and success_action.get('url'):
                response = requests.post(url, headers = success_action.get('headers'), json = act.data, params = success_action.get('url_params'))
            if success_action and success_action.get('receiver'):
                c = Communication(act.enterprise_id, act.role)
                c.send(
                    receiver = success_action.get('receiver'), 
                    title = success_action.get('title'), 
                    email_template = success_action.get('email_template'),
                    html_template = success_action.get('html_template'),
                    message = success_action.get('message') or "Async/dave action group successful", 
                )
        if return_act:
            return act
        return act.data
    except Exception as exc1:
        if raise_error:
            raise
        act = Action(enterprise_id, role, data = action_group.get('data'))
        act.communicate(
                view_type = 'transaction',
                receiver = {'emails': ['ananth@i2ce.in', act.enterprise['email']]}, 
                message = "Action group for enterprise {} was not completed due to error: {}".format(
                    enterprise_id, 
                    ('\n'.join(hp.print_error())).replace('{', '{{').replace('}', '}}')
                )
        )
        hp.print_error()
        raise Exception(getattr(exc1, 'args', getattr(exc1, 'message', 'Exception in actions')))

@celery.task(name = 'matcher_ops')
def matcher_ops(matcher_name, db_name, func, *args, **kwargs):
    cls = matcher.Matcher(matcher_name, DB_NAME = db_name)
    return getattr(cls, func)(*args, **kwargs)


def action_func(slf, ins, attr, val, *action_list, **kwargs):
    kwargs.update(ins)
    rdata = do_actions(
        {
            "actions": action_list,
            "data": kwargs
        },
        enterprise_id = slf.model.enterprise_id,
        role = slf.model.role,
        user_id = slf.model.user_id,
        raise_error = True,
        return_act = False
    )
    if kwargs.get("_attributes"):
        return {a: rdata[a] for a in hp.make_list(kwargs.get('_attributes'))}
    return rdata

val.make_function(
    action_func,
    "do_actions",
    given_args = ['self', 'instance', 'attribute', 'value'],
    help_string = "Run an action, args provide the action list and kwargs provides the data",
    idempotent = False
)

communication.do_actions = do_actions


if __name__ == "__main__":

    enterprise = model.Model('enterprise', 'core', 'admin')
    enterprise.delete('test1', None)
    e = enterprise.post({
        'name': 'test1',
        'email': 'test1@test1.com',
        'phone_number': '9980838165',
        'enterprise_logo': 'sl;fkd',
        'credits_left': 0
    })

    do_action({'enterprise_id': 'test1'}, {'_action': 'gryd_task', '_gryd_task': 'test_function', 'gryd_service': 'sample_service', 'kwargs': {'a': 5, 'b': 4}}) 
