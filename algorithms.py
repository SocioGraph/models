import os 
import time
import cherrypy

from functools import wraps
from collections import OrderedDict
from operator import itemgetter

from copy import deepcopy as copyof
import numpy as np
import helpers as hp
from model import Model, models
import interactions
from matcher import Matcher, MatcherDict,datetime, timedelta, date, dtime
from pgpersist import pgpcollection, pgpdict, PersistKeyError
from helpers import make_uuid
from helpers.runapi import RunApi, ProductNotAdded, UserNotAdded
from helpers.timeit import Timer
import string
from cacher import Cacher


DEBUG_LEVEL = hp.logging.INFO
logger = hp.get_logger(__name__, DEBUG_LEVEL)
timer = hp.get_logger('timer', hp.logging.INFO)

RunApi.url = None

t = Timer()


class StageNotAdded(hp.I2CEError):
    pass

class RequiredTypeAttributeError(hp.I2CEError):
    pass

class UnknownInteractionType(hp.I2CEError):
    pass

class InvalidId(hp.I2CEError):
    pass

class StaticsDataUnavailable(hp.I2CEError):
    pass

class FactorUpdater(object):
    def __init__(self, user_model, product_model, user_id, product_id=None):
        self.user_model = user_model
        self.product_model = product_model
        self.user_object = None
        self.product_object = None
        if user_id is not None:
            self.user_object = self.user_model.get(user_id)
            if not self.user_object:
                raise InvalidId("Id {} not found in model {}".format(
                    user_id, self.user_model.name))
        if product_id is not None:
            self.product_object = product_id if isinstance(product_id, dict) else self.product_model.get(product_id)
            if not self.product_object:
                if isinstance(product_id, dict):
                    raise StaticsDataUnavailable("No statistic data available for the request. Try retraining the model with these attributes")
                else:
                    raise InvalidId("Id {} not found in model {}".format(
                        product_id, self.product_model.name)
                    )
        self.factors = {
            'user_similarity': {},
            'product_preference': {},
            'common_interactions': {},
            'product_similarity': {},
            'user_bias': {},
            'common_customers': {}
        }


    model_to_factor_map = {
        'user_matcher': 'user_similarity',
        'user_aggr_matcher': 'product_preference',
        'user_interact_matcher': 'common_interactions',
        'product_matcher': 'product_similarity',
        'product_aggr_matcher': 'user_bias',
        'product_interact_matcher': 'common_customers'
    }
    
    def update_factor(self, model, keys, attribute, value, score, weight):
        logger.debug(
            "Inside update factor, model: %s, attribute: %s, score: %s",
            model.name, attribute, score)
        keys = hp.make_tuple(keys)
        factor = self.model_to_factor_map.get(model.name)
        if not factor:
            return None
        if not keys in self.factors[factor]:
            self.factors[factor][keys] = {}
        if not weight:
            return None
        if model.name == 'user_matcher':
            logger.debug("Adding user_similarity factor, %s, %s, %s, %s, %s, %s", keys, attribute, value, score, weight, self.user_object.get(attribute))
            if value and score == 0 and attribute in self.user_object:
                logger.debug("being added")
                title = attribute
                att = self.user_model.attributes.get(attribute)
                if att:
                    title = att.title or att.name
                self.factors['user_similarity'][keys][title] = (
                    weight, (self.user_object.get(attribute))
                )
        elif model.name == 'user_aggr_matcher':
            self.factors['product_preference'][keys][attribute] = (
                self.factors['product_preference'][keys].get(attribute,
                                                       (0, None))[0] - score,
                self.product_object.get(attribute, value) if self.product_object else value)
        elif model.name == 'user_interact_matcher':
            self.factors['common_interactions'][keys][value] = (
                self.factors['common_interactions'][keys].get(value,
                                                        (0, None))[0] + score,
                attribute)
        elif model.name == 'product_matcher' and self.product_object:
            self.factors['product_similarity'][keys][attribute] = (
                self.factors['product_similarity'][keys].get(attribute,
                                                       (0, None))[0] - score,
                self.product_object.get(attribute))
        elif model.name == 'product_aggr_matcher':
            self.factors['user_bias'][keys][attribute] = (
                self.factors['user_bias'][keys].get(attribute,
                                              (0, None))[0] - score, value)
        elif model.name == 'product_interact_matcher':
            self.factors['common_customers'][keys][value] = (
                self.factors['common_customers'][keys].get(value,
                                                     (0, None))[0] - score,
                attribute)
        else:
            pass

    def limit(self, keys, num=10):
        keys = hp.make_tuple(keys)
        num = int(num)
        return_dict = {}
        for n, d in self.factors.iteritems():
            logger.debug("limiting the factors for %s: %s", n, d)
            if d and d.get(keys):
                return_dict[n] = dict(
                    ((k, v[1])
                     for k, v in sorted(
                         d[keys].iteritems(), key=lambda x: x[1][0], reverse=True)[:num]
                    ))
        return return_dict


class BaseAlgorithm(object):
    """
    Base Algorithm. All other algorithms should follow this API.
    Algorithms should be able to predict if trained previously from
    a new instance.
    """

    def drop_customer_matchers(self):
        for m in [
                '_user_matcher', '_user_aggr_matcher',
                '_user_interact_matcher'
        ]:
            getattr(self, m).drop()

    def drop_product_matchers(self):
        for m in [
                '_product_matcher', '_product_aggr_matcher',
                '_product_interact_matcher'
        ]:
            getattr(self, m).drop()

    #@t.timeit
    @RunApi.via_request
    def __init__(self,
            enterprise_id,
            user_model=None,
            product_model=None,
            interaction_model=None,
            store_model=None,
            weights=None
        ):
        self.enterprise_id = enterprise_id
        self._user_dict = user_model or hp.make_single(models(enterprise_id, model_type = 'customer', _as_model = True), force = True)
        self._product_dict = product_model or hp.make_single(models(enterprise_id, model_type = 'product', _as_model = True), force = True)
        self._interactions_dict = interaction_model or hp.make_single(models(enterprise_id, model_type = 'interaction', _as_model = True), force = True)
        self._store_dict = store_model or hp.make_single(models(enterprise_id, model_type = 'store', _as_model = True), force = True)
        self._recommendation_model = Model('recommendation', enterprise_id)
        # Gets each user added once
        self._user_matcher = Matcher(
            'user_matcher',
            key_names=self._user_dict.ids,
            key_types=self._user_dict.id_atypes,
            DB_NAME=enterprise_id)
        # Gets each product added once
        self._product_matcher = Matcher(
            'product_matcher',
            key_names=self._product_dict.ids,
            key_types=self._product_dict.id_atypes,
            DB_NAME=enterprise_id)
        # Gets each store added once
        if self._store_dict:
            self._store_matcher = Matcher(
                'store_matcher',
                key_names=self._store_dict.ids,
                key_types=self._store_dict.id_atypes,
                binner_name=self._user_matcher.binner.name,
                document_mapper_name = self._user_matcher.document_mapper.name,
                image_mapper_name = self._user_matcher.image_mapper_name,
                DB_NAME=enterprise_id) if self._store_dict else None
        # Gets positivity score added for each interaction
        self._user_aggr_matcher = Matcher(
            'user_aggr_matcher',
            key_names=self._user_dict.ids,
            key_types=self._user_dict.id_atypes,
            binner_name=self._product_matcher.binner.name,
            document_mapper_name = self._product_matcher.document_mapper.name,
            image_mapper_name = self._product_matcher.image_mapper_name,
            DB_NAME=enterprise_id)
        if self._store_dict:
            self._store_aggr_matcher = Matcher(
                'store_aggr_matcher',
                key_names=self._store_dict.ids,
                key_types=self._store_dict.id_atypes,
                binner_name=self._product_matcher.binner.name,
                document_mapper_name = self._product_matcher.document_mapper.name,
                image_mapper_name = self._product_matcher.image_mapper_name,
                DB_NAME=enterprise_id) if self._store_dict else None
        # Gets positivity score added for each interaction
        self._product_aggr_matcher = Matcher(
            'product_aggr_matcher',
            key_names=self._product_dict.ids,
            key_types=self._product_dict.id_atypes,
            binner_name=self._user_matcher.binner.name,
            document_mapper_name = self._user_matcher.document_mapper.name,
            image_mapper_name = self._user_matcher.image_mapper_name,
            DB_NAME=enterprise_id)
        # Gets positivity score added for each interaction
        self._user_interact_matcher = Matcher(
            'user_interact_matcher',
            key_names=self._user_dict.ids,
            key_types=self._user_dict.id_atypes,
            DB_NAME=enterprise_id)
        # Gets positivity score added for each interaction
        self._product_interact_matcher = Matcher(
            'product_interact_matcher',
            key_names=self._product_dict.ids,
            key_types=self._product_dict.id_atypes,
            DB_NAME=enterprise_id)
        if self._store_dict:
            self._store_interact_matcher = Matcher(
                'store_interact_matcher',
                key_names=self._store_dict.ids,
                key_types=self._store_dict.id_atypes,
                DB_NAME=enterprise_id) if self._store_dict else None
        self._user_dict.add_attached_matchers(self._user_matcher, self._user_aggr_matcher, self._user_interact_matcher)
        self._product_dict.add_attached_matchers(self._product_matcher, self._product_aggr_matcher, self._product_interact_matcher)
        if self._store_dict:
            self._store_dict.add_attached_matchers(self._store_matcher, self._store_aggr_matcher, self._store_interact_matcher)
        self._matcher_weights_dict = pgpdict(
            'matcher_distance_weights', DB_NAME=enterprise_id)
        self.set_weights(weights if isinstance(weights, dict) else {})
        self._interaction_type_attr = self._interactions_dict.parent_ids[
            'interaction_stage']['self_ids'][0]
        self._interaction_stage_model = Model('interaction_stage',
                                              enterprise_id)
        self._interaction_stages = dict(
            (i.pop('name'), i)
            for i in self._interaction_stage_model.list(_as_option=True))
        self._experience_stages = dict(
            filter(lambda x: x[1].get('experienced_product', False),
                   self._interaction_stages.iteritems()))
        self._product_types = {i_: self._product_dict.attributes[i_].type for i_ in self._product_dict.attr_seq}
        self._user_types = {i_: self._user_dict.attributes[i_].type for i_ in self._user_dict.attr_seq}
        if self._store_dict: 
            self._store_types = {i_: self._store_dict.attributes[i_].type for i_ in self._store_dict.attr_seq}
        else:
            self._store_types = {}
        self._cacher = Cacher(self.enterprise_id)
        self.st = interactions.setup(self.enterprise_id)

    def _get_lowest_experienced_stage(self):
        return interactions.get_lowest_experienced_stage(self._interaction_stages)

    def _get_highest_interaction_stage(self):
        """
        gets the interaction stage for the highest positivity score
        """
        return interactions.get_highest_interaction_stage(self._interaction_stages)

    def _get_positivity_score(self, interaction_type, instance = None, default = interactions.UnknownInteractionType):
        """
        x is the interaction instance
        """
        return interactions.get_positivity_score(self.enterprise_id, self._interaction_stages, interaction_type, instance, default)

    def _get_quantities(self, instance):
        return interactions.get_quantities(self._interaction_stages, self._interaction_type_attr, instance)

    def _quantity_stages(self):
        return interactions.get_quantity_stages(self._interaction_stages)

    def _time_attr_stage(self, stage):
        return interactions.get_time_attr_stage(self._interaction_stages, stage)

    def _get_time_attributes(self, instance):
        return interactions.get_time_attr_from_instance(self._interaction_stages, self._interaction_type_attr, instance)


    def post_(self, model, _id=None, object_dict=None, add_dict=True, **kwargs):
        if object_dict is None: object_dict = {}
        _dict = getattr(self, '_' + model + '_dict')
        _matcher = getattr(self, '_' + model + '_matcher')
        _types = getattr(self, '_' + model + '_types')
        if _id is not None:
            _dict.ids_to_dict(_id, object_dict)
        if add_dict:
            object_dict = _dict.post(object_dict, internal = True)
            _id = _dict.dict_to_ids(object_dict)
        else:
            _id = _dict.dict_to_ids(object_dict)
            object_dict = _dict.get(_id, internal = True)
        st = hp.time()
        for k, v in _dict._default_weights.iteritems():
            if k not in _dict.ids and k in object_dict and v == 0:
                object_dict.pop(k, None)
        _matcher.cap(object_dict, instance_types = _types)
        logger.info("Time to cap: %s", hp.time() - st)
        return hp.make_single(_id)
    
    @RunApi.via_request
    def delete_(
            self, model, _id=None, object_dict=None, delete_dict=True, **kwargs
        ):
        if object_dict is None: object_dict = {}
        _dict = getattr(self, '_' + model + '_dict')
        _matcher = getattr(self, '_' + model + '_matcher')
        _aggr_matcher = getattr(self, '_' + model + '_aggr_matcher')
        _interact_matcher = getattr(self, '_' + model + '_interact_matcher')
        _types = getattr(self, '_' + model + '_types')
        if _id is None:
            _id = _dict.dict_to_ids(object_dict)
        if delete_dict:
            object_dict = _dict.delete(_id)
        _matcher.delete(_id)
        _aggr_matcher.delete(_id)
        _interact_matcher.delete(_id)
        return hp.make_single(_id)


    @RunApi.via_request
    def post_customer(self, user_id=None, object_dict=None, add_dict=True, **kwargs):
        return self.post_('user', _id = user_id, object_dict = object_dict, add_dict = add_dict, **kwargs)
    
    @RunApi.via_request
    def delete_customer(
            self, _id=None, object_dict=None, delete_dict=True, **kwargs
        ):
        return self.delete_('user', _id = _id, object_dict = object_dict, delete_dict = delete_dict, **kwargs)

    @RunApi.via_request
    def post_store(self, store_id=None, object_dict=None, add_dict=True, **kwargs):
        return self.post_('store', _id = store_id, object_dict = object_dict, add_dict = add_dict, **kwargs)
    
    @RunApi.via_request
    def delete_store(
            self, _id=None, object_dict=None, delete_dict=True, **kwargs
        ):
        return self.delete_('store', _id = _id, object_dict = object_dict, delete_dict = delete_dict, **kwargs)


    @RunApi.via_request
    def post_product(self, product_id=None, object_dict=None, add_dict=True, **kwargs):
        return self.post_('product', _id = product_id, object_dict = object_dict, add_dict = add_dict, **kwargs)
    
    @RunApi.via_request
    def delete_product(
            self, _id=None, object_dict=None, delete_dict=True, **kwargs
        ):
        return self.delete_('product', _id = _id, object_dict = object_dict, delete_dict = delete_dict, **kwargs)

    @RunApi.via_request
    def post_transfer(self, user_id1, user_id2, delete_dict = True, update_attributes = None, **kwargs):
        if update_attributes is None: update_attributes = []
        user_instance1 = self._user_dict.get(user_id1)
        if not user_instance1:
            raise UserNotAdded(
                '{} with id {} was not found'.format(self._user_dict.name.title(), user_id1), 
                log=False
            )
        user_instance2 = self._user_dict.get(user_id2)
        if not user_instance2:
            raise UserNotAdded(
                '{} with id {} was not found'.format(self._user_dict.name.title(), user_id2), 
                log=False
            )
        if user_id1 == user_id2:
            return user_instance1
        self._user_aggr_matcher.transfer(user_id1, user_id2)
        self._user_interact_matcher.transfer(user_id1, user_id2)
        self._user_matcher.delete(user_instance1)
        if delete_dict:
            for mn in self._user_dict.children:
                md = Model(mn, self.enterprise_id)
                fil = md.from_parent_id_dict(self._user_dict.name, user_instance1)
                ins = md.from_parent_id_dict(self._user_dict.name, user_instance2)
                logger.info("Updating child %s, filter by: %s, with ins %s", mn, fil, ins)
                md.update_many(ins, fil)
            for mn, md in self._user_dict.parents.iteritems():
                fil = self._user_dict.to_parent_id_dict(mn, user_instance1)
                ins = self._user_dict.to_parent_id_dict(mn, user_instance2)
                logger.info("Updating parent %s, filter by: %s, with ins %s", mn, fil, ins)
                md.update_many(ins, fil)
            self._user_dict.update(user_id2, {k: user_instance1.get(k) for k in hp.make_list(update_attributes)})
            self._user_dict.delete(user_id1)
        return user_instance2

    post_transfer_session = post_transfer

    @RunApi.via_request
    def get_preference(
            self, 
            _id,
            model_type = 'user',
        ):
        model_type = 'user' if model_type in ['user', 'customer'] else 'product'
        display = getattr(self, '_' + model_type + '_aggr_matcher').display(_id)
        return_dict = {}
        for kv, score in sorted(display.iteritems(), key = itemgetter(1), reverse = True):
            if score <= 0:
                continue
            k, v = kv.split('=')
            if k not in return_dict:
                return_dict[k] = OrderedDict()
            return_dict[k][v] = score
        return return_dict


    @RunApi.via_request
    def post_preference(
            self, 
            _id = None,
            _interaction_stage = None,
            _positivity_score = 0.1,
            _model_type = 'user',
            **object_dict
        ):
        model_type = 'user' if _model_type in ['user', 'customer'] else 'product'
        _dict = self._user_dict if model_type in ['user'] else self._product_dict
        _other_dict = self._product_dict if model_type in ['user'] else self._user_dict
        _id = hp.make_tuple(
            _id or self._interactions_dict.get_parent_ids(
                _dict.name, input_dict=object_dict
            )
        )
        positivity_score = self._get_positivity_score(_interaction_stage, object_dict, _positivity_score)
        instance = _dict.get(_id)
        if not instance:
            if model_type == 'user':
                raise UserNotAdded(
                    'Customer with id {} was not found'.format(_id), 
                    log=False
                )
            else:
                raise ProductNotAdded(
                    'Product with id {} was not found'.format(_id), 
                    log=False
                )
        for a in _dict.ids:
            object_dict.pop(a, None)
        logger.debug("Object_dict before filter is %s", object_dict)
        filter_by = {}
        _  = _other_dict._manage_params(_managed_params = filter_by, **object_dict)
        filter_by = filter_by['filter_by']
        logger.debug("In adding preference, filter_by dict before ids: %s", filter_by)
        filter_by = _dict.ids_to_dict(_id, filter_by)
        logger.debug("In adding preference, filter_by dict after ids: %s", filter_by)
        getattr(self, '_' + model_type + '_aggr_matcher').add(filter_by, positivity_score, instance_types = getattr(self, '_' + model_type + '_types'))
        return filter_by

    @RunApi.via_request
    def delete_interaction(
            self,
            user_id = None,
            product_id = None,
            store_id = None,
            interaction_type = None,
            object_dict = None,
            delete_dict = True,
            **kwargs
        ):
        return self.post_interaction(
            user_id = user_id, 
            product_id = product_id,
            store_id = store_id,
            interaction_type = interaction_type,
            object_dict = object_dict,
            delete_dict = delete_dict,
            as_delete = True,
            add_dict = False
        )

    @RunApi.via_request
    def post_interaction(self,
                         user_id=None,
                         product_id=None,
                         store_id=None,
                         interaction_type=None,
                         object_dict=None,
                         add_dict=True, as_delete = False, delete_dict = True):
        user_id = hp.make_tuple(
            user_id or self._interactions_dict.get_parent_ids(
                self._user_dict.name, input_dict=object_dict))
        product_id = hp.make_tuple(
            product_id or self._interactions_dict.get_parent_ids(
                self._product_dict.name, input_dict=object_dict))
        if self._store_dict: 
            store_id = hp.make_tuple(
                store_id or self._interactions_dict.get_parent_ids(
                    self._store_dict.name, input_dict=object_dict
                )
            )
        else:
            store_id = None
        if isinstance(store_id, (tuple, list)) and all(_ is None for _ in store_id):
            store_id = None
        interaction_type = interaction_type or object_dict.get(self._interaction_type_attr)
        quantities = self._get_quantities(object_dict)
        positivity_score = self._get_positivity_score(interaction_type, object_dict)
        if as_delete:
            positivity_score = - positivity_score
        user_instance = self._user_dict.get(user_id, internal = True)
        if not user_instance:
            raise UserNotAdded(
                'Customer with id {} was not found'.format(user_id), 
                log=False
            )
        product_instance = self._product_dict.get(product_id, internal = True)
        if not product_instance:
            raise ProductNotAdded(
                'Product with id {} was not found'.format(user_id), 
                log=False
            )
        if self._store_dict and store_id:
            store_instance = self._store_dict.get(store_id, internal = True)
            if not store_instance:
                raise UserNotAdded(
                    'Store with id {} was not found'.format(store_id), 
                    log=False
                )
        else:
            store_instance = {}
        if not interaction_type in self._interaction_stages:
            raise StageNotAdded(
                'interaction stage {} was not found for enterprise {}'.format(
                    interaction_type, self.enterprise_id
                )
            )
        self._interactions_dict.parent_ids_to_dict(
                self._user_dict.name,
                user_id, object_dict
        )
        self._interactions_dict.parent_ids_to_dict(
                self._product_dict.name,
                product_id, object_dict
        )
        if self._store_dict:
            self._interactions_dict.parent_ids_to_dict(
                    self._store_dict.name,
                    store_id, object_dict
            )
        object_dict.update(quantities)
        object_dict.update(self._get_time_attributes(object_dict))
        if add_dict:
            object_dict = self._interactions_dict.post(object_dict, internal = True)
            interaction_id = self._interactions_dict.dict_to_ids(object_dict)
        elif delete_dict and as_delete:
            interaction_id = self._interactions_dict.dict_to_ids(object_dict)
            self._interactions_dict.delete(interaction_id)
        else:
            interaction_id = self._interactions_dict.dict_to_ids(object_dict)
            if not as_delete:
                object_dict = self._interactions_dict.get(interaction_id, internal = True)

        # Add to user_interact_matcher        
        temp_dict = self._interactions_dict.parent_ids_to_dict(
            self._user_dict.name, user_id, in_self=False)
        temp_dict.update({
            interaction_type: '__'.join(hp.make_list(map(unicode, product_id))),
        })
        temp_dict.update(self._get_time_attributes(object_dict))
        self._user_interact_matcher.add(
                temp_dict, positivity_score, 
                timestamp_attr = self._interaction_stages[interaction_type].get('interaction_time_attribute', True),
        )
        # Add to product_interact_matcher
        temp_dict = self._interactions_dict.parent_ids_to_dict(
            self._product_dict.name, product_id, in_self=False
        )
        temp_dict.update({interaction_type: '__'.join(hp.make_list(map(unicode, user_id)))})
        temp_dict.update(self._get_time_attributes(object_dict))
        self._product_interact_matcher.add(
                temp_dict, positivity_score,
                timestamp_attr = self._interaction_stages[interaction_type].get('interaction_time_attribute', True),
        )
        # Add to store_interact_matcher
        if self._store_dict and store_id:
            temp_dict = self._interactions_dict.parent_ids_to_dict(
                self._store_dict.name, store_id, in_self=False)
            temp_dict.update({
                interaction_type: '__'.join(hp.make_list(map(unicode, product_id))),
            })
            temp_dict.update(self._get_time_attributes(object_dict))
            self._store_interact_matcher.add(
                    temp_dict, positivity_score, 
                    timestamp_attr = self._interaction_stages[interaction_type].get('interaction_time_attribute', True),
            )
        # Remove all ids from the interact dict
        for k in (
                  self._interactions_dict.ids + \
                  self._interactions_dict.get_parent_ids(
                                      self._user_dict.name) +\
                  self._interactions_dict.get_parent_ids(
                                  self._product_dict.name)
            ):
            object_dict.pop(k, None)
        # adding to product_aggr_matcher
        self._interactions_dict.parent_ids_to_dict(
            self._product_dict.name, product_id, user_instance, in_self=False)
        user_instance.update((k, object_dict[k]) \
            for k in self._interaction_stages[interaction_type][
                                    'customer_attributes'] if k in object_dict)
        user_instance.update((k, object_dict[k]) \
            for k in self._interaction_stages[interaction_type].get(
                                    'store_attributes', []) if k in object_dict)
        user_instance.update(store_instance)
        (user_instance.pop(_, None) for _ in self._user_dict.ids)
        t = self._user_types.copy()
        if self._store_dict:
            (user_instance.pop(_, None) for _ in self._store_dict.ids)
            t.update(self._store_types)
        self._product_aggr_matcher.add(user_instance, positivity_score, instance_types = t)
        # adding to store pref matcher
        product_instance.update((k, object_dict[k]) \
            for k in self._interaction_stages[interaction_type][
                                    'product_attributes'] if k in object_dict)
        # adding to user pref matcher
        self._interactions_dict.parent_ids_to_dict(
            self._user_dict.name, user_id, product_instance, in_self=False)
        (product_instance.pop(_, None) for _ in self._product_dict.ids)
        self._user_aggr_matcher.add(product_instance, positivity_score, instance_types = self._product_types)
        if self._store_dict and store_id:
            self._interactions_dict.parent_ids_to_dict(
                self._store_dict.name, store_id, product_instance, in_self=False)
            self._store_aggr_matcher.add(product_instance, positivity_score, instance_types = self._product_types)
        return_dict = {
            'interaction_stage': interaction_type,
            'positivity_score': positivity_score
        }
        return_dict.update(quantities)
        self._interactions_dict.parent_ids_to_dict(self._user_dict.name,
                                                   user_id, return_dict)
        self._interactions_dict.parent_ids_to_dict(self._product_dict.name,
                                                   product_id, return_dict)
        if self._store_dict:
            self._interactions_dict.parent_ids_to_dict(self._store_dict.name,
                                                       store_id, return_dict)
        self._interactions_dict.ids_to_dict(interaction_id, return_dict)
        return return_dict

    def _iter_compare(self,
                      model,
                      id_or_dict,
                      filter_list=None,
                      not_keys=False,
                      limit=10,
                      update_factor=None,
                      matcher_list=None,
                      attribute_weights=None,
                      **filters):
        limit = int(limit)
        model_dict = getattr(self, '_' + model + '_dict')
        if not isinstance(id_or_dict, dict):
            _dict = model_dict.get(id_or_dict, internal = True)
            if not _dict:
                raise InvalidId("Id {} not found in model {}".format(
                    id_or_dict, model_dict.name))
        if matcher_list is None:
            matcher_list = [
                getattr(self, '_' + model + m)
                for m in ['_matcher', '_aggr_matcher', '_interact_matcher']
            ]
        matcher_list = hp.make_list(matcher_list)
        if isinstance(filter_list, list) and not filter_list:
            raise StopIteration
        filter_list = self._make_filter_list(hp.make_list(filter_list or []), target_size = len(model_dict.ids))
        if filters:
            filters = self._filter_list_to_filters(model_dict, filter_list, not_keys = not_keys, filters = filters)
            filter_list = self._make_filter_list(model_dict.filter(_filter_attributes = model_dict.ids, _internal = True, **filters), model_dict.dict_to_ids)
            not_keys = False
        max_score = {}
        logger.info("Filter_list before map: %s, not_keys: %s", filter_list, not_keys)
        if not len(filter_list) or len(filter_list[0]) > limit or not_keys:
            old_filter_list = copyof(filter_list)
            key_dict = {}
            for m in matcher_list:
                w = self._matcher_weights_dict.get(m.name, 1.)
                max_score[m.name] = 0
                def max_score_adder(sc):
                    if sc > max_score[m.name]:
                        max_score[m.name] = sc
                logger.debug("In gen similar for matcher %s: %s", m.name, id_or_dict)
                for i, scores in m.gen_filter_similar(
                        id_or_dict,
                        filter_keys=filter_list,
                        not_keys = not_keys,
                        get_score = True,
                        max_score_adder = max_score_adder,
                        limit = (len(matcher_list)*limit)
                    ):
                    logger.debug("IN gen similar for matcher (dict) %s: %s, %s", m.name,  i, scores)
                    key_dict[i] = key_dict.get(i, 0) + (scores * w);
            logger.debug("After gen_filter_similar, dict: %s", key_dict)
            new_filter_list = []
            i1 = -1
            last_k = None
            for k in sorted(
                key_dict.iterkeys(), key=lambda x: key_dict[x],
                reverse=True):
                i1 += 1
                logger.debug("IN gen similar for matcher %s: %s, %s, %s", m.name, i1, k, key_dict[k])
                if i1 < (len(matcher_list) * limit):
                    new_filter_list.append(hp.make_list(k))
                    last_k = k
                    continue
                if key_dict[k] >= key_dict[last_k]:
                    new_filter_list.append(hp.make_list(k))
                if key_dict[k] < key_dict[last_k]:
                    break
            filter_list = self._make_filter_list(new_filter_list)
            if not filter_list:
                filter_list = old_filter_list
        logger.info("Filter_list after first iteration: %s", filter_list)
        logger.info("Matcher_list: %s", [m.name for m in matcher_list])
        key_dict = {}
        denom = 1e-16

        attribute_weights = attribute_weights or getattr(self, '_' + model + '_dict').get_weights()
        for m in matcher_list:
            w = self._matcher_weights_dict.get(m.name, 1.)
            for i, scores in m.iter_distances(
                    id_or_dict,
                    filter_keys=filter_list,
                    attribute_weights= attribute_weights,
                    max_distance = 2 * (
                        max_score.get(
                            m.name) or (
                                len(id_or_dict) if isinstance(id_or_dict, dict) else 1.
                            )
                        ),
                    update_factor=update_factor, 
                ):
                logger.debug("After iter distances in matcher %s: %s, scores: %s", m.name, i, scores)
                i = hp.make_single(i, tuple)
                key_dict[i] = key_dict.get(i, 0.) + (w * scores)
            denom += w 
        matcher_list_names = [m.name for m in matcher_list]
        logger.debug("Dict for matchers %s: %s", matcher_list_names, id_or_dict)
        for k in sorted(
                key_dict, key=lambda k: key_dict[k], reverse=True
            )[:limit]:
            logger.debug("After sorting for %s: %s, %s", matcher_list_names, k, key_dict[k] / denom)
            yield k, key_dict[k] / denom

    #@t.timeit
    @RunApi.via_request
    def get_similar_customers(self,
                          user_id,
                          **kwargs):
        """
        ids can be a list or tuple if the model has more than one id
        filter_list is a list of objects of the model among which we need the
        most similar interact_filter and a list of interactions that have made 
        with certain products/users, 
        e.g.
        {
         'purchased': ['id1', 'id2'],
         'liked': ['id1', 'id2']
        }
        if the product_id is a compound key, then 
        {
          'purchased': ['idA__idB1', 'idA__idB2']
        }
        """
        filter_list = kwargs.pop('_filter_list', None) or kwargs.pop('filter_list', None)
        not_keys = kwargs.pop('_not_keys', None)
        if not_keys is None: not_keys = kwargs.pop('not_keys', None)
        limit = kwargs.pop('_limit', None) or kwargs.pop('limit', 10)
        num_similar = kwargs.pop('_num_similar', None) or kwargs.pop('num_similar', None)
        update_factor = kwargs.pop('_update_factor', None) or kwargs.pop('update_factor', None)
        internal = kwargs.pop('_internal', None) or kwargs.pop('internal', None)
        interact_filters = kwargs
        output_list = []
        factor_updater = FactorUpdater(self._user_dict, self._product_dict, user_id)
        if num_similar and not update_factor:
            update_factor = factor_updater.update_factor
        for similar_id, score in self._iter_compare(
                'user', user_id, filter_list, not_keys, limit, update_factor,
                **interact_filters):
            if internal:
                yield similar_id, score
            else:
                odict = self._user_dict.ids_to_dict(similar_id)
                odict['score'] = score
                odict['customer_attributes'] = self._user_dict.get(similar_id)
                if num_similar:
                    odict.update(factor_updater.limit(hp.make_tuple(similar_id), num_similar))
                output_list.append(odict)
        if not internal:
            for i, v in zip(self._user_dict.ids, hp.make_list(user_id)):
                yield i, v
            yield 'similar_customers', output_list

    get_similar_users = get_similar_customers


    def _do_pagination(self, recommendation_obj, _page_number, _limit, _total_number = None):
        _total_number = _total_number if _total_number is not None else recommendation_obj.get('total_number', max(_page_number, 5) * _limit)
        recommendation_obj.update({
            'is_first': _page_number <= 1,
            'is_last': _page_number * _limit >= _total_number,
            'page_size': _limit,
            'page_number': _page_number,
            'total_number': _total_number
        })

    def _make_cache_id(self, product_id, reco, _output_name, _page_number, _limit):
        cd = reco.copy()
        cd.pop(_output_name)
        cd['page_size'] = _limit
        cd['page_number'] = _page_number
        return self._cacher.make_id(
            product_id,
            cd,
            _output_name,
        )
        

    #@t.timeit
    @RunApi.via_request
    def get_similar_products(self,
                             product_id = None,
                             **kwargs):
        _output_name = 'similar_products'
        _internal = kwargs.pop('_internal', None) or kwargs.pop('internal', None)
        _recommendation_id = kwargs.pop('_recommendation_id', None)
        _fresh = kwargs.pop('_fresh', False)
        _freshen = kwargs.pop('_freshen', True)
        _limit = int(kwargs.pop('_page_size', None) or kwargs.pop('_limit', None) or kwargs.pop('limit', 10))
        _page_number = int(kwargs.pop('_page_number', 1))
        _update_factor = None
        if _recommendation_id:
            reco = self._recommendation_model.get(_recommendation_id)
            if reco:
                self._do_pagination(reco, _page_number, _limit)
                if _page_number * _limit - 1 <= len(reco[_output_name]):
                    reco[_output_name] = reco[_output_name][(_page_number - 1) * _limit: _page_number * _limit]
                    for k, v in reco.iteritems():
                        yield k, v
                    raise StopIteration
                reco['filters'].update({'{}~'.format(self._product_dict.ids[0]): [d_[self._product_dict.ids[0]] for d_ in reco[_output_name]]})
        else:
            reco = {'product_id': product_id, 'filters': {}, _output_name: []}
        for k, v in kwargs.iteritems():
            if k in ['_update_factor', 'update_factor']: 
                _update_factor = kwargs[k]
            elif k.startswith('_'):
                k1 = k[1:]
                reco[k1] = reco.get(k1) or kwargs[k]
            elif k in ['limit', 'num_similar', 'not_keys', 'filter_list']:
                reco[k] = reco.get(k) or kwargs[k]
            else:
                reco['filters'][k] = kwargs[k]
        if reco.get('instance'):
            product_id = dict(self._product_matcher.obj_to_instance(reco['instance'], self._product_types))
        if product_id is None:
            raise InvalidId("Unknown id or instance")
        if _internal:
            _recommendation_id = None
        output_list = reco[_output_name]
        cache_id = self._make_cache_id(product_id, reco, _output_name, _page_number, _limit)
        reco['recommendation_id'] = _recommendation_id or cache_id
        if not _fresh and not _internal:
            response = self._cacher.get(cache_id)
            if response:
                logger.info("Rending recommendation response from cacher")
                for i, v in response.iteritems():
                    yield i, v
                raise StopIteration
        if not _recommendation_id:
            reco['total_number'] = self._product_dict.count(**reco.get('filters', {}))
        self._do_pagination(reco, _page_number, _limit)
        factor_updater = FactorUpdater(
            self._user_dict,
            self._product_dict,
            user_id=None,
            product_id=product_id)
        if reco.get('num_similar') and not _update_factor:
            _update_factor = factor_updater._update_factor
        recommendation_number = (_page_number - 1) * _limit
        for similar_id, score in self._iter_compare(
                'product', product_id, reco.get('filter_list'), reco.get('not_keys'), _limit,
                matcher_list = [self._product_matcher] if isinstance(product_id, dict) else None,
                update_factor = _update_factor, **reco.get('filters', {})
            ):
            if _internal:
                yield similar_id, score
            else:
                odict = self._product_dict.ids_to_dict(similar_id)
                odict['score'] = score
                odict['recommendation_number'] = recommendation_number
                recommendation_number += 1
                odict['product_attributes'] = self._product_dict.get(similar_id)
                if reco.get('num_similar'):
                    odict.update(factor_updater.limit(similar_id, reco.get('num_similar')))
                output_list.append(odict)
        if not _internal:
            reco[_output_name] = output_list
            for k, v in reco.iteritems():
                yield k, v
            self._recommendation_model.post(reco)
            if _freshen:
                logger.info("Saving recommendation for cacher id: %s", cache_id)
                self._cacher.set(cache_id, reco, shelf_life = _freshen if (
                    isinstance(_freshen, (float, int, long)) and not isinstance(_freshen, bool)
                    ) else 600
                )

    #@t.timeit
    @RunApi.via_request
    def get_influencers(self,
                        user_id,
                        product_id=None,
                        _limit=10,
                        _filter_list=None,
                        _num_similar=None,
                        _internal=False,
                        **user_filters):
        """
        Gets the user_ids which are closest to a particular user_id
        return [
                    {
                        "user_id": name of id attribute from customer model
                        "score": max - d/ max - min
                        "user_similarity" : {
                            "attribute1": "value1"
                            .
                            .
                        },
                        "product_preference": {
                            "interaction_type1": ['attribute1', 'attribute2',]
                            .
                            .
                        },
                        "common_interactions": {
                            "interaction_type1": ['product_id1', product_id2,]
                        },
                        "review": ....
                    },
                    .
                    .
                ]
        """
        limit = int(user_filters.pop('limit', None) or _limit)
        num_similar = user_filters.pop('num_similar', None) or _num_similar
        if num_similar: num_similar = int(num_similar)
        user_dict = self._user_dict.get(user_id, internal = True)
        if not user_dict:
            raise InvalidId("Id {} not found in model {}".format(
                user_id, self._user_dict.name))
        filter_list = self._make_filter_list(hp.make_list(_filter_list or []))
        logger.debug("filter_list initially in get_influencers: %s", filter_list)
        if user_filters:
            filter_list = self._make_filter_list(self._user_dict.filter(_filter_attributes = self._user_dict.ids, _internal = True, **user_filters), self._user_dict.dict_to_ids, filter_list)
            logger.debug("filter_list after applying user_filters: %s", filter_list)
        interact_filter = {}
        if product_id:
            interact_filter = dict((s, '__'.join(product_id)) for s in self._experience_stages) 
            logger.debug("interact filter: %s", interact_filter)
            interact_gen = self._user_interact_matcher.iterkeys(
                filter_by = interact_filter,
                filter_keys = filter_list,
            )
            filter_list = self._make_filter_list(interact_gen)
        output = []
        factor_updater = FactorUpdater(self._user_dict, self._product_dict,
                                       user_id, product_id)
        for output_id, score in self.get_similar_customers(
                user_id,
                _filter_list=filter_list,
                _limit=limit,
                _update_factor=factor_updater.update_factor,
                _internal=True):
            if _internal:
                yield output_id, score
            else:
                r = {}
                for i, v in zip(self._user_dict.ids, hp.make_list(output_id)):
                    r.update({i: v})
                r.update({'score': score})
                r.update({
                    'customer_attributes': self._user_dict.get(output_id)
                })
                if num_similar:
                    r.update(factor_updater.limit(output_id, num_similar))
                output.append(r)
        if not _internal:
            for i, v in zip(self._user_dict.ids, hp.make_list(user_id)):
                yield i, v
            if product_id is not None:
                for i, v in zip(self._product_dict.ids,
                                hp.make_list(product_id)):
                    yield i, v
            yield 'influencers', output

    def add_recommendation(recommendation_id, model_name, matcher_name, score):
        self._recos.append(dict(zip(["recommendation_id", "model_name", "matcher_name", " score"], [recommendation_id, model_name, matcher_name, score])))

    time_resolutions = {
        'second': 1,
        'seconds': 1,
        'every second': 1,
        'minute': 60,
        'minutes': 60,
        'every minute': 60,
        'hour': 3600,
        'hours': 3600,
        'hourly': 3600,
        'day': 3600*24,
        'daily': 3600*24,
        'week': 3600*24*7,
        'weekly': 3600*24*7,
        'month': 3600*24*30,
        'monthly': 3600*24*30,
        'year': 3600*24*365,
        'yearly': 3600*24*365,
        'decade': 3600*24*3650,
        'every decade': 3600*24*3650,
        'days': 3600*24,
        'weeks': 3600*24*7,
        'months': 3600*24*30,
        'years': 3600*24*365,
        'decades': 3600*24*3650,
    }
    
    def _datetime_split(self, value, value_type = None):
        if not value_type: value_type = type(value)
        if not isinstance(value, (datetime, date, dtime)):
            logger.warning("Trying to split non-datetime value : %s", value)
            raise StopIteration
        elif issubclass(value_type, datetime):
            yield 'timestamp', time.mktime(value.timetuple())
            yield 'hour', (str(value.hour), 0.0)
            yield 'hour_quart', (str(6*value.hour/6), 0.0)
            yield 'day', (str(value.isoweekday()), 0.0)
            yield 'month', (str(value.month), 0.0)
            yield 'year', (str(value.year), 0.0)
            yield 'decade', (str(10*(value.year/10)), 0.0)
        elif issubclass(value_type, date):
            yield 'timestamp', time.mktime(value.timetuple())
            yield 'day', (str(value.isoweekday()), 0.0)
            yield 'month', (str(value.month), 0.0)
            yield 'year', (str(value.year), 0.0)
            yield 'decade', (str(10*(value.year/10)), 0.0)
        elif issubclass(value_type, dtime):
            yield 'hour', (str(value.hour), 0.0)
            yield 'hour_quart', (str(6*value.hour/6), 0.0)

    def datetime_diff(self, date1, date2, resolution = None, reference_date = None):
        if type(date1) != type(date2):
            raise TypeError("Date 1 ({}) and Date 2 ({}) are not of the same type".format(date1, date2))
        date1 = hp.to_datetime(date1)
        date2 = hp.to_datetime(date2)
        if all(a == 0 for a in [date1.hour, date1.minute, date1.second, date2.second, date2.hour, date2.minute]):
            date1 = date1.date()
            date2 = date2.date()
        dict1 = dict(self._datetime_split(date1))
        if not resolution and reference_date:
            resolution = max(
                abs((date1 - reference_date).total_seconds()), 
                abs((date1 - date2).total_seconds()), 
                abs((date2 - reference_date).total_seconds())
            )
        diff = 0
        maxp = 0.
        for key, value in self._datetime_split(date2):
            if key == 'timestamp':
                if resolution:
                    diff += min(abs((value - dict1[key]))/(resolution*10), 1.)
            else:
                if isinstance(value, (str, unicode)):
                    diff += (value[1] if value[0] == dict1[key][0] else 1.)
            maxp += 1
        logger.debug('Diff is %s, date1: %s, date2: %s, Maxp: %s', diff, date1, date2, maxp)
        return (maxp - diff)/maxp
            


    def get_quantity_predictions(
            self, model_id, other_id, model = 'product', time_period = None, stages = None, 
            now = None, until = None, rounder = None, limit = 5, filter_attributes = None,
            details = True, _fresh = False, _freshen = True, **kwargs
        ):
        """
        model_id would be the product_id
        other_id would typically be the user/customer_id
        """
        if model in ['customer', 'user']:
            model_dict = self._user_dict
            other_dict = self._product_dict
            other = 'product'
        elif model == 'store':
            model_dict = self._store_dict
            other_dict = self._product_dict
            other = 'product'
        else:
            model_dict = self._product_dict
            other_dict = self._user_dict
            other = 'user'
        resolution = None
        cache_id = self._cacher.make_id(
            [model_id, other_id, model], 
            {
                'time_period':  time_period, 
                'stages': stages, 
                'now': now, 
                'until': until, 
                'rounder': rounder, 
                'limit': limit
            },
            'quantity_predict'
        )
        if not _fresh:
            response = self._cacher.get(cache_id)
            if response:
                logger.info("Rending get quantity response from cacher")
                return response
        if stages:
            if stages == True:
                stages = self._interaction_stages.keys()
            else:
                stages = hp.make_list(stages)
        if isinstance(rounder, (unicode, str)):
            if rounder == 'ceil':
                rounder = np.ceil
            elif rounder == 'round':
                rounder = np.round
            elif rounder == 'floor':
                rounder = np.floor
            else:
                rounder = lambda x: np.round(100.*x)/100.
        else:
            rounder = lambda x: x
        if time_period:
            #  time_period provided in seconds
            try:
                resolution = int(time_period)
            except ValueError:
                resolution = self.time_resolutions.get(time_period)
            if isinstance(now, (str, unicode)):
                now = hp.to_datetime(now)
            else:
                now = datetime.now()
            if not until:
                until = now + timedelta(seconds = resolution)
            if isinstance(until, (str, unicode)):
                until = hp.to_datetime(until)
        filters = self._interactions_dict.parent_ids_to_dict(other_dict.name, other_id)
        filters = self._interactions_dict.parent_ids_to_dict(model_dict.name, model_id, filters)
        weights_models = {}
        weights_other = {}
        min_m = 1e32
        max_m = -1e32
        limit = int(limit)
        if limit:
            for m, d, id_, w in [(model, model_dict, model_id, weights_models), (other, other_dict, other_id, weights_other)]:
                for similar_id, score in getattr(self, 'get_similar_' + m + 's')(id_, _limit = limit, _internal = True):
                    if min_m >= score: min_m = score
                    if max_m <= score: max_m = score
                    w[hp.make_tuple(similar_id)] = score
                    f = self._interactions_dict.parent_ids_to_dict(d.name, similar_id)
                    for k, v in f.iteritems():
                        filters[k] = hp.make_list(filters.get(k,[])) + [v]
        weights_models.update({hp.make_tuple(model_id): max(max_m, 1.)})
        weights_other.update({hp.make_tuple(other_id): max(max_m, 1.)})
        self_ids = (hp.make_tuple(model_id), hp.make_tuple(other_id))
        max_m = max(max_m, 1.)
        logger.debug("Min weight for neighbors: %s", min_m)
        logger.debug("Max weight for neighbors: %s", max_m)
        logger.debug("Weights models: %s\n Weights other: %s", weights_models, weights_other)
        output_dict = {}
        for stage, attr in self._quantity_stages():
            if stages and stage not in stages:
                continue
            f = {self._interaction_type_attr:  stage}
            f.update(filters)
            if stage not in output_dict:
                output_dict[stage] = {'__insight__': {}}
            od = output_dict[stage]
            odd = output_dict[stage]['__insight__']
            for a in attr:
                if a not in od:
                    od[a] = 0.
                    odd[a] = "{} that are similar (same {}) {} at least a {} of %s units".format(
                        hp.inflect.plural(self._store_dict.title if model == 'store' else self._user_dict.title).title(),
                        (self._store_dict if model == 'store' else self._user_dict).get_max_weight(),
                        stage,
                        self._interactions_dict.attributes[a].title
                    )
            sums = {}
            weights = {}
            tweights = {}
            all_combs = {}
            for interaction in self._interactions_dict.filter(_sort_by = self._time_attr_stage(stage), _sort_reverse = True, _internal = True, **f):
                ts = hp.to_datetime(interaction[self._time_attr_stage(stage)])
                mids = tuple(self._interactions_dict.parent_dict_to_ids(model_dict.name, interaction))
                oids = tuple(self._interactions_dict.parent_dict_to_ids(other_dict.name, interaction))
                all_combs[(mids, oids)] = None
                if resolution:
                    tslot = ((now - ts).total_seconds()) // int(resolution)
                else:
                    tslot = None
                if tslot not in tweights:
                    tweights[tslot] = {}
                    weights[tslot] = {}
                if resolution:
                    _now = now
                    while _now < until:
                        tdiff = self.datetime_diff(ts, _now, resolution = resolution)
                        logger.debug("Mids: %s, Oids: %s, Interaction type: %s, ts: %s, _now: %s, tdiff: %s, tslot: %s", mids, oids, stage, ts, _now, tdiff, tslot )
                        tweights[tslot][(mids, oids)] = tweights[tslot].get((mids, oids), []) + [tdiff]
                        _now += timedelta(seconds = resolution)
                else:
                    tweights[tslot][(mids, oids)] = [1.]
                for a in attr:
                    if filter_attributes and a not in filter_attributes:
                        continue
                    logger.debug("Mids: %s, Oids: %s, Interaction type: %s, quantity for %s: %s", mids, oids, stage, a, interaction.get(a, 0))
                    if a not in sums:
                        sums[a] = {}
                    if tslot not in sums[a]:
                        sums[a][tslot] = {}
                    sums[a][tslot][(mids, oids)] = sums[a][tslot].get((mids, oids), 0.) + interaction.get(a, 0.)
                if (mids, oids) not in weights.get(tslot, {}):
                    logger.debug("weight model: %s, weight other: %s", weights_models.get(hp.make_tuple(mids), min_m), weights_other.get(hp.make_tuple(oids), min_m))
                    logger.debug("mids: %s, oids: %s", mids, oids)
                    logger.debug("max_m: %s, min_m: %s", max_m, min_m)
                    mul1 = (weights_models.get(hp.make_tuple(mids), min_m) - min_m + 1e-6) / (max_m - min_m + 1e-6)
                    mul2 = (weights_other.get(hp.make_tuple(oids), min_m) - min_m + 1e-6) / (max_m - min_m + 1e-6)
                    logger.debug("mul1: %s, mul2: %s", mul1, mul2)
                    weights[tslot][(mids, oids)] = mul1 + mul2
            for a in attr:
                if filter_attributes and a not in filter_attributes:
                    continue
                denom1 = 1e-32
                for tslot in sums.get(a, {}).iterkeys():
                    denom2 = 0
                    s = 0
                    for mids, oids in all_combs.iterkeys():
                        if (mids, oids) not in weights[tslot]:
                            mul1 = (weights_models.get(hp.make_tuple(mids), min_m) - min_m + 1e-6) / (max_m - min_m + 1e-6)
                            mul2 = (weights_other.get(hp.make_tuple(oids), min_m) - min_m + 1e-6) / (max_m - min_m + 1e-6)
                            weights[tslot][(mids, oids)] = mul1 + mul2
                            if resolution:
                                _now = now
                                while _now < until:
                                    tdiff = self.datetime_diff(now - timedelta(seconds = tslot * resolution), _now, resolution = resolution)
                                    tweights[tslot][(mids, oids)] = tweights[tslot].get((mids, oids), []) + [tdiff]
                                    _now += timedelta(seconds = resolution)
                            else:
                                tweights[tslot][(mids, oids)] = [1.]
                        logger.debug(
                            "For stage %s, attr: %s, for mids: %s, oids: %s, tslot: %s, sums: %s, weights: %s, tweights: %s", 
                            stage, a, mids, oids, tslot, 
                            sums[a][tslot].get((mids, oids), 0.), weights[tslot][(mids, oids)], 
                            tweights[tslot][(mids, oids)]
                        )
                        for t in tweights[tslot][(mids, oids)]:
                            s = s + (sums[a][tslot].get((mids, oids),0.) * t)
                        denom2 += (np.mean(tweights[tslot][(mids, oids)]))
                    od[a] = od.get(a, 0.) + (weights[tslot][(mids, oids)]*s)
                    denom1 += weights[tslot][(mids, oids)] * denom2
                od[a] = rounder(od[a]/denom1)
                odd[a] = odd[a] % (od[a])
        if _freshen:
            logger.info("Saving prediction for cache_id: %s, freshen: %s", cache_id, _freshen)
            self._cacher.set(cache_id, output_dict, shelf_life = _freshen if (
                    isinstance(_freshen, (float, int, long)) and not isinstance(_freshen, bool)
                ) else 600
            )
        return output_dict

    def _filter_list_to_filters(self, model_dict, filter_list, not_keys = False, filters = None):
        if filters is None:
            filters = {}
        if filter_list is None:
            return filters
        if not not_keys and not (filter_list or filters):
            return None
        for _i, _ids in zip(model_dict.ids, filter_list):
            if not_keys:
                filters[_i + '~'] = _ids
            else:
                filters[_i] = _ids
        return filters

    def _make_filter_list(self, input_list, mapper = hp.make_list, previous_list = None, target_size = None):
        if not input_list:
            return previous_list or []
        if target_size and len(input_list) == target_size and all(isinstance(i, list) for i in input_list[:10]):
            mapped = input_list
        else:
            mapped = zip(
                *map(mapper, input_list)
            )
        if not previous_list: 
            previous_list = [[]]*len(mapped)
        if len(previous_list) != len(mapped):
            raise ValueError("Cannot concatenate two filter lists of size {} and {} (previous)".format(len(mapped), len(previous_list)))
        return [(list(i) + j) for i, j in zip(mapped, previous_list)]

    def _gap_to_seconds(self, gap):
        if gap is None:
            return None
        elif isinstance(gap, (int, float)):
            return gap
        elif gap == "every second":
            return 1
        elif gap == "every minute":
            return 60
        elif gap == "hourly":
            return 3600
        elif gap == "every 2 hours":
            return 2*3600
        elif gap == "every 3 hours":
            return 3*3600
        elif gap == "every 4 hours":
            return 4*3600
        elif gap == "every 6 hours":
            return 6*3600
        elif gap == "twice a day":
            return 12*3600
        elif gap == "daily":
            return 24*3600
        elif gap == "weekly":
            return 7*24*3600
        elif gap == "fortnightly":
            return 15*24*3600
        elif gap == "monthly":
            return 31*24*3600
        elif gap == "bimonthly":
            return 2*31*24*3600
        elif gap == "quarterly":
            return 3*31*24*3600
        elif gap == "half yearly":
            return 6*31*24*3600
        elif gap == "yearly":
            return 365*24*3600
        elif gap == "every decade":
            return 10*365*24*3600
        return None

    def _get_repeat_list(
                self,
                model,
                other,
                _id,
                now = None,
                filter_list = None,
                not_keys = None,
                limit = None
            ):
        time_attr = self._interaction_stages.get(self._get_lowest_experienced_stage(), {}).get('interaction_time_attribute')
        if now is None: now = datetime.now()
        if not time_attr:
            return filter_list, not_keys, now - timedelta(seconds = 30*24*3600)
        not_list = set()
        cutoff = now - timedelta(days = 365*10)
        for stage, stage_obj in self._interaction_stages.iteritems():
            repeat_gap = self._gap_to_seconds(stage_obj.get('time_period'))
            if not repeat_gap:
                repeat_gap = getattr(
                    self, '_' + other + '_interact_matcher'
                ).get_repeat_gap(_id, stage)
            logger.info("Repeat gap for stage %s: %s", stage, repeat_gap)
            if not repeat_gap:
                continue
            cutoff = max(cutoff, now - timedelta(seconds = repeat_gap))
            not_list.update(
                getattr(self, '_' + model + '_interact_matcher').iterkeys(
                    filter_by = getattr(self, '_' + model + '_interact_matcher')._iter_attrs(
                        {stage: '__'.join(hp.make_list(_id))}
                    ), 
                    updated_after = (now - timedelta(seconds = repeat_gap)),
                    now = now,
                )
            )
        not_list = self._make_filter_list(not_list)
        logger.debug("Not list : %s", not_list)
        if filter_list:
            if not_keys:
                filter_list = self._make_filter_list(not_list, previous_list = filter_list)
            else:
                fl = map(list,zip(*set(zip(*filter_list)) - set(zip(*not_list))))
                if not limit or (fl and len(fl[0]) >= limit):
                    filter_list = fl
                not_keys = False
        elif not_list and not_list[0]:
            filter_list = not_list
            not_keys = True
        return filter_list, not_keys, cutoff


    def _get_recommendations(
            self,
            model,
            other,
            output_name,
            _id,
            model_id=None,
            _limit=10,
            _filter_list=None,
            _influencers=None,
            _now = None,
            _num_similar=None,
            _raw_score_func = None,
            _quantity_predict = False,
            _filter_influencers = None,
            _sort_by = None,
            _internal = False,
            _freshen = True,
            _fresh = False,
            _additional_values = None,
            _recommendation_id = None,
            _page_size = None,
            _page_number = 1,
            _instance = None,
            _filter_map = None,
            **filters
        ):
        model_dict = getattr(self, '_' + model + '_dict')
        attrib_name = model_dict.name + '_attributes'
        other_dict = getattr(self, '_' + other + '_dict')
        odict = other_dict.get(_id, internal = True)
        if not odict:
            raise InvalidId(
                "Id {} not found in model {}".format(_id, other_dict.name)
            )
        _filter_map = hp.evaluate_args(_filter_map or self.st.enterprise.get('filter_map', {}), odict, force_none = True)
        filters.update(_filter_map)
        limit = _page_size or _limit
        limit = int(limit) if limit is not None else 10
        _page_number = int(_page_number) if isinstance(_page_number, (str, unicode, float, int, long)) else 1
        if _internal:
            _recommendation_id = None
        recommendation_obj = {
            'filter_list': _filter_list,
            'influencers': _influencers,
            'now': _now,
            'num_similar': _num_similar,
            'quantity_predict': _quantity_predict,
            'filter_influencers': _filter_influencers,
            'sort_by': _sort_by,
            'filters': filters,
            'additional_values': _additional_values,
            'recommendation_type': output_name,
            'total_number': limit - 1,
            output_name: []
        }
        for i, v in zip(other_dict.ids, hp.make_list(_id)):
            recommendation_obj.update({i: v})
        model_id = model_id or _instance
        if model_id is not None:
            if isinstance(model_id, dict):
                recommendation_obj.update(model_id)
            else:
                for i, v in zip(model_dict.ids, hp.make_list(model_id)):
                    recommendation_obj.update({i: v})
        if _recommendation_id:
            recommendation_obj = self._recommendation_model.get(_recommendation_id)
            if recommendation_obj:
                _filter_list = recommendation_obj.get('filter_list', _filter_list)
                _influencers = recommendation_obj.get('influencers', _influencers)
                _now = recommendation_obj.get('now', _now)
                _num_similar = recommendation_obj.get('num_similar', _num_similar)
                _quantity_predict = recommendation_obj.get('quantity_predict', _quantity_predict)
                _filter_influencers = recommendation_obj.get('filter_influencers', _filter_influencers)
                _sort_by = recommendation_obj.get('sort_by', _sort_by)
                filters = recommendation_obj.get('filters', filters)
                _additional_values = recommendation_obj.get('additional_values', _additional_values)
                if (_page_number * limit) <= len(recommendation_obj[output_name]):
                    recommendation_obj.update({
                        'is_first': _page_number <= 1,
                        'is_last': _page_number * limit >= recommendation_obj['total_number'],
                        'page_size': limit,
                        'page_number': _page_number
                    })
                    recommendation_obj[output_name] = recommendation_obj[output_name][(_page_number - 1) * limit: _page_number * limit]
                    for k, v in recommendation_obj.iteritems():
                        yield k, v
                    raise StopIteration
                filters.update({'{}~'.format(model_dict.ids[0]): [d_[model_dict.ids[0]] for d_ in recommendation_obj[output_name]]})
        output_list = recommendation_obj[output_name]
        cd = recommendation_obj.copy()
        for _ in [output_name, 'total_number']:
            cd.pop(_, None)
        cd['page_size'] = limit
        cd['page_number'] = _page_number
        cache_id = self._cacher.make_id(
            [model, other, _id, model_id, model_dict.role, model_dict.user_id],
            cd,
            output_name
        )
        recommendation_id = _recommendation_id or cache_id
        recommendation_obj['recommendation_id'] = recommendation_id
        if _filter_influencers is None: _filter_influencers = {}
        if not _fresh and not _internal:
            response = self._cacher.get(cache_id)
            if response:
                logger.info("Rending recommendation response from cacher")
                for i, v in response.iteritems():
                    yield i, v
                raise StopIteration
        influencers = int(_influencers) if _influencers is not None else None
        logger.info("In get recomendations limit = %s, page number = %s", limit, _page_number)
        logger.info("In get recomendations influencers = %s", influencers)
        raw_score_func = _raw_score_func
        if _now is None: 
            if isinstance(_quantity_predict, dict) and _quantity_predict.get('now'):
                now = hp.to_datetime(_quantity_predict.get('now'))
            else:
                now = datetime.now()
        else:
            now = hp.to_datetime(_now)
        sort_by = ['collaborative', 'preference', 'history'] if _sort_by is None else _sort_by
        if isinstance(sort_by, (str, unicode)):
            sort_by = hp.make_list(sort_by)
        logger.info("Sort By: %s", sort_by)
        if isinstance(_filter_list, (list, tuple)):
            filter_list = self._make_filter_list(hp.make_list(_filter_list or []))
        else:
            filter_list = None
        filter_links = filter(lambda x: bool(x), (a.get_filter_link() for a in model_dict.attributes.itervalues()))
        if filter_links:
            filters.update((f['attribute'], odict.get(f['links'])) for f in filter_links)
        if filters and not filter_list:
            logger.debug("filters are %s", filters)
            filter_list = self._make_filter_list(model_dict.filter(_filter_attributes = model_dict.ids, _internal = True, **filters), model_dict.dict_to_ids)
        if filter_list:
            filters = self._filter_list_to_filters(model_dict, filter_list)
        logger.info("List of objects after filtering: %s", filter_list)
        logger.debug("Final filters: %s", filters)
        # We don't want to recommend to ids where the experience has already occurred
        not_keys = False; cutoff = "1970-01-01";
        if filter_list and (max( recommendation_obj.get('total_number', 0), len(filter_list[0]) ) > limit ):
            filter_list, not_keys, cutoff = self._get_repeat_list(model, other, _id, now = now, filter_list = filter_list)
        logger.info("List of objects after repeat gap: not_keys: %s: %s", not_keys, filter_list)
        if filter_list and filter_list[0]:
            filters = self._filter_list_to_filters(model_dict, filter_list, not_keys)
        if not _recommendation_id:
            if filter_list and filter_list[0]:
                recommendation_obj['total_number'] = len(filter_list[0])
            else:
                recommendation_obj['total_number'] = model_dict.count()
        logger.debug("Filters after repeat gap: %s", filters)
        recommendation_obj.update({
            'is_first': _page_number == 1,
            'is_last': _page_number * limit >= recommendation_obj['total_number'],
            'page_size': limit,
            'page_number': _page_number
        })
        weights = []
        dicts = []
        input_dicts = []
        matchers = []
        matcher_list = []
        if not sort_by or 'preference' in sort_by:
            matcher_list.extend(['_matcher', '_aggr_matcher'])
        if not sort_by or 'collaborative' in sort_by:
            matcher_list.append('_interact_matcher')
        if model_id:
            for matcher in ['_matcher'] if isinstance(model_id, dict) else matcher_list:
                weights.append(
                    self._matcher_weights_dict.get('_' + model + matcher, 1.)
                )
                if isinstance(model_id, dict):
                    d = dict(getattr(self, '_' + model + matcher).obj_to_instance(model_id, getattr(self, '_' + model + '_types')))
                    logger.info("After converting to instance: %s", d)
                    weights[-1] = 10.
                else:
                    d = getattr(self, '_' + model + matcher).display(model_id, internal=True)
                if isinstance(sort_by, dict) and 'collaborative' in sort_by and matcher == '_interact_matcher':
                    pass
                input_dicts.append(d)
                matchers.append(getattr(self, '_' + model + matcher))

        if not sort_by or 'preference' in sort_by:
            # model matcher and other aggr
            logger.info("matchers by preference")
            weights.append(
                self._matcher_weights_dict.get('_' + other + '_matcher', 1.))
            logger.info("The display for id: %s is :%s", _id, self._user_matcher.display(_id, internal = True))
            input_dicts.append(
                getattr(self, '_' + other + '_matcher').display(
                    _id, internal=True, limit = 5*limit
                )
            )
            matchers.append(getattr(self, '_' + model + '_aggr_matcher'))

            # model aggr and other matcher
            weights.append(
                self._matcher_weights_dict.get('_' + other + '_aggr_matcher', 1.))
            input_dicts.append(
                getattr(self, '_' + other + '_aggr_matcher').display(
                    _id, internal=True, limit = 5*limit
                )
            )
            matchers.append(getattr(self, '_' + model + '_matcher'))
            logger.info("end matchers by preference")

        if not sort_by or 'collaborative' in sort_by:
            # model and other interact
            logger.info("matchers by collaboarative")
            if isinstance(sort_by, dict):
                exp_stages = hp.make_list(sort_by['collaborative'])
            else:
                exp_stages = self._experience_stages.keys()
            idict = {}
            for similar_id, score in getattr(self, 'get_similar_' + other + 's')(
                    _id, _limit=influencers or 10, _internal=True,
                ):
                try:
                    nitems = 0
                    for av, score1 in getattr(self, '_' + other + '_interact_matcher')._iter_display(
                            similar_id, internal=True
                        ):
                        # Get max score possible for each recommendation if the similar object has experienced
                        if av[0] in exp_stages:
                            k = (av[0], '__'.join(map(unicode, hp.make_list(similar_id))))
                            t = time.time()
                            for j in getattr(self, '_' + model + '_interact_matcher')._iter_attrs([k]):
                                idict[j] = max(idict.get(j, 0.), score*score1)
                            nitems += 1
                            if nitems > 5*limit:
                                break
                except PersistKeyError as e:
                    pass
            if len(idict):
                maxv = max(max(idict.values()), 1e-16)
                idict = dict((k, v/maxv) for k, v in idict.iteritems())
                input_dicts.append(idict)
                matchers.append(getattr(self, '_' + model + '_interact_matcher'))
                weights.append(
                    self._matcher_weights_dict.get(
                        '_' + other + '_interact_matcher',
                        1.
                    )
                )
            logger.info("end matchers by collaboarative")

        if not sort_by or 'history' in sort_by:
            logger.info("matchers by history")
            if isinstance(sort_by, dict):
                exp_stages = hp.make_list(sort_by['history'])
            else:
                exp_stages = self._experience_stages.keys()
            idict = {}
            nitems = 0
            for av, score in getattr(self, '_' + other + '_interact_matcher')._iter_display(
                    _id, internal=True
                ):
                # Get max score possible for each recommendation if the similar object has experienced
                if av[0] in exp_stages:
                    k = (av[0], '__'.join(map(unicode, hp.make_list(_id))))
                    t = time.time()
                    for j in getattr(self, '_' + model + '_interact_matcher')._iter_attrs([k]):
                        idict[j] = max(idict.get(j, 0.), score)
                    nitems += 1
                    if nitems > 5*limit:
                        break
            if len(idict):
                maxv = max(max(idict.values()), 1e-16)
                idict = dict((k, v/maxv) for k, v in idict.iteritems())
                input_dicts.append(idict)
                matchers.append(getattr(self, '_' + model + '_interact_matcher'))
                weights.append(
                    self._matcher_weights_dict.get('_preference', 1.)
                )
            logger.info("end matchers by history")

        denom = 0
        output_dict = {}
        weights_dict = {}
        attribute_weights = getattr(self, '_' + model + '_dict').get_weights()
        attribute_weights.update(getattr(self, '_' + other + '_dict').get_weights())
        for d, m, w in zip(input_dicts, matchers, weights):
            logger.debug("Filter_list in iter compare: %s, not_keys = %s", filter_list, not_keys)
            logger.debug("matcher: %s, input_dict = %s", m.name, d)
            for output_id, score in self._iter_compare(
                    model,
                    d,
                    filter_list=filter_list,
                    not_keys=not_keys,
                    limit=len(weights)*limit,
                    matcher_list=m,
                    attribute_weights=attribute_weights
                ):
                if hasattr(raw_score_func, '__call__'):
                    raw_score_func(recommendation_id, m.name, output_id, score)
                logger.info("_iter_compare: %s, output_id: %s, score: %s, w: %s", m.name, output_id, score, w)
                output_dict[output_id] = output_dict.get(output_id, 0.) + (
                    score * w
                )
                denom += score*w
                if weights_dict.get(output_id, (-1e32, None))[0] < score * w:
                    weights_dict[output_id] = (score * w, m.name)

        def do_price(price_attribute, w, reverse = False, category = '_price'):
            minp = model_dict.pivot(_action = 'min', _over = price_attribute, _as_option = True)[price_attribute]
            maxp = model_dict.pivot(_action = 'max', _over = price_attribute, _as_option = True)[price_attribute]
            for obj in model_dict.filter(_filter_attributes = model_dict.ids + [price_attribute], _sort_by = price_attribute, _page_size = 6*limit, _sort_reverse = reverse, _internal = True, **filters):
                if reverse:
                    score = ((obj.get(price_attribute, minp) - minp)/max(maxp - minp, 1e-32))
                else:
                    score = ((maxp - obj.get(price_attribute, maxp))/max(maxp - minp, 1e-32))
                output_id = hp.make_single(model_dict.dict_to_ids(obj), iterator = tuple)
                output_dict[output_id] = output_dict.get(output_id, 0.) + (
                    score * w
                )
                if weights_dict.get(output_id, (-1e32, None))[0] < score * w:
                    weights_dict[output_id] = (score * w, category)
                yield score*w

        def do_trending(previous_date, w, category):
            nlimit = int(6*limit/float(len(self._interaction_stages)))
            for stage, d in self._interaction_stages.iteritems():
                ta = self._time_attr_stage(stage)
                if not issubclass(self._interactions_dict.attributes[ta].atype[0], (date, datetime)):
                    return
                if any(i in filters for i in model_dict.ids):
                    nf = self._interactions_dict.from_parent_id_dict(model_dict.name, filters)
                else:
                    nf = filters.copy()
                nf.update({ta: (previous_date, now), self._interaction_type_attr: stage})
                f1 = hp.gen_get_top(nlimit, key_func = lambda x: hp.make_single(x, iterator = tuple))
                logger.debug("Filters in trending: %s", nf)
                self._interactions_dict.pivot(
                    self._interactions_dict.get_parent_ids(model_dict.name), 
                    _filter_results = [f1],
                    _output_format = 'ordered_dict',
                    _page_size = 1e6,
                    _sort_by = ta,
                    _sort_reverse = True,
                    **nf
                )
                maxp = 0
                if f1.top_list():
                    for output_id, count in f1.top_list():
                        if maxp < count:
                            maxp = count
                        if maxp == 0:
                            break
                        score = self._get_positivity_score(stage)*(count/float(maxp))
                        output_dict[output_id] = output_dict.get(output_id, 0.) + (
                            score * w
                        )
                        if weights_dict.get(output_id, (-1e32, None))[0] < score * w:
                            weights_dict[output_id] = (score * w, category)
                        yield score*w
                else:
                    for obj in model_dict.filter(_filter_attributes = model_dict.ids, _page_size = 6*limit,_internal = True, **filters):
                        output_id = hp.make_single(model_dict.dict_to_ids(obj), iterator = tuple)
                        score = 1e-6
                        output_dict[output_id] = output_dict.get(output_id, 0.) + (
                            score * w
                        )
                        if weights_dict.get(output_id, (-1e32, None))[0] < score * w:
                            weights_dict[output_id] = (score* w, category)
                        yield score*w

        if isinstance(sort_by, (list, dict, tuple)) and ('price_low' in sort_by or 'price_high' in sort_by or 'price' in sort_by):
            w = self._matcher_weights_dict.get('_price', 0.1)
            price_attribute = None
            if isinstance(sort_by, dict):
                sb = sort_by.get('price_low') or sort_by.get('price_high') or sort_by.get('price')
                if sb in model_dict.attributes:
                    price_attribute = sb
            if price_attribute is None:
                price_attributes = model_dict.get_attributes('name', type = 'price')
                if price_attributes:
                    price_attribute = price_attributes[0]
            if price_attribute:
                for s in do_price(price_attribute, w, reverse = True if 'price_high' in sort_by else False):
                    denom += s
        
        if isinstance(sort_by, (list, dict, tuple)) and ('latest' in sort_by):
            w = self._matcher_weights_dict.get('_latest', 0.1)
            price_attribute = None
            if isinstance(sort_by, dict):
                sb = sort_by.get('latest')
                if sb in model_dict.attributes:
                    price_attribute = sb
            if price_attribute is None:
                price_attributes = model_dict.get_attributes('name', type = ['date', 'datetime'])
                if price_attributes:
                    price_attribute = price_attributes[0]
            if price_attribute:
                for s in do_price(price_attribute, w, reverse = False):
                    denom += s

        if isinstance(sort_by, (list, dict, tuple)) and 'discount' in sort_by:
            w = self._matcher_weights_dict.get('_discount', 0.1)
            price_attribute = None
            if isinstance(sort_by, dict):
                price_attribute = sort_by.get('discount')
                if sb in model_dict.attributes:
                    price_attribute = sb
            if price_attribute is None:
                price_attributes = model_dict.get_attributes('name', type = ['discount', 'discount_percentage'])
                if price_attributes:
                    price_attribute = price_attributes[0]
            if price_attribute:
                for s in do_price(price_attribute, w, reverse = True, category = '_discount'):
                    denom += s
       
        if isinstance(sort_by, (list, dict, tuple)) and 'trending' in sort_by:
            w = self._matcher_weights_dict.get('_trending', 1.)
            if isinstance(sort_by, dict):
                try:
                    num_days = int(sort_by['trending'])
                except ValueError:
                    raise ValueError("Trending (sort_by) should be interms of number of days")
            else:
                num_days = 20
            for s in do_trending(now - timedelta(days = num_days), w, '_trending'):
                denom += s


        if isinstance(sort_by, (list, dict, tuple)) and 'popularity' in sort_by:
            w = self._matcher_weights_dict.get('_popularity', 1.)
            if isinstance(sort_by, dict):
                try:
                    num_months = int(sort_by['popularity'])
                except ValueError:
                    raise ValueError("Popularity (sort_by) should be interms of number of months")
            else:
                num_months = 3
            for s in do_trending(hp.add_month(now, - num_months), w, '_popularity'):
                denom += s

        logger.debug("Output dict after running iter compares: %s", output_dict)
        recommendation_number = (_page_number - 1) * limit
        params = {'_sort_by': self.st.interaction_time_attribute, '_page_size': 6*limit}
        params[self.st.interaction_time_attribute] = '{},'.format(cutoff)
        self._interactions_dict.parent_ids_to_dict(other_dict.name, _id, params)
        previous_interactions = list( self._interactions_dict.filter(**params) )
        int_model_id = hp.make_single(self._interactions_dict.get_parent_ids(model_dict.name))
        for output_id, score in sorted(
                output_dict.iteritems(), key=itemgetter(1),
                reverse=True)[:limit]:
            if _internal:
                yield output_id, score
                continue
            rdict = model_dict.ids_to_dict(output_id)
            rdict['current_stage'] = interactions.get_last_interaction_stage(
                    self.enterprise_id, 'admin', 
                    output_id if model_dict.model_type == 'product' else _id, 
                    output_id if model_dict.model_type == 'customer' else _id,
                    start_time = cutoff,
                    st = self.st,
                    interactions = filter(
                        lambda x: x[int_model_id] == output_id, previous_interactions
                    ),
                    recommended = True
            )
            rdict['recommendation_number'] = recommendation_number
            rdict['score'] = score / max(denom, 1e-6)
            recommendation_number += 1
            attribs = model_dict.get(output_id, _additional_values = _additional_values)
            if not attribs:
                continue
            rdict.update({
                attrib_name: attribs
            })
            if influencers:
                rdict.update({
                    'influencers':
                    dict(
                        self.get_influencers(
                            user_id=model_id if model == 'user' else _id,
                            product_id=model_id if model == 'product' else _id,
                            _limit=_influencers,
                            _num_similar=_num_similar, **_filter_influencers))['influencers']
                })
            if _quantity_predict or isinstance(_quantity_predict, dict):
                if isinstance(_quantity_predict, bool):
                    _quantity_predict = {}
                rdict.update({
                    'quantity_predictions': self.get_quantity_predictions(
                        output_id, _id, model = model, **_quantity_predict
                    )
                })
            output_list.append(rdict)
        if not _internal:
            for k, v in recommendation_obj.iteritems():
                if k == output_name:
                    yield k, v[(_page_number - 1) * limit: _page_number * limit]
                else:
                    yield k, v
            self._recommendation_model.post(recommendation_obj)
            if _freshen:
                logger.info("Saving recommendation for cacher id: %s", cache_id)
                self._cacher.set(cache_id, recommendation_obj, shelf_life = _freshen if (
                    isinstance(_freshen, (float, int, long)) and not isinstance(_freshen, bool)
                    ) else 600
                )

    #@t.timeit
    @RunApi.via_request
    def get_recommendees(
            self,
            product_id,
            user_id=None,
            **kwargs
        ):
        """
        Gets the users who are most likely to buy a product
        [
            {
                'user_id': ... name of id attribute from product model
                'score': max(from iter_compare stats) - d/ (max),
                'predicted_interaction': {
                    .
                    .
                },
                'user_object': <from user model>,
                'influencers': [
                     {
                         "user_id": name of id attribute from customer model
                         "score": max - d/ max - min
                         "user_similarity" : {
                             "attribute1": "value1"
                             .
                             .
                         },
                         "product_preference": {
                             .
                             .
                         },
                         "common_interactions": {
                         },
                         "review": ....
                     },
                     .
                     .
                 ]
            },
            .
            .
        ]
        """
        for op in self._get_recommendations(
                'user',
                'product',
                'recommendees',
                product_id,
                user_id,
                _limit = kwargs.pop('_limit', kwargs.pop('limit', 10)),
                _influencers = kwargs.pop('_influencers', kwargs.pop('influencers', None)),
                _num_similar = kwargs.pop('_num_similar', kwargs.pop('num_similar', None)),
                _now = kwargs.pop('_now', kwargs.pop('now', None)),
                _quantity_predict = kwargs.pop('_quantity_predict', kwargs.pop('quantity_predict', None)),
                **kwargs
            ):
            yield op

    #@t.timeit
    @RunApi.via_request
    def get_recommendations(
            self,
            user_id,
            product_id=None,
            **kwargs
        ):

        """
        Gets the products which are most likely to be liked by a user
        [
            {
                'product_id': name of id attribut from product model
                'score': max - d/ (max -min),
                'predicted_interaction': {
                    .
                    .
                'product_object': <from product model>
                }
                'influencers': [
                      {
                          "user_id": name of id attribute from customer model
                          "score": max - d/ max - min
                          "user_similarity" : {
                              "attribute1": "value1"
                              .
                              .
                          },
                          "product_preference": {
                              .
                              .
                          },
                          "common_interactions": {
                          },
                          "review": ....
                      },
                      .
                      .
                      ]
            },
            .
            .
        ]
        """
        for op in self._get_recommendations(
                'product',
                'user',
                'recommendations',
                user_id,
                product_id,
                _limit = kwargs.pop('_limit', kwargs.pop('limit', 10)),
                _influencers = kwargs.pop('_influencers', kwargs.pop('influencers', None)),
                _num_similar = kwargs.pop('_num_similar', kwargs.pop('num_similar', None)),
                _now = kwargs.pop('_now', kwargs.pop('now', None)),
                _quantity_predict = kwargs.pop('_quantity_predict', kwargs.pop('quantity_predict', None)),
                **kwargs
            ):
            yield op

    
    #@t.timeit
    @RunApi.via_request
    def get_customer_priority(self,
            _customer_ids = None,
            _customer_filters = None,
            _start=0,
            _limit=10,
            _product_ids = None,
            **product_filters
        ):
        if _product_ids is None: _product_ids = []
        filter_list = self._make_filter_list(_customer_ids or [])
        max_w = -1e30
        input_dict = {}
        for a in _product_ids + map(lambda x: hp.make_single(self._product_dict.dict_to_ids(x)), self._product_dict.filter(**product_filters)):
            for k, s in self._product_aggr_matcher._iter_display(capped = True):
                input_dict[k] = input_dict.get(k, 0.) + s
                if max_w < abs(input_dict[k]):
                    max_w = abs(input_dict[k])
        for k, v in input_dict.iteritems():
            input_dict[k] = v/max(max_x, 1e-6)
        for output_id, score in self._iter_compare(
                'user',
                input_dict,
                filter_by = _customer_filters,
                filter_list = filter_list,
                limit=limit,
                matcher_list=self._user_matcher
            ):
            yield output_id, score

    #@t.timeit
    @RunApi.via_request
    def get_customer_snapshot(self,
            _customer_id,
            _num_recommendations = 3,
            _num_preferences = 5,
            _num_influencers = 2,
            _num_similar = 5,
            _num_history = 8,
        ):
        output_dict = {}
        output_dict['customer_profile'] = self._user_dict.get(_customer_id)
        output_dict['product_recommendations'] = [self._product_dict.get(i) for i, v in self.get_recommendations(_customer_id, _limit = _num_recommendations, _internal = True)]
        output_dict['influencers'] = dict(self.get_influencers(_customer_id, limit = _num_influencers, _num_similar = _num_similar))['influencers']
        output_dict['history'] = OrderedDict(map(lambda x: (x[1], x[0]), self._user_interact_matcher.display(_customer_id, internal = True, limit = _num_history).iterkeys()))
        output_dict['preferences'] = OrderedDict(self._user_aggr_matcher.display(_customer_id, internal = True, limit = _num_preferences).iterkeys())
        return output_dict

    #@t.timeit
    @RunApi.via_request
    def get_people_who_also(
            self,
            product_id,
            _interaction_stage = None,
            _limit=10,
            _internal=False,
            **interact_filters
        ):
        output_list = []
        interaction_stage = self._get_lowest_experienced_stage() if _interaction_stage is None else _interaction_stage
        p = self._product_dict.get(product_id)
        if not p:
            raise ProductNotAdded("Product \"{}\" is unknown".format(product_id))
        product_display = {}
        for av, score in self._product_interact_matcher._iter_display(product_id, internal = True):
            if av[0] == interaction_stage:
                product_display.update({av: score})
        for similar_id, score in self._iter_compare(
                'product', product_display,limit = _limit, 
                matcher_list = [self._product_interact_matcher],
                **interact_filters):
            if _internal:
                yield similar_id, score
            else:
                odict = self._product_dict.ids_to_dict(similar_id)
                odict['score'] = score
                odict['product_attributes'] = self._product_dict.get(
                    similar_id)
                output_list.append(odict)
        if not _internal:
            yield 'people_who_' + interaction_stage + '_also_' + interaction_stage, output_list
            for i, v in zip(self._product_dict.ids, hp.make_list(product_id)):
                yield i, v


    def set_weights(self, weights):
        logger.debug('Called {} of {}'.format(self.__class__.__name__,
                                              hp.get_curr_func_name()))
        weights_list = [
            '_user_matcher', '_product_matcher', '_store_matcher', 
            '_user_aggr_matcher', '_store_aggr_matcher', '_product_aggr_matcher', 
            '_user_interact_matcher', '_product_interact_matcher', '_store_interact_matcher',
            '_price', '_discount', '_latest',
            '_trending', '_popularity', 
            '_preference'
        ]

        if isinstance(weights, dict):
            for w, w_ in weights.iteritems():
                self._matcher_weights_dict[w] = w_
        elif isinstance (weights, (list, tuple)):
            for i, w in enumerate(weights):
                self._matcher_weights_dict[weights_list[i]] = w
        for w in weights_list:
            if w not in self._matcher_weights_dict:
                self._matcher_weights_dict[w] = 1.



    def _add_dicts(self, dicts, weights=None, average=True):
        weights = weights or [1] * len(dicts)
        if not len(weights) == len(dicts):
            raise Exception(
                "Dicts and weights should have same length in add_dicts")
        dict3 = {}
        keys = []
        for d in dicts:
            keys.extend(d.keys())
        keys = set(keys)
        num = float(sum(weights))
        for key in keys:
            for w, d in zip(weights, dicts):
                dict3[key] = dict3.get(key, 0.) + w * d.get(key, 0.)
            if average:
                dict3[key] /= num
        return dict3
