    ################## PostGres Implementation ##################

global MASTER_TABLE_CREATED
from persist import *
import psycopg2
from psycopg2 import pool as pgpool
import psycopg2.extensions as pext
from psycopg2 import extras as pgextras, InternalError
import urlparse
from datetime import date
from datetime import timedelta
import pytz, tzlocal
import traceback
import decimal
from libraries import json_tricks as json
import string
dtime = type(datetime.time(datetime.now()))
NoneType = type(None)
import dill
urlparse.uses_netloc.append("postgres")
from objectifier import encode, decode
MASTER_TABLE_CREATED = {}
DB_USER = os.environ.get('DB_USER', 'i2ce')
DB_PASSWORD = os.environ.get('DB_PASSWORD', 'sgep@1234')
DB_NAME = os.environ.get('DB_NAME', 'pgpdict')
POSTGRES_URL = os.environ.get('POSTGRES_URL', 'localhost:5432')
POSTGRES_READ_URL = os.environ.get('POSTGRES_READ_URL', POSTGRES_URL)
LOCAL_TIMEZONE = tzlocal.get_localzone()
CURSOR_LIMIT = 2000

class ConnectionError(hp.BaseError):
    pass

DEC2FLOAT = psycopg2.extensions.new_type(psycopg2.extensions.DECIMAL.values,'DEC2FLOAT', lambda value, curs: float(value) if value is not None else None)
# DECARRAY2FLOAT = psycopg2.extensions.new_type(
#     psycopg2.extensions.DECIMALARRAY.values,
#     'DECARRAY2FLOAT', 
#     lambda value, curs: [float(v) if v is not None else None for v in value] if value is not None else None
# )
psycopg2.extensions.register_type(DEC2FLOAT)
# psycopg2.extensions.register_type(DECARRAY2FLOAT)


pgextras.register_default_jsonb(loads = json.loads)

DEBUG_LEVEL = hp.logging.INFO
logger = hp.get_logger(__name__, DEBUG_LEVEL)

_PY_2_PG_DICT = {
    'NoneType': 'bool',
    'bool': 'bool',
    'str': 'text',
    'unicode': 'text',
    'int': 'decimal',
    'long': 'decimal',
    'float': 'decimal',
    'double': 'decimal',
    'tuple': 'text',
    'date': 'date',
    'time': 'timetz',
    'dtime': 'timetz',
    'datetime': 'timestamptz',
    'timedelta': 'interval',
    'objectlist': 'jsonb',
    'floatlist': 'decimal[]',
    'numberlist': 'decimal[]',
    'functionlist': 'text[]',
    'stringlist': 'text[]',
    'list': 'text[]',
    'dict': 'jsonb',
    'function': 'text',
    'type': 'text',
    'object': 'text',
    'geolocation': 'point',
}

class numberlist(list):
    @classmethod
    def to_json(cls, inst):
        if isinstance(inst, (str, unicode)):
            return map(float, inst.split(','))
        elif isinstance(inst, (list, tuple)):
            return map(float, inst)
        else:
            return hp.make_list(float(inst))

class stringlist(list):
    @classmethod
    def to_json(cls, inst):
        if isinstance(inst, (str, unicode)):
            return map(string.strip, inst.split(','))
        elif isinstance(inst, (list, tuple)):
            return inst
        else:
            return hp.make_list(unicode(inst, errors = 'ignore'))

class objectlist(list):
    @classmethod
    def to_json(cls, inst):
        if isinstance(inst, (str, unicode)):
            return json.loads(inst)
        elif isinstance(inst, (list, tuple)):
            return inst
        else:
            return json.loads(unicode(inst, errors = 'ignore'))

class functionlist(list):
    pass

class geolocation(tuple):

    _allowed_types = (list, tuple, str, unicode)

    @classmethod
    def to_json(cls, inst):
        if not isinstance(inst, (tuple, list)):
            return None
        return ','.join(inst)

    def __new__(self, init):
        if not isinstance(init, self._allowed_types):
            raise ValueError("Geolocation input {} is not of the correct type: {}".format(type(init), self._allowed_types))
        if isinstance(init, (str, unicode)):
            init = init.split(',')
        init = tuple((i.strip() if isinstance(i, (str, unicode)) else str(i)) for i in init)
        if not len(init) == 2:
            raise ValueError("Geolocation can have only 2 inputs, not {}".format(init))
        return tuple.__new__(geolocation, init)

    def decimalize(self, digit):
        return '{},{}'.format(round(float(self[0]), digit), round(float(self[1]), digit))

def geo_to_db(gloc):
    x = pext.adapt(gloc[0]).getquoted()
    y = pext.adapt(gloc[1]).getquoted()
    return pext.AsIs("'(%s, %s)'" % (x, y))

pext.register_adapter(geolocation, geo_to_db)

MAX_CACHE = 200

POOL = {}
POOL_MAX_SIZE = int(os.environ.get('POOL_MAX_SIZE') or 0)

def get_meta_classes(cls, all_instances, name, *args, **kwargs):
    if 'DB_NAME' in kwargs:
        nname = hp.normalize_join(name) + '_' + kwargs['DB_NAME']
    else:
        nname = hp.normalize_join(name)
    no_cache = kwargs.pop('_update', False)
    if no_cache: all_instances.pop(nname, None)
    if nname in all_instances:
        i, t = all_instances[nname]
        if t >= i._get_last_updated():
            logger.debug("Pulling {} named {} from cache".format(cls.__name__, name))
            return i
        else:
            logger.debug("{} named {} has been updated".format(cls.__name__, name))
            all_instances.pop(nname, None)
    r = type.__call__(cls, name, *args, **kwargs)
    all_instances[nname] = (r, time.time())
    l = len(all_instances)
    if l > MAX_CACHE:
        s = sorted(all_instances.iteritems(), key = lambda k: k[1][1])
        for i in range(l - MAX_CACHE):
            all_instances.pop(s[i][0], None)
    return r

class metapgpdict(type):
    all_instances = {}
    def __call__(cls, name, *args, **kwargs):
        return get_meta_classes(cls, metapgpdict.all_instances, name, *args, **kwargs)

class metapgpcollection(type):
    all_instances = {}
    def __call__(cls, name, *args, **kwargs):
        return get_meta_classes(cls, metapgpcollection.all_instances, name, *args, **kwargs)


class pgpdict(pdict):

    __metaclass__ = metapgpdict
    _allowed_value_types = (
        NoneType, bool, str, unicode, int, float, date, dtime, datetime, 
        geolocation, stringlist, numberlist, objectlist, dict, functionlist, object
    )

    to_json = lambda self, x: json.dumps(x, encoding = "utf-8")


    def __init__(self, *args, **kwargs):
        db_name = kwargs.pop('DB_NAME', DB_NAME)
        self.db_name = db_name
        self.retain_previous_indexes = kwargs.pop('retain_previous_indexes', False)
        self.__database_url = urlparse.urlparse(
            os.environ.get(
                "DATABASE_URL", 
                "postgres://{}:{}@{}/{}".format(
                    DB_USER, DB_PASSWORD, POSTGRES_URL, db_name
                ) 
            )
        )
        self.__database_read_url = urlparse.urlparse(
            os.environ.get(
                "DATABASE_URL", 
                "postgres://{}:{}@{}/{}".format(
                    DB_USER, DB_PASSWORD, POSTGRES_READ_URL, db_name
                ) 
            )
        )
        if POOL_MAX_SIZE:
            try:
                self._write_pool = POOL.get(db_name) or pgpool.SimpleConnectionPool(1, POOL_MAX_SIZE, 
                    user = self.__database_url.username,
                    password = self.__database_url.password,
                    host = self.__database_url.hostname,
                    port = self.__database_url.port,
                    database = db_name
                )
                POOL[db_name] = self._write_pool
            except Exception:
                logger.info("Pools are all filled up")
                hp.print_error()
                logger.info("Going with regular connections")
                self._write_pool = None
            try:
                self._read_pool = POOL.get(db_name) or pgpool.SimpleConnectionPool(1, POOL_MAX_SIZE, 
                    user = self.__database_read_url.username,
                    password = self.__database_read_url.password,
                    host = self.__database_read_url.hostname,
                    port = self.__database_read_url.port,
                    database = db_name
                )
                POOL[db_name] = self._read_pool
            except Exception:
                logger.info("Pools are all filled up")
                hp.print_error()
                logger.info("Going with regular connections")
                self._read_pool = None
        else:
            self._read_pool = None
            self._write_pool = None
        self._col_index = dict((v, i) for i, v in enumerate(self._allowed_value_types))
        self._allowed_value_types_names = tuple(t.__name__ for t in self._allowed_value_types)
        self._cols = ', '.join(self._allowed_value_types_names)
        self._update_cols = ', '.join('{} = (%s)'.format(x) for x in self._allowed_value_types_names)
        self._update_cols_list = list('{} = (%s)'.format(x) for x in self._allowed_value_types_names)
        self._value_cols = ', '.join('{} {} NULL'.format(
                x, _PY_2_PG_DICT[x]
            ) 
            for x in self._allowed_value_types_names
        )
        self._py_2_pg_dict = _PY_2_PG_DICT
        for t in self._allowed_value_types: encode(t)
        indexes = kwargs.get('indexes', [])
        for i, ind in enumerate(indexes):
            if not isinstance(ind, tuple):
                indexes[i] = (hp.make_list(ind), False, [], '')
        if indexes:
            kwargs['indexes'] = indexes
        try:
            super(pgpdict, self).__init__(*args, **kwargs)
        except:
            #logger.debug("Database: %s, Read: %s", self.__database_url, self.__database_read_url)
            raise
        else:
            self._mem_cache_name = (self.name + '_' + db_name)

    def _update_master(self, cur = None):
        s = """
            INSERT INTO ___master_table (table_name, key_names, key_types, updated) VALUES ('{table_name}', %s, %s, now())
            ON CONFLICT (table_name)
            DO UPDATE SET  updated = now() WHERE ___master_table.table_name = '{table_name}';
            """.format(table_name = self.name)
        close = False
        if not cur:
            cur = self._connect(cursor = True, new = True)
            close = True
        cur = self._execute(s, (json.dumps(self.key_names), json.dumps([n.__name__ for n in self._key_types])), cur = cur)
        if close:
            self._close(cur)
        return

    def _py_2_pg(self, key_type):
        if not key_type in self._allowed_key_types:
            raise PersistKeyError("pdict supports keys of types '{}', not {}".format(
                    ', '.join(self._allowed_key_types_names), key_type,
                )
            )
        return self._py_2_pg_dict[key_type.__name__]

    def _type_2_name(self, value, as_string = True):
        if isinstance(value, (list, tuple)):
            logger.debug("Value %s is a list/tuple", value)
            if not value:
                return 'stringlist' if as_string else stringlist
            else:
                if all(isinstance(v, (str, unicode)) for v in value[:5]):
                    logger.debug("Returning stringlist for value %s ", value)
                    return 'stringlist' if as_string else stringlist
                elif all(isinstance(v, (int, long, float, decimal.Decimal)) for v in value[:5]):
                    logger.debug("Returning numberlist for value %s ", value)
                    return 'numberlist' if as_string else numberlist
                elif all(isinstance(v, dict) for v in value[:5]):
                    logger.debug("Returning objectlist for value %s ", value)
                    return 'objectlist' if as_string else objectlist
                else:
                    logger.debug("Returning functionlist for value %s ", value)
                    return 'functionlist' if as_string else functionlist
        elif isinstance(value, date) and isinstance(value, datetime):
            return 'datetime' if as_string else datetime
        return super(pgpdict, self)._type_2_name(value, as_string)

    def _typecast(self, value, value_type = None):
        if value_type is None: 
            value_type = self._type_2_name(value, as_string = False)
        if issubclass(value_type, NoneType):
            return True
        if issubclass(value_type, objectlist):
            return self.to_json(value)
        if issubclass(value_type, functionlist):
            logger.debug("Typecasting to object list %s", list(value))
            try:
                return [encode(v) for v in value]
            except:
                logger.error("Error in encoding value list: %s", value)
                raise
        if issubclass(value_type, dict):
            logger.debug("Typecasting to object (dict) %s", value)
            try:
                r = self.to_json(value)
                logger.debug("Typecasting json as string: %s", r)
                return r
            except:
                logger.error("Error in encoding object (dict): %s", value)
                raise
        if issubclass(value_type, self._convertible_types):
            return value
        logger.debug("typecasting value %s, %s", value, type(value))
        try:
            return encode(value)
        except:
            logger.error("Error in encoding value: %s, of value_type: %s", value, value_type)
            raise

    def setdefault(self, key, value):
        self[key] = self.get(key, value)

    def _uncast(self, value, value_type):
        if issubclass(value_type, functionlist):
            ret = [decode(o) if isinstance(o, (str, unicode)) else o for o in value]
            logger.debug("After uncasting objectlist: %s", ret)
            return ret
        if issubclass(value_type, self._convertible_types):
            if issubclass(value_type, NoneType):
                return None
            if value is None:
                return None
            if not isinstance(value, value_type):
                if issubclass(value_type, unicode):
                    if isinstance(value, str):
                        return value.decode('utf-8')
                    if isinstance(value, unicode):
                        return value
                return value_type(value)
            return value
        return decode(value)

    def _create_db(self, dbname = None, trial = 0):
        if dbname is None:
            url = self.__database_url
            dbname = url.path[0:].strip('/')
        if dbname is None:
            raise PersistError("DBName is not specified while creating.")
        cur = None
        try:
            cur = self._connect(dbname = 'postgres', trial = trial, cursor = True, new = True)
            cur.connection.set_isolation_level(0)
            self._execute('CREATE DATABASE %s' % (dbname,), cur = cur)
            self._commit(cur)
            self._close(cur)
        except psycopg2.ProgrammingError as e:
            if 'already exists' in str(e):
                logger.warning("Database {} probably created from another process".format(dbname))
            else:
                raise
        return True

    def _drop_db(selfi, dbname):
        if dbname is None:
            raise PersistError("DBName is not specified while dropping.")
        cur = self._connect(dbname = 'postgres', trial = trial, cursor = True)
        cur.connection.set_isolation_level(0)
        self._execute('DROP DATABASE %s', (dbname, ), cur = cur)
        self._commit(cur)
        self._close(cur)
        return True

    def _used_connections(self):
        r = POOL.get(self.db_name)
        if r:
            logger.debug("Used connections are: %s", r._used)
            return len(r._used)
        return None

    def _connect(self, trial = 0, dbname = None, cursor = False, new = False, named = False, read = False):
        st = time.time()
        if not new and dbname is None and self._conn is not None and self._conn.closed == 0:
            logger.debug("Connection status is %s", self._conn.status)
            if self._conn.status:
                if cursor:
                    return self._cursor(self._conn, named = named)
                return self._conn
        url = self.__database_read_url if read else self.__database_url
        if dbname is None:
            dbname = url.path[1:]
        npool = self._read_pool if read else self._write_pool
        conn = None
        if npool:
            try:
                conn = npool.getconn()
                logger.debug("Time to create connection via pool, using %s conns on %s: %s", len(npool._used), dbname, time.time() - st)
            except pgpool.PoolError as e_:
                logger.info(
                    "Connection pool is exhausted for %s %s, maybe increase the limit, used: %s", 
                    dbname, 
                    'READ' if read else 'WRITE', 
                    len(npool._used)
                )
                conn = None
        if not conn:
            try:
                conn = psycopg2.connect(
                    database=dbname,
                    user=url.username,
                    password=url.password,
                    host=url.hostname,
                    port=url.port,
                )
                logger.debug("Time to create connection directly: %s", time.time() - st)
            except psycopg2.OperationalError as e:
                hp.print_error(e)
                logger.error("Error in connecting to DB (trial %d): {error}".format(trial + 1, error = str(e)))
                if trial > 1:
                    raise ConnectionError("Cannot connect to database with {url} after {att} attempts".format(
                                            url = dbname, 
                                            att = (trial + 1)
                                        )
                                   )
                if 'database' in str(e) and 'does not exist' in str(e):
                    self._create_db(trial = trial + 1)
                conn = self._connect(trial + 1, dbname = dbname, cursor = False, new = new, named = named, read = read)
        if self.string_encoding:
            conn.set_client_encoding(self.string_encoding)
        if not new:
            if self._conn:
                if self._read_pool:
                    try:
                        self._read_pool.putconn(self._conn)
                        logger.debug("Released connection on %s READ, remaining connections: %s", self.db_name, len(self._read_pool._used))
                    except Exception as e:
                        #hp.print_error()
                        if self._write_pool:
                            try:
                                self._write_pool.putconn(self._conn)
                                logger.debug("Released connection on %s WRITE, remaining connections: %s", self.db_name, len(self._write_pool._used))
                            except Exception as e1:
                                #hp.print_error()
                                self._conn.close()
                        elif not self._conn.closed:
                            self._conn.close()
                elif not self._conn.closed:
                    self._conn.close()
            self._conn = conn
        if cursor:
            return self._cursor(conn, named = named)
        return conn

    def _cursor(self, conn = None, named = False, read = False):
        conn = conn or self._connect(cursor = False, read = read)
        try:
            if named:
                cur = conn.cursor(hp.id_generator())
                cur.itersize = CURSOR_LIMIT
            else:
                cur = conn.cursor()
        except conn.OperationalError:
            raise
        if self.time_zone:
            self._execute("SET TIME ZONE '%s';" % self.time_zone, cur = cur)
        return cur
    
    def _execute(self, command, values = None, cur = None, retries = 2):
        st = time.time()
        cur = cur or self._cursor()
        logger.debug("In transaction level: %s", self._in_transaction)
        if values is None:
            logger.debug(u"Executing command on {}: {}".format(cur.connection.dsn, command))
        else:
            logger.debug(u"Executing command on {}: {}".format(cur.connection.dsn, command), *values)
        try:
            
            if values is None:
                cur.execute(command)
            else:
                cur.execute(command, tuple(values))
        except psycopg2.IntegrityError as e:
            logger.debug(u"error: Executing command on %s: %s %% %s:\n %s", cur.connection.dsn, command, values, e)
            raise
        except psycopg2.errors.UndefinedTable as e:
            p = cur.connection.get_dsn_parameters()
            if p.get('host') in POSTGRES_READ_URL and p.get('host') not in POSTGRES_URL:
                logger.warning("Cannot connect with database, maybe there is a lag between read and write dbs?")
                self._close(cur, cur_conn = not self._in_transaction)
                cur = self._cursor(named = (cur.name is not None))
                return self._execute(command, values, cur)
        except psycopg2.errors.OperationalError as e:
            if 'connection pointer is null' in str(e).lower() and retries > 0:
                return self._execute(command, values, cur, retries = retries - 1)
            logger.info("In transaction level: %s", self._in_transaction)
            logger.error(u"ERROR: Executing command on %s: %s %% %s:\n %s", cur.connection.dsn, command, values, e)
            hp.print_error(e)
            raise e.__class__("Problem in executing command. Please connect with support")
        except Exception as e:
            logger.info("In transaction level: %s", self._in_transaction)
            logger.error(u"ERROR: Executing command on %s: %s %% %s:\n %s", cur.connection.dsn, command, values, e)
            hp.print_error(e)
            try:
                self._close(cur)
            except:
                pass
            raise e.__class__("Problem in executing command. Please connect with support")
        else:
            if values:
                logger.debug(u"Command Successful: {} in {}".format(command, time.time() - st), *values)
            else:
                logger.debug(u"Command Successful: {} in {}".format(command, time.time() - st))
        return cur
   
    def _close(self, cur = None, cur_conn = True):
        """
        If cursor is provided, then cursor is closed
        Otherwise the connection is closed
        If cur_conn is True, then the cursor's connection is also closed
        """
        if cur is None:
            if self._conn is not None and self._conn.closed == 0:
                self._conn.close()
            return
        if cur.closed == 0: 
            cur.close()
        if cur_conn and cur.connection:
            if self._read_pool:
                try:
                    self._read_pool.putconn(cur.connection)
                    logger.debug("Released connection on %s READ, remaining connections: %s", self.db_name, len(self._read_pool._used))
                except pgpool.PoolError as e:
                    logger.debug('Connection not from read pool')
                    #hp.print_error()
                    if self._write_pool:
                        try:
                            self._write_pool.putconn(cur.connection)
                            logger.debug("Released connection on %s WRITE, remaining connections: %s", self.db_name, len(self._write_pool._used))
                        except pgpool.PoolError as e1:
                            logger.debug('Connection not from read pool')
                            #hp.print_error()
                            if cur.connection.closed == 0:
                                cur.connection.close()
                    elif cur.connection.closed == 0:
                        cur.connection.close()
            elif cur.connection.closed == 0:
                cur.connection.close()

    
    def _commit(self, cur = None):
        if cur is None:
            if self._conn.closed:
                raise ConnectionError("Commiting on a closed connection")
            self._conn.commit()
        else:
            if cur.closed or cur.connection.closed:
                raise ConnectionError("Commiting on a closed connection")
            cur.connection.commit()
        return

    def _create_trigger(self, name):
        trigger_name = 'update_trigger_' + name
        if not MASTER_TABLE_CREATED.get(self.db_name):
            s1 = """
                CREATE TABLE IF NOT EXISTS ___master_table (
                        table_name text PRIMARY KEY,
                        updated timestamptz NOT NULL,
                        key_names jsonb NOT NULL,
                        key_types jsonb NOT NULL
                        );
                """
            s2 = """
                    CREATE OR REPLACE FUNCTION update_timstamp() 
                    RETURNS TRIGGER AS ${trigger_name}$
                    BEGIN
                        UPDATE ___master_table SET updated = now() 
                        WHERE table_name = '{table_name}';
                        RETURN NEW;
                    END;
                    ${trigger_name}$ language 'plpgsql';
                 """.format(trigger_name = trigger_name, table_name = name)
            s3 = """
                    CREATE OR REPLACE FUNCTION calculate_distance(lat1 float, lon1 float, lat2 float, lon2 float, units varchar)
                    RETURNS float AS $dist$
                        DECLARE
                            dist float = 0;
                            radlat1 float;
                            radlat2 float;
                            theta float;
                            radtheta float;
                        BEGIN
                            IF lat1 = lat2 AND lon1 = lon2
                                THEN RETURN dist;
                            ELSE
                                radlat1 = pi() * lat1 / 180;
                                radlat2 = pi() * lat2 / 180;
                                theta = lon1 - lon2;
                                radtheta = pi() * theta / 180;
                                dist = sin(radlat1) * sin(radlat2) + cos(radlat1) * cos(radlat2) * cos(radtheta);

                                IF dist > 1 THEN dist = 1; END IF;

                                dist = acos(dist);
                                dist = dist * 180 / pi();
                                dist = dist * 60 * 1.1515;

                                IF units = 'K' THEN dist = dist * 1.609344; END IF;
                                IF units = 'N' THEN dist = dist * 0.8684; END IF;

                                RETURN dist;
                            END IF;
                        END;
                    $dist$ LANGUAGE plpgsql;
                """
            cur = self._connect(cursor = True, new = True)
            cur.connection.set_isolation_level(0)
            cur = self._execute(s1, cur = cur)
            self._commit(cur)
            try:
                cur = self._execute(s2, cur = cur)
            except InternalError as e:
                if "tuple concurrently updated" in str(e):
                    pass
                else:
                    logger.warning("Caught error, which is not tuple concurrently updated: %s", str(e))
                    raise
            else:
                try:
                    cur = self._execute(s3, cur = cur)
                except InternalError as e:
                    if "tuple concurrently updated" in str(e):
                        pass
                    else:
                        logger.warning("Caught error, which is not tuple concurrently updated: %s", str(e))
                        raise
                else:
                    self._commit(cur)
            self._close(cur)
            MASTER_TABLE_CREATED[self.db_name] = True
        return trigger_name 

    def _find_existing_indexes(self):
        s = """
            SELECT c.relname
            FROM pg_catalog.pg_class c
            JOIN pg_catalog.pg_index i ON i.indexrelid = c.oid
            JOIN pg_catalog.pg_class c2 ON i.indrelid = c2.oid
            LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
            WHERE c.relkind IN ('i','')
            AND pg_catalog.pg_table_is_visible(c.oid)
            AND c2.relname = '{table_name}'; 
            """.format(table_name = self.name)
        cur = self._connect(cursor = True, new = True)
        cur = self._execute(s, cur = cur)
        ret = [r[0] for r in cur.fetchall() if 'pkey' not in r[0]]
        self._close(cur)
        return ret

    def _get_last_updated(self, default = None):
        s = """
            SELECT updated FROM ___master_table
            WHERE table_name = '{table_name}';
            """.format(table_name = self.name)
        cur = self._connect(cursor = True, new = True, read = True)
        default = default or datetime.now(LOCAL_TIMEZONE)
        try:
            cur = self._execute(s, cur = cur)
            ret = cur.fetchone()
            ret = ret[0] if ret else default
        except Exception as e:
            hp.print_error()
            logger.error("Could not get last updated")
            return time.time()
        finally:
            self._close(cur)
        return time.mktime(ret.timetuple())

    def _get_row_updated(self, key):
        s = """
            SELECT updated from {table_name} WHERE {keys};
            """.format(
            table_name = self.name,
            key_place = ' AND '.join(['{} = (%s)'.format(k) for k in self.key_names]),
        )
        value_in_statement = tuple(key)
        cur = self._connect(cursor = True, new = True, read = True)
        cur = self._execute(s, values = value_in_statement, cur = cur)
        if cur.rowcount:            
            r = cur.fetchone()[0]
        else:
            r = None
        self._close(cur)
        return r
    
    def _get_row_created(self, key):
        s = """
            SELECT created from {table_name} WHERE {keys};
            """.format(
            table_name = self.name,
            key_place = ' AND '.join(['{} = (%s)'.format(k) for k in self.key_names]),
        )
        value_in_statement = tuple(key)
        cur = self._connect(cursor = True, new = True, read = True)
        cur = self._execute(s, values = value_in_statement, cur = cur)
        if cur.rowcount:            
            r = cur.fetchone()[0]
        else:
            r = None
        self._close(cur)
        return r
    
    def _get_key_def(self):
        s = """
            SELECT key_names, key_types FROM ___master_table
            WHERE table_name = '{table_name}';
            """.format(table_name = self.name)
        cur = None
        try:
            cur = self._connect(cursor = True, new = True, read = True)
            cur = self._execute(s, cur = cur)
            ret = cur.fetchone()
        finally:
            if cur:
                self._close(cur)
        if not ret:
            return None, None
        return ret[0], [self._name2type[i] for i in ret[1]]

    def _get_reserved_key_words(self):
        s = """
            SELECT word FROM pg_get_keywords() WHERE catcode = %s
            """
        values = ['R']
        cur = self._connect(cursor = True, new = True, read = True)
        cur = self._execute(s, cur = cur, values = values)
        ret = cur.fetchall()
        self._close(cur)
        return [r[0] for r in ret]


    def _create(self, cached = False, cur = None):
        name = self.name
        self.trigger_name = trigger_name = self._create_trigger(name)
        with self._transaction(cur = cur) as cur:
            do_update = False
            if not self._exists(self.name):
                s = (
                        "CREATE TABLE IF NOT EXISTS {name} ("
                        "{keys},"
                        "{values},"
                        "created timestamptz not NULL, updated timestamptz not NULL,"
                        "{primary_key}"
                        ");"
                    ).format(
                        name = self.name, 
                        keys = ', '.join('{} {} NOT NULL'.format(
                                x, self._py_2_pg(y)
                            ) 
                            for x, y in zip(self.key_names, self._key_types)
                        ),
                        values = self._value_cols,
                        primary_key = 'PRIMARY KEY ({})'.format(', '.join(self.key_names)),
                    ) 
                try:
                    cur = self._execute(s, cur = cur)
                    logger.debug("do_update True while creating table: %s", self.name)
                except psycopg2.IntegrityError as e:
                    logger.warning("Table {} probably created from another process in DB {}".format(self.name, self.db_name))
                except psycopg2.ProgrammingError as e:
                    if 'already exists' in str(e):
                        logger.warning("Table {} probably created from another process in DB {}".format(self.name, self.db_name))
                    else:
                        raise
                do_update = True
            if not all(self._get_key_def()):
                logger.debug("Some entries are probably missing from ___master_table")
                do_update = True
            prev_indexes = self._find_existing_indexes()
            logger.debug("Existing indexes in %s are %s", self.name, prev_indexes)
            for cols, unique, clauses, attr in self.indexes:
                index_name = "{name}_{attr}_constraint{unique}".format(
                    name = self.name, 
                    attr = attr, 
                    unique = '_uniques' if unique else ""
                )
                logger.debug("cols: %s", cols)
                logger.debug("unique: %s", unique)
                logger.debug("clauses: %s", clauses)
                if index_name in prev_indexes:
                    prev_indexes.remove(index_name)
                else:
                    logger.info("do_update True while creating new index: %s", self.name)
                    do_update = True
                    values = []
                    s = u"CREATE {unique} INDEX IF NOT EXISTS {index_name} ON {name}{using} ({cols}){clauses};".format(
                            unique = 'UNIQUE' if unique else '',
                            index_name = index_name,
                            name = self.name,
                            cols = ', '.join(cols),
                            using = ' USING HASH' if all(c_ in self.key_names for c_ in cols) else '',
                            clauses = ' WHERE ({})'.format(' AND '.join(
                                    '{} {} %s'.format(c, f) for c, f, v in clauses
                                )
                            ) if clauses else ''
                    )
                    # To check if the index is already being created
                    prt = u"CREATE {unique} INDEX IF NOT EXISTS {index_name} ON {name}".format(
                            unique = 'UNIQUE' if unique else '',
                            index_name = index_name,
                            name = self.name,
                    )
                    for c, f, v in clauses: values.append(v)
                    logger.info(s, *values)
                    s1 = u"SELECT datname, query FROM pg_stat_activity WHERE cardinality(pg_blocking_pids(pid)) > 0;"
                    fi = False   # Tells us if the index is already being created elsewhere
                    cur = self._execute(s1, cur = cur)
                    for record in cur:
                        d = record[0]
                        r = record[1]
                        if d == self.db_name and prt in r:
                            logger.warning("There is another process which is creating the same index %s, so ignoring", index_name)
                            fi = True
                            break
                    if not fi:
                        cur = self._execute(s, values, cur = cur)
            for ri in prev_indexes:
                if not self.retain_previous_indexes:
                    s = "DROP INDEX IF EXISTS {name} CASCADE;".format(name = ri)
                    logger.info("Removing index %s", ri)
                    cur = self._execute(s, cur = cur)
                logger.debug("do_update True while deleting previous index: %s", self.name)
                do_update = True
            s = """
                DO 
                $$ 
                BEGIN 
                    IF NOT EXISTS(
                        SELECT * FROM information_schema.triggers 
                        WHERE event_object_table = '{table_name}' 
                        AND trigger_name = '{trigger_name}' 
                    ) 
                    THEN 
                        CREATE TRIGGER {trigger_name} AFTER INSERT OR UPDATE OR DELETE 
                        ON {table_name} EXECUTE PROCEDURE update_timstamp();
                    END IF ; 
                END; 
                $$ 
                """.format(table_name = self.name, trigger_name = self.trigger_name)
            if cached:
                cur = self._execute(s, cur = cur)
            if do_update:
                self._update_master(cur)
        return cur

    def _drop(self):
        self._conn.close()
        cur = self._connect(cursor = True, new = True)
        s1 = u"DROP TABLE IF EXISTS {name} CASCADE;".format(name = self.name)
        s2 = u"DELETE FROM ___master_table WHERE table_name = '{name}'".format(name = self.name)
        cur = self._execute(s1, cur = cur)
        cur = self._execute(s2, cur = cur)
        logger.debug("Dropping table: %s", s1)
        self._commit(cur)
        self._close(cur)
        self.__metaclass__.all_instances.pop(self._mem_cache_name, None)
        return True

    def _insert(self, key, value, value_type = None):
        key = hp.make_list(key)
        key = list(self._typecast(k, t) for k, t in zip(key, self._key_types))
        if value_type is None: value_type = self._type_2_name(value, as_string = False)
        try:
            s = u"INSERT INTO {table_name} ({key_names}, {col}, created, updated) VALUES ({key_place}, %s, %s, %s);".format(
                table_name = self.name,
                key_names = ', '.join(self.key_names),
                col = value_type.__name__,
                key_place = ', '.join(['%s']*len(key)),
            )
            now = datetime.now()
            cur = self._execute(s, values = tuple(key + [ self._typecast(value, value_type), now, now]))
        except psycopg2.IntegrityError as e:
            raise PrimaryKeyError(e, log = False)
        if not self._in_transaction:
            self._close(cur)
            return None
        return cur

    def _upsert(self, key, value, value_type = None):
        key = hp.make_list(key)
        key = list(self._typecast(k, t) for k, t in zip(key, self._key_types))
        if value_type is None: value_type = self._type_2_name(value, as_string = False)
        value = self._typecast(value, value_type)
        values = [None]*len(self._allowed_value_types)
        values[self._allowed_value_types.index(value_type)] = value
        try:
            s = u"""
                INSERT INTO {table_name} ({key_names}, {col}, created, updated) VALUES ({key_place}, %s, now(), now())
                ON CONFLICT ({key_names})
                DO UPDATE SET {cols}, updated=(now()) WHERE {keys};
                """.format(
                table_name = self.name,
                key_names = ', '.join(self.key_names),
                col = value_type.__name__,
                key_place = ', '.join(['%s']*len(key)),
                cols = self._update_cols,
                keys = ' AND '.join("{}.{} = %s".format(self.name, x) for x in self.key_names)
            )
            now = datetime.now()
            exc_values = tuple(key + [value] + values + key)
            logger.debug(u"Upserting: {}".format(s), *exc_values)
            cur = self._execute(s, values = exc_values )
        except psycopg2.IntegrityError as e:
            raise PrimaryKeyError(e, log = False)
        if not self._in_transaction:
            self._close(cur)
            return None
        return cur

    def _update(self, key, value, value_type = None, cur = None):
        if not isinstance(key, (list, tuple)):
            key = [key]
        key = list(key)
        if value_type is None: value_type = self._type_2_name(value, as_string = False)
        values = [None]*len(self._allowed_value_types)
        values[self._allowed_value_types.index(value_type)] = self._typecast(value, value_type)
        with self._transaction(cur = cur) as cur:
            s = u"UPDATE {table_name} SET {cols}, updated=(now()) WHERE {keys};".format(
                table_name = self.name,
                cols = self._update_cols,
                keys = 'AND '.join("{} = (%s)".format(x) for x in self.key_names)
            )
            cur = self._execute(s, values = tuple(values + key), cur = cur)
        if not self._in_transaction:
            self._close(cur)
            return None
        return cur
   
    def _count(self, filter_by = None):
        s = u"SELECT count(*) FROM {table_name}".format(
            table_name = self.name,
        )
        cur = self._connect(cursor = True, new = True, read = True)
        cur = self._execute(s, cur = cur)
        r = cur.fetchone()[0]
        self._close(cur)
        return r
        
    
    def _delete(self, key):
        s = u"DELETE FROM {table_name} WHERE {keys}".format(
            table_name = self.name,
            keys = ' AND '.join("{} = (%s)".format(x) for x in self.key_names)
        )
        cur = self._execute(s, tuple(key))
        if not self._in_transaction:
            self._close(cur)
        return True

    def _select(self, key, value_type = None, log_error = True):
        key = hp.make_list(key)
        s = u"SELECT {cols} FROM {table_name} WHERE {keys}".format(
            cols = self._cols,
            table_name = self.name,
            keys = ' AND '.join("{} = %s".format(x) for x in self.key_names)
        )
        cur = self._connect(cursor = True, new = True, read = True)
        cur = self._execute(s, tuple(key), cur = cur)
        if cur.rowcount < 1:
            raise PersistKeyError("No rows found", log = log_error)
        if cur.rowcount > 1:
            raise PrimaryKeyError("More than one primary key found")
        tindex = self._col_index.get(value_type)
        if tindex:
            ret = self._uncast(cur.fetchone()[tindex], value_type)
        else:
            ret = (self._uncast(v, r) for r, v in zip(self._allowed_value_types, cur.fetchone()) if v is not None).next()
        self._close(cur)
        return ret

    def _handle_clauses(self, filter_by = None, sort_by = None, unique = None, as_dict = False):
        filter_params = {
                '=': (lambda x, y: x == y),
                '>': (lambda x, y: x > y),
                '<': (lambda x, y: x < y),
                '>=': (lambda x, y: x >= y),
                '<=': (lambda x, y: x <= y),
                'LIKE': (lambda x, y: y in x),
                'IN': (lambda x, y: x in y),
                'IS NOT': (lambda x, y: x != y),
                'IS': (lambda x, y: x is y),
                'NOT IN': (lambda x, y: x not in y),
                '<>': (lambda x, y: x != y),
        }
        
        def handle_clause(clause, init = '', as_dict = False):
            if not (isinstance(clause, (list, tuple)) and len(clause) == 3):
                raise PersistFilterError(
                    "Unknown format of type {} or incorrect length. "
                    "Should be len(3)".format(type(clause))
                )
            if not isinstance(clause[1], (str, unicode)):
                raise PersistFilterError("Unknown operator {}. Operators should be str".format(clause[1]))
            if clause[0] not in (['key', 'value', '_updated'] + self.key_names):
                raise PersistFilterError("Unknown operative {} in clause {}. Should be either key, value, updated or key_names: {}".format(
                    clause[0], clause, self.key_names)
                )
            # This if-else clause is to check if the filter value is a single object or a list
            clause_type = 'value'
            if isinstance(clause[2], (tuple, list)):
                ctype = type(clause[2][0])
                cl = clause[2][0]
                col = ctype.__name__
                if not all(type(c).__name__ == col for c in clause[2]):
                    raise PersistFilterError("IN clause cannot have values of different types")
            else:
                ctype = type(clause[2])
                cl = clause[2]
                col = ctype.__name__
            if clause[0] == 'key':
                if len(self.key_names) != 1:
                    raise PersistFilterError("Ambiguous key for filter in case of composite key")
                col = self.key_names[0]
                ctype = self._key_types[0]
                col_index = 0
                clause_type = 'key'
            elif clause[0] == '_updated':
                col = 'updated'
                ctype = db.datetime
                col_index = len(self.key_names) + 2
                clause_type = 'key'
            elif clause[0] in self.key_names:
                col = clause[0]
                ctype = self._key_types[self.key_names.index(col)]
                col_index = self.key_names.index(col)
                clause_type = 'key'
            try:
                ctype(cl)
            except (TypeError, ValueError):
                raise PersistFilterError("Type of the filter_by clause {} ({}) does not match with column type {}".format(
                        clause,
                        type(cl), 
                        ctype, 
                        col
                    )
                )
            comp = clause[1].upper()
            if isinstance(clause[2], (tuple, list)):
                val = tuple(clause[2])
            else:
                val = clause[2]
            if comp not in filter_params:
                raise PersistFilterError("Unknown operator {}. Allowed operators are {}".format(comp, filter_params.keys()))
            comp_dict = filter_params[comp]
            if as_dict:
                if clause_type == 'value':
                    return (lambda k, v: comp_dict(v, val)), [val]
                else:
                    return (lambda k, v: comp_dict(k[col_index], val)), [val]
            return '{}{} {} %s'.format(init, col, comp), [val]

        def handle_dict(d, init = '', as_dict = False):
            if len(d) != 1:
                raise PersistFilterError("_filter supports one and only one of AND/OR in a single nesting. "
                                         "Please use NESTED filter_by clauses")
            clauses = []; values = []
            opr = d.keys()[0]
            if opr not in ['AND', 'OR']:
                raise PersistFilterError("_filter supports only 'OR and 'AND' clause connecters")
            if not isinstance(d[opr], (tuple, list)):
                raise PersistFilterError("_filter clause {} supports list, tuple or list of lists".format(opr))
            for clause in d[opr]:
                if isinstance(clause, dict):
                    cl, vl = handle_dict(clause, as_dict = as_dict)
                elif isinstance(clause, (list, tuple)) and len(clause) == 3:
                    # logger.info("Before handle_clause: %s", clause)
                    cl, vl = handle_clause(clause, as_dict = as_dict)
                    # logger.info("After handle_clause: %s %%  %s", cl, vl)

                else:
                    raise PersistFilterError(u"Unknown format of type {} or incorrect length. Should be len(3)".format(type(clause)))
                clauses.append(cl)
                values.extend(vl)
            if clauses:
                if as_dict:
                    if opr == 'OR':
                        return (lambda k, v: any(f(k,v) for f in clauses)), values
                    else:
                        return (lambda k, v: all(f(k,v) for f in clauses)), values
                return (init + (' {} '.format(opr)).join(clauses)), values
            if as_dict:
                return (lambda k,v: True), values
            return '', values

        def handle_filter_by(filter_by, as_dict = False):
            if filter_by is None:
                cl = ''; vl = []
            elif isinstance(filter_by, dict):
                cl, vl = handle_dict(filter_by, ' WHERE ', as_dict = as_dict)
            elif isinstance(filter_by, (list, tuple)) and len(filter_by) == 3:
                cl, vl = handle_clause(filter_by, ' WHERE ', as_dict = as_dict)
            else:
                raise PersistFilterError("Unknown format of type {} or incorrect length. Should be len(3)".format(type(filter_by)))
            return cl, vl

        def check_if_key(k):
            if k not in self.key_names:
                raise PersistFilterError("Unique and Order by constraint {} cannot be applied on non-key fields".format(k))
            return self.key_names.index(k)

        def handle_sort_by(sort_by, as_dict = False):
            def handle_tuple(o, obs):
                if isinstance(o, (list, tuple)) and len(o) == 2 and isinstance(o[0], (str, unicode)) and isinstance(o[1], bool):
                    if o[1] == True:
                        if as_dict:
                            key_index = check_if_key(o[0])
                            obs.append(-key_index)
                        else:
                            obs.append('{} DESC'.format(o[0]))
                    else:
                        if as_dict:
                            key_index = check_if_key(o[0])
                            obs.append(key_index)
                        else:
                            obs.append(o[0])
                elif isinstance(o, (list, tuple)):
                    for o1 in o:
                        handle_tuple(o1, obs)
                elif isinstance(o, (str, unicode)):
                    if as_dict:
                        key_index = check_if_key(o)
                        obs.append(key_index)
                    else:
                        obs.append(o)
                else:
                    raise PersistFilterError(
                        ("Unknown format of sort_by {}. "
                         "Should be tuple of (<key attr (str)>, <reverse(bool)>) or <key attr (str)>").format(type(o))
                    )
            if sort_by is None:
                return ''
            obs = []
            handle_tuple(sort_by, obs)
            if as_dict:
                return lambda x: ((x[k] if k>0 else (-x[-k] if isinstance(x[-k], (int, float)) else (-ord(x1) for x1 in x[-k]))) for k in obs)
            return (' ORDER BY ' + ', '.join(obs))

        def handle_unique(unique, sort_by, as_dict = False):
            if unique is None:
                if as_dict:
                    return [], sort_by
                return '', sort_by
            ret_unique = []
            if not isinstance(unique, (list, tuple)):
                unique = [unique]
            for u in unique:
                key_index = check_if_key(u)
                if as_dict:
                    ret_unique.append(key_index)
                else:
                    ret_unique.append(u)
                if sort_by is None:
                    sort_by = []
                sort_by.append(u)
            if as_dict:
                return ret_unique, sort_by
            ret_unique = "DISTINCT ON ({}) ".format(', '.join(ret_unique))
            return ret_unique, sort_by


        unique, sort_by = handle_unique(unique, sort_by, as_dict)
        clauses, values = handle_filter_by(filter_by, as_dict)
        orderbys = handle_sort_by(sort_by, as_dict)
        
        logger.debug("After handing clauses, %s , %s, %s, %s", clauses, orderbys, unique, values)
        return clauses, orderbys, unique, values


    def _filter(self, filter_by = None, sort_by = None, unique = None):
        clauses, orderbys, unique, values = self._handle_clauses(filter_by, sort_by, unique)
        s = u"SELECT {unique}{key_names}, {cols} FROM {table_name}{clauses}{orderbys}".format(
            unique = unique,
            key_names = ', '.join(self.key_names),
            cols = self._cols,
            table_name = self.name,
            clauses = clauses,
            orderbys = orderbys,
        )
        logger.debug('Applying Filter Query: {}'.format(s), *values)
        cur = None
        try:
            cur = self._connect(cursor = True, new = True, named = True, read = True)
            cur = self._execute(s, values, cur = cur)
            for record in cur:
                logger.debug("Fetching record %s", record)
                k = tuple(self._uncast(v, r) for r, v in zip(self._key_types, record[0:len(self.key_names)]))
                try:
                    v = (self._uncast(v, r) for r, v in zip(self._allowed_value_types, record[len(self.key_names):]) if v is not None).next()
                except StopIteration:
                    yield k, None
                else:
                    yield k, v
        except Exception as e:
            hp.print_error(e)
            self._close(cur)
        finally:
            self._close(cur)

    def _exists(self, name):
        cur = None
        try:
            cur = self._connect(cursor = True, new = True)
            self._execute("SELECT to_regclass('public.%s');" %  name, cur = cur)
            ret = bool(cur.fetchone()[0])
        except psycopg2.ProgrammingError as e:
            logger.error(e)
            ret = False
        finally:
            self._close(cur)
        return ret

    def _ifunc(self, keys, value, value_type, make_statment, timestamp = None):
        key = hp.make_list(keys)
        key = list(self._typecast(k, t) for k, t in zip(key, self._key_types))
        if not value_type in [int, float, numberlist, stringlist]:
            raise PersistCollectionError("Don't know how to iadd value of type {}".format(value_type))
        value = self._typecast(value, value_type)
        values = [None]*len(self._allowed_value_types)
        tindex = self._col_index[value_type]
        values[tindex] = value
        cols = list(self._update_cols_list)
        where_keys = list("{}.{} = %s".format(self.name, x) for x in self.key_names)
        new_vals = []
        cols, where_keys, new_vals = make_statment(cols, tindex, where_keys, new_vals)
        if timestamp:
            if isinstance(timestamp, date):
                timestamp = datetime.fromordinal(timestamp.toordinal())
            elif isinstance(timestamp, (str, unicode)):
                timestamp = hp.to_datetime(timestamp)
            s = """
                WITH n as (
                    INSERT INTO {table_name} ({key_names}, {col}, created, updated) VALUES ({key_place}, %s, {updated}, {updated})
                    ON CONFLICT ({key_names})
                    DO UPDATE SET {cols}, updated=({updated}) WHERE {keys} RETURNING *
                ) SELECT n.{col}, (n.updated - {table_name}.updated) FROM n LEFT OUTER JOIN {table_name} ON {key_joins};
                """.format(
                table_name = self.name,
                key_names = ', '.join(self.key_names),
                col = value_type.__name__,
                key_place = ', '.join(['%s']*len(key)),
                updated = '%s' if isinstance(timestamp, datetime) else 'now()',
                cols = ', '.join(cols),
                keys = ' AND '.join(where_keys),
                key_joins = ' AND '.join(['{table_name}.{k} = n.{k}'.format(table_name = self.name, k = k) for k in self.key_names]),
            )
            if isinstance(timestamp, datetime):
                value_in_statement = tuple(key + [value, timestamp, timestamp] + values + [timestamp] + key + new_vals)
            else:
                value_in_statement = tuple(key + [value] + values + key + new_vals)
        else:
            s = """
                INSERT INTO {table_name} ({key_names}, {col}, created, updated) VALUES ({key_place}, %s, now(), now())
                ON CONFLICT ({key_names})
                DO UPDATE SET {cols}, updated=(now()) WHERE {keys} RETURNING {col};
                """.format(
                table_name = self.name,
                key_names = ', '.join(self.key_names),
                col = value_type.__name__,
                key_place = ', '.join(['%s']*len(key)),
                cols = ', '.join(cols),
                keys = ' AND '.join(where_keys),
            )
            value_in_statement = tuple(key + [value] + values + key + new_vals)
        logger.debug(s, *value_in_statement)
        st = hp.time()
        cur = self._connect(cursor = True)
        cur = self._execute(s, values = value_in_statement, cur = cur)
        a = cur.fetchone()
        if not self._in_transaction:
            self._commit(cur)
            self._close(cur)
        logger.debug("Time to execute statement: %s", hp.time() - st)
        logger.debug("Value type in ifunc is: %s", value_type)
        ret = self._uncast(a[0], value_type)
        if not timestamp:
            return ret
        time_diff = a[1].total_seconds() if timestamp and a[1] else None
        return ret, time_diff

    def iadd(self, keys, value, value_type = None, operator = '+', timestamp = False):
        value_type = value_type or self._type_2_name(value, as_string = False)
        if value_type in (numberlist, stringlist):
            operator = '||'
        def make_statement(cols, tindex, where_keys, new_vals):
            cols[tindex] = '{vt} = {table_name}.{vt} {op} (%s)'.format(vt = value_type.__name__, op = operator, table_name = self.name)
            return cols, where_keys, new_vals
        return self._ifunc(keys, value, value_type, make_statement, timestamp = timestamp)

    def imax(self, keys, value, value_type = None, timestamp = False):
        value_type = value_type or self._type_2_name(value, as_string = False)
        def make_statement(cols, tindex, where_keys, new_vals):
            cols[tindex] = '{vt} = GREATEST({table_name}.{vt}, %s)'.format(vt = value_type.__name__, table_name = self.name)
            return cols, where_keys, new_vals
        return self._ifunc(keys, value, value_type, make_statement, timestamp = timestamp)

    def imin(self, keys, value, value_type = None, timestamp = False):
        value_type = value_type or self._type_2_name(value, as_string = False)
        def make_statement(cols, tindex, where_keys, new_vals):
            cols[tindex] = '{vt} = LEAST({table_name}.{vt}, %s)'.format(vt = value_type.__name__, table_name = self.name)
            return cols, where_keys, new_vals
        return self._ifunc(keys, value, value_type, make_statement, timestamp = timestamp)
    
    def _escape_query(self, input_string):
        return input_string.replace('(','\(').replace(')','\)').replace('+', '\+').replace('&','\&').replace('%', '\%').replace('*','\*').replace('|','\|').replace('?','\?').replace('[','\[').replace(']','\]').replace('{','\{').replace('}','\{')



class pgparray(parray):
    _dict_class = pgpdict


class pgpcollection(pcollection):
    
    __metaclass__ = metapgpcollection
    _dict_class = pgpdict

    def __init__(self, *args, **kwargs):
        super(pgpcollection, self).__init__(*args, **kwargs)
        self._mem_cache_name = self.name + '_' + kwargs['DB_NAME'] if 'DB_NAME' in kwargs else self.name

    def _clause_2_dict(self, filter_by):
        if filter_by:
            logger.debug("Filter by Init clause is: %s", filter_by)
        if isinstance(filter_by, dict):
            for k, v in filter_by.iteritems():
                filter_by[k] = self._clause_2_dict(v)
        elif isinstance(filter_by, list):
            for i, f in enumerate(filter_by):
                filter_by[i] = self._clause_2_dict(f)
        elif isinstance(filter_by, tuple):
            if filter_by[0] not in self.key_names:
                return {'AND': [('__attributes', '=', filter_by[0]), ('value', filter_by[1], filter_by[2])]}
        if filter_by:
            logger.debug("Filter by Final clause is: %s", filter_by)
        return filter_by

    def _get_last_updated(self, default = None):
        return self._dict._get_last_updated()

    def _update_master(self):
        return self._dict._update_master()

    def _unique(self, attribute, value_type = None):
        """
        yield all possible values an attribute takes
        """
        if attribute in self.key_names[:-1]:
            value_type = [self._key_types[self.key_names.index(attribute)]]
            s = "SELECT DISTINCT {attr} FROM {name}".format(attr = attribute, name = self.name)
            values = ()
        elif value_type:
            s = "SELECT DISTINCT {cols} FROM {name} WHERE __attributes = %s".format(
                    cols = value_type.__name__,
                    name = self.name,
                    attr = attribute
            )
            value_type = [value_type]
            values = (attribute, )
        else:

            value_type = self._dict._allowed_value_types
            s = "SELECT DISTINCT {cols} FROM {name} WHERE __attributes = %s".format(
                    cols = self._dict._allowed_value_types_names,
                    name = self.name,
            )
            values = (attribute, )
        cur = self._dict._connect(cursor = True, new = True, named = True, read = True)
        cur = self._dict._execute(s, values, cur = cur)
        for rec in cur:
            yield (self._uncast(v, t) for v, t in zip(rec, value_type) if v is not None).next()
        self._dict._close(cur)


    def __iter__(self, filter_by = None):
        filter_by = self._clause_2_dict(filter_by)
        logger.debug("filter_by is:{}".format(filter_by))
        
        clauses, orderbys, unique, values = self._dict._handle_clauses(unique = self.key_names, filter_by = filter_by)
        s = "SELECT {unique}{keys} FROM {table_name}{clauses}".format(
            unique = unique,
            keys = ', '.join(self.key_names),
            table_name = self.name,
            clauses = clauses,
        )
        cur = self._dict._connect(cursor = True, new = True, read = True)
        cur = self._dict._execute(s, values, cur = cur)
        for keys in cur.fetchall():
            if len(keys) == 1:
                yield self._dict._uncast(keys[0], self.key_types[0])
            else:
                yield [self._dict._uncast(k, t) for k, t in zip(keys, self.key_types)]
        self._dict._close(cur)

    def _count(self, filter_by = None):
        clauses, orderbys, unique, values = self._dict._handle_clauses(unique = self.key_names, filter_by = filter_by)
        s = "SELECT COUNT(*) FROM (SELECT {unique} {keys} FROM {table_name}{clauses}) AS temp".format(
            unique = unique,
            keys = ', '.join(self.key_names),
            table_name = self.name,
            clauses = clauses,
        )
        cur = self._dict._connect(cursor = True, new = True, read = True)
        cur = self._dict._execute(s, values, cur = cur)
        c = cur.fetchone()[0]
        self._dict._close(cur)
        return c

    def attribute_names(self):
        clauses, orderbys, unique, values = self._dict._handle_clauses(unique = ['__attributes'])
        s = "SELECT {unique} __attributes FROM {table_name}".format(
            unique = unique,
            table_name = self.name,
        )
        cur = self._dict._connect(cursor = True, new = True, read = True)
        cur = self._dict._execute(s, values, cur = cur)
        for attribute in cur.fetchall():
            yield attribute[0]
        self._dict._close(cur)


    def _make_unique_constraints(self, uniques, column_names = None):
        if column_names is None: column_names = [c for c in self._dict_class.type2names() if c != 'geolocation']*len(uniques)
        for u, c in zip(uniques, column_names):
            if u in self.key_names:
                raise PersistCollectionError("Cannot add further unique constraint on key(id) type {}".format(u))
            if isinstance(u, (str, unicode)):
                cols = hp.make_list(c)
                unique = True
                clauses = [('__attributes', '=', u)]
                yield (cols, unique, clauses, u)
            else:
                raise PersistCollectionError("Class {} does not support compound indexes on non-primary key columns".format(self.__class__.__name__))

    def clear(self):
        super(pgpcollection, self).clear()
        self.__metaclass__.all_instances.pop(self._mem_cache_name, None)

if __name__ == '__main__':
    d = pgpdict('test', DB_NAME = 'cat', a = 1, b = 'b', c = 3.0, d = ['a','b','c'], e = {'a': 1, 'b': 2}, f = [{'a': 1, 'b': 2}, {'c': 3}])
    print d
    print d.get('c')
    d['c'] = 'saldka'
    print d
    print len(d)
    print d.pop('c')
    print d.pop('g', None)
    print len(d)
    for k,v in d.iteritems(filter_by = {'AND': [('value', 'IN', ['b', 'a'])]}):
        print 'filter', k, v
    d['c'] = 3.0
    d.clear()
