import os, sys
from libraries import json_tricks as json
from glob import glob
import operator
import helpers as hp
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as pathsep
from  attribute import Attribute
import pgpersist as db
from stuf import stuf
from base64 import b64encode
import shutil
import validators as val
import decimal

logger = hp.get_logger(__name__)

class JsonBDict(db.pgpdict):
    _allowed_value_types = (dict,)

    def _handle_clauses(self, filter_by = None, sort_by = None, unique = None, as_dict = False, do_lower = True, filter_by_types = None):
        if filter_by_types is None: filter_by_types = {}
        filter_params = {
                '=': (lambda x, y: x == y),
                '>': (lambda x, y: x > y),
                '<': (lambda x, y: x < y),
                'LIKE': (lambda x, y: y in x),
                'IN': (lambda x, y: x in y),
                'IS NOT': (lambda x, y: x != y),
                'IS': (lambda x, y: x is y),
                'NOT IN': (lambda x, y: x not in y),
                '<>': (lambda x, y: x != y),
        }

        def handle_clause(clause, init = '', as_dict = False):
            if not (isinstance(clause, (list, tuple)) and len(clause) == 3):
                raise db.PersistFilterError(
                    "Unknown format of type {} or incorrect length. "
                    "Should be len(3)".format(type(clause))
                )
            if not isinstance(clause[1], (str, unicode)):
                raise db.PersistFilterError("Unknown operator {}. Operators should be str".format(clause[1]))
            if clause[0] not in (['key', 'value'] + self.key_names):
                raise db.PersistFilterError("Unknown operative {} in clause {}. Should be either key or value or key_names: {}".format(
                    clause[0], clause, self.key_names)
                )
            # This if-else clause is to check if the filter value is a single object or a list
            clause_type = 'value'
            if isinstance(clause[2], (tuple, list)):
                ctype = type(clause[2][0])
                cl = clause[2][0]
                col = ctype.__name__
                if not all(type(c).__name__ == col for c in clause[2]):
                    raise db.PersistFilterError("IN clause cannot have values of different types")
            else:
                ctype = type(clause[2])
                cl = clause[2]
                col = ctype.__name__
            if clause[0] == 'key':
                if len(self.key_names) != 1:
                    raise db.PersistFilterError("Ambiguous key for filter in case of composite key")
                col = self.key_names[0]
                ctype = self._key_types[0]
                col_index = 0
                clause_type = 'key'
            elif clause[0] in self.key_names:
                col = clause[0]
                ctype = self._key_types[self.key_names.index(col)]
                col_index = self.key_names.index(col)
                clause_type = 'key'
            try:
                ctype(cl)
            except (TypeError, ValueError):
                raise db.PersistFilterError("Type of the filter_by clause {} ({}) does not match with column type {}".format(
                        clause,
                        type(cl),
                        ctype,
                        col
                    )
                )
            comp = clause[1].upper()
            if isinstance(clause[2], (tuple, list)):
                val = tuple(clause[2])
            else:
                val = clause[2]
            if comp not in filter_params:
                raise db.PersistFilterError("Unknown operator {}. Allowed operators are {}".format(comp, filter_params.keys()))
            comp_dict = filter_params[comp]
            if as_dict:
                if clause_type == 'value':
                    return (lambda k, v: comp_dict(v, val)), [val], ""
                else:
                    return (lambda k, v: comp_dict(k[col_index], val)), [val], ""
            return '{}{} {} %s'.format(init, col, comp), [val], ""

        def handle_dict(d, init = '', as_dict = False, do_lower = True, filter_by_types = None):
            if filter_by_types is None: filter_by_types = {}
            clauses = []; values = []; distances = "";
            for k, v in d.iteritems():
                not_clause = ''
                list_type = bool(filter_by_types.get(k) and issubclass(filter_by_types[k], (list, dict)))
                geo_type = bool(filter_by_types.get(k) and issubclass(filter_by_types[k], db.geolocation))
                eq = '~' if list_type else '='
                list_condition = False
                pipe_condition = u' OR '
                if k.endswith('~'):
                    not_clause = 'NOT '
                    k = k[:-1]
                    eq = '!~' if filter_by_types.get(k) and issubclass(filter_by_types[k], (list, dict)) else '<>'
                if k.endswith(':'):
                    k = k[:-1]
                    list_condition = True
                    pipe_condition = u' AND '
                if k in self.key_names:
                    col_name = k
                    col_name_text = k
                else:
                    col_name = u'{cols}->>\'{k}\''.format(cols = self._cols, k = k)
                    col_name_text = u'{cols}#>>\'{{{k}}}\''.format(cols = self._cols, k = k)
                if not isinstance(v, (list, tuple)):
                    v_type = db._PY_2_PG_DICT.get(type(v).__name__, 'text')
                    logger.debug("=-----------------value type is {}".format(v_type))
                    if v_type in ['date', 'timetz', 'timestamptz', 'interval']:
                        clauses.append(u" CAST ({cols}#>> \'{{{k}, timestamp}}\' AS decimal) {eq} %s ".format(
                                k = k, value = v, cols = self._cols, eq = eq
                            )
                        )
                        values.append(hp.to_epoch(v))
                    elif v_type == 'text' and v.startswith('~'):
                        v = v[1:]
                        if (v.startswith('"') and v.startswith('"')) or (v.startswith("'") and v.startswith("'")):
                            v = v[1:-1]
                            clauses.append(
                                u' LOWER(CAST({cols} AS text)) {not_clause}LIKE %s'.format(
                                    cols = col_name, not_clause = not_clause
                                )
                            )
                            values.extend([u'%%{}%%'.format(v.lower())])
                        else:
                            clauses.append(
                                u"( {joined} )".format(
                                    joined = pipe_condition.join(
                                        u'LOWER(CAST({cols} AS text)) {not_clause}LIKE %s'.format(
                                            cols = col_name, not_clause = not_clause
                                        ) for k1 in v.split('|')
                                    ),
                                )
                            )
                            values.extend([u'%%{}%%'.format(v2.strip().lower()) for v2 in v.split('|')])
                    elif v_type == 'text' and v.startswith('^'):
                        v = v[1:]
                        clauses.append(
                            u' LOWER(CAST({cols} AS text)) {not_clause}LIKE %s'.format(
                                cols = col_name, not_clause = not_clause
                            )
                        )
                        values.extend(['{}%%'.format(v.lower())])
                    elif v_type == 'text' and v.endswith('^'):
                        v = v[:-1]
                        clauses.append(
                            u' LOWER(CAST({cols} AS text)) {not_clause}LIKE %s'.format(
                                cols = col_name, not_clause = not_clause
                            )
                        )
                        values.extend([u'%%{}'.format(v.lower())])
                    elif v_type == 'text' and v.startswith(' ') and v.endswith(' '):
                        # Word specific sub-text
                        v = v[1:-1]
                        clauses.append(
                            u'( LOWER(CAST({cols} as text)) {eq} %s {connector} LOWER(CAST({cols} AS text)) {not_clause}LIKE %s {connector} LOWER(CAST({cols} AS text)) {not_clause}LIKE %s {connector} LOWER(CAST({cols} AS text)) {not_clause}LIKE %s )'.format(
                                cols = col_name, not_clause = not_clause, connector = 'AND' if not_clause else 'OR', eq = eq
                            )
                        )
                        values.extend([v.lower(), u'%% {}'.format(v.lower()), u'%% {} %%'.format(v.lower()), u'{} %%'.format(v.lower())])
                    elif v_type == 'text':
                        if do_lower:
                            clauses.append(u" LOWER(CAST({cols} AS {type})) {eq} LOWER(%s) ".format(cols = col_name, type = v_type, eq = eq))
                        else:
                            clauses.append(u" CAST ({cols} AS {type}) {eq} %s ".format(cols = col_name, type = v_type, eq = eq))
                        values.append((u'\"{}\"'.format(self._escape_query(v))) if list_type else v)
                    elif v is None:
                        clauses.append(u" {cols} IS {not_clause}NULL ".format(cols = col_name, not_clause = not_clause))
                    else:
                        clauses.append(u" CAST ({cols} AS {type}) {eq} %s ".format(cols = col_name, type = v_type, eq = eq))
                        values.append(v)
                if isinstance(v, list) and v:
                    v_type = db._PY_2_PG_DICT.get(type(v[0]).__name__, 'text')
                    if v_type in ['date', 'timetz', 'timestamptz', 'interval']:
                        clauses.append(u" CAST ({cols}#>> \'{{{k}, timestamp}}\' AS decimal) {not_clause}IN %s ".format(
                                k = k, v = v, cols = self._cols, not_clause = not_clause,
                            )
                        )
                        values.append(tuple(hp.to_epoch(_) for _ in v))
                    elif v_type == 'text':
                        if filter_by_types.get(k) and issubclass(filter_by_types[k], (list, dict)):
                            if not list_condition:
                                if do_lower:
                                    clauses.append(u" LOWER(CAST ({cols} AS {type})) {eq} %s ".format(
                                            cols = col_name_text, type = v_type, eq = eq,
                                        )
                                    )
                                else:
                                    clauses.append(u" CAST ({cols} AS {type}) {eq} %s ".format(
                                            cols = col_name_text, type = v_type, eq = eq,
                                        )
                                    )
                                values.append(u'({})'.format(u'|'.join((u'{sep}{val}{sep}'.format(val = _.strip().strip('~').lower() if do_lower else _.strip().strip('~'), sep = u"" if _.strip().startswith('~') else "\"")).replace('(','\(').replace(')','\)') for _ in v)))
                            else:
                                if do_lower:
                                    clauses.append(
                                        u"( {joined} )".format(
                                            joined = pipe_condition.join(
                                                u'LOWER(CAST({cols} AS text)) {not_clause}LIKE %s'.format(
                                                    cols = col_name, not_clause = not_clause
                                                ) for k1 in v
                                            ),
                                        )
                                    )
                                else:
                                    clauses.append(
                                        u"( {joined} )".format(
                                            joined = pipe_condition.join(
                                                u'CAST({cols} AS text) {not_clause}LIKE %s'.format(
                                                    cols = col_name, not_clause = not_clause
                                                ) for k1 in v
                                            ),
                                        )
                                    )
                                values.extend([u'%%{sep}{val}{sep}%%'.format(val = v2.strip().strip('~').lower(), sep = u"" if v2.strip().startswith('~') else u"\"") for v2 in v])
                                
                        else:
                            if do_lower:
                                clauses.append(u" LOWER(CAST ({cols} AS {type})) {not_clause}IN %s ".format(
                                        cols = col_name_text, type = v_type, not_clause  = not_clause,
                                    )
                                )
                            else:
                                clauses.append(u" CAST ({cols} AS {type}) {not_clause}IN %s ".format(
                                        cols = col_name_text, type = v_type, not_clause  = not_clause,
                                    )
                                )
                            values.append(tuple(v1.lower() if do_lower else v1 for v1 in v))
                    else:
                        clauses.append(u" CAST({cols} AS {type}) {not_clause}IN %s ".format(
                                cols = col_name, type = v_type, not_clause = not_clause
                            )
                        )
                        values.append(tuple(v))
                elif isinstance(v,tuple):
                    if len(v)==2:
                        v_type = db._PY_2_PG_DICT.get(type(v[0] if v[0] is not None else v[1]).__name__, 'text')
                        if v_type in ['date', 'timetz', 'timestamptz', 'interval']:
                            if v[0] is None and v[1] is None:
                                pass
                            elif v[1] is None:
                                clauses.append(u" CAST ({cols}#>> \'{{{k}, timestamp}}\' AS decimal) >= %s".format(k = k, cols = self._cols, type = v_type))
                                values.append(hp.to_epoch(v[0]))
                            elif v[0] is None:
                                clauses.append(u" CAST ({cols}#>> \'{{{k}, timestamp}}\' AS decimal) <= %s ".format(k = k, max = v[1], cols = self._cols, type = v_type))
                                values.append(hp.to_epoch(v[1]))
                            else:
                                clauses.append(u" CAST ({cols}#>> \'{{{k}, timestamp}}\' AS decimal) >= %s AND CAST ({cols}#>> \'{{{k}, timestamp}}\' AS decimal) <= %s ".format(k = k, min = v[0], max = v[1], cols = self._cols, type = v_type))
                                values.extend(list(hp.to_epoch(_) for _ in v))

                        else:
                            if v[0] is None and v[1] is None:
                                if (filter_by_types.get(k) and issubclass(filter_by_types[k], (db.date, db.datetime))):
                                    clauses.append(" CAST ({cols}#>> \'{{{k}, timestamp}}\' AS decimal) IS {not_clause}NULL".format(k = k, cols = self._cols, not_clause = not_clause))
                            elif v[1] is None:
                                clauses.append(u" CAST ({cols} AS {type}) >= %s".format(cols=col_name, type = v_type))
                                values.append(v[0])
                            elif v[0] is None:
                                clauses.append(u" CAST ({cols} AS {type}) <= %s ".format(max = v[1], cols=col_name, type = v_type))
                                values.append(v[1])
                            else:
                                clauses.append(u" CAST ({cols} AS {type}) >=  %s AND CAST ({cols} AS {type}) <=  %s ".format(min = v[0], max = v[1], cols = col_name, type = v_type))
                                values.extend(list(v))
                    elif len(v) == 3 and geo_type:
                        v_type = db._PY_2_PG_DICT.get(type(v[0] if v[0] is not None else v[1]).__name__, 'text')
                        if any(v_ is None for v_ in v):
                            pass
                        else:
                            lt = float(v[2])/110.574
                            lt_max = float(v[0]) + lt
                            lt_min = float(v[0]) - lt
                            ln = float(v[2])/(111.320*hp.np.cos(float(v[0])*180/hp.np.pi))
                            ln_max = float(v[1]) + ln
                            ln_min = float(v[1]) - ln
                            clauses.append(" CAST (split_part({cols}#>>\'{{{k}}}\', \',\', 1) AS decimal) >= %s AND CAST (split_part({cols}#>>\'{{{k}}}\', \',\', 1) AS decimal) <= %s AND CAST (split_part({cols}#>>\'{{{k}}}\', \',\', 2) AS decimal) >= %s AND CAST (split_part({cols}#>>\'{{{k}}}\', \',\', 2) AS decimal) <= %s".format(cols = self._cols, k = k))
                            values.insert(0, v[1])
                            values.insert(0, v[0])
                            values.extend([
                                min(lt_min, lt_max), max(lt_min, lt_max), min(ln_min, ln_max), max(ln_min, ln_max)
                            ])
                            distances += "CAST(split_part({cols}#>>\'{{{k}}}\', \',\', 1) AS float), CAST(split_part({cols}#>>\'{{{k}}}\', \',\', 2) AS float)".format(cols = self._cols, k = k) 
                else:
                    pass
            return clauses, values, distances


        def handle_filter_by(filter_by, as_dict = False, do_lower = True, filter_by_types = None):
            if filter_by_types is None: filter_by_types = {}
            if filter_by is None:
                cl = ''; vl = []; dc = "";
            elif isinstance(filter_by, dict):
                cl, vl, dc = handle_dict(filter_by, as_dict = as_dict, do_lower = do_lower, filter_by_types = filter_by_types)
            elif isinstance(filter_by, (list, tuple)) and len(filter_by) == 3:
                cl, vl, dc = handle_clause(filter_by,  as_dict = as_dict)
            else:
                raise db.PersistFilterError("Unknown format of type {} or incorrect length. Should be len(3)".format(type(filter_by)))
            return cl, vl, dc

        def check_if_key(k):
            if k not in self.key_names:
                raise db.PersistFilterError("Unique and Order by constraint {} cannot be applied on non-key fields".format(k))
            return self.key_names.index(k)

        def handle_sort_by(sort_by, as_dict = False):
            def handle_tuple(o, obs):
                if isinstance(o, (list, tuple)) and len(o) >= 2 and isinstance(o[0], (str, unicode)) and isinstance(o[1], bool):
                    if o[0] in self.key_names:
                        if o[1] == True:
                            if as_dict:
                                obs.append(-key_index)
                            else:
                                obs.append('{} DESC'.format(o[0]))
                        else:
                            if as_dict:
                                obs.append(key_index)
                            else:
                                obs.append(o[0])
                    elif as_dict:
                        raise db.PersistFilterError("Unique and Order by constraint {} cannot be applied on non-key fields".format(o[0]))
                    elif len(o) > 2 and o[2] in ('date', 'datetime'):
                        obs.append('CAST(dict#>> \'{{{}, isoformat}}\' as {}) {}'.format(
                                o[0], db._PY_2_PG_DICT.get(o[2], 'text') if len(o)>2 else 'text', 'DESC' if o[1] else 'ASC'
                            )
                        )
                    elif o[0] == '__distance':
                        obs.append('__distance {}'.format('DESC' if o[1] else 'ASC'))
                    elif o[0] == '__timestamp':
                        obs.append('updated {}'.format('DESC' if o[1] else 'ASC'))
                    else:
                        obs.append('CAST(dict->> \'{}\' as {}) {}'.format(
                                o[0], db._PY_2_PG_DICT.get(o[2], 'text') if len(o)>2 else 'text', 'DESC' if o[1] else 'ASC'
                            )
                        )
                elif isinstance(o, (list, tuple)):
                    for o1 in o:
                        handle_tuple(o1, obs)
                elif isinstance(o, (str, unicode)):
                    if o in self.key_names:
                        if as_dict:
                            obs.append(key_index)
                        else:
                            obs.append(o)
                    elif as_dict:
                        raise db.PersistFilterError("Unique and Order by constraint {} cannot be applied on non-key fields".format(o))
                    elif o == '__distance':
                        obs.append('__distance')
                    elif o == '__timestamp':
                        obs.append('updated')
                    else:
                        obs.append('dict->>\'{}\''.format(o))
                else:
                    raise db.PersistFilterError(
                        ("Unknown format of sort_by {}. "
                         "Should be tuple of (<key attr (str)>, <reverse(bool)>) or <key attr (str)>").format(type(o))
                    )
            if sort_by is None:
                return ''
            obs = []
            handle_tuple(sort_by, obs)
            if as_dict:
                return lambda x: ((x[k] if k>0 else (-x[-k] if isinstance(x[-k], (int, float)) else (-ord(x1) for x1 in x[-k]))) for k in obs)
            return (' ORDER BY ' + u', '.join(obs))

        def handle_unique(unique, sort_by, as_dict = False):
            if not unique:
                if as_dict:
                    return [], sort_by
                return '', sort_by
            ret_unique = []
            if not isinstance(unique, (list, tuple)):
                unique = [unique]
            for u in unique:
                key_index = check_if_key(u)
                if as_dict:
                    ret_unique.append(key_index)
                else:
                    ret_unique.append(u)
                if sort_by is None:
                    sort_by = []
                sort_by.append(u)
            if as_dict:
                return ret_unique, sort_by
            ret_unique = "DISTINCT ON ({}) ".format(u', '.join(ret_unique))
            return ret_unique, sort_by


        unique, sort_by = handle_unique(unique, sort_by, as_dict)
        clauses, values, distances = handle_filter_by(filter_by, as_dict, do_lower = do_lower, filter_by_types = filter_by_types)
        orderbys = handle_sort_by(sort_by, as_dict)

        logger.debug("After handing clauses, %s , %s, %s, %s, %s", clauses, orderbys, unique, values, distances)
        return clauses, orderbys, unique, values, distances

    def _filter(self, filter_by = None, sort_by = None, unique = None, condition_type=None, start = 0, limit = None, load_total = None, base_filter = None, filter_by_types = None, last_updated = None, updated_before = None, do_lower=True):
        if filter_by and ('AND' in filter_by or 'OR' in filter_by):
            if not len(filter_by) == 1:
                raise db.PersistFilterError("Filter by clause currently only supports single level of nesting")
            joiner = filter_by.keys()[0].upper()
            filter_by = filter_by.values()[0]
        else:
            joiner = u'AND'
        if condition_type: joiner = condition_type
        if filter_by_types is None: filter_by_types = {}
        is_distance = False
        if not sort_by and filter_by and any(issubclass(filter_by_types.get(k, str), (db.geolocation,)) for k in filter_by):
            is_distance = True
            sort_by = "__distance"
        clauses, orderbys, unique, values, distances = self._handle_clauses(
            filter_by, sort_by, unique, filter_by_types=filter_by_types, do_lower=do_lower)
        if clauses:
            clauses_string = u"({})".format(u" {} ".format(joiner).join(clauses))
        else:
            clauses_string = u""
        if base_filter:
            clauses, _, _, v, _ = self._handle_clauses(base_filter, do_lower=do_lower)
            values += v
            clauses_string += u'{}({})'.format(u" AND " if clauses_string else u"", u" {} ".format('AND').join(clauses))
        if last_updated:
            try:
                last_updated = hp.to_datetime(last_updated)
            except Exception as e:
                logger.error("Could not parse last_updated value : %s", last_updated)
                raise 
            clauses_string += u'{}({})'.format(" AND " if clauses_string else "", " updated >= %s")
            values += [last_updated]
        if updated_before:
            try:
                updated_before = hp.to_datetime(updated_before)
            except Exception as e:
                logger.error("Could not parse updated_before value : %s", updated_before)
                raise 
            clauses_string += u'{}({})'.format(" AND " if clauses_string else "", " updated < %s")
            values += [updated_before]
        if clauses_string: clauses_string = ' WHERE ' + clauses_string

        
        s = u"SELECT {unique}{key_names}, {cols}{updated}{distance} FROM {table_name}{clauses}{orderbys}".format(
            unique = unique,
            key_names = u', '.join(self.key_names),
            cols = self._cols,
            table_name = self.name,
            updated = ', updated' if (last_updated or updated_before) else '',
            clauses = clauses_string,
            orderbys = orderbys,
            distance = ', calculate_distance({distances},%s, %s,\'K\') as __distance'.format(distances = distances) if is_distance else '',
        )
       
        # For loading total
        vcp = list(values)
        if is_distance:
            vcp = vcp[2:]
        sc = u"SELECT COUNT(*) FROM {table_name}{clauses}".format(
            unique = unique,
            key_names = u', '.join(self.key_names),
            cols = self._cols,
            table_name = self.name,
            clauses = clauses_string
        )

        if isinstance(load_total, dict):
            try:
                cur = self._connect(cursor = True, new = True, named = False, read = True)
                cur = self._execute(sc, vcp, cur = cur)
                load_total['total_number'] = cur.fetchone()[0]
            finally:
                self._close(cur)

        if isinstance(limit, (int, float)):
            s += ' LIMIT %s'
            values.append(int(limit - start))
        if isinstance(start, (int, float)):
            if start >= 0:
                s += ' OFFSET %s'
                values.append(int(start))
            if start < 0:
                s += ' OFFSET %s'
                if not isinstance(load_total, dict):
                    start = -start
                    values.append(int(start))
                else:
                    values.append(max(int(load_total['total_number'] + start), 0))


        logger.info(u"\n Query is\n {}\n".format(s), *tuple(values))
        try:
            cur = self._connect(cursor = True, new = True, named = True if not isinstance(limit, (int, float)) else False, read = True)
            cur = self._execute(s, values, cur = cur)
            for record in cur:
                logger.debug("Fetching record %s", record)
                k = tuple(self._uncast(v, r) for r, v in zip(self._key_types, record[0:len(self.key_names)]))
                backindex = 0
                if is_distance: backindex -= 1
                if last_updated or updated_before: backindex -= 1
                backindex = backindex or None
                v = (self._uncast(v, r) for r, v in zip(self._allowed_value_types, record[len(self.key_names):backindex]) if v is not None).next()
                if last_updated or updated_before: v['__timestamp'] = '{}'.format(record[-1])
                backindex = 0
                if is_distance:
                    backindex -= 1
                    v['__distance'] = record[backindex]
                if last_updated or updated_before:
                    backindex -= 1
                    v['__timestamp'] = '{}'.format(record[backindex])
                yield k, v
        finally:
            self._close(cur)

    def _unique_values(self, attribute, value_type):
        v_type = db._PY_2_PG_DICT.get(value_type.__name__, 'text')
        if value_type in [db.datetime, db.date, db.time]:
            s = u"SELECT CAST({cols}#>> '{{{attribute},isoformat}}' as {type}), COUNT(*) from {table_name} GROUP BY CAST({cols}#>> '{{{attribute},isoformat}}' as {type})".format(attribute = attribute, cols = self._cols, table_name = self.name, type = 'text')
            values = tuple()
        else:
            s = u"SELECT {cols}->> %s, COUNT({cols}->> %s) from {table_name} GROUP BY {cols} ->> %s".format(attribute = attribute, cols = self._cols, table_name = self.name)
            values = (attribute,)*3
        try:
            cur = self._connect(cursor = True, new = True, read = True)
            logger.info(s, *values)
            cur = self._execute(s, values, cur = cur)
            unique_values = {}
            total = 0
            for record in cur.fetchall():
                unique_values.update({record[0]: record[1]})
                total += record[1]
        finally:
            self._close(cur);
        return unique_values, total

    def _update_many(self, instance, filter_by, condition_type = None):
        if filter_by and ('AND' in filter_by or 'OR' in filter_by):
            if not len(filter_by) == 1:
                raise db.PersistFilterError("Filter by clause currently only supports single level of nesting")
            joiner = filter_by.keys()[0].upper()
            filter_by = filter_by.values()[0]
        else:
            joiner = u'AND'
        if condition_type: joiner = condition_type

        clauses, orderbys, unique, values, distances = self._handle_clauses(filter_by, None, None, as_dict = False, do_lower = False)
        clauses_string = u" {} ".format(joiner).join(clauses)
        s = u"UPDATE {table_name} SET {cols} = {cols} || (%s) WHERE {clauses}".format(
            table_name = self.name,
            clauses = clauses_string,
            cols = self._cols
        )
        values = [json.dumps(instance)] + values
        logger.info(u"Update many query is {}".format(s), *tuple(values))
        try:
            cur = self._connect(cursor = True, new = True)
            cur = self._execute(s, values, cur = cur)
            r = cur.rowcount
            self._commit(cur)
        finally:
            self._close(cur)
        return r

    def _delete_many(self, filter_by, condition_type = None):
        if filter_by and ('AND' in filter_by or 'OR' in filter_by):
            if not len(filter_by) == 1:
                raise db.PersistFilterError("Filter by clause currently only supports single level of nesting")
            joiner = filter_by.keys()[0].upper()
            filter_by = filter_by.values()[0]
        else:
            joiner = u'AND'
        if condition_type: joiner = condition_type

        clauses, _, _, values, _ = self._handle_clauses(filter_by, None, None, as_dict = False, do_lower = False)
        clauses_string = u" {} ".format(joiner).join(clauses)
        s = u"DELETE FROM {table_name}  WHERE {clauses}".format(
            table_name = self.name,
            clauses = clauses_string,
            cols = self._cols
        )
        logger.info(u"Delete many query is {}".format(s), *tuple(values))
        try:
            cur = self._connect(cursor = True, new = True)
            cur = self._execute(s, values, cur = cur)
            r = cur.rowcount
            self._commit(cur)
        finally:
            self._close(cur)
        return r

    def iadd(self, ids, attribute, value = 1, default = 0):
        ids = hp.make_list(ids)
        where_keys = list("{}.{} = %s".format(self.name, x) for x in self.key_names)
        now = db.datetime.now()
        s = u"""
            INSERT INTO {table_name} ({key_names}, dict, created, updated) VALUES ({key_place}, %s, %s, %s)
            ON CONFLICT ({key_names})
            DO UPDATE SET dict = {table_name}.dict || CONCAT(\'{{\"{attr}\":', COALESCE(({table_name}.dict->>%s){cast_type}, (%s){cast_type}){cast_type} {operator} {as_array}(%s{cast_type}), \'}}\')::jsonb, updated=(%s) WHERE {keys} RETURNING dict ->> %s;
            """.format(
            table_name = self.name,
            key_names = u', '.join(self.key_names),
            key_place = u', '.join(['%s']*len(ids)),
            keys = u' AND '.join(where_keys),
            attr = attribute,
            cast_type = "::decimal" if isinstance(value, (int, float)) else "::jsonb",
            operator = "+" if isinstance(value, (int, float)) else "||",
            as_array = "" if isinstance(value, (int, float)) else "",
        )
        if isinstance(default, list):
            default = '[]'
        value_in_statement = ids + [json.dumps({attribute: value}), now, now, attribute, default, value if isinstance(value, (int, float)) else json.dumps(value), now] + ids + [attribute]
        logger.debug(s, *value_in_statement)
        try:
            cur = self._connect(cursor = True)
            cur = self._execute(s, values = value_in_statement, cur = cur)
            ret = cur.fetchone()[0]
            if not isinstance(value, (int, float)):
                ret = json.loads(ret)
            if not self._in_transaction:
                self._commit(cur)
        finally:
            if not self._in_transaction:
                self._close(cur)
        return ret



class PGCollection(db.pgpcollection):

    def _filter(self, filter_by = None, sort_by = None, unique = None, condition_type="AND", load_total = None, last_updated = None, updated_before = None):
        """
        filter_by is a dict of the format
        {
            '<attribute>': <value>          # Equality, Partial string match
            '<attribute>': [<v1>,<v2>,..]   # IN  (list)
            '<attribute>': "<min>,<max>"    # WITHIN RANGE
            '<attribute>': ",<max>"         # <=
            '<attribute>': "<min>,"         # >=
            sort_by      : <attribute>      # sort by attribute
            page_size    : <num records>    # default 50
            page_number  : <which page>     # default 1
            condition_type: <AND/OR>        # default AND
        }
        """
        """
        e.g. Simple rule
        (<attribute>, '>', 3) # value > 3
        (<attribute>, '<>', None) # value != None
        (<attribute>, 'LIKE', 'xyz%') # value like xyz%
        (<attribute>, 'IN', ('abc', 'def', 'geh')) # value IN

        e.g. OR / AND / NOT
        {
            'OR': [(<attribute>, '=', 'xyz'), (<attribute>, '=', 'xyz')], # value == xyz
        }

        e.g. Nested
        {
            'AND': [
                (<attribute>, '=', 'xyz'), 
                {'OR': [
                    (<attribute>, '=', 'xyz'),
                    (<attribute>, 'LIKE', 'xyz%'),
                ]
            ]
        }
        """
        converted_filter_by = None
        if filter_by is not None:
            t =[]
            for k, v in filter_by.iteritems():
                not_clause = ''
                eq = "="
                v_type = db._PY_2_PG_DICT.get(type(v).__name__, 'text')
                if k.endswith('~'):
                    k = k[:-1]
                    not_clause = 'NOT '
                    eq = "<>"
                if isinstance(v, list):
                    t.append((k, not_clause + 'IN', tuple(v)))
                elif isinstance(v, tuple):
                    if v[0] is not None:
                        t.append((k, ">=", v[0]))
                    if v[1] is not None:
                        t.append((k, "<=", v[1]))
                elif v_type == 'text':
                    if v.startswith('~'):
                        v = v[1:]
                        t.append((k, not_clause + 'LIKE', u'%%{}%%'.format(v)))
                    elif v.startswith('^'):
                        v = v[1:]
                        t.append((k, not_clause + 'LIKE', u'{}%%'.format(v)))
                    elif v.endswith('^'):
                        v = v[:-1]
                        t.append((k, not_clause + 'LIKE', u'%%{}'.format(v)))
                    else:
                        t.append((k, eq, unicode(v)))
                    for av in v.split():
                        t.append((k, eq, v))
                else:
                    t.append((k, eq, v))
            if len(t) == 0:
               converted_filter_by = None 
            elif len(t)==1:
                converted_filter_by = t[0]
            elif len(t)>1:
                converted_filter_by = {condition_type : t}
                if condition_type == "AND":
                    result = self.filter(t[0])
                    del(converted_filter_by["AND"][0])
                    results2 = []
                    results = list(result)
                    temp = list(results)
                    for r in results:                    
                        for tu in converted_filter_by["AND"]:
                            if r in temp:
                                if tu[1] == "=":
                                    if isinstance(tu[2], (str,unicode) ):
                                        if not r[tu[0]].lower() in tu[2].lower():
                                            results2.append(r)
                                            temp.remove(r)
                                    else:
                                        if not r[tu[0]] == tu[2]:
                                            results2.append(r)
                                            temp.remove(r)                                    
                                elif tu[1] == "<>":
                                    if isinstance(tu[2], (str,unicode) ):
                                        if r[tu[0]].lower() in tu[2].lower():
                                            results2.append(r)
                                            temp.remove(r)
                                    else:
                                        if r[tu[0]] == tu[2]:
                                            results2.append(r)
                                            temp.remove(r)                                    
                                elif tu[1] == ">":
                                    if r[tu[0]] < tu[2]:
                                        results2.append(r)
                                        temp.remove(r)
                                elif tu[1] == "<":
                                    if r[tu[0]] > tu[2]:
                                        results2.append(r)
                                        temp.remove(r)
                                elif tu[1] == "IN":
                                    if isinstance(tu[2][0], (str,unicode)):
                                        if not r[tu[0]].lower() in [x.lower() for x in tu[2]]:
                                            results2.append(r)
                                            temp.remove(r)
                                    else:
                                        if not r[tu[0]] in tu[2]:
                                            results2.append(r)
                                            temp.remove(r)                                    
                                elif tu[1] == "NOT IN":
                                    if isinstance(tu[2][0], (str,unicode)):
                                        if r[tu[0]].lower() in [x.lower() for x in tu[2]]:
                                            results2.append(r)
                                            temp.remove(r)
                                    else:
                                        if r[tu[0]] in tu[2]:
                                            results2.append(r)
                                            temp.remove(r)                                    
                                elif tu[1] == "LIKE":
                                    if not r[tu[0]].lower() in [x.lower() for x in tu[2]]:
                                        results2.append(r)
                                        temp.remove(r)
                    return temp                                 
        return self.filter(converted_filter_by)

    def _iadd(self, ids, attribute, value_type, value = 1, default = 0):
            ids = hp.make_list(ids)
            ids.append(attribute)
            self._dict.iadd(ids, value, value_type)            
            return 1
        
    def _unique_values(self, attribute, value_type):
        s = "SELECT {cols}, COUNT({cols}) FROM {table_name} WHERE __attributes = %s GROUP BY {cols}".format(table_name = self.name, attribute = attribute, cols = value_type.__name__)
        try:
            cur = self._dict._connect(cursor = True, new = True, read = True)
            cur = self._dict._execute(s,[attribute], cur = cur)
            unique_values = {}
            total = 0
            for record in cur.fetchall():
                unique_values.update({record[0]: record[1]})
                total += record[1]
        finally:
            self._close(cur);
        return unique_values, total    



