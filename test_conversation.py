import yaml
import json
import sys
import conv_tester
import helpers as hp

logger = hp.get_logger(__name__)

def merge_data(test_data, conv, conversation_id = None):
    test_data["conversation_id"] = conversation_id or conv.get("conversation_id")
    test_data["entities"] = {}
    test_data["utterances"] = {}
    test_data["sequences"] = {}
    for e in conv['entities']:
        if "model" in e:
            test_data["entities"]['__' + e['name'] + '__'] = "/unique/{}/{}".format(e["model"], e["attribute"])
        else:
            test_data["entities"]['__' + e['name'] + '__'] = e['values']

    def make_test_data(cs, i, k):
        test_data["utterances"]["{}_{}".format(cs["name"], i)] = {
            "utterance": k,
            "customer_state": cs["name"],
            "system_response": hp.make_single(cs["follow_up"].keys()),
        }

    # First finish one set of each customer state, then pick other variations
    for i in range(10000):
        for cs in conv['customer_states']:
            if len(cs['keywords']) <= i:
                continue
            test_data["utterances"] = {}
            k = cs['keywords'][i]
            make_test_data(cs, i, k)
            logger.info("Analyszing customer state : %s", cs["name"])
            yield test_data

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--BASE_URL', help = "BASE URL to conduct tests", default = "https://marutisuzuki.iamdave.ai")
    parser.add_argument('--USER_ID', help = "USER_ID to conduct tests", default = "ananth+maruti@i2ce.in")
    parser.add_argument('--API_KEY', help = "ADMIN API KEY to conduct tests", default = "08e73ae1-f90b-313d-89c3-e18f080a4136")
    parser.add_argument('--ENTERPRISE_ID', help = "ENTERPRISE IF to conduct tests", default = "maruti_suzuki")
    parser.add_argument('--TEST_DATA', help = "Path to base test file")
    parser.add_argument('--CONVERSATION_FILE', help = "Path to conversation file which needs to be tested")
    parser.add_argument('--COVERAGE', help = "How many variations you want to cover", type=float, default = 200)
    parser.add_argument('--CONVERSATION_ID', help = "Conversation Id", default = None)
    parser.add_argument('--RUNNER', help = "How to present the results options are xml and text", default = None)
    parser.add_argument('--REPORT_DIR', help = "Directory to store results", default = 'test-reports')
    parser.add_argument('--TEST_CREATOR', help = "If True, then only the test cases are printed and actual tests are not conducted", default = None)
    args = parser.parse_args()

    with open(args.TEST_DATA, 'r') as fp:
        test_data = yaml.load(fp)
    with open(args.CONVERSATION_FILE, 'r') as fp:
        conv = json.load(fp)
    for test_data in merge_data(test_data, conv, args.CONVERSATION_ID):
        conv_tester.run_test(test_data, args)
