import os
import json
import cherrypy
import traceback

from algorithms import BaseAlgorithm

from helpers.runapi import UserNotAdded, ProductNotAdded


class Root(object):
    def __init__(self, algorithm_class = BaseAlgorithm):
        self.algorithm_class = algorithm_class

    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def default(self, func, dataset, *args, **kwargs):
        method = cherrypy.request.method.lower()
        if method == 'post':
            data = cherrypy.request.json
        elif method == 'get':
            data = cherrypy.request.params
        if data:
            kwargs.update(data)
        if func == '__init__':
            s = self.algorithm_class(dataset, *args, **kwargs)
            return {
                    'dataset': dataset,
                    'weights': kwargs['weights'] if len(args) < 1 else args[1]
                    }
        s = self.algorithm_class(dataset)
        try:
            return dict(getattr(s, method+'_'+func)(*args, **kwargs))
        except AttributeError as e:
            raise cherrypy.HTTPError(400, 'Invalid method: {}: {}'.format(func, e.message))
        except UserNotAdded as e:
            raise cherrypy.HTTPError(404, e.message)
        except ProductNotAdded as e:
            raise cherrypy.HTTPError(404, e.message)
        except Exception as e:

            # import traceback
            print traceback.print_exc()
            raise cherrypy.HTTPError(500, 'Error with message : {}'.format(e.message))


if __name__ == '__main__':
    algorithm = os.environ.get("DAVE_ALGORITHM", 'BaseAlgorithm')
    if not algorithm in globals():
        raise Exception("Unknown algorithm: {}".format(algorithm))
    algorithm = globals()[algorithm]
    # if not isinstance(algorithm, BaseAlgorithm) or algorithm is algorithms.BaseAlgorithm:
    #     raise Exception("Given algorithm {} is not an algorithm of class BaseAlgorithm".format(algorithm))
    try:
        cherrypy.quickstart(Root(algorithm_class = algorithm))
    except:
        pass