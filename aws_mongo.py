import os, sys
from copy import deepcopy as copyof, copy
import dill
import types
import helpers as hp
import persist
from pgpersist import get_meta_classes, geolocation, date, datetime, dtime, stringlist, numberlist, objectlist
import pymongo
    
DEFAULT_DB_NAME = 'i2ce'
#DATABASE_URL = os.environ.get('MONGO_URL', "km-dave-mongo-db.cluster-caphsxy1o5sy.ap-south-1.docdb.amazonaws.com:27017")
DATABASE_URL = os.environ.get('MONGO_URL', "localhost:27017")
PEM_FILE = os.environ.get('MONGO_PEM_FILE', hp.joinpath(os.environ.get('HOME'), '.ssh', 'ddb.pem') if 'amazonaws' in DATABASE_URL else None)
USERNAME = os.environ.get('MONGO_USERNAME')
PASSWORD = os.environ.get('MONGO_PASSWORD')
TZ = hp.timezone('UTC')

logger = hp.get_logger(__name__)

class MongoError(hp.I2CEError):
    pass

class metabasemongo(type):
    all_instances = {}
    def __call__(cls, name, *args, **kwargs):
        return get_meta_classes(cls, metabasemongo.all_instances, name, *args, **kwargs)

class mongodict(persist.pdict):

    _allowed_key_types = (unicode,)
    _allowed_value_types = (dict, )
    __metaclass__ = metabasemongo

    def __init__(self, name, key_types = None, key_names = None, indexes = None, iterable = None, DB_NAME = None, **kwargs):
        self.dbname = DB_NAME or DEFAULT_DB_NAME
        indexes = indexes or []
        for i, ind in enumerate(indexes):
            if not isinstance(ind, tuple):
                indexes[i] = (ind, False, False, str)
        self._db = self._connect(self.dbname)
        self._mt = self._db['master___table']
        super(mongodict, self).__init__(name, key_types, key_names, indexes = indexes, iterable = iterable, **kwargs)

    def _create(self, *args, **kwargs):
        self._c = self._db[self.name]
        self.create_indexes()
        self._update_master()

    def create_indexes(self):
        return
        #indexes = self._c.getIndexes()
        #now_indexes = {
        #    (x.infos['fields'][0], x.infos['type']): x for x in (
        #        indexes['geo'].values() + \
        #        indexes['skiplist'].values() + \
        #        indexes['hash'].values()
        #    )
        #}
        #for a, u, r, t in self.indexes:
        #    t = hp.make_list(t)
        #    if any(issubclass(
        #            t_, (str, unicode, int, long, float, date, datetime, dtime)
        #        ) for t_ in t) and a not in reduce(
        #            lambda x, y: x + y, [_.infos['fields'] for _ in indexes['skiplist'].values()], []
        #        ):
        #        self._c.ensureSkiplistIndex([a], unique = u, sparse = not r)
        #    else:
        #        now_indexes.pop((a, 'skiplist'), None)
        #    if any(issubclass(t_, (geolocation,)) for t_ in t) and a not in reduce(
        #        lambda x, y: x + y, [_.infos['fields'] for _ in indexes['geo'].values()], []
        #        ):
        #        self._c.ensureGeoIndex([a])
        #    else:
        #        now_indexes.pop((a, 'geo'), None)
        #    if any(
        #        issubclass(
        #                t_, (stringlist, numberlist, dict, objectlist)
        #            ) for t_ in t
        #        ) and '{}[*]'.format(a) not in reduce(
        #            lambda x, y: x + y,
        #            [_.infos['fields'] for _ in indexes['hash'].values()], []
        #        ):
        #        self._c.ensureSkipListIndex(["{}[*]".format(a)], unique = u, sparse = not r)
        #    else:
        #        now_indexes.pop((a, 'hash'), None)
        #while now_indexes:
        #    # Deleting indexes that don't exist now
        #    i = now_indexes.pop(next(iter(now_indexes)))
        #    i.delete()
        #return

    def _update_master(self):
        self._upsert(self.name, {
                'key_names': self.key_names, 
                'key_types': [n.__name__ for n in self._key_types], 
            }, _mt = True
        )

    def _drop(self):
        try:
            self._c.drop()
            self._delete(self.name, None, _mt = True)
        except ( pymongo.errors.CollectionInvalid, ) as err :
            pass


    def _make_key(self, key):
        key = hp.make_list(key)
        key = '--'.join(key)
        return hp.make_uuid(key)

    def _unmake_key(self, key):
        if key is None:
            return None
        try:
            key = hp.unmake_uuid(key)
        except Exception as e:
            logger.error("Got an error in converting key: %s", key)
            raise
        return key.split('--')

    def _set_col(self, **kwargs):
        if kwargs.get('_mt'):
            return self._mt
        return self._c

    def _upsert(self, key, value, *args, **kwargs):
        if not isinstance(value, dict):
            value = {'_value': value}
        _id = self._make_key(key)
        for i, k in enumerate(self.key_names):
            value[k] = key[i]
        value['__updated'] = hp.epoch()
        did = self._set_col(**kwargs).update_one({'_id': _id}, {'$set': value}, upsert = True)
        return did

    _update = _upsert
    _insert = _upsert

    def _select(self, key, *args, **kwargs):
        _key = self._make_key(key)
        ret = self._set_col(**kwargs).find_one({'_id': _key}, {'_id': 0})
        ret = ret or {}
        key, ret = self._jsonify(ret, *args, **kwargs)
        return ret 

    def _jsonify(self, ret, _timestamp = False, _id = False, *args, **kwargs):
        _key = _id or ret.pop('_id', None); 
        if '_value' in ret:
            return self._unmake_key(_key), ret['_value']
        ret.pop('__created', None);
        logger.debug("Return: %s", ret)
        if _timestamp and '__updated' in ret: 
            ret['__timestamp'] = TZ.localize(hp.datetime.utcfromtimestamp(ret.pop('__updated')/1000.))
        else:
            ret.pop('__updated', None)
        return self._unmake_key(_key), ret

    def _delete(self, key, *args, **kwargs):
        _key = self._make_key(key)
        ret = self._set_col(**kwargs).find_one({'_id': _key})
        if not ret:
            return ret or {}
        key, ret = self._jsonify(ret, *args, **kwargs)
        res = self._set_col(**kwargs).delete_one({'_id': _key})
        return (ret or {}) if res.deleted_count else {}

    def _count(self, filter_by = None, condition_type=None, base_filter = None, filter_by_types = None, last_updated = None, updated_before = None, *args, **kwargs):        
        if not (filter_by or base_filter or last_updated or updated_before):
            return self._set_col(**kwargs).count()
        clauses = self._handle_clauses(filter_by = filter_by, condition_type = condition_type, filter_by_types = filter_by_types, last_updated = last_updated, updated_before = updated_before, *args, **kwargs)
        return self._set_col(**kwargs).find(clauses).count()

    def _commit(self):
        pass

    def _close(self, *args, **kwargs):
        self._conn.close()

    def _exists(self, name, *args, **kwargs):
        if name in self._db.list_collection_names():
            if all(self._get_key_def()):
                return True
        return False

    def _get_key_def(self):
        ret = self._mt.find_one({'_id': self._make_key(self.name)})
        if not ret:
            return None, None
        return ret['key_names'], [self._name2type[i] for i in ret['key_types']]

    def _get_last_updated(self):
        return self._mt[self._make_key(self.name)]['__updated']

    def _connect(self, trial = 0, dbname = None, **kwargs):
        try:
            if dbname is None: dbname = self.dbname
            if PEM_FILE:
                self._conn = pymongo.MongoClient(u'mongodb://{username}:{password}@{database_url}/?ssl=true&ssl_ca_certs={pem_file}&replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false'.format(username = USERNAME, password = PASSWORD, database_url = DATABASE_URL, pem_file = PEM_FILE))
            elif USERNAME or PASSWORD:
                self._conn = pymongo.MongoClient(u'mongodb://{username}:{password}@{database_url}'.format(username = USERNAME, password = PASSWORD, database_url = DATABASE_URL))
            else:
                self._conn = pymongo.MongoClient(u'mongodb://{database_url}'.format(database_url = DATABASE_URL))
            return self._conn[dbname]
        except Exception as e:
            hp.print_error(e)
            raise

    def _delete_many(self, filter_by = None, condition_type = 'AND', 
            filter_by_types = None, base_filter = None, last_updated = None, updated_before = None, *args, **kwargs
        ):
        clauses = self._handle_clauses(filter_by = filter_by, condition_type = condition_type, filter_by_types = filter_by_types, last_updated = last_updated, updated_before = updated_before, *args, **kwargs)
        ret = self._set_col(**kwargs).delete_many(clauses)
        return ret.deleted_count

    def _update_many(self, instance, filter_by, condition_type = 'AND', 
            filter_by_types = None, base_filter = None, last_updated = None, updated_before = None, *args, **kwargs
        ):
        if not (instance and isinstance(instance, dict)):
            return 0
        clauses = self._handle_clauses(filter_by = filter_by, condition_type = condition_type, filter_by_types = filter_by_types, last_updated = last_updated, updated_before = updated_before, *args, **kwargs)
        ret = self._set_col(**kwargs).update_many(clauses, {'$set': instance})
        return ret.modified_count


    def _filter(self, filter_by = None, sort_by = None, unique = None, condition_type=None, start = 0, limit = None, load_total = None, base_filter = None, filter_by_types = None, last_updated = None, updated_before = None, *args, **kwargs):
        clauses = self._handle_clauses(filter_by = filter_by, condition_type = condition_type, filter_by_types = filter_by_types, last_updated = last_updated, updated_before = updated_before, base_filter = base_filter, *args, **kwargs)
        sort_by = self._handle_sort_by(sort_by = sort_by)
        if isinstance(load_total, dict):
            cur = self._set_col(**kwargs).find(clauses)
            load_total['total_number'] = cur.count();
        cur = self._set_col(**kwargs).find(clauses);
        if sort_by:
            cur = cur.sort(*sort_by)
        if start:
            cur = cur.skip(start)
        if limit:
            cur = cur.limit(limit)
        for ret in cur:
            _key, ret = self._jsonify(ret, _timestamp = bool(last_updated or updated_before))
            logger.debug("Fetching record %s", ret)
            yield _key, ret

    def _handle_clauses(self, filter_by = None, condition_type = None, filter_by_types = None, last_updated = None, updated_before = None, base_filter = None, *args, **kwargs):
        if filter_by_types is None: filter_by_types = {}
        if condition_type and condition_type.upper() == 'OR':
            clauses = {'$or': [{k:v} for k, v in filter_by.items()]}
        else:
            clauses = hp.copyof(filter_by)
        if isinstance(base_filter, dict):
            clauses.update(base_filter);
        if last_updated:
            clauses['__updated'] = {'$gte': last_updated}
        if updated_before:
            clauses['__updated'] = {'$lte': last_updated}
        logger.info("Query clauses on collection %s in DB %s are: %s", self.name, self.dbname, clauses)
        return clauses

    def _handle_sort_by(self, sort_by):
        return sort_by


if __name__ == '__main__':
    d = mongodict('test', DB_NAME = 'cat', a = {'a': 'a', 'b': 'b'}, b = {'b': 'b'}, c = {'a': 3.0, 'b': 5}, d = {'a':  ['a','b','c']}, e = {'a': 1, 'b': 2}, f = {'a': {'a': 1, 'b': 2}, 'b': {'c': 3}})
    print "This is the document", d
    print "This is the value of key 'c'", d.get('c')
    d['c'] = {'a': 'saldka', 'c': 'skajfdf'}
    print "Length of collection", len(d)
    print "Popping c", d.pop('c')
    print "Popping unavailable g", d.pop('g', None)
    print "Length of collection", len(d)
    for k,v in d.iteritems({'a': ['a', 'b']}, sort_by = ('a', 1)):
        print 'filter', k, v
    d.clear()

