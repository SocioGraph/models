#import json
import os, sys
from libraries import json_tricks as json
from glob import glob
import operator
import helpers as hp
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as pathsep, getmtime as modified_time
from  attribute import Attribute
import pgpersist as db
from collections import OrderedDict
from stuf import stuf
from base64 import b64encode
import shutil
import validators as val
import decimal
from pgp_db import JsonBDict, PGCollection, db as pgpdb
try:
    from arango import ardict
except ImportError as e:
    ardict = JsonBDict
from copy import deepcopy as copyof
import traceback
from cacher import Cacher
from contextlib import contextmanager

logger = hp.get_logger(__name__)

PIVOT_SPLIT = u'--*--'
NULL_STRING = 'None'

class PivotJoinError(hp.I2CEError):
    pass

class PivotActionError(hp.I2CEError):
    pass

class PivotAttributeError(hp.I2CEError):
    pass

class PivotFormatError(hp.I2CEError):
    pass

class UnknownAttributeError(hp.I2CEError):
    pass

def check_filter_by(attributes, params=None, filter_by = None):
    if filter_by is None:
        filter_by = {}
    if params is None:
        return filter_by
    logger.debug("Before checking filter_by is : %s", params)
    for k, v in params.iteritems():
        not_clause = False
        if k.endswith('~'):
            k = k[:-1]
            not_clause = True
        if k not in attributes:
            # we don't want to filter by meta attributes
            if k.startswith('_'):
                continue
            if not_clause:
                filter_by[k+'~'] = hp.to_string_search(v)
            else:
                filter_by[k] = hp.to_string_search(v)
            continue
        a = attributes.get(k, None)
        if isinstance(v, (list, tuple)):
            if len(v) == 1:
                v = v[0]
        if isinstance(v, (str, unicode)):
            v = unicode(v.lower())
            if ',' in v:
                s = v.split(',')
                if len(s) == 2:
                    if getattr(a, 'atype', ['unicode'])[0] in (int, float, db.datetime, db.dtime,
                                      db.date):
                        if getattr(a, 'converter'):
                            v = tuple(
                                val.execute(a.converter, a, {}, k, s1)
                                if s1 else None for s1 in s)
                        else:
                            v = tuple(s)
                        filter_by[k] = v
                        continue
                elif len(s) == 3:
                    if getattr(a, 'atype',['unicode'])[0] in (db.geolocation,):
                        filter_by[k] = tuple(map(lambda x: x.strip(), s))
                        logger.info("Got geo location: %s", filter_by)
                        continue
                    logger.warning(
                        "Three tuple parameter for non-geolocation type. Ignoring {}".format(v)
                    )
            elif v == "~":
                continue
            else:
                if getattr(a, 'converter'):
                    try:
                        v = val.execute(a.converter, a, {}, k, v)
                    except Exception as e:
                        logger.warn(traceback.format_exc())
                        logger.warn("Error in converting filter value {} to attribute type {}".format(v, a.atype))
        if isinstance(v, list):
            if getattr(a, 'converter'):
                v = list(
                    val.execute(a.converter, a, {}, k, v1) for v1 in v)
            if getattr(a, 'atype', [unicode])[0] in [str, unicode]:
                v = list(v1.lower() for v1 in v)
        if not_clause:
            filter_by[k+'~'] = v
        else:
            filter_by[k] = v
    logger.debug("After checking filter_by is : %s", filter_by)
    return filter_by

def run_condition(a, k, v, v1, instance):
    """
    a: is of type attribute
    k: is key for instance or name of attribute
    v1: is the value of k from instance
    v: is the value of the search parameter
    instance: is the current object under consideration
    """
    not_clause = False
    if k.endswith('~'):
        k = k[:-1]
        not_clause = True
    if v == v1:
        return not not_clause
    if v is None and None not in hp.make_list(v1) or v1 is None and None not in hp.make_list(v):
        return not_clause
    if a.get('converter'):
        v1 = val.execute(a['converter'], a, instance, k, v1)
    if isinstance(v, (unicode, str)):
        v = v.lower()
    if unicode in a.get('atype',[unicode]) and isinstance(v1, (str, unicode)):
        v1 = v1.lower()
    if isinstance(v1, (list, tuple)):
        if any(_v in v1 for _v in hp.make_list(v)):
            return not not_clause
        v1 = [_v.lower() if isinstance(_v, (str, unicode)) else _v for _v in v1]
    if v == v1:
        return not not_clause
    if isinstance(v, list):
        if any(_v in map( lambda x: x.lower() if isinstance(x, (str, unicode)) else x, v) for _v in hp.make_list(v1)):
            return not not_clause
    if isinstance(v, tuple):
        if v[0] is None:
            return not_clause if v1 > v[1] else not not_clause
        elif v[1] is None:
            return not_clause if v1 < v[0] else not not_clause
        elif v1 < v[0] or v1 > v[1]:
            return not_clause
        else:
            return not not_clause
    if isinstance(v, (str, unicode)):
        v1 = hp.make_list(v1)
        if v.startswith('~'):
            v = v[1:]
            if v.startswith('"') and v.endswith('"'):
                v = v[1:-1]
            else:
                v = v.split('|')
            v = hp.make_list(v)
            if any(any(v_ in v1_ for v_ in v) for v1_ in v1):
                return not not_clause
        elif v.startswith('^'):
            v = v[1:]
            if any(v1_.startswith(v) for v1_ in v1):
                return not not_clause
        elif v.endswith('^'):
            v = v[:-1]
            if any(v1_.endswith(v) for v1_ in v1):
                return not not_clause
        elif v in v1:
            return not not_clause
        return not_clause
    if v1 != v:
        return not_clause
    return not not_clause

def run_filter(instance, attr = None, filter_by = None, condition_type = 'AND'):
    if not attr: attr = {}
    if filter_by is None:
        filter_by = {}
        condition_type = 'AND'
    if condition_type == 'AND':
        hit = True
    else:
        hit = False
    for k, v in filter_by.iteritems():
        a = attr.get(k, {})
        if isinstance(a, Attribute):
            a = a._stuf
        if k.endswith('~'):
            v1 = instance.get(k[:-1])
        else:
            v1 = instance.get(k)
        if k not in instance and k.startswith('_'):
            continue
        if run_condition(a, k, v, v1, instance):
            if condition_type == 'AND':
                continue
            return True
        else:
            if condition_type == 'AND':
                return False
            continue
    return hit


def run_filters(item_iter, attr, filter_by = None, condition_type = 'AND'):
    """
    item_iter is a key, value pair of id and instance
    attr is the attributes dict of the instances in item_iter
    filter_by gives the conditions dict for filtering and 
    condition_type tells whether the params are OR or AND
    """
    if not attr: attr = {}
    if filter_by is None:
        filter_by = {}
        condition_type = 'AND'
    for key, instance in item_iter:
        if run_filter(instance, attr, filter_by, condition_type):
            yield instance


class Objects(object):

    def __init__(self, path, parent):
        self.path = path
        self.parent = parent
        self._cacher = Cacher(self.parent.enterprise_id)

    def get(self, ids, default = None):
        raise db.NotImplementedError("get")

    def set(self, ids, **instance):
        raise db.NotImplementedError("set")
    
    def pop(self, ids, default = KeyError):
        raise db.NotImplementedError("pop")

    def get_last_updated(self):
        raise db.NotImplementedError("get_last_updated")
    
    def clear(self):
        raise db.NotImplementedError("clear")

    def link(self, id1, id2, _bi_directional = True, **instance):
        raise db.NotImplementedError("link")

    def unlink(self, id1, id2, _bi_directional = True, **instance):
        raise db.NotImplementedError("unlink")

    def closest(self, id1, id2 = None, **filters):
        raise db.NotImplementedError("closest")

    def filter(
            self, 
            filter_by = None, sort_by = None, condition_type = 'AND', reverse = False, 
            limit = None, start = 0, 
            do_function = None, base_filter = None, load_total = None,
            last_updated = None, updated_before = None
        ):
        raise db.NotImplementedError("filter")

    def iadd(self, ids, attribute, value = 1, default = None):
        """
        add the value to the existing attribute
        """
        if default is None: default = type(value)()
        j = self.get(ids, default)
        j[attribute] = j.get(attribute, default) + value
        ret = self.set(ids, **j)
        return ret[attribute]

    def count(self):
        load_total = {'total_number': 0}
        f = self.filter(load_total = load_total)
        return load_total['total_number']

    def update_many(self, instance, filter_by):
        count = 0
        for now_instance in self.filter(filter_by = filter_by):
            now_instance.update(instance)
            ids = self.parent.dict_to_ids(now_instance)
            self.set(ids, **now_instance)
            count += 1
        return count

    def delete_many(self, filter_by):
        count = 0
        for now_instance in self.filter(filter_by = filter_by):
            ids = self.parent.dict_to_ids(now_instance)
            self.pop(ids, None)
            count += 1
        return count
    
    def _sort_by_to_key(self, sort_by = None, items = False):
        if sort_by:
            if sort_by == '__timestamp':
                default = db.datetime.min
            else:
                default = self.parent.attributes[sort_by].atype[0]
                if issubclass(default, (db.date, db.datetime, db.dtime)):
                    default = default.min
                else:
                    default = default()
            if items:
                return lambda x: (x[1].get(sort_by, default) or default)
            return lambda x: (x.get(sort_by, default) or default)
        return None

    def list(self, filter_by = None, sort_by = None, **params):
        return list(self.filter(filter_by, sort_by, **params))

    def exists(self, **params):
        """
        Takes in attribute and value and returns the list of data if attribute has taken that value already
        TODO: Make route /exists/<model_name> POST data = {'attribute': value'}
        """
        if all(p in self.parent.ids for p in params):
            i = self.get(self.parent.dict_to_ids(params))
            if i:
                return dict((p, i[p]) for p in params)
            return False
        for i in self.filter(filter_by = params, limit = 1):
            return dict((p, i.get(p)) for p in params)
        return False

    def unique_values(self, attribute, start = 0, limit = None, load_total = None, *args, **kwargs):
        load_total = {'total_number': 0}
        r = self.pivot(hp.make_list(attribute), 'count', attribute, None, limit = limit, load_total = load_total)
        return r, load_total['total_number']

    def _to_resolution_func(self, resolution, att, atype = None, mod = None, params = None, mn = None, mx = None):
        if atype is None: atype = self.parent.attributes[att].atype[0]
        if mod is None: mod = self.parent  # this is the parent of object, i.e. model
        params = params or {}
        natt = '{}~'.format(att)
        if mn is None:
            s = mod.list(_internal = True, _sort_by = att, _page_size = 1, _filter_attributes = [att], **params) #, **{att+'~': None})
            if s:
                mn = hp.make_single(s, force = True, default = {}).get(att)
        if mn is None and att not in params and natt not in params:
            paramsn = params.copy()
            paramsn[natt] = None
            s = mod.list(_internal = True, _sort_by = att, _page_size = 1, _filter_attributes = [att], **paramsn)
            if s:
                mn = hp.make_single(s, force = True, default = {}).get(att)
        fname = 'resolution_func_{}_for_{}_range{}_{}'.format(resolution, att, mn, mx)
        if mn is None:
            r = lambda x: None
            r.baskets = [None]
            r.__name__ = fname
            return r
        if mx is None:
            s = mod.list(_internal = True, _sort_by = att, _page_size = 1, _filter_attributes = [att], _sort_reverse = True, **params) #, **{att+'~': None})
            if s:
                mx = hp.make_single(s, force = True, default = {}).get(att)
        if mx is None and att not in params and natt not in params:
            paramsn = params.copy()
            paramsn[natt] = None
            s = mod.list(_internal = True, _sort_by = att, _page_size = 1, _filter_attributes = [att], _sort_reverse = True, **paramsn)
            if s:
                mx = hp.make_single(s, force = True, default = {}).get(att)
        fname = 'resolution_func_{}_for_{}_range{}_{}'.format(resolution, att, mn, mx)
        if atype in (db.datetime, db.date, db.dtime):
            r = self._resolution_functions.get(resolution)
            if r:
                r.__name__ = fname
                b = self._baskets.get(resolution)
                if isinstance(b, (list, tuple)):
                    r.baskets = b
                elif isinstance(b, (int, float)):
                    logger.debug('creating date list')
                    r.baskets = hp.date_list(mn, mx, r, b)
                    logger.debug('creating date list: %s', r.baskets)
                else:
                    r.baskets = []
                return r
            resolution = db.timedelta(seconds = resolution)
        try:
            resolution = hp.to_float(resolution)
        except ValueError as err:
            hp.print_error(err)
            raise ValueError(
                 """
                 Resolution {} is not of known formats: 
                 Integer or time in seconds, dictionary for each attribute or one of {}
                 but only if atype is of type datetime
                 """.format(resolution, self._resolution_functions.keys())
            )
        baskets = []
        d = mn
        while True:
            if d > mx:
                break
            baskets.append((d, d + resolution, '{}--{}'.format(d, d+resolution)))
            bd = d
            d += resolution
            if bd == d:
                raise ValueError("Resolution too small to pivot")
            logger.debug("Adding to baskets: %s, %s", len(baskets), d)
        logger.debug("Baskets for %s are %s", fname, baskets)
        def resolution_function(inp):
            for bmin, bmax, bname in baskets:
                if inp >= bmin and inp < bmax:
                    return bname
        resolution_function.__name__ = fname
        resolution_function.baskets = map(lambda x:x[2], baskets)
        return resolution_function

    _resolution_functions = dict([
        ('month_of_year', lambda x: x.strftime('%b')),
        ('month', lambda x: x.strftime('%Y-%m (%b)')),
        ('monthly', lambda x: x.strftime('%Y-%m (%b)')),
        ('day_of_week', lambda x: x.strftime('%a')),
        ('week_of_year', lambda x: db.datetime.strptime(x.strftime('%W %Y 1'), '%W %Y %w').strftime('%Y-%m-%d')),
        ('weekly', lambda x: db.datetime.strptime(x.strftime('%W %Y 1'), '%W %Y %w').strftime('%Y-%m-%d')),
        ('year', lambda x: x.strftime('%Y')),
        ('hour_of_day',lambda x: x.strftime('%H:00')),
        ('2hours_of_day',lambda x: str(2*(x.hour//2)) + ":00"),
        ('3hours_of_day',lambda x: str(3*(x.hour//3)) + ":00"),
        ('4hours_of_day',lambda x: str(4*(x.hour//4)) + ":00"),
        ('quarter_of_day',lambda x: str(6*(x.hour//6)) + ":00"),
        ('half_of_day', lambda x: (x.strftime('%Y-%m-%d ') + str(12*(x.hour//12)) + ":00")),
        ('day', lambda x: x.strftime('%Y-%m-%d (%a)')),
        ('daily', lambda x: x.strftime('%Y-%m-%d (%a)')),
    ])
    
    _baskets = dict([
        ('month_of_year', ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']),
        ('month', 24*3600*15),
        ('monthly', 24*3600*15),
        ('day_of_week', ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']),
        ('week_of_year', 24*3600*4),
        ('weekly', 24*3600*4),
        ('year', 3600*24*365),
        ('hour_of_day', [_resolution_functions['hour_of_day'](db.dtime(i)) for i in range(24)]),
        ('2hours_of_day', [_resolution_functions['2hours_of_day'](db.dtime(i)) for i in range(0, 24, 2)]),
        ('3hours_of_day', [_resolution_functions['3hours_of_day'](db.dtime(i)) for i in range(0, 24, 3)]),
        ('4hours_of_day', [_resolution_functions['4hours_of_day'](db.dtime(i)) for i in range(0, 24, 4)]),
        ('quarter_of_day', [_resolution_functions['quarter_of_day'](db.dtime(i)) for i in range(0, 24, 6)]),
        ('half_day', 12*3600),
        ('day', 24*3600),
        ('daily', 24*3600)
    ])


    _action_functions = {
        'unique': {
            'condition': lambda *x: True,
            'gen': lambda *x: hp.gen_unique(),
        },
        'count': {
            'condition': lambda *x: True,
            'gen': lambda *x: hp.gen_count(),
        },
        'any': {
            'condition': lambda *x: True,
            'gen': lambda *x: hp.gen_any(),
        },
        'all': {
            'condition': lambda *x: True,
            'gen': lambda *x: hp.gen_all(),
        },
        'sum': {
            'condition': lambda obj, mod, over: issubclass(mod.attributes[over].atype[0],  (float, int, long)),
            'gen': lambda *x: hp.gen_sum(),
        },
        'average': {
            'condition': lambda obj, mod, over: issubclass(mod.attributes[over].atype[0],  (float, int, long)),
            'gen': lambda *x: hp.gen_average(),
        },
        'min': {
            'condition': lambda obj, mod, over: issubclass(mod.attributes[over].atype[0], (float, int, long, db.datetime, db.date, db.dtime, str, unicode)),
            'gen': lambda obj, mod, over: hp.gen_min(mod.attributes[over].atype[0]),
        },
        'max': {
            'condition': lambda obj, mod, over: issubclass(mod.attributes[over].atype[0], (float, int, long, db.datetime, db.date, db.dtime, str, unicode)),
            'gen': lambda obj, mod, over: hp.gen_max(mod.attributes[over].atype[0]),
        },
        'top3': {
            'condition': lambda *x: True,
            'gen': lambda obj, mod, over: hp.gen_top(3),
        },
        'top5': {
            'condition': lambda *x: True,
            'gen': lambda obj, mod, over: hp.gen_top(5),
        },
        'top10': {
            'condition': lambda *x: True,
            'gen': lambda obj, mod, over: hp.gen_top(10),
        },
        'top20': {
            'condition': lambda *x: True,
            'gen': lambda obj, mod, over: hp.gen_top(20),
        },
        'bottom10': {
            'condition': lambda *x: True,
            'gen': lambda obj, mod, over: hp.gen_top(-10),
        },
        'concat': {
            'condition': lambda *x: True,
            'gen': lambda *x: hp.gen_concat(),
        },
        'uniquelist': {
            'condition': lambda *x: True,
            'gen': lambda *x: hp.gen_set(),
        },
        'unique_list': {
            'condition': lambda *x: True,
            'gen': lambda *x: hp.gen_set(),
        },
        'object_list': {
            'condition': lambda *x: True,
            'gen': lambda *x: hp.gen_list(),
        },
        'unique_object_list': {
            'condition': lambda *x: True,
            'gen': lambda *x: hp.gen_unique_list(),
        },
        'join': {
            'condition': lambda obj, mod, over: mod.foreign_key_to_parent(over) is not None,
            'condition_error': '_over attribute is not a foreign key',
            'gen': lambda obj, mod, over: lambda x, y, **z: mod.foreign_key_to_parent(over, y, **z)
        },
    }

    def _pivot(
            self, attributes, action, over, resolution_functions = None,
            filter_by = None, condition_type = 'AND', sort_by = None, reverse = None,
            join_filter_attributes = None, join = None, base_filter = None,
            last_updated = None, updated_before = None
        ):
        def add_to_tuple(i, tup_list = None):
            if tup_list is None:
                tup_list = [u""]
            if isinstance(i, (list, tuple)):
                ret_tup = []
                for li in i:
                    r = add_to_tuple(li, copyof(tup_list))
                    ret_tup.extend(r)
                return ret_tup
            elif isinstance(i, (dict)):
                ret_tup = []
                for k, v in i.iteritems():
                    tup = add_to_tuple(k, copyof(tup_list))
                    ret_tup.extend(add_to_tuple(v, tup))
                return ret_tup
            for i_, ut in enumerate(tup_list):
                if ut:
                    ut += PIVOT_SPLIT
                ut += self._format_pivot_key(i)
                tup_list[i_] = ut
            return tup_list 
        def instance_to_key(instance):
            tuplist = None
            for a in attributes:
                i = instance.get(a)
                logger.debug("Got value %s from attribute %s", i, a)
                if i is not None and a in resolution_functions:
                    i = resolution_functions[a](i)
                    logger.debug("After resolution value %s from attribute %s", i, a)
                tuplist = add_to_tuple(i, tuplist)
            if tuplist is None and not attributes:
                return [over]
            return tuplist or []
        def instance_to_list(instance):
            tuplist = None
            for a in attributes:
                i = instance.get(a)
                logger.debug("Got value %s from attribute %s in instance_to_list", i, a)
                if a in resolution_functions and hasattr(resolution_functions[a], 'baskets'):
                    i = resolution_functions[a].baskets
                    logger.debug("After resolution value %s from attribute %s", i, a)
                tuplist = add_to_tuple(i, tuplist)
            return tuplist or []
        if resolution_functions is None: resolution_functions = {}
        if not action in self._action_functions:
            raise PivotActionError("Action {} is not supported currently under the pivot.".format(action))
        action_dict = self._action_functions[action]
        check_model = None
        if over in self.parent.attributes:
            check_model = self.parent
        elif join and over in self.parent.parents[join].attributes:
            check_model = self.parent.parents[join]
        if not check_model or not action_dict.get('condition', lambda *x: True)(self, check_model, over):
            raise PivotAttributeError(
                "Action {} cannot be taken with pivoting Attribute {} of type {}. {}".format(
                    action, over, check_model.attributes[over].atype[0] if check_model else 'Unknown',
                    action_dict.get('condition_error','')
                )
            )
        if action == 'join':
            join_model = self.parent.foreign_key_to_parent(over)
            if len(join_model.ids) != 1:
                raise PivotActionError(
                    "Pivot action 'join' is not possible over parent model {} for model {} over attribute {} since it has a combination id".format(
                        join_model.name, self.parent.name, over
                    )
                )
        action_func = action_dict.get('action', action_dict.get('gen', lambda *x: lambda *y, **z: y[1])(self, self.parent, over))
        for instance in self.filter(
                filter_by = filter_by, condition_type = condition_type, 
                sort_by = sort_by, reverse = reverse, base_filter = base_filter,
                last_updated = last_updated, updated_before = updated_before
            ):
            if join:
                j = self.parent.get_from_parent(instance, join, attributes = join_filter_attributes)
                for k, v in j.iteritems():
                    if k not in instance:
                        instance[k] = v
            logger.debug("Filtered instance : %s", instance)
            if hasattr(action_func, 'init'):
                for t in instance_to_list(instance):
                    action_func.init(t)
            if action in ['object_list', 'unique_object_list']:
                if join_filter_attributes:
                    ni = {_: instance.get(_) for _ in hp.make_list(join_filter_attributes)}
                else:
                    ni = instance.copy()
            else:
                ni = instance.get(over)
            for t in instance_to_key(instance):
                action_func(
                    t, ni, filter_attributes = join_filter_attributes,
                )
        return action_func.get()


    def _format_pivot_key(self, i):
        try:
            return u'{}'.format(i)
        except UnicodeEncodeError as e:
            return i.decode('utf-8', 'ignore')
        except UnicodeDecodeError as e:
            return i.encode('utf-8', 'ignore')

    def _split_pivot_key(self, key): 
        return [None if k_ == NULL_STRING else k_ for k_ in hp.make_list(key.split(PIVOT_SPLIT))]


    def pivot(
            self, attributes, action, over, resolution, start = 0, limit = None, 
            load_total = None, filter_by = None,
            condition_type = 'AND', sort_by = None, reverse = False,
            filter_attributes = None, filter_results = None, join = None,
            base_filter = None, last_updated = None, updated_before = None,
            output_format = 'nested', _fresh = False, _freshen = True
        ):
        if not load_total: load_total = {}
        if not resolution: resolution = {}

        def add_to_dict(ndict, key, value):
            nk = len(key)
            for index, t in enumerate(key):
                if index < nk - 1:
                    if t not in ndict:
                        ndict[t] = OrderedDict()
                    ndict = ndict[t]
                else:
                    ndict[t] = value
        if filter_results is None: filter_results = []
        logger.debug("filter_by in pivot is: %s", filter_by)
        cache_id = self._cacher.make_id(
            attributes + [self.parent.name, action, over], 
            {
                'resolution_functions':  {k_: v_.__name__ for k_, v_  in resolution.iteritems()}, 
                'filter_by': filter_by, 
                'condition_type': condition_type, 
                'sort_by': sort_by, 
                'reverse': reverse, 
                'join_filter_attributes': filter_attributes,
                'join': join,
                'base_filter': base_filter,
                'updated_before': updated_before,
                'last_updated': last_updated
            },
            'pivot'
        )
        return_dict = None
        if not _fresh:
            logger.info("Getting pivot from cache")
            return_dict = self._cacher.get(cache_id)
        if not return_dict:
            return_dict = self._pivot(
                attributes, action, over, resolution_functions = resolution,
                filter_by = filter_by, condition_type = condition_type, sort_by = sort_by, reverse = reverse,
                join_filter_attributes = filter_attributes, join = join, 
                base_filter = base_filter, last_updated = last_updated, updated_before = updated_before
            )
            logger.debug("Result_dict after iterating: %s", return_dict)
            if _freshen:
                logger.info("Cache id to save: %s", cache_id)
                self._cacher.set(cache_id, return_dict, shelf_life = _freshen if (
                        isinstance(_freshen, (float, int, long)) and not isinstance(_freshen, bool)
                    ) else None
                )
        if output_format in ['nested']:
            ndict = OrderedDict()
            load_total['total_number'] = len(set(map(lambda x: self._split_pivot_key(x)[0], return_dict)))
        elif output_format in ['ordered_dict']:
            ndict = OrderedDict()
            load_total['total_number'] = len(return_dict)
        elif output_format in ['table']:
            ndict = []
            load_total['total_number'] = len(return_dict)
        else:
            raise PivotFormatError("Unknown format {}".format(output_format))
        n = 0
        to_pop = {}
        ldict = len(return_dict)
        if isinstance(limit, (int, float)) and (ldict + limit < 0):
            logger.info("No records to be found")
            return ndict
        for key1, value in return_dict.iteritems():
            logger.debug("Getting in to dicts: %s, %s", key1, value)
            key = self._split_pivot_key(key1)
            for i, t in enumerate(filter_results):
                if hasattr(t, '__call__'):
                    t(key, value)
            if output_format == 'nested':
                add_to_dict(ndict, key, value)
                n = len(ndict)
                if start >= 0 and n <= start: 
                    to_pop[key[0]] = None
                if start < 0 and n <= ldict + start:
                    to_pop[key[0]] = None
            elif output_format == 'table':
                n += 1
                if start >= 0 and n <= start: 
                    continue
                if start < 0 and n <= ldict + start:
                    continue
                ndict.append(key + [value])
            elif output_format == 'ordered_dict':
                n += 1
                if start >= 0 and n <= start: 
                    continue
                if start < 0 and n <= ldict + start:
                    continue
                ndict[tuple(key)] = value
            if isinstance(limit, (int, float)):
                if limit > 0 and n > limit:
                    break
                if limit <= 0 and n > ldict + limit:
                    break
        for t in to_pop:
            ndict.pop(t)
        return ndict
        

    def list_unique_values(self, attribute, start = 0, limit = None, load_total = None):
        if load_total is None: load_total = {'total_number': 0.}
        u, t = self.unique_values(attribute, start = start, limit = limit, load_total = load_total)
        load_total['total_number'] = t
        return dict(u.items()[start:limit])


    delete = pop


    def delete_all(self):
        self.clear()

    def _run_filters(self, item_iter, filter_by, condition_type = 'AND', load_total = None, base_filter = None):
        if not load_total:
            load_total = {'total_number': 0.}
        attr = self.parent.attributes
        for instance in run_filters(item_iter, attr, filter_by, condition_type):
            if not base_filter or run_filter(instance, attr, base_filter):
                yield instance
                load_total['total_number'] += 1


class PGDictTypeObject(Objects):

    def _get_index_col(self, attribute):
        if any(issubclass(_, (str, unicode)) for _ in attribute.atype):
            return "(LOWER(dict->>'{}'::text))".format(attribute.name)
        if any(issubclass(_, (db.datetime, db.date)) for _ in attribute.atype):
            return "( CAST( dict#>>'{{{},timestamp}}' AS decimal ) )".format(attribute.name)
        return "(dict->>'{}')".format(attribute.name)

    def __init__(self, path, parent):
        super(PGDictTypeObject, self).__init__(path, parent)
        self.name = parent.name
        splitpaths = path.split(pathsep)
        self.db_name = parent.enterprise_id
        indexes = [
            (
                [self._get_index_col(a)],
                a.unique,
                [],
                attr,
            ) for attr, a in self.parent.attributes.iteritems() if ( a.unique or a.indexed and not a.id )
        ]
        indexes.extend([
            (
                [attr],
                False,
                [],
                attr,
            ) for attr, a in self.parent.attributes.iteritems() if a.id
        ])
        logger.debug("indexes in %s: %s", self.name, indexes)
        self._db = JsonBDict(
            self.name,
            [self.parent.attributes[i].atype[0] for i in self.parent.ids],
            self.parent.ids,
            indexes = indexes,
            DB_NAME = self.db_name,
            _update = True,
            retain_previous_indexes = parent._is_core_model
        )

    @contextmanager
    def _transaction(self):
        with self._db._transaction() as v:
            yield v

    def count(self):
        return len(self._db)

    def get(self, ids, default = None):
        return self._db.get(ids, default)

    def set(self, ids, **instance):
        self._db[ids] = instance
        return instance

    def pop(self, ids, default = KeyError):
        try:
            return self._db.pop(ids, default)
        except db.PersistKeyError as e:
            if isinstance(default, Exception):
                raise
            elif isinstance(default, type) and issubclass(default, Exception):
                raise default(e)
            return default

    def clear(self):
        return self._db.clear()

    def update_many(self, instance, filter_by):
        return self._db._update_many(instance, filter_by)

    def delete_many(self, filter_by):
        return self._db._delete_many(filter_by)

    def filter(
            self, 
            filter_by = None, sort_by = None, condition_type = 'AND', reverse = False, 
            limit = None, start = 0, 
            do_function = None, base_filter = None, load_total = None, 
            last_updated = None, updated_before = None
        ):
        """
        Returns an iterator for a list of records by filter
        filter_by is a dict of the format
        {
            '<attribute>': <value>          # Equality, Partial string match
            '<attribute>': [<v1>,<v2>,..]   # IN  (list)
            '<attribute>': "<min>,<max>"    # WITHIN RANGE
            '<attribute>': ",<max>"         # <=
            '<attribute>': "<min>,"         # >=
        }
        TODO: Write tests for the same
        """
        logger.debug("In filter, filter_by: %s, sort_by: %s, reverse: %s", filter_by, sort_by, reverse)

        if sort_by: 
            sort_by = (sort_by, reverse, self.parent.attributes.get(sort_by).atype[0].__name__ if sort_by in self.parent.attributes else 'unicode')
        for k, instance in self._db._filter(
                filter_by, condition_type=condition_type, sort_by = sort_by, start = start, limit = limit, load_total = load_total,
                base_filter = base_filter, filter_by_types = self.parent.atypes, last_updated = last_updated, updated_before = updated_before
            ):
            try:
                yield do_function(instance) if hasattr(do_function, '__call__') else instance
            except self.parent._NotPermitted as e:
                pass

    def unique_values(self, attribute, *args, **kwargs):
        value_type = self.parent.attributes[attribute].atype[0]
        return self._db._unique_values(attribute, value_type)

    def iadd(self, ids, attribute, value = 1, default = None):
        if default is None: default = type(value)()
        return self._db.iadd(ids, attribute, value, default)

    def get_last_updated(self):
        return self._db._get_last_updated()


class AttributeRecordTypeObject(Objects):

    def __init__(self, path, parent):
        super(AttributeRecordTypeObject, self).__init__(path, parent)
        splitpaths = self.path.split(pathsep)
        self.name = self.parent.name
        self.db_name = self.parent.enterprise_id
        uniques = [k for k, a in self.parent.attributes.iteritems() if a.unique]
        unique_types = [a.atype[0].__name__ for k, a in self.parent.attributes.iteritems() if a.unique]
        self._db = PGCollection(
                self.name,
                key_names = self.parent.ids,
                key_types = [self.parent.attributes[i].atype[0] for i in self.parent.ids],
                uniques = uniques,
                unique_types = unique_types,
                DB_NAME = self.db_name,
                _update = True,
        )

    @contextmanager
    def _transaction(self):
        with self._db._transaction() as v:
            yield v

    def count(self):
        return len(self._db)

    def get(self, ids, default = None):
        return self._db.get(ids)

    def set(self, ids, **instance):
        self._db.set(**instance)
        return instance

    def pop(self, ids, default = KeyError):
        try:
            return self._db.pop(ids, default)
        except db.PersistKeyError as e:
            if isinstance(default, Exception):
                raise default
            elif isinstance(default, type) and issubclass(default, Exception):
                raise default(e)
            return default

    def clear(self):
        return self._db.clear()

    def filter(
            self, 
            filter_by = None, sort_by = None, condition_type = 'AND', reverse = False, 
            limit = None, start = 0, 
            do_function = None, base_filter = None, load_total = None,
            last_updated = None, updated_before = None
        ):
        """
        filter_by is a dict of the format
        {
            '<attribute>': <value>          # Equality, Partial string match
            '<attribute>': [<v1>,<v2>,..]   # IN  (list)
            '<attribute>': "<min>,<max>"    # WITHIN RANGE
            '<attribute>': ",<max>"         # <=
            '<attribute>': "<min>,"         # >=
            sort_by      : <attribute>      # sort by attribute
            page_size    : <num records>    # default 50
            page_number  : <which page>     # default 1
            condition_type: <AND/OR>        # default AND
        }
        """
        i = 0
        sent = 0
        for instance in list(self._db._filter(filter_by,condition_type=condition_type, load_total = load_total, sort_by = (sort_by, reverse) if sort_by else None)):
            if last_updated or updated_before:
                instance['__timestamp'] = last_updated or updated_before
            if base_filter and not run_filter(instance, self.parent.attributes, base_filter):
                continue
            sent += 1
            if start >= 0 and i < start:
                i+=1
                continue
            if start < 0 and i < load_total['total_number'] + start:
                i+=1
                continue
            yield do_function(instance) if hasattr(do_function, '__call__') else instance
            if isinstance(limit, (int, float)):
                if limit > 0 and sent > limit:
                    raise StopIteration
                if limit <= 0 and sent > load_total['total_number'] + limit:
                    raise StopIteration



    def iadd(self, ids, attribute, value = 1, default = None):
        if default is None: default = type(value)()
        value_type = self.parent.attributes[attribute].atype[0]
        return self._db._iadd(ids, attribute, value_type, value, default)

    def unique_values(self, attribute, *args, **kwargs):
        value_type = self.parent.attributes[attribute].atype[0]
        return self._db._unique_values(attribute, value_type)

    def get_last_updated(self):
        return self._db._get_last_updated()




class JsonTypeObject(Objects):
    serializer = json
    suffix = '.json'

    def __init__(self, path, parent):
        super(JsonTypeObject, self).__init__(path, parent)
        hp.mkdir_p(dirname(path))
        self.path = joinpath(path + self.suffix)
        if not ispath(self.path):
            with open(self.path, 'wb') as fp:
                obj = self.serializer.dump({}, fp, indent = 4, encoding = "UTF-8")

    def get(self, ids, default = None):
        ids = '__'.join(map(str, hp.make_list(ids)))
        with open(self.path, 'rb') as fp:
            obj = self.serializer.load(fp, encoding = "UTF-8")
        return obj.get(ids, default)

    def set(self, ids, **instance):
        ids = '__'.join(hp.make_list(ids))
        with open(self.path, 'r+b') as fp:
            obj = self.serializer.load(fp, encoding = "UTF-8")
            obj[ids] = instance
            fp.seek(0)
            self.serializer.dump(obj, fp, indent = 4, encoding = "UTF-8")
            fp.truncate()
        return instance

    def pop(self, ids, default = KeyError):
        ids = '__'.join(hp.make_list(ids))
        with open(self.path, 'r+b') as fp:
            obj = self.serializer.load(fp)
            try:
                r = obj.pop(ids)
            except KeyError:
                if isinstance(default, Exception):
                    raise
                r = default
            else:
                fp.seek(0)
                self.serializer.dump(obj, fp, indent = 4, encoding = "UTF-8")
                fp.truncate()
            return r

    def clear(self):
        logger.debug('Deleting all objects in path: {}'.format(self.path))
        try:
            os.remove(self.path)
        except OSError:
            logger.warning("Could not find path %s. Probably already deleted?", self.path)


    def filter(
            self, 
            filter_by = None, sort_by = None, condition_type = 'AND', reverse = False, 
            limit = None, start = 0, 
            do_function = None, base_filter = None, load_total = None,
            last_updated = None, updated_before = None
        ):
        """
        Returns an iterator for a list of records by filter
        filter_by is a dict of the format
        {
            '<attribute>': <value>          # Equality, Partial string match
            '<attribute>': [<v1>,<v2>,..]   # IN  (list)
            '<attribute>': "<min>,<max>"    # WITHIN RANGE
            '<attribute>': ",<max>"         # <=
            '<attribute>': "<min>,"         # >=
        }
        TODO: Write tests for the same
        """
        with open(self.path, 'rb') as fp:
            obj = self.serializer.load(fp, encoding = "UTF-8")
        logger.debug("In filter, filter_by: %s", filter_by)
        i = 0
        sent = 0
        if last_updated and last_updated > hp.to_datetime(self.get_last_updated()):
            raise StopIteration
        if updated_before and updated_before < hp.to_datetime(self.get_last_updated()):
            raise StopIteration
        for instance in sorted(
                self._run_filters(
                    obj.iteritems(), filter_by, condition_type,
                    load_total
                ),
                key = self._sort_by_to_key(sort_by),
                reverse = reverse
            ):
            if last_updated: instance['__timestamp'] = self.get_last_updated()
            if updated_before: instance['__timestamp'] = self.get_last_updated()
            sent += 1
            if isinstance(start, (int, float)):
                if start >= 0 and i < start:
                    i+=1
                    continue
                if start < 0 and i < load_total['total_number'] + start:
                    i+=1
                    continue
            yield do_function(instance) if hasattr(do_function, '__call__') else instance
            if isinstance(limit, (int, float)):
                if limit > 0 and sent > limit:
                    raise StopIteration
                if limit <= 0 and sent > load_total['total_number'] + limit:
                    raise StopIteration

    def unique_values(self, attribute, *args, **kwargs):
        """
        unique values of an attribute as a dict
        """
        with open(self.path, 'rb') as fp:
            obj = self.serializer.load(fp)
        unique_list = {}
        total = 0
        for instance in obj.itervalues():
            if not instance.has_key(attribute):
                continue
            v = instance.get(attribute)
            if v not in unique_list:
                unique_list[v] = 1
            else:
                unique_list[v] += 1
            total += 1
        return unique_list, total
    
    
    def get_last_updated(self):
        return modified_time(self.path)



class DirectoryTypeObject(Objects):
    serializer = json
    suffix='.json'
    def __init__(self, path, parent):
        super(DirectoryTypeObject, self).__init__(path, parent)
        self.folder_path = path
        hp.mkdir_p(self.folder_path)

    def get(self, ids, default = None):
        ids = '__'.join(hp.make_list(ids))
        file_path=joinpath(self.folder_path, b64encode(ids) + self.suffix)
        if not ispath(file_path):
            return default

        with open(file_path, 'rb') as fp:
            obj = self.serializer.load(fp, encoding = "UTF-8")
        return obj

    def set(self, ids, **instance):
        ids = '__'.join(hp.make_list(ids))
        file_path=joinpath(self.folder_path, b64encode(ids) + self.suffix)

        with open(file_path, 'wb') as fp:
            self.serializer.dump(instance, fp, indent = 4, encoding = "UTF-8")
        return instance

    def pop(self, ids, default = KeyError):
        ids = '__'.join(hp.make_list(ids))
        file_path=joinpath(self.folder_path, b64encode(ids) + self.suffix)
        if not ispath(file_path):
            if isinstance(default, Exception):
                raise default("Ids: {} not found".ids)
            return default
        with open(file_path, 'rb') as fp:
            r = self.serializer.load(fp, encoding = "UTF-8")
        os.remove(file_path)
        return r

    def clear(self):
        logger.debug('Deleting object in Path: {}'.format(self.folder_path))
        try:
            shutil.rmtree(self.folder_path)
        except OSError:
            logger.warning("Could not find path %s. Probably already deleted?", self.path)


    def filter(
            self, 
            filter_by = None, sort_by = None, condition_type = 'AND', reverse = False, 
            limit = None, start = 0, 
            do_function = None, base_filter = None, load_total = None,
            last_updated = None, updated_before = None
        ):
        """
        Returns an iterator for a list of records by filter
        filter_by is a dict of the format
        {
            '<attribute>': <value>          # Equality, Partial string match
            '<attribute>': [<v1>,<v2>,..]   # IN  (list)
            '<attribute>': "<min>,<max>"    # WITHIN RANGE
            '<attribute>': ",<max>"         # <=
            '<attribute>': "<min>,"         # >=
        }
        TODO: Write tests for the same
        """

        def iteritems():
            for jsonfile in os.listdir(self.folder_path):
                jsonfilepath = joinpath(self.folder_path, jsonfile)
                file_id, _ = splitext(jsonfile)
                with open(jsonfilepath, 'rb') as fp:
                    f = self.serializer.load(fp, encoding = "UTF-8")
                    if last_updated or updated_before:
                        f['__timestamp'] = modified_time(jsonfilepath)
                    yield file_id, f 

        i = 0
        sent = 0
        if last_updated or updated_before:
            if filter_by is None: filter_by = {}
            filter_by['__timestamp'] = "{},".format(last_updated) if last_updated else ",{}".format(updated_before)
        for instance in sorted(
                self._run_filters(
                    iteritems(), filter_by, condition_type,
                    load_total
                ),
                key = self._sort_by_to_key(sort_by),
                reverse = reverse
            ):
            sent += 1
            if isinstance(start, (int, float)):
                if start >= 0 and i < start:
                    i+=1
                    continue
                if start < 0 and i < load_total['total_number'] + start:
                    i+=1
                    continue
            yield do_function(instance) if hasattr(do_function, '__call__') else instance
            if isinstance(limit, (int, float)):
                if limit > 0 and sent > limit:
                    raise StopIteration
                if limit <= 0 and sent > load_total['total_number'] + limit:
                    raise StopIteration

    def unique_values(self, attribute, *args, **kwargs):
        """
        Unique values of an attribute
        """
        def get_vlist(o):
            if isinstance(o,dict):
                for k,v in o.iteritems():
                    for i in get_vlist(v):
                        yield i
            elif isinstance(o,list):
                for v in o:
                    for i in get_vlist(v):
                        yield i
            else:
                yield o

        def iter_attribute():
            for jsonfile in os.listdir(self.folder_path):
                jsonfilepath = joinpath(self.folder_path, jsonfile)
                with open(jsonfilepath, 'rb') as fp:
                    yield self.serializer.load(fp, encoding = "UTF-8")
        unique_list = {}
        total = 0.
        for instance in iter_attribute():
            if not instance.has_key(attribute):
                continue
            for v in get_vlist(instance.get(attribute)):
                total += 1
                unique_list[v] = unique_list.get(v,0)+1
        return unique_list, total

    def get_last_updated(self):
        return modified_time(self.folder_path)



class GraphObject(Objects):

    def __init__(self, path, parent):
        super(GraphObject, self).__init__(path, parent)
        self.name = parent.name
        splitpaths = path.split(pathsep)
        self.db_name = parent.enterprise_id
        indexes = [
            (
                attr,
                a.id or a.unique,
                a.id or a.required,
                a.atype
            ) for attr, a in self.parent.attributes.iteritems() if a.unique or a.indexed or a.id
        ]
        logger.debug("indexes in %s: %s", self.name, indexes)
        self._db = ardict(
                self.name,
                [self.parent.attributes[i].atype[0] for i in self.parent.ids],
                self.parent.ids,
                indexes = indexes,
                DB_NAME = self.db_name,
                _update = True,
        )

    def count(self):
        return len(self._db)

    def _uncast(self, r):
        if not isinstance(r, dict):
            return r
        for k in self.parent.times:
            if k in r:
                try:
                    r[k] = hp.to_datetime(r[k])
                except:
                    r[k] = None
        return r

    def get(self, ids, default = None):
        return self._uncast(self._db.get(ids, default))

    def set(self, ids, **instance):
        self._db[ids] = instance
        return instance

    def pop(self, ids, default = KeyError):
        try:
            return self._db.pop(ids, default)
        except db.PersistKeyError as e:
            if isinstance(default, Exception):
                raise
            elif isinstance(default, type) and issubclass(default, Exception):
                raise default(e)
            return default

    def clear(self):
        return self._db.clear()


    def filter(
            self, 
            filter_by = None, sort_by = None, condition_type = 'AND', reverse = False, 
            limit = None, start = 0, 
            do_function = None, base_filter = None, load_total = None, 
            last_updated = None, updated_before = None
        ):
        """
        Returns an iterator for a list of records by filter
        filter_by is a dict of the format
        {
            '<attribute>': <value>          # Equality, Partial string match
            '<attribute>': [<v1>,<v2>,..]   # IN  (list)
            '<attribute>': "<min>,<max>"    # WITHIN RANGE
            '<attribute>': ",<max>"         # <=
            '<attribute>': "<min>,"         # >=
        }
        TODO: Write tests for the same
        """
        logger.debug("In filter, filter_by: %s, sort_by: %s, reverse: %s", filter_by, sort_by, reverse)

        if sort_by: 
            sort_by = (sort_by, reverse, self.parent.attributes.get(sort_by).atype[0].__name__ if sort_by in self.parent.attributes else 'unicode')
        for k, instance in self._db._filter(
                filter_by, condition_type=condition_type, sort_by = sort_by, start = start, limit = limit, load_total = load_total,
                base_filter = base_filter, filter_by_types = self.parent.atypes, last_updated = last_updated, updated_before = updated_before
            ):
            try:
                instance = self._uncast(instance)
                yield do_function(instance) if hasattr(do_function, '__call__') else instance
            except self.parent._NotPermitted as e:
                pass


    def get_last_updated(self):
        return self._db._get_last_updated()


    def update_many(self, instance, filter_by):
        return self._db._update_many(instance, filter_by)

    def delete_many(self, filter_by):
        return self._db._delete_many(filter_by)


    def link(self, id1, id2, *args, **kwargs):
        return self._db.link(id1, id2, *args, **kwargs)

    def unlink(self, id1, id2, *args, **kwargs):
        return self._db.unlink(id1, id2, *args, **kwargs)

    def closest(self, id1, id2 = None, *args, **kwargs):
        return self._db.closest(id1, id2, *args, **kwargs)

    
    #def unique_values(self, attribute):
    #    value_type = self.parent.attributes[attribute].atype[0]
    #    return self._db._unique_values(attribute, value_type)

    #def iadd(self, ids, attribute, value = 1, default = 0):
    #    if default is None: default = type(value)()
    #    return self._db.iadd(ids, attribute, value, default)

class RedisObject(Objects):

    def __init__(self, path, parent):
        super(RedisObject, self).__init__(path, parent)
        self.name = parent.name
        splitpaths = path.split(pathsep)
        self.db_name = parent.enterprise_id
        indexes = [
            (
                [self._get_index_col(a)] ,
                a.unique,
                [],
                attr,
            ) for attr, a in self.parent.attributes.iteritems() if a.unique or a.indexed
        ]
        logger.debug("indexes in %s: %s", self.name, indexes)
        self._db = JsonBDict(
                self.name,
                [self.parent.attributes[i].atype[0] for i in self.parent.ids],
                self.parent.ids,
                indexes = indexes,
                DB_NAME = self.db_name,
                _update = True,
        )

    def count(self):
        return len(self._db)

    def get(self, ids, default = None):
        return self._db.get(ids, default)

    def set(self, ids, **instance):
        self._db[ids] = instance
        return instance

    def pop(self, ids, default = KeyError):
        try:
            return self._db.pop(ids, default)
        except db.PersistKeyError as e:
            if isinstance(default, Exception):
                raise
            elif isinstance(default, type) and issubclass(default, Exception):
                raise default(e)
            return default

    def clear(self):
        return self._db.clear()

    def update_many(self, instance, filter_by):
        return self._db._update_many(instance, filter_by)

    def delete_many(self, filter_by):
        return self._db._delete_many(filter_by)

    def filter(
            self, 
            filter_by = None, sort_by = None, condition_type = 'AND', reverse = False, 
            limit = None, start = 0, 
            do_function = None, base_filter = None, load_total = None, 
            last_updated = None, updated_before = None
        ):
        """
        Returns an iterator for a list of records by filter
        filter_by is a dict of the format
        {
            '<attribute>': <value>          # Equality, Partial string match
            '<attribute>': [<v1>,<v2>,..]   # IN  (list)
            '<attribute>': "<min>,<max>"    # WITHIN RANGE
            '<attribute>': ",<max>"         # <=
            '<attribute>': "<min>,"         # >=
        }
        TODO: Write tests for the same
        """
        logger.debug("In filter, filter_by: %s, sort_by: %s, reverse: %s", filter_by, sort_by, reverse)

        if sort_by: 
            sort_by = (sort_by, reverse, self.parent.attributes.get(sort_by).atype[0].__name__ if sort_by in self.parent.attributes else 'unicode')
        for k, instance in self._db._filter(
                filter_by, condition_type=condition_type, sort_by = sort_by, start = start, limit = limit, load_total = load_total,
                base_filter = base_filter, filter_by_types = self.parent.atypes, last_updated = last_updated, updated_before = updated_before
            ):
            try:
                yield do_function(instance) if hasattr(do_function, '__call__') else instance
            except self.parent._NotPermitted as e:
                pass

    def iadd(self, ids, attribute, value = 1, default = 0):
        if default is None: default = type(value)()
        return self._db.iadd(ids, attribute, value, default)

    def get_last_updated(self):
        return self._db._get_last_updated()

