#!/usr/bin/python
import os, sys, json
from functools import wraps
import helpers as hp
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile
import model as base_model
import validators as val
from datetime import datetime,timedelta,date
import time
import traceback
from stuf import stuf
from collections import OrderedDict
import pgpersist as db

logger = hp.get_logger(__name__)

def get_rates_func(billing_plan = 'common'):
    with hp.read_file('data', 'billings', 'rates.json') as rates:
        return rates[billing_plan]

def get_pricing(enterprise):
    rates = get_rates_func(enterprise.get('deployment_scenario', 'common'))
    return {r: enterprise.get('pricing', {}).get(r, v) for r, v in rates.iteritems()}

def deduct_charges(enterprise_id, deduction_head, units = 1, rates = None, st = None):
    if st is None:
        st = setup(enterprise_id, role = 'admin')
    if rates is None:
        rates = get_pricing(st.enterprise)
    rate = rates.get(deduction_head, 0)
    if rate:
        billing_model = base_model.Model('i2ce_usage', 'core')
        try:
            i = billing_model.attributes['deduction_head']._stuf.options.index(deduction_head)
            title = billing_model.attributes['deduction_title']._stuf.options[i]
        except ValueError:
            title = hp.make_title(deduction_head)
        billing_model.post({
            'enterprise_id': enterprise_id,
            'rate': rate,
            'units': units,
            'deduction_title': title,
            'deduction_head': deduction_head
        })
        st.enterprise_model.iadd(enterprise_id, 'credits_left', - float(rate*units))
        st.enterprise_model.iadd(enterprise_id, 'total_spent', float(rate*units))
    return rate*units


class UnknownInteractionType(hp.I2CEError):
    pass

class InteractionStageError(hp.I2CEError):
    pass

class MetaModelError(hp.I2CEError):
    pass

class EnterpriseIdError(hp.I2CEError):
    pass

def setup(enterprise_id, role = 'admin', user_id = None, st = None, raise_time_attr = False):
    stt = time.time()
    stc = base_model.get_enterprise_cache(enterprise_id, role = role, user_id = user_id)
    if stc and st is None:
        logger.info("Getting setup interactions from cache: %s", time.time() - stt)
        return stc
    if st is None:
        st = stuf()
    st.enterprise_id = enterprise_id
    st.role = role
    st.user_id = user_id
    st.enterprise_model = base_model.Model('enterprise', 'core')
    st.enterprise = st.enterprise_model.get(enterprise_id)
    if not st.enterprise:
        raise EnterpriseIdError("No enterprise with id: {}".format(st.enterprise_id))
    if user_id is not None:
        st.user = base_model.Model(role, enterprise_id,role).get(user_id)
    else:
        st.user = None
    st.interaction_stage_model = base_model.Model('interaction_stage', enterprise_id, role)
    st.admin_model = base_model.Model('admin', 'core')
    st.login_models = [st.admin_model]
    #st.all_models = {}
    for model_type in ['interaction', 'product', 'customer', 'store']:
        for model in base_model.models(enterprise_id, _as_model = True, role = role, user_id = user_id, model_type = model_type):
            if hasattr(st, model_type + '_model_name') and getattr(st, model_type + '_model_name') != model.name:
                raise MetaModelError(
                    "Model name {} of type {} exists, but found another model {} of same type in enterprise".format(
                        getattr(st, model_type + '_model_name'), model_type, model.name, st.enterprise_id)
                )
            setattr(st, model_type + '_model', model)
            setattr(st, model_type + '_model_name', model.name)
    for model in base_model.models(enterprise_id, _as_model = True, role = role, user_id = user_id, has_login = True):
        st.login_models.append(model)

    for model_type in ['interaction', 'product', 'customer']:
        if not hasattr(st, model_type + '_model_name'):
            #logger.error("All models :: %s", st.all_models);
            raise MetaModelError(
                "Model of type {} does not exist for enterprise {}".format(model_type, st.enterprise_id)
            )
    st.stage_attr = st.interaction_model.parent_ids['interaction_stage']['self_ids'][0]
    st.interaction_stages = list(st.interaction_stage_model.filter(
        _as_option = True, 
        _sort_by = 'default_positivity_score',
        _reverse = True,
    ))
    st.all_models_list = base_model.models(enterprise_id, 'name', role=role, user_id=user_id)
    st.interaction_time_attribute = get_time_attr(st.interaction_stages, raise_error = raise_time_attr, enterprise_id = st.enterprise_id)
    st.interaction_stage_dict = {x['name']: x for x in st.interaction_stages}
    st.interaction_stage_names = [x['name'] for x in st.interaction_stages]
    st.experienced_stages = map(lambda x: x['name'], filter(lambda x: x['experienced_product'], st.interaction_stages))
    st.lowest_experienced_stage = get_lowest_experienced_stage(st.interaction_stage_dict)
    st.positive_stages = map(lambda x: x['name'], filter(lambda x: x['default_positivity_score'] > 0, st.interaction_stages))
    st.negative_stages = map(lambda x: x['name'], filter(lambda x: x['default_positivity_score'] < 0, st.interaction_stages))
    st.visible_stages = map(lambda x: x['name'], filter(lambda x: x.get('display', True), st.interaction_stages))
    st.quantity_stages = map(lambda x: x['name'], filter(lambda x: x.get('quantity_attributes'), st.interaction_stages))
    if st.interaction_model.attributes.get(st.interaction_time_attribute):
        st.time_attribute_atype = st.interaction_model.attributes.get(st.interaction_time_attribute).atype[0]
    else:
        st.time_attribute_atype = unicode
    st.interaction_price_attributes = st.interaction_model.get_attributes('name', type = 'price')
    st.interaction_number_attributes = st.interaction_model.get_attributes('name', type = ['price', 'number', 'tax', 'discount', 'currency'])
    st.now = hp.now() if hasattr(st.time_attribute_atype, 'now') else hp.date.today()
    if not all((st.product_model, st.customer_model, st.interaction_model)):
        return st
    st.product_id_attr = st.interaction_model.parent_ids[st.product_model.name]['self_ids'][0]
    st.customer_id_attr = st.interaction_model.parent_ids[st.customer_model.name]['self_ids'][0]
    st.interaction_stage_attr = st.interaction_model.parent_ids[st.interaction_stage_model.name]['self_ids'][0]
    base_model.update_enterprise_cache(enterprise_id, role, user_id, st)
    logger.info("Time taken to initialize interactions: %s", time.time() - stt)
    return st



def get_interactions(model_name, enterprise_id, role = None, ids = None, user_id = None, _st = None, **params):
    m = base_model.Model(model_name, enterprise_id = enterprise_id, role = role)
    if ids:
        obj = m.get(ids)
        if not obj:
            raise base_model.ObjectIdError("Ids {} not found in model {}".format(ids, model_name))
    st = setup(enterprise_id, role, user_id, raise_time_attr = True) if _st is None else _st
    if m.model_type == 'customer':
        attribute_names = 'product_attributes'
        other_model = st.product_model
    elif m.model_type == 'product':
        attribute_names = 'customer_attributes'
        other_model = st.customer_model 
    else:
        raise base_model.ModelTypeError("Interactions can be got only from models of model_type 'product' or 'customer'")
    selected_interaction_stages = hp.make_list(
        params.pop('_interaction_stage', []) or params.pop('interaction_stage', [])
    ) or (
        map(
            lambda x: x['name'], st.interaction_stages
        ) + ['__ALL__']
    )
    most_recent = params.pop('_most_recent', None) or params.pop('most_recent', False)
    if '_page_size' not in params and 'page_size' not in params:
        params['_page_size'] = 100
    search_params = {'_sort_by': st.interaction_time_attribute, '_reverse': True}
    search_params.update(params)
    st.interaction_model.parent_ids_to_dict(model_name, ids, search_params)
    return_dict = {}
    all_previous = {}
    quantity_dict = {}
    interaction_attributes = hp.make_list(params.pop('_interaction_attributes', []))
    # We need to go through all the interactions if most_recent is true
    if not most_recent:
        search_params[st.interaction_stage_attr] = []
        for index, stage in enumerate(st.interaction_stages):
            if stage['name'] not in selected_interaction_stages:
                continue
            search_params[st.interaction_stage_attr].append(stage['name']) 
    for i in st.interaction_model.filter(**search_params):
        stage = st.interaction_model.get_from_parent(i, 'interaction_stage')
        interaction_id = hp.make_single(st.interaction_model.dict_to_ids(i))
        try:
            index = st.interaction_stage_names.index(stage['name'])
        except ValueError:
            logger.warning("Stage %s not found found in interaction object", stage['name'], i)
            continue
        interaction = {
            'interaction_stage': stage['name'],
            'interaction_id': interaction_id,
            'interaction_timestamp': i.get(stage.get('interaction_time_attribute'))
        }
        if most_recent:
            prv = hp.make_tuple(
                st.interaction_model.get_parent_ids(
                    other_model.name, input_dict = i
                )
            )
            if prv in all_previous:
                continue
            all_previous[prv] = stage['name']
        if stage['name'] not in selected_interaction_stages:
            continue
        interaction['quantity_attributes'] = {}
        if stage['name'] not in quantity_dict:
            quantity_dict[stage['name']] = {}
        qa = set(st.interaction_number_attributes + stage.get('quantity_attributes',[]))
        interaction['quantity_attributes'] = {a: i.get(a) for a in qa}
        qd = quantity_dict[stage['name']]
        for a in qa:
            if not a in qd:
                qd[a] = 0
            qd[a] += i.get(a, 0)
        if '_count' not in qd: qd['_count'] = 0
        qd['_count'] += 1
        interaction['details'] = st.interaction_model.get_from_parent(i, other_model.name, quick_view = params.get('_quick_view'))
        interaction['customer_id'] = i.get(st.customer_id_attr)
        interaction['product_id'] = i.get(st.product_id_attr)
        interaction['next_istage'] = next_stage(st.interaction_stages, stage, index, display = False) 
        interaction['next_stage'] = next_stage(st.interaction_stages, stage, index) 
        ns = st.interaction_stage_model.get(interaction['next_stage'])
        interaction['next_stage_quantity_attributes'] = ns.get('quantity_attributes',[])
        interaction['next_stage_tagged_products'] = interaction['details'].get(ns.get('tagged_products_attribute'))
        interaction['previous_istage'] = previous_stage(st.interaction_stages, stage, index, display = False) 
        interaction['previous_stage'] = previous_stage(st.interaction_stages, stage, index) 
        ps = st.interaction_stage_model.get(interaction['previous_stage'])
        interaction['previous_stage_quantity_attributes'] = ps.get('quantity_attributes',[])
        interaction['previous_stage_tagged_products'] = interaction['details'].get(ps.get('tagged_products_attribute'))
        interaction['new_quantity_attributes'] = list(set(interaction['quantity_attributes'].keys()) - set(interaction['next_stage_quantity_attributes']))
        interaction['next_stage_new_attributes'] = ns.get('new_interaction_attributes', [])
        interaction['previous_stage_new_attributes'] = ps.get('new_interaction_attributes', [])
        for a in stage[attribute_names]:
            interaction['details'][a] = i.get(a)
        interaction['recommended'] = i.get('recommended') or i.get('channel', None) == 'recommendation'
        interaction['tagged_products'] = i.get(stage.get('tagged_products_attribute'))
        interaction['interaction_group'] = i.get(stage.get('interaction_group_attribute'))
        interaction['_interaction_attributes'] = {}
        for a in interaction_attributes:
            interaction['_interaction_attributes'][a] = i.get(a)
        if stage['name'] not in return_dict:
            return_dict[stage['name']] = []
        return_dict[stage['name']].append(interaction)
        if '__ALL__' in selected_interaction_stages:
            if '__ALL__' not in return_dict:
                return_dict['__ALL__'] = []
            return_dict['__ALL__'].append(interaction)
    return_dict['__TOTAL__'] = quantity_dict
    return return_dict


def get_first_positive_stage(interaction_stages, display = None):
    if display:
        s = [(x['default_positivity_score'], x['name'], x) for x in interaction_stages if x['default_positivity_score'] > 0 and x.get('display', True)]
    else:
        s = [(x['default_positivity_score'], x['name'], x) for x in interaction_stages if x['default_positivity_score'] > 0]
    if not s:
        raise InteractionStageError("None of the stages have a positive default positivity score")
    return min(s)

def get_first_negative_stage(interaction_stages, display = None):
    if display:
        s = [(x['default_positivity_score'], x['name'], x) for x in interaction_stages if x['default_positivity_score'] <= 0 and x.get('display', True)]
    else:
        s = [(x['default_positivity_score'], x['name'], x) for x in interaction_stages if x['default_positivity_score'] <= 0]
    if s:
        return max(s)
    return 0, '__NULL__', {}

def get_lowest_experienced_stage(interaction_stage_dict):
    if isinstance(interaction_stage_dict, (list, tuple)):
        interaction_stage_dict = {x['name']: x for x in interaction_stage_dict}
    try:
        l = min((x['default_positivity_score'], n)
                for n, x in interaction_stage_dict.iteritems()
                if x.get('experienced_product', False) and x['default_positivity_score'] > 0)
    except ValueError:
        return None
    return l[1]

def get_highest_interaction_stage(interaction_stage_dict):
    """
    gets the interaction stage for the highest positivity score
    """
    if isinstance(interaction_stage_dict, (list, tuple)):
        interaction_stage_dict = {x['name']: x for x in interaction_stage_dict}
    return max((x['default_positivity_score'], n)
               for n, x in interaction_stage_dict.iteritems())[1]

def get_positivity_score(enterprise_id, interaction_stage_dict, stage, instance = None, default = UnknownInteractionType):
    """
    x is the interaction instance
    """
    if isinstance(interaction_stage_dict, (list, tuple)):
        interaction_stage_dict = {x['name']: x for x in interaction_stage_dict}
    if not stage in interaction_stage_dict:
        if isinstance(default, type) and issubclass(default, Exception):
            raise UnknownInteractionType(
                "Interaction type {} is not defined for enterprise {}".format(
                    stage, enterprise_id
                )
            )
        return default
    s = interaction_stage_dict[stage]['default_positivity_score']
    if not interaction_stage_dict[stage].get('positivity_score_function'):
        return s
    f = interaction_stage_dict[stage]['positivity_score_function']
    qa = f.get('quantity_attribute')
    if not qa:
        return s
    if instance is None:
        instance = {}
    q = instance.get(qa)
    if not q:
        return s
    if q >= f.get('neutral_value', 0):
        r = (q - f.get('neutral_value', 0))/float(max(1e-32, f.get('max_value', 1) - f.get('neutral_value', 0)))
    else:
        r = (q - f.get('min_value', -1))/float(max(1e-32, f.get('neutral_value', 0) - f.get('min_value', -1)))
    return r


def get_quantities(interaction_stage_dict, interaction_stage_attr, instance):
    if isinstance(interaction_stage_dict, (list, tuple)):
        interaction_stage_dict = {x['name']: x for x in interaction_stage_dict}
    def _get_quantity_attrs(instance):
        return interaction_stage_dict.get(
            instance.get(interaction_stage_attr), {}
        ).get('quantity_attributes')
    return dict((q, instance.get(q, 1.))
                for q in (_get_quantity_attrs(instance) or []))

def get_quantity_stages(interaction_stage_dict):
    if isinstance(interaction_stage_dict, (list, tuple)):
        interaction_stage_dict = {x['name']: x for x in interaction_stage_dict}
    for s, a in interaction_stage_dict.iteritems():
        yield s, a.get('quantity_attributes', [])

def get_time_attr(interaction_stages, raise_error = False, enterprise_id = None):
    if isinstance(interaction_stages, (list, tuple)):
        interaction_time_attribute = [x.get('interaction_time_attribute') for x in interaction_stages if x.get("interaction_time_attribute")]
    else:
        interaction_time_attribute = [x.get('interaction_time_attribute') for k, x in interaction_stages.iteritems() if x.get("interaction_time_attribute")]
    interaction_time_attribute = set(interaction_time_attribute)
    if len(interaction_time_attribute) > 1:
        if raise_error:
            raise base_model.UnknownAttribute("Different interactions have different interaction_time attribute, cannot pivot")
        else:
            return tuple(interaction_time_attribute)[0]
    elif len(interaction_time_attribute) == 0:
        if enterprise_id:
            its = base_model.models(enterprise_id, 'name', type = ['datetime', 'date'])
            if len(its) > 1 or len(its) == 0:
                raise base_model.UnknownAttribute("No time attribute in interaction stages.")
            interaction_time_attribute = its[0]
        else:
            raise base_model.UnknownAttribute("No time attribute in interaction stages.")
    return tuple(interaction_time_attribute)[0]

def get_time_attr_stages(interaction_stage_dict):
    if isinstance(interaction_stage_dict, (list, tuple)):
        interaction_stage_dict = {x['name']: x for x in interaction_stage_dict}
    for s, a in interaction_stage_dict.iteritems():
        yield s, a.get('interaction_time_attribute', 'updated')

def get_time_attr_stage(interaction_stage_dict, stage):
    if isinstance(interaction_stage_dict, (list, tuple)):
        interaction_stage_dict = {x['name']: x for x in interaction_stage_dict}
    return interaction_stage_dict.get(stage).get('interaction_time_attribute', 'updated')

def get_product_tag_stage(interaction_stage_dict, stage):
    if isinstance(interaction_stage_dict, (list, tuple)):
        interaction_stage_dict = {x['name']: x for x in interaction_stage_dict}
    return interaction_stage_dict.get(stage).get('tagged_products_attribute')

def get_quantity_stage(interaction_stage_dict, stage):
    if isinstance(interaction_stage_dict, (list, tuple)):
        interaction_stage_dict = {x['name']: x for x in interaction_stage_dict}
    if stage == '__NULL__':
        return []
    if not stage in interaction_stage_dict:
        raise UnknownInteractionType("UnknownInteractionType {}".format(stage))
    return interaction_stage_dict[stage].get('quantity_attributes',[])

def get_new_attribute_stage(interaction_stage_dict, stage):
    if isinstance(interaction_stage_dict, (list, tuple)):
        interaction_stage_dict = {x['name']: x for x in interaction_stage_dict}
    if stage == '__NULL__':
        return []
    if not stage in interaction_stage_dict:
        raise UnknownInteractionType("UnknownInteractionType {}".format(stage))
    return interaction_stage_dict[stage].get('new_interaction_attributes',[])

def get_quantities_from_instance(interaction_stage_dict, interaction_stage_attr, instance):
    if isinstance(interaction_stage_dict, (list, tuple)):
        interaction_stage_dict = {x['name']: x for x in interaction_stage_dict}
    qattrs = interaction_stage_dict.get(instance[interaction_stage_attr]).get('quantity_attributes')
    if qattrs:
        return {q: instance.get(q, 0) for q in qattrs}
    return {}

def get_time_attr_from_instance(interaction_stage_dict, interaction_stage_attr, instance):
    if isinstance(interaction_stage_dict, (list, tuple)):
        interaction_stage_dict = {x['name']: x for x in interaction_stage_dict}
    tattr = interaction_stage_dict.get(instance[interaction_stage_attr]).get('interaction_time_attribute')
    if tattr:
        return {tattr: instance.get(tattr, datetime.now())}
    return {}


def get_last_interaction_stage(
        enterprise_id, role, product_id, customer_id, start_time = '', end_time = '', 
        user_id = None, st = None, interactions = None, recommended = None, **kwargs
    ):
    if st is None:
        st = setup(
            enterprise_id, 'admin', None
        )
    params = {'_sort_by': st.interaction_time_attribute, '_page_size': 1}
    params[st.interaction_time_attribute] = '{},{}'.format(start_time, end_time)
    st.interaction_model.parent_ids_to_dict(st.product_model.name, product_id, params)
    st.interaction_model.parent_ids_to_dict(st.customer_model.name, customer_id, params)
    if interactions is None:
        interactions = list( st.interaction_model.filter(**params) )
    product = st.product_model.get(product_id)
    stage = {}
    if len(interactions):
        i = interactions[0]
        stage = st.interaction_model.get_from_parent(i, 'interaction_stage')
    if not len(interactions) or not stage:
        psi, fps = tuple(get_first_positive_stage(st.interaction_stages, display = False)[1:])
        nsi, fns = tuple(get_first_negative_stage(st.interaction_stages, display = False)[1:])
        ps, fps = tuple(get_first_positive_stage(st.interaction_stages, display = True)[1:])
        ns, fns = tuple(get_first_negative_stage(st.interaction_stages, display = True)[1:])
        return {
            "product_id": hp.make_single(product_id),
            "customer_id": hp.make_single(customer_id),
            "stage": None,
            "interaction_stage": None,
            "quantity_attributes": {},
            "next_stage_quantity_attributes": fps.get('quantity_attributes',[]),
            "previous_stage_quantity_attributes": fns.get('quantity_attributes',[]),
            "next_stage_tagged_products": product.get(fps.get('tagged_products_attribute')),
            "next_stage_new_attributes": fps.get('new_interaction_attributes', []),
            "previous_stage_new_attributes": fns.get('new_interaction_attributes', []),
            "timestamp": None,
            "recommended": recommended,
            "next_stage": ps,
            "previous_stage": ns,
            "next_istage": psi,
            "previous_istage": nsi,
        }
    index = map(lambda x: x['name'], st.interaction_stages).index(stage['name'])
    rd = {
        "stage": stage['name'],
        "interaction_stage": stage['name'],
        "timestamp": i[st.interaction_time_attribute],
        "product_id": hp.make_single(product_id),
        "customer_id": hp.make_single(customer_id),
        "quantity_attributes": {a: i.get(a) for a in stage.get('quantity_attributes',[])},
        "next_stage": next_stage(st.interaction_stages, stage, index),
        "next_istage": next_stage(st.interaction_stages, stage, index, display = False),
        "previous_stage": previous_stage(st.interaction_stages, stage, index),
        "previous_istage": previous_stage(st.interaction_stages, stage, index, display = False),
        "recommended": i.get('recommended', False)
    }
    if rd['next_stage'] != '__NULL__':
        rd["next_stage_quantity_attributes"] = get_quantity_stage(st.interaction_stage_dict, rd['next_stage'])
        rd["next_stage_new_attributes"] = get_new_attribute_stage(st.interaction_stage_dict, rd['next_stage'])
        rd["next_stage_tagged_products"] = product.get(get_product_tag_stage(st.interaction_stage_dict, rd['next_stage']))
    if rd['previous_stage'] != '__NULL__':
        rd["previous_stage_quantity_attributes"] = get_quantity_stage(st.interaction_stage_dict, rd['previous_stage'])
        rd["previous_stage_new_attributes"] = get_new_attribute_stage(st.interaction_stage_dict, rd['previous_stage'])
    return rd

def is_recommended(enterprise_id, role, instance, user_id = None, st = None):
    if st is None:
        st = setup(enterprise_id, 'admin', None)
    stage = st.interaction_model.get_from_parent(instance, 'interaction_stage')
    params = {
        st.product_id_attr: st.interaction_model.parent_dict_to_ids(st.product_model.name, instance),
        st.customer_id_attr: st.interaction_model.parent_dict_to_ids(st.customer_model.name, instance),
    }
    r = base_model.Model('recommendation', enterprise_id)
    rl = list(r.filter(_as_option = True, _sort_by = 'timestamp', _reverse_sort = True, _page_size = 1, **params))
    if not len(rl):
        return {}
    return {
        'recommended': True,
        'quantity_attributes': rl[0].get('quantity_predictions', {})
    }


def set_interaction(enterprise_id, role, instance, user_id = None):
    if instance.get('_channel') or instance.get('channel'):
        channel = instance.get('_channel') or instance.get('channel')
        if channel in ['recommendation', 'recommended']:
            instance['recommended'] = True
        else:
            instance['recommended'] = False
    elif is_recommended(enterprise_id, 'admin', instance):
        instance['recommended'] = True
    else:
        instance['recommended'] = False
    return instance

def before_interaction(act, instance, st = None):
    if st is None:
        st = setup(act.enterprise_id, 'admin', None)
    stage = instance.get(st.interaction_stage_attr)
    actions = st.interaction_stage_dict.get(stage,{}).get('pre_actions') or []
    for i, a in enumerate(actions):
        yield a, i

def after_interaction(act, instance, st = None):
    if st is None:
        st = setup(act.enterprise_id, 'admin', None)
    stage = instance.get(st.interaction_stage_attr)
    if stage in st.experienced_stages:
        deduct_charges(act.enterprise_id, 'customer_interaction', 1, st = st)
    if instance.get('recommended', False):
        units = get_positivity_score(st.enterprise_id, st.interaction_stage_dict, stage, instance, default = 0)
        if units > 0:
            deduct_charges(act.enterprise_id, 'customer_conversion', units, st = st)
    actions = st.interaction_stage_dict[stage].get('post_actions') or []
    for i, a in enumerate(actions):
        yield a, i


def set_customer(enterprise_id, role, instance, loyal_interactions = None, user_id = None, st = None):
    if st is None:
        st = setup(enterprise_id, 'admin', None)
    params = {}
    customer_id = st.interaction_model.parent_dict_to_ids(st.customer_model.name, instance)
    customer = st.customer_model.get(customer_id)
    loyal_interactions = loyal_interactions or get_preference(enterprise_id, 'loyal_interactions', 5, st)
    customer_lifetime_stage = get_preference(enterprise_id, 'customer_lifetime_stage', 'customer_lifetime_stage', st)
    stage = customer.get(customer_lifetime_stage)
    prev_stage = stage
    qualified_stages = hp.make_list(get_preference(enterprise_id, 'qualified_stages', None, st) or st.positive_stages)
    experienced_stages = hp.make_list(get_preference(enterprise_id, 'purchased_stages', None, st) or st.experienced_stages)
    stages_map = {x: get_preference(st.enterprise_id, x, x, st) for x in ["new","follow_up", "qualified","customer","repeat","loyal","loosing","lost"]}
    stages_inv_map = {v: k for k, v in stages_map.iteritems()}
    if not stage or stage == '__NULL__':
        stage = stages_map['new']
    if stage in [
            stages_map['new'],
            stages_map['loosing'],
            stages_map['lost']
        ]:
        if instance[st.interaction_stage_attr] in experienced_stages:
            stage = stages_map['customer']
        elif instance[st.interaction_stage_attr] in qualified_stages:
            stage = stages_map['qualified']
    elif stage in [
            stages_map['customer'],
            stages_map['repeat']
        ]:
        params[st.interaction_time_attribute] = '{},'.format(st.now - timedelta(days = get_preference(enterprise_id, 'lost_days', 90, st)))
        intrs = st.interaction_model.list(_sort_by = st.interaction_time_attribute, _sort_reverse = True, _as_option = True, **params)
        num_exp = sum(i[st.interaction_stage_attr] in st.experienced_stages for i in intrs)
        logger.debug("Loyal interactions: %s, %s", loyal_interactions, type(loyal_interactions))
        if num_exp >= loyal_interactions - 1 and instance[st.interaction_stage_attr] in experienced_stages:
            stage = stages_map['loyal']
        elif num_exp >= 1 and instance[st.interaction_stage_attr] in experienced_stages:
            stage = stages_map['repeat']
    logger.info("%s for customer %s is %s in enterprise_id %s", customer_lifetime_stage, customer, stage, enterprise_id)
    return prepare_lifetime_action_list(st, prev_stage, stage, customer, **{
        "receiver_ids": customer_id, 
        "receiver_model": st.customer_model.name, 
        "stages_map": stages_map, 
        "stages_inv_map": stages_inv_map
    })

def prepare_lifetime_action_list(st, previous_stage, next_stage, customer, **params):
    stages_map = params.pop(
        "stages_map", {
            x: get_preference(
                st.enterprise_id, x, x, st
            ) for x in ["new", "follow_up", "qualified", "customer", "repeat", "loyal", "loosing", "lost"]
        }
    )
    stages_inv_map = params.pop("stages_inv_map", {v: k for k, v in stages_map.iteritems()})
    enabled = get_preference(st.enterprise_id, "enable_campaign", False, st=st)
    customer_lifetime_stage = get_preference(st.enterprise_id, 'customer_lifetime_stage', 'customer_lifetime_stage', st)
    stages = get_preference(
        st.enterprise_id, "customer_lifetime_stages_list", 
        ["new", "follow_up", "qualified", "customer", "repeat", "loyal", "loosing", "lost"], st = st
    )
    customer_id = st.customer_model.dict_to_ids(customer)
    ac_list = []
    if next_stage != previous_stage and next_stage != "__NULL__":
        ac_list.append({
            "_action": "update",
            "_model": st.customer_model.name,
            "_instance": {
                customer_lifetime_stage: next_stage
            },
            "_id": customer_id
        })
    if 'follow_up' in stages:
        fuda = get_preference(st.enterprise_id, "follow_up_date_attribute", None, st = st)
        if fuda and customer.get(fuda) and hp.to_datetime(customer.get(fuda)).date() == hp.date.today():
            if previous_stage == next_stage:
                # If follow up is to be done, but customer has not been updated recently, we update it now
                st.customer_model.update(customer_id, {})
            data = get_preference(st.enterprise_id, "{}_stage_data".format("follow_up"),None, st = st)
            acs = {
                '_action': 'communicate',
                '_name': 'customer lifetime stage campaign',
                'html_template': data.pop("path", None),
                'message': """
                    {}. Please visit {{personalized_url}} to see in browser.
                    """.format(
                        get_preference(st.enterprise_id, "{}_stage_message".format("follow_up"),None, st=st)
                    ),
                'enterprise_id': st.enterprise_id,
                'enterprise_name': st.enterprise['name'],
                'subject': get_preference(st.enterprise_id, "{}_stage_mail_subject".format("follow_up"),None, st = st),
                'channels': get_preference(st.enterprise_id, "{}_stage_chanels".format("follow_up"),None, st = st),
                'data': data,
                'customer': customer,
                'enterprise': st.enterprise
            }
            acs.update(params)
            if "push_notification" in get_preference(st.enterprise_id, "{}_stage_chanels".format("follow_up"),None, st = st):
                acs['notification_data'] = {
                    "_action": "open",
                    "_page": "customer",
                    "_id": "{{customer.{}}}".format(hp.make_single(st.customer_model.ids))
                }
            fa = get_preference(st.enterprise_id, "follow_up_receiver_attributes", None, st = st)
            if fa:
                acs.update({
                    "receiver": {
                        "emails": [customer.get(a) for a in st.customer_model.emails if a in fa and customer.get(a)],
                        "phone_numbers": [customer.get(a) for a in st.customer_model.mobile_numbers if a in fa and customer.get(a)],
                        "push_tokens": [customer.get(a) for a in st.customer_model.push_tokens if a in fa and customer.get(a)]
                    }
                })
                acs.pop("receiver_model", None)
                acs.pop("receiver_ids", None)
            ac_list.append(acs)
    changed_stage = stages_inv_map.get(next_stage)
    if not enabled or changed_stage not in stages or previous_stage == next_stage or next_stage == "__NULL__":
        return ac_list
    data = get_preference(st.enterprise_id, "{}_stage_data".format(changed_stage), None, st = st)
    acs = {
        '_action': 'communicate',
        '_name': 'customer lifetime stage campaign',
        'html_template': data.pop("path", None),
        'message': """
            {}. Please visit {{personalized_url}} to see in browser.
            """.format(
                get_preference(st.enterprise_id, "{}_stage_message".format(changed_stage),None, st=st)
            ),
        'enterprise_id': st.enterprise_id,
        'enterprise_name': st.enterprise['name'],
        'subject': get_preference(st.enterprise_id, "{}_stage_mail_subject".format(changed_stage),None, st=st),
        'channels': get_preference(st.enterprise_id, "{}_stage_chanels".format(changed_stage),None, st=st),
        'customer': customer,
        'data': data,
        'enterprise': st.enterprise
    }
    acs.update(params)
    if "push_notification" in get_preference(st.enterprise_id, "{}_stage_chanels".format(changed_stage),None, st = st):
        acs['notification_data'] = {
            "_action": "open",
            "_page": "customer",
            "_id": "{{customer.{}}}".format(hp.make_single(st.customer_model.ids))
        }
    fa = get_preference(st.enterprise_id, "{}_receiver_attributes".format(changed_stage), None, st = st)
    if fa:
        acs.update({
            "receiver": {
                "emails": [customer.get(a) for a in st.customer_model.emails if a in fa and customer.get(a)],
                "phone_numbers": [customer.get(a) for a in st.customer_model.mobile_numbers if a in fa and customer.get(a)],
                "push_tokens": [customer.get(a) for a in st.customer_model.push_tokens if a in fa and customer.get(a)]
            }
        })
        acs.pop("receiver_model", None)
        acs.pop("receiver_ids", None)
    ac_list.append(acs)
    logger.info(get_preference(st.enterprise_id, "{}_stage_chanels".format(changed_stage),None, st=st))
    return ac_list


def get_preference(enterprise_id, name, default, st = None):
    if st is None:
        st = setup(enterprise_id, 'admin', None)
    return st.enterprise.get(
    'preferences', {}
    ).get(
        'customer_lifetime_stages', {}
    ).get(
        name, default
    )

def get_customer_stage(enterprise_id, role, customer_id, loyal_interactions = None, lost_days = None, losing_days = None, st = None):
    if st is None:
        st = setup(enterprise_id, 'admin', None)
    loyal_interactions = loyal_interactions or get_preference(enterprise_id, 'loyal_interactions', 5, st)
    lost_days = lost_days or get_preference(enterprise_id, 'lost_days', 90, st)
    losing_days = losing_days or get_preference(enterprise_id, 'losing_days', 30, st)
    params = {i:c for i, c in zip(st.customer_model.ids, hp.make_list(customer_id))}
    intrs = st.interaction_model.list(_sort_by = st.interaction_time_attribute, _sort_reverse = True, _as_option = True, _page_size = 1, _internal = True, **params)
    if not intrs:
        return get_preference(enterprise_id, 'new', 'new', st)
    if intrs[0][st.interaction_time_attribute] < st.now - timedelta(days = lost_days):
        return get_preference(enterprise_id, 'lost', 'lost', st)
    if intrs[0][st.interaction_time_attribute] < st.now - timedelta(days = losing_days):
        return get_preference(enterprise_id, 'losing', 'losing', st)
    params[st.interaction_stage_attr] = st.experienced_stages
    params[st.interaction_time_attribute] = "{},{}".format(st.now - timedelta(days = losing_days), st.now)
    num_exp = st.interaction_model.list(_sort_by = st.interaction_time_attribute, _sort_reverse = True, _as_option = True, _page_size = loyal_interactions, _internal = True, **params)
    if num_exp >= loyal_interactions:
        return get_preference(enterprise_id, 'loyal', 'loyal', st)
    if num_exp >= 2:
        return get_preference(enterprise_id, 'repeat', 'repeat', st)
    if num_exp:
        stage = get_preference(enterprise_id, 'customer', 'customer', st)
    return get_preference(enterprise_id, 'qualified', 'qualified', st)

def _next_stage_filter(interaction_stages, stage, index, null_stage = '__NULL__', next_ = True, **kwargs):
    if next_:
        r = range(index, 0, -1)
        s = -1
    else:
        r = range(index, len(interaction_stages)-1)
        s = 1
    interaction_stage_dict = {x['name']: x for x in interaction_stages}
    for i in r:
        ns = stage.get('next_stage' if next_ else 'previous_stage', interaction_stages[i+s]['name'])
        if ns == null_stage:
            return ns
        stage = interaction_stage_dict[ns]
        is_matched = True
        for k, v in kwargs.iteritems():
            if stage.get(k) != v:
                is_matched = False
        if is_matched:
            return ns
    else:
        ns = stage.get('next_stage' if next_ else 'previous_stage', null_stage)
        if ns == null_stage:
            return ns
        stage = interaction_stage_dict[ns]
        for k, v in kwargs.iteritems():
            if stage.get(k) != v:
                return null_stage
        return ns
    return null_stage

def next_stage(interaction_stages, stage, index, null_stage = '__NULL__', **kwargs):
    return _next_stage_filter(interaction_stages, stage, index, null_stage, **kwargs)

def previous_stage(interaction_stages, stage, index, null_stage = '__NULL__', **kwargs):
    return _next_stage_filter(interaction_stages, stage, index, null_stage, False, **kwargs)

def get_conversion_rates(
        enterprise_id, role = None,  stage = None, start_time = '',  end_time = '', 
        recommended = None, 
        user_id = None, st = None, 
        **params
    ):
    if st is None:
        st = setup(
            enterprise_id, 'admin', None
        )
    if stage:
        params[st.stage_attr] = hp.make_list(stage)
    if recommended is not None:
        params['recommended'] = True
    params[st.interaction_time_attribute] = '{},{}'.format(start_time, end_time)
    return_dict = OrderedDict()
    num_stages = len(st.interaction_stages)
    for i, stage in enumerate(sorted(st.interaction_stages, key = lambda x: x['default_positivity_score'])):
        ns = next_stage(st.interaction_stages, stage, i)
        given_next_stage = stage.get('next_stage', '__NULL__')
        previous_stage = stage.get('previous_stage', '__NULL__')
        if stage.get('default_positivity_score', 0) >= 0:
            if ns != '__NULL__' and ns != stage['name']:
                return_dict[(stage['name'], ns)] = [0., 0., True]
        else:
            if given_next_stage != '__NULL__':
                return_dict[(stage['name'], given_next_stage)] = [0., 0., True]
        if previous_stage != '__NULL__':
            return_dict[(stage['name'], previous_stage)] = [0., 0., False]
    summed = st.interaction_model.pivot(
        [st.customer_id_attr, st.product_id_attr, st.stage_attr],
        _sort_by = st.interaction_time_attribute, _reverse_sort = True,
        _page_size = 1e10,
        _as_option = True, **params
    )
    for c, cv in summed.iteritems():
        for p, pv in cv.iteritems():
            for st1, st2 in return_dict.keys():
                return_dict[(st1, st2)][0] += pv.get(st2, 0)
                return_dict[(st1, st2)][1] += pv.get(st1, 0)
    return_list = []
    for st1, st2 in return_dict.keys():
        return_list.append([st1, st2, return_dict[(st1, st2)][0], return_dict[(st1, st2)][1], return_dict[(st1, st2)][2]])

    return return_list

        




