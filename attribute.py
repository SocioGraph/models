import os, sys, json
import helpers as hp
from copy import deepcopy as copyof, copy
from stuf import stuf
import string
import pgpersist as db
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, getmtime as modified_time
import validators as val
from urlparse import urlparse, parse_qs
import time

logger = hp.get_logger(__name__)

class ModelNotFound(hp.I2CEError):
    def __init__(self, path, *args, **kwargs):
        logger.warn("Model not found in path: {}".format(path))
        super(ModelNotFound, self).__init__(*args, **kwargs)

class ReferenceError(hp.I2CEError):
    pass

class AttributeUrlParseError(hp.I2CEError):
    pass

class AttributeAutoUpdaterError(hp.I2CEError):
    pass

class RelationParentAttributeError(hp.I2CEError):
    pass

class AttributeTypeError(hp.I2CEError):
    
    def __init__(self, name, value, cls = None, model_name = None, message = 'Unknown format'):
        super(AttributeTypeError, self).__init__(
            """
            {} = {} is for unknown format or not allowed.
            attribute: {}
            model: {}
            error: {}
            """.format(
                name, value,
                cls.name if cls else name, cls.model.name if cls else model_name,
                message
            )
        )

class AttributeOptionsError(hp.I2CEError):
    
    def __init__(self, name, value, cls):
        super(AttributeOptionsError, self).__init__(
            """
            {} = {} is not among the options.
            attribute: {}
            model: {}
            """.format(
                name, value,
                cls.name, cls.model.name
            )
        )

class ValueUniqueError(hp.I2CEError):
    pass

class AttributeAutoUpdaterError(hp.I2CEError):
    pass

class AttributeValueError(hp.I2CEError):
    
    def __init__(self, name, value, cls):
        super(AttributeValueError, self).__init__(
            """
            {} for value {} is not understandable.
            attribute: {}
            model: {}
            """.format(
                name, value,
                cls.name, cls.model.name
            )
        )

class ValueConstraintError(hp.I2CEError):
    def __init__(self, value, constraint, cls, exception):
        super(ValueConstraintError, self).__init__(
            """
            Value {} does not follow constraint {}.
            attribute: {}
            model: {}
            exception: {}
            """.format(
                value, constraint,
                cls.name, cls.model.name, exception
            )
        )


class AttributeNameError(hp.I2CEError):
    pass


class ValidatorOptionsError(hp.I2CEError):
    pass

def to_unique_name(model, config):
    return '_'.join([model._model_name, 'attribute', config.get('name', hp.normalize_join(config.get('title')))])

class MetaAttribute(type):
    """
    Don't reload unless it has been updated
    """
    all_instances = {}
    def __call__(cls, model, config, forced = False):
        # TODO: check if name is not restricted and is singular
        name = to_unique_name(model, config)
        filepath = model.filepath
        if not forced and name in MetaAttribute.all_instances and ispath(filepath):
            mtime = modified_time(filepath)
            if MetaAttribute.all_instances[name][0] >= mtime:
                return MetaAttribute.all_instances[name][1]
        try:
            mtime = modified_time(filepath)
        except OSError:
            mtime = time.time()
        r = type.__call__(cls, model, config)
        MetaAttribute.all_instances[r._unique_name] = (mtime, r)
        return r



class Attribute(object):

    __metaclass__ = MetaAttribute
    serializer = json
    with open(joinpath(dirname(__file__), 'data', 'core', 'models', 'attribute.json'), 'rb') as fp:
        _attributes = stuf()
        _attr_seq = []
        _ids = []
        _names = []
        obj = serializer.load(fp, encoding = "UTF-8")
        for a in obj['attributes']:
            _attr_seq.append(a['name'])
            _attributes[a['name']] = a
        _ids = obj['ids']

    _name2type_dict = dict((o.__name__, o) for o in db.pgpdict._allowed_value_types)

    def _to_contains(self, func):
        """
        returns an object where func is converted to a '__contains__' method 
        """
        as_process = False
        if val.is_validator(func):
            as_process = True
        elif isinstance(func, (list, dict, tuple)):
            func = func.__contains__
        class Container(object):

            def __init__(self, model):
                self.model = model
            
            def __contains__(self, inp):
                if as_process:
                    return val.execute(func, self.model, {}, 'attribute', inp)
                else:
                    return func(inp)

            def __repr__(self):
                return "Function: {}, as_process: {}".format(func, as_process)

        return Container(self)

    def __repr__(self):
        return "{} with name '{}' for model '{}' (type = {})".format(self.__class__.__name__, self.name, self.model.name, self._stuf.type)
    
    def __init__(self, model, config, **kwargs):
        if not config.get('name') and not config.get('title'):
            raise AttributeNameError("Attribute config {} definition should have a 'name' key or a 'title' key".format(config))
        self._unique_name = to_unique_name(model, config)
        self.name = config.get('name', hp.normalize_join(config.get('title')))
        self.model = model
        self._stuf = stuf()
        if self.name in RESERVED_NAMES and self.model.name not in ['model', 'attribute']:
            raise AttributeNameError("Attribute name '{}' is a reserved name. Use a different one.".format(self.name))
        elif self.name != hp.normalize_join(self.name):
            raise AttributeNameError("Attribute should be all lower cases without special characters and spaces. Should not start with an underscore")
        if not config.get('type'):
            config['type'] = 'text'
        for aname in self._attr_seq:
            logger.debug("Attribute of Attribute name is {}".format(aname))
            self.process(config, aname, _store = self._stuf, _att = self._attributes[aname], _is_db = True)
        self.name = self._stuf.name
        self._stuf.atype = self._name2type(self._stuf.atype)
        if self._stuf.id:
            self._stuf.required = True
            self._stuf.unique = False
            if not all(issubclass(a_, (str, unicode, int, long)) for a_ in hp.make_list(self._stuf.atype)):
                raise AttributeTypeError(self.name, self._stuf.atype, self, message = "Id should only be of types str, unicode and int")
            if self.model.has_login:
                self._stuf.converter = {'function': "to_login_id", 'args': [False]}
        if self._stuf.required and self._stuf.default is None:
            self._stuf.default = {'function': 'raise_error'}
        if not self._stuf.required and isinstance(self._stuf.default, dict) and self._stuf.default.get('function') == 'raise_error':
            self._stuf.default = None
        self._to_refers()
        if not self._stuf.refers and self._stuf.required is None:
            self._stuf.required = False
        self._to_converter()
        if self._stuf.auto_updater and self._stuf.default is None:
            logger.error("Instance: {}".format(config))
            raise AttributeAutoUpdaterError("Auto-updater is true, but default value is not provided for attribute {} in model {}".format(
                    self.name,self.model.name
                )
            )
        if not 'weight' in self._stuf:
            self._stuf.weight = None
        if self._stuf.type in [
            'name', 'password', 'email', 'id', 'url', 'image', 'video', 'file', 'image_list', 'image_object', 'uid', 'facebook_id', 
            'reddit_username', 'pintrest_username', 'vine_username',  'linkedin_id', 'twitter_handle', 'instagram_username',
            'ip_address', 'ipv6', 'mac_address', 
            ] or self._stuf.id:
            if self._stuf.weight is None:
                self._stuf.weight = 0.
        else:
            if self._stuf.weight is not None:
                self._stuf.weight = float(self._stuf.weight)
        if self._stuf.type in ['tags'] and not self._stuf.list_type or self._stuf.list_type == 'text':
            self._stuf.list_type = "category"
        if self._stuf.title is None:
            self._stuf.title = self.name.replace('_',' ').title()
        if self._stuf.placeholder is None:
            self._stuf.placeholder = self._stuf.title
        logger.debug("Attribute %s in model %s has id type %s and ids are %s", self.name, self.model.name, self._stuf.id, self.model.ids)
        if self._stuf.id:
            if self.name not in self.model.ids:
                self.model.ids.append(self.name)
            logger.debug("Attribute %s in model %s has id type %s and updated ids are %s", self.name, self.model.name, self._stuf.id, self.model.ids)
        if self._stuf.type == 'name':
            if self.name not in self.model.names:
                self.model.names.append(self.name)
        if self._stuf.type in ['image', 'image_list', 'image_object']:
            if self.name not in self.model.images:
                self.model.images.append(self.name)
        if self._stuf.constraint is None:
            self._stuf.constraint = self._to_constraint()
        if self._stuf.ui_element is None:
            self._stuf.ui_element = self._to_ui_element()
        for a in self._stuf:
            setattr(self, a, self._stuf[a])

    def _name2type(self, name):
        if not name:
            return (unicode, )
        name = hp.make_list(name)
        ret = []
        check_in = lambda x, y : (x not in y and x.__name__ not in y)
        for n in name:
            if isinstance(n, type):
                ret.append(n)
                if n in (unicode, str):
                    if check_in(str, ret): ret.append(str)
                    if check_in(unicode, ret): ret.append(unicode)
                if n in (db.geolocation, ):
                    if check_in(str, ret): ret.append(str)
                    if check_in(unicode, ret): ret.append(unicode)
                    if check_in(list, ret): ret.append(list)
                if n in (db.stringlist, db.numberlist, db.objectlist):
                    if check_in(list, ret): ret.append(list)
                if n in (int, float): 
                    if check_in(float, ret): ret.append(float)
                    if check_in(int, ret): ret.append(int)
                continue
            if n not in self._name2type_dict:
                raise AttributeTypeError('atype', n, self)
            a = self._name2type_dict[n]
            if a not in ret:
                ret.append(a)
            if n in ('unicode', 'str'):
                if check_in(str, ret): ret.append(str)
                if check_in(unicode, ret): ret.append(unicode)
            if n in ('geolocation',):
                if check_in(str, ret): ret.append(str)
                if check_in(unicode, ret): ret.append(unicode)
                if check_in(list, ret): ret.append(list)
            if n in ('int', 'float'): 
                if check_in(float, ret): ret.append(float)
                if check_in(int, ret): ret.append(int)
            if n in ('numberlist', 'stringlist', 'objectlist'):
                if check_in(list, ret): ret.append(list)
        return tuple(ret)


    def _to_converter(self):
        if self._stuf.type in ['password']:
            if not self._stuf.converter:
                self._stuf.converter = {'function':'hash_password', 'kwargs': {'_no_convert_for_constraint': True}}
        if self._stuf.type in ('price', 'discount','tax'):
            if not self._stuf.converter:
                self._stuf.converter = {'function': 'make_rounded'}
            if not self._stuf.jsoner:
                self._stuf.jsoner = {'function': 'make_rounded'}
        if self._stuf.type in ('discount_percentage', 'tax_percentage', 'percentage'):
            if not self._stuf.converter:
                self._stuf.converter = {'function': 'make_percent'}
            if not self._stuf.jsoner:
                self._stuf.jsoner = {'function': 'make_inv_percent'}
        if self._stuf.type in ('email', ):
            if not self._stuf.converter:
                self._stuf.converter = {'function': 'to_lowercase'}
        if self._stuf.type in ('url', ):
            if not self._stuf.converter:
                self._stuf.converter = {'function': 'url_converter'}
        if len(self._stuf.atype) == 1 and self._stuf.type not in ('true_false', 'yes_no'):
            if self._stuf.atype[0] in (db.date, db.datetime, db.dtime):
                if not self._stuf.converter:
                    self._stuf.converter = {'function': 'to_' + self._stuf.atype[0].__name__, 'args': [self._stuf.units] if self._stuf.units else []}
                if not self._stuf.jsoner:
                    self._stuf.jsoner = {'function': 'from_' + self._stuf.atype[0].__name__, 'args': [self._stuf.units] if self._stuf.units else []}
            else:
                if not self._stuf.converter:
                    self._stuf.converter = {'function': 'to_' + self._stuf.atype[0].__name__}
                if not self._stuf.jsoner:
                    self._stuf.jsoner = {'function': 'from_' + self._stuf.atype[0].__name__}
        if self._stuf.atype[0] in (float,):
            if not self._stuf.converter:
                self._stuf.converter = {'function': 'to_float'}
            if not self._stuf.jsoner:
                self._stuf.jsoner = {'function': 'from_float'}
        if self._stuf.atype[0] in (int,):
            if not self._stuf.converter:
                self._stuf.converter = {'function': 'to_int'}
            if not self._stuf.jsoner:
                self._stuf.jsoner = {'function': 'from_int'}
        if self._stuf.type in ('true_false', 'yes_no'):
            if not self._stuf.converter:
                self._stuf.converter = {'function': 'to_bool_from_' + self._stuf.type}
            if not self._stuf.jsoner:
                self._stuf.jsoner = {'function': 'from_bool_to_' + self._stuf.type}
        if self._stuf.type in ('rgb',):
            self._stuf.converter = {'function': 'to_rgb'}
            self._stuf.jsoner = {'function': 'from_rgb'}
        if all(a in (str, unicode) for a in self._stuf.atype):
            if not self._stuf.converter:
                self._stuf.converter = {'function': 'to_unicode'}
        if self._stuf.type in ('image_list',):
            if not self._stuf.adder:
                self._stuf.adder = {'function': 'list_adder', 'kwargs': {'unique': True}}
        if self._stuf.type in ('image_object',):
            if not self._stuf.dict_keys:
                self._stuf.dict_keys = ['image', 'thumbnail', 'title', 'size', 'orientation', 'scaling', 'description', 'name']

    def _to_constraint(self):
        if self._stuf.type in ['mobile_number', 'phone_number']:
            return {'function': 'phone_validator'}
        elif self._stuf.type in ['email']:
            return {'function': 'email_validator'}
        elif self._stuf.type in ['pincode']:
            return {'function': 'pincode_validator'}
        elif self._stuf.type in ['tags', 'list']:
            if self._stuf.list_type in ['mobile_number', 'phone_number']:
                return {'function': 'phone_validator_list'}
            elif self._stuf.list_type in ['email']:
                return {'function': 'email_validator_list'}
            elif self._stuf.list_type in ['pincode']:
                return {'function': 'pincode_validator_list'}
        elif self._stuf.type in ['rgb']:
            return {'function': 'rgb_validator'}
        elif self._stuf.type in ['url']:
            return {'function': 'url_validator'}
        elif self._stuf.type in ['geolocation']:
            return {'function': 'geolocation_validator'}
        return None

    def get_filter_link(self):
        if self._stuf.type != 'filter_attribute_link':
            return None
        return {
            'model': self._stuf.refers.split('.')[1],
            'attribute': self.name,
            'links': self._stuf.refers.split('.')[2]
        }

    def _to_refers(self):
        if not self._stuf.refers:
            return None
        sp = self._stuf.refers.split('.')
        if not len(sp) in [2, 3]:
            raise ReferenceError("Unknown format of reference for model {} in attribute {}: {}".format(
                    self.model.name, self.name, self._stuf.refers
                )
            )
        relationship = sp.pop(0)
        relation_model_name = sp.pop(0)
        relation_model_called = self._stuf.relation_called or (relation_model_name if relationship == 'parents' else self.model.name)
        relationship_attribute = None
        if relationship not in ['parents', 'children', 'filter_link']:
            raise ReferenceError("Unknown relationship {} for model {} in attribute {}: {}".format(
                    relationship, self.model.name, self.name, self._stuf.refers
                )
            )
        if len(sp):
            relationship_attribute = sp.pop(0)
        if relationship == 'parents':
            if not relation_model_called in self.model.parents:
                self.model._set_parent(relation_model_name, relation_model_called)
            relation_model = self.model.parents[relation_model_called]
            if relationship_attribute:
                if not relationship_attribute in relation_model.attributes:
                    raise RelationParentAttributeError(
                        "Relationship Attribute for attribute {} ( {} ) not found for parent model {}, parent of {}".format(
                            self.name,
                            relationship_attribute,
                            relation_model.name,
                            self.model.name,
                        )
                    )
                try:
                    i = self.model.parent_ids[relation_model_called].get('parent_ids',[]).index(relationship_attribute)
                except ValueError:
                    if not 'parent_self_map' in self.model.parent_ids[relation_model_called]:
                        self.model.parent_ids[relation_model_called]['parent_self_map'] = {}
                    if not 'self_parent_map' in self.model.parent_ids[relation_model_called]:
                        self.model.parent_ids[relation_model_called]['self_parent_map'] = {}
                    self.model.parent_ids[relation_model_called]['parent_self_map'][relationship_attribute] = self.name
                    self.model.parent_ids[relation_model_called]['self_parent_map'][self.name] = relationship_attribute
                    self._stuf.required = False
                    self._stuf.auto_updater = True
                    self._stuf['default'] = val.make_function(lambda: None,
                         'get_{}.{}_for_model_{}_in_{}'.format(self.name, relationship_attribute, self.model.name, self.model.enterprise_id),
                    )
                else:
                    self.model.parent_ids[relation_model_called]['self_ids'][i] = self.name
                    self.model.parent_ids[relation_model_called]['ids_present'][i] = True
                    if self._stuf.required == False:
                        self.model.parent_ids[relation_model_called]['required'] = self._stuf.required
                        self._stuf['option_others'] = True
                        self._stuf['ui_element'] = 'typeahead'
                    else:
                        self._stuf.required = True
                        self.model.parent_ids[relation_model_called]['required'] = True
                        self._stuf['ui_element'] = 'select'
                    if not self._stuf.get('options'):
                        self._stuf['options'] = '/unique/{}/{}'.format(relation_model_name, relationship_attribute)
                for a in ['type', 'atype']:
                    self._stuf[a] = relation_model.attributes[relationship_attribute]._stuf[a]
            else:
                self._stuf.atype = [dict]
                self._stuf.type = 'nested_object'
                self._stuf['dict_keys'] = '/attributes/{}/name'.format(relation_model_name)
                self._stuf['options'] = None
                logger.debug("Before setting the default value: %s, %s, %s", self.name, self.model.name, self.model.parent_ids[relation_model_called])
                self._stuf['jsoner'] = val.make_function(self.model._get_parents,
                     'get_{}_for_model_{}_in_{}'.format(self.name, self.model.name, self.model.enterprise_id),
                     ['instance'],
                     parent_name = relation_model_called
                )
                self._stuf['default'] = self._stuf['jsoner']
                if self._stuf.required is None: self._stuf.required = False
        if relationship == 'children':
            self._stuf.atype = [dict]
            self._stuf.type = 'object_list'
            self._stuf['dict_keys'] = '/attributes/{}'.format(relation_model_name)
            self._stuf['jsoner'] = val.make_function(
                self.model._get_children,
                'get_{}_for_model_{}_in_{}'.format(self.name, self.model.name, self.model.enterprise_id),
                ['instance'],
                child_name = relation_model_name,
                parent_called = relation_model_called
            )
            if self._stuf.required is None: self._stuf.required = False
        if relationship == 'filter_link':
            relation_model = self.model.__class__(relation_model_name, self.model.enterprise_id, self.model.role)
            relationship_attribute = relationship_attribute or self.name
            if not relationship_attribute in relation_model.attributes:
                raise RelationParentAttributeError(
                    "Relationship Attribute for attribute {} ( {} ) not found for model {}, filter_link of {}".format(
                        self.name,
                        relationship_attribute,
                        relation_model.name,
                        self.model.name,
                    )
                )
            attr_atype = relation_model.attributes[relationship_attribute]._stuf.atype[0]
            if not issubclass(attr_atype, (str, unicode, float, int, long, bool)):
                raise RelationshipLinkError(
                    "Cannot link attribute filter for attribute {} in model {} with model {}, since it is not a number or string".format(
                        relationship_attribute, relation_model_name, self.model.name
                    )
                )
            self._stuf.atype = [db.stringlist if issubclass(attr_atype, (str, unicode, bool)) else db.numberlist]
            self._stuf.type = 'filter_attribute_link',
            self._stuf.options = '/unique/{}/{}'.format(relation_model_name, relationship_attribute)
            self._stuf.ui_element = 'select'
        return self._stuf.refers

    def _json_children(self, instance, model_name):
        return None

    def _url_to_list(self, url, name = None):
        url = urlparse(url)
        sp = url.path.split('/')
        if len(sp) < 2:
            raise AttributeTypeError(name or self.name, url, self)
        method = sp[1]
        model_name = sp[2]
        params = parse_qs(url.query)
        try:
            if model_name == 'core':
                model = self.model
            else:
                model = self.model.__class__(model_name, self.model.enterprise_id)
            if method in self.model.__class__.contains_methods:
                check_method = self.model.__class__.contains_methods[method](model, *sp[3:])
                return self._to_contains(check_method)
            model_method = self.model.__class__.url_to_method(method)
            return getattr(model, model_method)(*sp[3:], _as_option =  True, **params)
        except Exception as e:
            raise AttributeUrlParseError("Error while converting URL {} to a list: In model.method {} -> {}. Error: {}".format(url, model_name, method, e.message))
        

    def _to_options(self, options):
        if isinstance(options, list):
            return options
        elif not options:
            return []
        elif isinstance(options, (str, unicode)):
            return self._url_to_list(options, 'options')
        elif isinstance(options, (dict,)):
            return self._to_contains(options)
        raise AttributeTypeError('options', options, self)


    def _convert_constrain(self, converter, constraint, atype, instance, attribute, value, in_adder = False):
        if value is None:
            return value
        new_value = None
        e = ''
        logger.debug("Before constrain-convert, Old Value: %s, New Value: %s", value, new_value)
        if in_adder and isinstance(constraint, dict) and constraint.get('function','').endswith('_list'):
            # Constraint function is being applied on a list directly
            pass
        elif val.is_validator(constraint):
            is_valid = False
            try:
                is_valid = val.execute(constraint, self, instance, attribute, value)
            except Exception as e:
                is_valid = False
            if not is_valid and val.is_validator(converter) and not converter.get('kwargs', {}).get('_no_convert_for_constraint'):
                try:
                    new_value = val.execute(converter, self, instance, attribute, value)
                except Exception as e1:
                    is_valid = False
                else:
                    try:
                        is_valid = val.execute(constraint, self, instance, attribute, new_value)
                    except Exception as e:
                        is_valid = False
            if not is_valid:
                raise ValueConstraintError(value, constraint['function'], self, e)
        if new_value:
            logger.debug("After constrain-convert, new_value, Old Value: %s, New Value: %s", value, new_value)
            value = new_value
        elif val.is_validator(converter):
            logger.debug("Before convert, Old Value: %s, New Value: %s, converter: %s", value, new_value, converter)
            new_value = val.execute(converter, self, instance, attribute, value)
            logger.debug("After convert, Old Value: %s, New Value: %s, converter: %s", value, new_value, converter)
            value = new_value
        return value

    def _to_atype(self, instance):
        t = getattr(self._stuf, 'type', instance.get('type', 'text'))
        if t in [
            'text', 'name', 'email', 'password', 'phone_number', 'mobile_number', 'uid',
            'description', 'address', 'category', 'month_of_year', 'day_of_week', 'url', 'image',
            "pincode", "zipcode", "areacode", "city", "location", "state", "county", "country", "zone", "region",
            'audio', 'video', 'file', 'ip_address', 'ipv6', 'mac_address', 'device_id', 'access_token', 'push_token', "apk",
            'facebook_id', 'twitter_handle', 'pintrest_username', 'reddit_username', 'instagram_username', 'vine_username',
            'linkedin_id'
        ]: 
            return unicode
        if t in ['list', 'tags', 'image_list', 'filter_attribute_link']:
            return db.stringlist
        if t in ['true_false', 'yes_no', 'flag', 'bool', 'boolean']:
            return bool
        if t in ['number', 'price', 'discount','discount_percentage','tax','tax_percentage', 'percentage', 'rate', 'rating', 'currency', 'age']:
            return float
        if t in ['sequence', 'index', 'id', 'serial_number']:
            return int
        if t in ['date']:
            return db.date
        if t in ['time']:
            return db.dtime
        if t in ['timestamp', 'datetime']:
            return db.datetime
        if t in ['time_difference', 'time_interval', 'number_days', 'number_hours', 'number_seconds']:
            return float
        if t in ['geolocation']:
            return db.geolocation
        if t in ['nested_object', 'media_map', 'action_settings']:
            return dict
        if t in ['object_list', 'image_object']:
            return db.objectlist
        if t in ['number_list', 'range', 'image_vector', 'rgb']:
            return db.numberlist
        if t in ['multi']:
            return [
                bool, unicode, int, float, db.date, db.dtime, db.datetime, db.geolocation, db.stringlist, 
                db.objectlist, db.numberlist, dict]
        return unicode

    def _to_ui_element(self, instance = None):
        if instance is None: instance = {}
        t = getattr(self._stuf, 'type', instance.get('type', 'text'))
        lt = getattr(self._stuf, 'list_type', instance.get('list_type', 'text'))
        a = getattr(self._stuf, 'atype', self._to_atype(instance))
        if isinstance(a,(tuple,list)):
            a = a[0]
        o = getattr(self._stuf, 'options', instance.get('options', []))
        ot = getattr(self._stuf, 'option_others', instance.get('option_others', False))
        dk = self._to_options(getattr(self._stuf, 'dict_keys', instance.get('dict_keys', [])))
        if t in ['list', 'tags'] and lt in ['image']:
            return 'slideshow'
        elif t in ['image_list']:
            return 'slideshow'
        elif t in ['image_object']:
            return 'thumbnails'
        elif t in ['rating']:
            return 'rating'
        if a in (int, long, float):
            return 'number'
        elif a in [db.stringlist, db.numberlist]:
            if o:
                if ot:
                    return 'typeahead-tags'
                return 'multiselect'
            return 'tags'
        elif a == bool:
            return 'switch'
        elif a == db.date:
            return 'date'
        elif a == db.dtime:
            return 'time'
        elif a == db.datetime:
            return 'datetime'
        elif a == db.timedelta:
            return 'number'
        elif a == db.objectlist:
            return 'table'
        elif a == dict:
            if all(d_ in dk for d_ in ['title', 'image']):
                return 'thumbnails'
            return 'card'
        elif a == db.geolocation:
            return 'geolocation'
        elif a in [str, unicode]:
            if t in ['description', 'textarea', 'address']:
                return 'textarea'
            elif t in ['mobile_number', 'phone_number']:
                return 'tel'
            elif t in ['day_of_week']:
                return 'week'
            elif t in ['month_of_year']:
                return 'month'
            elif o:
                if ot:
                    return 'typeahead'
                return 'select'
            elif t in ['email', 'password', 'url', 'image', 'audio', 'video', 'file', 'ipv6']:
                return t
        return 'text'


    def _refers_constraint(self, refers, instance):
        split = refers.split('.')
        for i in range(0, len(split) - 1, 2):
            if not split[i] in ['children', 'parents', 'filter_link']:
                raise ReferenceError(refers, "should specify whether refers to 'children', 'parents' or 'filter_link'")
        for i in range(1, len(split) - 1, 2):
            if not any(split[i] == (_i[0] if isinstance(_i, (list, tuple)) else _i) for _i in getattr(self.model._dict, split[i-1], [])):
                raise ReferenceError(refers, "model {} is not a {} of {}".format(split[i], split[i-1], self.model.name))
        return True

    def _refers_model(self, instance, *args):
        refers = instance.get(args[0])
        if isinstance(refers, (str, unicode)):
            relation = refers.split('.')[0]
            return refers.split('.')[1] if relation == 'parents' else self.model.name
        return None


    def process(self, instance, attribute, _store = None, _att = None, _def_att = None, _is_db = None, _is_new = False, _is_updated = True, _updated = None, _previous = None):
        if _updated is None: _updated = []
        if _store is None: _store = instance
        att = _att or self._stuf
        def_att = _def_att or self._attributes.get(attribute, {})
        if _is_db is None: _is_db = getattr(self.model, 'is_db', False)
        default = att.get('default', def_att.get('default', None))
        atype = self._name2type(att.get('atype') or def_att.get('atype', 'unicode'))
        options = self._to_options(att.get('options', def_att.get('options', [])))
        if options is None: options = []
        unique = att.get('unique', def_att.get('unique', False))
        if unique is None: unique = False
        value = instance.get(attribute)
        if value is None: value = hp.copyof(default)
        auto_updater = att.get('auto_updater', def_att.get('auto_updater', False))
        if auto_updater is None: auto_updater = False
        force_update = att.get('force_update', def_att.get('force_update', False))
        if force_update is None: force_update = False
        adder = att.get('adder', def_att.get('adder', None))
        prev_value = _previous
        if force_update and (
                (
                    isinstance(force_update, bool)                # Simple if force update is boolean
                ) or (
                    not _updated and value in (None, default)     # when it is a post or put type
                ) or (
                    isinstance(force_update, (unicode, str, list, tuple)) and any(u_ in hp.make_list(force_update) for u_ in _updated) 
                    # when specific attributes are updated
                )
            ):
            if not val.is_validator(value):
                prev_value = value
            value = default
        if value == default and val.is_validator(default):
            logger.debug(u"Execute default function for attribute {} in model {}: {}".format(attribute, self.model.name, value))
            value = val.execute(default, self, instance, attribute, value if prev_value is None else prev_value, prev_value)
            if prev_value is not None and value is None and auto_updater:
                value = prev_value
            logger.debug(u"After default function for attribute {} in model {}: {}".format(attribute, self.model.name, value))
        required = att.get('required', def_att.get('required', None))
        if required and value is None:
            if isinstance(required, (str, unicode)) and required in self.model.attributes:
                required = list(set([attribute, required]))
            if isinstance(required, (list, tuple)):
                val.execute({'function': 'raise_error_if_none_of', 'args': required}, self, instance, attribute, value)
            elif val.is_validator(required):
                val.execute(required, self, instance, attribute, value)
            else:
                val.execute({'function': 'raise_error'}, self, instance, attribute, value)
        constraint = att.get('constraint', def_att.get('constraint', None))
        converter = att.get('converter', def_att.get('converter', None))
        if val.is_validator(adder) and value is not None:
            if _is_updated or _is_new:
                value = self._convert_constrain(converter, constraint, atype, instance, attribute, value, in_adder = True)
            value = val.execute(adder, self, instance, attribute, value)
        if _is_updated or _is_new:
            value = self._convert_constrain(converter, constraint, atype, instance, attribute, value)
        if attribute == 'atype':
            options = self._name2type(options)
            value = self._name2type(value)
        def check_in_options(v, ol, oll):
            if isinstance(v, (str, unicode)):
                try:
                    return ol[oll.index(v.lower())]
                except ValueError:
                    return v
            return v
        if options and isinstance(options, (list, tuple)) and att.get('list_type') in (
                None, 'text', 'category', 'month_of_year', 'day_of_week', "city", "location", "state", "county", "country", "zone", "region", 'rgb'
            ):
            ol = filter(lambda x: isinstance(x, (str, unicode)), options)
            oll = map(string.lower, ol)
            if isinstance(value, (tuple, list)):
                value = type(value)(check_in_options(v, ol, oll) for v in value)
            elif isinstance(value, (str, unicode)):
                value = check_in_options(value, ol, oll)
        if options and value is not None and not att.get('option_others', def_att.get('option_others', False)):
            if isinstance(value, (tuple, list)):
                for v in value:
                    if v not in options:
                        logger.error(
                            """
                            model = {}
                            attribute = {}
                            attribute's attribute = {}
                            current value = {}
                            all values = {}
                            options = {}
                            type(value) = {}
                            """.format(self.model.name, self.name, attribute, v, value, options, type(v))
                        )
                        raise AttributeOptionsError(attribute, v, self)
            elif attribute in ['default', 'jsoner', 'adder', 'converter', 'constraint', 'cleaner'] and self.name != attribute and isinstance(value, dict):
                if not value.get('function') in map(lambda x: x['function'], val.functions_with_help):
                    raise ValidatorOptionsError("Function {} not among valid available functions".format(value.get('function')))
            elif value is not None and any(v_ not in options for v_ in hp.make_list(value)):
                logger.error("Options are: {}".format(options))
                raise AttributeOptionsError(attribute, value, self)
        if value:
            if not isinstance(value, atype) and attribute != 'atype':
                if not (_is_updated and _is_new):
                    # This is the scenario when the type of the object has changed and we have old values of a different type
                    value = self._convert_constrain(converter, constraint, atype, instance, attribute, value)
            if not isinstance(value, atype) and attribute != 'atype':
                raise AttributeTypeError(attribute, value, self, message = "allowed atypes {}. value is {}".format(atype, type(value)))
        if atype == db.geolocation and unique:
            raise ValueUniqueError("Attribute {} ({}) of type geolocation cannot be unique".format(self.name, attribute))
        if _is_new and unique and not _is_db:
            el = self.model.list(**{attribute: value})['data']
            if el:
                ids = self.model.ids
                nids = [instance.get(i) for i in ids]
                if any([[l.get(i) for i in ids] != nids for l in el]):
                    raise ValueUniqueError(
                        "Object with {} ({}) = {} already exists in {}".format(
                            self.name, attribute, value, self.model.name
                        )
                    )
        _store[attribute] = value
        # Need to check if we can update the old instance in all cases
        # if instance.get(attribute) is None and value is not None:
        # if _is_updated or _is_new:
        instance[attribute] = value
        return value

    def to_json(self, instance, attribute, value):
        if self._stuf.jsoner:
            return val.execute(self._stuf.jsoner, self, instance, attribute, value)
        return value

    def to_store(self):
        r = {}
        for k, v in self._stuf.iteritems():
            if v is not None: 
                if k == 'atype':
                    r[k] = [a.__name__ for a in v if a not in (str, list)]
                else:
                    r[k] = v
        return r

    def to_dict(self, ):
        r = self.to_store()
        if isinstance(r['atype'], list):
            r['atype'] = r['atype'][0]
        return r






RESERVED_NAMES = [
   "all",
   "analyse",
   "analyze",
   "and",
   "any",
   "array",
   "as",
   "asc",
   "args",
   "_args",
   "_action",
   "_name",
   "asymmetric",
   "_attribute",
   "both",
   "case",
   "cast",
   "check",
   "collate",
   "column",
   "constraint",
   "create",
   "current_catalog",
   "current_date",
   "current_role",
   "current_time",
   "current_timestamp",
   "current_user",
   "default",
   "deferrable",
   "desc",
   "distinct",
   "do",
   "else",
   "end",
   "except",
   "false",
   "fetch",
   "for",
   "foreign",
   "from",
   "grant",
   "group",
   "having",
   "in",
   "id",
   "ids",
   "_id",
   "_ids",
   "instance",
   "_instance",
   "initially",
   "intersect",
   "into",
   "lateral",
   "leading",
   "limit",
   "localtime",
   "localtimestamp",
   "model_id",
   "model_ids",
   "_model",
   "_model_id",
   "_model_ids",
   "not",
   "null",
   "offset",
   "on",
   "only",
   "or",
   "order",
   "object",
   "placing",
   "primary",
   "references",
   "returning",
   "select",
   "session_user",
   "some",
   "symmetric",
   "table",
   "then",
   "to",
   "trailing",
   "true",
   "union",
   "unique",
   "user",
   "using",
   "variadic",
   "value",
   "when",
   "where",
   "window",
   "with",
]
