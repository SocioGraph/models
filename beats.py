#!/usr/bin/python

# Contains code for periodically occuring tasks e.g. customer lifetime status, subscription

import os, sys, json
from functools import wraps
import helpers as hp
import numpy as np
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile
import model as base_model
import algorithms as alg
import validators as val
from datetime import datetime,timedelta,date
import time
import traceback
from stuf import stuf
from collections import OrderedDict
import pgpersist as db
import interactions
import action as act
from celery import Celery
from celery.schedules import crontab
from flask import render_template, current_app as app, url_for
from communication import Communication,View
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import time
import cacher
import perspectives
import kombu
from image_operations import ImageMapper

CELERY_CONFIG = {  
    'CELERY_BROKER_URL': 'redis://{}/{}'.format(os.environ.get('REDIS_URL', 'localhost:6379'), os.environ.get('REDIS_DB_NUMBER',0)),
    'CELERY_TASK_SERIALIZER': 'pickle',
    'BROKER_USE_SSL': os.environ.get('BROKER_USE_SSL', 'False').lower() == 'true',
}
celery = Celery(__name__, broker=CELERY_CONFIG['CELERY_BROKER_URL'])
celery.conf.update(CELERY_CONFIG)
logger = hp.get_logger(__name__)
current_module = __import__(__name__)
I2CE_PHONE_NUMBER = '9980838165'
I2CE_EMAIL_ID = 'ananth@i2ce.in'

def get_default_jobs(product_type = 'jobs'):
    if product_type not in ['distributor', 'store', 'magento', 'occ']:
        product_type = 'jobs'
    with hp.read_file('data', 'beats', '{}.json'.format(product_type)) as jobs:
        return jobs

def create_enterprise_task(enterprise_id, job):
    if not (isinstance(job, dict) and 'task' in job and get_from_module(job['task'])):
        logger.error("Job {} is ill-formed for enterprise {}".format(job, enterprise_id))
        return False
    enterprise_model = base_model.Model('enterprise', 'core', 'admin')
    enterprise = enterprise_model.get(enterprise_id)
    jobs = get_enterprise_jobs(enterprise)
    for i, j in enumerate(jobs):
        if j['task'] == job['task']:
            jobs[i] = job
            break
    else:
        jobs.append(job)
    enterprise_model.update(enterprise_id, {'jobs': jobs})
    return add_task(celery, enterprise_id, job)


def get_enterprise_jobs(enterprise):
    return enterprise.get('jobs', get_default_jobs(enterprise.get('deployment_scenario', 'common')))

def add_enterprise_tasks(sender, enterprise):
    enterprise_id = enterprise['enterprise_id']
    jobs = get_enterprise_jobs(enterprise)
    for job in jobs:
        add_task(sender, enterprise_id, job)

def add_base_task(sender, job):
    logger.info("Adding periodic job", job)
    if not (isinstance(job, dict) and 'task' in job and get_from_module(job['task'])):
        logger.error("Job {} is ill-formed for enterprise".format(job))
        return False
    sender.add_periodic_task(
        crontab(**job.get('schedule', {'minute': 0, 'hour': 0})),
        get_from_module(job['task']).s(
            *job.get('args', []),
            **job.get('kwargs', {})
        ),
        name = 'base_{}'.format(job.get('name')),
        expires = job.get('expires'),
        queue = job.get('queue')
    )
    return True

def add_task(sender, enterprise_id, job):
    logger.info("Adding periodic job: %s for enterprise: %s", job, enterprise_id)
    if not (isinstance(job, dict) and 'task' in job and get_from_module(job['task'])):
        logger.error("Job {} is ill-formed for enterprise {}".format(job, enterprise_id))
        return False
    sender.add_periodic_task(
        crontab(**job.get('schedule', {'minute': 0, 'hour': 0})),
        get_from_module(job['task']).s(
            enterprise_id,
            *job.get('args', []),
            **job.get('kwargs', {})
        ),
        name = '{}_{}'.format(enterprise_id, job.get('name')),
        expires = job.get('expires'),
        queue = job.get('queue')
    )
    return True


def add_to_module(f):
    setattr(current_module, 'tasks_{}__'.format(f.name), f)
    return f

def get_from_module(f):
    return getattr(current_module, 'tasks_{}__'.format(f))

@celery.task(name = 'send_credit_notifications')
def send_credit_notifications(enterprise, current_credit, recommended_recharge):
    admin_model = base_model.Model('admin', 'core')
    admins = admin_model.list(enterprise_id = enterprise['enterprise_id'], _as_option = True)
    comm = Communication(enterprise['enterprise_id'])
#            "push_tokens": reduce(lambda x, y: x + y['push_token'], admins, [])
    comm.send(
        receiver = {
            "emails": map(lambda x: x['user_id'], admins),
            "phone_numbers": map(lambda x: x['phone_number'], admins),
            "push_tokens" : []
        },
        sender = {
            "email": I2CE_EMAIL_ID,
            "phone_number": I2CE_PHONE_NUMBER,
        },
        title = "Low credit alert on Dave.AI",
        view_type="Low balance alert",
        enterprise = enterprise,
        email_template= "email_recharge_alert.html",
        html_template= "email_recharge_alert.html",
        push_template= "email_recharge_alert.html",
        message = """
                Your enterprise {title} ({id_})is running low on credits with Dave.AI.
                Balance Credit: {cc}
                Recommended Recharge Amount: {rate}
        """.format(
            title = enterprise['name'],
            id_ = enterprise['enterprise_id'],
            cc = current_credit,
            rate = recommended_recharge
          ),
        cc = current_credit,
        rate = recommended_recharge,
        key=os.environ.get('RAZRPAY_API_KEY', 'rzp_test_htVSSrrdDO0Mvj')
        )

@add_to_module
@celery.task(name = "view_clean_up")
def view_clean_up(keep_days = 60, start_date = '2016-01-01'):
    v = base_model.Model('view', 'core')
    earliest_date = str(hp.date.today() - hp.timedelta(days = keep_days))
    logger.info("Deleting views before %s", earliest_date)
    for v1 in v.filter(_last_updated = start_date):
        if v1['__timestamp'] < earliest_date:
            v.delete(v1['view_id'])


@add_to_module
@celery.task(name = 'subscriptions')
def subscriptions(enterprise_id):
    def do_check(daily_rate):
        cl = st.enterprise_model.get(enterprise_id).get('credits_left', 0) 
        if cl < daily_rate*5:
            send_credit_notifications.delay(st.enterprise, cl, 100*(1 + (30*daily_rate//100)))
    st = interactions.setup(enterprise_id, 'admin')
    total_rates = 0
    num_products = st.product_model.pivot(active = True, _page_size = 1e6, _as_option = True).get(st.product_model.ids[0], 0)
    logger.info("Number of active products for enterprise %s: %s", enterprise_id, num_products)
    product_multiplier = max(1, 0.1*(np.ceil(num_products/100.)))
    logger.info("Product multiplier for enterprise %s: %s", enterprise_id, product_multiplier)
    total_rates += interactions.deduct_charges(enterprise_id, "daily_subscription", 1, st = st)
    logger.info("Total rates after daily subscription for enterprise %s: %s", enterprise_id, total_rates)
    num_customers = len(st.interaction_model.pivot(
        st.customer_id_attr,
        **{
            st.interaction_time_attribute: '{},'.format(hp.add_month(st.now, -1)), 
            '_as_option': True, 
            '_page_size': 1e6
        }
    ))
    customer_multiplier = max(0.1, 0.1*(np.ceil(num_products/1000.)))
    logger.info("Num Customers for enterprise %s: %s", enterprise_id, num_customers)
    logger.info("Customer multiplier for enterprise %s: %s", enterprise_id, customer_multiplier)
    total_rates += interactions.deduct_charges(enterprise_id, "customer_subscription", customer_multiplier * product_multiplier, st = st)
    logger.info("Total rates after customer subscription for enterprise %s: %s", enterprise_id, total_rates)
    num_users = (sum(s.count() for s in st.login_models[1:] if s.name != 'admin') + (st.admin_model.count(enterprise_id = enterprise_id) - 1)) * product_multiplier 
    logger.info("Num Users for enterprise %s: %s", enterprise_id, num_users)
    total_rates += interactions.deduct_charges(
        enterprise_id, "user_subscription", 
        num_users,
        st = st
    )
    logger.info("Total rates after user subscription for enterprise %s: %s", enterprise_id, total_rates)
    try:
        device_model = base_model.Model("device", enterprise_id)
        total_rates += interactions.deduct_charges(enterprise_id, "device_subscription", device_model.count() * product_multiplier)
    except base_model.ModelNotFound:
        pass
    logger.info("Total rates after device subscription for enterprise %s: %s", enterprise_id, total_rates)
    do_check(total_rates)


@add_to_module
@celery.task(name = 'set_engagement_stage')
def set_engagement_stage(enterprise_id):
    st = interactions.setup(enterprise_id)
    customer_lifetime_stage = interactions.get_preference(enterprise_id, 'customer_lifetime_stage', 'customer_lifetime_stage', st)
    logger.info("Setting up periodic task set_engagement_stage for %s", enterprise_id)
    for customer in st.customer_model.filter(_internal = True):
        customer_id =  st.customer_model.dict_to_ids(customer)
        prev_stage = customer.get(customer_lifetime_stage)
        stage = interactions.get_customer_stage(enterprise_id, 'admin', customer_id, st = st)
        if stage != prev_stage:
            customer = st.customer_model.update(customer_id, {customer_lifetime_stage: stage})
        tasks = interactions.prepare_lifetime_action_list(
            st, prev_stage, stage, customer, **{
                "receiver_ids": [customer_id], 
                "receiver_model": st.customer_model.name
            }
        )
        if tasks:
            logger.info("Setting up tasks for customer_id: %s - %s (Prev Stage - %s, Next Stage - %s", customer_id, tasks, prev_stage, stage)
            act.do_actions.delay({'actions': tasks, 'error_action': {'receiver': { 'emails': ['ananth@i2ce.in']}}}, enterprise_id, 'admin', return_act = False)


@add_to_module
@celery.task(name = 'cleanup_cache')
def cleanup_cache(enterprise_id):
    c = cacher.Cacher(enterprise_id)
    c.cleanup()


@add_to_module
@celery.task(name = "quantity_predictions")
def quantity_predictions(enterprise_id, *args, **kwargs):
    st = interactions.setup(enterprise_id, 'admin')
    total_rates = 0
    for p in st.product_model.filter():
        index = 0
        actions = {}
        for c in st.customer_model.filter():
            if index % 1000 == 0:
                if actions.get('actions'):
                    act.do_actions.delay(actions, enterprise_id, 'admin', return_act = False)
                actions = {'actions': [], 'error_action': {'receiver': {'emails':  st.enterprise.get('email')}}}
                index = 0
            action = {
                '_action': 'quantity_predictions',
                '_name': 'daily_predictions',
                '_ids': [p[st.product_model.ids[0]], c[st.customer_model.ids[0]]],
                '_data': kwargs
            }
            actions['actions'].append(action)
            index += 1
        if actions.get('actions'):
            act.do_actions.delay(actions, enterprise_id, 'admin', return_act = False)

@add_to_module
@celery.task(name = "cleanup_orders")
def cleanup_orders(enterprise_id, order_model = 'invoice', order_open_attr = 'order_closed', updated_time = 'updated', cleanup_days = 2, **kwargs):
    td = hp.timedelta(days = cleanup_days)
    st = interactions.setup(enterprise_id, 'admin')
    try:
        om = base_model.Model(order_model, enterprise_id, 'admin')
    except base_model.ModelNotFound as e:
        logger.error("Model %s not found for enterprise %s", order_model, enterprise_id)
        return
    for orr in om.filter(**{order_open_attr: False, updated_time: ",{}".format(hp.now() - td)}):
        om.delete(om.dict_to_ids(orr))

@add_to_module
@celery.task(name = "cleanup_uploaded_files")
def cleanup_uploaded_files(enterprise_id, cleanup_days = 30, **kwargs):
    for k, v in kwargs.iteritems():
        if not isinstance(v, dict):
            v = {'cleanup_days': v}
        td = hp.to_epoch(hp.now() - hp.timedelta(days = v.get('cleanup_days', cleanup_days)))
        pth = hp.joinpath(val.STATIC_DIR, enterprise_id, k)
        logger.info("Analyzing old files in %s", pth)
        for f in hp.glob(hp.joinpath(pth, v.get('pattern', '*'))):
            mt = hp.modified_time(f)
            if  mt < td:
                logger.warn("Removing file %s which is older than %s days: %s", f, v.get('cleanup_days', cleanup_days), hp.to_datetime(mt))
                os.remove(f)
        td = hp.to_epoch(hp.now() - hp.timedelta(days = v.get('cleanup_days', cleanup_days)))
        pth = hp.joinpath(hp.dirname(hp.dirname(abspath(__file__))), 'static', 'uploads', enterprise_id, k)
        logger.info("Analyzing old files in %s", pth)
        for f in hp.glob(hp.joinpath(pth, v.get('pattern', '*'))):
            mt = hp.modified_time(f)
            if  mt < td:
                logger.warn("Removing file %s which is older than %s days: %s", f, v.get('cleanup_days', cleanup_days), hp.to_datetime(mt))
                os.remove(f)


@add_to_module
@celery.task(name = "make_clusters")
def make_clusters(enterprise_id, init_count = 10, min_update_count = 100, max_clusters=5, min_split=10, **kwargs):
    st = interactions.setup(enterprise_id, 'admin')
    model = st.product_model
     
    al = act.ALGORITHM_CLASS(enterprise_id)
    im = al._product_matcher.image_mapper
    im.max_clusters = max_clusters
    im.min_split=min_split
    _updated_time = im.last_updated()
    logger.info("Last updated time of the kmeans cluster %s %s %s", _updated_time, model.count(), im.path)
    if not _updated_time:
        c = model.count()
        if c < init_count: return
    else:
        c = model.list(_last_updated=_updated_time, _page_size=1)["total_number"]
        if c < min_update_count: return

    im.train()
    for p in model.filter():
        act.add_to_algo.delay(model.name, enterprise_id, 'admin', None, 'product', p)


@celery.task(name = 'do_recommendation')
def do_recommendation(st, customer_id, limit, second_limit, product_filters, **kwargs):
    total_products = len(st.product_model.list(_as_option = True, _filter_attributes = st.product_model.ids, **product_filters))
    dp = 0
    data = product_filters.copy()
    data.update(kwargs)
    data.update({'_limit': limit, '_freshen': True, '_fresh': True})
    a = act.Action(st.enterprise_id)
    leave_out = {
        st.product_model.ids[0] + '~': []
    }
    while dp < total_products:
        action = {
            '_action': 'recommendations',
            '_ids': hp.make_list(customer_id),
            '_data': data
        }
        logger.info("Executing action: %s", action)
        r = a.act(action, dp)
        dp += len(r['recommendations'])
        for ri in r['recommendations']:
            leave_out[st.product_model.ids[0] + '~'].append(ri[st.product_model.ids[0]])
        data['_limit'] = second_limit
        data.update(leave_out)
    comm = Communication(st.enterprise_id)
    comm.send(
        receiver = {
            "emails": 'ananth@i2ce.in',
        },
        sender = {
            "email": I2CE_EMAIL_ID,
            "phone_number": I2CE_PHONE_NUMBER,
        },
        title = "Ran Do Recommendations",
        view_type="transaction",
        enterprise = st.enterprise,
        message = "Ran recommendation for Customer {customer_id}, Product Filters {product_filters}".format(
            customer_id = customer_id, 
            product_filters = ', '.join('{}:{}'.format(k, v) for k, v in product_filters.iteritems())
        )
    )
        

@add_to_module
@celery.task(name = 'do_recommendations')
def do_recommendations(enterprise_id, 
        perspective_name = 'mobile_application', 
        limit = 'no_of_recommendations', 
        second_limit = 'count_after_next_recommendation',
        category_hierarchy = 'product_category_hierarchy', 
        **kwargs
    ):
    if category_hierarchy is None:
        category_hierarchy = []
    def get_preference(name, default = None):
        return st.enterprise.get(
        'preferences', {}
        ).get(
            perspective_name, {}
        ).get(
            name, default
        )
    def permute_hierarchy(ch):
        previous = []
        for c in ch:
            new = []
            for p in previous:
                variations = st.product_model.pivot(c, _as_option = True, **p).keys()
                for v in variations:
                    p1 = p.copy()
                    p1[c] = v
                    new.append(p1)
            if not previous:
                variations = st.product_model.pivot(c, _as_option = True).keys()
                for v in variations:
                    new.append({c: v})
            previous = new
        return previous
    st = interactions.setup(enterprise_id, 'admin')
    product_hierarchy_list = permute_hierarchy(get_preference(category_hierarchy))
    limit = get_preference(limit, 10)
    second_limit = limit - get_preference(second_limit, 5)
    kwargs['_quantity_predict'] = True
    for k, v in kwargs.iteritems():
        kwargs[k] = get_preference(v, v)
    if not product_hierarchy_list:
        product_hierarchy_list = [{}]
    num_jobs = 0
    for cs in st.customer_model.filter(_filter_attributes = st.customer_model.ids):
        for product_filters in product_hierarchy_list:
            kw = kwargs.copy()
            for k, v in list(product_filters.items()):
                kw['_additional_values[_s_{}]'.format(k)] = v
            logger.info("Running recommendations for enterprise_id: %s, customer_id: %s, product_filters: %s, kwargs: %s", 
                enterprise_id, cs[st.customer_model.ids[0]], product_filters, kw
            )
            if num_jobs >= 10:
                time.sleep(60)
                num_jobs = 0
            while True:
                try:
                    do_recommendation.delay(st, cs[st.customer_model.ids[0]], limit, second_limit, product_filters, **kw)
                except kombu.exceptions.OperationalError as e:
                    logger.info("Got an exception, probably overloaded redis, waiting for a while")
                    time.sleep(600)
                else:
                    num_jobs += 1
                    break

        

@add_to_module
@celery.task(name = 'set_weights')
def set_weights(enterprise_id, *args, **kwargs):
    st = interactions.setup(enterprise_id, 'admin')
    st.product_model.set_weights()
    st.customer_model.set_weights()
    if st.store_model:
        st.store_model.set_weights()

@add_to_module
@celery.task(name = 'run_actions')
def run_actions(enterprise_id, role = 'admin', user_id = None, **kwargs):
    if not kwargs.get('actions'):
        return
    st = interactions.setup(enterprise_id, role, user_id = user_id)
    if not kwargs.get('error_action'):
        kwargs['error_action'] = {
            'receiver': {'emails':  st.enterprise.get('email')}
        }
    act.do_actions(kwargs, enterprise_id, role = role, user_id = user_id, return_act = False)


def do_warehousing(warehouse_action, *args, **kwargs):
    enterprise_model = base_model.Model('enterprise', 'core')
    for e in enterprise_model.list(_as_option = True):
        enterprise_id = e['enterprise_id']
        logger.info("Running %s for enterprise: %s", warehouse_action, enterprise_id)
        for m in base_model.models(enterprise_id, _as_name = True):
            logger.info("Running %s for model %s in enterprise: %s", warehouse_action, m, enterprise_id)
            action = {
                '_action': warehouse_action, 
                '_model': m, 
                '_name': '{}_{}_in_{}'.format(warehouse_action, m, enterprise_id)
            }
            action.update(kwargs)
            act.do_action({
                'enterprise_id': enterprise_id
            }, action)
        for m in ['audio_logs', 'engagement', 'communication', 'conversation_history']:
            action = {
                '_action': warehouse_action, 
                '_model': m, 
                '_name': '{}_{}_in_{}'.format(warehouse_action, m, enterprise_id)
            }
            action.update(kwargs)
            act.do_action({
                'enterprise_id': enterprise_id
            }, action)

@add_to_module
@celery.task(name = 'accumulate')
def accumulate(resolution = 3600, **kwargs):
    do_warehousing('accumulate', resolution = resolution, **kwargs)

@add_to_module
@celery.task(name = 'warehouse')
def warehouse(keep_days = 60, resolution = 86400, **kwargs):
    do_warehousing('warehouse', keep_days = keep_days, resolution = resolution, **kwargs)
    
@add_to_module
@celery.task(name = 'create_customer_state_list')
def create_customer_state_list(enterprise_id, *args, **kwargs):
    updated_attr = kwargs.pop('updated_attr', 'updated')
    customer_model = kwargs.pop('customer_model', 'person')
    back_days = int(kwargs.pop('back_days', 1))
    state_list_attr = kwargs.pop('state_list_attr', 'customer_state_list')
    drop_off_attr = kwargs.pop('drop_off_attr', 'customer_drop_off')
    cm = base_model.Model(customer_model, enterprise_id)
    al = base_model.Model('audio_logs', enterprise_id)
    for k in cm.filter(**{updated_attr: '{},'.format(hp.now() - hp.timedelta(days = back_days))}):
        customer_states = al.pivot(_action = 'concat', _over = 'customer_state', customer_id = hp.make_single(cm.dict_to_ids(k)), _sort_by = updated_attr, _as_option = True, created = '{},'.format(hp.now() - hp.timedelta(days = back_days))**kwargs)
        cm.update(cm.dict_to_ids(k), {state_list_attr: customer_states, drop_off_attr: customer_states[-1]})
    
