import sys, os, glob, model
from libraries import json_tricks as json
import inflect as inflect_
inflect = inflect_.engine()

if len(sys.argv) > 1:
    enterprise_id = sys.argv[1]
    models_path = os.path.join(os.curdir, 'data', enterprise_id, 'models')
    objects_path = os.path.join(os.curdir, 'data', enterprise_id, 'objects')
else:
    enterprise_id = 'core'
    models_path = os.path.join(os.path.dirname(__file__), 'data', enterprise_id, 'models')
    objects_path = os.path.join(os.path.dirname(__file__), 'data', enterprise_id, 'objects')

for objects in glob.glob(objects_path + '/*'):
    if os.path.isdir(objects):
        _, model_name = os.path.split(objects)
        model_name = inflect.singular_noun(model_name)
        objiter = map(lambda x: json.load(open(x, 'r')), glob.glob(objects + '/*.json'))
    else:
        _, model_name = os.path.split(objects)
        model_name, _ = os.path.splitext(model_name)
        with open(objects, 'r') as fp:
            objiter = json.load(fp).itervalues()

    m = model.Model(model_name, enterprise_id, 'admin')
    if not (enterprise_id == 'core' and model_name == 'permission'):
        for obj in objiter:
            print >> sys.stderr, "Migrating model", model_name, "Object", obj
            m.post(obj)


