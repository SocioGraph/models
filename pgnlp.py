#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys
from libraries import json_tricks as json
from glob import glob
import operator
import helpers as hp
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as pathsep
from  attribute import Attribute
import pgpersist as db
from stuf import stuf
from base64 import b64encode
import shutil
import validators as val
import decimal
from pgp_db import JsonBDict

DEBUG_LEVEL = hp.logging.INFO
logger = hp.get_logger(__name__, DEBUG_LEVEL)

class NlpDict(JsonBDict):

    def __init__(self, *args, **kwargs):
        self.language = kwargs.pop('language', 'english')
        super(NlpDict, self).__init__(*args, **kwargs)

    def _create(self, cur = None, **kwargs):
        with self._transaction(cur = cur) as cur:
            super(NlpDict, self)._create(cur = cur, **kwargs)
            s = "CREATE INDEX IF NOT EXISTS {index_name} ON {name} USING GIN (to_tsvector('simple', {cols}));".format(
                    index_name = 'tsidx_{}'.format('_'.join(self.key_names)),
                    name = self.name,
                    cols = ' || \' \' || '.join(self.key_names),
                    language = self.language
            )
            logger.info(s)
            cur = self._execute(s, cur = cur)

    def _find_existing_indexes(self):
        ret = super(NlpDict, self)._find_existing_indexes()
        ret = [r for r in ret if 'tsidx' not in r]
        return ret

    def search(self, phrase, language = None, limit = 200):
        limit = limit or 200
        if isinstance(phrase, (str, unicode)):
            phrase = phrase.split()
        phrase = filter(lambda x: x not in ['?', '!', '.',','], phrase)
        language = language or self.language
        list_keys = {}
        def do_record(cur):
            index = len(list_keys)
            for record in cur:
                logger.debug("Fetching record %s", record)
                if index and record[-1] <= 0:
                    raise StopIteration
                v = {}
                for i, k1 in enumerate(self.key_names):
                    v[k1] = record[i]
                v.update(record[i+1])
                v['_weight'] = record[-1]
                k = u', '.join(self._uncast(v, r) for r, v in zip(self._key_types, record[0:len(self.key_names)]))
                if k not in list_keys:
                    index += 1
                    list_keys[k] = None
                    yield k, v
                if index >= limit:
                    raise StopIteration
        values = [u'&'.join(phrase), u'&'.join(phrase)]
        cur = self._connect(cursor = True, new = True, named = True, read = True)
        try:
            s = u"""
                SELECT {key}, dict, ts_rank_cd(to_tsvector('{language}', {key}),to_tsquery('{language}', %s)) as relevance 
                from {name} where {key_0} @@ to_tsquery('{language}', %s) 
                order by relevance desc 
                limit {limit}
            """.format(
                    key = ', '.join(self.key_names),
                    language = language,
                    name = self.name,
                    limit = limit,
                    key_0 = self.key_names[0],
            )
            logger.info(s, *values)
            cur = self._execute(s, values, cur = cur)
            for k, v in do_record(cur):
                yield k, v
        finally:
            self._close(cur)
        if len(list_keys) >= limit:
            raise StopIteration
        values = [u'|'.join(phrase), u'|'.join(phrase)]
        cur = self._connect(cursor = True, new = True, named = True, read = True)
        try:
            s = u"""
                SELECT {key}, dict, ts_rank_cd(to_tsvector('{language}', {key}),to_tsquery('{language}', %s)) as relevance 
                from {name} where {key_0} @@ to_tsquery('{language}', %s) and dict#>>'{{entities}}' is NOT NULL
                order by relevance desc 
                limit {limit}
            """.format(
                    key = ', '.join(self.key_names),
                    language = language,
                    name = self.name,
                    limit = limit,
                    key_0 = self.key_names[0],
            )
            logger.info(s, *values)
            cur = self._execute(s, values, cur = cur)
            for k, v in do_record(cur):
                yield k, v
        finally:
            self._close(cur)
        if len(list_keys) >= limit:
            raise StopIteration
        cur = self._connect(cursor = True, new = True, named = True, read = True)
        try:
            s = u"""
                SELECT {key}, dict, ts_rank_cd(to_tsvector('{language}', {key}),to_tsquery('{language}', %s)) as relevance 
                from {name} where {key_0} @@ to_tsquery('{language}', %s) 
                order by relevance desc 
                limit {limit}
            """.format(
                    key = ', '.join(self.key_names),
                    language = language,
                    name = self.name,
                    limit = limit,
                    key_0 = self.key_names[0],
            )
            logger.info(s, *values)
            cur = self._execute(s, values, cur = cur)
            for k, v in do_record(cur):
                yield k, v
        finally:
            self._close(cur)
        if len(list_keys) >= limit:
            raise StopIteration
        cur = self._connect(cursor = True, new = True, named = True, read = True)
        try:
            s = u"""
                SELECT {key}, dict, ts_rank_cd(to_tsvector({key}),to_tsquery(%s)) as relevance 
                from {name} where {key_0} @@ to_tsquery(%s) and dict#>>'{{entities}}' is NOT NULL
                order by relevance desc 
                limit {limit}
            """.format(
                    key = ', '.join(self.key_names),
                    language = language,
                    name = self.name,
                    limit = limit,
                    key_0 = self.key_names[0],
            )
            logger.info(s, *values)
            cur = self._execute(s, values, cur = cur)
            for k, v in do_record(cur):
                yield k, v
        finally:
            self._close(cur)
        if len(list_keys) >= limit:
            raise StopIteration
        cur = self._connect(cursor = True, new = True, named = True, read = True)
        try:
            s = u"""
                SELECT {key}, dict, ts_rank_cd(to_tsvector({key}),to_tsquery(%s)) as relevance 
                from {name} where {key_0} @@ to_tsquery(%s) 
                order by relevance desc 
                limit {limit}
            """.format(
                    key = ', '.join(self.key_names),
                    language = language,
                    name = self.name,
                    limit = limit,
                    key_0 = self.key_names[0],
            )
            logger.info(s, *values)
            cur = self._execute(s, values, cur = cur)
            for k, v in do_record(cur):
                yield k, v
        finally:
            self._close(cur)



if __name__ == '__main__':
    d = NlpDict('text', DB_NAME = 'cat', language = 'english')
    d['i am good'] = {'a': 1}
    d['i am not bad'] = {'b': 1}
    d['good, you are good'] = {'a': 1}
    d['good, you are __mine__'] = {'c': 1}
    d[u'आप अच्छे हैं'] = {'c': 1}
    for k, w in d.search(' '.join(sys.argv[1:]), language = 'english'):
        print(w)
    d.clear()
    
