import os, sys
import traceback
from glob import glob
from functools import wraps
import helpers as hp
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, getmtime as modified_time
from contextlib import contextmanager
import time
from attribute import Attribute, ModelNotFound, AttributeTypeError, AttributeNameError, RESERVED_NAMES
import pgpersist as db
from objects import JsonTypeObject, DirectoryTypeObject, PGDictTypeObject, AttributeRecordTypeObject, run_filters, PivotActionError, PivotAttributeError, PivotJoinError, run_filter, check_filter_by
from helpers import normalize, get_logger, error_raiser, pprint, BaseError, inflect, inf, normalize_join, I2CEError, I2CEErrorWrapper, print_error, json
from stuf import stuf
import validators as val
import shutil
from matcher import Matcher, MatcherDict
num_generations = 5


class GA(object):

    def cal_pop_fitness(self, equation_inputs, pop):
        # Calculating the fitness value of each solution in the current population.
        # The fitness function calculates the sum of products between each input and its corresponding weight.
        fitness = numpy.sum(pop*equation_inputs, axis=1)
        return fitness


    def select_mating_pool(self, pop, fitness, num_parents):
        # Selecting the best individuals in the current generation as parents for producing the offspring of the next generation.
        parents = numpy.empty((num_parents, pop.shape[1]))
        for parent_num in range(num_parents):
            max_fitness_idx = numpy.where(fitness == numpy.max(fitness))
            max_fitness_idx = max_fitness_idx[0][0]
            parents[parent_num, :] = pop[max_fitness_idx, :]
            fitness[max_fitness_idx] = -99999999999
        return parents

num_parents_mating = 4
for generation in range(num_generations):
     # Measuring the fitness of each chromosome in the population.
     fitness = GA.cal_pop_fitness(equation_inputs, new_population)
    # Selecting the best parents in the population for mating.
     parents = GA.select_mating_pool(new_population, fitness, 
                                       num_parents_mating)
 
     # Generating next generation using crossover.
     offspring_crossover = GA.crossover(parents,
                                        offspring_size=(pop_size[0]-parents.shape[0], num_weights))
 
     # Adding some variations to the offsrping using mutation.
     offspring_mutation = GA.mutation(offspring_crossover)
# Creating the new population based on the parents and offspring.
     new_population[0:parents.shape[0], :] = parents
     new_population[parents.shape[0]:, :] = offspring_mutation
