import datrie
import string
from collections import OrderedDict
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile, getmtime as modified_time
import helpers as hp
import time

BASE_PATH = joinpath(dirname(dirname(abspath(__file__))), 'data')

class TrieSpellChecker(object):
    def __init__(self, name, db_name = 'test', *args, **kwargs):
        self.spelltrie = SpellTrie(*args, **kwargs)
        self.name = name
        self.db_name = db_name
        self.load()
        self.path = joinpath(BASE_PATH, self.db_name, 'tries', self.name)

    def load(self):
        if exists(self.path):
            self.spelltrie.load(self.path)
            return
        hp.mkdir_p(self.path)

    def save(self):
        hp.mkdir_p(self.path)
        self.spelltrie.save(self.path)


    def similar_words(self, word):
        pass


class SpellTrie(object):

    def __init__(self, max_penalty=3, duplicate_penalty=0.5, interchange_penalty=0.5, deletion_weights=None, substitution_weights=None,normal_penalty=1, qwerty_penalty=0.7, qwerty_dict={}, *args, **kwargs):
        self.qwerty_dict = {'q': 'w', 'w': ('e', 'q'), 'e': ('r', 'w'), 'r': ('t', 'e'), 't': ('y', 'r'), 'y': ('u', 't'), 'u': ('i', 'y'), 'i': ('o', 'u'), 'o': ('p', 'i'), 'p': 'o', 'a': 's', 's': ('d', 'a'), 'd': ('f', 's'), 'f': ('g', 'd'), 'g': ('h', 'f'), 'h': ('j', 'g'), 'j': ('k', 'h'), 'k': ('l', 'j'), 'l': 'k', 'z': 'x', 'x': ('c', 'z'), 'c': ('v', 'x'), 'v': ('b', 'c'), 'b': ('n', 'v'), 'n': ('m', 'b'), 'm': 'n'}
        
        self.deletion_weights = deletion_weights or {'i': 0.8, 'e': 0.7, 'o':0.9, 'a': 0.9, 'u': 0.9}
        self.substitution_weights = substitution_weights or {('a', 's'): 0.8, ('i', 'e'): 0.8}
        
        self.max_penalty = max_penalty
        self.normal_penalty = normal_penalty
        self.qwerty_penalty = qwerty_penalty
        self.duplicate_penalty = duplicate_penalty
        self.interchange_penalty = interchange_penalty
        self.trie_forward = datrie.Trie(string.ascii_lowercase)
        self.trie_backward = datrie.Trie(string.ascii_lowercase)

    def save(self, path):
        hp.mkdir_p(path)
        self.trie_forward.save(join(path, 'f.pkl'))
        self.trie_backward.save(join(path, 'b.pkl'))
    
    def load(self, path):
        if not exists(path):
            raise Exception("Unknown path: %s", path)
        self.trie_forward = datrie.Trie.load(join(path, 'f.pkl'))
        self.trie_backward = datrie.Trie.load(join(path, 'b.pkl'))

    def add_word(self, word, freq = 1):
        word = unicode(word)
        bword = word[::-1]
        self.trie_forward[word] = freq
        self.trie_backward[bword] = freq
        return word

    def remove_word(self, word):
        word = unicode(word)
        bword = word[::-1]
        self.trie_forward.pop(word, None)
        self.trie_backward.pop(bword, None)
        return word

    def get_trie_list(self, word, max_penalty = 3):
        word = unicode(word)

        def matched_list(word, slice_increment):
            bword = word[::-1]
            l = len(word)
            mr = (l/2)-slice_increment
            lstd = {}
            for i in range(l, mr, -1):
                ph1 = self.trie_forward.suffixes(word[0:i])
                if ph1:
                    lstd.update({(word[0:i] + p):self.trie_forward[(word[0:i] + p)] for p in ph1})
                ph2 = self.trie_backward.suffixes(bword[0:i])
                if ph2:
                    lstd.update({(bword[0:i] + p)[::-1]:self.trie_forward[(bword[0:i] + p)[::-1]] for p in ph2})
                if i == mr+1 and ph1 == []:
                    ph1 = self.trie_forward.suffixes(word[0:i-1])
                    lstd.update({(word[0:i-1] + p):self.trie_forward[(word[0:i-1] + p)] for p in ph1})
                if i == mr+1 and ph2 == []:
                    ph2 = self.trie_backward.suffixes(bword[0:i-1])
                    lstd.update({(bword[0:i-1] + p)[::-1]:self.trie_forward[(bword[0:i-1] + p)[::-1]] for p in ph2})
            return lstd
        
        lstd = matched_list(word,1)

        lstd = OrderedDict(sorted(lstd.items(), key=lambda t: t[1], reverse=True))
        flstd ={}
        for f in lstd:
            if abs(len(f)-len(word))<=max_penalty:
                ed = self.edit_distance(f,word)
                if ed<=max_penalty:
                    flstd[f] = lstd[f]
        return flstd
    
    def edit_distance(self, w1, w2, m=None, n=None, dp=None, max_penalty=3):
        m = len(w1) if m is None else m
        n = len(w2) if n is None else n
        dp = [[0 for x in range(n + 1)] for x in range(m + 1)] if dp is None else dp
    
        if abs(m-n)>max_penalty:
            return abs(m-n)
        
        p = 0
        for i in range(m + 1):
            #for j in range(max(0, i - max_penalty), min(n + 1, i + max_penalty)):
            for j in range(n + 1):
                if i == 0:
                    dp[i][j] = j
                elif j == 0:
                    dp[i][j] = i
                
                elif w1[i-1] == w2[j-1]:
                    dp[i][j] = dp[i-1][j-1]
                else:
                    ins = dp[i][j-1]
                    delete = dp[i-1][j]
                    sub = dp[i-1][j-1]
                    md = min(ins, delete, sub)
    
                    if ins == md and i<m-1 and w1[i-1]==w2[j-2]:
                            p = self.duplicate_penalty
    
                    elif delete ==md:
                        if j<n-1 and w1[i-2]==w2[j-1]:
                            p = self.duplicate_penalty
                        elif w2[j-1] in self.deletion_weights.keys():
                            p = self.deletion_weights.get(w2[j-1])
                        else:
                            p = self.normal_penalty

                    elif sub ==md:
                        try:
                            if j>1 and i>1 and w2[j-1] in self.qwerty_dict[w1[i-1]]:
                                p = self.qwerty_penalty
                            else:
                                for a in self.substitution_weights.keys():
                                    if w1[i-1]==a[0] and w2[j-1]==a[1] or w1[i-1]==a[1] and w2[j-1]==a[0]:
                                        p = self.substitution_weights.get(a)
                                        break
                        except KeyError:
                            i #TODO find a way of dealing with this error
                    elif p==0:
                        p = self.normal_penalty

                    if w2[j-1] == w1[i-2] and w1[i-1]==w2[j-2]:
                        p = min(self.interchange_penalty,p)
                    
                    dp[i][j] = p+md

                    '''if dp[i][j]>max_penalty:
                        return max_penalty+1'''

            if min(dp[i]) > max_penalty:
                return max_penalty + 1

        if dp[m][n]<max_penalty:
            max_penalty = dp[m][n]
        return dp[m][n]

if __name__ == '__main__':
    m = SpellTrie()
    import requests
    import time
    #url = "https://www.dcs.bbk.ac.uk/~roger/missp.dat"
    url = "https://www.dcs.bbk.ac.uk/~roger/wikipedia.dat"
    uc = (requests.get(url).content).decode("utf-8").split("\n")
    wordgen = [row.lower() for row in uc]
    #cs - correctly spelt ws-wrongly spelt ki - key index cwd - correct wrong words dict
    cwd = {}
    for w in wordgen:
        if w.startswith('$'):
            m.add_word(w[1:],len(w))
            key = w
        else:
            if cwd.get(key) != None:
                l = list((cwd.get(key),[w]))
                cwd[key]=[item for sl in l for item in sl]
            else:
                cwd[key]=[w]
    cs = cwd.keys()
    ws = [item for sl in cwd.values() for item in sl]

    def check_ecs(word,word_dictionary):
        value = ''
        for k, v in word_dictionary.items():
            for z in v:
                if z==word:
                    value=k
                    break
        return value

    #cvc - correct value count wvc-wrong value count
    empty_count, cvc, wvc = (0,0,0)
    min_t = 1
    max_t = 0
    tt = 0
    ecs = '' #expected correct 
    for i,w in enumerate(ws):
        #if i<10:
            it = time.time()
            b = m.get_trie_list(w)
            ft = time.time()
            tt += ft-it
            ecs = check_ecs(w, cwd)
            if ecs[1:] in list(b.keys()):
                cvc += 1
            else:
                wvc += 1
            if b =={}:
                empty_count+=1
            if ft-it>max_t:
                max_t = ft-it
            if ft-it<min_t:
                min_t = ft-it
    print "Minimum time:", min_t, "Maximum time:", max_t, "Average time:", tt/len(ws), "Total count:", len(ws)
    print "Blanks:", empty_count, "Correct Values", cvc, "wrong values", wvc, "accuracy", float(cvc)/(cvc+wvc)*100
    print len(wordgen), len(ws), i+len(cs)+1
