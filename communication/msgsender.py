import sys,os
from os.path import exists as ispath, dirname, join as joinpath, abspath, sep as dirsep,isfile
d = dirname(dirname(abspath(__file__)))
if d not in sys.path:
    sys.path.append(d)
d = joinpath(dirname(dirname(abspath(__file__))), 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
from celery import Celery
import helpers as hp
import model as mod
import urllib
from xml.etree import ElementTree
import requests
from httplib import HTTPConnection
import pprint
import logging
from celery import current_app
import dill
import json
import boto3   # For AmazonSMSSender

def debug_requests_on():
    '''Switches on logging of the requests module.'''
    HTTPConnection.debuglevel = 1

    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True

config = {}
config['CELERY_BROKER_URL'] = 'redis://{}/{}'.format(os.environ.get('REDIS_URL', 'localhost:6379'), os.environ.get('REDIS_DB_NUMBER',0))
config['CELERY_TASK_SERIALIZER']='pickle'
celery = Celery(__name__, broker=config['CELERY_BROKER_URL'])
celery.conf.update(config)
AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
AWS_REGION_NAME = os.environ.get('AWS_REGION_NAME',"us-west-2")

DEBUG_LEVEL=logging.DEBUG
MAX_WAIT = 3600*24
EXOTEL_GATEWAY_NUMBER = '08030474799'
TEXTLOCAL_GATEWAY_SENDER = 'DAVEAI'

class SenderNotSetError(Exception):
    pass

def get_logger(name, level = None, output_file = None):
    logger = logging.getLogger(name)
    if not logger.handlers:
        if output_file is None:
            h = logging.StreamHandler(sys.stderr)
        else:
            h = logging.FileHandler(output_file)
        h.setFormatter(logging.Formatter("%(asctime)s: %(name)s: %(levelname)s:  %(message)s"))
        logger.addHandler(h)
        logger.setLevel(level or DEBUG_LEVEL)
    logger.propagate = False
    return logger
                             
logger = get_logger(__name__, DEBUG_LEVEL)

class SMSsender(object):

    def __init__(self, sender = None, hook = None, credentials = None, receive_hook = None, request_hook = None):
        self.sent_hook = hook
        self.receive_hook = receive_hook or hook
        self.sender = sender
        self.request_hook = request_hook
    
    def _check_to_sender(self, to, sender = None):
        #logger.debug("In Base sendMsg To = %s", to)
        if not isinstance(to, (list, tuple)):
            to = [to]
        sender = sender or self.sender
        if not sender:
            raise SenderNotSetError("sender field is not set")
        return to, sender

    def _format_mobile_number(self,no):
        no = str(no)
        if not no.startswith("+"):
            if len(no) == 10:
                no = "+91"+no
            else:
                no = "+"+no

        return no

    def _translate_status(self, status):
        return status

    def sendMsg(self, to, message, enterprise_id, view_id = None, sender = None, **kwargs):
        pass
             
    
    def process_hook(self, event = None, payload = None, **kwargs):
        pass        



class ExotelSMSSender(SMSsender):

    _sid = 'sociographsolutions'
    _token = 'f047de0da8cd64b17de5377b2edb42f6c39c489d'
    _base_url='twilix.exotel.in/v1/Accounts/'
    _url='https://twilix.exotel.in/v1/Accounts/{_sid}/Sms/send.json'.format(_sid=_sid)

    credentials = {'sid': _sid, 'token': _token, 'url': _url}

    def sendMsg(self,to, msg,enterprise_id,view_id=None,sender =None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        print "----to_msg---", to
        for no in to:
            no = self._format_mobile_number(no)
            ssid, status = self._toExotel(sender, no, msg,enterprise_id ,view_id, **kwargs)
            #logger.debug("status1:%s", status)
            if hasattr(self.sent_hook, '__call__'): 
                if status:
                    self.sent_hook(view_id,'sms', 'sent', enterprise_id, communication_id = kwargs.get('communication_id'))
                else:
                    self.sent_hook(view_id,'sms', 'failed', enterprise_id, communication_id = kwargs.get('communication_id'))
        return to

    
    def _toExotel(self, sender, to, msg, enterprise_id, view_id=None, **kwargs):
        call_back= '{}://{}/{}?{}'.format(
            os.environ.get('HTTP'),
            os.environ.get('SERVER_NAME', 'rudraksh.herokuapp.com'),
            'hooks/sms/exotel' if 'iamdave.ai' in os.environ.get('SERVER_NAME','') else 'dummy',
            urllib.urlencode({
                'enterprise_id': enterprise_id,
                'view_id': view_id,
                'communication_id': kwargs.get('communication_id')
            }),
        )
        r = requests.post(
            (kwargs.get('credentials') or {}).get('url') or self._url,
            auth = (
                (kwargs.get('credentials') or {}).get('sid') or self._sid, 
                (kwargs.get('credentials') or {}).get('token') or self._token
            ),
            data = {
                'sender': sender,
                'To': to,
                'Body': unicode(msg),
                'StatusCallback':call_back
            }
        )

        if r.status_code==400:
            return None, False
        ssid = r.json().get('SMSMessage').get('Sid')
        return ssid, (r.json().get('SMSMessage').get('Status')=='queued')


    def process_hook(self, event=None, payload=None, **kwargs):
        reason = None
        if payload.get('Status') in ['sent','failed','failed_dnd']:
            if payload.get('Status') =='sent':
                event = 'received'
            else:
                event = 'failed'
                reason = 'dnd' if payload.get('Status') == 'failed_dnd' else None
        else:
            raise Exception("Status {} is unknown for exotel hook".format(payload.get('Status')))
        if hasattr(self.receive_hook,'__call__'):
            call_event_hook=self.receive_hook(
                payload['view_id'], 'sms', event, payload['enterprise_id'], 
                reason = reason,
                communication_id = payload.get('communication_id')
            )
        return 'ok' 

class RouteSmsSender(SMSsender):
    route_url='http://sms6.routesms.com:8080/bulksms/bulksms'

    params={
        'username':'sociograph',
        'password':'socio123',
        'type':0,
        'dlr':1

    }

    def sendMsg(self, to, message,enterprise_id, sender = None):
        self.params['source']=sender
        self.params['message']=message
        to = self._format_mobile_number(to)
        self.params['destination']=to
        msg_id,status,to_no=self._toRouteSms()
        if hasattr(self.sent_hook, '__call__'): 
            self.sent_hook(view_type,no, status,enterprise_id)
        # if hasattr(self.receive_hook,'__call__'):
        #     if status:
        #         y=msg_checker.apply_async(args = [self, view_type,campaign_id,no,ssid,enterprise_id],countdown=60)
        return "success"

    def _toRouteSms(self):
        request=requests.post(self.route_url,params=self.params)
        r=request.text.split('|')
        status=r[0]
        to_no=r[1].split(':')[0]
        msg_id=r[1].split(':')[1]
        print status
        return msg_id,(status=='1701'),to_no

class AmazonSMSSender(SMSsender):

    def __init__(self, *args, **kwargs):
        super(AmazonSMSSender, self).__init__(*args, **kwargs)

        self.client = boto3.client(
            "sns",
            aws_access_key_id=AWS_ACCESS_KEY_ID,
            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
            region_name=AWS_REGION_NAME
        )

        self.client.set_sms_attributes(
            attributes={
                'DefaultSenderID': 'DAVEAI',
                'DefaultSMSType' : 'Transactional'
            }
        )
    
    def sendMsg(self,to, msg,enterprise_id,view_id=None,sender =None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        for no in to:
            if not (isinstance(no, (str, unicode)) and no):
                logger.warning("Empty sms number for view_id %s", view_id)
                continue
            no = self._format_mobile_number(no)
            try:
                resp = self.client.publish(
                    PhoneNumber=no,
                    Message=msg
                )
                logger.info("Sent SMS to number: %s via AmazonSMSSender, enterprise_id: %s, view_id: %s", no, enterprise_id, view_id)
                if hasattr(self.sent_hook, '__call__'): 
                    self.sent_hook(view_id,'sms', 'sent', enterprise_id, communication_id = kwargs.get('communication_id'))
            except Exception as ex:
                if hasattr(self.sent_hook, '__call__'): 
                    self.sent_hook(view_id,'sms', 'failed', enterprise_id, communication_id = kwargs.get('communication_id'))
                logger.error("Exception during send sms to: %s",no)
        return to

    
    def process_hook(self, event=None, payload=None, **kwargs):
        pass


class TextLocalSMSSender(SMSsender):
    route_url="https://api.textlocal.in/"
    api_key = 'ePXQ/jYW7lU-IYIFO28gNDHU9CwiTrldFMQpXsPqwr'

    def sendMsg(self,to, msg,enterprise_id,view_id=None,sender =None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        print "----to_msg---", to
        for no in to:
            no = self._format_mobile_number(no)
            ssid, status = self._toTextLocal(sender, no, msg,enterprise_id ,view_id, **kwargs)
            if hasattr(self.sent_hook, '__call__'): 
                if status == 'success':
                    self.sent_hook(view_id, 'sms', 'sent', enterprise_id, communication_id = kwargs.get('communication_id'))
                else:
                    self.sent_hook(view_id, 'sms', 'failed', enterprise_id, communication_id = kwargs.get('communication_id'))
        return to


    def _toTextLocal(self, sender, to, msg, enterprise_id, view_id=None, **kwargs):
        call_back= '{}://{}/{}'.format(
            os.environ.get('HTTP'),
            os.environ.get('SERVER_NAME', 'rudraksh.herokuapp.com'),
            'hooks/sms/textlocal' if 'iamdave.ai' in os.environ.get('SERVER_NAME','') else 'dummy',
        )
        logger.info("Text Local Message: %s", msg)
        r = requests.post(
            "{}/send".format(self.route_url),
            data = {
                'apikey' : kwargs.get('credentials', {}).get('api_key', self.api_key),
                'sender': kwargs.get('credentials', {}).get('sender', sender),
                'numbers': to,
                'message': (u'{}'.format(msg)).encode('utf-8'),
                'receipt_url' : call_back,
                'custom': '{}|{}|{}'.format(enterprise_id, view_id, kwargs.get('communication_id',''))
            }
        )

        if r.status_code>=400:
            logger.error("Error sending SMS: %s", r.text)
            return None, 'error'
        r = r.json()
        logger.info("Received response from TextLocal: %s", r)
        ssid = hp.make_single(r.get('messages', []), force = True, default = {}).get('id')
        logger.info("SSid : %s", ssid)
        return ssid, r.get('status')

    sts_map = {
        'D' : {
            "event" : "received",
            "reason" : "Message was delivered successfully."
        },
        'U' : {
            "event" : "failed",
            "reason" : "The message was undelivered."
        },
        "P" : {
            "event" : "pending",
            "reason" : "Message pending, the message is en route."
        },
        "I" : {
            "event" : "failed",
            "reason" : "The number was invalid."
        },
        "E" : {
            "event" : "failed",
            "reason" : "The message has expired."
        },
        "?" : {
            "event" : "pending",
            "reason" :"Message pushed to networks, the message is en route."
        },
        "B" : {
            "event" : "failed",
            "reason" : "dnd"
        }
    }

    def process_hook(self, event=None, payload=None, **kwargs):
        reason = None
        recipient = None
        sts = payload.get('status')
        try:
            enterprise_id, view_id, communication_id = payload.get('customId','||').split('|')
        except Exception:
            raise Exception("The customId: {} received from TextLocal is not parsable".format(payload.get('customId')))
        if sts in self.sts_map:
            args = [view_id, "sms", self.sts_map[sts]["event"], enterprise_id]
            kwargs.update({
                "reason": self.sts_map[sts]["reason"],
                "communication_id": communication_id or None
            })
        elif event == "request":
            view_id, enterprise_id, recipient, reason = self.process_request_hook(payload)
            args = [view_id, "sms", event, enterprise_id]
            kwargs.update({
                "recipient": recipient,
                "reason": reason
            })
            if hasattr(self.request_hook,'__call__'):
                call_event_hook = self.request_hook(
                    *args, **kwargs
                )
            return "ok"
        else:
            raise Exception("Status {} is unknown for exotel hook".format(sts))
        
        if hasattr(self.receive_hook,'__call__'):
            call_event_hook=self.receive_hook(
                *args, **kwargs
            )
        return 'ok' 

    def process_request_hook(self, payload):
        view_model = mod.Model('view','core')
        view_id = payload.get('comments')
        view = view_model.get(view_id)
        if not view:
            logger.error("No view with id %s", view_id)
        enterprise_id = view.get('enterprise_id')
        role = view.get("role") or "admin"
        user_id = view.get("user_id")
        return view_id, enterprise_id, {"phone_numbers": [payload.get("sender")]}, view.get('message')


def get_tag(text, name):
    return text[text.find('<' + name + '>'):text.find('</' + name + '>')].replace('<' + name + '>','')

class MGageSender(SMSsender):

    route_url = 'https://api.mgage.solutions/SendSMS/sendmsg.php'

    def __init__(self, *args, **kwargs):
        super(MGageSender, self).__init__(*args, **kwargs)
        credentials = kwargs.get('credentials') or {}
        self.credentials = {
                'uname': credentials.get('username', 'SPARAPI'),
                'pass': credentials.get("password", "max@123456"),
                'send': credentials.get('senderid', 'SPARON'),
        }

    def sendMsg(self,to, msg,enterprise_id,view_id=None,sender =None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        print "----to_msg---", to
        for no in to:
            params = {
                'dest': self._format_mobile_number(no),
                'msg': msg
            }
            params.update(self.credentials)
            r = requests.get(self.route_url, params = params)
            status = None
            if r.status_code != 200:
                status = r.text
            elif not r.text.isdigit():
                status = r.text
                logger.error("Could not sent SMS: %s", status)
            else:
                message_id = r.text
                payload = {
                    'communication_id': kwargs.get('communication_id'),
                    'enterprise_id': enterprise_id,
                    'view_id': view_id
                }
            if hasattr(self.sent_hook, '__call__'): 
                if status is None:
                    self.sent_hook(view_id,'sms', 'sent', enterprise_id, communication_id = kwargs.get('communication_id'))
                else:
                    self.sent_hook(view_id,'sms', 'failed', enterprise_id, reason = status, communication_id = kwargs.get('communication_id'))
        return to
    
    def process_hook(self, event=None, payload=None, **kwargs):
        reason = payload.get('reason')

        if hasattr(self.receive_hook,'__call__'):
            call_event_hook=self.receive_hook(
                payload['view_id'], 'sms', event, payload['enterprise_id'], 
                reason = reason,
                communication_id = payload.get('communication_id')
            )
        return 'ok' 

class KarixSMSSender(SMSsender):
    route_url = "https://karixsms.ktkbank.com/ConnectOneUrl/sendsms/sendsms"
    def __init__(self, *args, **kwargs):
        super(KarixSMSSender, self).__init__(*args, **kwargs)
        credentials = kwargs.get('credentials') or {
            'uname': os.environ.get('SMS_UNAME'),
            'password': os.environ.get('SMS_PASSWORD')
        }
        self.credentials = {}
        for k, v in [
                ('uname', None),
                ('pwd', ''),
                ('send', 'KBLBNK')

            ]:
            self.credentials[k] = credentials.get(k, v)

    def _format_mobile_number(self, no):
        no = str(no)
        if len(no) > 10:
            return '91'+no[-10:]
        return '91'+no

    def sendMsg(self,to, msg,enterprise_id,view_id=None,sender =None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        print "----to_msg---", to
        for no in to:
            params = {
                'mobile': self._format_mobile_number(no),
                'text': msg
            }
            params.update(self.credentials)
            try:
                r = requests.get(self.route_url, params = params, timeout=10)
                status = None
                if r.status_code != 200:
                    status = r.text
                    if self.sent_hook:
                        self.sent_hook(view_id,'sms', 'failed', enterprise_id, reason = status, communication_id = kwargs.get('communication_id'))
                else:
                    if self.sent_hook:
                        self.sent_hook(view_id,'sms', 'sent', enterprise_id, communication_id = kwargs.get('communication_id'))
                    logger.info("Message %s sent to %s successfully", msg, to)
            except requests.exceptions.ConnectTimeout:
                logger.error("Error in sending message")
                self.sent_hook(view_id,'sms', 'failed', enterprise_id, reason = 'ConnectionTimeout', communication_id = kwargs.get('communication_id'))
        return to
    
    def process_hook(self, event=None, payload=None, **kwargs):
        reason = payload.get('reason')

        if hasattr(self.receive_hook,'__call__'):
            call_event_hook=self.receive_hook(
                payload['view_id'], 'sms', event, payload['enterprise_id'], 
                reason = reason,
                communication_id = payload.get('communication_id')
            )
        return 'ok' 

class TataSender(SMSsender):
    route_url = "https://api.tatacommunications.com/mmx/v1/messaging/sms"
    def __init__(self, *args, **kwargs):
        super(TataSender, self).__init__(*args, **kwargs)
        credentials = kwargs.get('credentials') or {}
        self.credentials = {}
        for k, v in [
                ("SMS_TaTaUSERNAME", os.environ.get('SMS_TaTaUSERNAME', 'username')),
                ("SMS_TaTaPASSWORD", os.environ.get('SMS_TaTaPASSWORD', 'password'))
            ]:
            self.credentials[k] = credentials.get(k, v)

    def _format_mobile_number(self, no):
        no = str(no)
        if len(no) > 10:
            no = no[-10:]
        return "91" + no

    def sendMsg(self,to, msg,enterprise_id,view_id=None,sender =None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        print "----to_msg---", to
        for no in to:
            params = {
                'from': self.credentials.get('from', 'MSNEXA'),
                'to': self._format_mobile_number(no),
                'msg': msg
            }
            params.update(self.credentials)
            try:
                r = requests.post(
                    self.route_url, 
                    json = params, 
                    headers = {
                        'Authorization': 'Basic ' + hp.base64.b64encode('{}:{}'.format(
                                self.credentials['SMS_TaTaUSERNAME'], self.credentials['SMS_TaTaPASSWORD']
                            )
                        )
                    }, timeout=10
                )
                status = None
                if r.status_code >= 400:
                    status = r.text
                    if getattr(self, 'sent_hook'):
                        self.sent_hook(view_id,'sms', 'failed', enterprise_id, reason = status, communication_id = kwargs.get('communication_id'))
                    logger.error("Message %s sent to %s failed: %s", msg, to, status)
                else:
                    if getattr(self, 'sent_hook'):
                        self.sent_hook(view_id,'sms', 'sent', enterprise_id, communication_id = kwargs.get('communication_id'))
                    logger.info("Message %s sent to %s successfully", msg, to)
            except requests.exceptions.ConnectTimeout:
                logger.error("Error in sending message")
                if getattr(self, 'sent_hook'):
                    self.sent_hook(view_id,'sms', 'failed', enterprise_id, reason = 'ConnectionTimeout', communication_id = kwargs.get('communication_id'))
        return to
    
    def process_hook(self, event=None, payload=None, **kwargs):
        reason = payload.get('reason')

        if hasattr(self.receive_hook,'__call__'):
            call_event_hook=self.receive_hook(
                payload['view_id'], 'sms', event, payload['enterprise_id'], 
                reason = reason,
                communication_id = payload.get('communication_id')
            )
        return 'ok' 

class InfobipSender(SMSsender):
    route_url = "/sms/2/text/advanced"
    def __init__(self, *args, **kwargs):
        super(InfobipSender, self).__init__(*args, **kwargs)
        credentials = kwargs.get('credentials') or {}
        self.credentials = {}
        for k, v in [
                ("INFOBIP_BASE_URL", "https://5v2g6y.api.infobip.com"),
                ("INFOBIP_API_KEY", "25770715665e29992fa21bf2fa4631bf-5feb465b-d22e-4fb8-bfd3-2fe67e96e8b6"),
                ("INFOBIP_SENDER", "KOTAKA"),
                ("INFOBIP_TEMPLATE_ID", "1007563432718311953"),
                ("INFOBIP_ENTITY_ID", "110100001269")
            ]:
            self.credentials[k] = credentials.get(k, v)

    def _format_mobile_number(self, no):
        no = str(no)
        if len(no) > 10:
            no = no[-10:]
        return '91' + no

    def sendMsg(self,to, msg,enterprise_id,view_id=None,sender =None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        print "----to_msg---", to
        if not self.credentials.get('INFOBIP_API_KEY'):
            raise Exception("API_KEY not set for INFOBIP")
        if not self.credentials.get('INFOBIP_BASE_URL').startswith('http'):
            self.credentials['INFOBIP_BASE_URL'] = 'https://{}'.format(self.credentials['INFOBIP_BASE_URL'])
        for no in to:
            params = {
                "messages": [
                    {
                        "destinations": [
                            {
                                "to": self._format_mobile_number(no)
                                }
                            ],
                        "from": self.credentials.get("INFOBIP_SENDER", "InfoSMS"),
                        "text": msg,
                        "regional": {
                            "indiaDlt": {
                                "contentTemplateId": self.credentials.get("INFOBIP_TEMPLATE_ID", ""),
                                "principalEntityId": self.credentials.get("INFOBIP_ENTITY_ID", "")
                            }
                        }
                    }
                ]
            }
            headers = {
                'Authorization': 'App {}'.format(self.credentials.get('INFOBIP_API_KEY')),
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
            logger.info("URL: %s", self.credentials.get('INFOBIP_BASE_URL') + self.route_url)
            logger.info("Params: %s", params)
            logger.info("Headers: %s", headers)
            try:
                r = requests.post(
                    self.credentials.get('INFOBIP_BASE_URL') + self.route_url, 
                    json = params, headers = headers, timeout=10
                )
                status = None
                if r.status_code >= 400:
                    status = r.text
                    if getattr(self, 'sent_hook'):
                        self.sent_hook(view_id,'sms', 'failed', enterprise_id, reason = status, communication_id = kwargs.get('communication_id'))
                    logger.error("Message %s sent to %s failed: %s", msg, no, status)
                else:
                    if getattr(self, 'sent_hook'):
                        self.sent_hook(view_id,'sms', 'sent', enterprise_id, communication_id = kwargs.get('communication_id'))
                    logger.info("Message \"%s\" sent to %s successfully", msg, self._format_mobile_number(no))
            except requests.exceptions.ConnectTimeout:
                logger.error("Error in sending message")
                if getattr(self, 'sent_hook'):
                    self.sent_hook(view_id,'sms', 'failed', enterprise_id, reason = 'ConnectionTimeout', communication_id = kwargs.get('communication_id'))
        return to
    
    def process_hook(self, event=None, payload=None, **kwargs):
        reason = payload.get('reason')

        if hasattr(self.receive_hook,'__call__'):
            call_event_hook=self.receive_hook(
                payload['view_id'], 'sms', event, payload['enterprise_id'], 
                reason = reason,
                communication_id = payload.get('communication_id')
            )
        return 'ok' 

class TwoFactorSender(SMSsender):
    route_url = "https://2factor.in/API/V1"
    def __init__(self, *args, **kwargs):
        super(TwoFactorSender, self).__init__(*args, **kwargs)
        credentials = kwargs.get('credentials') or {}
        self.credentials = {}
        for k, v in [
                ('api_key', 'f71b267a-2cfb-11ed-9c12-0200cd936042'),
                ('template_name', 'OTP Website - Merino Laminates'),
                ('sender_id', 'MERINO')
            ]:
            self.credentials[k] = credentials.get(k, v)

    def _format_mobile_number(self, no):
        no = str(no)
        if len(no) > 10:
            return no[-10:]
        return no

    def sendMsg(self,to, msg,enterprise_id,view_id=None,sender =None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        print "----to_msg---", to
        for no in to:
            try:
                url = '{route_url}/{api_key}/SMS/{no}/{msg}/{template_name}'.format(
                    route_url = self.route_url, 
                    api_key = self.credentials.get('api_key'), 
                    no = no,
                    msg = msg,
                    template_name = self.credentials.get('template_name')
                )
                logger.info("URL for TwoFactorSender: %s", url)
                r = requests.get(url)
                status = None
                if r.status_code != 200:
                    status = r.text
                    if getattr(self, 'sent_hook'):
                        self.sent_hook(view_id,'sms', 'failed', enterprise_id, reason = status, communication_id = kwargs.get('communication_id'))
                else:
                    if getattr(self, 'sent_hook'):
                        self.sent_hook(view_id,'sms', 'sent', enterprise_id, communication_id = kwargs.get('communication_id'))
                    logger.info("Message %s sent to %s successfully", msg, to)
            except requests.exceptions.ConnectTimeout:
                logger.error("Error in sending message")
                self.sent_hook(view_id,'sms', 'failed', enterprise_id, reason = 'ConnectionTimeout', communication_id = kwargs.get('communication_id'))
        return to
    
    def process_hook(self, event=None, payload=None, **kwargs):
        reason = payload.get('reason')

        if hasattr(self.receive_hook,'__call__'):
            call_event_hook=self.receive_hook(
                payload['view_id'], 'sms', event, payload['enterprise_id'], 
                reason = reason,
                communication_id = payload.get('communication_id')
            )
        return 'ok' 

class AirtelSender(SMSsender):
    route_url = "https://digimate.airtel.in:15443/BULK_API/SendMessage"
    def __init__(self, *args, **kwargs):
        super(AirtelSender, self).__init__(*args, **kwargs)
        credentials = kwargs.get('credentials') or {}
        self.credentials = {}
        for k, v in [
                ('loginID', 'maruti_tuser'),
                ('password', 'vik@123'),
                ('DLT_PE_ID', '1601100000000001379'),
                ('DLT_CT_ID', '1007823664046087539'),
                ('DLT_TM_ID', '1001096933494158'),
                ('senderid', 'MARENA'),
                ('route_id', 'DLT_SERVICE_IMPLICT'),
                ('Unicode', 0),
                ('camp_name', 'maruti_tuser')
            ]:
            self.credentials[k] = credentials.get(k, v)

    def _format_mobile_number(self, no):
        no = str(no)
        if len(no) > 10:
            return no[-10:]
        return no

    def sendMsg(self,to, msg,enterprise_id,view_id=None,sender =None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        print "----to_msg---", to
        for no in to:
            params = {
                'mobile': self._format_mobile_number(no),
                'text': msg
            }
            params.update(self.credentials)
            try:
                r = requests.get(self.route_url, params = params, timeout=10)
                status = None
                if r.status_code != 200:
                    status = r.text
                    if getattr(self, 'sent_hook'):
                        self.sent_hook(view_id,'sms', 'failed', enterprise_id, reason = status, communication_id = kwargs.get('communication_id'))
                else:
                    if getattr(self, 'sent_hook'):
                        self.sent_hook(view_id,'sms', 'sent', enterprise_id, communication_id = kwargs.get('communication_id'))
                    logger.info("Message %s sent to %s successfully", msg, to)
            except requests.exceptions.ConnectTimeout:
                logger.error("Error in sending message")
                self.sent_hook(view_id,'sms', 'failed', enterprise_id, reason = 'ConnectionTimeout', communication_id = kwargs.get('communication_id'))
        return to
    
    def process_hook(self, event=None, payload=None, **kwargs):
        reason = payload.get('reason')

        if hasattr(self.receive_hook,'__call__'):
            call_event_hook=self.receive_hook(
                payload['view_id'], 'sms', event, payload['enterprise_id'], 
                reason = reason,
                communication_id = payload.get('communication_id')
            )
        return 'ok' 

class DizitalArenaSender(SMSsender):

    route_url = 'http://trans.dizisms.in/sendsms.jsp'
    report_url = 'http://trans.dizisms.in/getDLR.jsp'

    def __init__(self, *args, **kwargs):
        super(DizitalArenaSender, self).__init__(*args, **kwargs)
        credentials = kwargs.get('credentials') or {}
        self.credentials = {
                'user': credentials.get('username', 'DZDEMO'),
                'password': credentials.get("password", "DZDEMO"),
                'senderid': credentials.get('senderid', 'DIZITL'),
        }

    def sendMsg(self,to, msg,enterprise_id,view_id=None,sender =None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        print "----to_msg---", to
        for no in to:
            params = {
                'mobiles': self._format_mobile_number(no),
                'sms': msg
            }
            params.update(self.credentials)
            r = requests.get(self.route_url, params = params)
            status = None
            if r.status_code != 200:
                status = r.text
            elif 'error-description' in r.text:
                status = get_tag(r.text, 'error-description')
                logger.error("Could not sent SMS: %s", status)
            else:
                message_id = get_tag(r.text, 'messageid')
                payload = {
                    'communication_id': kwargs.get('communication_id'),
                    'enterprise_id': enterprise_id,
                    'view_id': view_id
                }
                get_dizital_report.apply_async(args = (self, message_id, payload), kwargs = {'countdown': 600}, countdown = 600)
            if hasattr(self.sent_hook, '__call__'): 
                if status is None:
                    self.sent_hook(view_id,'sms', 'sent', enterprise_id, communication_id = kwargs.get('communication_id'))
                else:
                    self.sent_hook(view_id,'sms', 'failed', enterprise_id, reason = status, communication_id = kwargs.get('communication_id'))
        return to
    
    def process_hook(self, event=None, payload=None, **kwargs):
        reason = payload.get('reason')

        if hasattr(self.receive_hook,'__call__'):
            call_event_hook=self.receive_hook(
                payload['view_id'], 'sms', event, payload['enterprise_id'], 
                reason = reason,
                communication_id = payload.get('communication_id')
            )
        return 'ok' 

@celery.task(name='get_dizital_report')
def get_dizital_report(self, message_id, redownload = False, countdown = 600):
    params = {
        'messageid': message_id,
        'responsetype': 'xml'
    }
    if redownload:
        params['redownload'] =  'yes'
    params.update(self.credentials)
    r = requests.get(self.report_url, params = params)
    event = get_tag(r.text, 'status')
    payload['reason'] = get_tag(r.text, 'undeliveredreason')
    self.process_hook(event, payload)
    if event == 'pending':
        get_dizital_report.apply_async(args = (self, message_id, payload), kwargs = {'redownload': True, 'countdown': 2*countdown}, countdown = 2*countdown)


PROVIDERS = {
    'AmazonSMSSender': AmazonSMSSender,
    'ExotelSMSSender': ExotelSMSSender,
    'RouteSmsSender': RouteSmsSender,
    'TextLocalSMSSender': TextLocalSMSSender,
    'DizitalArenaSender': DizitalArenaSender,
    'MGage': MGageSender,
    'AirtelSender': AirtelSender,
    'TataSender': TataSender,
    'InfobipSender': InfobipSender,
    'KarixSMSSender': KarixSMSSender,
    'TwoFactorSender': TwoFactorSender,
    '__default__': TextLocalSMSSender
}

#celery task for msg and mail
@celery.task(name='start_sms_task')
def start_sms(
        to_nos, 
        sms_text,
        sender_name, 
        view_type=None, enterprise_id=None, view_id=None, 
        hook=None, 
        provider=None, 
        **kwargs
    ):
    """
    Inputs:
        to_nos              : phone_number list
        sms_text            : subject line
        sender_name         : sender's name/number
        view_type           : campaign name, or recommendation or conversation or transaction
        enterprise_id       : enterprise_id for the enterprise we want to send the mail for
        view_id             : view_id to be sent for global link access
        hook                : event hook function
        provider            : the mail provider
        test                : test mode is True
        credentials         : credentials for the provider, takes default ones if not provided
        communication_id    : communication id if required
    """
    
    """
    if provider=='route_sms':
        sender_object = RouteSmsSender(hook=hook)
        sent_no=sender_object.sendMsg(to_nos, sms_text, enterprise_id, view_id, sender = sender_name, **kwargs)    
    elif provider == 'exotel':
        sender_object = ExotelSMSSender(hook=hook)
        sent_no=sender_object.sendMsg(to_nos, sms_text, enterprise_id, view_id, sender = sender_name, **kwargs)
    else:
    """
    if kwargs.get('test') or os.environ.get('ENVIRONMENT', 'dev').strip() not in ['production', 'staging']:
        logger.info("Original SMS number is: %s", ",".join(hp.make_list(to_nos)))
        to_nos = os.environ.get('I2CE_PHONE_NUMBER', '+91 9700938804')
        logger.info("Sending debug SMS to: %s", to_nos)

    provider = provider or '__default__'
    sender_object = PROVIDERS.get(provider, AmazonSMSSender)(hook=hook, credentials = kwargs.get('credentials'))
    sent_no=sender_object.sendMsg(to_nos, sms_text, enterprise_id, view_id, sender = sender_name, **kwargs)

    

if __name__=='__main__':
    sent_no=start_sms((len(sys.argv) > 1 and sys.argv[1]) or '9980838165', (len(sys.argv) > 2 and sys.argv[2]) or '3243', 'DAVEAI',view_id='grtrg', provider = (len(sys.argv)> 3 and sys.argv[3]) or 'TwoFactorSender')
