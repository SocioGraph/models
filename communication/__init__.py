import sys, os
from os.path import exists as ispath, dirname, join as joinpath, abspath, sep as dirsep,isfile
BASE_DIR_PATH = dirname(dirname(dirname(abspath(__file__))))
if BASE_DIR_PATH not in sys.path:
    sys.path.append(BASE_DIR_PATH)
d = joinpath(BASE_DIR_PATH, 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
from celery import Celery 
import mail_sender, msgsender,notification_sender
from models import helpers as hp, model as base_model, interactions
from flask import render_template,current_app as app, url_for,render_template_string, Flask
import jinja2.exceptions  as jinja_exep
from flask_cdn import CDN
Model = base_model.Model
logger = hp.get_logger(__name__)

class CommunicationSendError(hp.I2CEError):
    pass

class ReceiverIdNotExist(hp.I2CEError):
    pass

class UnknownTemplateError(hp.I2CEError):
    pass

class UnknownViewId(hp.I2CEError):
    pass

class UnknownActionError(hp.I2CEError):
    pass

class UnknownEnterpriseId(hp.I2CEError):
    pass

class ViewTemplateError(hp.I2CEErrorWrapper):
    pass

class ViewParamError(hp.I2CEError):
    pass

class TemplateNotFound(hp.I2CEError):
    pass

class View(object):
   
    @property
    def template_folder(self):
        with app.app_context():
            return app.config.get('TEMPLATE_FOLDER', joinpath(BASE_DIR_PATH, 'templates'))
    @property
    def static_folder(self):
        with app.app_context():
            return app.config.get('UPLOADS_FOLDER', joinpath(BASE_DIR_PATH, 'static/uploads'))


    template_list = ['html', 'csv', 'push', 'mail', 'json', 'email']
    view_alias = {
            'w': 'html',
            's': 'html',
            'p': 'html',
            'm': 'mail_view',
            'j': 'json',
            'c': 'csv',
    }
    channel_alias={
            'w': 'web',
            's': 'sms',
            'p': 'push_notification',
            'm': 'email',
            'j': 'json',
            'c': 'csv',
    }

    @classmethod
    def init_view_object(cls, view_id):
        m = Model('view', 'core')
        v = m.get(view_id)
        if not v:
            raise UnknownViewId("view_id: {} is not found".format(view_id))
        enterprise_id=v.get('enterprise_id')
        if not enterprise_id:
            raise UnknownEnterpriseId("No enterprise with id {} found".format(enterprise_id))
        return v

    @classmethod
    def init_view(cls, view_id):
        v = cls.init_view_object(view_id)
        view = cls(v.pop('enterprise_id'), **v)
        return view

    @classmethod
    def get_action(cls, view_id, action=None, **data):
        view_obj = cls.init_view_object(view_id)
        action_groups = view_obj.get('action_groups', view_obj.get('data', {}).get("action_groups",{}))
        if action not in action_groups:
            raise UnknownActionError("Unknown action {} for view_id {}".format(action, view_id))
        naction = action_groups.get(action, {})
        naction['data'] = naction.get('data', {})
        naction['data'].update(data)
        naction['data']['view_data'] = view_obj.get("data",{})
        return naction, view_obj.get('enterprise_id'), 'admin'


    @classmethod
    def render_view(cls, view_id, render_type = 'json', channel = None, communication_id = None, **kwargs):
        view = cls.init_view(view_id)
        view.run_actions()
        communication_hook(view_id, cls.channel_alias.get(channel, 'web'), 'viewed', view.get("enterprise_id"), communication_id = communication_id)
        render_type = cls.view_alias.get(channel,"json")
        renderer = getattr(view, 'render_' + render_type)
        if not renderer:
            raise UnknownTemplateError("Template type {} is unknown for enterprise {}. Allowed types are {}".format(
                render_type, enterprise_id, ', '.join(cls.template_list))
            )
        return renderer(**kwargs)

    _view_attributes = dict(
        view_type = 'transaction', 
        title = None, message = None, 
        email_template=None, html_string = None,  html_template = None, csv_template = None, push_template = None, 
        pdf = False, cc = None, bcc = None, individual = True, credentials = None, provider = None,
        receiver_model = None, receiver_ids = None, delete_after_send = True,
        sender = {}, receiver = {}, 
        view_id = None, pre_actions = {},
        data = {},
    )

    @classmethod
    def get_view_attributes(cls):
        return cls._view_attributes.copy()

    def __init__(self, enterprise_id, role = 'admin', user_id = None, **kwargs):
        self.view = {
            'enterprise_id': enterprise_id,
            'role': role,
            'user_id': user_id
        }
        self.view.update(kwargs)
        self.id = kwargs.get('view_id')
        self.enterprise_id = enterprise_id
        self.receiver = kwargs.get('receiver', {})
        self.data = kwargs.get('data', {})
        self.data['personalized_url'] = '{personalized_url}'
        self.data['sender'] = self.view.get('sender')
        self.data['receiver'] = self.view.get('receiver')
        self.data['enterprise_id'] = self.view['enterprise_id']
        enterprise = Model("enterprise", "core").get(self.view["enterprise_id"])
        self.data['show_notification'] = False
        self.data['enterprise_name']= enterprise.get("name")
        self.data['enterprise_obj']= enterprise
        self.enterprise_obj = enterprise
        if self.view.get('receiver_model'):
            rmodel = Model(self.view['receiver_model'], self.view['enterprise_id'], self.view.get('role', 'admin'), user_id = self.view.get('user_id'))
            if rmodel.push_tokens:
                action_groups = self.view.get('action_groups', self.data.pop("action_groups",{}))
                action_groups['subscribe_notification'] = {
                    'actions': [
                        {
                            "_action": "update",
                            "_model": self.view.get('receiver_model'),
                            "_model_ids": self.view.get("receiver_ids"),
                            "_instance": {rmodel.push_tokens[0]: '{player_id}'}
                        }
                    ]
                }
                self.view['action_groups'] = action_groups
                self.data['show_notification'] = True
                self.data['onesignal_app_id'] = enterprise.get("onesignal_app_id", "c2ae342a-ac06-4592-99e1-44648a089416")
                self.data['safari_id'] = enterprise.get("safariweb_app_id", "web.onesignal.auto.129325ca-9c33-4be9-9043-3e5a3c869fcf")
        self.format_data = hp.dstuf()
        for k_, v_ in self.data.iteritems():
            hp.make_format_dict(self.format_data, k_, v_)
        for a in ['message', 'title']:
            if self.view.get(a) is not None:
                try:
                    self.view[a] = hp.format_string(self.view.get(a), self.format_data)
                except IndexError:
                    pass
                except (AttributeError, KeyError, SyntaxError, NameError) as e:
                    raise ViewParamError("Template variable {} not found in the view data for {}: {}".format(e, a, self.view.get(a)))
                self.data[a] = self.view.get(a)
        for a in ['cc', 'bcc', 'pdf', 'individual', 'credentials']:
            if self.view.get(a) is not None:
                try:
                    self.view[a] = hp.format_string(self.view.get(a), self.format_data)
                except(AttributeError, KeyError,  IndexError) as e:
                    logger.warning("Template variable %s not found in the view data for %s: %s", e, a, self.view.get(a))
                self.data[a] = self.view.get(a)

    def get_receiver(self, attrib = None, default = None):
        if attrib:
            return self.receiver.get(attrib, default)
        return self.receiver.copy()

    def get_data(self, attrib = None, default = None):
        if attrib:
            return self.data.get(attrib, default)
        return self.data.copy()

    def get(self, attrib = None, default = None):
        if attrib:
            return self.view.get(attrib, default)
        return self.view.copy()

    get_view = get

    def create(self):
        view_model = Model('view', 'core')
        v = view_model.post(hp.remove_unwanted_keys(self.view))
        self.id = v['view_id']
        self.view['view_id'] = self.id
        return self.id

    def update(self):
        view_model = Model('view', 'core')
        self.id = self.view.get('view_id')
        view_model.update(self.id, hp.remove_unwanted_keys(self.view))
        return self.id

    def run_actions(self):
        actions = self.view.get("pre_actions") or self.data.get("pre_actions",[])
        if actions:
            act = do_actions(
                {"actions": actions, "data": self.data}, 
                self.data["enterprise_id"], 
                role = self.view.get('role', 'admin'), 
                user_id = self.view.get('user_id'),
                raise_error = True,
                return_act = True
            )
            self.data.update(act.data)
            act.evaluate_args(self.view)
            logger.info("Current data: %s", self.data)

    def _render_template(self, path, as_string = False, **data):
        with app.app_context():
            data.update(hp.default_dict())
            if as_string:
                r = render_template_string(path, **data)
            elif ispath(path):
                with open(path, 'r') as fp:
                    r = render_template_string(fp.read(), **data)
            elif ispath(joinpath(self.static_folder, path)):
                with open(joinpath(self.static_folder, path), 'r') as fp:
                    r = render_template_string(fp.read(), **data)
            elif ispath(joinpath(self.template_folder, self.enterprise_id, path)):
                r = render_template(
                        joinpath(self.enterprise_id, path),
                        **data
                )
            elif ispath(joinpath(self.template_folder, path)):
                r = render_template(
                        path,
                        **data
                )
            else:
                raise TemplateNotFound("view template {} is not found, in both templates folder and uploads folder".format(path))
            r = r.strip()
            return r

    def render_mail(self, as_email = True, communication_id = None, **kwargs):
        try:
            self.data['view_id'] = self.id
            data = self.data.copy()
            data['personalized_url'] = url_for(
                'i2ce_route.get_view',channel='m',
                view_id = self.id, 
                communication_id = communication_id, 
                _external = True
            )
            kwargs.update(data)
            if self.view.get("html_string"):
                p = self.view.get("html_string")
                as_string = True
            else:
                p = self.view.get("email_template") or self.view.get("html_template")
                as_string = False
            return self._render_template(
                p, 
                as_string = as_string,
                as_email = as_email,
                **kwargs
            )
        except jinja_exep.TemplateNotFound:
            raise TemplateNotFound("view template {} is not found.".format(p))
        except Exception as e:
            hp.print_error()
            raise ViewTemplateError("In view template {}, Tries to operate unknown`s:".format(e))

    def render_push(self, **kwargs):
        pass


    render_email = render_mail

    def render_mail_view(self, communication_id = None, **kwargs):
        return self.render_mail(as_email = False, communication_id = communication_id, **kwargs)

    render_email_view = render_mail_view

    copy_data_to_pop = ['message', 'title', 'sender', 'receiver', 'receiver_model', 'receiver_ids', 'enterprise_obj', 'show_notification', 'personalized_url']

    def render_html(self, **kwargs):
        if self.view.get('html_string'):
            tmpl = self.view.get("html_string")
            as_string = True
        else:
            tmpl = self.view.get("html_template") or self.view.get("email_template")
            as_string = False
        if not tmpl:
            raise TemplateNotFound("view template {} is not found.".format(tmlp))
        try:
            self.data['view_id'] = self.id
            kwargs.update(self.data)
            return self._render_template(tmpl, as_string = as_string, **kwargs)
        except jinja_exep.TemplateNotFound:
            raise TemplateNotFound("view id {} template {} is not found.".format(self.id, tmpl))
        except Exception as e:
            raise ViewTemplateError("In view id {} template {}, Tries to operate unknown`s:".format(self.id, tmpl))

    def render(self, **kwargs):
        v = self.data.copy()
        for t in self.copy_data_to_pop:
            v.pop(t, None)
        kwargs.update(v)
        return kwargs

    def render_json(self, **kwargs):
        return hp.json.dumps(self.render(**kwargs), indent = 4, encoding = 'utf-8')

    def render_csv(self, **kwargs):
        pass

def request_hook(view_id, channel, event_name, enterprise_id, recipient = None, reason = None, communication_id = None, **data):
    data.update({
        "receiver":  recipient,
        "message": u"{message} {http}://{server}/v/{view_id}".format(
            http = os.environ.get('HTTP', 'http'), 
            server = os.environ.get("SERVER_NAME", "localhost:5000"),
            view_id = view_id,
            message = reason or "Please visit the website you requested: "
        ) if view_id else reason,
        "channels": [ channel ],
        "delay": 0
    })
    try:
        c = Communication(
            enterprise_id, 'admin'
        )
        c.send(
            **data
        )
    except Exception as e:
        hp.print_error()
        return False
    return True

def communication_hook(view_id, channel, event_name, enterprise_id, recipient = None, reason = None, communication_id = None, **data):
    rates_plan={
                "total_email_failed": 10, 
                "total_email_viewed": 50, 
                "total_email_clicked": 10, 
                "total_email_sent":  1,
                "total_sms_sent": 3,  
                "total_sms_failed": 0,
                "total_sms_received":0,
                "total_notification_sent": 0
            }

    mail_bill={'sent':'total_email_sent',
        'viewed':'total_email_viewed',
        'failed':'total_email_failed'
    }
    sms_bill={'sent':'total_sms_sent',
        'failed':'total_sms_failed',
        'received':'total_sms_received'
    }
    notification_bill = {
        'sent': 'total_notification_sent'
    }
    enterprise = Model('enterprise', 'core')
    comm = Model('communication', enterprise_id)
    com_obj = {}
    if communication_id:
        com_obj = comm.get(communication_id)
    post_obj = {
        'view_id': view_id,
        'event': event_name,
        'channel': channel, 
        'reason': reason or com_obj.get('reason'), 
        'data': data or com_obj.get('data'), 
        'recipient': recipient or com_obj.get('recipient')
    }
    if communication_id:
        post_obj['communication_id'] = hp.make_uuid(communication_id, channel, event_name)
        com_obj = comm.get(post_obj['communication_id'])
        if com_obj:
            return com_obj
    logger.info("Posting Communication: %s", post_obj)
    com_obj = comm.post(post_obj)
    if mail_bill.get(event_name) and (channel in ['email', 'mail']):
        interactions.deduct_charges(enterprise_id, mail_bill[event_name], 1)
    if sms_bill.get(event_name) and (channel in ['sms', 'message']):
        interactions.deduct_charges(enterprise_id, sms_bill[event_name], 1)
    if notification_bill.get(event_name) and (channel in ['notification', 'push_notification', 'push']):
        interactions.deduct_charges(enterprise_id, notification_bill[event_name], 1)
    return com_obj


class Communication(object):

    def get_prefered_channels(self, receiver_object, channels = None):
        # find which data is available and send a list of channels
        # TODO: matcher
        if receiver_object.get('emails') and (channels is None or 'email' in channels or 'mail' in channels):
            yield 'email'
        if receiver_object.get('phone_numbers') and (channels is None or 'sms' in channels or 'message' in channels):
            yield 'sms'
        if receiver_object.get('push_tokens') and (channels is None or 'push_notification' in channels or 'notification' in channels):
            yield 'push_notification'

    def get_prefered_delay(self, receiver, channel, delay = 0):
        return delay

    def __init__(self,enterprise_id, role = 'admin', receiver_model = None, test = False, user_id = None):
        self.enterprise_id = enterprise_id
        self.role = role
        self.test = test
        self.user_id = user_id
        self.enterprise = Model('enterprise', 'core').get(enterprise_id)
        if receiver_model:
            self.receiver_model = Model(receiver_model, enterprise_id, role, user_id = user_id)
        else:
            self.receiver_model=None


    # These are the attributes we can send to _send_attributes
    _send_attributes = View.get_view_attributes().update(channels = None, delay = 0)

    def send_mail(self, view, sender, receiver, countdown):
        logger.info("sending mail to receiver: %s", receiver)
        if not view.get('individual', True):
            receiver['emails'] = hp.make_list(','.join(hp.make_list(receiver["emails"])))
        tot = len(receiver['emails'])
        sent = 0
        for recipient in receiver['emails']:
            com_obj = communication_hook(view.id, 'email', 'created', self.enterprise_id, recipient = recipient)
            html = ''
            if view.get('email_template') or view.get('html_string') or view.get('html_template'):
                html = view.render_mail(communication_id = com_obj['communication_id'])
            provider = view.get('provider') or self.enterprise.get('email_provider', '__default__')
            credentials = self.enterprise.get('email_provider_credentials') or {}
            credentials.update(view.get('credentials') or view.data.get('_credentials') or {})
            sent += 1
            args1 = [ 
                recipient, 
                view.get('title') or 'Message from {}'.format(self.enterprise.get('name')), 
                html if html else None,
                sender['name'],
                view.get('view_type')
            ]
            kwargs1 = { 
                'enterprise_id': self.enterprise_id,
                'view_id': view.id,
                'cc': view.get('cc', view.data.get('_cc',)),
                'bcc': view.get('bcc', view.data.get('_bcc')),
                'pdf' : view.get('pdf', view.data.get('_pdf')), 
                'pdf_html' : html,
                'hook':communication_hook,
                'text': view.get('message') or view.get('title'),
                'provider': provider,
                'credentials': credentials,
                'test': self.test,
                'communication_id': com_obj['communication_id'],
                'files' : self.files,
                'delete_after_send': view.get('delete_after_send', view.data.get('delete_after_send', True)) if sent >= tot else False
            }
            mail_sender.start_mail.apply_async(
                args = args1,
                kwargs = kwargs1,
                countdown = countdown,
                queue = 'communication'
            )
            #mail_sender.start_mail(*args1, **kwargs1)

    def send_sms(self, view, sender, receiver, countdown):
        logger.info("sending sms to receiver: %s", receiver)
        for recipient in receiver['phone_numbers']:
            com_obj = communication_hook(view.id, 'sms', 'created', self.enterprise_id, recipient = recipient)
            sms_text = view.get('message') or view.get('title') or ''
            if '{personalized_url}' in sms_text:
                sms_text = sms_text.format(
                    personalized_url = url_for(
                        'i2ce_route.get_view',channel='s',
                        view_id = view.id, 
                        communication_id = com_obj['communication_id'], 
                        _external = True
                    )
                )
            elif (view.get('email_template') or view.get('html_string') or view.get('html_template')) and not view.data.get('_skip_personalized_url'):
                sms_text += ': Please visit {} for more details'.format(
                        url_for('i2ce_route.get_view',channel='s',view_id = view.id, communication_id = com_obj['communication_id'], _external = True)
                )
            provider = view.get('provider') or self.enterprise.get('sms_provider', '__default__')
            credentials = self.enterprise.get('sms_provider_credentials') or {}
            credentials.update(view.get('credentials', view.data.get('_credentials', {})))
            args1 = [
                recipient,
                sms_text,
                credentials.get('sms_gateway_number',  msgsender.TEXTLOCAL_GATEWAY_SENDER),
                view.get('view_type'),
            ]
            kwargs1 = {
                'view_id': view.id,
                'enterprise_id': self.enterprise_id,
                'provider': provider,
                'hook': communication_hook,
                'credentials': credentials,
                'communication_id': com_obj['communication_id'],
                'test': self.test,
            }
            msgsender.start_sms.apply_async(
                args = args1,
                kwargs = kwargs1,
                countdown = countdown,
                queue = 'communication'
            )
            #msgsender.start_sms(*args1, **kwargs1)
    
    def send_push(self, view, sender, receiver, countdown):
        logger.info("sending push notification to receiver: %s", receiver)
        for recipient in receiver['push_tokens']:
            com_obj = communication_hook(view.id, 'push_notification', 'created', self.enterprise_id, recipient = recipient)
            url = None
            if view.get('email_template') or view.get('html_string') or view.get('html_template'):
                url = url_for('i2ce_route.get_view',channel='p',view_id = view.id, communication_id = com_obj['communication_id'], _external = True)
            args=[
                recipient,
                view.get('title') or 'Notification from {}'.format(self.enterprise.get('enterprise_name', self.enterprise_id)),
                view.get('message') or view.get('title') or 'Message from {}'.format(self.enterprise.get('enterprise_name', self.enterprise_id))
            ]
            credentials = self.enterprise.get('app_data', {})
            cr = view.get('credentials', view.data.get('_credentials', {}))
            if isinstance(cr, (str, unicode)):
                cr = self.enterprise.get('social_media_credentials', {}).get(cr)
            if isinstance(cr, dict):
                credentials.update(cr)
            kwargs = {
                'provider': view.get('provider') or view.get('data', {}).get('push_provider') or self.enterprise.get('push_provider', 'OneSignalMessaging'),
                'credentials': credentials,
                'sender_name': sender.get('phone_number') or sender.get('name'),
                'url': url,
                'logo': sender.get('logo'),
                'view_type': view.get('view_type'),
                'enterprise_id': self.enterprise_id,
                'server': os.environ.get('SERVER_NAME'),
                'communication_id': com_obj['communication_id'],
                'data': view.get('data',{}).get('notification_data', {}) or {},
                'view_id': view.id,
                'hook': communication_hook
            }
            if countdown:
                notification_sender.start_notification_task.apply_async(
                    args = args,
                    kwargs = kwargs,
                    countdown = countdown,
                    queue = 'communication'
                )
            else:
                return notification_sender.start_notification_task(*args, **kwargs)

    def send(self, **kwargs):
        """
        receiver can be an object like this 
        {
                'emails': <email>,
                'phone_numbers': <number>,
                'push_tokens': <token>,
        }
        or can be the id of the receiver_model + receiver_ids defined in init
        if receiver model is not defined and id is provided, raise Error

        sender can be an object like this
        {
                'email': <email>,
                'phone_number': <number>,
                'name': <name>,
                'logo': <logo>
        } 
        if not provided, it will be taken from the enterprise object.
        other paramters are listed 
        view_type = 'transaction', 
        title = None, message = None, 
        email_template=None, html_string = None,  html_template = None, csv_template = None, push_template = None, pdf = False, 
        sender = None, receiver = None, 
        view_id = None,
        channels -- None or list of required channels
        delay -- Delay in seconds for sending out the mail
        """
        # If view_id is there, then get the view
        view = None if kwargs.get('view_id') is None else View.init_view(kwargs.pop('view_id'))
        receiver = kwargs.get('receiver') or kwargs.get('recipient') or {}
        logger.info("Receiver is 1:: %s", receiver)
        self.files = kwargs.get('files') or []

        # Set up defaults for the receiver information, default to empty list, but also split comma separated values
        for c in ['emails', 'phone_numbers', 'push_tokens']:
            h = receiver.get(c)
            if isinstance(h, (str, unicode)):
                receiver[c] = h.split(',')
            elif not h:
                receiver[c] = []
            else:
                receiver[c] = hp.make_list(h)
        
        logger.info("Receiver is 2:: %s", receiver)
        # If receiver ids are provided, then we need to load the specific models
        receiver_ids = hp.make_single(
            filter(
                lambda x: x is not None, 
                [kwargs.get('receiver_filter'), kwargs.get('receiver_ids'), kwargs.get('receiver_id')]
            ),  
            force = True, default = None
        )
        if receiver_ids is not None:
            designated_receiver_obj = None
            receiver_model = kwargs.get('receiver_model') or self.receiver_model
            if not receiver_model:
                raise CommunicationSendError("Receiver model not set. Cannot pick from receiver_ids")
            if not isinstance(receiver_model, Model):
                receiver_model = Model(receiver_model, self.enterprise_id, self.role, user_id = self.user_id)
            if not isinstance(receiver_ids, dict):
                receiver_ids = receiver_model.ids_to_dict(receiver_ids)
            ri = receiver_ids.copy()
            ri['_filter_attributes'] = receiver_model.ids
            def get_attrs(te, appended):
                # gets the communication attribute from the receiver obj
                if te and te != '__NULL__' and te not in appended: 
                    if isinstance(te, list):
                        for ta in te:
                            if ta not in appended:
                                appended.append(ta)
                    else:
                        appended.append(te)
            for a, l in [('email_attribute', 'emails'), ('sms_attribute', 'phone_numbers'), ('token_attribute', 'push_tokens')]:
                if getattr(receiver_model, l):
                    for l1 in hp.make_list(kwargs.get(a) or getattr(receiver_model, l)):
                        rn = ri.copy()
                        rn['{}~'.format(l1)] = None
                        receiver_objs = receiver_model.filter(**rn)
                        for receiver_obj in receiver_objs:
                            receiver_obj = receiver_model.get(receiver_model.dict_to_ids(receiver_obj))
                            if not designated_receiver_obj:
                                designated_receiver_obj = receiver_obj
                            get_attrs(receiver_obj.get(l1), receiver[l])
        # If view is there, then the default receiver can be got from the view... useful when you want to resend stuff
        logger.info("Receiver is :: %s", receiver)
        if not any(receiver.values()) and view:
            receiver = view.get_receiver()

        logger.info("Receiver is :: %s", receiver)
        # NEW: We can create a view even if there is no-one to send to
        # if not any(receiver.values()):
        #    raise CommunicationSendError("Either receiver obj, receiver_ids and receiver model or view id should be provided.")
        # Set the default sender
        sender = kwargs.get('sender', {} if view is None else view.get_data('sender', {}))
        sender['email'] = sender.get('email', self.enterprise.get('email'))
        if not '@' in sender.get('name', ''):
            sender['name'] = '{name} <{email}>'.format(name = sender.get('name', self.enterprise.get('name')), email = sender['email'])
        sender['phone_number'] = sender.get("phone_number", self.enterprise.get('phone_number')),
        sender['logo'] = sender.get('logo', self.enterprise.get("enterprise_logo"))
        # get channels and delay
        channels = kwargs.pop('channels', kwargs.pop("channel", None))
        delay = kwargs.pop('delay', None)
        # Now create a new view
        view_dict = view.get_view() if view else {}
        for k, v in View.get_view_attributes().iteritems():
            view_dict[k] = kwargs.pop(k, view_dict.get(k, v))
        if not view_dict.get('receiver_model') and self.receiver_model:
            view_dict['receiver_model'] = self.receiver_model.name
        if view_dict.get('receiver_ids'):
            view_dict['receiver_ids'] = hp.make_list(view_dict['receiver_ids'])
        view_dict['receiver'] = receiver
        view_dict['sender'] = sender
        if view_dict.get('data') is None:
            view_dict['data'] = {}
        view_dict['data'].update(kwargs)
        if receiver_ids is not None:
            view_dict['data'].update({receiver_model.name: designated_receiver_obj})
        if not view:
            view_dict.pop('view_id', None)
            view_dict.pop('enterprise_id', None)
            view = View(enterprise_id = self.enterprise_id, role = self.role, user_id = self.user_id, **view_dict)
            view.create()
        else:
            view.update()
        view.run_actions()
        for channel in self.get_prefered_channels(receiver, channels):
            with app.app_context():
                countdown = self.get_prefered_delay(receiver, channel, delay)
                if channel in ['email', 'mail']:
                    rt = self.send_mail(view, sender, receiver, countdown)
                if channel == "sms":
                    rt = self.send_sms(view, sender, receiver, countdown)
                if channel == "push_notification":
                    rt = self.send_push(view, sender, receiver, countdown)
        if kwargs.get('_return_send_data') and rt is not None:
            return rt
        return view.id


if __name__ == '__main__':
    a = Flask(__name__)
    with a.app_context():
        c = Communication(
            'supermarket_appointment', 'admin'
        )
        c.send(
                receiver = {'push_tokens': '9980838165'},
                channels =  ["push_notification"],
                data = {
                    'push_provider': "GupshupWhatsAppSender"
                },
                message =  "Testing GupShup Sender"
        )
