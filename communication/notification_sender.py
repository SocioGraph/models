#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, os
from os.path import exists as ispath, dirname, join as joinpath, abspath, sep as dirsep,isfile
d = dirname(dirname(abspath(__file__)))
if d not in sys.path:
    sys.path.append(d)
d = joinpath(dirname(dirname(abspath(__file__))), 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
from celery import Celery
from xml.etree import ElementTree
import requests
import pprint
import logging
from celery import current_app
import dill
import subprocess
from models import helpers as hp, model as base_model, interactions
config = {}
config['CELERY_BROKER_URL'] = 'redis://{}/{}'.format(os.environ.get('REDIS_URL', 'localhost:6379'), os.environ.get('REDIS_DB_NUMBER',0))
config['CELERY_TASK_SERIALIZER'] = 'pickle'
config['CELERY_ACCEPT_CONTENT'] = ['pickle']
celery = Celery(__name__, broker=config['CELERY_BROKER_URL'])
celery.conf.update(config)


class SenderNotSetError(Exception):
    pass


DEBUG_LEVEL = logging.DEBUG
MAX_WAIT = 3600 * 24

logger = hp.get_logger(__name__, DEBUG_LEVEL)


class NotificationSender(object):
    def __init__(
            self,
            sender=None,
            hook=None,
            receive_hook = None,
            request_hook=None,
        ):
        self.sent_hook = hook
        self.receive_hook = receive_hook or hook
        self.request_hook = request_hook
        self.sender = sender

    def _check_to_sender(self, to, sender=None):
        # logger.debug("In Base sendMsg To = %s", to)

        if not isinstance(to, (list, tuple)):
            to = [to]
        sender = sender or self.sender
        return (to, sender)

    def _translate_status(self, status):
        return status

    def sendMsg(
            self,
            to,
            notification_title,
            notification_description,
            enterprise_id=None,
            sender=None,
            communication_id=None,
            **kwargs
    ):
        pass

    def checkSentReport(
            self,
            no,
            ssid,
            enterprise_id,
            communication_id=None,
            previous_count=60,
    ):
        pass

    def process_hook(self,event=None,payload=None,**params):
        pass


class OneSignalMessaging(NotificationSender):
    # 1. All ids required
    # 2. data to be sent
    # one_signal_app_id = "cbfdcd8f-c810-410b-9e1b-be28572d19fc"
    # one_signal_basic_web_authorization = "NjRlODVhY2MtZmVjYS00NDgwLWJhNTAtY2JkZGQ0N2MwMDEx"

    onesignal_url = 'https://onesignal.com/api/v1/notifications'
    credentials = {
        "one_signal_app_id": 'c2ae342a-ac06-4592-99e1-44648a089416',
        "one_signal_basic_web_authorization": 'ODlhYmQ4ZDktY2U3NC00ZjdhLTkyNjQtYjhlNGM3MTM3ZDJl',
    }

    def sendMsg(
            self,
            to,
            notification_title,
            notification_description,
            enterprise_id=None,
            sender=None,
            communication_id=None,
            credentials=None,
            **kwargs
    ):
        if not credentials: credentials = {}

        title = {'en': notification_title}
        description = {'en': notification_description}
        header = {'Content-Type': 'application/json; charset=utf-8',
                  'Authorization': 'Basic '+ credentials.get('one_signal_basic_web_authorization', self.credentials.get('one_signal_basic_web_authorization'))}
        payload = {
            'app_id': credentials.get('one_signal_app_id') or self.credentials.get('one_signal_app_id'),
            'include_player_ids': to,
            'headings': title,
            'contents': description,
            'url': kwargs.get('url', 'https://www.iamdave.ai'),
            'chrome_web_icon': kwargs.get('logo', 'https://d3u7t62cos37nz.cloudfront.net/static/img/images/logo.png'),
            'safari_icon_32_32': kwargs.get('logo', 'https://d3u7t62cos37nz.cloudfront.net/static/img/images/logo.png'),
            'chrome_web_origin': kwargs.get('server_name', 'https://api.iamdave.ai'),
            'safari_site_origin': kwargs.get('server_name', 'https://api.iamdave.ai'),
            "small_icon": kwargs.get('logo','https://d3u7t62cos37nz.cloudfront.net/static/img/images/logo.png') ,
            "data" : kwargs.get("data",{})
        }


        logger.debug(header)
        logger.debug(payload)
        req = requests.post(self.onesignal_url, headers=header,
                            json=hp.json.loads(hp.json.dumps(payload, primitives = True)))
        logger.info("Response from One Signal: %s", req.text)
        return to


class GupshupWhatsAppSender(NotificationSender):
    gupshup_url = 'https://api.gupshup.io/sm/api/v1/msg'
    credentials = {
        "gupshup_api_key": os.environ.get("GUPSHUP_API_KEY", "3c64f675690747e1c76b2e1838526644"),
        "gupshup_app_name": os.environ.get("GUPSHUP_APP_NAME", "DaveAI"),
        "gupshup_sender": os.environ.get("GUPSHUP_SENDER", "917834811114")
    }
    event_map = {
        "enqueued": "enqueued",
        "failed": "failed",
        "sent": "enqueued",
        "delivered": "received",
        "read": "viewed",
        "sandbox-start": "test",
        "opted-in": "subscribed",
        "opted-out": "unsubscribed",
        "text": "request",
        "image": "request",
        "audio": "request",
        "video": "request",
        "file": "request",
        "location": "request",
        "contact": "request"
    }
    failure_map = {
        "1001": "Last Mapped Bot Details And Sender Details Mismatch",
        "1002": "Number Does Not Exists On WhatsApp",
        "1003": "Unable To Send Message | Check your wallet balance",
        "1004": "Message sending failed as user is inactive for session message and template messaging is disabled",
        "1005": "Message sending failed as user is inactive for session message and template did not match",
        "1006": "Message sending failed as user is inactive for session message and not opted in for template message",
        "1007": "Message sending failed as user is inactive for session message, not opted in for template message and template did not match",
        "1008": "User is not Opted in and Inactive"
    }
    default_response = "Hello user, Do you agree to receive notifications from this business? https://signupforservices.com/whatsapp/optin/?bId=a03e81e4-bded-4f96-ae37-f3b9f3fce51e&bName=testiamdave&s=URL&lang=en_US"

    def __init__(self, *args, **kwargs):
        super(GupshupWhatsAppSender, self).__init__(*args, **kwargs)
        self.enterprise_model = base_model.Model('enterprise', 'core')

    def _format_mobile_number(self, no, return_null = False):
        no = str(no)
        if return_null and not all(_.isdigit() for _ in no):
            return None
        if len(no) > 10:
            return no
        return "91" + no

    def _de_format_mobile_number(self, no):
        no = str(no)
        if len(no) > 10:
            return no[-10:]
        return no

    def sendMsg(
            self,
            to,
            notification_title = None,
            notification_description = None,
            enterprise_id=None,
            sender=None,
            communication_id=None,
            credentials=None,
            **kwargs
        ):
        if not credentials: credentials = {}
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            "apikey": credentials.get('gupshup_api_key', self.credentials.get('gupshup_api_key'))
        }
        payload = {
            "channel": "whatsapp",
            "source": self._format_mobile_number(sender or self.sender, return_null = True) or credentials.get('gupshup_sender', self.credentials.get('gupshup_sender')),
            "destination": self._format_mobile_number(to),
            "src.name": credentials.get('gupshup_app_name', self.credentials.get('gupshup_app_name')),
            'message': notification_description or notification_title
        }
        logger.debug("Headers for GupshupWhatsAppSender: %s", headers)
        logger.info("Payload for GupshupWhatsAppSender: %s", payload)
        res = requests.post(
            self.gupshup_url, headers=headers,
            data = payload
        )
        logger.info("Response from Gupshup: %s", res.text)
        if hasattr(self.sent_hook, '__call__'):
            if res.status_code == 200:
                payload = res.json()
                logger.info("Sending hook for event 'sent' for communication_id %s: %s", communication_id, payload.get('messageId'))
                self.sent_hook(kwargs.get('view_id'), 'push_notification', 'sent', enterprise_id, reason = payload.get("messageId"), communication_id = communication_id)
            else:
                self.sent_hook(kwargs.get('view_id'), 'push_notification', 'failed', enterprise_id, reason = res.text, communication_id = communication_id)
        return to

    def process_hook(self, event=None, payload=None, **kwargs):
        event = self.event_map.get(payload.get('payload', {}).get('type'))
        message_id = payload.get('payload', {}).get('gsId') or payload.get('payload', {}).get('id')
        app_name = payload.get('app')
        if not app_name:
            logger.error("No app name found in GupshupWhatsAppSender hook: %s", payload)
        e = hp.make_single(self.enterprise_model.list(whatsapp_app_name = app_name, _page_size = 1, _as_option = True), force = True, default = {})
        enterprise_id = e.get('enterprise_id')
        if not enterprise_id:
            logger.error(
                "Could not location enterprise_id either from message id: %s or app_name: %s ... Payload: %s", 
                message_id, app_name, payload
            ) 
            return "Unknown Enterprise"
        com_model = base_model.Model('communication', enterprise_id)
        c = hp.make_single(com_model.list(reason = message_id, _page_size = 1, _as_option = True), force = True, default = {})
        if not c and event != "request":
            logger.error("No communication with message id: %s... Payload: %s", message_id, payload)
            return "Unknown event {}".format(event)
        logger.info("Communication Obj: %s", c)
        args = [c.get('view_id'), "push_notification", event, enterprise_id]
        kwargs = {
            "communication_id": c.get('communication_id')
        }
        if event == "request":
            sender = payload.get('payload', {}).get('source') or payload.get('sender', {}).get('phone', {})
            if not sender:
                logger.error("No sender in GupshupWhatsAppSender request: %s", payload)
                return "No sender"
            text = payload.get('payload', {}).get('payload', {}).get('text')
            if not text:
                logger.warning("No text in GupshupWhatsAppSender request from sender: %s from EnterpiseId: %s", sender, enterprise_id)
                return "No text"
            kwargs = self.process_request_hook(enterprise_id, e, sender, text, payload)
            if hasattr(self.request_hook,'__call__'):
                call_event_hook=self.request_hook(
                    *args, **kwargs
                )
                return None
        elif event == "subscribed":
            sender = payload.get('payload', {}).get('phone') or payload.get('payload', {}).get('source') or payload.get('sender', {}).get('phone', {})
            if not sender:
                logger.error("No sender in GupshupWhatsAppSender request: %s", payload)
                return "No sender"
            self.process_subscribe_hook(enterprise_id, e, self._de_format_mobile_number(sender))
            return "ok"
        elif event in ["test", "enqueued", "unsubscribed"]:
            logger.info("Got Hook for communication_id: %s, event: %s", c.get('communication_id'), event)
            return "ok"
        elif event == "failed":
            kwargs["reason"] = self.failure_map(payload.get('payload', {}).get('payload', {}).get('code'), "Failed due to unknown reasons")
        elif event in ["received", "viewed"]:
            kwargs["reason"] = c.get('reason')
        else:
            raise Exception("Status {} is unknown for GupshupWhatsAppSender hook".format(event))
        if hasattr(self.receive_hook,'__call__'):
            call_event_hook=self.receive_hook(
                *args, **kwargs
            )
        return "Hold on, I'll answer that"

    def process_request_hook(self, enterprise_id, enterprise, sender, text, payload):
        response = {
            "pre_actions": [],
            "recipient": {"push_tokens": sender},
            "reason": "{placeholder}",
            "data": {
                "enterprise_obj": enterprise,
                "push_provider": "GupshupWhatsAppSender"
            }
        }
        cm = base_model.Model(enterprise.get('customer_model_name'), enterprise_id)
        if not cm.mobile_numbers:
            return response
        conversation_id = enterprise.get('app_data', {}).get('gupshup_conversation_id')
        if not conversation_id:
            return response
        customer_obj = hp.make_single(
            cm.list(
                _page_number = 1, _as_option = True, 
                **{cm.mobile_numbers[0]: self._de_format_mobile_number(sender)}
            ), 
            force = True
        )
        if not customer_obj:
            response['data']['reason'] = enterprise.get('app_data', {}).get('gupshup_default_response') or self.default_response
            return response
        response['pre_actions'].append({
            "_action": "converse",
            "_conversation_id": conversation_id,
            "_customer_id": customer_obj.get(cm.ids[0]),
            "customer_response": text,
            "system_response": "sr_init"
        })
        return response
        
        
    def process_subscribe_hook(self, enterprise_id, enterprise, sender):
        cm = base_model.Model(enterprise.get('customer_model_name'), enterprise_id)
        if not cm.mobile_numbers:
            return response
        return cm.post({
            cm.mobile_numbers[0]: sender,
        })
        

class GoogleMyBusinessSender(NotificationSender):
    gmb_url = 'https://businessmessages.googleapis.com/v1/conversations/{}/messages'
    credentials = {
        "service_json_path": os.environ.get('GOOGLE_SERVICE_ACCOUNT_PATH', joinpath(os.environ.get('HOME'), '.google_service_account.json')),
    }


    def _convert_conversation_data(self, data):
        data = data or {}
        if not data.get('state_options'):
            return {}
        if isinstance(data['state_options'], list):
            if isinstance(data['state_options'][0], (str, unicode)):
                itr = [(o, hp.replace('_', ' ').title()) for o in data['state_options']]
            if isinstance(data['state_options'][0], list):
                itr = [(o[0], o[1]) for o in data['state_options']]
            if isinstance(data['state_options'][0], dict):
                itr = data['state_options'].items()
        elif isinstance(data['state_options'], dict):
            itr = data['state_options'].items()
        else:
            return {}
        rdata = {
            'suggestions': []
        }
        for k, v in itr:
            rdata['suggestions'].append({
                'reply': {
                    'text': v,
                    'postbackData': hp.json.dumps({'customer_state': k, 'customer_response': v})
                },
            })
        return rdata

    def sendMsg(
            self,
            to,
            notification_title = None,
            notification_description = None,
            enterprise_id=None,
            sender_name=None,
            communication_id=None,
            credentials=None,
            **kwargs
        ):
        credentials = credentials or {}
        for k, v in self.credentials.iteritems():
            if k not in credentials:
                credentials[k] = v
        headers = {
            'Content-Type': 'application/json',
            'Authorization': subprocess.check_output([
                'oauth2l', 'header', '--json', credentials.get('service_json_path'), 'businessmessages'
            ]).split(":")[-1].strip()
        }
        payload = {
            'messageId': kwargs.get('data', {}).get('response_id') or communication_id,
            'text': notification_description,
            'representative': {
                'avatarImage': credentials.get('avatar_image') or kwargs.get('logo'),
                'displayName': hp.make_single(credentials.get('avatar_name') or sender_name , force = True),
                'representativeType': 'BOT'
            }
       }
        payload.update(self._convert_conversation_data(kwargs.get('data', {})))
        logger.info("Headers for GoogleMyBusinessSender: %s", headers)
        logger.info("Payload for GoogleMyBusinessSender: %s", hp.json.dumps(payload, indent = 4))
        for t in hp.make_list(to):
            url = self.gmb_url.format(t)
            logger.info("URL for GoogleMyBusinessSender: %s", url)
            try:
                res = requests.post(
                    url, headers=headers,
                    json = payload
                )
                logger.info("Response from GMB: %s", res.text)
                if hasattr(self.sent_hook, '__call__'):
                    if res.status_code == 200:
                        payload = res.json()
                        logger.info("Sending hook for event 'sent' for communication_id %s: %s", communication_id, payload.get('messageId'))
                        self.sent_hook(kwargs.get('view_id'), 'push_notification', 'sent', enterprise_id, reason = payload.get("messageId"), communication_id = communication_id)
                    else:
                        self.sent_hook(kwargs.get('view_id'), 'push_notification', 'failed', enterprise_id, reason = res.text, communication_id = communication_id)
            except Exception as e:
                hp.print_error(e)
                if hasattr(self.sent_hook, '__call__'):
                    self.sent_hook(kwargs.get('view_id'), 'push_notification', 'failed', enterprise_id, reason = str(e), communication_id = communication_id)
        return to

class WhatsAppSender(NotificationSender):
    url = 'https://graph.facebook.com/v16.0/{account_id}/messages'
    credentials = {
        "account_id": os.environ.get('WHATSAPP_ACCOUNT_ID'),
        "authorization_header": os.environ.get('WHATSAPP_AUTHORIZATION')
    }

    def _format_mobile_number(self,no):
        no = str(no)
        if len(no) <= 10:
            no = "91"+no
        return no

    def _convert_conversation_data(self, data):
        data = data or {}
        if not data.get('state_options'):
            return {}
        if isinstance(data['state_options'], list):
            if isinstance(data['state_options'][0], (str, unicode)):
                itr = [(o, hp.replace('_', ' ').title()) for o in data['state_options']]
            if isinstance(data['state_options'][0], list):
                itr = [(o[0], o[1]) for o in data['state_options']]
            if isinstance(data['state_options'][0], dict):
                itr = data['state_options'].items()
        elif isinstance(data['state_options'], dict):
            itr = data['state_options'].items()
        else:
            return {}
        rdata = {
            'suggestions': []
        }
        for k, v in itr:
            rdata['suggestions'].append({
                'reply': {
                    'text': v,
                    'postbackData': hp.json.dumps({'customer_state': k, 'customer_response': v})
                },
            })
        return rdata

    def sendMsg(
            self,
            to,
            notification_title = None,
            notification_description = None,
            enterprise_id=None,
            sender_name=None,
            communication_id=None,
            credentials=None,
            **kwargs
        ):
        credentials = credentials or {}
        for k, v in self.credentials.iteritems():
            if k not in credentials:
                credentials[k] = v
        headers = {
            'Content-Type': 'application/json',
            'Authorization': credentials.get('authorization_header')
        }
        data = kwargs.get('data',{})
        #payload.update(self._convert_conversation_data(kwargs.get('data', {})))
        for t in hp.make_list(to):
            payload = { 
                "messaging_product": "whatsapp", 
                "to": self._format_mobile_number(t), 
                "type": "text", 
                "text": { 
                    "body": data.get('message',"hello_world")
                } 
            }
            logger.info("Headers for WhatsAppSender: %s", headers)
            logger.info("Payload for WhatsAppSender: %s", hp.json.dumps(payload, indent = 4))
            url = self.url.format(account_id = credentials.get('account_id'))
            logger.info("URL for WhatsAppSender: %s", url)
            try:
                res = requests.post(
                    url, headers=headers,
                    json = payload
                )
                logger.info("Response from WhatsApp: %s", res.text)
                if hasattr(self.sent_hook, '__call__'):
                    if res.status_code == 200:
                        payload = res.json()
                        logger.info("Sending hook for event 'sent' for communication_id %s: %s", communication_id, payload.get('messageId'))
                        self.sent_hook(kwargs.get('view_id'), 'push_notification', 'sent', enterprise_id, reason = payload.get("messageId"), communication_id = communication_id)
                    else:
                        self.sent_hook(kwargs.get('view_id'), 'push_notification', 'failed', enterprise_id, reason = res.text, communication_id = communication_id)
            except Exception as e:
                hp.print_error(e)
                if hasattr(self.sent_hook, '__call__'):
                    self.sent_hook(kwargs.get('view_id'), 'push_notification', 'failed', enterprise_id, reason = str(e), communication_id = communication_id)
        return to
        

class SprinklrWhatsAppSender(NotificationSender):
    
    def send_whatsapp_message_to_autoexpo(self, enterprise_id=None, **kwargs):
        
        data = kwargs.get('data', {})
        mobile_number = str(data.get('mobile_number', ''))
        if not mobile_number and self.to_mobile_number:
            mobile_number = str(hp.make_single(self.to_mobile_number, force = True, default = ''))

        image_link =data.get('image_link')
        if not mobile_number or not image_link:
            return None
        customer_name = data.get('user_name', 'User')
        creds = {} #kwargs.get('credentials',{})
        if not creds:
            em = base_model.Model('enterprise', 'core')
            eo = em.get(enterprise_id)
            creds = eo.get('social_media_credentials', {})

        logger.info('Social media credentials :: %s', creds)
        journey_id = creds.get('sprinklr_journey_id', '63ad354304f2e3577c478005')
        payload = {
            "journeyId": journey_id,
            "contextParams": {
            "customer_name":customer_name,
            "IGNORE_DEDUP":True
            },
            "unifiedProfile": {
            "contact": {
            "firstName": customer_name.split(' ')[0]
            },
            "profiles": [
                {
                "channelType": "WHATSAPP_BUSINESS",
                "channelId": self.check_phone_number(mobile_number),
                "name": customer_name
                }
            ],
            "createdTime": 1655650673
            }
        }

        logger.info('Created json body to send Whatsapp Message %s', hp.json.dumps(payload))
        api_key = creds.get('sprinklr_api_key', 'CCEx6Nu3WbC62rL8IqVSZH+AM9j4oP9E6SgxwJ+D7sYzZTgyNDQxNzk1ODgyZTg3ODljZjQwNDNmMjNiYTZlNg==')
        if 'Bearer'  not in api_key:
            api_key = 'Bearer ' + api_key

        headers = {
            'Content-Type':'application/json',
            'key':creds.get('sprinklr_client_id','9kheqth5j9dh4dqdk2ykgdq7'),
            'Authorization': api_key 
        
        }
       
        logger.info('Headers for the sprinklr request :: %s', headers)
        url  = data.get('url','https://api2.sprinklr.com/prod2/api/v2/marketing-journey/triggerWithNewProfile')
        logger.info('Request url: %s', url)
        try:
            r = requests.post(url , headers= headers, data = hp.json.dumps(payload))
            logger.info('Response after posting to %s :: %s', url, r.text)
            if r.status_code < 400:
                return r 
            hp.print_error('Failed to send WhatsappMessage to %s for enteprise %s', mobile_number, enterprise_id)
        except Exception as e:
            hp.print_error('Failed to send WhatsappMessage to %s for enteprise %s :: %s', mobile_number, enterprise_id, e)
        return r
    
    def payload(self, journey_id, customer_name, mobile_number, additional_keys = None ):
        p = {
            "journeyId": journey_id,
            "contextParams": {
            "customer_name":customer_name,
            "IGNORE_DEDUP":True
            },
            "unifiedProfile": {
            "contact": {
            "firstName": customer_name.split(' ')[0]
            },
            "profiles": [
                {
                "channelType": "WHATSAPP_BUSINESS",
                "channelId": self.check_phone_number(mobile_number),
                "name": customer_name
                }
            ],
            "createdTime": 1655650673
            }
        }
        
        p['contextParams'].update(additional_keys or {})
        logger.info('Created json body to send Whatsapp Message %s', hp.json.dumps(p, indent=4))

        return p
    
    def headers(self, enterprise_id, credentials = None, key = 'sprinklr_template_credentials'):
        credentials = credentials or {}
        em = base_model.Model('enterprise', 'core')
        eo = em.get(enterprise_id)
        spr_creds = eo.get('social_media_credentials', {}).get(key,{})
        credentials.update(spr_creds)
        key = credentials.get('sprinklr_client_id','9kheqth5j9dh4dqdk2ykgdq7') 
        sprinklr_api_key = credentials.get('sprinklr_api_key','') 
        
        if 'Bearer'  not in sprinklr_api_key:
            sprinklr_api_key = 'Bearer ' + sprinklr_api_key

        h = {
            'Content-Type':'application/json',
            'key':key,
            'Authorization':sprinklr_api_key  
        }
        logger.info('Headers for sprinklr template :: %s', hp.json.dumps(h, indent=4))

        return h

    def sprinklr_whatsapp_template_api(self, enterprise_id=None, **kwargs):
        
        data = kwargs.get('data', {})
        mobile_number = str(data.get('mobile_number', ''))
        if not mobile_number and self.to_mobile_number:
            mobile_number = str(hp.make_single(self.to_mobile_number, force = True, default = ''))

        if not mobile_number:
            return None
        
        payload = self.payload(data.get('journy_id'), data.get('user_name','User'), mobile_number, data.get('additional_keys',{}))
        
        headers = self.headers(enterprise_id, kwargs.get('credentials', {}), key = data.get('sprinklr_credential_key', 'sprinklr_template_credentials'))
       
        url  = data.get('api_url','https://api2.sprinklr.com/prod2/api/v2/marketing-journey/triggerWithNewProfile')
        logger.info('Request url: %s', url)

        try:
            r = requests.post(url , headers= headers, data = hp.json.dumps(payload))
            logger.info('Response after posting to %s :: %s', url, r.text)
            if r.status_code < 400:
                return r 
            hp.print_error('Failed to send WhatsappMessage to %s for enteprise %s', mobile_number, enterprise_id)
        except Exception as e:
            hp.print_error('Failed to send WhatsappMessage to %s for enteprise %s :: %s', mobile_number, enterprise_id, e)
        return r

    def sendMsg(
        self,
        to,
        notification_title = None,
        notification_description = None,
        enterprise_id=None,
        sender_name=None,
        communication_id=None,
        credentials=None,
        **kwargs
    ):
        self.to_mobile_number = to
        kwargs.update({'credentials':credentials if credentials else kwargs.get('credentials', {})})

        res = self.sprinklr_whatsapp_template_api(enterprise_id = enterprise_id, **kwargs)
        try:
            logger.info("Response from Sprinklr WhatsApp: %s", res.text)
            if hasattr(self.sent_hook, '__call__'):
                
                if res and res.status_code == 200:
                    payload = res.json()
                    logger.info("Sending hook for event 'sent' for communication_id %s: %s", communication_id, hp.make_single(self.to_mobile_number, force=True))
                    self.sent_hook(kwargs.get('view_id'), 'push_notification', 'sent', enterprise_id, reason = hp.make_single(self.to_mobile_number, force=True), communication_id = communication_id)
                else:
                    self.sent_hook(kwargs.get('view_id'), 'push_notification', 'failed', enterprise_id, reason = res.text, communication_id = communication_id)
        except Exception as e:
            hp.print_error(e)
            if hasattr(self.sent_hook, '__call__'):
                self.sent_hook(kwargs.get('view_id'), 'push_notification', 'failed', enterprise_id, reason = str(e), communication_id = communication_id)
        return to
        
    def check_phone_number(self, phone):
        phone = str(phone)
        if len(phone)==12 and phone.startswith('91'):
            return phone
        if len(phone)==13 and phone.startswith('+91'):
            phone = phone.replace('+','')
            return phone
        if len(phone)==11 and phone.startswith('0'):
            phone = '91' + phone[1:]
            return phone
        if len(phone)==10:
            phone = '91' + phone
            return phone

class AirtelWhatsAppSender(NotificationSender):
    
    base_url = 'https://iqwhatsapp.airtel.in/gateway/airtel-xchange/basic/whatsapp-manager/v1'
    credentials = {
        "account_id": os.environ.get('AIRTEL_WHATSAPP_ACCOUNT_ID'),
        "authorization_header": os.environ.get('AIRTEL_WHATSAPP_AUTHORIZATION')
    }

    def _format_mobile_number(self,no):
        no = str(no)
        if len(no) <= 10:
            no = "91"+no
        return no

    def _convert_conversation_data(self, data):
        data = data or {}
        if not data.get('state_options'):
            return {}
        if isinstance(data['state_options'], list):
            if isinstance(data['state_options'][0], (str, unicode)):
                itr = [(o, hp.replace('_', ' ').title()) for o in data['state_options']]
            if isinstance(data['state_options'][0], list):
                itr = [(o[0], o[1]) for o in data['state_options']]
            if isinstance(data['state_options'][0], dict):
                itr = data['state_options'].items()
        elif isinstance(data['state_options'], dict):
            itr = data['state_options'].items()
        else:
            return {}
        rdata = {
            'suggestions': []
        }
        for k, v in itr:
            rdata['suggestions'].append({
                'reply': {
                    'text': v,
                    'postbackData': hp.json.dumps({'customer_state': k, 'customer_response': v})
                },
            })
        return rdata

    def sendMsg(
            self,
            to,
            notification_title = None,
            notification_description = None,
            enterprise_id=None,
            sender_name=None,
            communication_id=None,
            credentials=None,
            **kwargs
        ):
        credentials = credentials or {}
        for k, v in self.credentials.iteritems():
            if k not in credentials:
                credentials[k] = v
        headers = {
            'Content-Type': 'application/json',
            'Authorization': credentials.get('authorization_header')
        }
        data = kwargs.get('data',{})
        success_hook = data.pop('_success_hook', None)
        failure_hook = data.pop('_failure_hook', None)
        #payload.update(self._convert_conversation_data(kwargs.get('data', {})))
        for t in hp.make_list(to):
            _type = data.get("type","text")
            payload = {
                "from":self._format_mobile_number(credentials.get("phone_number_id")),
                "to": self._format_mobile_number(t)
            }
            if _type == "template":
                url = self.base_url + "/template/send" 
                payload["templateId"] = data.get("template_id","01h7f30aq7e7ba5a2rcmzckw7s")
                payload["message"]={
                    "variables" : data.get("variables",[]),
                    "payload":data.get("buttons",[])
                }
            else:    
                payload["sessionId"] = data.get("session_id","")
                if _type == "text":
                    url = self.base_url + "/session/send/text"
                    payload["message"] = {
                        "text" : data.get('message',"hello_world")
                    }
                if _type in ["image","document","audio","video"]:
                    url = self.base_url + "/session/send/media"
                    payload["mediaAttachment"] = {
                        "type": _type.upper(),
                        "url": data.get("media_url",""),
                        "fileName": data.get("filename",""),
                        "caption": data.get("caption","")
                    }
             
            logger.info("Headers for WhatsAppSender: %s", headers)
            logger.info("Payload for WhatsAppSender: %s", hp.json.dumps(payload, indent = 4))
            # url = self.url.format(account_id = credentials.get('account_id'))
            logger.info("URL for WhatsAppSender: %s", url)
            try:
                res = requests.post(
                    url, headers=headers,
                    json = payload
                )
                logger.info("Response from WhatsApp: %s", res.text)
                if res.status_code == 200:
                    payload = res.json()
                    logger.info("Sending hook for event 'sent' for communication_id %s: %s", communication_id, payload.get('messageRequestId'))
                    if hasattr(self.sent_hook, '__call__'):
                        self.sent_hook(kwargs.get('view_id'), 'push_notification', 'sent', enterprise_id, data = payload, communication_id = communication_id)
                    if callable(success_hook):
                        success_hook(payload)
                else:
                    if hasattr(self.sent_hook, '__call__'):
                        self.sent_hook(kwargs.get('view_id'), 'push_notification', 'failed', enterprise_id, reason = res.text, communication_id = communication_id)
                    if callable(failure_hook):
                        failure_hook(res.text)
            except Exception as e:
                hp.print_error(e)
                if hasattr(self.sent_hook, '__call__'):
                    self.sent_hook(kwargs.get('view_id'), 'push_notification', 'failed', enterprise_id, reason = str(e), communication_id = communication_id)
                if callable(failure_hook):
                    failure_hook(res.text)
        return to


PROVIDERS = {
    'GoogleMyBusinessSender': GoogleMyBusinessSender,
    'GupshupWhatsAppSender': GupshupWhatsAppSender,
    'OneSignalMessaging': OneSignalMessaging,
    'WhatsAppMessaging': WhatsAppSender,
    'SprinklrWhatsAppMessaging':SprinklrWhatsAppSender,
    'AirtelWhatsAppSender':AirtelWhatsAppSender,
    '__default__': OneSignalMessaging
    }

# celery task for notification
@celery.task(name='start_notification_task')
def start_notification_task(
        to_nos,
        notification_title,
        notification_description = None,
        sender_name=None,
        enterprise_id=None,
        communication_id=None,
        provider=None,
        hook=None,
        **kwargs
    ):
    notify_msg = PROVIDERS.get(provider, OneSignalMessaging)(hook = hook)
    notify_msg_result = notify_msg.sendMsg(
        to_nos,
        notification_title,
        notification_description,
        enterprise_id,
        sender_name,
        communication_id,
        **kwargs
    )
    return notify_msg_result


if __name__ == '__main__':
    #start_notification_task(
    #    ['6b51cd48-3886-487a-a6e7-d94a8729e83f'],
    #    'this is a python test with push token id sent by rinki',
    #    'dscription',
    #    logo="https://www.google.co.vi/images/branding/googleg/1x/googleg_standard_color_128dp.png",
    #    server_name="http://dashboard.iamdave.ai",
    #    url='http://www.google.com',
    #)
    #start_notification_task(
    #    ["eed1fa96-a83f-4a54-aae6-b9222248f8f5"],
    #    'test',
    #    'this is a python test with push token id sent by Ananth',
    #    provider = 'GoogleMyBusinessSender',
    #    communication_id = '6b51cd48-3886-487a-a6e7-d94a8729e83f'
    #)
    # start_notification_task(
    #     ["8621950003"],
    #     'test',
    #     'this is a test for service multiparty whatsapp bot',
    #     provider = 'WhatsAppMessaging',
    #     credentials = {
    #     "account_id":'102690369470112',
    #     "authorization_header" : 'Bearer EAAKJcSzd1XcBAMLX6FlEakfwppiZCd5rieiAFkGU7bQOZCY8SfaAtdvNvOn3cLOZAqJaIiQCBViKCscZCZCYoBTsyW5WATpYS5cteX3Tv6mrH792MxKQ5npH8MKh2nlk98Uym7oeSoMMdLZAbY9GK7ZBB54eX6KcOpAoG7DRZBHXsat1NZCbHwZBDOsvTZAshZB04PlBehMt1b2DZAaPyTKOCNDlgWGypnNqtgesZD'
    #     },
    #     **{
    #     'data':{
    #         "message":"This is the test messsage for multiparty"
    #     }}
    # )
    # start_notification_task(
    #     ["9348927558"],
    #     'test',
    #     'this is a python test with push token id sent by Ananth',
    #     provider = 'SprinklrWhatsAppMessaging',
    #     communication_id = '6b51cd48-3886-487a-a6e7-d94a8729e83f',
    #     **{
    #         "data":{ 
    #         "journy_id":"64a559f2c8a8e93d6e7e76f9",
    #         "user_name":"Nitesh Sahu",
    #         "additional_keys":{
    #         "registration":"MH03K234",
    #         "vehicle_name":"ALTO",
    #         "url":"https://maruti.co.in/"}
    #         }

    #     }
    # )
    start_notification_task(
        ["8621950003"],
        "test",
        provider = 'AirtelWhatsAppSender',
        credentials = {
        "phone_number_id":'918308309367',
        "authorization_header" : 'Basic ZGF2ZV9haTpJSjJQVjhebDVjODU='
        },
        **{
        'data':{
            "type":"template",
            "template_id" : "01h986cx0gtjyhn57d94v91mt2",
            "variables" : [
                "Test 01",
                "JH87HU78765",
                "Rohan Motors",
                "Service Advisor Jaipreet Singh - 8527493387",
                "Service Advisor Jaipreet Singh - 8527493387",
                "Service Advisor Jaipreet Singh - 8527493387",
                "Rohan Motors",
                "9999999999"
            ],
            "buttons": [
                "Start Chat"
            ]
        }}
    )



    
