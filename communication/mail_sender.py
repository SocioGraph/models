import sys,os
from os.path import exists as ispath, dirname, join as joinpath, abspath, sep as dirsep,isfile
d = dirname(dirname(abspath(__file__)))
if d not in sys.path:
    sys.path.append(d)
d = joinpath(dirname(dirname(abspath(__file__))), 'models', 'libraries')
if d not in sys.path:
    sys.path.insert(0, d)
import helpers as hp
import datetime
import time
import dateutils
import tzlocal
import requests
import pprint
import logging
from celery import Celery
from celery import current_app
import json
import hmac
import hashlib
from validate_email import validate_email
import dill
from headless_pdfkit import generate_pdf
from tempfile import NamedTemporaryFile
from mailer import Mailer, Message
import boto3
from botocore.exceptions import ClientError as AWSClientError
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication


config={}
config['CELERY_BROKER_URL'] = 'redis://{}/{}'.format(os.environ.get('REDIS_URL', 'localhost:6379'), os.environ.get('REDIS_DB_NUMBER',0))
config['CELERY_TASK_SERIALIZER']='pickle'
config['CELERY_ACCEPT_CONTENT'] = ['pickle']
celery = Celery(__name__, broker=config['CELERY_BROKER_URL'])
celery.conf.update(config)

class SenderNotSetError(Exception):
	pass
DEBUG_LEVEL=logging.DEBUG
MAX_WAIT = 3600*48

current_module = __import__(__name__)
def get_logger(name, level = None, output_file = None):
    logger = logging.getLogger(name)
    if not logger.handlers:
        if output_file is None:
            h = logging.StreamHandler(sys.stderr)
        else:
            h = logging.FileHandler(output_file)
        h.setFormatter(logging.Formatter("%(asctime)s: %(name)s: %(levelname)s:  %(message)s"))
        logger.addHandler(h)
        logger.setLevel(level or DEBUG_LEVEL)
    logger.propagate = False
    return logger
                             
logger = get_logger(__name__, DEBUG_LEVEL)

class Mailsender(object):
    def __init__(self, sender = None, hook = None, blacklist_hook = None, credentials = None, receive_hook = None, request_hook = None):

        self.sent_hook = hook
        self.receive_hook = receive_hook or hook
        self.blacklist_hook = blacklist_hook
        self.request_hook = request_hook
        self.sender = sender

    def _check_to_sender(self, to, sender = None):
        #logger.debug("In Base sendMsg To = %s", to)
        if not isinstance(to, (list, tuple)):
            to = [to]
        sender = sender or self.sender
        if not sender:
            raise SenderNotSetError("sender field is not set")
        return to, sender

    def sendMail(self, to, subject,enterprise_id,view_id,files=None,sender = None,**kwargs):
        pass
             
    def get_past_details(self,campaignName,campaign_id,**kwargs):
        pass

    def process_hook(self,event=None,payload=None,**params):
        pass

def do_validate_email(email):
    for e in email.split(','):
        e = e.strip()
        if not validate_email(e):
            return False
    return True

class MailgunSender(Mailsender):

    _key = "key-5029bb836fa6cc7b062b9abcca868301"
    _domain1 = "https://api.mailgun.net/v3/mg.sociographsolutions.in/messages"
    _domain2 = "https://api.mailgun.net/v3/mg.sociographsolutions.in/events"
    _default_html = "<html> Sample html </html>"
    _domain3="https://api.mailgun.net/v3/mg.sociographsolutions.in/campaigns"


    credentials = {'key': _key}

    def verified(self,token, timestamp, signature):
        hmac_digest = hmac.new(key=self._key,
                msg='{}{}'.format(timestamp, token),
                digestmod=hashlib.sha256).hexdigest()
        r = hmac.compare_digest(unicode(signature), unicode(hmac_digest))
        return r


    def sendMail(self, to, subject,enterprise_id,view_id,files=None, sender = None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        logger.debug("Sending mails to Ids to: %s", to)
        for mailTo in to:
            if not (isinstance(mailTo, (str, unicode)) and mailTo):
                logger.warning("Empty email for view_id %s", view_id)
                continue
            if kwargs.get('test') or os.environ.get('ENVIRONMENT', 'dev') not in ['production', 'staging']:
                logger.info("Original mail Id to send to: %s", mailTo)
                dMailTo = []
                for m in mailTo.split(','):
                    if os.environ.get('I2CE_EMAIL'):
                        c = os.environ.get("I2CE_EMAIL".split('@'))
                        m='{}+{}@{}'.format(c[0], m.replace('@','_').replace(".","_"), c[1])
                    else:
                        m='{}+{}@iamdave.ai'.format(os.environ.get('I2CE_USER', 'dinesh'), m.replace('@','_').replace(".","_"))
                    dMailTo.append(m)
                mailTo=','.join(dMailTo)
                logger.info("Sending debug mail to %s", mailTo)
            if not do_validate_email(mailTo):
                self.sent_hook(view_id,'email', 'failed', enterprise_id, reason = 'invalid email', communication_id = kwargs.get('communication_id'))
                continue
            response= requests.get("https://api.mailgun.net/v3/mg.sociographsolutions.in/bounces/"+mailTo,auth=("api", self._key))
            response1=response.json()
            if response1.get("error"):
                if hasattr(self.sent_hook, '__call__'):
                    self.sent_hook(
                        view_id,'email', 'failed', enterprise_id, reason = 'in bounced list', 
                        communication_id = kwargs.get('communication_id')
                    )
                continue
            msgid, status, reason = self._toMailgun(sender, mailTo, subject, enterprise_id, view_id, files=files, **kwargs)
            if hasattr(self.sent_hook, '__call__'):
                if status:
                    self.sent_hook(view_id,'email', 'sent',enterprise_id, communication_id = kwargs.get('communication_id'))
                else:
                    self.sent_hook(view_id,'email', 'failed', enterprise_id, reason = reason, communication_id = kwargs.get('communication_id'))
        return to



    def _toMailgun(self, sender, mailTo, subject, enterprise_id = None, view_id = None, files=None,**kwargs):
        data = {  
            "from": sender,
            "to": mailTo,
            "subject": subject,
            'text':kwargs.get('text',None),
            "v:custom_data" :json.dumps({
                 'view_id':(view_id or ''),
                 'enterprise_id':(enterprise_id or ''),
                 'recipient': (mailTo or ''),
                 'server': os.environ.get('SERVER_NAME'),
                 'communication_id': kwargs.get('communication_id')
             }),
            "html": kwargs.get('html', self._default_html)
        }
        if kwargs.get('cc'):
            data['cc'] = kwargs['cc']
        if kwargs.get('bcc'):
            data['bcc'] = kwargs['bcc']
        logger.debug("Files are : %s", files)
        request = requests.post(self._domain1,auth=("api", (kwargs.get('credentials', {}) or self.credentials).get('key') or self._key), 
            files=files,
            data=data
        )
        logger.info('Data: %s', data)
        logger.info('Status: %s', request.status_code)
        logger.info('Body: %s', request.text)
        if request.status_code != 200:
            return None, False, request.text
        rt= request.json()
        return rt.get('id'), True, rt.get('message')
    
    def process_hook(self, event=None, payload=None,**params):
        events={ 
            'opened':'viewed',
            'unsubscribed':'unsubscribed',
            'delivered':'received',
            'bounced':'failed',
            'dropped':'failed'
        }
        if event in events:
            if self.verified(payload.get('token'), payload.get('timestamp'), payload.get('signature')):
                custom = json.loads(payload.get('custom_data', '{}'))
                if custom.get('view_id') and custom.get('enterprise_id'):
                    if custom.get('server', 'localhost') == os.environ.get('SERVER_NAME'):
                        call_event_hook=self.receive_hook(
                            custom['view_id'], 'email', events[event], custom['enterprise_id'], 
                            communication_id = custom.get('communication_id')
                        )
                        return "ok"
                    else:
                        return "Not sent from this server" 
                return "Is it a test?"
            raise Exception("Did not get verified by mailgun api_key")
        raise Exception("Unknown event {}".format(event))


def make_pdf(html1, path):
    try:
        n = generate_pdf(html1, path)
    except:
        logger.error("Error in generating pdf")
        hp.print_error()
    return path

class SmtpSender(Mailsender):

    def __init__(self, *args, **kwargs):
        super(SmtpSender, self).__init__(*args, **kwargs)
        credentials = kwargs.get('credentials') or {}
        self.mailer = Mailer(
            host = credentials.get('host', 'smtp.mailgun.org'),
            port = credentials.get('port', 465),
            use_tls = credentials.get('use_tls', False),
            use_ssl = credentials.get('use_ssl', True),
            usr = credentials.get('username', 'postmaster@i2ce.in'),
            pwd = str(credentials.get('password', 'b0a03a36ea38ce9428f1d8f8faf87dfe'))
        )

    def sendMail(self, to, subject,enterprise_id,view_id,files=None, sender = None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        logger.debug("Sending mails to Ids to: %s", to)
        for mailTo in to:
            if not (isinstance(mailTo, (str, unicode)) and mailTo):
                logger.warning("Empty email for view_id %s", view_id)
                continue
            if kwargs.get('test') or os.environ.get('ENVIRONMENT', 'dev').strip() != 'production':
                logger.info("Original mail Id to send to: %s", mailTo)
                mailTo='{}+{}@i2ce.in'.format(os.environ.get('I2CE_USER', 'dinesh'), mailTo.replace('@','_').replace(".","_"))
                logger.info("Sending debug mail to %s", mailTo)
            if not do_validate_email(mailTo):
                if hasattr(self.sent_hook, '__call__'):
                    self.sent_hook(view_id,'email', 'failed', enterprise_id, reason = 'invalid email', communication_id = kwargs.get('communication_id'))
                continue
            msg = Message(
                From = sender,
                To = hp.make_list_from_csv(mailTo),
                CC = hp.make_list_from_csv(kwargs.get('cc')),
                BCC = hp.make_list_from_csv(kwargs.get('bcc')),
                charset = 'utf-8'
            )
            msg.Subject = subject
            if kwargs.get('html'):
                msg.Html = kwargs['html']
            if kwargs.get('text'):
                msg.Body = kwargs['text']
            if files:
                for f in hp.make_list(files):
                    msg.attach(f[1][0], content = f[1][1].read())
            try:
                self.mailer.send(msg)
            except Exception as e:
                hp.print_error()
                logger.error("Error in sending email: %s", e)
                if hasattr(self.sent_hook, '__call__'):
                    self.sent_hook(view_id,'email', 'failed', enterprise_id, reason = str(e), communication_id = kwargs.get('communication_id'))
            else:
                if hasattr(self.sent_hook, '__call__'):
                    self.sent_hook(view_id,'email', 'sent',enterprise_id, communication_id = kwargs.get('communication_id'))
        return to

class AwsSender(Mailsender):


    def __init__(self, *args, **kwargs):
        super(AwsSender, self).__init__(*args, **kwargs)
        self.credentials = {
            'region_name': "ap-south-1",
            'aws_access_key_id': None,
            'aws_secret_access_key': None,
            'endpoint_url': None
        }
        if isinstance(kwargs.get('credentials'), dict):
            self.credentials.update(kwargs.get('credentials'))
        for k, v in list(self.credentials.items()):
            if v is None:
                self.credentials.pop(k)
        self.client = boto3.client('ses',  **self.credentials)

    def sendMail(self, to, subject,enterprise_id,view_id,files=None, sender = None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        logger.debug("Sending mails to Ids to: %s", to)
        for mailTo in to:
            if not (isinstance(mailTo, (str, unicode)) and mailTo):
                logger.warning("Empty email for view_id %s", view_id)
                continue
            if kwargs.get('test') or os.environ.get('ENVIRONMENT', 'dev').strip() != 'production':
                logger.info("Original mail Id to send to: %s", mailTo)
                mailTo='{}+{}@iamdave.ai'.format(os.environ.get('I2CE_USER', 'dinesh'), mailTo.replace('@','_').replace(".","_"))
                logger.info("Sending debug mail to %s", mailTo)
            if not do_validate_email(mailTo):
                logger.warning("Email: %s is not an email address", mailTo)
                self.sent_hook(view_id,'email', 'failed', enterprise_id, reason = 'invalid email', communication_id = kwargs.get('communication_id'))
                continue
            dest = {
                'ToAddresses': [
                    mailTo
                ],
                'CcAddresses': hp.make_list_from_csv(kwargs.get('cc') or ''),
                'BccAddresses': hp.make_list_from_csv(kwargs.get('bcc') or '')                        
            }
            tags = []
            if enterprise_id:
                tags.append({
                    'Name': 'enterprise_id',
                    'Value': enterprise_id or ''
                })
            if view_id:
                tags.append({
                    'Name': 'view_id',
                    'Value': view_id or ''
                })
            if os.environ.get('SERVER_NAME'):
                tags.append({
                    'Name': 'server',
                    'Value': (os.environ.get('SERVER_NAME') or '').replace('.', '_')
                })
            if kwargs.get('communication_id'):
                tags.append({
                    'Name': 'communication_id',
                    'Value': kwargs.get('communication_id') or ''
                })
            try:
                if not kwargs.get('html') and not files:
                    response = self.client.send_email(
                            Source=sender,
                            Destination=dest,
                            Message={
                                'Subject': {
                                    'Data': subject,
                                    'Charset': 'utf-8'
                                    },
                                'Body': {
                                    'Text': {
                                        'Data': kwargs.get('text', ''),
                                        'Charset': 'utf-8'
                                    },
                                },
                            },
                            ReplyToAddresses = [
                                sender
                            ],
                            ReturnPath=sender,
                            Tags=tags
                    )
                else:
                    message = MIMEMultipart()
                    message['Subject'] = subject
                    message['From'] = sender
                    message['To'] = mailTo
                    if kwargs.get('cc'):
                        message['Cc'] = kwargs.get('cc')
                    if kwargs.get('bcc'):
                        message['Bcc'] = kwargs.get('bcc')
                    part = MIMEText(kwargs.get('html') or kwargs.get('text') or '', 'html')
                    message.attach(part)
                    if files:
                        for f in hp.make_list(files):
                            part = MIMEApplication(f[1][1].read())
                            part.add_header('Content-Disposition', 'attachment', filename=f[1][0])
                            message.attach(part)
                    recipient_list = hp.make_list(hp.make_list_from_csv(mailTo) + hp.make_list_from_csv(kwargs.get('cc')) + hp.make_list_from_csv(kwargs.get('bcc')))
                    logger.info("recipient_list: %s", recipient_list)
                    response = self.client.send_raw_email(
                            Source=sender,
                            Destinations=recipient_list,
                            RawMessage={'Data': message.as_string()},
                            Tags=tags
                    )
                if 'ErrorResponse' in response:
                    logger.error("Error in sending email: %s", response)
                    if hasattr(self.sent_hook, '__call__'):
                        self.sent_hook(view_id,'email', 'failed', enterprise_id, reason = str(e), communication_id = kwargs.get('communication_id'))
            except Exception as e:
                hp.print_error()
                logger.error("Error in sending email: %s", e)
                if hasattr(self.sent_hook, '__call__'):
                    self.sent_hook(view_id,'email', 'failed', enterprise_id, reason = str(e), communication_id = kwargs.get('communication_id'))
            else:
                if hasattr(self.sent_hook, '__call__'):
                    self.sent_hook(view_id,'email', 'sent',enterprise_id, communication_id = kwargs.get('communication_id'))
        return to


class ZeptoMailSender(Mailsender):

    def __init__(self, *args, **kwargs):
        super(ZeptoMailSender, self).__init__(*args, **kwargs)
        self.credentials = {
            "url": "https://api.zeptomail.com/v1.1/email",
            "bounce_address" : "msilautoexpo2023@bounce.selfiewithvarundhawan.com",
            "authorization": "Zoho-enczapikey wSsVR610+h71D6x9z2KvI7xrzQ5RBlmjQx5721en73T/S/GT9Mc/whCfUFfyHvcfQGBqHDsW9+56nh4JgTdd29Qtzl4IWiiF9mqRe1U4J3x17qnvhDzJV2tZmhCMJIkLxwRin2ZmEMBu"
        }
        if isinstance(kwargs.get('credentials'), dict):
            self.credentials.update(kwargs.get('credentials'))
        for k, v in list(self.credentials.items()):
            if v is None:
                self.credentials.pop(k)

    def sendMail(self, to, subject,enterprise_id,view_id,files=None, sender = None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        logger.debug("Sending mails to Ids to: %s", to)
        for mailTo in to:
            if not (isinstance(mailTo, (str, unicode)) and mailTo):
                logger.warning("Empty email for view_id %s", view_id)
                continue
            if kwargs.get('test') or os.environ.get('ENVIRONMENT', 'dev') not in ['production', 'staging']:
                logger.info("Original mail Id to send to: %s", mailTo)
                dMailTo = []
                for m in mailTo.split(','):
                    if os.environ.get('I2CE_EMAIL'):
                        c = os.environ.get("I2CE_EMAIL".split('@'))
                        m='{}+{}@{}'.format(c[0], m.replace('@','_').replace(".","_"), c[1])
                    else:
                        m='{}+{}@iamdave.ai'.format(os.environ.get('I2CE_USER', 'dinesh'), m.replace('@','_').replace(".","_"))
                    dMailTo.append(m)
                mailTo=','.join(dMailTo)
                logger.info("Sending debug mail to %s", mailTo)
            if not do_validate_email(mailTo):
                self.sent_hook(view_id,'email', 'failed', enterprise_id, reason = 'invalid email', communication_id = kwargs.get('communication_id'))
                continue
            url = self.credentials.get('url')
            payload =  {
                'bounce_address': self.credentials.get('bounce_address'),
                'from': {
                    'address': u'noreply@selfiewithvarundhawan.com',
                    'name': 'MSIL Auto Expo 2023'
                },
                'htmlbody': (kwargs.get('html') or '<p>{}</p>'.format(kwargs.get('text'))),
                'subject': subject,
                'to': [{
                    'email_address': {
                        'address': mailTo,
                    }
                }]
            }
            headers = {
                'accept': "application/json",
                'content-type': "application/json",
                'authorization': self.credentials.get('authorization'),
            }
            response = requests.request("POST", url, json = payload, headers=headers)
            if response.status_code < 400:
                status = True
            else:
                reason = response.content
            if hasattr(self.sent_hook, '__call__'):
                if status:
                    self.sent_hook(view_id,'email', 'sent',enterprise_id, communication_id = kwargs.get('communication_id'))
                else:
                    self.sent_hook(view_id,'email', 'failed', enterprise_id, reason = reason, communication_id = kwargs.get('communication_id'))
        return to


class SendGridSender(Mailsender):

    def __init__(self, *args, **kwargs):
        super(SendGridSender, self).__init__(*args, **kwargs)
        credentials = kwargs.get('credentials') or {}
        self.headers = {
            'Content-Type': "application/json",
            'Authorization': 'Bearer {}'.format(credentials.get('key') or "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"),
        }

    def split_name(self, name):
        if not name:
            return None
        if '<' in name and '>' in name:
            return {
                'email': name.split('<')[1].strip('>').strip(), 
                'name': name.split('<')[0].strip()
            }
        return {
            'email': name
        }

    def sendMail(self, to, subject,enterprise_id,view_id,files=None, sender = None, **kwargs):
        to, sender = self._check_to_sender(to, sender)
        logger.debug("Sending mails to Ids to: %s", to)
        def do_names(mailTo):
            dMailTo = []
            if isinstance(mailTo, (str, unicode)):
                mailTo = mailTo.split(',')
            mailTo = hp.make_list(mailTo)
            for m in mailTo:
                if not m:
                    continue
                m = self.split_name(m)
                if kwargs.get('test') or os.environ.get('ENVIRONMENT', 'dev') != 'production':
                    logger.info("Original mail Id to send to: %s", m['email'])
                    m['email']='{}+{}@i2ce.in'.format(os.environ.get('I2CE_USER', 'dinesh'), m['email'].replace('@','_').replace(".","_").replace('>','').replace('<','').replace(' ',''))
                    logger.info("Sending debug mail to %s", m['email'])
                dMailTo.append(m)
            return dMailTo
        for mailTo in to:
            if not (isinstance(mailTo, (str, unicode)) and mailTo):
                logger.warning("Empty email for view_id %s", view_id)
                continue
            if not do_validate_email(mailTo):
                self.sent_hook(view_id,'email', 'failed', enterprise_id, reason = 'invalid email', communication_id = kwargs.get('communication_id'))
                continue
            msg = {
                "personalizations": [
                    {
                        "to": do_names(mailTo),
                        "subject": subject or "Hello, World!",
                        "custom_args": {
                            "communication_id": kwargs.get('communication_id'),
                            "enterprise_id": enterprise_id
                        }
                    }
                ],
                "from": hp.make_single(do_names(sender), ignore_dict = True),
                "reply_to": hp.make_single(do_names(sender), ignore_dict = True),
                "content": [
                    {
                        "type": "text/html" if kwargs.get('html') else 'text/plain' ,
                        "value": kwargs.get('html') if kwargs.get('html') else kwargs.get('text')
                    }
                ]
            }
            if kwargs.get('cc'):
                msg['personalizations'][0]['cc'] = do_names((kwargs.get('cc') or ''))
            if kwargs.get('bcc'):
                msg['personalizations'][0]['bcc'] = do_names((kwargs.get('bcc') or ''))
            logger.info("Final Message: %s", msg)
            if files:
                a = []
                for f in files:
                    r = {
                        "content": hp.base64.b64encode(f[1][1].read()),
                        "filename": f[1][0],
                        "disposition": "attachment"
                    }
                    a.append(r)
                msg['attachments'] = a
            response = requests.post("https://api.sendgrid.com/v3/mail/send", headers = self.headers, json = msg)
            logger.info("Response: %s: %s", response.status_code, response.text)
            if response.status_code >= 400:
                e = response.json()
                e = ', '.join(map(lambda x: x['message'], e['errors']))
                if hasattr(self.sent_hook, '__call__'):
                    self.sent_hook(
                        view_id,'email', 'failed', enterprise_id, reason = e, 
                        communication_id = kwargs.get('communication_id')
                    )
                continue
            if hasattr(self.sent_hook, '__call__'):
                self.sent_hook(view_id,'email', 'sent', enterprise_id, communication_id = kwargs.get('communication_id'))
        return to


PROVIDERS = {
    'AwsSender': AwsSender,
    'MailgunSender': MailgunSender,
    'SmtpSender': SmtpSender,
    'SendGridSender': SendGridSender,
    'ZeptoMailSender': ZeptoMailSender,
    '__default__': AwsSender
}

@celery.task(name='start_mail_task')
def start_mail(
        to_email, subject, html, sender_email, view_type,
        enterprise_id=None, view_id=None,
        pdf=False, hook=None, text=None, pdf_html=None, 
        provider=None, 
        **kwargs
    ):
    """
    Inputs:
        to_email            : email list to be sent
        cc                  : cc
        bcc                 : bcc
        subject             : subject line
        html                : html to be sent to email
        sender_email        : sender's email address can be of type "name <email>"
        view_type           : campaign name, or recommendation or conversation or transaction
        enterprise_id       : enterprise_id for the enterprise we want to send the mail for
        view_id             : view_id to be sent for global link access
        pdf                 : set true if you want to attach a pdf of the html
        hook                : event hook function
        text                : required if html is not supported
        pdf_html            : if a different html needs to be converted to pdf than the main mail
        provider            : the mail provider
        test                : test mode is True
        credentials         : credentials for the provider, takes default ones if not provided
        communication_id    : communication id if required,
        files               : paths of files to send as attachments,
        delete_after_send   : Deletes the attachment files after send : default False
    """
    files = kwargs.pop("files",[])
    delete_after_send = kwargs.pop('delete_after_send', False)
    if pdf:
        fn = '{}.pdf'.format(hp.normalize_join(subject, view_id))
        p = NamedTemporaryFile(delete = False)
        tmp_flpth = p.name
        p = make_pdf(pdf_html or html, p.name)
        if p:
            files.append((p,fn))
    send_files = []
    op = {}
    logger.debug("Among the files: %s", files)
    for fil in files:
        if isinstance(fil, (tuple,list)):
            filPth, filNme = fil
        else:
            filPth, filNme = fil,None
        logger.debug("Doing the file: %s, %s, %s", fil, filPth, filNme)
        if filPth and ispath(filPth):
            op[filPth] = open(filPth, 'rb')
            send_files.append(('attachment',(filNme or op[filPth].name, op[filPth])))
    provider = provider or '__default__'
    r = PROVIDERS.get(provider, MailgunSender)(hook=hook, credentials = kwargs.get('credentials'))
    sin=r.sendMail(
        to_email, subject, enterprise_id, view_id, send_files, sender_email, 
        text=text,html=html, 
        **kwargs
    )
    for i in op:
        if op[i]: 
            op[i].close()
            if delete_after_send:
                try:
                    os.remove(i)
                except Exception as error:
                    logger.error("Error removing or closing downloaded file handle %s", error)
    return sin

def blacklist_hook(mid, mailTo):
    print mailTo, "is blacklisted"
    return

if __name__ == '__main__':
    #a=start_mail(
    #    'ananth@i2ce.in', 'test','<p> This is a test email for SMTP </p>', 'ananth@i2ce.in', 'transaction',
    #    pdf=False, credentials = {"host": "mail1.maxhypermarkets.com",
    #            "username": "spar.hypermarket",
    #            "password": "VAD31eAF", "use_ssl": False, "use_tls": False, "port": 587},
    #    provider='SmtpSender', 
    #)
    #b=start_mail(
    #    'ananth@i2ce.in', 'test','<p> This is a test email from MailGun API</p>', 'ananth@i2ce.in', 'transaction',
    #    pdf=True,
    #    provider='MailgunSender', 
    #)
    #c=start_mail(
    #    'ananth@iamdave.ai', 'test','<p> This is a test email for AWS </p>', 'DaveAI Admin <info@iamdave.ai>', 'transaction',
    #    pdf=False,
    #    provider='ZeptoMailSender', 
    #)
    c=start_mail(
        'ananth@iamdave.ai', 'test','<p> This is a test email for AWS </p>', 'DaveAI Admin <info@iamdave.ai>', 'transaction',
        pdf=False,
        provider='AwsSender', cc = "ggananth+1@gmail.com,ggananth@yahoo.com"
    )

    # a=start_mail(
    #    ['bharath.v@iamdave.ai, niteshiamdave.ai'], 'test','<p> This is a test email for SMTP </p>', 'sparmis@maxhypermarkets.com', 'transaction',
    #    pdf=False, credentials = {
    #                         "host": "mail1.maxhypermarkets.com",
    #                         "username": "sparmis",
    #                         "password": "Sp@r$mis",
    #                         "use_ssl": False,
    #                         "use_tls": False,
    #                         "port": 587
    #                         },
    #    provider='SmtpSender',cc="siddhesh@iamdave.ai, ntssahu485@gmail.com, bharathhbharath123@gmail.com"
    # )
    pass
