import os, sys, json,time
from user_agents import parse as agent_parse
from glob import glob
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, getmtime as modified_time
from flask import Blueprint, jsonify,request, render_template,redirect, url_for
from datetime import timedelta
import helpers as hp
from model import Model, CORE_MODELS, CORE_OBJECTS, models
from flask import current_app as app,flash
import traceback
from cacher import Cacher

logger = hp.get_logger(__name__)

authenticate = Blueprint('authenticate', __name__)
ROOT_PATH = joinpath(abspath(os.curdir), 'data')
DAVE_ROUTES = [
    'recommendations', 'recommendees', 'async', 
    'similar_users', 'similar_products', 'similar_customers', 'influencers', 'quantity_predictions', 'people_who_also',
    'conversation', 'executive-helper', "search"
]
MODEL_ROUTES = [
    'model', 'models', 'attributes'
]
OBJECT_ROUTES = [
    'objects', 'object', 'unique', 'exists', 'pivot', 'interactions', 'transfer_session', 'preference', 'next_interaction', 'iupdate', 'options', 'link'
]
SETTINGS_ROUTE = [
    'settings', 'perspective',
]
I2CE_ROUTES = DAVE_ROUTES + MODEL_ROUTES + OBJECT_ROUTES + SETTINGS_ROUTE

def request_error(request_type, message, code, response = None, redirect_to = None, **kwargs):
    if isinstance(message, Exception):
        message = message.message
        logger.error(
            "Enterprise_id: %s, route = %s, user = %s, ids = %s, params = %s", 
            kwargs.get('enterprise_id'),
            kwargs.get('route'),
            kwargs.get('user'),
            kwargs.get('ids'),
            kwargs.get('params')
        )
        logger.error(traceback.format_exc())
        logger.error(message)
    if request_type == 'json':
        return jsonify({'error': str(message)}), code, {"Access-Control-Allow-Origin": "*"}
    elif request.is_xhr:
        return str(message),code,{"Access-Control-Allow-Origin": "*"}
    elif redirect_to:
        flash(message)
        r = redirect(redirect_to)
        r.headers["Access-Control-Allow-Origin"] = "*"
        r.headers["Vary"] = "Origin"
        return r
    elif response is None:
        r = redirect(url_for(app.config.get("ERROR_URL","error"),**{'message' : message,'code' : code}))
        r.headers["Access-Control-Allow-Origin"] = "*"
        r.headers["Vary"] = "Origin"
        return r
    return response, code

def get_user_details(request, session, params = None):
    r = (
        request.headers.get('X-I2CE-USER-ID'),
        request.headers.get('X-I2CE-ENTERPRISE-ID'),
        None,
        request.headers.get('X-I2CE-API-KEY'),
        request.headers.get('X-I2CE-PUSH-TOKEN')
    )
    params = params or {}
    if all(r_ is None for r_ in r):
        r = tuple(session.get('i2ce_user',{}).get(r_, None) for r_ in ('user_id', 'enterprise_id', 'role', 'api_key', 'push_token'))
    if all(r_ is None for r_ in r):
        # Possible Basic Auth
        try:
            t = request.environ['HTTP_AUTHORIZATION'].split(' ')[1]
            td = hp.base64.b64decode(t).split(':')
            if len(td) != 3:
                return r
        except Exception as e:
            hp.print_error(e)
            return r
        else:
            d = authenticate_user(td[0], td[1], password = td[2])
            r = (d.get(r_, None) for r_ in ('user_id', 'enterprise_id', 'role', 'api_key', 'push_token')) 
    if all(r_ is None for r_ in r):
        # Possible JWT token
        t = request.headers.get('X-I2CE-TOKEN')
        if not t:
            return r
        d = hp.jwt_decode(t)
        r = (d.get(r_, None) for r_ in ('user_id', 'enterprise_id', 'role', 'api_key', 'push_token')) 
    if all(r_ is None for r_ in r):
        r = tuple(params.get('_i2ce_user',{}).get(r_, None) for r_ in ('user_id', 'enterprise_id', 'role', 'api_key', 'push_token'))
    return r

class Unauthenticated(Exception):
    pass

class NotPermitted(Exception):
    pass

class UnknownUser(Exception):
    pass

class UnknownModel(Exception):
    pass

class ZeroBalanceError(Exception):
    pass

class NotValidated(Exception):
    pass

def authenticate_user(user_id, enterprise_id, api_key = None, password = None, update_login = False, push_token = None, logout = False):
    if not (api_key or password):
        raise Unauthenticated("No API_KEY/PASSWORD is provided")
    if not user_id:
        raise UnknownUser("Unknown user: %s", user_id)
    u_user_id = user_id
    user_id = user_id.lower()
    enterprise_id = hp.normalize_join(enterprise_id)
    enterprise = Model("enterprise", "core").get(enterprise_id)
    if not enterprise:
        raise UnknownUser("USERNAME: {} API_KEY/PASSWORD is incorrect for given enterprise id {}".format(user_id, enterprise_id))
    if api_key:
        cid = hp.make_uuid3(enterprise_id, user_id, api_key)
    else:
        cid = hp.make_uuid3(enterprise_id, user_id, password)
    cach = Cacher(enterprise_id, enterprise.get('social_media_credentials', {}).get('login_cache_timeout', 600))
    user_api_expiry = enterprise.get('social_media_credentials', {}).get('user_api_expiry')
    if not logout and not update_login and not isinstance(user_api_expiry, (int, float)):
        cobj = cach.get(cid)
        if cobj:
            logger.info("Authenticating from cache")
            return cobj
    user = { 'enterprise_id' : hp.normalize_join(enterprise_id), 'user_id':user_id, 'role' : 'admin'}
    user_model = Model("admin", 'core', user['role'])
    user_obj = user_model.get(user_id, internal = True) or user_model.get(u_user_id, internal = True)
    if not user_obj or user_obj['enterprise_id'] != enterprise_id:
        user_model = Model("login", enterprise_id, user['role'])
        user_obj = user_model.get(user_id) or user_model.get(u_user_id)
    else:
        user_obj['role'] = 'admin'
    if not user_obj:
        raise UnknownUser("USERNAME: {} API_KEY/PASSWORD is incorrect.".format(user_id))
    time_now = hp.now(tz = user_model.timezone)
    time_since = time_now - hp.to_datetime(user_obj['last_login'])
    user_api_expiry = enterprise.get('social_media_credentials', {}).get(user_obj['role'] + '_api_expiry') or user_api_expiry
    if api_key:
        if user_obj.get('api_key') != api_key or user_obj.get('enterprise_id') != enterprise_id:
            raise UnknownUser("USERNAME: {} API_KEY is incorrect.".format(user_id))
        if isinstance(user_api_expiry, (int, float)):
            logger.info("This enterprise has api expiry implemented for role %s: %s", user_obj['role'], user_api_expiry)
            if time_since > hp.timedelta(seconds = user_api_expiry):
                user_model.update(user_obj['user_id'], {'api_key': None, 'is_logged_in': False})
                raise UnknownUser("USERNAME: {} API_KEY is outdated, please login again.".format(user_id))
            else:
                update_login = True
    elif password:
        if not hp.verify_password(password,user_obj.get('password')) or user_obj.get('enterprise_id') != enterprise_id:
            raise UnknownUser("USERNAME: {} PASSWORD is incorrect.".format(user_id))
        if not user_obj.get('validated'):
            raise NotValidated("USERNAME: {} is not validated".format(user_id))
        if isinstance(user_api_expiry, (int, float)):
            logger.info("This enterprise has api expiry implemented for role %s: %s", user_obj['role'], user_api_expiry)
            user_obj = user_model.update(user_obj['user_id'], {'api_key': None, 'is_logged_in': True, 'last_login': time_now})
            update_login = False
    if push_token and user_obj.get('push_token'):
        logger.info("Sent Push Token: %s, Logged in Push Token: %s", push_token, user_obj.get('push_token'))
        if user_obj.get('push_token') != push_token:
            if logout:
                update_login = True
            elif user_obj.get('push_token') == '__NULL__':
                update_login = True
            elif time_since < hp.timedelta(seconds = 600) and user_obj.get("is_logged_in", False):
                raise Unauthenticated("USERNAME: {} is logged in elsewhere. Login again if you want to use this device.".format(user_id))
    if time_since > hp.timedelta(seconds = 600) or logout:
        update_login = True
    user['role'] = user_obj['role']
    user['api_key'] = user_obj['api_key']
    user['user_id'] = user_obj['user_id']
    if update_login:
        d_ = {'is_logged_in': not logout, 'last_login': time_now}
        if push_token: d_['push_token'] = push_token
        if logout and isinstance(user_api_expiry, (int, float)):
            d_['api_key'] = None
        user_obj = user_model.update(user_obj['user_id'], d_)
    user['push_token'] = user_obj.get('push_token')
    logger.debug("USER OBJ after authenticate: %s", user_obj)
    if logout:
        cach.pop(cid)
    else:
        cach.set(cid, user)
    return user

def user_permitted(user, model_name,  route, request_method, ids, instance):
    role = user.get('role', 'anonymous')
    user_id = user.get('user_id')
    enterprise_id = user.get('enterprise_id')
    ids = hp.make_single(ids) if ids else None
    enterprise_model = None
    admin_model = None
    enterprise = None
    if model_name == 'dave' or route in DAVE_ROUTES:
        enterprise_model = Model(
            "enterprise", "core"
        )
        enterprise = enterprise_model.get(enterprise_id)
        if enterprise.get('credits_left') <= 0:
            raise ZeroBalanceError("You have no credits left. Cannot perform the said action")
        return True
    if role in ['admin']:
        return check_admin_permissions(enterprise_id, user_id, role, model_name, route, request_method, ids, instance, enterprise_model,  enterprise)
    else:
        return check_user_permissions(enterprise_id, user_id, role, model_name, route, request_method, ids, instance, enterprise_model, enterprise)


def check_admin_permissions(enterprise_id, user_id, role, model_name, route, request_method, ids, instance, enterprise_model = None, enterprise = None, admin_model = None):
    if route in MODEL_ROUTES and model_name in CORE_MODELS and request_method in ['write', 'delete']:
        raise NotPermitted("Not allowed to write/delete CORE model")
    if model_name in ['enterprise']:
        if not enterprise_model:
            enterprise_model = Model(
                "enterprise", "core"
            )
            enterprise = enterprise_model.get(enterprise_id)
        if route not in ['object']:
            raise NotPermitted("Not allowed to observe enterprise objects")
        elif enterprise_id != ids:
            raise NotPermitted("Cannot access model objects of core models")
        return True
    if model_name in ["admin"]:
        if not admin_model:
            admin_model = Model(
                "admin", "core"
            )
            if not ids:
                ids = admin_model.dict_to_ids(instance)
        if route not in ['object']:
            raise NotPermitted("Not allowed to observe admin objects")
        elif ids and user_id == ids and request_method == 'delete':
            raise NotPermitted("Cannot delete self")
        return True
    if model_name in ['view']:
        if route in I2CE_ROUTES:
            raise NotPermitted("Cannot modify a view")
        return True
    if model_name in ['perspective']:
        if route not in SETTINGS_ROUTE + ['object', 'objects']:
            raise NotPermitted("Cannot add perspectives on the fly")
        elif route in ['object', 'objects'] and request_method != 'read':
            raise NotPermitted("Cannot add perspectives on the fly")
    if model_name in ['i2ce_invoice', 'i2ce_usage']:
        if route not in ['objects', 'pivot', 'object']:
            raise NotPermitted("Not allowed to observe {} objects".format(model_name))
        if route in ['objects', 'pivot']:
            instance.update({'enterprise_id': enterprise_id, '_condition_type': 'AND'})
        else:
            if request_method not in ['GET']:
                raise NotPermitted("Not allowed to modify {} objects".format(model_name))
            imodel = Model(
                model_name, "core"
            )
            ins = imodel.get(ids)
            if ins['enterprise_id'] != enterprise_id:
                raise NotPermitted("Not allowed to view this object".format(model_name))
    return True

def check_user_permissions(enterprise_id, user_id, role, model_name, route, request_method, ids, permissions_role = None, enterprise_model = None, enterprise = None):
    if route in MODEL_ROUTES and request_method in ['write', 'delete']:
        raise NotPermitted("Not allowed to write/delete models")
    if model_name == 'dave':
        if route == 'async':
            return False
        return True
    if model_name in ['enterprise']:
        if not enterprise_model:
            enterprise_model = Model(
               "enterprise", "core"
            )
            enterprise = enterprise_model.get(enterprise_id)
        if route not in ['object']:
            raise NotPermitted("Not allowed to observe enterprise-level objects")
        elif request_method not in ['read']:
            raise NotPermitted("Not allowed to modify enterprise-level objects")
        elif enterprise_id != ids:
            raise NotPermitted("Cannot access model objects of other enterprises")
        return True
    if model_name in ['login']:
        if not enterprise_model:
            enterprise_model = Model(
               "enterprise", "core"
            )
            enterprise = enterprise_model.get(enterprise_id)
        if route not in ['object']:
            raise NotPermitted("Not allowed to observe enterprise-level objects")
        elif request_method not in ['read']:
            raise NotPermitted("Not allowed to modify enterprise-level objects")
        elif user_id != ids:
            raise NotPermitted("Cannot access model objects of others")
        return True
    if model_name in ['permission']:
        if not enterprise_model:
            enterprise_model = Model(
               "enterprise", "core"
            )
            enterprise = enterprise_model.get(enterprise_id)
        if route not in ['object']:
            raise NotPermitted("Not allowed to observe enterprise-level objects")
        elif request_method not in ['read']:
            raise NotPermitted("Not allowed to modify enterprise-level objects")
        elif role != ids:
            raise NotPermitted("Cannot access model objects of other models")
        return True
    if model_name in ["admin", "i2ce_usage", "i2ce_invoice", "login", "permission"]:
        raise NotPermitted("Cannot access admin models")
    if model_name in ['view']:
        if route in I2CE_ROUTES:
            raise NotPermitted("Cannot modify a view")
        return True
    if model_name in ['perspective']:
        if route not in SETTINGS_ROUTE + ['object']:
            raise NotPermitted("Cannot add perspectives on the fly")
        elif route == 'object' and request_method != 'read':
            raise NotPermitted("Cannot add perspectives on the fly")
    return True

def to_request_method(method):
    _method_dict = {
        'read': 'read',
        'write': 'write',
        'delete': 'delete',
        'put': 'write',
        'update': 'write',
        'patch': 'write',
        'post': 'write',
        'get': 'read',
    }
    if not method.lower() in _method_dict:
        raise NotPermitted("Unknown Method: {}".format(method))
    return _method_dict[method.lower()]


def agent_info():
    user_agent = request.headers.get('User-Agent')
    try:
        user_agent = agent_parse(user_agent)
    except Exception as e:
        hp.print_error()
        logger.error("Cannot parse User agent: %s", e)
        return {}
    try:
        ip = hp.make_single(
            (request.environ.get('HTTP_X_FORWARD_FOR') or request.environ.get('HTTP_X_REAL_IP') or request.remote_addr or '').split(','), 
            force = True
        ).strip()
    except Exception as e:
        hp.print_error(str(e))
        ip = None
    r = {
        'browser': user_agent.browser.family,
        'os': user_agent.os.family,
        'device': user_agent.device.family,
        'ip': ip,
        'is_mobile': user_agent.is_mobile,
        'is_tablet': user_agent.is_tablet,
        'is_touch_capable': user_agent.is_touch_capable,
        'is_pc': user_agent.is_pc,
        'is_bot': user_agent.is_bot
    }
    logger.debug("Received request from: %s", json.dumps(r, indent = 2))
    return r

def parse_url(*args, **kwargs):
    request_type = 'json' if 'json' in request.headers.get('Content-Type', 'html') else 'html' 
    model_name = kwargs.get("model_name", None)
    request_method = to_request_method(request.method)
    split = request.environ['PATH_INFO'].split('/')
    if len(split) < 2:
        if kwargs.get('do_raise', True):
            raise BadRequest
    route = split[1]
    model_name = split[2] if len(split) > 2 else None
    ids = split[3:]
    try:
        params = request.get_json() or json.loads(request.data) if request.data else {}
    except ValueError:
        params = {}
    params.update(hp.parse_forms_dict(request.values.to_dict(flat = False)))
    return request_type, model_name, route, ids, request_method, params
