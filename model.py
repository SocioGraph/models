import os, sys
import traceback
from glob import glob
from functools import wraps
import helpers as hp
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, getmtime as modified_time
from contextlib import contextmanager
import time
from collections import OrderedDict
from attribute import Attribute, ModelNotFound, AttributeTypeError, AttributeNameError, RESERVED_NAMES, RelationParentAttributeError, ValueConstraintError
import pgpersist as db
from objects import JsonTypeObject, DirectoryTypeObject, PGDictTypeObject, GraphObject, RedisObject, AttributeRecordTypeObject, run_filters, PivotActionError, PivotAttributeError, PivotJoinError, run_filter, check_filter_by
from helpers import normalize, get_logger, error_raiser, pprint, BaseError, inflect, inf, normalize_join, I2CEError, I2CEErrorWrapper, print_error, json
from stuf import stuf
import validators as val
import shutil
from matcher import Matcher, MatcherDict
import map_reduce


CORE_MODELS= ["admin","attribute","communication","enterprise","i2ce_invoice","interaction_stage","login","model","permission","perspective", "view", "recommendation","conversation","engagement", "i2ce_usage",'app_configuration', "audio_logs", "dictionary", "statistic", "warehouse", "conversation_history", "accumulator", "pipeline", "audit_log", "dave_space", "dave_room", "dave_message", "dave_message_event"]
CORE_OBJECTS = ['admin','enterprise','i2ce_invoice','perspective','view','i2ce_usage', "dictionary"]
TEMPLATE_MODELS = ["customer", "product", 'interaction']
ROOT_PATH = joinpath(abspath(os.curdir), 'data')
DATA_TYPE = 'object_concurrency_large'
MODEL_TYPE = 'default'
SERIALIZER = json
SUFFIX = 'json'
LOGIN_ATTRIBUTES = ['api_key', 'last_login', 'push_token', 'validated']
PASSWORD_LOGIN_ATTRIBUTES = ["password", "secret_key"] + LOGIN_ATTRIBUTES
SEARCHEABLE_LIST_TYPES = [
    'text', 'category', 'name', 'uid', 'email', 'phone_number', 'address', 'mobile_number'
    "description", "pincode", "zipcode", "areacode", "city", "state",
    "county", "country", "region", "zone", "location",
]
SEARCHEABLE_TYPES = SEARCHEABLE_LIST_TYPES + ["tags", "list"]
MAX_CACHE = 200

logger = hp.get_logger(__name__)

ENTEPRISE_CACHE = {}

@contextmanager
def read_file(*path, **kwargs):
    if kwargs.get('full_path', False):
        path = joinpath(*path)
    elif not path[0].startswith(dirname(dirname(abspath(__file__)))):
        path = joinpath(dirname(dirname(abspath(__file__))), *path)
    else:
        path = joinpath(*path)
    if not ispath(path):
        yield kwargs.get('default', {})
        raise StopIteration
    with open(path, 'rb') as the_file:
        try:
            json_data = json.load(the_file, encoding = 'UTF-8')
            yield json_data
        except ValueError:
            if kwargs.get('do_raise', False):
                raise
            yield kwargs.get('default', {})

@contextmanager
def update_file(*path, **kwargs):
    if kwargs.get('full_path', False):
        path = joinpath(*path)
    elif not path[0].startswith(dirname(dirname(abspath(__file__)))):
        path = joinpath(dirname(dirname(abspath(__file__))), *path)
    else:
        path = joinpath(*path)
    mode = 'r+b' if ispath(path) else 'w+b'
    logger.debug("Opening update in mode: %s", mode)
    with open(path, mode) as the_file:
        json_data = {}
        try:
            json_data = json.load(the_file, encoding = 'UTF-8') if mode == 'r+b' else {}
            yield json_data
        except ValueError:
            yield json_data
        finally:
            the_file.seek(0)
            if kwargs.get('log'):
                logger.info("Updating data in path: %s -- %s", path, json_data)
            json.dump(json_data, the_file, indent = 4, encoding = 'UTF-8', primitives = True)
            the_file.truncate()

def write_enterprise_timestamp(enterprise_id):
    with update_file(ROOT_PATH, enterprise_id, 'updated.json', full_path = True) as j:
        j['timestamp'] = hp.time()
    return j['timestamp']

def get_enterprise_timestamp(enterprise_id):
    with read_file(ROOT_PATH, enterprise_id, 'updated.json', full_path = True) as j:
        pass
    return j.get('timestamp') or hp.time()

def pop_enterprise_cache(enterprise_id):
    ENTEPRISE_CACHE[enterprise_id] = default_enterprise_cache(write_enterprise_timestamp(enterprise_id))
    return ENTEPRISE_CACHE[enterprise_id]

def default_enterprise_cache(timestamp = None):
    return {
        "timestamp": timestamp or hp.time(),
        "data": OrderedDict()
    }

def get_enterprise_cache(enterprise_id, role = None, user_id = None):
    t = get_enterprise_timestamp(enterprise_id)
    if not enterprise_id in ENTEPRISE_CACHE:
        ENTEPRISE_CACHE[enterprise_id] = default_enterprise_cache()
    r = ENTEPRISE_CACHE.get(enterprise_id)
    if r['timestamp'] >= t:
        return r["data"].get((role, user_id))
    logger.info("The enterprise updated json has timestamp later than cached enterprise")
    ENTEPRISE_CACHE[enterprise_id] = default_enterprise_cache()
    return None

def update_enterprise_cache(enterprise_id, role = None, user_id = None, data = None):
    if not enterprise_id in ENTEPRISE_CACHE:
        ENTEPRISE_CACHE[enterprise_id] = default_enterprise_cache()
    r = ENTEPRISE_CACHE.get(enterprise_id)
    if len(r["data"]) > 20:
        ENTEPRISE_CACHE[enterprise_id]["data"].popitem(last = False)
    ENTEPRISE_CACHE[enterprise_id]["data"][(role, user_id)] = data
    return data

def pop_cache(enterprise_id, name):
    _model_name = Model.to_model_name(enterprise_id, name)
    ms = [k for k in MetaModel.all_instances if k.startswith(_model_name)]
    for k in ms:
        MetaModel.all_instances.pop(k, None)
    pop_enterprise_cache(enterprise_id)

# Functions at the 'model' level
def get(model_name, enterprise_id, role='admin', model=None, user_id = None):
    m = Model(model_name, enterprise_id=enterprise_id, role=role, user_id = user_id)
    ret = m.to_dict()
    return ret


def post(model_name, enterprise_id, role='admin', model = None, user_id = None, as_dict = True):
    if not model:
        model = {}
    try:
        m = Model(model_name, enterprise_id, role)
        m.update_model(model)
        pop_cache(enterprise_id, model_name)
    except ModelNotFound:
        logger.warning("Model {} is not found".format(model_name))
        model.pop('name', None)
        m = Model(model_name, enterprise_id, role, model)
    except (RelationParentAttributeError, ValueConstraintError, RelationParentIdError) as e:
        if model.get('delete_all'):
            delete(model_name, enterprise_id)
            m = Model(model_name, enterprise_id, role, model)
        hp.print_error(e)
        if model:
            pop_cache(enterprise_id, model_name)
        raise
    except Exception as e:
        hp.print_error(e)
        if model:
            pop_cache(enterprise_id, model_name)
        raise
    if not as_dict:
        return m
    return m.to_dict()


def delete(model_name, enterprise_id, role='admin', model=None, user_id = None):
    try:
        m = Model(model_name, enterprise_id, role)
    except ModelNotFound:
        raise
    except hp.I2CEError as err:
        filepath = Model.to_file_path(enterprise_id, model_name)
        os.remove(filepath)
        return {}
    for c in m.children:
        try:
            cm = Model(c, enterprise_id, role)
        except (ModelNotFound, hp.I2CEError) as err:
            pass
        else:
            if cm.parent_ids.get(m.name, {}).get('required', False):
                delete(c, enterprise_id, role = role)
    p = Model('permission', enterprise_id)
    l = Model('login', enterprise_id)
    p.objects.delete_many(filter_by = {'model': model_name})
    l.objects.delete_many(filter_by = {'role': model_name})
    m.objects.delete_all()
    m.objects.clear()
    if m.weights:
        m.weights.clear()
    for am in m.attached_matchers:
        logger.info("deleting matcher %s from model %s in enterprise %s", am.name, m.name, enterprise_id)
        am.drop(True)
    os.remove(m.filepath)
    pop_cache(enterprise_id, model_name)
    return m.to_dict()

def delete_enterprise(enterprise_id,role='admin'):
    enterprise=Model('enterprise', 'core')
    filter_by={"enterprise_id":enterprise_id, '_as_option': True}
    for model in models(enterprise_id, _no_errors = True, _do_conversation_data = True):
        try:
            delete(model.get('name'),enterprise_id)
        except Exception as ex:
            logger.error("Error deleting model: {}".format(model.get('name')))
            print_error(ex)
    for model in CORE_MODELS:
        if model not in CORE_OBJECTS + ['model', 'attribute']:
            try:
                m = Model(model, enterprise_id, role)
                m.objects.delete_all()
                m.objects.clear()
            except Exception as e:
                logger.error("Error deleting from model: {}".format(model))
                print_error(e)
    try:
        for m in ['admin', 'view', 'i2ce_invoice', 'i2ce_usage']:
            n=Model(m, 'core')
            for i in n.filter(**filter_by):
                logger.info("Deleting all objects in model {} with id {}".format(m, hp.make_single([i.get(_) for _ in n.ids])))
                n.delete([i.get(_) for _ in n.ids])
        enterprise.delete(enterprise_id)
    except Exception as e:
        print_error(e)
        raise
    if ispath(joinpath(ROOT_PATH, enterprise_id, 'updated.json')):
        os.remove(joinpath(ROOT_PATH, enterprise_id, 'updated.json'))
    return "enterprise deleted successfully"


def models(enterprise_id, attribute_name=None, role='admin', user_id = None, **params):
    """
    list all the models
    """
    ndir = joinpath(ROOT_PATH, enterprise_id, 'models', '*.json')
    ret_list = []
    _as_model = params.pop('_as_model', False)
    _as_name = params.pop('_as_name', False)
    _do_logs = params.pop('_do_logs', False)
    _do_conversation_data = params.pop('_do_conversation_data', False)
    def yield_model():
        ms = glob(ndir)
        for r in ms:
            f, _ = splitext(r)
            _, f = pathsplit(f)
            if not _do_conversation_data and f.startswith('conversation_data'):
                logger.debug("Skipping conversation data model: %s", f)
                continue
            logger.debug("Opening model %s", f)
            try:
                #m = get(f, enterprise_id, role, user_id = user_id)
                with open(r, 'rb') as fp:
                    try:
                        m = SERIALIZER.load(fp, encoding="UTF-8")
                    except ValueError as exc:
                        fp.seek(0)
                        logger.error("Cannot read as json file {} with contents\n{}".format(r, fp.read()))
                        continue
            except NotPermitted:
                pass
            except Exception as e:
                if params.get('_no_errors'):
                    continue
                else:
                    raise
            else:
                yield m['name'], m
        if _do_logs:
            mdir = joinpath(abspath(os.curdir), 'models', 'data', 'core', 'models', 'audio_logs.json')
            with open(mdir, 'rb') as fp:
                try:
                    m = SERIALIZER.load(fp, encoding="UTF-8")
                except ValueError as exc:
                    fp.seek(0)
                    logger.error("Cannot read as json file {} with contents\n{}".format(mdir, fp.read()))
                else:
                    yield m['name'], m
            mdir = joinpath(abspath(os.curdir), 'models', 'data', 'core', 'models', 'conversation_history.json')
            with open(mdir, 'rb') as fp:
                try:
                    m = SERIALIZER.load(fp, encoding="UTF-8")
                except ValueError as exc:
                    fp.seek(0)
                    logger.error("Cannot read as json file {} with contents\n{}".format(mdir, fp.read()))
                else:
                    yield m['name'], m
            
    for instance in run_filters(yield_model(), attr = {}, filter_by = params):
        if attribute_name:
            ret_list.append(instance.get(attribute_name))
        elif _as_model:
            try:
                ret_list.append(Model(instance['name'], enterprise_id, role = role, user_id = user_id))
            except NotPermitted:
                pass
            except Exception as e:
                if params.get('_no_errors'):
                    continue
                else:
                    raise
        elif _as_name:
            ret_list.append(instance['name'])
        else:
            ret_list.append(instance)
    return ret_list
        



def to_model_path(enterprise_id, name, is_template=False):
    plural = inflect.plural(name)
    if name in CORE_MODELS or is_template:
        return joinpath(dirname(__file__), 'data', 'core', 'models', name)
    return joinpath(ROOT_PATH, enterprise_id, 'models', name)


def to_objects_path(enterprise_id, name, plural=None):
    if not plural: plural = inflect.plural(name)

    if name in CORE_OBJECTS:
        return joinpath(dirname(__file__), 'data', 'core', 'objects', plural)
    elif name == 'login' and enterprise_id == 'core':
        return joinpath(dirname(__file__), 'data', 'core', 'objects', plural)
    return joinpath(ROOT_PATH, enterprise_id, 'objects', plural)


class UnknownUser(hp.I2CEError):
    pass

class UnknownAttribute(hp.I2CEError):
    pass

class UpdateIdError(hp.I2CEError):
    pass

class UpdateUniqueError(hp.I2CEError):
    pass

class UpdateRequiredError(hp.I2CEError):
    pass


class ModelIdError(hp.I2CEError):
    pass
    
class ObjectIdError(hp.I2CEError):
    pass

class ModelTypeError(hp.I2CEError):
    pass

class ModelNameError(hp.I2CEError):
    pass


class PermissionValueError(hp.I2CEError):
    pass

class NotPermitted(hp.I2CEError):
    pass

class RelationParentIdError(hp.I2CEError):
    pass

class ParentAttributeError(hp.I2CEError):
    pass

class UpdateCoreError(hp.I2CEError):
    pass

class LoginIdError(hp.I2CEError):
    pass

class SortByError(hp.I2CEError):
    pass

class ParamError(hp.I2CEError):
    pass


class MetaModel(type):
    """
    Don't reload unless it has been updated
    """
    all_instances = {}

    def __call__(cls, name, enterprise_id, role = 'admin', model = None, is_template = False, user_id = None):
        # TODO: check if name is not restricted and is singular
        if name in RESERVED_NAMES:
            raise ModelNameError("Model name '{}' is reserved. Please use a different name for the model".format(name))
        if name != normalize_join(name):
            raise ModelNameError("Model anme should be all lower cases without special characters and spaces. Should not start with an underscore.")
        name = normalize_join(name)
        _model_name = cls.to_model_name(enterprise_id, name, role, user_id)
        filepath = cls.to_file_path(
            enterprise_id, name, is_template=is_template)
        if _model_name in MetaModel.all_instances and ispath(filepath):
            t, r = MetaModel.all_instances[_model_name]
            mtime = r.get_last_updated()
            if t >= mtime:
                #r._set_role_user(enterprise_id, role, user_id = user_id)
                logger.debug("Getting model '{}' from the cache".format(r.name))
                return r
        r = type.__call__(cls, name, enterprise_id, role, model, is_template, user_id)
        logger.debug("Initializing model '{}'".format(r.name))
        mtime = time.time()
        MetaModel.all_instances[r._model_name] = (mtime, r)
        l = len(MetaModel.all_instances)
        if l > MAX_CACHE:
            s = sorted(MetaModel.all_instances.iteritems(), key = lambda k: k[1][0])
            for i in range(l - MAX_CACHE):
                MetaModel.all_instances.pop(s[i][0], None)
        return r


class Model(object):
    @staticmethod
    def to_file_path(enterprise_id, name, is_template=False):
        return '{}.{}'.format(
            to_model_path(enterprise_id, name, is_template), SUFFIX)

    @staticmethod
    def to_model_name(enterprise_id, name, role = None, user_id = None):
        if name in CORE_OBJECTS:
            enterprise_id = 'core'
            role = None
            user_id = None
        elif name in CORE_MODELS:
            role = 'admin'
            user_id = None
        g = [enterprise_id, name]
        if role: g.append(role)
        if user_id: g.append(user_id)
        return '_'.join(g)

    _NotPermitted = NotPermitted

    exposed_methods = {
        'GET': {
            'attributes': 'get_attributes',
            'unique': 'get_unique',
            'options': 'get_options',
            'objects': 'list',
            'object': 'get',
            'new': 'new',
            'exists': 'exists',
            'models': 'get_models',
            'list': 'list',
            'filter': 'list'
        },
        'POST': {
            'object': 'post',
            'warehouse': 'warehouse',
            'restore': 'restore',
            'accumulate': 'accumulate',
            'objects': 'update_many',
            'update_many': 'update_many'
        },
        'PUT': {
            'object': 'put',
            'put': 'put'
        },
        'DELETE': {
            'object': 'delete',
            'delete': 'delete',
            'objects': 'delete_many',
            'delete_many': 'delete_many'
        },
        'UPDATE': {
            'object': 'update',
            'update': 'update'
        },
        'PATCH': {
            'object': 'update',
            'patch': 'patch'
        }
    }

    contains_methods = {
            'unique': lambda model, *attr: (lambda *x: model.exists(**dict(zip(attr,x)))),
            'objects': lambda model, *attr: (lambda x: model.exists(**model.ids_to_dict(model.dict_to_ids(x)))),
            'options': lambda model, *attr: (lambda x: model.exists(**{model.ids[0]: x}))
    }


    @staticmethod
    def check_filter_by(attributes, params):
        return check_filter_by(attributes, params)

    @staticmethod
    def run_filter(instance, attr = None, filter_by = None, condition_type = 'AND'):
        return run_filter(instance, attr = attr, filter_by = filter_by, condition_type = condition_type)

    @staticmethod
    def run_filters(instances, attr = None, filter_by = None, condition_type = 'AND'):
        for instance in run_filters(instances, attr = attr, filter_by = filter_by, condition_type = condition_type):
            yield instance

    @staticmethod
    def url_to_method(url_method, http_method='GET'):
        return Model.exposed_methods.get(
            http_method, Model.exposed_methods['GET']).get(url_method, 'get')

    __metaclass__ = MetaModel


    def __repr__(self):
        return "{} with name '{}' (ids = {})".format(self.__class__.__name__, self.name, self.ids)

    def pop_cache(self):
        self.update_model(self.to_dict())
        pop_cache(self.enterprise_id, self.name)

    def _set_role_user(self, enterprise_id, role, user_id = None):
        if hasattr(self, 'enterprise_model'):
            self.enterprise = self.enterprise_model.get(enterprise_id)
        if user_id and hasattr(self, 'role_model'):
            self.user = self.role_model.get(user_id)

    def get_last_updated(self):
        try:
            mtime = modified_time(self.filepath)
        except OSError:
            mtime = time.time()
        otime = None
        # TODO: FInd out why this is needed
        #if self.objects:
        #    try:
        #        otime = self.objects.get_last_updated()
        #    except OSError as e:
        #        logger.error("Cannot get last updated from objects")
        #        hp.print_error()
        #        otime = time.time()
        #    else:
        #        if not isinstance(otime, float):
        #            otime = time.mktime(otime.timetuple())
        return max(mtime, otime)

    def __init__(
            self,
            name,
            enterprise_id,
            role='admin',
            model=None,
            is_template=False,
            user_id = None
        ):
        """
        name: model name
        default_attributes: contains spec for attributes. Each Attribute is of type Attribute
            At least one attribute should have type 'id'
            The primary key is made from all the 'id' attributes
        """
        if model is None: model = {}
        if not name:
            raise ModelNameError("Name has to be provided for a model")
        self.name = hp.normalize_join(name)
        if self.name in RESERVED_NAMES: 
            self.name += '_'
        self.enterprise_id = enterprise_id
        self.role = role or 'admin'
        self.is_template = is_template
        self.enterprise = {}
        if self.name not in CORE_MODELS and not self.is_template:
            self.enterprise_model = Model('enterprise', 'core')
            self.admin_model = Model('admin', 'core')
            self.enterprise = self.enterprise_model.get(enterprise_id)
        elif self.name in CORE_OBJECTS:
            self.enterprise_id = 'core'
        elif self.name in CORE_MODELS:
            self.enterprise_model = Model('enterprise', 'core')
            self.admin_model = Model('admin', 'core')
            self.enterprise = self.enterprise_model.get(enterprise_id)
        if self.role == self.name:
            self.role_model = self
        elif self.name not in CORE_MODELS:
            self.role_model = self.__class__(self.role, 'core' if self.role == 'admin' else self.enterprise_id)
        else:
            self.role_model = self.__class__('admin', 'core')
            self.role = 'admin'
            user_id = None
        # TODO: check if name is not restricted and is singular
        self._original_name = name
        self._model_name = self.to_model_name(self.enterprise_id, self.name, self.role, user_id)
        if self.name in CORE_MODELS:
            self._is_core_model = True
        else:
            self._is_core_model = False
        self.filepath = self.to_file_path(
            self.enterprise_id, self.name, is_template=self.is_template)
        write = False
        if not model.get('attributes'):
            try:
                logger.debug("Trying to open model %s at path %s", self.name,
                             self.filepath)
                # If model already exists, then load the model
                with open(self.filepath, 'rb') as fp:
                    try:
                        model = SERIALIZER.load(fp, encoding="UTF-8")
                    except ValueError as exc:
                        fp.seek(0)
                        logger.error("Cannot read as json file {} with contents\n{}".format(self.filepath, fp.read()))
                        raise
            except IOError as exc:
                if not model.get('attributes'):
                    raise ModelNotFound(self.filepath,
                                        "Model \"{}\" not found".format(self.name), log = False)
                if enterprise_id == 'core':
                    raise UpdateCoreError("Cannot update core model {}".format(self.name), log = False)
                logger.debug("New model %s specified at path %s, will be writing",
                             self.name, self.filepath)
                write = True
        else:
            if enterprise_id == 'core':
                raise UpdateCoreError("Cannot update core model {}".format(self.name))
            logger.debug("Posting a model update")
            write = True

        # Init the json_data object with the model provided
        # TODO: Load model.json and use the template to load it.
        json_data = self._init_dict(**model)

        # Get all the special attributes
        if self.name in ['permission'] + CORE_OBJECTS:
            self.permission_model = None
        else:
            self.permission_model = self.__class__('permission', self.enterprise_id)
        attributes = json_data.pop('attributes', [])
        parents = json_data.pop('parents', [])
        children = json_data.pop('children', [])
        permissions = json_data.pop('permissions', [])
        attached_matchers = json_data.pop('attached_matchers', [])
        quick_views = json_data.pop('quick_views', {})
        preferences = json_data.pop('preferences', {})
        actions = json_data.pop('actions', {})
        warehouser = json_data.pop('warehouser', [])
        accumulator = json_data.pop('accumulator', [])
        self._dict = stuf()
        for k, v in json_data.iteritems():
            self._dict[k] = v
            setattr(self, k, v)
        self._update_parents(parents)
        self._update_children(children)
        self._update_attributes(attributes, forced = write)
        self._update_quick_views(quick_views, forced = write)
        self._update_preferences(preferences, forced = write)
        self._update_actions(actions, forced = write)
        self._update_warehouser(warehouser, forced = write)
        self._update_accumulator(accumulator, forced = write)
        self._update_attached_matchers(attached_matchers)

        if self.has_login:
            self.login_errors()
            self.login = Model('login', enterprise_id=self.enterprise_id)
            if self.name not in [p.get('role') for p in permissions]:
                permissions.append({'role': self.name, 'read_access': True, 'write_access': True, 'delete_access': True})
        else:
            self.login = None

        if self.name != 'permission':
            if self.allow_anonymous_view:
                permissions.append({'role': 'anonymous', 'read_access': True})
            self._update_permissions(permissions)

        self._set_objects()
        if hasattr(self, 'enterprise'):
            if self.model_type in ['customer', 'product', 'interaction', 'connection', 'store']:
                m = self.model_type
                logger.debug("Now updating enterprise with %s_model_name", m)
                if not self.enterprise.get(m + '_model_name'):
                    self.enterprise = self.enterprise_model.update(self.enterprise_id, {m + '_model_name': self.name})
                    logger.debug("After updating enterprise with customer model: %s", self.enterprise)

        # Write the model is initializing for the first time
        if self.role == 'admin' or self.name in ['permission', 'perspective']:
            self.now_permissions = None
        else:
            self.now_permissions = self.permissions.get(self.role)
            if not self.now_permissions:
                self.now_permissions = self.permissions.get('anonymous')
            if not self.now_permissions:
                self.user_id = hp.make_single(user_id)
                return self._raise_not_permitted()
        if user_id:
            self.user_id = hp.make_single(user_id)
            self.user = self.role_model.get(self.user_id)
            if not self.user:
                id_ = self.role_model.ids[0] 
                self.user = hp.make_single(self.role_model.list(_page_size = 1, _as_option = True, **{id_: self.user_id}), force = True)
            if not self.user:
                raise UnknownUser("User id {} is unknown in role model {}".format( self.user_id, self.role))
        else:
            self.user_id = None
            self.user = None
        if write:
            logger.debug("Writing model %s at path %s", self.name,
                         self.filepath)
            hp.mkdir_p(dirname(self.filepath))
            with open(self.filepath, 'wb') as fp:
                SERIALIZER.dump(
                    self.to_store(), fp, indent=4, encoding="UTF-8")
                fp.truncate()


    def login_errors(self):
        if len(self.ids) > 1:
            raise LoginIdError(
                "Model {} (enterprise_id: {}) has a login and should have unique id not {}".
                format(self.name, self.enterprise_id, self.ids))
        if not (self.emails or self.mobile_numbers):
            raise LoginIdError(
                "Model {} (enterprise_id: {}) has a login and some attributes should be of type email or mobile_number".format(
                    self.name, self.enterprise_id
                )
            )
        if not any(self.attributes[_i].id or self.attributes[_i].required for _i in (self.emails + self.mobile_numbers)):
            raise LoginIdError(
                "Model {} (enterprise_id: {}) has a login and at least 1 of the attributes {} should have required = True".format(
                    self.name, self.enterprise_id, self.emails + self.mobile_numbers
                )
            )

    def _init_dict(self, **kwargs):
        json_data = dict(
            name = self.name,
            title = kwargs.get('title') or hp.make_title(self._original_name),
            description = kwargs.get('description') or hp.make_title(self._original_name),
            plural = kwargs.get('plural') or inflect.plural(self._original_name),
            attributes = kwargs.get('attributes') or [],
            permissions = kwargs.get('permissions') or [],
            has_login = kwargs.get('has_login') or False,
            parents = (kwargs.get('parents') or []),
            children = (kwargs.get('children') or []),
            data_type = kwargs.get('data_type') or DATA_TYPE,
            model_type = kwargs.get('model_type') or MODEL_TYPE,
            timezone = kwargs.get('timezone') or 'UTC',
            expand_attributes = kwargs.get('expand_attributes', True),
            allow_anonymous_view = kwargs.get('allow_anonymous_view', False),
            attached_matchers = kwargs.get('attached_matchers', []),
            quick_views = kwargs.get('quick_views', {}),
            preferences = kwargs.get('preferences', {}),
            actions = kwargs.get('actions', {}),
            warehouser = kwargs.pop('warehouser', []),
            accumulator = kwargs.pop('accumulator', []),
        )
        self.ids = []
        self.names = []
        self.quick_views = {}
        self.preferences = {}
        self.actions = {}
        self.warehouser = []
        self.accumulator = []
        return json_data

    def add_attached_matchers(self, *matchers):
        j = self.to_store()
        prev_m = j['attached_matchers']
        matchers = [m.name for m in matchers]
        j['attached_matchers'] = matchers
        logger.debug("updated model to get new matchers: %s", matchers)
        if matchers != prev_m:
            self.update_model(j)

    def get_weights(self):
        return dict(self.weights.items())

    def set_weights(self, do_default = True):
        cw = self._default_weights if do_default else self.weights
        for a in self.attr_seq:
            if a not in cw:
                if self.model_type in ['product', 'customer', 'interaction', 'store'] and self.attached_matchers:
                    logger.debug("Setting weights for attribute %s in model %s", a, self.name)
                    d = self.attached_matchers[0].prob(a)
                    logger.debug("Out put of prob is %s for attached matcher %s", d, self.attached_matchers[0].name)
                    if d:
                        self.weights[a] = hp.make_entropy(d)
                    else:
                        self.weights[a] = 0.
    
    def get_max_weight(self, attribute_only = True):
        self.set_weights(do_default = False)
        m = max(v_ for v_ in self.weights.itervalues() if not (hp.np.isnan(v_) or hp.np.isinf(v_)))
        w = []
        i = []
        for i_, w_ in self.weights.iteritems():
            if w_ == m:
                w.append(w_)
                i.append(i_)
        if not len(w):
            i = self.weights.keys()
            w = [1.]*len(i)
        if len(w) == 1:
            ri = 0
        else:
            ri = hp.histogram_sample(w)
        if attribute_only:
            return i[ri]
        return i[ri], w[ri]
                

    def _to_dict_from_self(self):
        for attr in (
                'preferences', 'quick_views', 
                'ids', 'names', 'times', 'emails', 'push_tokens', 
                'phone_numbers', 'mobile_numbers', 'images', 
                'actions', 'warehouser', 'accumulator', 'locations'
            ):
            self._dict[attr] = getattr(self, attr)
        return self._dict

    def _set_objects(self):
        if self.is_template or (self.enterprise_id == 'core' and self.name == 'permission'):
            self.objects = None
            return
        self.data_type = getattr(self, 'data_type', DATA_TYPE)
        if self.data_type == 'mostly_static':
            self.objects = JsonTypeObject(
                to_objects_path(self.enterprise_id, self.name, self.plural),
                self)
            self.is_db = False
        elif self.data_type == 'object_concurrency_small':
            self.objects = DirectoryTypeObject(
                to_objects_path(self.enterprise_id, self.name, self.plural),
                self)
            self.is_db = False
        elif self.data_type == 'object_concurrency_large':
            self.is_db = True
            self.objects = PGDictTypeObject(
                to_objects_path(self.enterprise_id, self.name, self.plural),
                self)
        elif self.data_type == 'record_concurrency_large':
            self.is_db = True
            self.objects = AttributeRecordTypeObject(
                to_objects_path(self.enterprise_id, self.name, self.plural),
                self)
        elif self.data_type == 'graph':
            self.is_db = True
            self.objects = GraphObject(
                to_objects_path(self.enterprise_id, self.name, self.plural),
                self)
        elif self.data_type == 'cache':
            self.is_db = True
            self.objects = RedisObject(
                to_objects_path(self.enterprise_id, self.name, self.plural),
                self)
        else:
            raise Exception("Unknown datatype: {}".format(self.data_type))

    def _check_permissions(self, instance, method = 'read', do_verify = True):
        logger.debug("Model %s has permissions: %s, %s, %s", self.name, self.now_permissions, self.role, self.user_id)
        if self.now_permissions is not None:
            if not isinstance(instance, dict):
                ids = instance
                instance = self.get(ids)
            else:
                ids = self.dict_to_ids(instance)
            if self.user_id and self.role and hp.make_list(self.user_id) == ids and self.role == self.name:
                return None, None
            if not self.now_permissions.get(method + '_access', False):
                return self._raise_not_permitted(ids, method = method)
            uf = self.now_permissions.get(method + '_user_filter', {})
            if uf and not self.run_filter(self.user, self.role_model.attributes, uf):
                # will try to make the filter something more generic
                return self._raise_not_permitted(ids, method = method)
            if self.user_id:
                hidden = list(set(self.now_permissions.get('hidden', [])) |  set(self.attr_seq) - set(self.now_permissions.get('visible', self.attr_seq)))
                readonly = list(set(self.now_permissions.get('readonly', [])) | set(self.attr_seq) - set(self.now_permissions.get('writeonly', self.attr_seq)))
                filters = self.check_filter_by(self.attributes, hp.evaluate_args(self.now_permissions.get(method + '_filter', {}), self.user))
                if do_verify:
                    if not instance and method in ['read', 'delete']:
                        return None, None
                    if not self.run_filter(instance, self.attributes, filters):
                        return self._raise_not_permitted(ids, method = method)
                    if self.role in self.parents:
                        pa = self.now_permissions.get(method + '_children_only', False)
                        if pa:
                            filters.update(self.from_parent_id_dict(self.role, self.user))
                            if not self.run_filter(instance, self.attributes, filters):
                                return self._raise_not_permitted(ids, method = method)
                    if self.role in self.children:
                        pa = self.now_permissions.get(method + '_parent_only', False)
                        if pa:
                            filters.update(self.role_model.to_parent_id_dict(self.name, self.user))
                            if not self.run_filter(instance, self.attributes, filters):
                                return self._raise_not_permitted(ids, method = method)
                if self.role == self.name:
                    if hp.make_list(self.user_id) != ids:
                        #TODO: Handle Spouse
                        hidden = list(set(self.now_permissions.get('hidden_others', [])) | set(self.attr_seq) - set(self.now_permissions.get('visible_others', self.attr_seq)))
                        pa = self.now_permissions.get(method + '_sibling', False)
                        if pa:
                            hits = False
                            for pm in pa:
                                if not do_verify:
                                    hidden = list(set(hidden + pm.get('hidden',[])) | set(self.attr_seq) - set(pm.get('visible', self.attr_seq)))
                                    readonly = list(set(readonly + pm.get('readonly',[])) | set(self.attr_seq) - set(pm.get('writeonly', self.attr_seq)))
                                elif do_verify and pm.get('parent_model'):
                                    _filters = filters.copy()
                                    self.get_parent_ids(pm['parent_model'], input_dict = self.user, output_dict = _filters)
                                    if self.run_filter(instance, self.attributes, _filters):
                                        hidden = list(set(hidden + pm.get('hidden',[])) | set(self.attr_seq) - set(pm.get('visible', self.attr_seq)))
                                        readonly = list(set(readonly + pm.get('readonly',[])) | set(self.attr_seq) - set(pm.get('writeonly', self.attr_seq)))
                                        hits = True
                                        break
                            if not hits:
                                return self._raise_not_permitted(ids, method = method)
                pa = self.now_permissions.get(method + '_spouse', []) or []
                for pm in pa:
                    if not do_verify:
                        hidden = list(set(hidden + pm.get('hidden',[])) | set(self.attr_seq) - set(pm.get('visible', self.attr_seq)))
                        readonly = list(set(readonly + pm.get('readonly',[])) | set(self.attr_seq) - set(pm.get('writeonly', self.attr_seq)))
                    elif pm and isinstance(pm, dict) and pm.get('child_model'):
                        if do_verify:
                            child_model = self.__class__(pm.get('child_model'), self.enterprise_id)
                            if self.role in child_model.parents and self.name in child_model.parents:
                                f = child_model.from_parent_id_dict(self.role, self.user)
                                f = child_model.from_parent_id_dict(self.name, instance, f)
                                if not child_model.exists(**f):
                                    return self._raise_not_permitted(ids, method = method)
                                else:
                                    hidden = list(set(hidden + pm.get('hidden',[])) | set(self.attr_seq) - set(pm.get('visible', self.attr_seq)))
                                    readonly = list(set(readonly + pm.get('readonly',[])) | set(self.attr_seq) - set(pm.get('writeonly', self.attr_seq)))
                return hidden, readonly
        return None, None


    def _filter_write(self, instance, is_new = False, updated = None, previous = None): 
        if self.now_permissions is not None:
            _, readonly = self._check_permissions(instance, method = 'write')
            if readonly:
                if isinstance(updated, dict):
                    [updated.pop(_r, None) for _r in readonly]
                elif is_new:
                    [instance.pop(_r, None) for _r in readonly]
                elif isinstance(previous, dict):
                    for _r in readonly:
                        instance[_r] = previous.get(_r)

    def _manage_attributes(self, instance, is_new=False, updated=None, previous = None, updated_list = None):
        new_instance = {}
        if isinstance(previous, dict):
            previous = hp.copyof(previous)
        if isinstance(updated, dict):
            instance.update(updated)
        if not isinstance(updated_list, dict):
            updated_list = {}
        logger.debug("managing attributes for parent refers")
        remaining = {}
        for p, v in self.parent_ids.iteritems():
            logger.debug("Parent: %s, Map: %s", p, v.get('self_parent_map'))
            if v.get('self_parent_map'):
                try:
                    pinst = self._get_parents(instance, p, do_raise = True)
                except RelationParentIdError:
                    remaining[p] = v
                else:
                    for ac, ap in v['self_parent_map'].iteritems():
                        instance.update({ac: pinst.get(ap)})
                        if isinstance(updated, dict) and ac not in updated: updated[ac] = pinst.get(ap)
        logger.debug("Updating attributes : %s for model %s", updated, self.name)
        for k in self.attr_seq:
            logger.debug("Processing attribute %s", k)
            logger.debug("Current instance %s", instance)
            logger.debug("Current new_instance %s", new_instance)
            logger.debug("Current value %s", instance.get(k))
            a = self.attributes[k]
            v_prev = (previous or {}).get(k)
            v = a.process(
                instance,
                k,
                new_instance,
                _is_new = is_new,
                _is_updated = (not isinstance(updated, (dict))) or (updated.get(k) is not None and (v_prev is None or v_prev != updated.get(k))),
                _updated = updated if isinstance(updated, dict) else None,
                _previous = v_prev
            )
            if v is not None:
                new_instance[k] = v
            if not (v_prev == v):
                updated_list[k] = v_prev
            logger.debug("Current new_value %s\n", new_instance.get(k))
        for p, v in remaining.iteritems():
            logger.debug("Parent: %s, Map: %s", p, v.get('self_parent_map'))
            if v.get('self_parent_map'):
                pinst = self._get_parents(new_instance, p)
                for ac, ap in v['self_parent_map'].iteritems():
                    v_prev = (previous or {}).get(ac)
                    v = pinst.get(ap)
                    new_instance.update({ac: v})
                    if v_prev != v:
                        updated_list[k] = v_prev
        if self.expand_attributes:
            for k, v in instance.iteritems():
                v_prev = (previous or {}).get(k)
                if not isinstance(k, (str, unicode)) or not k or k.startswith('_'):
                    continue
                if k not in new_instance and v is not None:
                    new_instance[k] = v
                    if v_prev != v:
                        updated_list[k] = v_prev
        self._filter_write(new_instance, is_new, updated, previous)
        return new_instance

    def _raise_not_permitted(self, ids = None, method = 'access'):
        raise NotPermitted("Role: {}{} is not permitted to {} object {}in model {}".format(
                self.role,
                ', user_id: {}'.format(self.user_id) if self.user_id else '',
                method,
                'with id {} '.format(hp.make_single(ids)) if ids else '',
                self.name
            )
        )


    def _filter_read(self, instance, attributes = None, method = 'read', do_verify = True):
        if not instance:
            return attributes
        hidden, _ = self._check_permissions(instance, do_verify = do_verify)
        if hidden:
            if attributes is None: attributes = self.attr_seq
            attributes = list(set(attributes) - set(hidden))
        return attributes

    def _to_internal(self, k, v):
        if k in self.attributes:
            if db.geolocation in self.attributes[k].atype:
                return db.geolocation(v)
            if db.date in self.attributes[k].atype:
              return hp.to_datetime(instance[k])
            if db.datetime in self.attributes[k].atype:
                return hp.to_datetime(instance[k])
        return v

    def _jsonify_attributes(self, instance, attributes=None, internal = False, additional_values = None, do_verify = True, enforce_attributes = False):
        if not instance:
            return instance
        attributes = self._filter_read(instance, attributes, do_verify = do_verify)
        if attributes is not None:
            attributes = hp.make_list(attributes)
        if isinstance(additional_values, dict):
            additional_values = additional_values.copy()
            additional_values.update(instance)
            instance = additional_values
        if (internal or enforce_attributes) and isinstance(attributes, (list, tuple)):
            attr_seq = attributes
        else:
            attr_seq = self.attr_seq
        for k in attr_seq:
            v = instance.get(k, None)
            a = self.attributes[k]
            # If internal we want to maintain atypes
            if a.jsoner and val.is_validator(a.jsoner) and not (internal and a.jsoner['function'].startswith('from_')):
                try:
                    v = val.execute(a.jsoner, a, instance, k, v)
                except Exception as e:
                    logger.warn("Error while executing jsoner function {} on instance {}, attribute = {}, value = {}, model = {}".format(
                        a.jsoner, instance, k, v, self.name
                        )
                    )
                    if v is not None:
                        raise
            if v is None and a.default is not None:
                if val.is_validator(a.default):
                    if val.is_idempotent(a.default):
                        try:
                            v = val.execute(a.default, a, instance, k, v)
                        except Exception as e:
                            logger.warn("Error while executing default function {} on instance {}, attribute = {}, value = {}, model = {}".format(
                                    a.default, instance, k, v, self.name
                                )
                            )
                else:
                    v = a.default
            if v is None:
                instance.pop(k, None)
                continue
            if a.atype[0] == db.geolocation and internal:
                v = db.geolocation(v)
            if k not in PASSWORD_LOGIN_ATTRIBUTES or internal:
                instance[k] = v
        if isinstance(attributes, (list, tuple)):
            new_instance = {}
            for a in attributes:
                i = instance.get(a)
                if i is not None:
                    new_instance[a] = i
            return new_instance
        return instance

    def new(self):
        ret = {}
        for a in self.attr_seq:
            att = self.attributes[a]
            if val.is_validator(att.default):
                ret[att.name] = None
            else:
                ret[att.name] = att.default
        return ret

    def get(self, ids, 
            default = None, 
            attributes = None, 
            internal = False, 
            quick_view = False, 
            _additional_values = None, 
            **kwargs
        ):
        if default is None:
            default = {}
        if quick_view and not attributes:
            attributes = self.to_quick_view(quick_view)
        ids = hp.make_list(ids)
        r = self.objects.get(ids)
        if r:
            r = self._jsonify_attributes(r, attributes, internal, additional_values = _additional_values, enforce_attributes = kwargs.get('_enforce_attributes'))
            if self.login:
                r.update(
                    self.login.get(
                        ids, attributes=LOGIN_ATTRIBUTES if not internal else PASSWORD_LOGIN_ATTRIBUTES,
                        internal = internal,
                    )
                )
            for i, n in zip(ids, self.ids):
                r[n] = i
            return r
        return default

    def put(self, ids, instance, *args, **kwargs):
        ids = hp.make_list(ids)
        internal = kwargs.get('internal', False)
        if self.login:
            instance['role'] = self.name
            instance.update(
                (_k, _v) for _k, _v in self.login.put(
                    ids, 
                    instance, 
                    internal = internal
                ).iteritems() if _k in LOGIN_ATTRIBUTES
            )
            instance.pop('role', None)
        for i, v in zip(self.ids, ids):
            instance[i] = v
        updated_list = {}
        instance = self._manage_attributes(instance, is_new=False, previous = self.get(ids), updated_list = updated_list)
        ro = self.objects.set(ids, **instance)
        r = self._jsonify_attributes(ro, internal = internal)
        r.update(self.ids_to_dict(ids))
        self._update_children_refers(ro, updated_list)
        return r

    def delete(self, ids, *args, **kwargs):
        ids = hp.make_list(ids)
        self._check_permissions(ids, method = 'delete')
        r = self.objects.pop(ids, {})
        for k in self.attr_seq:
            a = self.attributes[k]
            v = r.get(k)
            if a.cleaner and val.is_validator(a.cleaner):
                v1 = val.execute(a.cleaner, a, r, k, v)
                r[k] = v1 or v
        if self.login:
            r.update((_k, _v) for _k, _v in self.login.delete(ids).iteritems() if _k in LOGIN_ATTRIBUTES)
        self._delete_children_refers(r)
        return r

    
    def update(self, ids, instance, *args, **kwargs):
        ids = hp.make_list(ids)
        old_instance = self.objects.get(ids)
        internal = kwargs.get('internal', False)
        if not old_instance:
            raise KeyError("Model {} does not have any objects on ids {}".
                           format(self.name, ids))
        if isinstance(kwargs.get('_previous_instance'), dict):
            kwargs['_previous_instance'].update(old_instance)
        for i, v in zip(self.ids, ids):
            instance[i] = v
        updated_list = {}
        instance = self._manage_attributes(old_instance, is_new=False, updated=instance, previous = old_instance, updated_list = updated_list)
        ro = self.objects.set(ids, **instance)
        r = self._jsonify_attributes(ro, internal = internal)
        r.update(self.ids_to_dict(ids))
        self._update_children_refers(ro, updated_list)
        if self.login:
            r.update(
                (_k, _v) for _k, _v in self.login.update(
                    ids, 
                    instance, 
                    internal = internal
                ).iteritems() if _k in LOGIN_ATTRIBUTES
            )
        return r
    
    def link(self, id1, id2, _bi_directional = True, _distance = None, _location_attr = None, _default_distance = 1., *args, **kwargs):
        id1 = hp.make_list(id1)
        id2 = hp.make_list(id2)
        i1 = self.get(id1, internal = True)
        i2 = self.get(id2, internal = True)
        _location_attr = _location_attr or hp.make_single(self.locations, force = True)
        logger.debug("Location Attribute: %s", _location_attr)
        logger.debug("L1: %s", i1.get(_location_attr))
        logger.debug("L2: %s", i2.get(_location_attr))
        logger.debug("Calculated distance: %s", hp.distance(i1.get(_location_attr), i2.get(_location_attr)))
        r = self.objects.link(
            id1, id2,
            _bi_directional, 
            _distance = _distance if isinstance(_distance, (int, long, float)) else hp.distance(i1.get(_location_attr), i2.get(_location_attr)),
            _default_distance = _default_distance,
            **kwargs
        )
        return r
    
    def unlink(self, id1, id2, _bi_directional = True, *args, **kwargs):
        id1 = hp.make_list(id1)
        id2 = hp.make_list(id2)
        i1 = self.get(id1)
        i2 = self.get(id2)
        r = self.objects.unlink(
            id1, id2,
            _bi_directional,
        )
        return r

    def closest(self, id1, id2 = None, **params):
        load_total = {'total_number': 0}
        params['_load_total'] = load_total
        _params = {}
        if '_as_option' not in params:
            params['_as_option'] = False
        elif params.get('_as_option'):
            params.pop('_load_total', None)
        start, limit, page_number, page_size, internal, _as_option, filter_attributes, filter_by, condition_type, sort_by, reverse, load_total = self._manage_params(_managed_params = _params, **params)
        base_filter = self._filter_permissions(method = 'get')
        data = list(
            self.objects.closest(
                id1, id2, limit = limit, filter_by = filter_by, base_filter = base_filter,
                last_updated = _params.get('last_updated'),
                updated_before = _params.get('updated_before')
            )
        )
        if params.get('_as_option') or params.get('internal'):
            return data
        load_total['total_number'] = len(data)
        data = data[start:limit]
        return {
            'page_number': _params['page_number'],
            'page_size': _params['page_size'],
            'data': data,
            'total_number': load_total['total_number'],
            'is_first': self.is_first(_params['start'], _params['limit'], load_total['total_number']),
            'is_last': self.is_last(_params['start'], _params['limit'], load_total['total_number'])
        }

    
    def iupdate(self, ids, instance, *args, **kwargs):
        ids = hp.make_list(ids)
        old_instance = self.objects.get(ids)
        internal = kwargs.get('internal', False)
        if not old_instance:
            raise KeyError("Model {} does not have any objects on ids {}".
                           format(self.name, ids))
        if isinstance(kwargs.get('_previous_instance'), dict):
            kwargs['_previous_instance'].update(old_instance)
        for k, v in instance.items():
            if not isinstance(k, (str, unicode)) or k.startswith('_'):
                continue
            if v is None:
                continue
            try:
                instance[k] = self.iadd(ids, k, v)
            except (db.ValueTypeError, AttributeTypeError) as e:
                hp.print_error("Got error while doing an iupdate: {}".format(e))
        for i, v in zip(self.ids, ids):
            instance[i] = v
        updated_list = {}
        new_instance = self._manage_attributes(old_instance, is_new = False, updated = instance, previous = old_instance, updated_list = updated_list)
        self._update_children_refers(new_instance, updated_list)
        return self._jsonify_attributes(new_instance, internal = internal)

    def update_many(self, instance, filters = None, **kwargs):
        # really long running , don't use it preferrably, use the one in actions
        updated = []
        if filters is None: filters = {}
        filters.update(kwargs)
        for c in self.filter(_method = 'write', **filters):
            try:
                self.update(self.dict_to_ids(c), instance)
            except Exception as e:
                updated.append({"error": str(e), "ids": self.dict_to_ids(c)})
            else:
                updated.append({"info": "updated", "ids": self.dict_to_ids(c)})
        return updated

    def delete_many(self, filters = None, **kwargs):
        # really long running , don't use it preferrably, use the one in actions
        if filters is None: filters = {}
        filters.update(kwargs)
        deleted = []
        for c in self.filter(_method = 'delete', **filters):
            try:
                self.delete(self.dict_to_ids(c))
            except Exception as e:
                deleted.append({"error": str(e), "ids": self.dict_to_ids(c)})
            else:
                deleted.append({"info": "deleted", "ids": self.dict_to_ids(c)})
        return deleted

    def patch(self, *args, **kwargs):
        return self.update(*args, **kwargs)

    def update_from_instance(self, instance, *args, **kwargs):
        ids = self.dict_to_ids(instance)
        return self.update(ids, instance, *args, **kwargs)


    def post(self, instance, internal = False, **kwargs):
        ids = [instance.get(k) for k in self.ids]
        if any(i is None for i in ids):
            r = None
        else:
            r = self.objects.get(ids)
        initial_keys = instance.keys()
        logger.debug("New instance for post before processing is %s", instance)
        updated_list = {}
        if r:
            if isinstance(kwargs.get('_previous_instance'), dict):
                kwargs['_previous_instance'].update(r)
            new_instance = self._manage_attributes(r, is_new = False, updated = instance, previous = r, updated_list = updated_list)
        else:
            new_instance = self._manage_attributes(instance, is_new = True, updated_list = updated_list)
        ids = [new_instance[k] for k in self.ids]
        # IN some cases the id is tied to a few unique attributes. So after managing attributes we might find that the object did actually exist. 
        # So unless explicitly set or auto_updater in the instance, we should pick from the older instance
        r = self.objects.get(ids)
        if r:
            for k, v in r.iteritems():
                if k not in initial_keys and new_instance.get(k) is None:
                    new_instance[k] = v
        if self.login:
            l = self.login.get(ids, internal = internal)
            a = self.admin_model.get(ids, internal = internal) if hasattr(self, 'admin_model') else {}
            if a or (l and l['role'] != self.name):
                raise ObjectIdError("User Id {} already exists in role {}".format(ids, l.get('role', 'admin')))
        logger.debug("New instance for post is %s", new_instance)
        if any(i is None for i in ids):
            raise ObjectIdError(
                "One or more of the ids {} are not provided in the instance {} in model {}: {}".
                format(self.ids, instance, self.name, ids))
        ro = self.objects.set(ids, **new_instance)
        r = self._jsonify_attributes(ro, internal = internal, do_verify = False)
        self._update_children_refers(ro, updated_list)
        if self.login:
            instance['role'] = self.name
            instance.update(new_instance)
            r.update(
                (_k, _v) for _k, _v in self.login.post( 
                    instance, 
                    internal = internal
                ).iteritems() if _k in LOGIN_ATTRIBUTES
            )
            r.pop('role', None)
        return r
    
    

    def _update_children_refers(self, instance, updated = None):
        for c in self.children:
            cm = self.__class__(c, self.enterprise_id)
            names = filter(lambda x: x[1].get('parent_model', x[0]) == self.name, cm.parent_ids.iteritems())
            for name, pids in names:
                update_dict = {}
                if 'parent_self_map' in pids:
                    for k1, k2 in pids['parent_self_map'].iteritems():
                        if not updated or k1 in updated:
                            update_dict[k2] = instance.get(k1)
                if update_dict:
                    filter_by = cm.from_parent_id_dict(name, instance)
                    logger.info("Updating child %s of %s with dict %s, filter_by: %s", c, self.name, update_dict, filter_by)
                    cm.objects.update_many(update_dict, filter_by = filter_by)
                else:
                    logger.info("Nothing to update in child %s of %s for updated attributes %s", c, self.name, updated)

    def _delete_children_refers(self, instance):
        lm = self.__class__('login', self.enterprise_id)
        for c in self.children:
            cm = self.__class__(c, self.enterprise_id)
            names = filter(lambda x: x[1].get('parent_model', x[0]) == self.name, cm.parent_ids.iteritems())
            for name, pids in names:
                filter_by = cm.from_parent_id_dict(name, instance)
                if cm.parent_ids[name].get('required', True):
                    logger.info("Deleting child %s of %s, filter_by: %s", c, self.name, filter_by)
                    if cm.has_login:
                        i = 1
                        while True:
                            user_ids = cm.list(_as_option = True, _page_size = 100, _page_number = i, _filter_attributes = cm.ids, **filter_by)
                            if not user_ids:
                                break
                            logger.info("Deleting logins for %s which 'has_login', user_ids: %s", c, user_ids)
                            lm.objects.delete_many(filter_by = {"user_id": map(lambda x: x[cm.ids[0]], user_ids)})
                            logger.info("Deleting logins for %s which 'has_login', page_number: %s", c, i)
                            i += 1
                    cm.objects.delete_many(filter_by = filter_by)
                else:
                    update_dict = {}
                    if 'parent_self_map' in pids:
                        for k1, k2 in pids['parent_self_map'].iteritems():
                            update_dict[k2] = None
                    for k1 in pids['self_ids']:
                        update_dict[k1] = None
                    if update_dict:
                        logger.info("Updating orphaned child %s of %s with dict %s, filter_by: %s", c, self.name, update_dict, filter_by)
                        cm.objects.update_many(update_dict, filter_by = filter_by)

    def iadd(self, ids, attribute, value, *args, **kwargs):
        ids = hp.make_list(ids)
        at = self.attributes.get(attribute)
        if at:
            if not any(t in [int, float, db.decimal.Decimal, db.stringlist, db.numberlist] for t in at.atype):
                raise AttributeTypeError(attribute, value, at, self.name, "Attribute {} is not of type number/list to add".format(attribute))
            if any(t in [db.stringlist, db.numberlist] for t in at.atype):
                value = hp.make_list(value)
            if not isinstance(value, (int, float, db.decimal.Decimal, list,tuple,db.stringlist, db.numberlist)):
                raise db.ValueTypeError("Value, {} is not a number or list".format(value))
        elif not isinstance(value, (int, float, list)):
            raise db.ValueTypeError("Value, {} is not a number or list".format(value))
        return self.objects.iadd(ids, attribute, value)

    def get_attributes(self, attribute_name=None, _as_option = False, **params):
        ret = []
        if not attribute_name in Attribute._attributes:
            attribute_name = None
        for a in self.run_filters(((i, self.attributes[i]._stuf) for i in self.attr_seq), 
                attr = Attribute._attributes, filter_by = params, 
                condition_type = params.pop('condition_type', 'AND')
            ):
            ca = self.attributes[a['name']].to_dict()
            if attribute_name:
                ret.append(ca.get(attribute_name))
            else:
                ret.append(ca)
        return ret

    def get_options(self, *attributes, **params):
        attributes = [a for a in attributes if a not in self.ids]
        data = []
        managed_params = {}
        params['_filter_attributes'] = self.ids + hp.make_list(attributes)
        params['_load_total'] = {'total_number': 0}
        if '_as_option' not in params: 
            params['_as_option'] = False
        elif params.get('_as_option'):
            params.pop('_load_total', None)
        for d in self.filter(_managed_params = managed_params, **params):
            data.append(hp.make_single(self.dict_to_ids(d) + [d.get(a) for a in attributes]))
        if managed_params['_as_option']:
            return data
        return {
            'page_number': managed_params['page_number'],
            'page_size': managed_params['page_size'],
            'data': data,
            'total_number': managed_params['load_total']['total_number'],
            'is_first': self.is_first(managed_params['start'], managed_params['limit'], managed_params['load_total']['total_number']),
            'is_last': self.is_last(managed_params['start'], managed_params['limit'], managed_params['load_total']['total_number']),
        }

    def get_unique(self, attribute, **params):
        if params is None: params = {}
        page_size = int(params.get('page_size', 50))
        page_number = int(params.get('page_number', 1))
        start = (page_number - 1)*page_size if '_as_option' not in params else 0
        limit = (page_number)*page_size if '_as_option' not in params else None
        if not params.get('_as_option'):
            load_total = {'total_number': 0}
        else:
            load_total = None
        data = self.objects.list_unique_values(
            attribute, start = start, limit = limit, load_total = load_total
        )
        if params.get('_as_option', False):
            return data.keys()
        return {
            'page_number': page_number,
            'page_size': page_size,
            'data': data,
            'total_number': load_total['total_number'],
            'is_first': self.is_first(start, limit, load_total['total_number']),
            'is_last': self.is_last(start, limit, load_total['total_number']),
        }

    def get_models(self, attribute_name, **params):
        return models(self.enterprise_id, attribute_name, role='admin', **params)


    def pivot(self, attributes = None, 
            _action = 'count', _over = None, _resolution = None, 
            _join = None, _filter_results = None, _output_format = 'nested', 
            _fresh = False, _freshen = True, _allow_other_attributes = False,
            _min = None, _max = None,
            **params
        ):
        """
        attrubutes - the list of attributes that you want to pivot over
        _output_formats are 'nested', "ordered_dict", "table"
        _action can be 'count' (default), 'sum', 'average', 'concat', 'min', 'max', 'unique', 'uniquelist', 'top3', 'top5', 'top10, 'top20', 'object_list', 'unique_object_list', 'any', 'all'
        _over is the attribute which you want to combine into your list
        _fresh - Normally the results of this are cached for 24 hours. If you want fresh results, then set this to true
        _freshen - If you want to change the amount of time you want to cache the results then set this value to a number
        _resolution - required only for number or date/time type attributes, lets you know what resolution you want to accumulate the buckets
        _min - required for number or date/time type for the minimum value of the bucket (if not provided, it will try to calculate)
        _max - required for number or date/time type for the maximum value of the bucket (if not provided, it will try to calculate)
        _allow_other_attributes - Allows you to pivot over other attributes which are not in model, but is allowed only for text/string type of attributes
        """
        attributes = hp.make_list(attributes or [])
        over = _over
        action = _action
        resolution = _resolution
        filter_results = _filter_results
        join = _join
        minimum = _min
        maximum = _max
        if over is None: over = self.ids[0]
        if action not in self.objects._action_functions:
            raise PivotActionError("Action {} is not supported currently under the pivot.".format(action))
        #if not attributes:
        #    raise PivotActionError("Attributes for pivoting is an empty list")
        def do_resolution(resolution):
            if resolution in ([None] + self.objects._resolution_functions.keys()):
                return resolution
            if isinstance(resolution, (int, float, long)):
                return resolution
            if isinstance(resolution, (str, unicode)):
                try:
                    return float(resolution)
                except ValueError:
                    raise PivotAttributeError(
                        """
                        Resolution {} is not of known formats: 
                        Integer or time in seconds, dictionary for each attribute or one of {}
                        """.format(resolution, self.objects._resolution_functions.keys())
                    )
            raise PivotAttributeError(
                """
                Resolution {} is not of known formats: 
                Integer or in time in seconds, dictionary for each attribute or one of {}
                """.format(resolution, self.objects._resolution_functions.keys())
            )
        if resolution is None:
            resolution = {}
        elif isinstance(resolution, dict): 
            resolution = dict((a, do_resolution(v)) for a, v in resolution.iteritems() if a in attributes)
        else:
            resolution = dict((a, do_resolution(resolution)) for a in attributes)
        if minimum is None:
            minimum = {}
        elif isinstance(minimum, dict):
            minimum = {a: hp.to_float(v, False) for a, v in minimum.iteritems() if a in attributes}
        else:
            minimum = {a: hp.to_float(minimum, False) for a in attributes}
        if maximum is None:
            maximum = {}
        elif isinstance(maximum, dict):
            maximum = {a: hp.to_float(v, False) for a, v in maximum.iteritems() if a in attributes}
        else:
            maximum = {a: hp.to_float(maximum, False) for a in attributes}
        parent = None
        if join:
            parent = self.__class__(join, self.enterprise_id, self.role)
            if not parent.name in self.parents:
                raise PivotJoinError("Join on {} is not possible, since it is not a parent of the object {}".format(join, self.name))
        _managed_params = {}
        start, limit, page_number, page_size, internal, _as_option, filter_attributes, filter_by, condition_type, sort_by, reverse, load_total = self._manage_params(_managed_params, **params)
        num_attrs = len(attributes)
        def get_atype(a):
            in_self = True
            if a not in self.attributes:
                if join and a in parent.attributes:
                    in_self = False
                elif _allow_other_attributes:
                    return unicode, True
                else:
                    raise PivotAttributeError("Attribute '{}' not present in model '{}'{}".format(a, self.name, " or '{}'".format(parent.name) if join else ''))
            if in_self: 
                atype = self.attributes[a].atype[0]
            else:
                atype = parent.attributes[a].atype[0]
            return atype, in_self
        for i, a in enumerate(attributes):
            atype, in_self = get_atype(a)
            if not issubclass(atype, (float, int, long, db.date, db.datetime, str, unicode, bool, db.stringlist, db.numberlist, dict, list)):
                raise PivotAttributeError("Attribute {} type ({}) does not allow pivoting".format(a, str(atype)))
            if issubclass(atype, (float, int, long, db.date, db.datetime)) and not issubclass(atype, (bool)):
                if a not in resolution:
                    raise PivotAttributeError("Attribute {} is a quantity/datetime attribute. Resolution must be mentioned for pivoting".format(a))
                if isinstance(resolution[a], (int, float, long)):
                    resolution[a] = self.objects._to_resolution_func(resolution[a], a, atype, self if in_self else parent, filter_by, minimum.get(a), maximum.get(a))
                elif resolution[a] in self.objects._resolution_functions.keys():
                    resolution[a] = self.objects._to_resolution_func(resolution[a], a, atype, self if in_self else parent, filter_by)
                else:
                    raise PivotAttributeError("Resolution {} for attribute {} is not of known formats: number or in time in seconds, dictionary for each attribute or one of {}".format(resolution[a], a, self.objects._resolution_functions.keys()))
            else:
                resolution.pop(a, None)
        if over and over not in [a['name'] for a in self._dict.attributes]:
            if join and over in parent.attributes:
                in_self = False
            else:
                raise PivotAttributeError("Attribute '{}' not present in model '{}'{}".format(over, self.name, " or '{}'".format(parent.name) if join else ''))
        if not _as_option:
            load_total = {'total_number': 0}
        else:
            load_total = None
        data = self.objects.pivot(
            attributes, action = action, over = over, resolution = resolution,
            start = start, limit = limit, load_total = load_total,
            filter_by = filter_by, condition_type = condition_type,
            sort_by = sort_by, reverse = reverse, filter_attributes = filter_attributes,
            filter_results = filter_results, join = join, base_filter = self._filter_permissions(),
            last_updated = _managed_params.get('last_updated'),
            updated_before = _managed_params.get('updated_before'),
            output_format = _output_format, _fresh = _fresh, _freshen = _freshen
        )
        if _as_option:
            return data
        return {
            'page_number': page_number,
            'page_size': page_size,
            'data': data,
            'total_number': load_total['total_number'],
            'is_first': self.is_first(start, limit, load_total['total_number']),
            'is_last': self.is_last(start, limit, load_total['total_number']),
        }
        

    def exists(self, **params):
        return self.objects.exists(**params)

    def count(self, **kwargs):
        if not kwargs:
            return self.objects.count()
        kwargs.pop('_internal', None)
        kwargs.pop('_as_option', None)
        return self.list(_page_size = 1, **kwargs)['total_number']

    def _manage_params(self, _managed_params = None, **params):
        logger.debug("Params in manage_params: %s", params)
        sort_by = params.pop('_sort_by', None) or params.pop('sort_by', None)
        reverse = params.pop('_reverse', None) or params.pop('_sort_reverse', None) or params.pop('sort_reverse', False)
        page_size = params.pop('_page_size', None) or params.pop('page_size', None)
        internal = params.pop('_internal', False)
        _as_option = params.pop('_as_option', False)
        load_total = params.pop('_load_total', None)
        additional_values = params.pop('_additional_values', None)
        if isinstance(page_size, (str, unicode)) and page_size.lower() in ['null', 'none', 'inf', 'infinity']: page_size = None
        if page_size is not None: 
            page_size = int(page_size)
        elif not _as_option:
            page_size = 50
        page_number = int(params.pop('_page_number', None) or params.pop('page_number', 1))
        if page_number == 0:
            raise ParamError("Page number cannot be 0")
        filter_attributes = params.pop('_filter_attributes', None) or params.pop('filter_attributes', None)
        enforce_attributes = params.pop('_enforce_attributes', False)
        quick_view = params.pop('_quick_view', None) or params.pop('quick_view', False)
        if quick_view:
            filter_attributes = self.to_quick_view(quick_view) + hp.make_list(filter_attributes or [])
        if sort_by and sort_by not in self.attributes and sort_by != '__timestamp':
            raise SortByError("Cannot sort by attribute {}".format(sort_by))
        if sort_by and sort_by in self.attributes and any(
                a in (list, db.geolocation, db.stringlist, db.objectlist, db.numberlist, dict) for a in self.attributes[sort_by].atype
            ):
            raise SortByError("Cannot sort by attribute {} of types {}".format(sort_by, self.attributes[sort_by].atype))
        _keyword = params.pop('_keywords', None) or params.pop('_keyword', None) or params.pop('keyword', None) or params.pop('keywords', None)
        if _keyword:
            is_indexed = False
            for a in self.get_attributes('name', indexed = True, type = SEARCHEABLE_TYPES, list_type = [None] + SEARCHEABLE_LIST_TYPES):
                params[a] = '~' + _keyword
                is_indexed = True
            if not is_indexed:
                for a in self.get_attributes('name', type = SEARCHEABLE_TYPES, list_type = [None] + SEARCHEABLE_LIST_TYPES):
                    params[a] = '~' + _keyword
            else:
                for a in self.get_attributes('name', id = True, type = SEARCHEABLE_TYPES, list_type = [None] + SEARCHEABLE_LIST_TYPES):
                    params[a] = '~' + _keyword
            params['condition_type'] = 'OR'
        condition_type = 'AND' if (params.pop('_condition_type', None) or params.pop('condition_type', 'and')).lower() == 'and' else 'OR'
        if isinstance(page_size, (int, long, float)):
            if page_number >= 0:
                start = (page_number - 1) * page_size
                limit = page_number * page_size
            elif page_number < 0:
                start = page_number * page_size
                limit = (page_number + 1) * page_size
        else:
            start = 0
            limit = None
        filter_by = check_filter_by(self.attributes, params)
        last_updated = hp.to_datetime(params.pop('_last_updated', None))
        updated_before = hp.to_datetime(params.pop('_updated_before', None))
        logger.debug("\nfilter_by is:\n {}\n\n".format(filter_by))
        if isinstance(_managed_params, dict):
            _managed_params.update({
                'start': start,
                'limit': limit,
                'page_number': page_number,
                'page_size': page_size,
                'internal': internal,
                '_as_option': _as_option,
                'filter_attributes': filter_attributes,
                'filter_by': filter_by,
                'condition_type': condition_type,
                'sort_by': sort_by,
                'reverse': reverse,
                'load_total': load_total,
                'additional_values': additional_values,
                'last_updated': last_updated,
                'updated_before': updated_before,
                'enforce_attributes': enforce_attributes
            })
        return start, limit, page_number, page_size, internal, _as_option, filter_attributes, filter_by, condition_type, sort_by, reverse, load_total


    def _filter_permissions(self, method = 'read'):
        base_filter = None
        if self.now_permissions:
            if self.role and self.user_id and self.role == self.name:
                base_filter = self.ids_to_dict(self.user_id)
            if not self.now_permissions.get(method + '_access', False):
                if base_filter:
                    logger.info("Base filter for self")
                    return base_filter
                self._raise_not_permitted()
            if self.user_id:
                base_filter = self.check_filter_by(self.attributes, hp.evaluate_args(self.now_permissions.get(method + '_filter', {}), self.user))
                if self.role in self.parents:
                    pa = self.now_permissions.get(method + '_children_only', False)
                    if pa:
                        base_filter.update(self.from_parent_id_dict(self.role, self.user))
                if self.role in self.children:
                    pa = self.now_permissions.get(method + '_parent_only', False)
                    if pa:
                        base_filter.update(self.role_model.to_parent_id_dict(self.name, self.user))
                if self.role == self.name:
                    pa = self.now_permissions.get(method + '_sibling_only', False)
                    if pa:
                        for pm in pa:
                            if pm.get('parent_model'):
                                self.get_parent_ids(pm['parent_model'], input_dict = self.user, output_dict = base_filter)
                                break
                uf = self.now_permissions.get(method + '_user_filter', {})
                if uf and not self.run_filter(self.user, self.role_model.attributes, uf):
                    # will try to make the filter something more generic
                    base_filter['whaaatever'] = 'whaatever'
                pa = self.now_permissions.get(method + '_spouse', []) or []
                for pm in pa:
                    if pm and isinstance(pm, dict) and pm.get('child_model'):
                        child_model = self.__class__(pm.get('child_model'), self.enterprise_id)
                        if self.role in child_model.parents and self.name in child_model.parents:
                            na = hp.make_single(child_model.get_parent_ids(self.name), force = True)
                            pi = child_model.pivot(
                                _over = na, 
                                _action = 'concat',
                                _internal = True,
                                _as_option = True,
                                **child_model.from_parent_id_dict(self.role, self.user)
                            ).get(na)
                            if pi:
                                base_filter.update({hp.make_single(child_model.get_parent_ids(self.name, in_self = False), force = True): pi}) 
        logger.info("For model: %s, role: %s, Base filters are : %s", self.name, self.role, base_filter)
        return base_filter
        

    def filter(self, _managed_params = None, _method = 'read', **params):
        if '_as_option' not in params:
            params['_as_option'] = True
        if params.get('_as_option'):
            params['_load_total'] = None
        logger.debug("In filter function, filter_by: %s", params)
        if _managed_params is None:
            _managed_params = {}
        start, limit, page_number, page_size, internal, _as_option, filter_attributes, filter_by, condition_type, sort_by, reverse, load_total = self._manage_params(_managed_params = _managed_params, **params)
        base_filter = self._filter_permissions(method = _method)
        for d in self.objects.filter(
                filter_by,
                sort_by,
                limit=limit,
                start=start,
                do_function = lambda x: self._jsonify_attributes(
                    x, 
                    filter_attributes, 
                    internal = internal, 
                    additional_values = _managed_params.get('additional_values'),
                    do_verify = False,
                    enforce_attributes = _managed_params.get('enforce_attributes')
                ),
                condition_type=condition_type,
                reverse = reverse,
                load_total = load_total,
                base_filter = base_filter,
                last_updated = _managed_params.get('last_updated'),
                updated_before = _managed_params.get('updated_before')
            ):
            yield d

    def is_first(self, start, limit, total):
        if start==0:
            return True
        if start < -total:
            return True
        return False

    def is_last(self, start, limit, total):
        if limit is None:
            return True
        if limit >= total:
            return True
        if limit == 0:
            return True
        return False

    def yield_list(self, **params):
        """
        Similar to filter, but does it page wise
        """
        logger.debug("Params in list are %s", params)
        load_total = {'total_number': 0}
        params['_load_total'] = load_total
        _params = {}
        params['_as_option'] = False
        if not ('_page_number' in params or 'page_number' in params):
            params['_page_number'] = 1
        while True:
            for d in self.filter(_managed_params = _params, **params):
                yield d
            if self.is_last(_params['start'], _params['limit'], load_total['total_number']):
                break
            params['_page_number'] = params.get('_page_number', params.get('page_number')) + 1
            logger.info("Moving to next page")

    def list(self, **params):
        """
        filter_by is a dict of the format
        {
            '<attribute>': <value>              # Equality, Partial string match
            '<attribute>!': <value>             # Not equal to
            '<attribute>': [<v1>,<v2>,..]       # IN  (list)
            '<attribute>': "<min>,<max>"        # WITHIN RANGE
            '<attribute>': ",<max>"             # <=
            '<attribute>': "<min>,"             # >=
            _sort_by      : <attribute>          # sort by attribute
            _page_size    : <num records>        # default 50, set explicitly to None (null) if you want all the records
            _page_number  : <which page>         # default 1
            _condition_type: <AND/OR>            # default AND
            _filter_attributes: list of 
            attributes of the model that 
            is required. 
            Other attributes are not sent       # default None
        }
        """
        logger.debug("Params in list are %s", params)
        _params = {}
        if '_as_option' not in params:
            params['_as_option'] = False
        if not params.get('_as_option'):
            load_total = {'total_number': 0}
            params['_load_total'] = load_total
        data = list(self.filter(_managed_params = _params, **params))
        if _params['_as_option'] or _params['internal']:
            return data
        return {
            'page_number': _params['page_number'],
            'page_size': _params['page_size'],
            'data': data,
            'total_number': load_total['total_number'],
            'is_first': self.is_first(_params['start'], _params['limit'], load_total['total_number']),
            'is_last': self.is_last(_params['start'], _params['limit'], load_total['total_number'])
        }


    def update_model(self, json_data):
        if json_data.get('delete_all', False):
            for m in self.attached_matchers:
                logger.info("deleting matcher %s from model %s in enterprise %s", m.name, self.name, self.enterprise_id)
                m.drop(True)
                if m.name in json_data.get('attached_matchers', []):
                    json_data['attached_matchers'].remove(m.name)
            try:
                self.delete_many()
                self.objects.clear()
            except db.psycopg2.ProgrammingError as e:
                hp.print_error(e)
                logger.warning("Maybe the table %s is already deleted?", self.name)
            if self.login:
                self.login.delete_many(filters = {'role': self.name})
        else:
            ids = filter(lambda x: x.get('id', False), json_data['attributes'])
            old_ids = filter(lambda x: x.get('id', False), self._dict['attributes'])
            if len(ids) != len(old_ids) or any(any(str(ids[i].get(a) or 'text') != str(old_ids[i].get(a)) for a in ['name', 'type']) for i in range(len(ids))):
                logger.error("Old_ids: %s", map(lambda x: (x.get('name'), x.get('type')), old_ids))
                logger.error("New_ids: %s", map(lambda x: (x.get('name'), x.get('type')), ids))
                raise UpdateIdError(
                        """
                        Cannot change ids or id types {} --> {} in the model {} without deleting all the data. 
                        Set delete_all = True to do this
                        """.format(
                            map(lambda x: (x.get('name', 'no name'), x.get('type')), old_ids),
                            map(lambda x: (x.get('name', 'no name'), x.get('type')), ids),
                            self.name
                    )
                )
            uniques = map(lambda x: x['name'], filter(lambda x: x.get('unique',False), json_data['attributes']))
            old_uniques = map(lambda x: x['name'], filter(lambda x: x.get('unique',False), self._dict['attributes']))
            if any(u not in old_uniques for u in uniques):
                raise UpdateUniqueError(
                    "Cannot change unique attributes {}-->{} in the model {} without deleting all the data. Set delete_all = True to do this.".format(
                        old_uniques, uniques, self.name
                    )
                )
            reqs = map(lambda x: x['name'], filter(lambda x: x.get('required',False), json_data['attributes']))
            old_reqs = map(lambda x: x['name'], filter(lambda x: x.get('required', False), self._dict['attributes']))
            if any(u not in old_reqs for u in reqs):
                raise UpdateRequiredError(
                    "Cannot change required {}-->{} attributes in the model {} without deleting all the data. Set delete_all = True to do this.".format(
                        reqs, old_reqs, self.name
                    )
                )
            data_type = json_data.get('data_type', 'object_concurrency_large')
            if data_type != self.data_type:
                raise UpdateCoreError("Cannot alter model {} data_type without deleting all the data. Set delete_all = True to do this.".format(self.name))
            if self.model_type in ['customer', 'product', 'interaction', 'connection', 'store']:
                other_model = hp.make_single(models(self.enterprise_id, attribute_name = 'name', model_type = self.model_type, _no_errors = True), force = True)
                if other_model and other_model != self.name:
                    raise UpdateCoreError(
                        "Model of type {} with name {} already exists. Only one model of this type is permitted for enterprise {}".format(
                            self.model_type, other_model, self.enterprise_id
                        )
                    )
        for k in [
                'title', 'plural', 'has_login', 'model_type', 'data_type',
                'timezone', 'expand_attributes', 'allow_anonymous_view'
        ]:
            v = json_data.get(k, self._dict[k])
            self._dict[k] = v
            setattr(self, k, v)
        for k, d in [
                ('parents', []),
                ('children', []),
                ('attributes', []),
                ('permissions', []),
                ('attached_matchers', []),
                ('quick_views', {}),
                ('preferences', {}),
                ('actions', {}),
                ('warehouser', []),
                ('accumulator', []),
            ]:
            getattr(self, '_update_' + k)(json_data.get(k, d), forced=True)
        if self.has_login:
            self.login_errors()
        self._set_objects()
        with open(self.filepath, 'wb') as fp:
            SERIALIZER.dump(
                self.to_store(), fp, indent=4,
                encoding="UTF-8",
            )
            fp.truncate()
            
    def _update_quick_views(self, quick_views, forced=False):
        self.quick_views = {} if forced else getattr(self, 'quick_views', {})
        def_qv = self.ids + self.names
        if self.images:
            def_qv.append(self.images[0])
        def_qv = list(set(def_qv))
        for v, vs in quick_views.iteritems():
            for v1 in vs:
                if v1 not in self.attributes:
                    raise UnknownAttribute("Unknown attribute {} in quick_view {}".format(v1, v))
        self.quick_views['default'] = quick_views.pop('default', def_qv)
        self.quick_views['download'] = quick_views.pop('download', sorted([_ for _ in self.attributes if any(issubclass(a_, (str, unicode, int, float, db.stringlist, db.numberlist, bool, db.date, db.dtime, db.datetime, db.geolocation, db.NoneType)) for a_ in self.attributes[_]._stuf.atype)]))
        self.quick_views['cached'] = quick_views.pop('cached', [])
        self.quick_views.update(quick_views)

    def _update_preferences(self, preferences, forced=False):
        if not self.has_login:
            self.preferences = {}
            return self.preferences
        self.preferences = {} if forced else getattr(self, 'preferences', {})
        self.preferences.update(preferences)
        return self.preferences

    def _update_actions(self, actions, forced=False):
        self.actions = {} if forced else getattr(self, 'actions', {})
        self.actions.update(actions)
        nks = [k_ for k_ in self.actions if k_ not in ['read', 'write', 'delete', 'post', 'put', 'delete', 'patch', 'update', 'list', 'refresh', 'get', 'exists', 'pivot'] and not k_.startswith('_')]
        for k in nks: self.actions.pop(k)
        return self.actions

    def _update_self_value(self, attr_name, value, forced=False, default = None):
        return setattr(self, attr_name, value or default)

    def _update_warehouser(self, warehouser, forced = False):
        return self._update_self_value('warehouser', warehouser, forced = forced, default = [])

    def _update_accumulator(self, accumulator, forced = False):
        return self._update_self_value('accumulator', accumulator, forced = forced, default = [])

    def _update_attributes(self, attributes, forced=False):
        """
        TODO: Validate attributes
        """
        self.attr_seq = []
        self._dict.attributes = []
        self.attributes = stuf() if forced else getattr(self, 'attributes', stuf())
        self.names = [] if forced else getattr(self, 'names', [])
        self.images = [] if forced else getattr(self, 'images', [])
        self.ids = [] if forced else getattr(self, 'ids', [])
        if not self.ids: forced = True
        for i, a in enumerate(attributes):
            at = Attribute(self, a, forced=forced)
            self.attributes[at.name] = at
            if not at.name in self.attr_seq:
                self.attr_seq.append(at.name)
                self._dict.attributes.append(at.to_dict())
        logger.debug("After update attributes:%s:%s", self.name, self.ids)
        if not self.ids:
            raise ModelIdError(
                """At least one attribute of model {}  needs to be of type id
                attributes: {}
                """.format(self.name, attributes))
        if not getattr(self, 'names', None):
            self.names = list(self.ids)
        self.atypes = {
            n: a.atype[0] for n, a in self.attributes.iteritems()
        }
        self.id_atypes = [
            a.atype[0] for n, a in self.attributes.iteritems() if a.id
        ]
        self.uniques = [n for n, a in self.attributes.iteritems() if a.unique]
        self.unique_atypes = [
            a.atype[0] for n, a in self.attributes.iteritems() if a.unique
        ]
        self.emails = [
            n for n, a in self.attributes.iteritems() if a.type == 'email' or a.list_type == 'email'
        ]
        self.phone_numbers = [
            n for n, a in self.attributes.iteritems()
            if a.type in ('phone_number', 'mobile_number') or a.list_type in ('phone_number', 'mobile_number')
        ]
        self.mobile_numbers = [
            n for n, a in self.attributes.iteritems()
            if a.type in ('mobile_number',)
        ]
        self.push_tokens = [
            n for n, a in self.attributes.iteritems()
            if a.type in ('push_token',) or a.list_type in ('push_token',)
        ]
        if self.has_login and 'push_token' not in self.push_tokens:
            self.push_tokens.append('push_token')
        self.times = [
            n for n, a in self.attributes.iteritems()
            if a.type in ('date', 'datetime', 'timestamp') or a.list_type in ('date', 'datetime', 'timestamp')
        ]
        self.locations = [
            n for n, a in self.attributes.iteritems()
            if a.type in ('geolocation', ) or a.list_type in ('geolocation', )
        ]
 
        self.title_to_name = stuf({a.title: a.name for a in self.attributes.itervalues()})
        self.name_to_title = stuf({a.name: a.title for a in self.attributes.itervalues()})
        self.name_to_ui_element = stuf({a.name: a.ui_element for a in self.attributes.itervalues()})
        self.name_to_type = stuf({a.name: a.type for a in self.attributes.itervalues()})
        self._default_weights = dict( (n, float(a.weight)) for n, a in self.attributes.iteritems() if a.weight is not None)
        self.weights = MatcherDict(
            self.name + '___weights_',
            key_types = [unicode],
            key_names = ['attribute'],
            iterable = self._default_weights.iteritems(),
            DB_NAME = self.enterprise_id
        )
        for parent_called, atts in self.parent_ids.iteritems():
            for i, t in enumerate(atts['ids_present']):
                if not t:
                    a = atts['self_ids'][i]
                    ap = atts['parent_ids'][i]
                    parent_model = atts.get('parent_model', parent_called)
                    att_config = {
                        'name': a,
                        'refers': 'parents.{}.{}'.format(parent_model, ap),
                        'relation_called': parent_called,
                    }
                    self.attributes[a] = Attribute(self, att_config)

    def _json_children(self, instance, child_name):
        ids = [instance[i] for i in self.ids]
        return '/objects/{}?{}'.format(child_name,
                                       '&'.join('{}={}'.format(a, instance[a])
                                                for a in self.ids))

    def _get_children(self, instance, child_name, parent_called = None, attribute = None, *args, **kwargs):
        m = Model(child_name, self.enterprise_id, self.role, user_id = self.user_id)
        parent_called = parent_called or self.name
        parent_ids = m.parent_ids[parent_called]['self_ids']
        params = {'_page_size': None, '_as_option': True, '_filter_attributes': attribute}
        params.update((n, instance.get(i)) for n, i in zip(parent_ids, self.ids))
        return m.list(**params)

    def _get_parents(self, instance, parent_name, attribute = None, do_raise = False, *args, **kwargs):
        model = self.parents[parent_name]
        ids = []
        for i in self.parent_ids[parent_name]['self_ids']:
            if instance.get(i) is None:
                if do_raise or self.attributes[i]._stuf.required:
                    raise RelationParentIdError(
                        """No key called {} present in instance {} of model {}.
                        Corresponding to attribute {} for parent {}.
                        The ids corresponding to parents are {}.
                        """.format(
                            i, instance, self.name, 
                            self.parent_ids[parent_name]['parent_ids'],
                            parent_name,
                            self.parent_ids[parent_name]['self_ids']
                        )
                    )
                else:
                    return None if attribute else {}
            ids.append(instance.get(i))        
        if attribute:
            return model.get(ids, {}).get(attribute)
        return model.get(ids, {})


    def _set_parent(self, parent_model, parent_name = None):
        if parent_name is None: parent_name = parent_model
        if parent_model == self.name:
            raise RelationParentIdError("Cannot have self parenting in model: %s", parent_model)
        try:
            m = Model(
                parent_model, self.enterprise_id, is_template=self.is_template
            )
        except ModelNotFound:
            logger.warning("Model %s not found. Maybe delete out of order, or created out of order", parent_model)
            return
        self.parents[parent_name] = m
        self.parent_ids[parent_name] = {
            'self_ids': [parent_model + '_' + i for i in m.ids],
            'parent_ids': m.ids,
            'ids_present': [False for i in m.ids],
            'parent_attribute_features': [m.attributes[i].to_dict() for i in m.ids],
            'parent_model': parent_model,
            'required': True
        }
        if parent_model == parent_name:
            if parent_model not in self._dict.parents:
                self._dict.parents.append(parent_model)
        elif [parent_model, parent_name] not in self._dict.parents:
                self._dict.parents.append([parent_model, parent_name])
        if m._set_child(self.name, parent_name if parent_name != parent_model else None) and m.name not in CORE_MODELS:
            m.update_model(m.to_dict())

    def _update_parents(self, new_parents, *args, **kwargs):
        self._dict.parents = []
        self.parents = stuf()
        self.parent_ids = stuf()
        for p in new_parents:
            if not isinstance(p, (tuple, list)):
                p = (p, None)
            self._set_parent(*p)

    def _set_child(self, c, p = None):
        self.children[c] = None
        if p is None:
            if [c, p] not in self._dict.children:
                self._dict.children.append([c, p])
                return True
        elif c not in self._dict.children:
            self._dict.children.append(c)
            return True
        return False

    def _update_children(self, new_children, *args, **kwargs):
        self._dict.children = []
        self.children = stuf()
        for c in new_children:
            if not isinstance(c, (tuple, list)):
                c = (c, None)
            self._set_child(*c)

    def _update_attached_matchers(self, matchers, *args, **kwargs):
        self.attached_matchers = []
        self._dict.attached_matchers = []
        for m in matchers:
            m = Matcher(m, key_names = self.ids, key_types = self.id_atypes, DB_NAME = self.enterprise_id)
            self.attached_matchers.append(m)
            self._dict.attached_matchers.append(m.name)

    def _set_permission(self, p, forced = False):
        if self.is_template or self.name == 'enterprise' or self.enterprise_id == 'core':
            return
        r = p.get('role', None)
        if not r:
            raise PermissionValueError(
                "Malformed permission's record: {} for model {}. Requires 'role'".
                format(p, self.name))
        if r not in CORE_MODELS:
            pass
            #try:
            #    role_model = Model(self.enterprise_id, role)
            #except ModelNotFound:
            #    raise ModelNotFound(self.filepath, "Model specified in permissions file for role {} in model '{}' not found".format(role, self.name))
            #if not role_model.has_login:
            #    raise PermissionValueError("Role '{}' specified for permissions to model '{}' is not found".format(role, self.name))
        m = p.get('model', None)
        if not m:
            m = self.name
            p['model'] = m
        if m != self.name:
            return False
        if forced:
            try:
                self.permission_model.update((r, m), p)
            except (db.PersistKeyError, KeyError) as e:
                self.permission_model.post(p)
        self.permissions[r] = p
        self._dict.permissions = self.permissions.values()

    def _update_permissions(self, new_permissions=None, forced = False, *args, **kwargs):
        if self.is_template or self.name == 'enterprise' or self.enterprise_id == 'core':
            return
        new_permissions = new_permissions or getattr(self._dict, 'permissions',
                                                     [])
        self._dict.permissions = []
        self.permissions = {}
        for p in new_permissions:
            self._set_permission(p, forced)
            # TODO: check if the role is a model
        if self.has_login and forced:
            self._set_permission({
                    'model': 'enterprise',
                    'role': self.name,
                    'read_access': True,
                    'write_access': False,
                    'delete_access': False,
                }, forced = forced
            )
            #old_permissions.post({
            #    'model': 'perspective',
            #    'role': self.name,
            #    'read_access': True,
            #    'write_access': False,
            #    'delete_access': False,
            #})
            self._set_permission({
                'model': 'perspective',
                'role': self.name,
                'read_access': True,
                'write_access': False,
                'delete_access': False,
            })

    def to_dict(self):
        self._to_dict_from_self()
        return dict(self._dict)

    def to_quick_view(self, quick_view = 'default'):
        if not quick_view:
            return self.attributes.keys()
        return self.quick_views.get(quick_view, self.quick_views['default'])

    def to_store(self, attr_func='to_store'):
        d = self.to_dict()
        d['attributes'] = [
            getattr(self.attributes[a], attr_func)() for a in self.attr_seq
        ]
        return d

    def dict_to_ids(self, input_dict, default = None, pop = False):
        r =  [input_dict.pop(i, default) if pop else input_dict.get(i, default) for i in self.ids]
        for i, n in zip(r, self.ids):
            if isinstance(i,type) and issubclass(i, Exception):
                raise i("Id {} not found for Model {} in dict {}".format(n, self.name, input_dict))
        return r

    def ids_to_dict(self, id_list, dict_to_update = None):
        id_list = hp.make_list(id_list)
        if not dict_to_update:
            dict_to_update = {}
        dict_to_update.update((a, v) for a, v in zip(self.ids, id_list))
        return dict_to_update

    def get_parent_by_foreign_key(self, attribute_name, name_only = False):
        for p in self.parents:
            if attribute_name in self.parent_ids[p]['self_ids']:
                return p if name_only else self.parents[p]
        return None

    def get_parent_ids(self, parent_name, in_self = True, input_dict = None, default = None, output_dict = False):
        rids = self.parent_ids[parent_name]['self_ids' if in_self else 'parent_ids']
        if input_dict is None:
            return rids
        r = [input_dict.get(i, default) for i in rids]
        for i, n in zip(r, rids):
            if isinstance(i,type) and issubclass(i, Exception):
                raise i("Id {} not found for Model {} in dict {}".format(n, parent_name, input_dict))
        if output_dict or isinstance(output_dict, dict):
            return self.parent_ids_to_dict(parent_name, r, dict_to_update = output_dict if isinstance(output_dict, dict) else None)
        return r

    def to_parent_id_dict(self, parent_name, instance, r = None):
        try:
            sids = self.parent_ids[parent_name]['self_ids']
            pids = self.parent_ids[parent_name]['parent_ids']
        except KeyError as e:
            raise RelationParentIdError("No parent called {} exists for model {}: ({})".format(parent_name, self.name, e))
        if not isinstance(r, dict): r = {}
        for s, p in zip(sids, pids):
            r[p] = instance.get(s)
        return r

    def from_parent_id_dict(self, parent_name, instance, r = None):
        try:
            sids = self.parent_ids[parent_name]['self_ids']
            pids = self.parent_ids[parent_name]['parent_ids']
        except KeyError as e:
            raise RelationParentIdError("No parent called {} exists for model {}: ({})".format(parent_name, self.name, e))
        logger.debug("self_ids: %s", sids)
        logger.debug("self_parent_ids: %s", pids)
        if not isinstance(r, dict): r = {}
        for s, p in zip(sids, pids):
            r[s] = instance.get(p)
        return r


    def parent_ids_to_dict(self, parent_name, id_list, dict_to_update = None, in_self = True):
        id_list = hp.make_list(id_list)
        if not dict_to_update:
            dict_to_update = {}
        dict_to_update.update((a, v) for a, v in zip(self.parent_ids[parent_name]['self_ids' if in_self else 'parent_ids'], id_list))
        return dict_to_update

    def parent_dict_to_ids(self, parent_name, instance, in_self = True):
        return self.get_parent_ids(parent_name, in_self = in_self, input_dict = instance)

    def foreign_key_to_parent(self, attribute_name, attribute_value = None, filter_attributes = None):
        for p, v in self.parent_ids.iteritems():
            if attribute_name in v['self_ids']:
                join_model = self.__class__(v.get('parent_model', p), self.enterprise_id, self.role)
                if attribute_value is None:
                    return join_model
                return join_model.get(attribute_value, attributes = filter_attributes)
        return None

    @contextmanager
    def _transaction(self):
        if getattr(self.objects, '_db') and getattr(self.objects._db, '_transaction'):
            with self.objects._db._transaction() as v:
                yield v
        elif getattr(self.objects, '_transaction'):
            with self.objects._transaction() as v:
                yield v
        else:
            try:
                yield None
            except Exception as e:
                raise
            finally:
                logger.error("Error in transaction")


    def get_from_parent(self, instance, parent_name, parent_attribute = None, **kwargs):
        parent_ids = self.parent_ids[parent_name]['self_ids']
        ids = [instance.get(i) for i in parent_ids]
        parent_dict = self.parents[parent_name].get(ids, **kwargs)
        if parent_attribute is None:
            return parent_dict
        return parent_dict.get(parent_attribute, default)

    def accumulate(self, resolution = 3600, start_time = None, **kwargs):
        smo = Model('statistic', self.enterprise_id)
        am = Model('accumulator', self.enterprise_id)
        start_time = hp.to_datetime(start_time, tz = self.timezone)
        accumulator = self.accumulator + am.list(_as_option = True, model = self.name)
        for s in accumulator:
            sm = smo
            if s.get('statistic_model'):
                statistic_model = s.get('statistic_model')
                try:
                    sm = Model(statistic_model, self.enterprise_id)
                except ModelNotFound:
                    smd = Model('statistic', self.enterprise_id).to_dict()
                    smd['attributes'][map(lambda x: x['name'] == 'statistic_value', smd['attributes']).index(True)].pop('atype', False)
                    post(statistic_model, self.enterprise_id, 'admin', smd)
                    sm = Model(statistic_model, self.enterprise_id)
            statistic_title = s.get('statistic_title')
            if not statistic_title:
                continue
            if kwargs.get('statistic_titles') and statistic_title not in hp.make_list(kwargs.get('statistic_titles', [])):
                continue
            attributes = hp.make_list(s.get('attributes', []))
            filters = s.get('filters', {})
            title_map = s.get('title_map', {})
            timestamp_attribute = s.get('timestamp_attribute')
            sst = hp.to_datetime(s.get('start_time'), tz = self.timezone)
            st = None
            def make_sort_by(t1 = None, t2 = None):
                return {
                    '_sort_by': timestamp_attribute or '__timestamp',
                    '_sort_reverse': True
                }
            def make_last_updated(t1, t2 = None):
                if timestamp_attribute:
                    return {
                        timestamp_attribute: '{},'.format(t1)
                    }
                return {
                    '_last_updated': t1
                }
            def make_updated_before(t1=None, t2 = None):
                if timestamp_attribute:
                    return {
                        timestamp_attribute: ',{}'.format(t2)
                    }
                return {
                    '_updated_before': t2
                }
            def make_range(t1,t2):
                if timestamp_attribute:
                    return {
                        timestamp_attribute: '{},{}'.format(t1, t2)
                    }
                return {
                    '_last_updated': t1,
                    '_updated_before': t2
                }
            def join_up(r, fl, t1 = None, t2 = None):
                r = r or {}
                for f in hp.make_list(fl):
                    r.update(f(t1, t2))
                return r
            fingerprint = hp.make_uuid3(
                '--'.join(u'{}.{}'.format(k_, v_) for k_, v_ in sorted(filters.iteritems())) + statistic_title + self.name
            )
            while True:
                st = st or hp.make_single(
                    sm.list(
                        _as_option = True, 
                        _page_size = 1, 
                        _sort_by = 'end_timestamp', 
                        _sort_reverse = True,
                        fingerprint = fingerprint,
                    ), default = {}
                ).get('end_timestamp') or sst or start_time or hp.make_single(
                    self.list(
                        _as_option = True, 
                        _page_size = 1, 
                        **join_up(None, [make_sort_by, make_updated_before], None, hp.now())
                    ), default = {}
                ).get(timestamp_attribute or '__timestamp')
                st = hp.to_datetime(st, tz = self.timezone)
                if not st:
                    logger.warn("There is no data in this model %s, will not accumulate", self.name)
                    break
                res = int(s.get('resolution', resolution))
                ts = int(time.mktime(st.timetuple()))
                rem = res - (ts % res)
                et = st + hp.timedelta(seconds = rem)
                if et > hp.now():
                    logger.info("Finished collecting all the statistics")
                    break
                logger.info("Finding statistic %s range %s -- %s", s['statistic_title'], st, et)
                action = s.get('action', 'count')
                over = s.get('over', self.ids[0])
                at = self.attributes[over].atype
                try:
                    resp = self.pivot(
                        attributes, 
                        _action = action, 
                        _over = over,
                        _fresh = True,
                        _as_option = True, 
                        _output_format = 'ordered_dict',
                        **join_up(filters, [make_range], st, et)
                    )
                    if action in ['sum', 'average', 'count', 'unique','top3', 'top5', 'top10', 'top20', 'bottom3', 'bottom5', 'bottom10', 'bottom20' ]:
                        value_type = 'number'
                        default_value = 0
                    elif action in ['all', 'any']:
                        value_type = 'bool'
                    elif action in ['concat', 'uniquelist', 'unique_list', 'object_list', 'unique_object_list']:
                        value_type = 'list'
                    elif any(issubclass(at_, (db.date, db.datetime, db.dtime)) for at_ in at):
                        value_type = 'datetime'
                    elif any(issubclass(at_, (float, long, int)) for at_ in at):
                        value_type = 'number'
                    else:
                        value_type = None
                    found = False
                    for k, v in resp.iteritems():
                        f = {k__:v__ for k__, v__ in filters.iteritems() if not k__.startswith('_') and k__ != timestamp_attribute}
                        p = {}
                        to_p = {}
                        for i, a in enumerate(attributes):
                            p[a] = k[i]
                            to_p['pivot_attribute_{}'.format(i)] = k[i]
                        to_p.update({
                            "statistic_title": s['statistic_title'],
                            "start_timestamp": st,
                            "end_timestamp": et,
                            "filters": f,
                            "pivot_attributes": p,
                            "model_name": self.name,
                            "model_title": self.title,
                            "filter_titles": {title_map.get(k_, hp.make_title(k_)): v_ for k_, v_ in f.iteritems()},
                            "pivot_attribute_titles": {title_map.get(k_, hp.make_title(k_)): v_ for k_, v_ in p.iteritems()},
                            "fingerprint": fingerprint,
                            "value_type": value_type,
                            "statistic_value": v
                        });
                        sm.post(to_p)
                        found = True
                    if not found:
                        f = {k__:v__ for k__, v__ in filters.iteritems() if not k__.startswith('_') and k__ != timestamp_attribute}
                        sm.post({
                            "statistic_title": s['statistic_title'],
                            "start_timestamp": st,
                            "end_timestamp": et,
                            "filters": f,
                            "model_name": self.name,
                            "model_title": self.title,
                            "filter_titles": {},
                            "fingerprint": fingerprint,
                            "value_type": value_type,
                            "statistic_value": None,
                            "is_null": True
                        })
                except Exception as e:
                    hp.print_error(e)
                st = et + hp.timedelta(seconds = 0.001)

    def _warehouse(self, filters, params, updated_before = None, last_updated = None, wm = None, timestamp_attr = None, child_timestamp_attr = None):
        wm = wm or Model('warehouse', self.enterprise_id)
        timestamp_attribute = timestamp_attr or '__timestamp'
        # child_timestamp_attr = child_timestamp_attr or {}
        def join_up(filters, updated_before):
            filters = filters or {}
            if timestamp_attribute == '__timestamp':
                return {
                    'filter_by': filters
                }
            filters[timestamp_attribute] = (last_updated, updated_before)
            return {
                'filter_by': filters,
                'sort_by': timestamp_attribute
            }
        with self._transaction():
            who = []
            ids = []
            fs = {a: [] for a in self.ids}
            for r in self.objects.filter(**join_up(filters, updated_before)):
                id_ = self.dict_to_ids(r)
                if not id_:
                    continue
                who.append(r)
                ids.append(self.dict_to_ids(r))
                for a in self.ids:
                    fs[a].append(r[a])
            if not ids:
                logger.info("No objects left in %s within the filters provided %s", self.name, filters)
                return False
            try:
                for c in self.children:
                    cm = self.__class__(c, self.enterprise_id)
                    names = filter(lambda x: x[1].get('parent_model', x[0]) == self.name, cm.parent_ids.iteritems())
                    for name, pids in names:
                        if cm.parent_ids[name].get('required', True):
                            # params_copy = hp.copyof(params)
                            params_copy = {'start_date': params.get("start_date"), 'end_date': params.get("end_date")}
                            params_copy['description'] = '{}->{}'.format(params['description'], cm.name)
                            cm._warehouse(
                                cm.from_parent_id_dict(self.name, fs), 
                                params_copy, 
                                wm = wm,
                                timestamp_attr=None
                                # timestamp_attr = child_timestamp_attr.get(
                                #     name, 
                                #     hp.make_single(cm.times, force = True) if timestamp_attr != '__timestamp' else None
                                # ), 
                                # child_timestamp_attr = child_timestamp_attr
                            )
                params['model'] = self.name
                params['data'] = who
                params['timestamp_attribute'] = timestamp_attr
                # params['child_timestamp_attribute'] = child_timestamp_attr
                logger.info("Posting params to warehouse for model %s: %s", self.name, params)
                wm.post(params)
            except Exception as e:
                logger.error("Error while warehousing data")
                hp.print_error(e)
                raise
            else:
                for id_ in ids:
                    self.delete(id_)
        return True

    def warehouse(self, start_date = None, end_date = None, keep_days = 60, resolution = 24*3600, timestamp_attr = None, child_timestamp_attr = None):
        c = self.count()
        if not c:
            logger.warn("There is no data in this model %s, will not warehouse", self.name)
            return
        wm = Model('warehouse', self.enterprise_id)
        for w in self.warehouser:
            warehouse_model = w.get('warehouse_model', 'warehouse')
            try:
                wm = Model(warehouse_model, self.enterprise_id)
            except ModelNotFound:
                post(warehouse_model, self.enterprise_id, 'admin', Model('warehouse', self.enterprise_id).to_dict())
                wm = Model(warehouse_model, self.enterprise_id)
            ta = w.get('timestamp_attribute' or timestamp_attr)
            if ta and ta not in self.attr_seq:
                raise AttributeNameError("No attribute called %s in model %s", ta,  self.name)
            if ta and not self.attributes[ta].atype[0] in (db.date, db.datetime):
                raise AttributeNameError("Attribute %s in model %s is not a date or datetime attribute", ta,  self.name)
            
            sd = hp.to_datetime(start_date or hp.make_single(self.list(_as_option = True, _page_size = 1, _sort_by = ta, default = {})).get(ta), tz = self.timezone)
            ed = hp.to_datetime(end_date or hp.now(tz = self.timezone) - hp.timedelta(days = w.get('keep_days', keep_days)), tz = self.timezone)
            sd1 = sd
            res = int(w.get('resolution', resolution))
            ts = int(time.mktime(sd1.timetuple()))
            rem = res - (ts % res)
            ed1 = sd1 + hp.timedelta(seconds = rem)
            if sd1 >= ed:
                logger.info("No records before %s to warehouse", ed)
                continue
            while True:
                logger.info("Warehousing %s between %s - %s for filters %s", self.name, sd1, ed1, w.get('filters', {}))
                params = {'description': w.get('description'), 'start_date': sd1, 'end_date': ed1, 'filters': w.get('filters', {})}
                filters = w.get('filters', {})
                self._warehouse(
                    filters, 
                    params, 
                    ed1, 
                    sd1,
                    wm, 
                    timestamp_attr = w.get('timestamp_attribute', timestamp_attr), 
                    child_timestamp_attr = w.get('child_timestamp_attribute', child_timestamp_attr)
                )
                sd1 = ed1 + hp.timedelta(seconds = 0.000001)
                ed1 = min(sd1 + hp.timedelta(seconds = res), ed)
                if sd1 >= ed:
                    break

    def restore(self, start_date = None, end_date = None, description = None, warehouse_model=None, **filters):
        wm = Model('warehouse', self.enterprise_id)
        start_date = hp.to_datetime(start_date or '1900-01-01', tz = self.timezone)
        end_date = hp.to_datetime(end_date or hp.now(tz = self.timezone), tz = self.timezone)
        d = {
            "model": self.name
        }
        if description:
            d['description'] = description


        def _restore(warehouse_model, **d):
            try:
                wm = Model(warehouse_model, self.enterprise_id)
            except ModelNotFound:
                post(warehouse_model, self.enterprise_id, 'admin', Model('warehouse', self.enterprise_id).to_dict())
                wm = Model(warehouse_model, self.enterprise_id)

            pids_ = []
            for r in wm.filter(end_date = (start_date, None), start_date = (None, end_date), **d):
                with self._transaction():
                    d = r.get('data', [])
                    timestamp_attr = r.get('timestamp_attribute') or '__timestamp'
                    dl = []
                    for d1 in d:
                        ts=None
                        if timestamp_attr in d1:
                            ts = hp.to_datetime(d1[timestamp_attr], tz = self.timezone) 
                        if not ts or (ts >= start_date and ts < end_date):
                            if filters and not run_filter(d1, {}, filters):
                                dl.append(d1)
                                continue
                            try:
                                self.objects.set(self.dict_to_ids(d1), **d1)
                            except Exception as e:
                                logger.error("Error while restoring the data %s", d1)
                                hp.print_error(e)
                                dl.append(d1)
                            pids_.append((r, self.dict_to_ids(d1)))
                        else:
                            dl.append(d1)
                    
                    if not dl:
                        wm.delete(wm.dict_to_ids(r))
                    else:
                        wm.patch(wm.dict_to_ids(r), {'data': dl})
            
            for c in self.children:
                cm = self.__class__(c, self.enterprise_id)
                names = filter(lambda x: x[1].get('parent_model', x[0]) == self.name, cm.parent_ids.iteritems())
                for name, pids in names:
                    if cm.parent_ids[name].get('required', True):
                        for r, d1 in pids_:
                            ds = '{}->{}'.format(r['description'], cm.name)
                            p = cm.parent_ids_to_dict(self.name, d1)
                            cm.restore(start_date = r['start_date'], end_date = r['end_date'], description = None, warehouse_model=wm.name, **p)
        
        if warehouse_model:
            _restore(warehouse_model, **d)
        else:
            for w in self.warehouser:
                warehouse_model = w.get('warehouse_model', 'warehouse')
                if description and description != w.get('description'):
                    continue

                _restore(warehouse_model, **d)

            
if __name__ == '__main__':
    import time
    from datetime import datetime, timedelta

    enterprise = Model('enterprise', 'core', 'admin')
    enterprise.delete('test1', None)
    e = enterprise.post({
        'name': 'test1',
        'email': 'test1@test1.com',
        'phone_number': '9980838165',
        'enterprise_logo': 'sl;fkd',
        'credits_left': 0
    })
    print "Enterprise Time", get_enterprise_timestamp('test1')
    print enterprise.iadd('test1', 'credits_left', -10)
    print enterprise.iupdate('test1', {'credits_left': 10})
    print enterprise.get("test1"), "------------------------"
    customer = {
        'data_type': 'object_concurrency_large',
        "name": "customer",
        "attributes": [{
            "name": "name",
            "type": "name",
            "unique": True
        }, {
            "name": "city",
            "required": True
        }, {
            "name":
            "region",
            "options": ['Asia', 'Pacific', 'North America', 'South America']
        }, {
            "name": "email",
            "unique": True,
            "required": True,
            'type': 'email'
        }, {
            "name": "uid",
            "id": True,
            "default": {
                "function": "make_uuid3",
                "args": ["email"]
            },
            "auto_updater": True
        }, {
            "name": "other_city",
            "type": "true_false"
        }, {
            "name": "abc",
            "type": "number"
        }, {
            "name": "colour",
            "type": "rgb"
        }, {
            "name": "a_date",
            "type": "date"
        }, {
            "name": "true_false",
            "type": "bool"
        }, {
            "name": "my_list",
            "type": "list"
        }],
    }

    product = {
        'data_type':
        'object_concurrency_large',
        "name":
        "product",
        "attributes": [{
            "name": "name",
            "default": {
                "function": "normalize_join",
                "args": ["title"]
            },
            "id": True,
            "auto_updater": True
        }, {
            "name": "title",
            "required": True
        }, {
            "name": "recurring",
            "type": "true_false"
        }, {
            "name": "abc",
            "type": "number"
        }, {
            "name": "created_time",
            "type": "timestamp",
            "indexed": True
        }, {
            "name": "my_list",
            "type": "list"
        },
      ],
    }

    order = {
        'data_type': 'object_concurrency_large',
        'name': 'invoice',
        'attributes': [
            {
                "name": "order_id",
                "default": {
                    "function": "now"
                },
                "id": True
            },
            {
                "name": "customer_id",
                "refers": "parents.customer.uid"
            },
            {
                "name": "customer_email",
                "refers": "parents.customer.email"
            },
            {
                "name": "customer_abc",
                "refers": "parents.customer.abc"
            },
            {
                "name": "location",
                "type": "geolocation"
            }
        ],
        "parents": ["customer"]
    }

    interaction = {
        'data_type': 'object_concurrency_large',
        "name": "interaction",
        "has_login": True,
        "attributes": [
        {
            "name": "interaction_id",
            "default": {
                "function": "make_view_id"
            },
            "type": "uid",
            "id": True
        }, {
            "name": "stage",
            "required": True,
            "default": "new"
        }, {
            "name": "order_id",
            "refers": "parents.invoice.order_id",
            "required": False,
            "options": "/options/invoice/customer_email?customer_uid=a"
        }, {
            "name": "customer_uid",
            "default": {
                "function": "get_from_model",
                "args": [
                    "invoice",
                    "order_id",
                    "customer_id"
                ]
            },
            "refers": "parents.customer.uid",
            "relation_called": "my_customer"
        }, {
            "name": "customer_email",
            "refers": "parents.customer.email",
            "relation_called": "my_customer",
            "required": True
        }, {
            "name": "interaction_email",
            "default": "ananth@iamdave.ai",
            "type": "email",
            "required": True
        }, {
            "name": "interaction_email",
            "type": "email",
            "required": True,
            "default": "ananth@iamdave.ai"
        }, {
            "name": "customer_abc",
            "refers": "parents.customer.abc",
            "relation_called": "my_customer"
        }, {
            "name": "product_name",
            "refers": "parents.product.name"
        }
        ],
        "parents": [["customer", "my_customer"], "product", "invoice"]
    }



    try:
        delete('interaction', 'test1')
    except ModelNotFound:
        pass
    try:
        delete('invoice', 'test1')
    except ModelNotFound:
        pass
    try:
        delete('customer', 'test1')
    except ModelNotFound:
        pass
    try:
        delete('product', 'test1')
    except ModelNotFound:
        pass
    post('customer', 'test1', 'admin', customer)
    customer['attributes'][2]['options'] = [
        'Asia', 'Pacific', 'North America', 'South America', 'Africa'
    ]
    post('customer', 'test1', 'admin', customer)
    post('product', 'test1', 'admin', product)
    post('invoice', 'test1', 'admin', order)
    i = post('interaction', 'test1', 'admin', interaction)
    print "Result of posting", i['attributes'][3]['default']
    c = Model('customer', 'test1', 'admin')
    print "Customer Attribute names", c.get_attributes('name')
    p = Model('product', 'test1', 'admin')
    om = Model('invoice', 'test1', 'admin')
    i = Model('interaction', 'test1', 'admin')
    print i.attributes['customer_uid'].to_dict()
    print "Interaction attribute default for customer_uid", i.get_attributes('default', name = 'customer_uid')
    print "customer children", c.children
    ci = c.post({
        "name": "customer1",
        "city": "Pune",
        "email": "customer1@gmail.com",
        "abc": 3,
        "a_date": "12:21",
        "true_false": True,
    })
    ci = c.post({
        "name": "customer2",
        "city": "delhi",
        "email": "customer2@gmail.com",
        "abc": 7,
        "a_date": "2:31",
        "true_false": True,
    })
    ci = c.post({
        "name": "customer3",
        "city": "Pune",
        "email": "customer3@gmail.com",
        "abc": 13,
        "a_date": "4:34",
        "colour": [1,2,3],
        "true_false": True,
    })
    ci1 = c.post({
        "name": "customer4",
        "city": "delhi",
        "email": "customer4@gmail.com",
        "abc": 17,
        "a_date": "14:23",
        "true_false": False,
    })
    ci = c.post({
        "name": "customer5",
        "city": "Pune",
        "email": "customer5@gmail.com",
        "a_date": "17:43",
        "abc": 17,
        "true_false": True,
    })
    for o in c.list(
            **
        {"abc": "5,17",
         "city": ["pune", "delhi"],
         "condition_type": "OR"})["data"]:
        print "Listing customers", o
    print "iAdd 17+40", c.iadd(ci["uid"], "abc", 40)
    print "iUpdate 57+20", c.iupdate(ci["uid"], {"abc": 20})
    print "iUpdate mylist", c.iupdate(ci["uid"], {"my_list": ["mumbai"]})
    print "iUpdate mylist", c.iupdate(ci["uid"], {"my_list": ["pune"]})
    print "Get customer", c.get([ci["uid"]])
    print c.get_unique("abc")
    print "Filtering attributes: Customers in delhi or Pune"
    for o in c.list(**{"city": ["delhi", "Pune"],
                       "condition_type": "and"})["data"]:
        print o
    print "Filtering negative page number -1, 1"
    for o in c.list(_page_number = -1, _page_size = 1)["data"]:
        print o
    print "Filtering negative page number -2, 1"
    for o in c.list(_page_number = -2, _page_size = 1)["data"]:
        print o
    print "Filtering negative page number -3, 2"
    for o in c.list(_page_number = -1, _page_size = 3)["data"]:
        print o
    print "Filtering negative page number -2, 2"
    for o in c.list(_page_number = -2, _page_size = 2)["data"]:
        print o
    print "Pivoting min", c.pivot(["city", "email"], "min", "abc", _as_option = True, sort_by = "a_date", sort_reverse = False, _fresh = True)
    print "Pivoting concat", c.pivot(["city", "region"], "concat", "abc", _as_option = True, sort_by = "a_date", sort_reverse = False, _fresh = True)
    print "Pivoting any", c.pivot(["city", "region"], "any", "true_false", _as_option = True, sort_by = "a_date", sort_reverse = False, _fresh = True)
    print "Pivoting all", c.pivot(["city", "region"], "all", "true_false", _as_option = True, sort_by = "a_date", sort_reverse = False, _fresh = True)
    print "Customers with abc = 7\n",
    for o in c.list(**{"abc": 7})["data"]:
        print o
    for o in c.list(**{
            "city": "delhi",
            "name": "customer2",
            "abc": [13, 30, 20],
            "condition_type": "OR"
    })["data"]:
        print o
    for o in c.list(**{"abc": 7})["data"]:
        print o
    print "Count", c.count()
    print "Adding products"
    pi1 = p.post({
        'title': 'my product 1',
        'abc': 3,
        'created_time': datetime.now(),
        "my_list": ['c', 'd', 'e']
    })
    pi = p.post({
        'title': 'my product',
        'abc': 3,
        'created_time': datetime.now() - timedelta(days = 400),
        "my_list": ['a', 'b', 'c']
    })
    print "Filtering products by created time: ", json.dumps(p.list(_as_option = True, created_time = '2020-01-01,'), indent = 4)
    print "Adding order"
    oo1 = om.post({'customer_id': ci1['uid'], 'location': '12.9716, 77.5946'})
    oo2 = om.post({'customer_id': ci['uid'], 'location': '19.0760, 72.8777'})
    print "Pivoting", om.pivot("customer_id", _fresh = True)
    print "Pivoting with page size", om.pivot("customer_id", _fresh = True, _page_number = -1, _page_size = 2)
    print "Adding interaction"
    i = Model('interaction', 'test1', 'admin')
    ii = i.post({'product_name': pi['name'], 'order_id': oo1['order_id'], 'customer_uid': oo1['customer_id']})
    ii = i.post({'product_name': pi['name'], 'order_id': oo2['order_id'], 'customer_uid': oo1['customer_id']})
    print "Foreign Key for Customer in Interaction in ", i.get_parent_by_foreign_key('customer_uid', name_only = True)
    print i.pivot('product_name', _fresh = True)
    print "Interaction", json.dumps(ii, indent=4)
    print enterprise.get('test1')['customer_model_name']
    print "Listing unique values by city"
    print c.get_unique('city')
    print "Filtering products from list", json.dumps(p.list(my_list = 'a', _as_option = True), indent = 4)
    print "Getting last updated products", json.dumps(p.list(_last_updated = '2016-07-01', _sort_by = '__timestamp', _reverse = True))
    print "Getting last updated customer", json.dumps(c.list(_last_updated = '2016-07-01', _sort_by = '__timestamp', _reverse = True))
    print "Getting last updated orders", json.dumps(om.list(_last_updated = '2016-07-01', _sort_by = '__timestamp', _reverse = True))
    print "Getting last updated interactions", json.dumps(i.list(_last_updated = '2016-07-01', _sort_by = '__timestamp', _reverse = True))
    print "Get customer", c.get(ci["uid"]) 
    print "Updating customer", c.update(ci["uid"], {"email": "updated_email@email.com"})
    print "Get Order", json.dumps(om.get(oo2['order_id']), indent = 4)
    print "Search Order by geolocation", json.dumps(om.list(_as_option = True, location = '19,72,100'), indent = 4)
    print "Get Interaction", json.dumps(i.get(ii['interaction_id']), indent = 4)
    print "Get all interactions", json.dumps(i.list(), indent = 4)
    print "Delete Order", json.dumps(om.delete(oo1['order_id']), indent = 4)
    print "Deleting customer", c.delete(ci1['uid'])
    print "Get all interactions", json.dumps(i.list(), indent = 4)
    print "Get all interactions", json.dumps(list(i.yield_list()), indent = 4)
    print "Deleting customer", c.delete(ci['uid'])
    print "List Orders", json.dumps(om.list(customer_id = ci['uid'], _as_option = True), indent = 4)
    print "List Orders", json.dumps(list(om.yield_list(customer_id = ci['uid'], _as_option = True)), indent = 4)
    print "List Interactions", json.dumps(i.list(customer_uid = ci['uid'], _as_option = True), indent = 4)
    print "Get customer", c.get(ci["uid"]) 
    print c.ids
    delete('invoice', 'test1', 'admin')
    delete('interaction', 'test1', 'admin')
    delete('customer', 'test1', 'admin')
    delete('product', 'test1', 'admin')
    enterprise.delete('test1')

