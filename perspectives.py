import model as base_model
import datetime 
import helpers as hp
from model import Model
import calendar
from flask import jsonify
from itertools import cycle
import json
import unicodedata
import interactions
from collections import OrderedDict

class PerspectiveError(hp.I2CEError):
    pass

class PerspectiveNotFound(Exception):
    pass

class PerspectiveParamError(Exception):
    pass


def get_perspective_attributes(name, enterprise_id, user_id, role = 'admin'):
    pref = Preference(enterprise_id, user_id, role)
    return pref.get_preferences(name)

class Preference(object):

    def __init__(self, enterprise_id, user_id = None, role = 'admin', st = None):
        self.enterprise_id = enterprise_id
        self.role = role
        self.user_id = user_id
        self.enterprise_model = Model('enterprise', 'core')
        self.enterprise = self.enterprise_model.get(enterprise_id)
        if role == 'admin':
            self.login_model = Model('admin', 'core')
            self.login = self.login_model.get([user_id])
            self.role_model = None
            self.role_obj = {}
        elif user_id:
            self.login_model = Model('login', enterprise_id)
            self.login = self.login_model.get(user_id)
            self.role_model = Model(self.role, self.enterprise_id, self.role)
            self.role_obj = self.role_model.to_dict()
        else:
            self.login_model = Model('login', enterprise_id)
            self.login = {}
            self.role_model = None
            self.role_obj = {}
        if user_id and not self.login:
            raise base_model.UnknownUser("Unkown user {} from enterprise {}".format(user_id, enterprise_id))
        self.perspective = Model('perspective', 'core')
        self.pbf = PersBaseFunc(enterprise_id, user_id, role, st = st)

    def get_preferences(self, perspective_name):
        perspective = self._getp(perspective_name)
        ca = OrderedDict(perspective.get('custom_attributes'))
        for k, v in ca.iteritems():
            v['default'] = self.get(perspective_name, k, v.get('default'), get_perpective_default = False)
            v['options'] = self.expand_from_perspective_dict(v.get('options'))
            v['name'] = k
            self.pbf._dict[k] = v['default']
        return ca

    def expand_from_perspective_dict(self, attr):
        if isinstance(attr, (str, unicode)) and attr.startswith('{') and attr.endswith('}'):
            try:
                r = self.pbf.to_dict(attr.strip('{').strip('}'))
            except PerspectiveError as e:
                return attr
            else:
                return r
        return attr

    def _getp(self, perspective_name):
        perspective = self.perspective.get(perspective_name)
        if not perspective:
            raise PerspectiveNotFound("Perspective {} is not defined".format(perspective_name))
        return perspective

    def _getp_default(self, perspective_name, preference_name):
        perspective = self._getp(perspective_name)
        if not preference_name in perspective.get('custom_attributes', {}).iterkeys():
            raise PerspectiveParamError("Customer preference attribute {} is unknown in perspective {}".format(preference_name, perspective_name))
        return perspective.get('custom_attributes',{}).get(preference_name, {}).get('default')

    def _set(self, perspective_name, preference_name, preference_value, obj, mdl):
        perspective = self._getp(perspective_name)
        if not obj.get('preferences'):
            obj['preferences'] = {}
        if not perspective_name in obj.get('preferences'):
            obj['preferences'][perspective_name] = {}
        obj['preferences'][perspective_name][preference_name] = preference_value
        mdl.update(mdl.dict_to_ids(obj), {'preferences': obj['preferences']})
        return preference_value
    
    def _set_dict(self, perspective_name, preference_dict, obj, mdl):
        perspective = self._getp(perspective_name)
        if not obj.get('preferences'):
            obj['preferences'] = {}
        if not perspective_name in obj.get('preferences'):
            obj['preferences'][perspective_name] = {}
        obj['preferences'][perspective_name].update(preference_dict)
        mdl.update(mdl.dict_to_ids(obj), {'preferences': obj['preferences']})
        return obj['preferences'][perspective_name]
    
    def _delete(self, perspective_name, preference_name, obj, mdl):
        perspective = self._getp(perspective_name)
        if not obj.get('preferences') or not perspective_name in obj.get('preferences', {}):
            return None
        r = obj['preferences'][perspective_name].pop(preference_name, None)
        mdl.post(obj)
        return r

    def set(self, perspective_name, preference_name, preference_value):
        if not self.login_model:
            raise UnknownUser("Unknown user cannot set perspective")
        return self._set(perspective_name, preference_name, preference_value, self.login, self.login_model)
    
    def set_dict(self, perspective_name, preference_dict):
        if not self.login_model:
            raise UnknownUser("Unknown user cannot set perspective")
        return self._set_dict(perspective_name, preference_dict, self.login, self.login_model)
    
    def delete(self, perspective_name, preference_name):
        return self._delete(perspective_name, preference_name, self.login, self.login_model)

    def delete_dict(self, perspective_name, preference_dict):
        return self._delete_dict(perspective_name, preference_dict, self.login, self.login_model)

    def enterprise_set(self, perspective_name, preference_name, preference_value):
        return self._set(perspective_name, preference_name, preference_value, self.enterprise, self.enterprise_model)

    def enterprise_set_dict(self, perspective_name, preference_dict):
        return self._set_dict(perspective_name, preference_dict, self.enterprise, self.enterprise_model)

    def enterprise_delete(self, perspective_name, preference_name):
        return self._delete(perspective_name, preference_name, self.enterprise, self.enterprise_model)

    def enterprise_delete_dict(self, perspective_name, preference_dict):
        return self._delete(perspective_name, preference_dict, self.enterprise, self.enterprise_model)

    def role_set(self, perspective_name, preference_name, preference_value, role):
        perspective = self._getp(perspective_name)
        role = Model(role, self.enterprise_id, role)
        obj = role.to_dict()
        if not obj.get('preferences'):
            obj['preferences'] = {}
        if not perspective_name in obj.get('preferences'):
            obj['preferences'][perspective_name] = {}
        obj['preferences'][perspective_name][preference_name] = preference_value
        role.update_model(obj)
        return preference_value
    
    def role_set_dict(self, perspective_name, preference_dict, role):
        perspective = self._getp(perspective_name)
        role = Model(role, self.enterprise_id, role)
        obj = role.to_dict()
        if not obj.get('preferences'):
            obj['preferences'] = {}
        if not perspective_name in obj.get('preferences'):
            obj['preferences'][perspective_name] = {}
        obj['preferences'][perspective_name].update(preference_dict)
        role.update_model(obj)
        return role.to_dict()
    
    def role_get(self,perspective_name,preference_name,role):
        perspective = self._getp(perspective_name)
        role = Model(role, self.enterprise_id, role)
        obj = role.to_dict()
        if not obj.get('preferences'):
            return None
        if not perspective_name in obj.get('preferences'):
            return None
        if not  obj['preferences'][perspective_name].get(preference_name):
            return None
        else:
            return obj['preferences'][perspective_name][preference_name]


    def role_delete(self, perspective_name, preference_name, role):
        perspective = self._getp(perspective_name)
        role = Model(role, self.enterprise_id, role)
        obj = role.to_dict()
        if not obj.get('preferences'):
            obj['preferences'] = {}
        if not perspective_name in obj.get('preferences'):
            obj['preferences'][perspective_name] = {}
        obj['preferences'][perspective_name].pop(preference_name, None)
        role.update_model(obj)
        return role.to_dict()

    def get_dict(self, perspective_name, preference_dict, get_perpective_default = True):
        ep = self.enterprise.get('preferences', {}).get(perspective_name, {})
        rp = self.role_obj.get('preferences', {}).get(perspective_name, {})
        lp = self.login.get('preferences', {}).get(perspective_name, {})
        r = {}
        for preference_name, default in preference_dict.iteritems():
            if default is None and get_perpective_default:
                default = self._getp_default(perspective_name, preference_name)
            r[preference_name] = self.expand_from_perspective_dict(
                lp.get(
                    preference_name, rp.get(
                        preference_name, ep.get(
                            preference_name, default
                        )
                    )
                )
            )
        return r

    def get(self, perspective_name, preference_name, default = None, get_perpective_default = True):
        if default is None and get_perpective_default:
            default = self._getp_default(perspective_name, preference_name)
        return self.expand_from_perspective_dict(
            self.login.get('preferences', {}).get(perspective_name, {}).get(
                preference_name,
                self.role_obj.get('preferences', {}).get(perspective_name, {}).get(
                    preference_name,
                    self.enterprise.get('preferences', {}).get(perspective_name, {}).get(
                        preference_name, default
                    )
                )
            )
        ) or default

class PersBaseFunc(object):
    
    def __init__(self,enterprise_id,user_id,role=None,st=None):
        self.enterprise_id=enterprise_id
        self.curr_datetime=datetime.datetime.now()
        self.today = self.curr_datetime.date() 
        self.user_id=user_id
        self.role=role
        st = st or interactions.setup(enterprise_id, role = role, user_id = user_id)
        interaction_group_attributes = [x['interaction_group_attribute'] for x in st.interaction_stages if x.get('interaction_group_attribute')]
        invoice_models = filter(lambda x: x is not None, set(st.interaction_model.get_parent_by_foreign_key(x, name_only = True) for x in interaction_group_attributes)) or filter(lambda x: x not in [st.product_model_name, st.customer_model_name, 'interaction_stage' ], st.interaction_model.parents.keys())
        probable_invoice_model = st.interaction_model.get_parent_by_foreign_key(
            st.interaction_stage_dict.get(
                st.lowest_experienced_stage, {}
            ).get(
                'interaction_group_attribute', hp.make_single(
                    interaction_group_attributes,
                    force = True,
                    default = 'order_id'
                )
            )
        ) or hp.make_single(
            filter(
                lambda x: x.name not in [st.product_model_name, st.customer_model_name, 'interaction_stage' ], st.interaction_model.parents.values()
            ), 
            force = True
        )
        self.interaction_stage_model = st.interaction_stage_model
        self._dict = {
            'enterprise_id': st.enterprise_id,
            'enterprise_url': st.enterprise.get('enterprise_url'),
            'invoice_models': invoice_models,
            'invoice_model': probable_invoice_model,
            'invoice_model_name': probable_invoice_model.name if probable_invoice_model else None,
            'invoice_id_attr_name': hp.make_single(probable_invoice_model.ids, force = True) if probable_invoice_model else None,
            'invoice_attributes': probable_invoice_model.get_attributes('name') if probable_invoice_model else [],
            'invoice_reso_attributes': probable_invoice_model.get_attributes('name', type = ['price', 'discount', 'number']) if probable_invoice_model else [],
            'invoice_date_attributes': probable_invoice_model.get_attributes('name', type = ['price', 'discount', 'number']) if probable_invoice_model else [],
            'invoice_date_attribute': hp.make_single(probable_invoice_model.get_attributes('name', type = ['date', 'datetime', 'timestamp']), force = True) if probable_invoice_model else None,
            'invoice_boolean_attrs': probable_invoice_model.get_attributes('name', ui_element = 'switch') if probable_invoice_model else [],
            'invoice_boolean_attr': hp.make_single(probable_invoice_model.get_attributes('name', ui_element = 'switch'), force = True) if probable_invoice_model else None,
            'customer_model': st.customer_model,
            'product_model': st.product_model,
            'interaction_model': st.interaction_model,  
            'customer_model_name': st.customer_model_name,
            'product_model_name': st.product_model_name,
            'interaction_model_name': st.product_model_name,  
            'customer_model_names': [st.customer_model.name],
            'product_model_names': [st.product_model.name],
            'interaction_model_names': [st.interaction_model.name],
            'default_model_names': base_model.models(self.enterprise_id, 'name'),
            'role_model_names': base_model.models(self.enterprise_id, 'name', has_login = True),
            'role': self.role,
            'product_attributes': st.product_model.get_attributes('name',type=["category", "tags", "price", "discount", "text", "bool", "boolean", "yes_no", "true_false", "image", "image_list", "image_object", "name", "number"]),
            'product_filter_attributes': st.product_model.get_attributes('name',type=["category", "tags", "price"]),
            'product_name_attributes': st.product_model.get_attributes('name', type=['name']),
            'customer_name_attributes': st.customer_model.get_attributes('name', type=['name']),
            'product_id_attributes': st.product_model.ids,
            'customer_id_attributes': st.customer_model.ids,
            'product_id_attribute': hp.make_single(st.product_model.ids, force = True),
            'customer_id_attribute': hp.make_single(st.customer_model.ids, force = True),
            'customer_attributes': st.customer_model.get_attributes('name'),
            'interaction_attributes': st.interaction_model.get_attributes('name'),
            'customer_card_image': st.customer_model.get_attributes('name', type=["image","image_list","image_object"]),
            'product_card_image': st.product_model.get_attributes('name', type=["image","image_list","image_object"]),
            'interaction_price_attributes': st.interaction_model.get_attributes('name', type = 'price'),
            'interaction_stage_names': map(lambda x: x['name'], st.interaction_stage_model.filter(filter_attributes='name', _sort_by = 'default_positivity_score')),
            'product_reso_attributes':st.product_model.get_attributes('name',type=['price','discount']),
            'customer_reso_attributes':st.customer_model.get_attributes('name',type=['price','discount']),
            'product_discount_attributes':st.product_model.get_attributes('name',type=['discount']),
            'customer_discount_attributes':st.customer_model.get_attributes('name',type=['discount']),
            'interaction_datetime_attributes': st.interaction_model.get_attributes('name', type =['date','datetime','timestamp']),
            'interaction_qty_attributes': st.interaction_model.get_attributes('name', type = 'number'),
            'interaction_summation_attributes': st.interaction_model.get_attributes('name', type = ['number','price']),
            'lowest_experienced_stage': st.lowest_experienced_stage,
            'experienced_stages': st.experienced_stages,
            'product_view_attrs': st.product_model.get_attributes('name',type=['price','discount', 'category', 'tags', 'name', 'number']),
        }
        if len(self._dict['customer_name_attributes']):
            self._dict['customer_name_default'] = self._dict['customer_name_attributes'][0]
        else:
            self._dict['customer_name_default'] = self._dict['customer_id_attributes'][0]
            
        if len(self._dict['customer_attributes']):
            self._dict['customer_sub_header_default'] = self._dict['customer_attributes'][0]
        else:
            self._dict['customer_sub_header_default'] = self._dict['customer_id_attributes'][0]
            
        if len(self._dict['customer_card_image']):
            self._dict['customer_card_image_default'] = self._dict['customer_card_image'][0]
            
        if len(self._dict['product_name_attributes']):
            self._dict['product_name_default'] = self._dict['product_name_attributes'][0]
        else:
            self._dict['product_name_default'] = self._dict['product_id_attributes'][0]
       
        if len(self._dict['product_attributes']):
            self._dict['product_sub_header_default'] = self._dict['product_attributes'][1]
        else:
            self._dict['product_sub_header_default'] = self._dict['product_id_attributes'][0]
            
        if len(self._dict['product_reso_attributes']):
            self._dict['product_card_price_default'] = self._dict['product_reso_attributes'][0]
        else:
            self._dict['product_card_price_default'] = self._dict['product_id_attributes'][0]
            
        if len(self._dict['product_card_image']):
            self._dict['product_card_image_default'] = self._dict['product_card_image'][0]
            
        if len(self._dict['interaction_price_attributes']):
            self._dict['interaction_net_amount_default'] = self._dict['interaction_price_attributes'][0]
        else:
            self._dict['interaction_net_amount_default'] = None
            
        if len(self._dict['interaction_qty_attributes']):
            self._dict['interaction_qty_default'] = self._dict['interaction_qty_attributes'][0]
        else:
            self._dict['interaction_qty_default'] = None
        
     
        


        
    def to_dict(self, attribute = None, raise_error = False, return_dict = None):
        if return_dict is None: return_dict = {}
        for k, v in self._dict.iteritems():
            if hasattr(v, 'to_dict'):
                return_dict[k] = v.to_dict()
            elif hasattr(v, '__call__'):
                return_dict[k] = v(return_dict)
            else:
                return_dict[k] = v
        if not attribute:
            return return_dict
        if raise_error and attribute not in self._dict:
            raise PerspectiveError("Attribute {} not found in Perspective dict".format(attribute))
        return return_dict.get(attribute)


    def create_range(self,period):
        curr_date=self.curr_datetime
        period_range=[]
        if period=="hourly":
            prev_date=curr_date-datetime.timedelta(minutes=1440)
            for i in range(24):
                prev_date=prev_date+datetime.timedelta(minutes=60)
                period_range.append(prev_date.strftime('%H')+":00")
        elif period=="weekly":
            curr_week=datetime.datetime.strptime(curr_date.strftime('%W %Y 1'), '%W %Y %w')
            prev_date=curr_week-datetime.timedelta(minutes=70560)
            for i in range(7):
                prev_date=prev_date+datetime.timedelta(minutes=10080)
                period_range.append(prev_date.strftime('%Y-%m-%d'))
        elif period == "monthly":
            curr_month=datetime.datetime.strptime(curr_date.strftime('1 %Y %m'), '%d %Y %m')
            prev_date=curr_month-datetime.timedelta(minutes=518400)
            for i in range(12):
                prev_date=prev_date+datetime.timedelta(minutes=43200)
                period_range.append(prev_date.strftime('%b'))
        elif period =="yearly":
            prev_date=curr_date-datetime.timedelta(minutes=5259488)
            for i in range(10):
                prev_date=prev_date+datetime.timedelta(minutes=525600)
                period_range.append(prev_date.strftime('%Y'))
        return period_range

    def get_past_date(self,period,for_card=False):
        t_in_min={
                "hourly":1440,
                "weekly":70560,
                "monthly":525600,
                "yearly":5259488
                }

        periods={
                "daily":2880,#two days
                "weekly":20160,#two weeks
                "monthly":43200,#one_month
                "yearly":525600 #one_year
                }

        if for_card:
            if periods.get(period):
                if period not in ['monthly','yearly']:
                    return str(self.today-datetime.timedelta(minutes=periods[period]))+","+ str(self.today)
                elif period == "monthly":
                    curr_month=self.today.month
                    if curr_month==1:
                        prev_month=12
                        prev_year=self.today.year-1
                        return str(self.today.replace(year=prev_year,month=prev_month,day=1)) + "," + str(self.today)
                    else:
                        prev_month=curr_month-1
                        return str(self.today.replace(month=prev_month,day=1)) + "," + str(self.today)
                elif period == "yearly":
                    return str(self.today.replace(month=1,day=1)-datetime.timedelta(minutes=periods[period])) + "," + str(self.today)
            else: 
                return "please give proper time period"
        if t_in_min.get(period):
            return str(self.curr_datetime-datetime.timedelta(minutes=t_in_min[period]))+"," + str(self.curr_datetime)
        else: 
            return "please give proper time period"

    def get_resolution(self,period,for_card=False):
        res={
                "hourly":"hour_of_day",
                "weekly":"week_of_year",
                "monthly":"month_of_year",
                "yearly":"year"
                }
        res_for_card={
                "hourly":60*60,
                "daily":60*60*24,
                "weekly":60*60*24*7,
                "monthly":"month",
                "yearly":"year"
                }
        if for_card:
            return res_for_card[period]
        return res[period]


    # def get_starting_date(self,period):
    #     if period =="monthly":
    #         first_date=str(self.today.replace(day=1))
    #     elif period == "yearly":
    #         first_date=str(self.today.date().replace(month=1, day=1))
    #     return firt_date
            



    # def card_dates(self,period):
    #     periods={
    #             "daily":2880,#two days
    #             "weekly":20160,#two weeks
    #             "monthly":43200,#one_month
    #             "yearly":525600 #one_year
    #             }
    #     if periods.get(period):
    #         if period not in ['monthly','yearly']:
    #             return str(self.today-datetime.timedelta(minutes=periods[period]))+","+ str(self.today)
    #         elif period == "monthly":
    #             curr_month=self.today.month
    #             if curr_month==1:
    #                 prev_month=12
    #                 prev_year=self.today.year-1
    #                 return str(self.today.replace(year=prev_year,month=prev_month,day=1)) + "," + str(self.today)
    #             else:
    #                 prev_month=curr_month-1
    #                 return str(self.today.replace(month=prev_month,day=1)) + "," + str(self.today)
    #         elif period == "yearly":
    #             return str(self.today.replace(month=1,day=1)-datetime.timedelta(minutes=periods[period])) + "," + str(self.today)
    #     else: 
    #         return "please give proper time period"


    def get_model_by_type(self, mtype):
        m = base_model.models(self.enterprise_id, model_type = mtype, _as_model = True)
        return hp.make_single(m)

    def get_data(self,chart_for,options,period,**kwargs):
        interaction=self._dict['interaction_model']
        date_attribute=kwargs.get('datetime_attr')
        if not date_attribute:
            raise PerspectiveError("No attribute in interactions which of type date, datetime or timestamp. Cannot plot")
        if chart_for == "interactions":
            if kwargs.get('for_conversion_cards'):
                p1={date_attribute:self.get_past_date(period,for_card=True)}
                no_of_unique_cust=len(self.do_pivot(interaction,self._dict['customer_model'].ids,params=p1,only_data=True)) if self.do_pivot(interaction,self._dict['customer_model'].ids,params=p1,only_data=True) else 0
                kwargs['unique_cust_no']=no_of_unique_cust
                time_range=self.get_past_date(period,for_card=True).split(',')
                start_date=time_range[0]
                end_date=time_range[1]
                interaction_data= interactions.get_conversion_rates(self.enterprise_id,self.role,start_date=start_date,end_date=end_date,recommended=True)
                interaction_data2= interactions.get_conversion_rates(self.enterprise_id,self.role,start_date=start_date,end_date=end_date)
                chart_data=self.create_chart_data(interaction_data,period,options,interaction_data2=interaction_data2, **kwargs)
                return chart_data
            if kwargs.get('for_stage_cards'):
                attributes=[date_attribute]
                params={kwargs["interaction_stage_attribute"]:options,'_resolution':self.get_resolution(period,for_card=True),date_attribute:self.get_past_date(period,for_card=True),'_action':'sum','_over':kwargs['sum_over_option']}
                return self.do_pivot(interaction,attributes,period,options,params,chart_for=chart_for,**kwargs )
            attributes=[kwargs['interaction_stage_attribute'],date_attribute]
            if kwargs.get('for_price'):
                params={"_resolution":self.get_resolution(period),"_action":"sum","_over":kwargs['pricing_attribute'], date_attribute:self.get_past_date(period)}
            else:
                params={"_resolution":self.get_resolution(period),date_attribute:self.get_past_date(period)}    
            return self.do_pivot(interaction,attributes,period,options,params,chart_for=chart_for,**kwargs )

        elif chart_for in [ "customers","products"]:
            params={}
            if chart_for=="customers":
                if kwargs['join_option'] in self._dict['customer_reso_attributes']:
                    params['_resolution']=500 if kwargs['join_option'] not in self._dict['customer_discount_attributes'] else 5
                req_model=self._dict['customer_model']
            elif chart_for=="products":
                if kwargs['join_option'] in self._dict['product_reso_attributes']:
                    params['_resolution']=500 if kwargs['join_option'] not in self._dict['product_discount_attributes'] else 5
                req_model=self._dict['product_model']
            attributes=[kwargs['join_option']]
            #params['_join']=req_model.name
            no_of_items=int(kwargs.get("no_of_items"))
            f1 = hp.gen_get_top(no_of_items, key_func = lambda x: hp.make_single(x))
            f2 = hp.gen_get_total()
            if options in kwargs['sum_over_options']:
                params.update({kwargs['interaction_stage_attribute']:kwargs['interaction_stage_name'],date_attribute:self.get_past_date(period) if period !="all time" else ",","_action":"sum","_over":options,"_filter_results":[f1,f2]})
            else:
                params.update({kwargs["interaction_stage_attribute"]:kwargs['interaction_stage_name'],date_attribute:self.get_past_date(period) if period !="all time" else ",","_filter_results":[f1,f2]})
            return self.do_pivot(interaction,attributes,period,options,params,top_list_func=f1.top_list,total_func=f2.total,req_model=req_model, chart_for=chart_for,**kwargs)

    def do_pivot(self,interaction,attributes,period=None,options=None,params=None,only_data=False,**kwargs):
        interaction_data=interaction.pivot(attributes, _as_option = True, **params)
        if only_data:
            return interaction_data
        if kwargs['chart_for'] in [ "customers","products"]:
            chart_data=self.create_chart_data(interaction_data,period,options,top_list=kwargs['top_list_func'](),total=kwargs['total_func'](), **kwargs)
        else:
            chart_data=self.create_chart_data(interaction_data,period,options,**kwargs)
        return chart_data


    def create_chart_data(self,data,period,options,**kwargs):
        chart_data={}
        if kwargs['chart_type']=="stacked_bar":
            chart_data={
                    "id":"",
                    "labels":[],
                    "series":[],
                    "keys":[]
                    }
            if kwargs.get('selector_type','id')=="id":
                chart_data['id']="#"+kwargs['selector']
            else:
                chart_data['id']="."+kwargs['selector']
            chart_data["labels"]=self.create_range(period)
            for i_stage in data:
                if i_stage in options:
                    chart_data["keys"].append(str(i_stage))
                    series=[]
                    for label in chart_data["labels"]:
                        series.append(data[i_stage].get(label,0))
                    chart_data['series'].append(series)
            return chart_data
        elif kwargs['chart_type']=="donut_chart":
            chart_data={
                "id":kwargs['selector'],
                "data":[]
            }
            _total= float(kwargs['total'])
            if not _total:
                chart_data['data'].append({'value': 1,'label':'No Data'})
                return chart_data
            for entity in kwargs['top_list']:
                if entity[1] >0:
                    _total -= entity[1]
                    chart_data['data'].append({"value":round(entity[1],2),"label":unicodedata.normalize('NFKD', unicode(entity[0])).encode('ascii','ignore') if entity[0]!=None else 'None' })
            chart_data['data'].append({'value':round(_total,2),'label':'Rest'})
            return chart_data
        elif kwargs.get('chart_type')=='interaction_card':
            if kwargs.get('for_stage_cards'):
                chart_data['stage']=unicodedata.normalize('NFKD', unicode(options)).encode('ascii','ignore')
                if len(data.values()) < 1:
                    chart_data['error'] = "True"
                    chart_data['error_text']="No data for these options"
                    return chart_data
                if not data.values()[0] or data.values()[0] <= 0:
                    chart_data['error'] = "True"
                    chart_data['error_text']="No data for these options"
                    return chart_data
                value1=float(data.values()[0])
                if len(data.values()) < 2:
                    value2=0.0
                else:
                    value2=float(data.values()[1])
                value3=value2-value1
                if value3 > 0:
                    chart_data['increasing'] = 'True'
                else:
                    chart_data['increasing']='False'
                chart_data['value']=round(abs(value3))
                chart_data['value_percent']=round(chart_data['value']*100/value1,2)
                chart_data['error']="False"
                return chart_data
        elif kwargs.get('chart_type')=='conversion_cards':
            chart_data['unique_cust_no']=kwargs.get('unique_cust_no',0)
            chart_data['reco_interaction']=[]
            chart_data['non_reco_interaction']=[]
            for item in data:
                chart_data['reco_interaction'].append([unicodedata.normalize('NFKD', unicode(x)).encode('ascii','ignore') for x in item])
            for item in kwargs['interaction_data2']:
                chart_data['non_reco_interaction'].append([unicodedata.normalize('NFKD', unicode(x)).encode('ascii','ignore') for x in item])
            return chart_data


#f1 = gen_get_top(10, key_func = lambda x: x[0])
#f2 = gen_get_total()
#f3 = gen_get_top(10, lambda x, y: x < y)

#pivot(filter_result = [f1, f2, f3])
#print f1.top_list()
#print f2.total()
#print f3.top_list()


