import os, sys
from helpers import normalize, get_logger, error_raiser, pprint, BaseError, inflect, inf, normalize_join, I2CEError, I2CEErrorWrapper
from copy import deepcopy as copyof, copy
import traceback
import types
from functools import partial
import pgpersist as db
from attribute import Attribute, AttributeRequiredError
from os.path import exists as ispath, dirname, join as joinpath, abspath
import validators as val
import objectifier as objf


def set_db_user(user = 'i2ce'):
    db.DB_USER = user

def set_db_password(password = 'sgep@1234'):
    db.DB_PASSWORD = password

def set_db_name(name = 'pgpdict'):
    db.DB_NAME = name


logger = get_logger(__name__)

MODELS = {}
                             
class AttributeNotFoundError(I2CEError):
    pass

class AttributeTypeError(I2CEError):
    pass

class ValueConstraintError(I2CEErrorWrapper):
    pass

class ValueConvertError(I2CEErrorWrapper):
    pass

class AuthenticationError(I2CEError):
    pass


def pop_children(self, pkeys, now_dict, _check_queue = None, **kwargs):
    """
    Used as pre_delete_hook
    """
    if _check_queue is None: _check_queue = [self.name]
    logger.debug("In pop children: %s", now_dict)
    logger.debug("In pop kwargs: %s", kwargs)
    for called, a in self._children.iteritems():
        if a['name'] not in _check_queue:
            dc = []
            if not a['orphaned']:
                child = self.__class__(a['name'])
                _check_queue.append(child.name)
                for keys in child.iterkeys(
                        filter_by = self._make_filter_by_for_children(a['parent_called'], pkeys)
                    ):
                    dc.append(child.pop(_keys = keys, _check_queue = _check_queue))
            if not now_dict.has_key(called):
                now_dict[called] = dc
    return now_dict

def check_parent_keys(name, instance, parent_name):
    """
    Used as a constraint, cannot use self because of recursive referencing
    """
    self = BaseObject(name)
    if not parent_name in self._parents:
        return False
    parent = BaseObject(parent_name)
    keys = tuple(instance.get(parent_name + '_' + k, None) for k in parent.key_names)
    return parent.has_key(keys)



class MetaBaseObject(db.metapgpcollection):
    all_instances = {}
    def __call__(cls, name, *args, **kwargs):
        if name in MetaBaseObject.all_instances:
            return MetaBaseObject.all_instances[name]
        return type.__call__(cls, name, *args, **kwargs)

class BaseObject(db.pgpcollection):
    __metaclass__ = MetaBaseObject
    _collection_class = db.pgpcollection
    _list_class = db.pgparray
    _dict_class = db.pgpdict
    _new_saved_attributes = (
        '_uniques', '_default_attributes'
    )
    _saved_attributes = db.pgpcollection._saved_attributes +  _new_saved_attributes

    def __init__(self, name, default_attributes = None, title = None, plural = None, cached = None, relative_class = None):
        """
        name: model name
        default_attributes: contains spec for attributes. Each Attribute is of type Attribute
            At least one attribute should have type 'id'
            The primary key is made from all the 'id' attributes
        """
        self.name = normalize_join(name)
        if self.name != name:
            raise I2CEError("BaseObject should be all small letter with no spaces in between: {} e.g. {}".format(name, self.name))
        self.plural = plural or inflect.plural(name)
        self._relative_class = relative_class or BaseRelation
        self._base_init(name, default_attributes, title, cached)
        self._parents = self._dict_class(self.name + '_parents_', cached = True)
        self._children = self._dict_class(self.name + '_children_', cached = True)
        self._relatives = self._dict_class(self.name + '_relatives_', cached = True)

    def _base_init(self, name, default_attributes, title, cached):
        self.title = title or self.name.replace('_', ' ').replace('-', ' ').title()
        default_attributes = default_attributes or []
        self._attribute_dict = self._dict_class(self.name+'_attribute_dict_', cached = True)
        self._attributes = self._collection_class(
             '_'.join([self.name, 'attributes_']), 
             'name', 
             get_hook = (lambda x: Attribute(self, x)), 
             set_hook = dict,
             cached = True
        )
        for att in self._new_saved_attributes:
            setattr(self, att, self._attribute_dict.get(att, []))
        for attr_config in default_attributes:
            logger.debug("Setting init default attribute, %s", attr_config['name'])
            self._set_attribute_config(attr_config, True)
        self._uniques = list(a.name for n, a in self._attribute_filter('unique', True) if a.type != 'id')
        key_names = []; key_types = []
        if self._default_attributes:
            try:
                key_names, key_types = zip(*((n, a.atype) for n, a in self._attribute_filter('type', 'id')))
            except ValueError:
                raise I2CEError("There should be at least one attribute with type 'id'")
        super(BaseObject, self).__init__(
            name, key_names, key_types, 
            uniques = self._uniques, 
            cached = cached,
            pre_delete_hooks = [pop_children]
        )
        self.__metaclass__.all_instances[name] = self


    def _delete_attr(self, now_dict, attr, value = bool):
        for k, a in self._attribute_filter(attr, value):
            now_dict.pop(k, None)
        return now_dict

    def _attribute_filter(self, attribute_feature, value = bool, length = None):
        index = 0
        for k, a in self._attributes.iteritems():
            f = getattr(a, attribute_feature)
            if (hasattr(value, '__call__') and value(f)) or f == value:
                yield k, a
                index += 1
                if length is not None and index > length:
                    raise StopIteration

    def _set_attribute_config(self, config, is_default = False):
        key = config.get('name')
        attribute = Attribute(self, config)
        self._attributes[key] = attribute
        if is_default and key not in self._default_attributes:
            logger.debug("Added to default_attributes %s", key)
            self._default_attributes = self._default_attributes + [key]

    def _add_new_attribute(self, key, value, atype = None):
        if atype is None: atype = type(value)
        config = {
             'name': key, 
             'default': atype(),
             'atype': atype,
             'type': 'feature',
        }
        self._set_attribute_config(config)
             
    def _get_default(self, attribute, instance):
        default = self._attributes[attribute].default
        if hasattr(default, '__call__'):
            default = default(instance)
        else:
            default = copyof(default)
        return default

    def _set_default(self, attribute, instance):
        if instance.has_key(attribute):
            return instance[attribute]
        instance[attribute] = self._get_default(attribute, instance)
        return instance[attribute]

    def _run_adder(self, attribute, instance):
        adder = self._attributes[attribute].adder
        if adder and hasattr(adder,'__call__'):
            instance[attribute] = adder(self._get_default(attribute, instance), instance[attribute])
        return instance[attribute]

    def _run_auto_updater(self, attribute, instance):
        auto_updater = self._attributes[attribute].auto_updater
        if auto_updater:
            if hasattr(auto_updater, '__call__'):
                instance[attribute] = auto_updater(attribute, instance)
            else:
                instance[attribute] = self._get_default(attribute, instance)
        return instance[attribute]

    def _convert(self, attribute, instance):
        converter = self._attributes[attribute].converter
        if not converter:
            return instance[attribute]
        try:
            converted = converter(instance[attribute])
        except Exception as e:
            raise ValueConvertError("Model {}: The value {value} for key {key} cannot be converted by function {func}".format(
                    self.name,
                    value = instance[attribute], 
                    key = attribute, 
                    func = converter)
            )
        else:
            instance[attribute] = converted
        return instance[attribute]

    def _constrain(self, attribute, instance, error_class = None):
        run_convert = True
        if error_class is None: error_class = ValueConstraintError
        constraint = self._attributes[attribute].constraint
        if not constraint:
            return instance[attribute]
        if isinstance(constraint, (str, unicode)):
            if constraint in globals():
                raise error_class("Constraint function {} is not available in global scope".format(constraint))
            constraint = getattr(hp, constraint)
        try:
            is_pass = constraint(instance[attribute], instance)
            if not is_pass:
                is_pass = constraint(self._convert(attribute, instance), instance)
                run_convert = False
        except Exception as e:
            raise error_class("Model {}: Constraint '{func}' cannot be applied on value '{value}' for attribute {key}".format(
                                                        self.name,
                                                        value = instance[attribute],
                                                        key = attribute,
                                                        func = constraint)
            )
        else:
            if not is_pass:
                raise I2CEError("Model {}: Value '{value}' for attribute '{key}' does not follow constraint {func}".format(
                        self.name,
                        value = instance[attribute],
                        key = attribute,
                        func = constraint)
                )
        return instance[attribute], run_convert

    def _set_attribute(self, attribute, instance):
        logger.debug("now setting attribute %s\tinstance: %s", attribute, pprint(instance))
        self._set_default(attribute, instance)
        _, run_convert = self._constrain(attribute, instance)
        if run_convert: self._convert(attribute, instance)
        self._run_adder(attribute, instance)
        self._run_auto_updater(attribute, instance)
        return instance[attribute]

    def _make_unique_constraints(self, uniques):
        column_names = tuple(self._attributes[a].atype.__name__ for a in uniques)
        return super(BaseObject, self)._make_unique_constraints(uniques, column_names)

    def _get_parents(self, now_dict, _check_queue = None, _hierarchy = 0):
        if _check_queue is None: _check_queue = [self.name]
        for called, a in self._parents.iteritems():
            if a['name'] not in _check_queue:
                parent = self.__class__(a['name'])
                _check_queue.append(parent.name)
                try:
                    keys = dict((n, now_dict[a['called'] + '_' + n]) for n in parent.key_names)
                    dp = parent.get(
                        _hierarchy = _hierarchy, 
                        _check_queue = _check_queue, 
                        _mask_attrs = a['mask_attrs'], 
                        _prepend = a['called'] + '_', 
                        **keys
                    )
                    now_dict.update(dp)
                except AuthenticationError:
                    pass
        return now_dict

    def _get_children(self, now_dict, _check_queue = None, _hierarchy = 0):
        if _check_queue is None: _check_queue = [self.name]
        for called, a in self._children.iteritems():
            if a['name'] not in _check_queue:
                logger.debug("attributes for children %s", a)
                child = self.__class__(a['name'])
                _check_queue.append(child.name)
                dc = list(
                    child.filter(
                        filter_by = self._make_filter_by_for_children(a['parent_called'], now_dict),
                        _get_filter = partial(self._process_get,
                            _mask_attrs =  a['mask_attrs'],
                            _hierarchy = _hierarchy,
                            _check_queue = _check_queue
                        )
                    )
                )
                if not now_dict.has_key(called):
                    now_dict[called] = dc

    def _make_filter_by_for_children(self, parent_called, keys_or_instance):
        keys = self._keys_to_list(keys_or_instance)
        return {'AND': list((parent_called + '_' + n, '=', k) for n, k in zip(self.key_names, keys))}
    
    def _get_relatives(self, now_dict, _check_queue = None, _hierarchy = 0):
        if _check_queue is None: _check_queue = []
        for r, a in self._relatives.iteritems():
            r = self._relative_class(r)
            try:
                if hasattr(r.get_authenticate, '__call__'): r.get_authenticate(now_dict)
            except AuthenticationError:
                continue
            if r.name not in _check_queue:
                _check_queue.append(r.name)
                dr = list(
                    r.filter(
                        filter_by = ((self.name + '_' + k, '=', now_dict[k]) for k in self.key_names),
                        _get_filter = partial(self._process_get,
                            _mask_attrs =  a['mask_attrs'],
                            _hierarchy = _hierarchy,
                            _check_queue = _check_queue
                        )
                    )
                )
                pln = r.get_plural(self.name)
                if not now_dict.has_key(pln):
                    now_dict[pln] = dr

    def _process_get(self, instance, _hierarchy = 0, _mask_attrs = None, _check_queue = None, _prepend = ''):
        if _check_queue is None: _check_queue = [self.name]
        if _hierarchy > 0:
            self._get_parents(instance, _check_queue, _hierarchy - 1)
            self._get_children(instance, _check_queue, _hierarchy - 1)
            self._get_relatives(instance, _check_queue, _hierarchy - 1)
        if _mask_attrs is None: _mask_attrs = []
        _mask_attrs = ( _mask_attrs or [] ) + list(k for k, a in self._attribute_filter('mask', True))
        for k in _mask_attrs:
            instance.pop(k, None)
        if _prepend:
            for k in instance.keys():
                instance[_prepend + k] = instance.pop(k)
        return instance

    def _manage_attributes(self, _ro_override = None, **instance):
        # if hasattr(self.set_authenticate, '__call__'): self.set_authenticate(instance)
        default_keys = {}
        for att in self._default_attributes:
            logger.debug("Checking default attribute %s", att)
            self._set_attribute(att, instance)
            default_keys[att] = None
        for key, value in instance.iteritems():
            if not self._attributes.has_key(key):
                self._add_new_attribute(key, value)
            if not default_keys.has_key(key):
                self._set_attribute(key, instance)
        if _ro_override is None: _ro_override = []
        for attr, value in self._attribute_filter('readonly'):
            if value['type'] != 'id' and attr in instance and attr not in _ro_override:
                instance.pop(attr)
        return instance

    def authenticate(self, instance, attributes = '_get_auth_attr', raise_error = True):
        """
        It authenticates the get method if ALL the required attributes are present and at least ONE of the optional arguments are present
        If not, it checks if the parent's authentication can be performed
        """
        def func_validate(instance, req, optional, is_set = False):
            rows = 0
            filter_by = {'AND': list((n, '=', instance[n]) for n in req)}
            logger.debug('In validate filter_by = %s', filter_by)
            for row in self.filter(filter_by = filter_by):
                rows += 1
                if not optional:
                    return row
                for n in optional:
                    if instance[n] == row[n]:
                        return row
            if not rows and is_set: 
                return True
            return False
        if len(getattr(self, attributes)) == 0:
            return True
        req = []
        optional = []
        validate = {}
        for key, attr in getattr(self, attributes):
            if attr == 'required':
                req.append(key)
            elif attr == 'optional':
                optional.append(key)
            validate[key] = instance.get(key, None)
        ret = func_validate(validate, req, optional, is_set = (attributes == '_set_auth_attr'))
        if ret:
            return ret
        for parent_called, attr in self._parents.iteritems():
            validate = {}
            parent = self.__class__(parent_called)
            for k in instance:
                if k.startswith(parent_called + '_'):
                    validate[k.replace(parent_called + '_', '', 1)] = instance[k]
                if parent.authenticate(validate, attributes = attributes, raise_error = False):
                    return True
        if raise_error:
            raise AuthenticationError("Cannot authenticate instance {} to {}".format(instance, attributes))
        return False
        
    
    def get_authenticate(self, instance, raise_error = True):
        """
        It authenticates the get method if ALL the required attributes are present and at least ONE of the optional arguments are present
        If not, it checks if the parent's authentication can be performed
        """
        return self.authenticate(instance, attributes = '_get_auth_attr', raise_error = raise_error)

    def set_authenticate(self, instance, raise_error = True):
        """
        It authenticates the set/update/delete method if ALL the required attributes are present and 
        at least ONE of the optional arguments are present
        If not, it checks if the parent's authentication can be performed
        """
        return self.authenticate(instance, attributes = '_set_auth_attr', raise_error = raise_error)

        

    def hierarchy(self, called = None, get_parents = True, convert_hook = lambda x: x):
        ret = self.to_dict()
        if called is not None: ret['name'] = called
        ret['children'] = []
        for c, a in self._children.iteritems():
            child = self.__class__(a['name'])
            ret['children'].append(child.hierarchy(c, get_parents = False,  convert_hook = convert_hook))
        if get_parents:
            ret['parents'] = []
            for c, a in self._parents.iteritems():
                parent = self.__class__(a['name'])
                ret['parents'].append(parent.hierarchy(c, get_parents = False, convert_hook = convert_hook))
        return convert_hook(ret)

    def to_dict(self):
        return {
                'name': self.name,
                'title': self.title,
                'plural': self.get_plural(),
                'ids': self.key_names,
                'attributes': list(self.attributes())
               }

    def set(self, **instance):
        """
        Upserts an instance of the object to the collection
        """
        instance = self._manage_attributes(**instance)
        logger.debug("Before setting, managed attributes: %s", instance)
        return self._set(**instance)

    def get(self, _hierarchy = inf, _mask_attrs = None, _check_queue = None, _prepend = '', **instance):
        if isinstance(_hierarchy, (str, unicode)) and _hierarchy == 'inf': _hierarchy = inf
        #if hasattr(self.get_authenticate, '__call__'): self.get_authenticate(instance)
        d = self._get(**instance)
        if not d: return d
        return self._process_get(d, _check_queue = _check_queue, _hierarchy = _hierarchy, _mask_attrs = _mask_attrs, _prepend = _prepend )


    def delete(_hierarchy = inf, _check_queue = None, **instance):
        _keys = self._keys_to_list(**instance)
        return self.pop(_keys, _hierarchy = _hierarchy, _check_queue = _check_queue, **instance)

    def pop(self, _keys = None, _default = db.PersistKeyError, _hierarchy = 0, _check_queue = None, **kwargs):
        _keys = _keys or kwargs
        if hasattr(self.set_authenticate, '__call__'): self.set_authenticate(kwargs)
        if _check_queue is None: _check_queue = [self.name]
        _get_hook = partial(
            self._process_get,
            _check_queue = _check_queue,
            _hierarchy = _hierarchy,
        )
        return self._pop(_keys = _keys, _default = _default, _instance = {}, _get_hook = _get_hook)

    def get_plural(self):
        return self.plural

    def new(self):
        """
        Returns an empty dictionary without id
        """
        d = dict((k, None) for k in self._attributes.keys())
        return self._delete_attr(self._delete_attr(d, 'mask', True), 'readonly', True)

    def attributes(self):
        for a, v in self._attributes.iteritems():
            yield v.to_dict()

    def list_attributes(self):
        return list(self.attributes())

    def update(self, **instance):
        old_instance = self._get(**instance)
        if not old_instance:
            raise db.PersistCollectionKeyError("Instance {} does not have any records".format(instance))
        old_instance.update(instance)
        logger.debug("In Update Old instance %s", old_instance)
        instance = self._manage_attributes(**old_instance)
        return self._update(**instance)

    def unique_values(self, attribute):
        try:
            value_type = self._attributes[attribute]
        except db.PersistKeyError:
            raise AttributeNotFoundError("Attribute {} not found in model {}".format(attribute, self.name))
        return self._unique(attribute, value_type)

    def list_unique_values(self, attribute):
        return list(self.unique_values(attribute))

    def clear(self):
        logger.debug('Inside clear')
        for c, a in self._children.iteritems():
            logger.debug('child called %s', c)
            if not a['orphaned']:
                logger.debug('clearing child %s', a['name'])
                child = self.__class__(a['name'])
                child.clear()
        self._parents.clear()
        self._children.clear()
        self._relatives.clear()
        self._attributes.clear()
        return super(BaseObject, self).clear()

    def is_orphaned(self, parent_called):
        if not parent_called in self._parents:
            return False
        return self._parents[parent_called]['orphaned']

    def add_parent(self, parent_name, called = None, mask_attrs = None, mask_parent_attrs = None, readonly_attrs = None, add_child = True, orphaned = False, children_called = None):
        called = called or parent_name
        children_called = children_called or self.get_plural()
        self._parents[called] =  {
            'name': parent_name,
            'mask_attrs': mask_parent_attrs,
            'orphaned': orphaned,
            'called': called,
            'children_called': children_called,
            }
        parent = self.__class__(parent_name)
        for key, atype in zip(parent.key_names, parent.key_types):
            pk = called + '_' + key
            n = self.name
            pn = parent.name
            self._set_attribute_config(
                    {
                        'name': pk, 
                        'type': 'parent', 
                        'atype': atype, 
                        'required': not orphaned,
                        'default' : None if orphaned else lambda x: error_raiser(
                                AttributeRequiredError("Attribute {key} is required for object {obj}".format(
                                   key = pk, obj = n,
                                )
                            )
                        ),
                        'constraint': lambda x, i: x is None or check_parent_keys(n, i, pn),
                        'mask': True if mask_parent_attrs and pk in mask_parent_attrs else False,
                        'readonly': False,
                    },
                    is_default = True
            )
        if add_child:
            parent.add_child(
                child_name = self.name, 
                called = children_called,
                mask_child_attrs = mask_attrs, 
                readonly_child_attrs = readonly_attrs, 
                add_parent = False
            )

    def add_child(self, child_name, called = None, mask_attrs = None, mask_child_attrs = None, readonly_child_attrs = None, add_parent = True, orphaned = False, parent_called = None):
        child = self.__class__(child_name)
        called = called or child.get_plural()
        parent_called = parent_called or self.name
        self._children[called] = {
                'name': child_name,
                'mask_attrs': mask_child_attrs,
                'orphaned': orphaned,
                'readonly_attrs': readonly_child_attrs,
                'called': called,
                'parent_called': parent_called,
        }
        if add_parent:
            child.add_parent(
                parent_name = self.name,
                called = parent_called,
                mask_parent_attrs = mask_attrs, 
                add_child = False, 
                orphaned = orphaned
            )


class BaseRelation(BaseObject):
    _new_saved_attributes = (
        '_models',
    )
    _saved_attributes = db.pgpcollection._saved_attributes +  _new_saved_attributes
    
    def __init__(self, name, model_name = None, default_attributes = None, title = None, base_model = None):
        """
        name: model name
        default_attributes: contains spec for attributes. Each Attribute is of type Attribute
            At least one attribute should have type 'id'
            The primary key is made from all the 'id' attributes
        """
        self.name = name
        default_attributes = default_attributes or []
        self._base_model = base_model or self.__class__.__base__
        self._base_init(name, default_attributes, title)
        if model_name:
            model = self._base_model(model_name)
            for k, t in zip(model.key_names, model._key_types):
                default_attributes.insert(0, {
                    'name': m + '_' + k,
                    'type': 'id',
                    'atype': t,
                    'required': True,
                    'default' : lambda x: error_raiser(
                            AttributeRequiredError("Attribute {key} is required for object {obj}".format(
                               key = k, obj = m,
                            )
                        )
                    ),
                    'constraint': lambda x, i: check_parent_keys(i, m),
                })

    def get_plural(self, model_name):
        if not model_name in self._models:
            raise BaseError("model {} not present in relationship {}".format(model_name, self.name))
        ms = list(self._models)
        ms.remove(model_name)
        ms[-1] = inflect.plural(ms[-1])
        return '_'.join(ms)



class Seed(object):

    def __init__(self, datadir, storedir = None):

        self.storedir = storedir or os.environ.get('STOREDIR', dirname(__file__))
        self.datadir = datadir

    def run(self, startfrom = 0):
        for f in sorted(self.__class__.__dict__):
            if 'seed_' in f and f.startswith('seed_'):
                now_seed = int(f.replace('seed_',''))
                if now_seed < startfrom:
                    continue
                nfile = joinpath(self.storedir, '.'+f)
                if not ispath(nfile):
                    getattr(self, f)()
                    open(nfile, 'w').close()


if __name__ == '__main__':

    import time
    from functools import partial
    import json
    import validators as val
    import helpers as hp


    enterprises = BaseObject(
        'enterprise',
        default_attributes = [
            {'name': 'name', 'weight': 0.0, 'unique': True, 'converter': normalize, 'type': 'name'},
            {'name': 'address', 'atype': unicode},
            {'name': 'city', 'converter': normalize},
            {'name': 'country', 'default': 'india',' converter': normalize},
            {'name': 'zipcode', 'atype': int, 'constraint': val.get_zipcode_validator()},
            {'name': 'email', 'constraint': val.get_email_validator(), 'unique': True},
            {'name': 'password', 'weight': 0.0, 'constraint': val.get_password_validator() , 'converter': hp.hash_password},
            {'name': 'created_timestamp', 'atype': float, 'type': 'timestamp', 'default': lambda x: time.time(), 'mask': True },
            {'name': 'updated_timestamp', 'atype': float, 'type': 'timestamp', 'default': lambda x: time.time(), 'auto_updater': True, 'mask': True},
            {'name': 'uid', 'default': val.get_uuid_maker('email', 'timestamp'), 'type': 'id', 'readonly': True},
        ],
        cached = True,
    )
    customers = BaseObject(
        'customer',
        default_attributes = [
            {'name': 'name', 'converter': normalize},
            {'name': 'email', 'unique': True, 'constraint': val.get_email_validator()},
            {'name': 'password', 'constraint':  val.get_password_validator(), 'converter': hp.hash_password },
            {'name': 'created_timestamp', 'atype': float, 'type': 'timestamp', 'default': lambda x: time.time(), 'mask': True},
            {'name': 'updated_timestamp', 'atype': float, 'type': 'timestamp', 'default': lambda x: time.time(), 'auto_updater': True, 'mask': True},
            {'name': 'uid', 'default': val.get_uuid_maker('email', 'timestamp'), 'type': 'id', 'readonly': True}
        ],
        cached = True,
    )
   
        
    customers.add_parent('enterprise', mask_parent_attrs = ['city', 'email', 'password', 'address'], mask_attrs = ['password'])

    print json.dumps(enterprises.hierarchy(), indent = 4)

    e1 = enterprises.set(name = 'test_enterprise', address = 'test address', city = 'test city', country = 'test country', 
                         zipcode = 232028, email = 'test@email.com', password = 'Infy!2#4')
    print "First set enterprises", json.dumps(enterprises.get(uid = e1['uid']), indent = 4)

    c1 = customers.set(name = 'test_customer', email = 'test@email.com', password = 'Infy!2#4', enterprise_uid = e1['uid'])
    print "First set customer", json.dumps(customers.get(uid = c1['uid'], _hierarchy = 1), indent = 4)
    c2 = customers.set(name = 'test_customer_set', email = 'test_set@email.com', password = 'Infy1234', enterprise_uid = e1['uid'])
    print "Second set customer", json.dumps(customers.get(uid = c1['uid'], _hierarchy = 1), indent = 4)
    c1 = customers.update(name = 'test_customer_updated', uid = c1['uid'])
    print "Update customer", json.dumps(customers.get(uid = c1['uid']), indent = 4)
    e1 = enterprises.update(name = 'test_enterprise_updated', uid = e1['uid'])
    print "Update enterprise", json.dumps(enterprises.get(uid = e1['uid'], _hierarchy = 1), indent = 4)
    print "List enterprises", json.dumps(enterprises.all(), indent = 4)
    print "To dict enterprise", json.dumps(enterprises.to_dict(), indent = 4)
    print "Unique attributes", json.dumps(customers.list_unique_values('email'), indent = 4)



    print "Popped customer", json.dumps( customers.delete(uid = c1['uid']), indent = 4)
    print "Popped enterprise", json.dumps(enterprises.delete(uid = e1['uid']), indent = 4)
    enterprises.clear()

    
