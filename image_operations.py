import cv2
import numpy as np
import sys, os, string, tempfile
from os.path import exists as ispath, dirname, join as joinpath, abspath, split as pathsplit, splitext, getmtime as modified_time
from glob import glob
import json
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import KMeans
from pgpersist import pgpdict, pgparray, numberlist, stringlist, datetime, dtime, NoneType, date, get_meta_classes, geolocation, timedelta
import helpers as hp
import dill
import collections
from datetime import datetime

SERVER_NAME = os.environ.get('SERVER_NAME', 'localhost:5000')
HTTP = os.environ.get('HTTP', 'http')
UPLOAD_FOLDER = joinpath('static', 'uploads')
if os.environ.get('LARGE_FILE_BASE_URL') and os.environ.get('LARGE_FILE_BASE_DIR'):
    STATIC_DIR = joinpath(os.environ.get('LARGE_FILE_BASE_DIR'), UPLOAD_FOLDER)
    CDN_DOMAIN = "{}://{}/{}".format(os.environ.get('HTTP', 'http'), os.environ.get('LARGE_FILE_BASE_URL'), UPLOAD_FOLDER.replace('\\','/'))
else:
    STATIC_DIR = joinpath(dirname(dirname(abspath(__file__))), UPLOAD_FOLDER)
    CDN_DOMAIN = "{}://{}/{}".format(HTTP, os.environ.get('CDN_DOMAIN') or SERVER_NAME, UPLOAD_FOLDER.replace('\\','/'))

ROOT_PATH = joinpath(abspath(os.curdir), 'data')
logger = hp.get_logger(__name__)

class ImageDoesNotExist(hp.BaseError):
    pass

class ReadImageError(Exception):
    pass


class ImageCacheDict(pgpdict):
    _allowed_value_types = (dict,)


"""
{
    'model': <knn model>,
    'kmeans': <kmeans model>,
    'count': 50,
    'n_clusters': 4
    '0': {
            'count': 6,
            'n_clusters': 0
         },
    '1': {
            'count': 20,
            'model': <knn model>,
            'kmeans': <kmeans model>,
            'n_clusters': 2,
            '1_0': {
                    'count': 15', 
                    'n_clusters': 2,
                    'model': <knn model>,
                    'kmeans': <kmeans model>
                    '1_0_0': {'count': 7, 'n_clusters': 0},
                    '1_0_1': {'count': 8, 'n_clusters': 0}
                },
            '1_1': {'count': 5', 'n_clusters': 0}
        },
    '2': {
            'count': 9,
            'n_clusters': 0
        }
    '3': {
            'count': 15,
            'model': <knn model>,
            'kmeans': <kmeans model>,
            'n_clusters': 2,
            '3_0': {'count': 9, 'n_clusters': 0},
            '3_1': {'count': 6, 'n_clusters': 0}
        }
}
"""

def read_image(image_path, directory = None, return_path = False, do_delete = None, unchanged = False):
    if not isinstance(image_path, (str, unicode)): return image_path, image_path
    download_path = image_path
    if any(image_path.startswith(_a) for _a in ['http://', 'https://', 'www']):
        if image_path.startswith(CDN_DOMAIN):
            download_path = image_path.replace(CDN_DOMAIN, STATIC_DIR)
            if do_delete is None: do_delete = False
        else:
            do_delete = True
            tf = tempfile.NamedTemporaryFile(suffix = '.{}'.format(image_path.rsplit('.',1)[-1]), dir = directory)
            tf.close()
            hp.download_file(image_path, tf.name, max_chunks = 20480)
            logger.info("Downloaded file is at %s", tf.name)
            download_path = tf.name
    if unchanged:
        r = cv2.imread(download_path, cv2.IMREAD_UNCHANGED)
    else:
        r = cv2.imread(download_path)
    if r is None:
        if do_delete: os.remove(download_path)
        raise ImageDoesNotExist("Error reading image from {} ({}), unavailable or corrupted..".format(image_path, download_path))
    logger.info("Shape in pixels of the file %s is %s", image_path, r.shape)
    
    if return_path:
        return r, download_path
    if do_delete:
        os.remove(download_path)
    return r


class ImageMapper(object):
    
    def load(self):
        self.knn = None
        self.kmeans = None
        if ispath(self.path):
            try:
                with open(self.path, "r") as fp:
                    self.knn = dill.load(fp)
                self.n_clusters = self.knn['n_clusters']
            except dill.UnpicklingError as ex:
                hp.print_error("Could not unpicle file %s",self.path)
            except Exception as ex:
                hp.print_error(ex)
        if not self.knn:
            self.knn = {'model': NearestNeighbors(n_neighbors=1), 'kmeans': KMeans(n_clusters=self._n_clusters(), random_state=0), 'n_clusters': 0}
            self.n_clusters = 0

    def _n_clusters(self, l = None):
        if l is None: 
            l = len(self._bins)
        if l == 0:
            return 0
        if l < self.min_split: 
            return 1
        return int(min(l//self.min_split, self.max_clusters))

    def last_updated(self):
        return self.knn.get("previous_update_time", None)
       
    def dump(self):
        with open(self.path, 'w') as fp:
            dill.dump(self.knn, fp)

    def open(self, image_path, max_dimension = 2000):
        """
        if URL download, else read and return image array
        if already an array then just return
        if not isinstance(image_path, (str, unicode)): return image_path, image_path
        if image_path.startswith("http"):
            while True: 
                pth =joinpath('static', 'uploads', "{}.{}".format(hp.id_generator(), image_path.rsplit(".",1)[-1]))
                if not ispath(pth):
                    break
            image_path = hp.download_file(image_path, path=pth)


        elif not ispath(image_path): 
            raise ImageDoesNotExist("Image with the path {} does not exists".format(image_path))

        logger.info("Opening the image : %s", image_path)
        img_o = cv2.imread(image_path)
        if not isinstance(img_o, np.ndarray):
            raise ImageDoesNotExist("Image with the path {} does not exists".format(image_path))
        """
       
        img_o, _ = read_image(image_path, return_path=True)
             
        shp = img_o.shape[:-1]

        if not max_dimension and any(z > max_dimension for z in shp):
            if shp[0] > shp[1]:
                width = max_dimension
                height = int((shp[1]) * (float(width)/shp[0]))
            else:
                height = max_dimension
                width = int((shp[0]) * (float(height)/ shp[1]))
            img_o = cv2.resize(img_o,(width, height), interpolation = cv2.INTER_CUBIC)
 
        return image_path, img_o 
    
    def make_clusters(self, kmeans,model, _set = None, labels = None):
        kmeans.fit(map(lambda x:x[1], _set))
        model.fit(kmeans.cluster_centers_) 
        res = {
            'count': len(_set),
            'model': model,
            'kmeans': kmeans,
            'n_clusters': kmeans.n_clusters,
        }
        co=collections.Counter(kmeans.labels_)

        labels = labels or []
        for (l,c) in co.iteritems():
            _l = "_".join([str(i) for i in labels+[l]])
            if self._n_clusters(c) > 1:
                print "Kmeans further split:: ", self._n_clusters(c), co, _l
                res[_l] = self.make_clusters(
                        KMeans(n_clusters=self._n_clusters(c), random_state=0), 
                        NearestNeighbors(n_neighbors=1), 
                        list(_set[i] for i,lbl in enumerate(kmeans.labels_) if lbl == l),
                        labels+[l]
                    )
            else:
                print "Kmeans ::", kmeans.n_clusters, len(_set), self._n_clusters(c),_l
                res[_l] = {
                    "count": c,
                    "n_clusters" : 0
                }
            for i,lbl in enumerate(kmeans.labels_):
                if _l.endswith(str(lbl)):
                    s = self._bins[_set[i][0]]
                    s.update({_l: None})
                    self._bins[_set[i][0]] = s
        return res

    def train(self, kmeans = None, model = None, n_clusters = None):
        if not len(self._bins):
            return
        if model is None: model = self.knn['model']
        if kmeans is None: kmeans = self.knn['kmeans']
        if n_clusters is None: n_clusters = self._n_clusters()

        train_set = []
        for _,o in self._bins.iteritems():
            train_set.append((o["image_url"], o["image_vector"]))

        kmeans.n_clusters = self._n_clusters(len(train_set))
        self.knn = self.make_clusters(kmeans, model, train_set) 
        self.knn["previous_update_time"] = datetime.now() 
        self.dump()


    def yield_k(self, image_path, add = False):
        if image_path in self._bins:
            ks = self._bins[image_path]
            for kname, v in ks.iteritems():
                if kname in ['image_vector', 'image_url']:
                    continue
                yield kname
            else:
                yield '0'
            raise StopIteration
        try:
            image_path, img = self.open(image_path)
        except ImageDoesNotExist as ex:
            hp.print_error(ex)
            yield '__NULL__'
            raise StopIteration
        vec = self.get_vector(img)
        add_dict = {'image_vector': vec, 'image_url': image_path}
        def get_k(model_dict, vec, prev_ks = None):
            knn = model_dict['model']
            prev_ks = prev_ks or []
            _, indexes = knn.kneighbors([vec])
            k = str(indexes[0][0])
            _l = "_".join(list(str(z) for z in prev_ks+[k]))
            if model_dict[_l].get("n_clusters"):
                for k1 in get_k(model_dict[_l], vec, prev_ks+[k]):
                    yield k1
            yield _l
        if self.knn.get("n_clusters") == 0:
            yield '0'
        else:
            for k in get_k(self.knn, vec):
                yield k
                add_dict[k] = None
        if add:
            self._bins[image_path] = add_dict
            logger.info("Added image to matcher %s %s %s", image_path, len(self._bins.keys()), self.name)

    def __init__(self, name, **kwargs):
        self.name = hp.normalize_join(name)
        self.min_split = kwargs.get("min_split", 10)
        self.max_clusters = kwargs.get("max_clusters", 5)
        self._bins = ImageCacheDict(self.name, key_types = [unicode], key_names = ['image_url'], **kwargs)
        self.path = joinpath(ROOT_PATH, kwargs.get("DB_NAME", 'core'),"image_models", '{}.dmp'.format(self.name))
        hp.mkdir_p(dirname(self.path))
        self.load()

    def get_vector(self, img):
        VL=4
        image_path,img = self.open(img)
        width, height = img.shape[:-1]
        def find_aggregate_block(blks):
            o = []
            o.extend(np.std(blks, axis = 0))
            return o

        def make_blocks(img, width, height):
            sa = np.prod(img.shape[:2])
            wh = width*height
            i = int(np.floor(float(sa)/wh))
            ni = i*wh
            iarr = img.reshape((sa, img.shape[2]))
            niarr = iarr[:ni,:]
            bniarr = niarr.reshape((i, wh, img.shape[2]))
            return np.mean(bniarr, axis = 1)

        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_gray = img_gray.reshape(tuple(list(img.shape[:2]) + [1]))
        img = np.concatenate((img, img_gray), axis = 2)
        block_aggs = list(np.mean(np.mean(img, axis = 0), axis = 0))
        block_aggs = map(lambda x:float(x)/256, block_aggs)
        while True: 
            if width <= 1 or height <= 1 or len(block_aggs)/VL > 5:
                break
            
            image_agg = []
            blocks = make_blocks(img, width, height)
            l = find_aggregate_block(blocks)
            
            block_aggs = l + block_aggs
            width = int(width/2)
            height = int(height/2)

        for i in range(6- (len(block_aggs)/VL)):
            block_aggs = block_aggs + block_aggs[-VL:]

        
        hist = map(lambda x: float(x)/(img.shape[0]*img.shape[1]),reduce(lambda x, y: x + list(np.histogram(img[:,:,y].flatten(), 10, [0,256])[0]), range(4), []))
        block_aggs += hist
        return block_aggs


if __name__ == '__main__':
    n = ImageMapper("product_matcher_image_mapper", DB_NAME="ajanta_tiles")
    """
    with open("../../imops/trained.json", "r") as fp:
        ob = json.load(fp)

        for o in ob["data"]:
            print list(n.yield_k(o[0].replace("./","../../imops/")))
    """
    #n.train()
    #print list(n.yield_k("https://sc02.alicdn.com/kf/HTB1FtG1JVXXXXbWXpXXq6xXFXXXO/Kajaria-Floor-Tiles-and-Wall-Tile.jpg_350x350.jpg"))
    print list(n.yield_k("https://d3chc9d4ocbi4o.cloudfront.net/kajaria/Silica Ocre.png"))
    print list(n.yield_k("https://d3chc9d4ocbi4o.cloudfront.net/kajaria/Bolivia Quadro L_D.png"))
    print list(n.yield_k("https://d3chc9d4ocbi4o.cloudfront.net/kajaria/Silica Cubix.png"))



